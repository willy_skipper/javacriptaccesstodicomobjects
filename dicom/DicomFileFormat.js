﻿function DicomFileFormat() {

    var /*DcmFileMetaInfo*/ _metainfo;
    var /*DcmDataset*/ _dataset;

    this.DicomFileFormat = function (/*DcmDataset*/ dataset) {
        _metainfo = new DcmFileMetaInfo();
        _metainfo.FileMetaInformationVersion = DcmFileMetaInfo.Version;
        _metainfo.MediaStorageSOPClassUID = dataset.GetUID(DicomTags.SOPClassUID);
        _metainfo.MediaStorageSOPInstanceUID = dataset.GetUID(DicomTags.SOPInstanceUID);
        _metainfo.TransferSyntax = dataset.InternalTransferSyntax;
        _metainfo.ImplementationClassUID = Implementation.ClassUID;
        _metainfo.ImplementationVersionName = Implementation.Version;
        _metainfo.SourceApplicationEntityTitle = "";
        _dataset = dataset;
    }

    this.FileMetaInfo = function ()
    {
        if (_metainfo == null)
            _metainfo = new DcmFileMetaInfo();
        return _metainfo;
    }

    this.Dataset = function()
    {
        return _dataset;
    }

    this.ChangeTransferSyntax = function (/*DicomTransferSyntax*/ ts, /*DcmCodecParameters*/ parameters) {
        //Dataset.ChangeTransferSyntax(ts, parameters);
        //FileMetaInfo.TransferSyntax = ts;
    }

    this.CheckFileHeader = function(/*Stream*/ fstream)
    {

        var dv = new DataView(fstream, 128);
        
        var d, i, c, m;
        d = "D".charCodeAt(0);
        i = "I".charCodeAt(0);
        c = "C".charCodeAt(0);
        m = "M".charCodeAt(0);


        return (dv.getUint8(0) == d &&
                dv.getUint8(1) == i &&
                dv.getUint8(2) == c &&
                dv.getUint8(3) == m);
    }

    this.Load = function (file, stopTag, options)
    {
        var dis = this;

        fs.root.getFile(file, { create: true }, function (fileEntry) 
        {
            fileEntry.file(function (file)
            {
                var reader = new FileReader();

                reader.onloadend = function (e)
                {                    
                    if (!dis.CheckFileHeader(this.result))
                        return DicomReadStatus.UnknownError;

                    _metainfo = new DcmFileMetaInfo();
                    var dsr = new DicomStreamReader(this.result);
                    dsr.Dataset(_metainfo);
                    dsr.Read(DcmFileMetaInfo.StopTag, options | DicomReadOptions.FileMetaInfoOnly, DicomReadOptions.FileMetaInfoOnly);

                    var _dataset = new DcmDataset(_metainfo.TransferSyntax());

                    dsr.Dataset(_dataset,true);
                    dsr.Read(stopTag, DicomReadOptions.Default);
                    _dataset = dsr.Dataset();

                    console.log(_dataset.GetString(DicomTags.StudyInstanceUID, null));
                    console.log(_dataset.GetString(DicomTags.SeriesInstanceUID, null));
                    console.log(_dataset.GetString(DicomTags.SOPInstanceUID, null));

                };

                reader.readAsArrayBuffer(file);

            }, errorHandler);

        }, errorHandler);

    }

    this.WritePduValue = function (file, bytes) {

        var streLen = bytes.length;
        var _bytes = new Uint8Array(streLen);
        for (var zz = 0; zz < streLen; zz++) {
            _bytes[zz] = bytes[zz];
        }

        fs.root.getFile(file, { create: true }, function (fileEntry) {

            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function (fileWriter) {

                fileWriter.seek(fileWriter.length); // Start write position at EOF.

                // Create a new Blob and write it to log.txt.
                //var blob = new Blob(['                                                                                                                                DICM']);
                //fileWriter.write(blob);
                //
                var blob = new Blob([_bytes.buffer]);
                fileWriter.write(blob);

            }, errorHandler);

        }, errorHandler);

    }

    this.GetFileHeader = function (options)
    {
        var s = "";
        for (var i = 0; i < 128; i++) {
            s += "\0";
        }
        s += "DICM";        

        var dsw = new DicomStreamWriter(null, true);
        dsw.Write(_metainfo, options | DicomWriteOptions.CalculateGroupLengths);
        var buf = dsw.Buffer();
                

        //ekperiment
        var strBufView = new Uint8Array(str2ab(s));
        var bufView = new Uint8Array(buf);
        //konkatenacija
        var viewStr = new Uint8Array(buf.byteLength + strBufView.byteLength);
        viewStr.set(strBufView);
        viewStr.set(bufView, strBufView.byteLength);
        return viewStr.buffer;

        //return [s, buf];

    }

    this.SaveAsync = function (/*string*/ file, /*DicomWriteOptions*/ options, bytes, byteSum)
    {

        //var _bytes = new Uint8Array(byteSum);

        //var cnt = 0;
        //for (var k = 0; k < bytes.length; k++)
        //{
        //    var byLen = bytes[k].byteLength;
        //    for (var zz = 0; zz < byLen; zz++)
        //    {
        //        var viju = new Uint8Array(bytes[k]);
        //        _bytes[cnt] = viju[zz];
        //        cnt++;
        //    }
        //}


        fs.root.getFile(file, { create: true }, function (fileEntry) {

            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function (fileWriter) {

                //fileWriter.seek(128); // Start write position at EOF.

                fileWriter.onwriteend = function (e) {
                    console.log('Write completed.');
                };

                fileWriter.onerror = function (e) {
                    console.log('Write failed: ' + e.toString());
                };

                //var s = "";
                //for (var i = 0; i < 128; i++) {
                //    s += "\0";
                //}
                //s += "DICM";

                //var dsw = new DicomStreamWriter(null,true);
                //dsw.Write(_metainfo, options | DicomWriteOptions.CalculateGroupLengths);
                //var buf = dsw.Buffer();

                //var b = [s, buf];
                var blob1 = new Blob(bytes);

                // Create a new Blob and write it to log.txt.
                //var blob1 = new Blob([s, buf, new Uint8Array(bytes[0]), new Uint8Array(bytes[1]),
                //                              new Uint8Array(bytes[2]), new Uint8Array(bytes[3]),
                //                              new Uint8Array(bytes[4])]);
                //var blob2 = new Blob([buf]);

                fileWriter.write(blob1);
                //fileWriter.write(blob2);



            }, errorHandler);

        }, errorHandler);

    }
}

DcmFileMetaInfo.LoadFileMetaInfo = function(/*String*/ file, /*bool*/ useIsoStore) // useIsoStore = false)
{
    //if (useIsoStore)
    //{
    //    using (var store = IsolatedStorageFile.GetUserStoreForApplication())
    //    {
    //        using (var fs = store.OpenFile(file, FileMode.Open, FileAccess.Read))
    //        {
    //            fs.Seek(128, SeekOrigin.Begin);
    //            if (!CheckFileHeader(fs)) return null;
    //            DicomStreamReader dsr = new DicomStreamReader(fs);
    //            DcmFileMetaInfo metainfo = new DcmFileMetaInfo();
    //            dsr.Dataset = metainfo;
    //            dsr.Read(DcmFileMetaInfo.StopTag, DicomReadOptions.Default | DicomReadOptions.FileMetaInfoOnly);
    //            fs.Close();
    //            return metainfo;
    //        }
    //    }
    //}
    //else
    //{
        //using (var fs = File.OpenRead(file))
        //{
        //    fs.Seek(128, SeekOrigin.Begin);
        //    CheckFileHeader(fs);
        //    DicomStreamReader dsr = new DicomStreamReader(fs);
        //    DcmFileMetaInfo metainfo = new DcmFileMetaInfo();
        //    dsr.Dataset = metainfo;
        //    dsr.Read(DcmFileMetaInfo.StopTag, DicomReadOptions.Default | DicomReadOptions.FileMetaInfoOnly);
        //    fs.Close();
        //    return metainfo;
        //}
    //}



}