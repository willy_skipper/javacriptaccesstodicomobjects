

var DicomWriteOptions =
{
	"None":0x00000000,
	"CalculateGroupLengths":0x00000001,
	"ExplicitLengthSequence":0x00000002,
	"ExplicitLengthSequenceItem":0x00000004,
	"WriteFragmentOffsetTable":0x00000008,
	
	"Default": 0x00000001
};

var DicomReadOptions = 
{
		"None":0x00000000,
		"KeepGroupLengths":0x00000001,
		"UseDictionaryForExplicitUN":0x00000002,
		"AllowSeekingForContext":0x00000004,
		"DeferLoadingLargeElements":0x00000008,
		"DeferLoadingPixelData":0x00000010,
		"ForcePrivateCreatorToLO":0x00000020,
		"FileMetaInfoOnly":0x00000040,
		"SequenceItemOnly":0x00000080,	
		
		"Default": 0x00000002 | 0x00000004 | 0x00000008 | 0x00000010 | 0x00000020,
		"DefaultWithoutDeferredLoading": 0x00000002 | 0x00000004 | 0x00000020
};

	function Flags()
	{
		
	}

	// Flags.IsSet = function(DicomDumpOptions options, DicomDumpOptions flag) {
		// return (options & flag) == flag;
	// }

	Flags.IsSet = function(/* DicomReadOptions */ options, /* DicomReadOptions */ flag) {
		return (options & flag) == flag;
	}

	// Flags.IsSet = function(DicomWriteOptions options, DicomWriteOptions flag) {
		// return (options & flag) == flag;
	// }

	// Flags.IsSet = function(XDicomOptions options, XDicomOptions flag) {
		// return (options & flag) == flag;
	// }
