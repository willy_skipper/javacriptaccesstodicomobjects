function CEchoClient()
{
	
	var el = this;
    
    el.DcmClientBase = new DcmClientBase();
    el.DcmNetworkBase = this.DcmClientBase.DcmNetworkBase;
	
	//el.DcmNetworkBase.LogID = "C-ECHO SCP";
    el.DcmClientBase.CallingAE("ECHO_SCU");
    el.DcmClientBase.CalledAE("ECHO_SCP");
	

	//public DcmResponseCallback OnCEchoResponse;

	//#region Protected Overrides
	el.OnConnected = function() 
	{
		var /*DcmAssociate*/ associate = new DcmAssociate();

		var /*byte*/ pcid = associate.AddPresentationContext1(DicomUID.VerificationSOPClass);
		associate.AddTransferSyntax(pcid, DicomTransferSyntax.ImplicitVRLittleEndian);

		associate.CalledAE(el.DcmClientBase.CalledAE());
        associate.CallingAE(el.DcmClientBase.CallingAE());
        associate.MaximumPduLength(el.DcmClientBase.MaxPduSize());

		this.DcmNetworkBase.SendAssociateRequest(associate);
	}

	el.OnReceiveAssociateAccept = function(/*DcmAssociate*/ association) 
	{
		var /*byte*/ pcid =  el.DcmNetworkBase.Associate().FindAbstractSyntax(DicomUID.VerificationSOPClass);
		el.DcmNetworkBase.SendCEchoRequest(pcid, el.DcmNetworkBase.NextMessageID(), el.DcmClientBase.Priority());
	}

	el.OnReceiveCEchoResponse = function(/*byte*/ presentationID, /*ushort*/ messageID, /*DcmStatus*/ status) 
	{
		if (OnCEchoResponse != null)
			console.log("OnReceiveCEchoResponse");
			//OnCEchoResponse(presentationID, messageID, status);
		el.DcmNetworkBase.SendReleaseRequest();
	}
	//#endregion
}