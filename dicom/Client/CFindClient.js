function CFindQuery()
{
	//#region Private Members
    var /*DcmQueryRetrieveLevel*/ _queryLevel;
    var _userState;
	//#endregion

	//#region Public Properties
    this.QueryRetrieveLevel = function(value)
    {
        if(value == undefined) { return _queryLevel; }
        else { _queryLevel = value; }
    };

    this.UserState = function(value)
    {
        if(value == undefined) { return _userState; }
        else { _userState = value; }
    };
	//#endregion

	//#region Public Members
    this.ToDataset = function(/*DicomTransferSyntax*/ ts) 
    {
        var /*DcmDataset*/ dataset = new DcmDataset(ts);
        dataset.DcmDataset(ts);
        var level = this.QueryRetrieveLevel();
        var str = string_of_enum(DcmQueryRetrieveLevel, level).toUpperCase();
        if (level != DcmQueryRetrieveLevel.Worklist) {
            dataset.AddElementWithValue(DicomTags.QueryRetrieveLevel, DcmValueType.String, str);
        }
        
        dataset.SaveDicomFields(this);
        AdditionalMembers(dataset);
        return dataset;
    }

    //todo!
    function AdditionalMembers(/*DcmDataset*/ dataset) 
    {
		dataset.AddElement(DicomTags.StudyDate, DicomVR.DA);
        dataset.AddElement(DicomTags.Modality, DicomVR.CS);
        dataset.AddElement(DicomTags.PatientsBirthDate, DicomVR.DA);
        dataset.AddElement(DicomTags.PatientsSex, DicomVR.CS);
        //dataset.AddElement(DicomTags.SpecificCharacterSet, DicomVR.CS);
        dataset.AddElement(DicomTags.NumberOfStudyRelatedSeries, DicomVR.IS);
        dataset.AddElement(DicomTags.NumberOfStudyRelatedInstances, DicomVR.IS);
    }
	//#endregion
}

function CFindResponse()
{
    //#region Protected Members
	var /*DcmDataset*/ _dataset;
	//#endregion

	//#region Public Properties
	this.Dataset = function()
	{
        return _dataset;
    }
	//#endregion

	//#region Public Members
	this.FromDataset = function(/*DcmDataset*/ dataset) 
	{
		_dataset = dataset;
        dataset.LoadDicomFields(this);
    }
	//#endregion
}

function CFindClient() //DcmClientBase
{
    //<Tq, Tr> : DcmClientBase
    //where Tq : CFindQuery
    //where Tr : CFindResponse 

    var el = this;
    
    el.DcmClientBase = new DcmClientBase();
    el.DcmNetworkBase = this.DcmClientBase.DcmNetworkBase;

    //#region Private Members
    var _findSopClass;
    var _queries;
    var _current;
    //#endregion

    //#region Public Constructor
    //this.CFindClient = function() // : base() {
    //{
    el.DcmNetworkBase.LogID = "C-Find SCU";
    el.DcmClientBase.CallingAE("FIND_SCU");
    el.DcmClientBase.CalledAE("FIND_SCP");
        _queries = []; //new Queue<Tq>();
        _findSopClass = DicomUID.StudyRootQueryRetrieveInformationModelFIND;
        _current = null;
    //}
    //#endregion

//#region Public Properties
    //public delegate void CFindResponseDelegate(Tq query, Tr result);
    //public CFindResponseDelegate OnCFindResponse;

    //public delegate void CFindCompleteDelegate(Tq query);
    //public CFindCompleteDelegate OnCFindComplete;

    el.FindSopClassUID = function (value)
    {
        if(value == undefined) { return _findSopClass; }
        else { _findSopClass = value; }
    }

    el.FindQueries = function (value)
    {
        if(value == undefined) { return _queries; }
        else { _queries = value; }
    }
    //#endregion

    //#region Public Members
    el.AddQuery = function (/*Tq == CFindQuery*/ query)
    {
        _queries.push(query);
    }
	//#endregion

	//#region Protected Overrides
    el.OnConnected = function ()
    {
        var /*DcmAssociate*/ associate = new DcmAssociate();

        var pcid = associate.AddPresentationContext1(el.FindSopClassUID());
        //associate.AddTransferSyntax(pcid, DicomTransferSyntax.ExplicitVRLittleEndian);
        associate.AddTransferSyntax(pcid, DicomTransferSyntax.ImplicitVRLittleEndian);

        associate.CalledAE(el.DcmClientBase.CalledAE());
        associate.CallingAE(el.DcmClientBase.CallingAE());
        associate.MaximumPduLength(el.DcmClientBase.MaxPduSize());

        this.DcmNetworkBase.SendAssociateRequest(associate);
    }

    function PerformQueryOrRelease() 
    {
        if (el.FindQueries().length > 0)
        {
            var query = el.FindQueries().pop();
            _current = query;

            var pcid = el.DcmNetworkBase.Associate().FindAbstractSyntax(el.FindSopClassUID());

            if (el.DcmNetworkBase.Associate().GetPresentationContextResult(pcid) == DcmPresContextResult.Accept)
            {
                //var dataset = query.ToDataset(el.DcmNetworkBase.Associate.GetAcceptedTransferSyntax(pcid));
                el.DcmNetworkBase.SendCFindRequest(pcid, el.DcmNetworkBase.NextMessageID(), el.DcmClientBase.Priority(), _current);
            }
            else
            {
                //Log.Info("{0} <- Presentation context rejected: {1}", LogID, Associate.GetPresentationContextResult(pcid));
                el.DcmNetworkBase.SendReleaseRequest();
            }
        }
        else 
        {
            el.DcmNetworkBase.SendReleaseRequest();
        }
    }

    this.OnReceiveAssociateAccept = function(/*DcmAssociate*/ association) 
    {
        PerformQueryOrRelease();
    }

    function OnReceiveCFindResponse(presentationID, messageID, dataset, status) 
    {
        if (status.State != DcmState.Pending) 
        {
            if (OnCFindComplete != null) 
            {
                //OnCFindComplete(_current);
            }
            //PerformQueryOrRelease();
        }
        else if (dataset != null) 
        {
            if (OnCFindResponse != null) {
                //var result = Activator.CreateInstance<Tr>();
                //result.FromDataset(dataset);
                //OnCFindResponse(_current, result);
            }
        }
    }
	//#endregion
}

	//#region Study
function CFindStudyQuery() //CFindQuery 
{
    this.CFindQuery = new CFindQuery();

    //#region Private Members
    var _patientId;
    var _patientName;
    var /*DcmDateRange*/ _studyDate;
    var /*DcmDateRange*/ _studyTime;
    var _studyId;
    var _accessionNumber;
    var _studyInstanceUid;
    var _modalitiesInStudy;
    var _studyDescription;
    var _institutionName;
    //#endregion

    //#region Public Constructors
    //public CFindStudyQuery() 
    //{
    this.CFindQuery.QueryRetrieveLevel(DcmQueryRetrieveLevel.Study);
    _studyDate = new DcmDateRange();
	//_studyDate.DcmDateRange("19001010");
	
    //}
    //#endregion

    //#region Public Properties
    //[DicomField(DicomConstTags.PatientID, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    this.PatientID = function(value)
    {
        if(value == undefined) { return _patientId; }
        else { _patientId = value; }
    }

    //[DicomField(DicomConstTags.PatientsName, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string PatientsName {
    //    get { return _patientName; }
    //    set { _patientName = value; }
    //}

    //[DicomField(DicomConstTags.StudyDate, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    this.StudyDate = function(value){
        if(value == undefined) { return _studyDate; }
        else { _studyDate = value; }
    }

    //[DicomField(DicomConstTags.StudyTime, DefaultValue = DicomFieldDefault.MinValue, CreateEmptyElement = true)]
    //public DcmDateRange StudyTime {
    //    get { return _studyTime; }
    //    set { _studyTime = value; }
    //}

    //[DicomField(DicomConstTags.StudyID, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string StudyID {
    //    get { return _studyId; }
    //    set { _studyId = value; }
    //}

    //[DicomField(DicomConstTags.AccessionNumber, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string AccessionNumber {
    //    get { return _accessionNumber; }
    //    set { _accessionNumber = value; }
    //}

    //[DicomField(DicomConstTags.StudyInstanceUID, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string StudyInstanceUID {
    //    get { return _studyInstanceUid; }
    //    set { _studyInstanceUid = value; }
    //}

    //[DicomField(DicomConstTags.ModalitiesInStudy, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string ModalitiesInStudy {
    //    get { return _modalitiesInStudy; }
    //    set { _modalitiesInStudy = value; }
    //}

    //[DicomField(DicomConstTags.StudyDescription, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string StudyDescription {
    //    get { return _studyDescription; }
    //    set { _studyDescription = value; }
    //}

    //[DicomField(DicomConstTags.InstitutionName, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string InstitutionName {
    //    get { return _institutionName; }
    //    set { _institutionName = value; }
    //}
    //#endregion

    //#region Protected Members
    function AdditionalMembers(/*DcmDataset*/ dataset) 
    {
        dataset.AddElement(DicomTags.StudyDate, DicomVR.DA);
		dataset.AddElement(DicomTags.Modality, DicomVR.CS);
        dataset.AddElement(DicomTags.PatientsBirthDate, DicomVR.DA);
        dataset.AddElement(DicomTags.PatientsSex, DicomVR.CS);
        //dataset.AddElement(DicomTags.SpecificCharacterSet, DicomVR.CS);
        dataset.AddElement(DicomTags.NumberOfStudyRelatedSeries, DicomVR.IS);
        dataset.AddElement(DicomTags.NumberOfStudyRelatedInstances, DicomVR.IS);
    }
	//#endregion
}

function CFindStudyResponse() // : CFindResponse 
{

    this.base = new CFindResponse();

    //#region Private Members
    var _patientId;
    var _patientName;
    var _patientBirthDate;
    var _patientSex;
    var _studyId;
    var _accessionNumber;
    var _studyInstanceUid;
    var _modality;
    var _modalitiesInStudy;
    var _studyDescription;
    var _institutionName;
    var _studyDate;
    var _studyTime;
    var _numberOfStudyRelatedSeries;
    var _numberOfStudyRelatedInstances;
    //#endregion

    //#region Public Properties
    //[DicomField(DicomConstTags.PatientID, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    this.PatientID = function(value)
    {
        if(value == undefined) { return _patientId; }
        else { _patientId = value; }
    }

    //[DicomField(DicomConstTags.PatientsName, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string PatientsName {
    //    get { return _patientName; }
    //    set { _patientName = value; }
    //}

    //[DicomField(DicomConstTags.PatientsBirthDate, DefaultValue = DicomFieldDefault.MinValue, CreateEmptyElement = true)]
    //public DateTime PatientsBirthDate {
    //    get { return _patientBirthDate; }
    //    set { _patientBirthDate = value; }
    //}

    //[DicomField(DicomConstTags.PatientsSex, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string PatientsSex {
    //    get { return _patientSex; }
    //    set { _patientSex = value; }
    //}

    //[DicomField(DicomConstTags.StudyDate, DefaultValue = DicomFieldDefault.MinValue, CreateEmptyElement = true)]
    this.StudyDate = function(value){
        if(value == undefined) { return _studyDate; }
        else { _studyDate = value; }
    }

    //[DicomField(DicomConstTags.StudyTime, DefaultValue = DicomFieldDefault.MinValue, CreateEmptyElement = true)]
    //public DateTime StudyTime {
    //    get { return _studyTime; }
    //    set { _studyTime = value; }
    //}

    //[DicomField(DicomConstTags.StudyID, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string StudyID {
    //    get { return _studyId; }
    //    set { _studyId = value; }
    //}

    //[DicomField(DicomConstTags.AccessionNumber, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string AccessionNumber {
    //    get { return _accessionNumber; }
    //    set { _accessionNumber = value; }
    //}

    //[DicomField(DicomConstTags.StudyInstanceUID, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string StudyInstanceUID {
    //    get { return _studyInstanceUid; }
    //    set { _studyInstanceUid = value; }
    //}

    //[DicomField(DicomConstTags.Modality, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string Modality {
    //    get { return _modality; }
    //    set { _modality = value; }
    //}

    //[DicomField(DicomConstTags.ModalitiesInStudy, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string ModalitiesInStudy {
    //    get { return _modalitiesInStudy; }
    //    set { _modalitiesInStudy = value; }
    //}

    //[DicomField(DicomConstTags.StudyDescription, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string StudyDescription {
    //    get { return _studyDescription; }
    //    set { _studyDescription = value; }
    //}

    //[DicomField(DicomConstTags.InstitutionName, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public string InstitutionName {
    //    get { return _institutionName; }
    //    set { _institutionName = value; }
    //}

    //[DicomField(DicomConstTags.NumberOfStudyRelatedSeries, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public int NumberOfStudyRelatedSeries {
    //    get { return _numberOfStudyRelatedSeries; }
    //    set { _numberOfStudyRelatedSeries = value; }
    //}

    //[DicomField(DicomConstTags.NumberOfStudyRelatedInstances, DefaultValue = DicomFieldDefault.Default, CreateEmptyElement = true)]
    //public int NumberOfStudyRelatedInstances {
    //    get { return _numberOfStudyRelatedInstances; }
    //    set { _numberOfStudyRelatedInstances = value; }
    //}
    //#endregion
}

//function CFindStudyClient : CFindClientT<CFindStudyQuery, CFindStudyResponse> {
//    public CFindStudyClient() : base() {
//    FindSopClassUID = DicomUID.StudyRootQueryRetrieveInformationModelFIND;
//}

//#endregion