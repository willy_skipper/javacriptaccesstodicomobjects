﻿function CMoveQuery()
{
	//#region Private Members
    var /*DcmQueryRetrieveLevel*/ _queryLevel;
    var _patientId;
    var _studyUid;
    var _seriesUid;
    var _instanceUid;
    var _userState;
	//#endregion

	//#region Public Constructors
    //this.CMoveQuery() {
        _queryLevel = DcmQueryRetrieveLevel.Study;
    //}

    this.CMoveQuery = function(/*DcmQueryRetrieveLevel*/ level) 
    {
			_queryLevel = level;
    }

    this.CMoveQuery = function(/*DcmQueryRetrieveLevel*/ level, /*string*/ uid) 
    {
	    _queryLevel = level;
        if (_queryLevel == DcmQueryRetrieveLevel.Patient)
            _patientId = uid;
        else if (_queryLevel == DcmQueryRetrieveLevel.Study)
            _studyUid = uid;
        else if (_queryLevel == DcmQueryRetrieveLevel.Series)
            _seriesUid = uid;
        else
        _instanceUid = uid;
    }

    //this.CMoveQuery = function(studyUid) {
	//    queryLevel = DcmQueryRetrieveLevel.Study;
    //    _studyUid = studyUid;
    //}

    //public CMoveQuery(string studyUid, string seriesUid) {
	//		    _queryLevel = DcmQueryRetrieveLevel.Series;
    //_studyUid = studyUid;
    //_seriesUid = seriesUid;
    //}

    //public CMoveQuery(string studyUid, string seriesUid, string instUid) {
	//		    _queryLevel = DcmQueryRetrieveLevel.Image;
    //_studyUid = studyUid;
    //_seriesUid = seriesUid;
    //_instanceUid = instUid;
    //}
	//#endregion

    //#region Public Properties
    this.QueryRetrieveLevel = function(value) 
    {
        if(value == undefined) { return _queryLevel; }
        else { _queryLevel = value; }
    }

    this.PatientID = function (value)
    {
        if(value == undefined) { return _patientId; }
        else { _patientId = value; }
    }

   this.StudyInstanceUID = function(value)
    {
        if(value == undefined) { return _studyUid; }
        else {
            _studyUid = value;
            if (_queryLevel < DcmQueryRetrieveLevel.Study)
                _queryLevel = DcmQueryRetrieveLevel.Study;
        }
    }

   this.SeriesInstanceUID = function (value)
    {
        if(value == undefined) { return _seriesUid; }
        else {
            _seriesUid = value;
            if (_queryLevel < DcmQueryRetrieveLevel.Series)
                _queryLevel = DcmQueryRetrieveLevel.Series;
        }
    }

   this.SOPInstanceUID = function (value)
    {
        if(value == undefined) { return _instanceUid; }
        else {
            _instanceUid = value;
            if (_queryLevel < DcmQueryRetrieveLevel.Image)
                _queryLevel = DcmQueryRetrieveLevel.Image;
        }
    }

    this.ToDataset = function() 
    {
        var dataset = new DcmDataset();
        switch (_queryLevel) {
            case DcmQueryRetrieveLevel.Patient:
                dataset.AddElementWithValueString2(DicomTags.QueryRetrieveLevel, "PATIENT");
                dataset.AddElementWithValueString2(DicomTags.PatientID, this.PatientID());
                break;
            case DcmQueryRetrieveLevel.Study:
                dataset.AddElementWithValueString2(DicomTags.QueryRetrieveLevel, "STUDY");
                if (!String.IsNullOrEmpty(this.PatientID()))
                    dataset.AddElementWithValueString2(DicomTags.PatientID, this.PatientID());
                dataset.AddElementWithValueString2(DicomTags.StudyInstanceUID, this.StudyInstanceUID());
                break;
            case DcmQueryRetrieveLevel.Series:
                dataset.AddElementWithValueString2(DicomTags.QueryRetrieveLevel, "SERIES");
                if (!String.IsNullOrEmpty(this.PatientID()))
                    dataset.AddElementWithValueString2(DicomTags.PatientID, this.PatientID());
                if (!String.IsNullOrEmpty(this.StudyInstanceUID()))
                    dataset.AddElementWithValueString2(DicomTags.StudyInstanceUID, this.StudyInstanceUID());
                dataset.AddElementWithValueString2(DicomTags.SeriesInstanceUID, this.SeriesInstanceUID());
                break;
            case DcmQueryRetrieveLevel.Image:
                dataset.AddElementWithValueString2(DicomTags.QueryRetrieveLevel, "IMAGE");
                if (!String.IsNullOrEmpty(this.PatientID()))
                    dataset.AddElementWithValueString2(DicomTags.PatientID, this.PatientID());
                if (!String.IsNullOrEmpty(this.StudyInstanceUID()))
                    dataset.AddElementWithValueString2(DicomTags.StudyInstanceUID, this.StudyInstanceUID());
                if (!String.IsNullOrEmpty(this.SeriesInstanceUID()))
                    dataset.AddElementWithValueString2(DicomTags.SeriesInstanceUID, this.SeriesInstanceUID());
                dataset.AddElementWithValueString2(DicomTags.SOPInstanceUID, this.SOPInstanceUID());
                break;
            default:
                break;
        }
        return dataset;
    }

    this.UserState = function(value)
    {
        if(value == undefined) { return _userState; }
        else { _userState = value; }
    }
	//#endregion
}

function CMoveClient() // : DcmClientBase {
{
    var el = this;    
    this.DcmClientBase = new DcmClientBase();
    this.DcmNetworkBase = this.DcmClientBase.DcmNetworkBase;

	//#region Private Members
    var _destAe;
    var /*DicomUID*/ _moveSopClass;
    var _moveQueries = [];
    var /*CMoveQuery*/ _current;
	//#endregion

	//#region Public Constructor
    //public CMoveClient() : base() {
        this.DcmNetworkBase.LogID = "C-Move SCU";
        this.DcmClientBase.CallingAE("MOVE_SCU");
        this.DcmClientBase.CalledAE("MOVE_SCP");
        _moveSopClass = DicomUID.StudyRootQueryRetrieveInformationModelMOVE;
        //_moveQueries = new Queue<CMoveQuery>();
        _current = null;
    //}
	//#endregion

	//#region Public Properties
    //public CMoveResponseDelegate OnCMoveResponse;

    this.DestinationAE = function(value)
    {
        if(value == undefined) { return _destAe; }
        else { _destAe = value; }
    }

    this.MoveSopClassUID = function(value)
    {
        if(value == undefined) { return _moveSopClass; }
        else { _moveSopClass = value; }
    }
	//#endregion

	//#region Public Members
    this.AddQuery = function(/*CMoveQuery*/ query) 
    {
	    _moveQueries.push(query);
    }

    this.AddQuery1 = function(/*DcmQueryRetrieveLevel*/ level, instance) 
    {
	    this.AddQuery(new CMoveQuery(level, instance));
    }
	//#endregion

	//#region Protected Overrides
    this.OnConnected = function() 
    {
        var associate = new DcmAssociate();

        var pcid = associate.AddPresentationContext1(_moveSopClass);
        associate.AddTransferSyntax(pcid, DicomTransferSyntax.ExplicitVRLittleEndian);
        associate.AddTransferSyntax(pcid, DicomTransferSyntax.ImplicitVRLittleEndian);

        associate.CalledAE(el.DcmClientBase.CalledAE());
        associate.CallingAE(el.DcmClientBase.CallingAE());
        associate.MaximumPduLength(el.DcmClientBase.MaxPduSize());

        this.DcmNetworkBase.SendAssociateRequest(associate);
    }

    function PerformQueryOrRelease() 
    {
        if (_moveQueries.length > 0) 
        {
            var pcid = el.DcmNetworkBase.Associate().FindAbstractSyntax(el.MoveSopClassUID());
            if (el.DcmNetworkBase.Associate().GetPresentationContextResult(pcid) == DcmPresContextResult.Accept)
            {
                _current = _moveQueries.pop();
                el.DcmNetworkBase.SendCMoveRequest(pcid, 1, el.DestinationAE(), el.DcmClientBase.Priority(), _current.ToDataset());
            }
            else {
                //Log.Info("{0} -> Presentation context rejected: {1}", LogID, Associate.GetPresentationContextResult(pcid));
                el.DcmNetworkBase.SendReleaseRequest();
            }
        }
        else 
        {
            el.DcmNetworkBase.SendReleaseRequest();
        }
    }

    this.OnReceiveAssociateAccept = function(/*DcmAssociate*/ association) 
    {
    	PerformQueryOrRelease();
    }

    //function OnReceiveCMoveResponse(byte presentationID, ushort messageID, DcmDataset dataset, 
    //                                DcmStatus status, ushort remain, ushort complete, ushort warning, ushort failure) 
    //{
    //    if (OnCMoveResponse != null) 
    //    {
    //    OnCMoveResponse(_current, dataset, status, remain, complete, warning, failure);
    //    }
    //    if (remain == 0 && status != DcmStatus.Pending) 
    //    {
    //        PerformQueryOrRelease();
    //    }
    //}
	//#endregion
}