﻿

    //public delegate void DcmResponseCallback(byte presentationID, ushort messageID, DcmStatus status);

function DcmClientBase() // : DcmNetworkBase 
{
    this.DcmNetworkBase = new DcmNetworkBase();    

	//#region Private Members
    var _callingAe;
    var _calledAe;
    var _maxPdu;
    var _priority;
    var _closedEvent;
    var _closedOnError;
    var _error;
    var _userState;
	//#endregion

	//#region Public Constructors
    //this.DcmClientBase = function()
    //{
        _maxPdu = 32768;
        _priority = DcmPriority.High;
        _closedOnError = false;
        //_closedEvent = new ManualResetEvent(false);
        _error = "No error";
    //}
	//#endregion

	//#region Public Properties
    this.CallingAE = function(value)
    {
        if(value == undefined) { return _callingAe; }
        else { _callingAe = value; }
    }

    this.CalledAE = function (value)
    {
        if(value == undefined) { return _calledAe; }
        else {
            _calledAe = value;
            LogID = _calledAe;
        }
    }

    this.MaxPduSize = function (value)
    {
        if(value == undefined) { return _maxPdu; }
        else { _maxPdu = value; }
    }

    this.ClosedOnError = function()
    {
        return _closedOnError;
    }

    this.Priority = function (value)
    {
        if(value == undefined) { return _priority; }
        else { _priority = value; }
    }

    this.ErrorMessage = function()
    {
        return _error;
    }

    this.UserState = function (value)
    {
        if(value == undefined) { return _userState; }
        else { _userState = value; }
    }
	//#endregion

	//#region Public Methods
    this.Release = function()
    {
        if (!IsClosed)
            this.DcmNetworkBase.SendReleaseRequest();
    }

    this.ForceClose = function()
    {
        _closedOnError = true;
        if (this.DcmNetworkBase.Socket != null)
            this.DcmNetworkBase.SocketClose();
    }

    this.Close = function() {
        InternalClose(true);
    }

    function InternalClose(fireClosedEvent) 
    {
        this.DcmNetworkBase.ShutdownNetwork();
		//if (_closedEvent != null && fireClosedEvent) 
		//{
        //    _closedEvent.Set();
        //    _closedEvent = null;
        //}
    }

    this.Wait = function() 
    {
        if (_closedEvent != null)
            _closedEvent.WaitOne();
        return !_closedOnError;
    }

    this.Wait = function(timeout) 
    {
        if (_closedEvent != null) 
        {
            if (!_closedEvent.WaitOne(timeout))
                ForceClose(); 
        }
        return !_closedOnError;
    }
	//#endregion

	//#region DcmNetworkBase Overrides
    function OnConnectionClosed() 
    {
        this.Close();
    }

    function OnNetworkError(e) 
    {
		_error = e.Message;
        _closedOnError = true;
        this.Close();
    }

    function OnDimseTimeout() {
        _closedOnError = true;
        _error = "DIMSE Timeout";
        this.Close();
    }

    function OnReceiveAssociateReject(/*DcmRejectResult*/ result, /*DcmRejectSource*/ source, /*DcmRejectReason*/ reason) 
    {
		_closedOnError = true;
		this.Close();
    }

    function OnReceiveAbort(/*DcmAbortSource*/ source, /*DcmAbortReason*/ reason) 
    {
	    _closedOnError = true;
	    this.Close();
    }

    function OnReceiveReleaseResponse() 
    {
        _closedOnError = false;
        this.Close();
    }

    function OnReceiveReleaseRequest() 
    {
        this.DcmNetworkBase.SendReleaseResponse();
    }
    //#endregion

}
