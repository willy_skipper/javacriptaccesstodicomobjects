
//inherits DcmItem
function DcmElement(/* DicomTag */ tag, /* DicomVR */ vr, /* long */ pos, /* Endian */ endian, /* ByteBuffer */ buffer)
{
    //var DcmItem = new DcmItem();
	
	// #region Protected Members
	var /* ByteBuffer */ _bb; //ArrayBuffer !?!?!?! ili Uint8Array ??

	//konstruktor		
	this.inherit = new DcmItem(/* DicomTag */ tag, /* DicomVR */ vr, /* long */ pos, /* Endian */ endian);
		
	if(buffer == undefined){
	    _bb = new ByteBuffer();
	}
	else{
		
		_bb = buffer;
	}	

	//this.kuruza = _bb.Length();

	// #region Public Properties
	/// <summary>
	/// Gets the length of the internal byte buffer
	/// </summary>
	/// <value>The length.</value>
	this.Length = function()
	{
		if (_bb == null)
		{
			return 0;
		}
			
		return _bb.Length();	
	}

	/// <summary>
	/// Gets or sets the byte buffer.
	/// </summary>
	this.ByteBuffer = function(value){
		if(value == undefined) {
			
			if (_bb == null){
				_bb = new ArrayBuffer();
			}
			
			return _bb;
		}
		else { _bb = value; }
	}
    // #endregion


	// #region DcmItem Methods
	//override Dcm item metode
	this.CalculateWriteLength = function(/* DicomTransferSyntax */ syntax, /* DicomWriteOptions */ options) {
		var length = 4; // element tag
		if (syntax.IsExplicitVR) {
			length += 2; // vr
			if (this.inherit.VR().Is16BitLengthField())
				length += 2;
			else
				length += 6;
		} else {
			length += 4; // length tag				
		}
		length += this.Length();
		return length;
	}

	function ChangeEndianInternal() 
	{
		// if (ByteBuffer.Endian != Endian) {
			// ByteBuffer.Endian = Endian;
			// ByteBuffer.Swap(VR.UnitSize);
		// }
	}
	
	
	function Preload() {
		if (_bb != null){
			//_bb.Preload();
		}
	}

	function Unload() {
		if (_bb != null){
			//_bb.Unload();
			}
	}	
	
	
	this.Clone = function(){
		return DcmElement.Create(Tag, VR, StreamPosition, Endian, ByteBuffer() );
	}
	
}

	// #region Static Create Methods
    DcmElement.Create = function (/* DicomTag */tag) {
		var /* DicomVR */ vr = tag.Entry().DefaultVR();
		return DcmItem.Create1(tag, vr);
	}

    DcmElement.Create1 = function (/* DicomTag */tag, /* DicomVR */vr) {
		return DcmItem.Create2(tag, vr, 0, /*Endian.LocalMachine*/ "little");
	}

    DcmElement.Create2 = function (/* DicomTag */tag, /* DicomVR */vr, /* long */pos, /* Endian */endian) {
		return DcmItem.Create3(tag, vr, pos, endian, null);
	}

    DcmElement.Create3 = function (/* DicomTag */tag, /* DicomVR */vr, /* long */pos, /* Endian */endian, /* ByteBuffer */buffer) {



        if (vr == DicomVR.SQ) {
			alert("Sequence Elements should be created explicitly");
			return;
		}

		switch (vr.VR()) 
		{
		case "AE":
			return new DcmApplicationEntity(tag, pos, endian, buffer);
		case "AS":
			return new DcmAgeString(tag, pos, endian, buffer);
		case "AT":
			return new DcmAttributeTag(tag, pos, endian, buffer);
		case "CS":
			return new DcmCodeString(tag, pos, endian, buffer);
		case "DA":
			return new DcmDate(tag, pos, endian, buffer);
		case "DS":
			return new DcmDecimalString(tag, pos, endian, buffer);
		case "DT":
			return new DcmDateTime(tag, pos, endian, buffer);
		case "FD":
			return new DcmFloatingPointDouble(tag, pos, endian, buffer);
		case "FL":
			return new DcmFloatingPointSingle(tag, pos, endian, buffer);
		case "IS":
			return new DcmIntegerString(tag, pos, endian, buffer);
		case "LO":
			return new DcmLongString(tag, pos, endian, buffer);
		case "LT":
			return new DcmLongText(tag, pos, endian, buffer);
		case "OB":
			return new DcmOtherByte(tag, pos, endian, buffer);
		case "OF":
			return new DcmOtherFloat(tag, pos, endian, buffer);
		case "OW":
			return new DcmOtherWord(tag, pos, endian, buffer);
		case "PN":
			return new DcmPersonName(tag, pos, endian, buffer);
		case "SH":
			return new DcmShortString(tag, pos, endian, buffer);
		case "SL":
			return new DcmSignedLong(tag, pos, endian, buffer);
		case "SS":
			return new DcmSignedShort(tag, pos, endian, buffer);
		case "ST":
			return new DcmShortText(tag, pos, endian, buffer);
		case "TM":
			return new DcmTime(tag, pos, endian, buffer);
		case "UI":
			return new DcmUniqueIdentifier(tag, pos, endian, buffer); //ova
		case "UL":
			return new DcmUnsignedLong(tag, pos, endian, buffer); //ova
		case "UN":
			return new DcmUnknown(tag, pos, endian, buffer);
		case "US":
			return new DcmUnsignedShort(tag, pos, endian, buffer); //ovo
		case "UT":
			return new DcmUnlimitedText(tag, pos, endian, buffer);
		default:
			break;
		}
		
		alert("Unhandled VR: " + vr.VR());
	}
	// #endregion


	//inherits DcmElement
	function DcmMultiStringElement(/* DicomTag */ tag, /* DicomVR */ vr, /* long */ pos, /* Endian */ endian, /* ByteBuffer */ buffer)
	{
	    this.inherit;

	    this.Name = "DcmMultiStringElement";
	
/* 		// #region Public Constructors
		public DcmMultiStringElement(DicomTag tag, DicomVR vr) : base(tag, vr) {
		}

		public DcmMultiStringElement(DicomTag tag, DicomVR vr, long pos, Endian endian)
			: base(tag, vr, pos, endian) {
		}

		public DcmMultiStringElement(DicomTag tag, DicomVR vr, long pos, Endian endian, ByteBuffer buffer)
			: base(tag, vr, pos, endian, buffer) {
		}
		// #endregion */
		
		
	    this.inherit = new DcmElement(tag, vr, pos, endian, buffer);

		// #region Abstract Overrides
		var _vm = -1;
		this.GetVM = function() 
		{
			if (_vm == -1){
				_vm = this.GetValues().length;
			}
			return _vm;
		}

		this.GetValueString = function() {
		    return this.inherit.ByteBuffer().GetString().replace(' ', '\0');
		}
		this.SetValueString = function(/* string */ val) {
			//ByteBuffer.SetString(val, VR.Padding);
		}

		this.GetValueType = function() {
			//return typeof(string);
		}
		this.GetValueObject = function() {
			return this.GetValue();
		}
		this.GetValueObjectArray = function() {
			return this.GetValues();
		}
		this.SetValueObject = function(/* object */ val) {
			if (val.GetType() != GetValueType())
				alert("Invalid type for Element VR!");
			this.SetValue(val);
		}
		this.SetValueObjectArray = function(/* object[] */ vals) {
			if (vals.length == 0)
				this.SetValues(new string[0]);
			if (vals[0].GetType() != this.GetValueType())
				alert("Invalid type for Element VR!");
			this.SetValues(vals);
		}
		// #endregion

		// #region Public Members
		this.GetValue = function() {
			return this.GetValue1(0);
		}

		this.GetValue1 = function(index) 
		{
			var /* string[] */ vals = this.GetValues();
			if (index >= vals.length)
				alert("Value index out of range");
			return vals[index].replace(' ', '\0');
		}

		this.GetValues = function() {
			return this.GetValueString().replace(' ', '\0').split('\\');
		}

		this.GetValueList = function ()
		{
			//return new List<string>(GetValues());
		}

		this.SetValue = function(/*string*/ value) {
		    var buff = this.inherit.ByteBuffer();
		    buff.SetString(value, this.inherit.inherit.VR().Padding());
		}

		this.SetValues = function(/*string[]*/ values) {
		    this.inherit.ByteBuffer().SetString(values.join("\\"), VR.Padding());
		}

		this.SetValues = function(/*IEnumerable<string>*/ values) {
			//StringBuilder sb = new StringBuilder();
			//var valueEnum = values.GetEnumerator();
			//if (valueEnum.MoveNext()) {
			//	sb.Append(valueEnum.Current);
			//	while (valueEnum.MoveNext()) {
			//		sb.Append("\\");
			//		sb.Append(valueEnum.Current);
			//	}
			//}
			//ByteBuffer.SetString(sb.ToString(), VR.Padding);
		}

		// #endregion
	}
	
    function DcmDateElementBase(/* DicomTag */ tag, /* DicomVR */ vr, /* long */ pos, /* Endian */ endian, /* ByteBuffer */ buffer)
    {
        this.inherit = new DcmMultiStringElement(tag, DicomVR.UI, pos, endian, buffer);
        this.Type = DcmElementType.DcmDateElementBase;

        this.SetDateTimeRange = function (/*DcmDateRange*/ range)
        {
			if (range != null)
			    this.inherit.SetValue(range.toString(_formats[0]));
            else
			    this.inherit.SetValue(String.Empty);
        }
    }

	function DcmStringElement(/* DicomTag */ tag, /* DicomVR */ vr, /* long */ pos, /* Endian */ endian, /* ByteBuffer */ buffer)
	{
	    this.Name = "DcmStringElement";

	    this.inherit = new DcmElement(tag, vr, pos, endian, buffer);

	    this.SetValue = function (/*string*/ value)
	    {
	        this.inherit.ByteBuffer().SetString(value, vr.Padding());
	    }

	    this.GetValueString = function ()
	    {
	        return this.inherit.ByteBuffer().GetString().rtrimWithNull();
	    }

	    this.GetValue = function (index)
	    {
	        if (index != 0) {
	            alert("Non-zero index used for single value string element");
	        }

	        return this.GetValueString().rtrimWithNull();
        }
	}
	
	function DcmValueElement(tag, vr, pos, endian, buffer)
	{
	    var /*string*/ StringFormat;
	    var /*NumberStyles*/ NumberStyle;

	    this.Name = "DcmValueElement";

	    this.inherit = new DcmElement(tag, vr, pos, endian, buffer);

	    this.DcmValueElement1 = function(tag, vr)
	    {
	        StringFormat = "{0}";
	        NumberStyle = NumberStyles.Any;
	    }

	    this.DcmValueElement2 = function(tag, vr, pos, endian)
	    {
	        StringFormat = "{0}";
	        NumberStyle = NumberStyles.Any;
	    }

	    this.DcmValueElement3 = function(tag, vr, pos, endian, buffer)
	    {
	        StringFormat = "{0}";
	        NumberStyle = NumberStyles.Any;
	    }

        //#region Abstract Overrides
	    this.GetVM = function () {

	        var VR = this.inherit.inherit.VR();
	        var buf = this.inherit.ByteBuffer();
	        return buf.Length() / VR.UnitSize();
	    }

	    this.GetValue = function () {
	        return this.GetValue1(0);
	    }



	    this.GetValue1 = function(index) 
	    {
	        if (index >= this.GetVM())
	        {
	            alert("Value index out of range");
	        }
	        //Endian = Endian.LocalMachine;
	        //T[] vals = new T[1];

	        var VR = this.inherit.inherit.VR();

	        var vals = this.inherit.ByteBuffer().BlockCopy(index * VR.UnitSize(), 0, VR.UnitSize());
            
	        var ret = new Uint16Array(vals.buffer);

	        return ret[0];
        }

	    this.GetValueString = function() 
	    {	        
	        var vals = GetValues();
	        var /*StringBuilder*/ sb = new StringBuilder();
	        
	        //foreach (T val in vals) 
	        //{
			//	sb.AppendFormat(StringFormat, val).Append('\\');
	        //}

	        //if (sb.Length > 0) 
	        //{
	        //    sb.Length = sb.Length - 1;
	        //}

	        return sb.ToString();
        }

	    this.ParseNumber = function(/*string*/ val) 
	    {
            try 
	        {
                if (typeof(T) == typeof(byte)) {
                    return byte.Parse(val, NumberStyle, CultureInfo.InvariantCulture);
                }
                if (typeof(T) == typeof(sbyte)) {
                    return sbyte.Parse(val, NumberStyle, CultureInfo.InvariantCulture);
                }
                if (typeof(T) == typeof(short)) {
                    return short.Parse(val, NumberStyle, CultureInfo.InvariantCulture);
                }
                if (typeof(T) == typeof(ushort)) {
                    return ushort.Parse(val, NumberStyle, CultureInfo.InvariantCulture);
                }
                if (typeof(T) == typeof(int)) {
                    return int.Parse(val, NumberStyle, CultureInfo.InvariantCulture);
                }
                if (typeof(T) == typeof(uint)) {
                    return uint.Parse(val, NumberStyle, CultureInfo.InvariantCulture);
                }
                if (typeof(T) == typeof(long)) {
                    return long.Parse(val, NumberStyle, CultureInfo.InvariantCulture);
                }
                if (typeof(T) == typeof(ulong)) {
                    return ulong.Parse(val, NumberStyle, CultureInfo.InvariantCulture);
                }
                if (typeof(T) == typeof(float)) {
                    return float.Parse(val, NumberStyle, CultureInfo.InvariantCulture);
                }
                if (typeof(T) == typeof(double)) {
                    return double.Parse(val, NumberStyle, CultureInfo.InvariantCulture);
                }
            } 
            catch(err)
            { 
                //
            }
            
            return null;
            
        }

        this.SetValueString = function(/*string*/ val) 
        {
            if (val == null || val == String.Empty) 
            {
                SetValues(new T[0]);
                return;
            }

            var /*string[]*/ strs = val.split('\\');

            var /*T[]*/ vals = T[strs.length];

            for (var i = 0; i < vals.length; i++) 
            {
                vals[i] = parseInt(strs[i]);
            }

            SetValues(vals);
        }

        this.GetValueType = function() 
        {
            return typeof T;
        }

       this.GetValueObject = function() 
        {
            if (GetVM() == 0)
                return null;
            return GetValue();
        }

        this.GetValueObjectArray = function() 
        {
            var v = GetValues();
            var o = [v.Length];
            //Array.Copy(v, o, v.Length);
            o = v;
            return o;
        }

        this.SetValueObject = function(/*object*/ val) 
        {
            if ( (typeof val) != this.GetValueType()){
                alert("Invalid type for Element VR!");
            }
            
            this.SetValue(val);
        }

        this.SetValueObjectArray = function(/*object[]*/ vals) 
        {
            if (vals.length == 0){
                SetValues(T[0]);
            }

            if (vals[0].GetType() != GetValueType()){
                throw new DicomDataException("Invalid type for Element VR!");
            }

            var v = [vals.length];
            //Array.Copy(vals, v, vals.Length);
            //SetValues(v);
        }

        this.SetValue = function(value) 
        {
            var vals = [];
            vals[0] = value;
            this.SetValues(vals);
        }

        this.SetValues = function(/*T[]*/ vals) 
        {
            //Endian = Endian.LocalMachine;
            var VR = this.inherit.inherit.VR();
            var /*byte[]*/ bytes = new Uint8Array(vals.length * VR.UnitSize());            
            this.inherit.ByteBuffer().BlockCopy2(vals, 0, bytes, 0, bytes.byteLength);
            this.inherit.ByteBuffer().FromBytes(bytes);
        }

        this.SetValuesFromByte = function (/*T[]*/ vals) {
            //Endian = Endian.LocalMachine;
            //var VR = this.inherit.inherit.VR();
            //var bytes = new Uint8Array(vals.length * VR.UnitSize());
            //this.inherit.ByteBuffer().BlockCopy2(vals, 0, bytes, 0, bytes.byteLength);
            this.inherit.ByteBuffer().FromBytes(vals);
        }

        //#endregion

	}
	

	function DcmApplicationEntity(tag, pos, endian, buffer) {
	    this.inherit = new DcmMultiStringElement(tag, DicomVR.AE, pos, endian, buffer);
	    this.Type = DcmElementType.DcmApplicationEntity;
	}
	function DcmAgeString(tag, pos, endian, buffer)
	{
	    this.inherit = new DcmMultiStringElement(tag, DicomVR.AS, pos, endian, buffer);
	    this.Type = DcmElementType.DcmAgeString;
	}
	function DcmAttributeTag(tag, pos, endian, buffer)
	{
	    this.inherit = new DcmElement(tag, DicomVR.AT, pos, endian, buffer);
	    this.Type = DcmElementType.DcmAttributeTag;
	}

	function DcmCodeString(/*DicomTag*/ tag, /*long*/ pos, /*Endian*/ endian, /*ByteBuffer*/ buffer)
	{
	    this.inherit = new DcmMultiStringElement(tag, DicomVR.CS, pos, endian, buffer);
	    this.Type = DcmElementType.DcmCodeString;
	}

	function DcmDate(/*DicomTag*/ tag, /*long*/ pos, /*Endian*/ endian, /*ByteBuffer*/ buffer)
	{
	    this.inherit = new DcmDateElementBase(tag, DicomVR.DA, pos, endian, buffer);
	    this.Type = DcmElementType.DcmDate;
	}

	function DcmDecimalString(tag, pos, endian, buffer) {
	    this.inherit = new DcmMultiStringElement(tag, DicomVR.DS, pos, endian, buffer);
	    this.Type = DcmElementType.DcmDecimalString;
	}
	function DcmDateTime(tag, pos, endian, buffer) {
	    this.inherit = new DcmDateElementBase(tag, DicomVR.DT, pos, endian, buffer);
	    this.Type = DcmElementType.DcmDateTime;
	}
	function DcmFloatingPointDouble(tag, pos, endian, buffer) {
	    this.inherit = new DcmValueElement(tag, DicomVR.FD, pos, endian, buffer);
	    this.Type = DcmElementType.DcmFloatingPointDouble;
	}
	function DcmFloatingPointSingle(tag, pos, endian, buffer) {
	    this.inherit = new DcmValueElement(tag, DicomVR.FL, pos, endian, buffer);
	    this.Type = DcmElementType.DcmFloatingPointSingle;
	}
	function DcmIntegerString(tag, pos, endian, buffer) {
	    this.inherit = new DcmMultiStringElement(tag, DicomVR.IS, pos, endian, buffer);
	    this.Type = DcmElementType.DcmIntegerString;
	}
	function DcmLongString(tag, pos, endian, buffer) {
	    this.inherit = new DcmStringElement(tag, DicomVR.LO, pos, endian, buffer);
	    this.Type = DcmElementType.DcmLongString;
	}
	function DcmLongText(tag, pos, endian, buffer) {
	    this.inherit = new DcmStringElement(tag, DicomVR.LT, pos, endian, buffer);
	    this.Type = DcmElementType.DcmLongText;
	}

	function DcmOtherByte(/*DicomTag*/ tag, /*long*/ pos, /*Endian*/ endian, /*ByteBuffer*/ buffer) // : DcmValueElement<byte> 
	{
	    this.inherit = new DcmValueElement(tag, DicomVR.OB, pos, endian, buffer);
	    this.Type = DcmElementType.DcmOtherByte;
	}

	function DcmOtherFloat(tag, pos, endian, buffer) {
	    this.inherit = new DcmValueElement(tag, DicomVR.OF, pos, endian, buffer);
	    this.Type = DcmElementType.DcmOtherFloat;
	}
	function DcmOtherWord(tag, pos, endian, buffer) {
	    this.inherit = new DcmValueElement(tag, DicomVR.OW, pos, endian, buffer);
	    this.Type = DcmElementType.DcmOtherWord;
	}
	function DcmPersonName(tag, pos, endian, buffer) {
	    this.inherit = new DcmStringElement(tag, DicomVR.PN, pos, endian, buffer);
	    this.Type = DcmElementType.DcmPersonName;
	}
	function DcmShortString(tag, pos, endian, buffer) {
	    this.inherit = new DcmStringElement(tag, DicomVR.SH, pos, endian, buffer);
	    this.Type = DcmElementType.DcmShortString;
	}
	function DcmSignedLong(tag, pos, endian, buffer) {
	    this.inherit = new DcmValueElement(tag, DicomVR.SL, pos, endian, buffer);
	    this.Type = DcmElementType.DcmSignedLong;
	}
	function DcmSignedShort(tag, pos, endian, buffer) {
	    this.inherit = new DcmValueElement(tag, DicomVR.SS, pos, endian, buffer);
	    this.Type = DcmElementType.DcmSignedShort;
	}
	function DcmShortText(tag, pos, endian, buffer) {
	    this.inherit = new DcmStringElement(tag, DicomVR.ST, pos, endian, buffer);
	    this.Type = DcmElementType.DcmShortText;
	}
	function DcmTime(tag, pos, endian, buffer) {
	    this.inherit = new DcmDateElementBase(tag, DicomVR.TM, pos, endian, buffer);
	    this.Type = DcmElementType.DcmTime;
	}
	
	function DcmUniqueIdentifier(/*DicomTag*/ tag, /*long*/ pos, /*Endian*/ endian, /*ByteBuffer*/ buffer) //: DcmMultiStringElement 
	{
	    //#region Public Constructors
	    this.inherit = new DcmMultiStringElement(tag, DicomVR.UI, pos, endian, buffer);
        	    
	    this.Type = DcmElementType.DcmUniqueIdentifier;

        //#region Access Methods
	    this.GetUID = function() {
	        return DicomUID.Lookup(this.inherit.GetValue());
	    }

	    this.GetTS = function() 
	    {
	        return DicomTransferSyntax.Lookup(this.GetUID());
	    }

	    this.SetUID = function(/*DicomUID*/ ui) {
	        this.inherit.SetValue(ui.UID);
	    }

	    this.SetTS = function(/*DicomTransferSyntax*/ ts) {
	        this.inherit.SetUID(ts.UID);
	    }

	    this.SetValueObject = function(/*object*/ val) 
	    {
            //if (val is DicomUID)
            //{
            //    SetUID(val);
            //}
	        //else
	        //{
	        //    base.SetValueObject(val);
	        //}
	    }
        //#endregion
	}

	/// <summary>Unsigned Long (UL)</summary>
	function DcmUnsignedLong(tag, pos, endian, buffer)
	{

	    this.inherit = new DcmValueElement(tag, DicomVR.UL, pos, endian, buffer);

	    this.Type = DcmElementType.DcmUnsignedLong;

		////#region Public Constructors
	    //this.DcmUnsignedLong1 = function(/*DicomTag*/ tag)
	    //{
	    //    inherit.DcmValueElement1(tag, DicomVR.UL);
	    //}

	    //this.DcmUnsignedLong2 = function(/*DicomTag*/ tag, /*long*/ pos, /*Endian*/ endian)
	    //{
	    //    inherit.DcmValueElement2(tag, DicomVR.UL, pos, endian);
	    //}

	    //this.DcmUnsignedLong3 = function(/*DicomTag*/ tag, /*long*/ pos, /*Endian*/ endian, /*ByteBuffer*/ buffer)
	    //{
	        //inherit.DcmValueElement3(tag, DicomVR.UL, pos, endian, buffer);
	    //}

	}

	function DcmUnknown(tag, pos, endian, buffer) {
	    this.inherit = new DcmValueElement(tag, DicomVR.UN, pos, endian, buffer);
	    this.Type = DcmElementType.DcmUnknown;
	}

	function DcmUnsignedShort(/*DicomTag*/ tag, /*long*/ pos, /*Endian*/ endian, /*ByteBuffer*/ buffer) //: DcmValueElement<ushort> 
	{
	    this.inherit = new DcmValueElement(tag, DicomVR.US, pos, endian, buffer);
	    this.Type = DcmElementType.DcmUnsignedShort;
	}	

	function DcmUnlimitedText(tag, pos, endian, buffer) {
	    this.inherit = new DcmStringElement(tag, DicomVR.UT, pos, endian, buffer);
	    this.Type = DcmElementType.DcmUnlimitedText;
	}

	
	
	
	
