﻿var DicomWriteStatus =
{
    "Success": 0,
    "UnknownError": 1
};

function DicomStreamWriter(stream, bBinaryWriter)
{
    //#region Private Members
    var /*const uint*/ UndefinedLength = 0xFFFFFFFF;

    var /*Stream*/ _stream = null;
    var /*BinaryWriter*/ _writer = null;
    var /*DicomTransferSyntax*/ _syntax = null;
    var /*Encoding*/ _encoding = DcmEncoding.Default();
    var /*Endian*/ _endian;

    var /*ushort*/ _group = 0xffff;
	//#endregion

	//#region Public Constructors
    /// <summary>
    /// Initializes a new DicomStreamWriter with a target stream
    /// </summary>
    /// <param name="stream">Target stream</param>
    //this.DicomStreamWriter = function(/*Stream*/ stream) 
    //{
	    //_stream = stream;
        //this.TransferSyntax(DicomTransferSyntax.ExplicitVRLittleEndian);
    //}
	//#endregion

	//#region Public Properties
    /// <summary>
    /// Transfer syntax
    /// </summary>
    this.TransferSyntax = function(value)
    {
        if(value == undefined) { return _syntax; }
        else 
        {
            _syntax = value;
            _endian = true; //_syntax.Endian;

            //if (_syntax.IsDeflate)
            //    _writer = EndianBinaryWriter.Create(new DeflateStream(_stream, CompressionMode.Compress), _encoding, _endian);
            //else

            if (bBinaryWriter) {
                _writer = new BinaryWriter();
            }
            else {
                //if (_stream.Buffer() == undefined)
                    _writer = _stream; //.Buffer();
                //else
                //    _writer = _stream.Buffer();
            }
        }
    }

    //konstruktor
    _stream = stream;
    this.TransferSyntax(DicomTransferSyntax.ExplicitVRLittleEndian);

    /// <summary>
    /// String encoding
    /// </summary>
    this.Encoding = function(value)
    {
        if(value == undefined) { return _encoding; }
        else 
        {
            _encoding = value;
            TransferSyntax = _syntax;
        }
    }
    //#endregion

    /// <summary>
    /// Write dataset to stream
    /// </summary>
    /// <param name="dataset">Dataset</param>
    /// <param name="options">DICOM write options</param>
    /// <returns>Status code</returns>
    this.Write = function(/*DcmDataset*/ dataset, /*DicomWriteOptions*/ options, okidac) 
    {
        var elems;
        var gl;
        if (okidac != undefined) {
            this.TransferSyntax(dataset.InternalTransferSyntax());
            elems = dataset.Elements();
            gl = dataset;
        }
        else {
            this.TransferSyntax(dataset.inherit.InternalTransferSyntax());
            elems = dataset.inherit.Elements();
            gl = dataset.inherit;
        }

        //dataset.SelectByteOrder(_syntax.Endian);

        //foreach (DcmItem item in dataset.Elements) 
        //var elems = dataset.inherit.Elements();
        for(itm in elems)
        {
            var num = parseInt(itm);
            if (isNaN(itm)) { continue; }

            var item = elems[num];
            if (item.inherit.Type == DcmElementType.DcmDateElementBase)
                item = item.inherit;
            
            var tag = item.inherit.inherit.inherit.Tag();                            

            if (tag.Element() == 0x0000)
            {
                continue;
            }           

            if (Flags.IsSet(options, DicomWriteOptions.CalculateGroupLengths) 
                && tag.Group() != _group && tag.Group() <= 0x7fe0)
            {
                _group = tag.Group();
                _writer.Write("ushort",_group);
                _writer.Write("ushort",0x0000);
                if (_syntax.IsExplicitVR) 
                {
                    _writer.Write("byte",'U'.charCodeAt(0));
                    _writer.Write("byte",'L'.charCodeAt(0));
                    _writer.Write("ushort",4);
                } else {
                    _writer.Write("uint",4);
                }
                _writer.Write("uint",gl.CalculateGroupWriteLength(_group, _syntax, options));
            }

            _writer.Write("ushort",tag.Group());
            _writer.Write("ushort",tag.Element());

            if (_syntax.IsExplicitVR) 
            {
                var vrStr = item.inherit.inherit.inherit.VR().VR();

                //console.log(vrStr);
                _writer.Write("byte", vrStr.charCodeAt(0));
                _writer.Write("byte", vrStr.charCodeAt(1));
            }

            if (item.Type == "DcmItemSequence") 
            {
                var /*DcmItemSequence*/ sq = item; // as DcmItemSequence;

                if (_syntax.IsExplicitVR)
                    _writer.Write("ushort",0x0000);

                if (Flags.IsSet(options, DicomWriteOptions.ExplicitLengthSequence) || (tag.IsPrivate() && !_syntax.IsExplicitVR)) {
                    var hl = _syntax.IsExplicitVR ? 12 : 8;
                    _writer.Write("uint",sq.CalculateWriteLength(_syntax, options & ~DicomWriteOptions.CalculateGroupLengths) - /*(uint)*/hl);
                } else {
                    _writer.Write("uint",UndefinedLength);
                }

                //foreach (DcmItemSequenceItem ids in sq.SequenceItems) 
                for(var k=0; k < sq.SequenceItems.length; k++)
                {
                    var ids = sq.SequenceItems[k];

                    ids.Dataset.ChangeTransferSyntax(dataset.InternalTransferSyntax, null);

                    _writer.Write("ushort",DicomTags.Item.Group);
                    _writer.Write("ushort",DicomTags.Item.Element);

                    if (Flags.IsSet(options, DicomWriteOptions.ExplicitLengthSequenceItem)) 
                    {
                        _writer.Write("uint",ids.CalculateWriteLength(_syntax, options & ~DicomWriteOptions.CalculateGroupLengths) - /*(uint)*/8);
                    } else {
                        _writer.Write("uint",UndefinedLength);
                    }

                    Write(ids.Dataset, options & ~DicomWriteOptions.CalculateGroupLengths);

                    if (!Flags.IsSet(options, DicomWriteOptions.ExplicitLengthSequenceItem)) 
                    {
                        _writer.Write("ushort",DicomTags.ItemDelimitationItem.Group);
                        _writer.Write("ushort",DicomTags.ItemDelimitationItem.Element);
                        _writer.Write("uint",0x00000000);
                    }
                }

                if (!Flags.IsSet(options, DicomWriteOptions.ExplicitLengthSequence) && !(tag.IsPrivate && !_syntax.IsExplicitVR)) {
                    _writer.Write("ushort",DicomTags.SequenceDelimitationItem.Group);
                    _writer.Write("ushort",DicomTags.SequenceDelimitationItem.Element);
                    _writer.Write("uint",0x00000000);
                }
            }				
            else if (item.Type == "DcmFragmentSequence") 
            {
                var /*DcmFragmentSequence*/ fs = item; // as DcmFragmentSequence;

                if (_syntax.IsExplicitVR){
                    _writer.Write("ushort",0x0000);
                }

                _writer.Write("uint",UndefinedLength);

                _writer.Write("ushort",DicomTags.Item.Group);
                _writer.Write("ushort",DicomTags.Item.Element);

                if (Flags.IsSet(options, DicomWriteOptions.WriteFragmentOffsetTable) && fs.HasOffsetTable) {
                    _writer.Write("uint",fs.OffsetTableBuffer.Length);
                    fs.OffsetTableBuffer.CopyTo(_writer.BaseStream);
                } else 
                {
                    _writer.Write("uint",0x00000000);
                }

                //foreach (ByteBuffer bb in fs.Fragments) 
                for(var j=0; j < fs.Fragments.length; j++)
                {
                    bb = fs.Fragments[j];

                    _writer.Write("ushort",DicomTags.Item.Group);
                    _writer.Write("ushort",DicomTags.Item.Element);
                    _writer.Write("uint",bb.Length);
                    bb.CopyTo(_writer.BaseStream);
                }

                _writer.Write("ushort",DicomTags.SequenceDelimitationItem.Group);
                _writer.Write("ushort",DicomTags.SequenceDelimitationItem.Element);
                _writer.Write("uint",0x00000000);
            }				
            else 
            {
                var /*DcmElement*/ de = item; // as DcmElement;

                var vrStr;
                if (item.inherit.Type == DcmElementType.DcmDateElementBase)
                    vrStr = item.inherit.inherit.inherit.inherit.VR().VR();
                else
                    vrStr = item.inherit.inherit.inherit.VR().VR();

                if (_syntax.IsExplicitVR)
                {
                    if (de.inherit.inherit.inherit.VR().Is16BitLengthField())
                    {
                        _writer.Write("ushort", de.inherit.inherit.Length());
                    }
                    else
                    {
                        _writer.Write("ushort",0x0000);
                        _writer.Write("uint", de.inherit.inherit.Length());
                    }
                }
                else
                {
                    _writer.Write("uint",de.inherit.inherit.Length());
                }

                de.inherit.inherit.ByteBuffer().CopyTo(_writer,true);
            }
        }

        return DicomWriteStatus.Success;
    }

    this.Buffer = function ()
    {
        return _writer.Buffer();
    }
}