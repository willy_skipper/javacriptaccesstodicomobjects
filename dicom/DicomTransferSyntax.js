﻿function DicomTransferSyntax(/*DicomUID*/ uid, /*bool*/ be, /*bool*/ evr, /*bool*/ encaps, /*bool*/ lssy, /*bool*/ dflt)
{
	/*DicomUID */ this.UID;
	this.IsBigEndian;
	this.IsExplicitVR;
	this.IsEncapsulated;
	this.IsLossy;
	this.IsDeflate;
	/*Endian*/ this.Endian;

//	this.DicomTransferSyntax = function(/*DicomUID*/ uid, /*bool*/ be, /*bool*/ evr, /*bool*/ encaps, /*bool*/ lssy, /*bool*/ dflt) 
//	{
		this.UID = uid;
		this.IsBigEndian = be;
		this.IsExplicitVR = evr;
		this.IsEncapsulated = encaps;
		this.IsLossy = lssy;
		this.IsDeflate = dflt;
		//Endian = IsBigEndian ? Endian.Big : Endian.Little;
//	}

//	this.ToString = function() 
//	{
//		return UID.Description;
//	}

}

    DicomTransferSyntax.Lookup = function(/*String */uid) 
    {
		return Lookup(DicomUID.Lookup(uid));
	}
	
	DicomTransferSyntax.Lookup = function(/*DicomUID*/ uid) 
	{
	    var cnt = EntriesTS.length;
		for(var i=0; i < cnt; i++) 
		{
			if (EntriesTS[i].UID.UID == uid)
			{
				return EntriesTS[i];
		    }
		}
		return new DicomTransferSyntax(uid, false, true, true, false, false);
	}


    //#region Dicom Transfer Syntax
    /// <summary>Implicit VR Little Endian</summary>
	DicomTransferSyntax.ImplicitVRLittleEndian = new DicomTransferSyntax(DicomUID.ImplicitVRLittleEndian, false, false, false, false, false);

	/// <summary>Explicit VR Little Endian</summary>
	DicomTransferSyntax.ExplicitVRLittleEndian = new DicomTransferSyntax(DicomUID.ExplicitVRLittleEndian, false, true, false, false, false);

	/// <summary>Explicit VR Big Endian</summary>
	DicomTransferSyntax.ExplicitVRBigEndian = new DicomTransferSyntax(DicomUID.ExplicitVRBigEndian, true, true, false, false, false);

	/// <summary>Deflated Explicit VR Little Endian</summary>
	DicomTransferSyntax.DeflatedExplicitVRLittleEndian = new DicomTransferSyntax(DicomUID.DeflatedExplicitVRLittleEndian, false, true, false, false, true);

	/// <summary>JPEG Baseline (Process 1)</summary>
	DicomTransferSyntax.JPEGProcess1 = new DicomTransferSyntax(DicomUID.JPEGBaselineProcess1, false, true, true, true, false);

	/// <summary>JPEG Extended (Process 2 &amp; 4)</summary>
	DicomTransferSyntax.JPEGProcess2_4 = new DicomTransferSyntax(DicomUID.JPEGExtendedProcess2_4, false, true, true, true, false);

	/// <summary>JPEG Extended (Process 3 &amp; 5) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess3_5Retired = new DicomTransferSyntax(DicomUID.JPEGExtendedProcess3_5RETIRED, false, true, true, true, false);

	/// <summary>JPEG Spectral Selection, Non-Hierarchical (Process 6 &amp; 8) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess6_8Retired = new DicomTransferSyntax(DicomUID.JPEGSpectralSelectionNonHierarchicalProcess6_8RETIRED, false, true, true, true, false);

	/// <summary>JPEG Spectral Selection, Non-Hierarchical (Process 7 &amp; 9) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess7_9Retired = new DicomTransferSyntax(DicomUID.JPEGSpectralSelectionNonHierarchicalProcess7_9RETIRED, false, true, true, true, false);

	/// <summary>JPEG Full Progression, Non-Hierarchical (Process 10 &amp; 12) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess10_12Retired = new DicomTransferSyntax(DicomUID.JPEGFullProgressionNonHierarchicalProcess10_12RETIRED, false, true, true, true, false);

	/// <summary>JPEG Full Progression, Non-Hierarchical (Process 11 &amp; 13) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess11_13Retired = new DicomTransferSyntax(DicomUID.JPEGFullProgressionNonHierarchicalProcess11_13RETIRED, false, true, true, true, false);

	/// <summary>JPEG Lossless, Non-Hierarchical (Process 14)</summary>
	DicomTransferSyntax.JPEGProcess14 = new DicomTransferSyntax(DicomUID.JPEGLosslessNonHierarchicalProcess14, false, true, true, false, false);

	/// <summary>JPEG Lossless, Non-Hierarchical (Process 15) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess15Retired = new DicomTransferSyntax(DicomUID.JPEGLosslessNonHierarchicalProcess15RETIRED, false, true, true, false, false);

	/// <summary>JPEG Extended, Hierarchical (Process 16 &amp; 18) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess16_18Retired = new DicomTransferSyntax(DicomUID.JPEGExtendedHierarchicalProcess16_18RETIRED, false, true, true, true, false);

	/// <summary>JPEG Extended, Hierarchical (Process 17 &amp; 19) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess17_19Retired = new DicomTransferSyntax(DicomUID.JPEGExtendedHierarchicalProcess17_19RETIRED, false, true, true, true, false);

	/// <summary>JPEG Spectral Selection, Hierarchical (Process 20 &amp; 22) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess20_22Retired = new DicomTransferSyntax(DicomUID.JPEGSpectralSelectionHierarchicalProcess20_22RETIRED, false, true, true, true, false);

	/// <summary>JPEG Spectral Selection, Hierarchical (Process 21 &amp; 23) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess21_23Retired = new DicomTransferSyntax(DicomUID.JPEGSpectralSelectionHierarchicalProcess21_23RETIRED, false, true, true, true, false);

	/// <summary>JPEG Full Progression, Hierarchical (Process 24 &amp; 26) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess24_26Retired = new DicomTransferSyntax(DicomUID.JPEGFullProgressionHierarchicalProcess24_26RETIRED, false, true, true, true, false);

	/// <summary>JPEG Full Progression, Hierarchical (Process 25 &amp; 27) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess25_27Retired = new DicomTransferSyntax(DicomUID.JPEGFullProgressionHierarchicalProcess25_27RETIRED, false, true, true, true, false);

	/// <summary>JPEG Lossless, Hierarchical (Process 28) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess28Retired = new DicomTransferSyntax(DicomUID.JPEGLosslessHierarchicalProcess28RETIRED, false, true, true, false, false);

	/// <summary>JPEG Lossless, Hierarchical (Process 29) (Retired)</summary>
	DicomTransferSyntax.JPEGProcess29Retired = new DicomTransferSyntax(DicomUID.JPEGLosslessHierarchicalProcess29RETIRED, false, true, true, false, false);

	/// <summary>JPEG Lossless, Non-Hierarchical, First-Order Prediction (Process 14 [Selection Value 1])</summary>
	DicomTransferSyntax.JPEGProcess14SV1 = new DicomTransferSyntax(DicomUID.JPEGLosslessProcess14SV1, false, true, true, false, false);

	/// <summary>JPEG-LS Lossless Image Compression</summary>
	DicomTransferSyntax.JPEGLSLossless = new DicomTransferSyntax(DicomUID.JPEGLSLosslessImageCompression, false, true, true, false, false);

	/// <summary>JPEG-LS Lossy (Near-Lossless) Image Compression</summary>
	DicomTransferSyntax.JPEGLSNearLossless = new DicomTransferSyntax(DicomUID.JPEGLSLossyNearLosslessImageCompression, false, true, true, true, false);

	/// <summary>JPEG 2000 Lossless Image Compression</summary>
	DicomTransferSyntax.JPEG2000Lossless = new DicomTransferSyntax(DicomUID.JPEG2000ImageCompressionLosslessOnly, false, true, true, false, false);

	/// <summary>JPEG 2000 Lossy Image Compression</summary>
	DicomTransferSyntax.JPEG2000Lossy = new DicomTransferSyntax(DicomUID.JPEG2000ImageCompression, false, true, true, true, false);

	/// <summary>MPEG2 Main Profile @ Main Level</summary>
	DicomTransferSyntax.MPEG2 = new DicomTransferSyntax(DicomUID.MPEG2MainProfileMainLevel, false, true, true, true, false);

	/// <summary>RLE Lossless</summary>
	DicomTransferSyntax.RLELossless = new DicomTransferSyntax(DicomUID.RLELossless, false, true, true, false, false);
//#endregion

	DicomTransferSyntax.IsImageCompression = function (/*DicomTransferSyntax*/ tx)
	{
	    return tx.UID != DicomTransferSyntax.ImplicitVRLittleEndian.UID &&
               tx.UID != DicomTransferSyntax.ExplicitVRLittleEndian.UID &&
               tx.UID != DicomTransferSyntax.ExplicitVRBigEndian.UID &&
               tx.UID != DicomTransferSyntax.DeflatedExplicitVRLittleEndian.UID;
	}

function DicomDictonaryTS()
{
    var niz= new Array();
    //niz[DicomUID.VerificationSOPClass.UID]=DicomUID.VerificationSOPClass;
    
    niz.push(DicomTransferSyntax.ImplicitVRLittleEndian);
	niz.push(DicomTransferSyntax.ExplicitVRLittleEndian);
	niz.push(DicomTransferSyntax.ExplicitVRBigEndian);
	niz.push(DicomTransferSyntax.DeflatedExplicitVRLittleEndian);
	niz.push(DicomTransferSyntax.JPEGProcess1);
	niz.push(DicomTransferSyntax.JPEGProcess2_4);
	niz.push(DicomTransferSyntax.JPEGProcess3_5Retired);
	niz.push(DicomTransferSyntax.JPEGProcess6_8Retired);
	niz.push(DicomTransferSyntax.JPEGProcess7_9Retired);
	niz.push(DicomTransferSyntax.JPEGProcess10_12Retired);
	niz.push(DicomTransferSyntax.JPEGProcess11_13Retired);
	niz.push(DicomTransferSyntax.JPEGProcess14);
	niz.push(DicomTransferSyntax.JPEGProcess15Retired);
	niz.push(DicomTransferSyntax.JPEGProcess16_18Retired);
	niz.push(DicomTransferSyntax.JPEGProcess17_19Retired);
	niz.push(DicomTransferSyntax.JPEGProcess20_22Retired);
	niz.push(DicomTransferSyntax.JPEGProcess21_23Retired);
	niz.push(DicomTransferSyntax.JPEGProcess24_26Retired);
	niz.push(DicomTransferSyntax.JPEGProcess25_27Retired);
	niz.push(DicomTransferSyntax.JPEGProcess28Retired);
	niz.push(DicomTransferSyntax.JPEGProcess29Retired);
	niz.push(DicomTransferSyntax.JPEGProcess14SV1);
	niz.push(DicomTransferSyntax.JPEGLSLossless);
	niz.push(DicomTransferSyntax.JPEGLSNearLossless);
	niz.push(DicomTransferSyntax.JPEG2000Lossless);
	niz.push(DicomTransferSyntax.JPEG2000Lossy);
	niz.push(DicomTransferSyntax.MPEG2);
	niz.push(DicomTransferSyntax.RLELossless);    

//    this.TryGetValue = function(uid,o)
//    {
//        if(uid in niz){
//            return niz[uid]; 
//        }
//        else{
//            return null;
//        }
//    }

    return niz;

}

var EntriesTS = DicomDictonaryTS();