function DcmItem(/* DicomTag */ tag, /* DicomVR */ vr, /* long */ pos, /* Endian */ endian)
{
	// #region Private Members
	var /* DicomTag */ _tag;
	var /* DicomVR */ _vr;
	var /* long */ _streamPosition;
	var /* Endian */ _endian;
    // #endregion 
  

	// #region Public Constructors
	// public DcmItem(DicomTag tag, DicomVR vr) {
		_tag = tag;
		_vr = vr;
		//_streamPosition = 0;
		//_endian = Endian.LocalMachine;
	// }

	// public DcmItem(DicomTag tag, DicomVR vr, long pos, Endian endian) {
		//_tag = tag;
		//_vr = vr;
		
		if(pos == undefined)
		{
			_streamPosition = 0;
		}
		else{		
			_streamPosition = pos;
		}
		
		if(endian == undefined){
			_endian = Endian.LocalMachine; //todo
		}
		else{
			_endian = endian;
		}
		
	// }
	// #endregion

	// #region Public Properties
	this.Tag = function()
	{
		return _tag; 
	}

	this.Name  = function()
	{
	    return _tag.Entry().Name;
	}

	this.VR = function()
	{
		return _vr;
	}

	this.StreamPosition = function(value)
	{
		if(value != undefined) { return _streamPosition; }
		else { _streamPosition = value; }
	}

	this.Endian = function(value){
		if(value != undefined) { return _endian; }
		else {
			if (_endian != value) {
				_endian = value;
				//ChangeEndianInternal(); //todo
			}
		}
	}
	// #endregion

	// #region Public Methods
	this.ToString = function() {
		return _tag.Entry().ToString();
	}
	// #endregion

	// #region Methods to Override
	// internal abstract uint CalculateWriteLength(DicomTransferSyntax syntax, DicomWriteOptions options);

	// protected abstract void ChangeEndianInternal();

	// internal abstract void Preload();

	// internal abstract void Unload();

	// public abstract DcmItem Clone();

	// public virtual void Dump(StringBuilder sb, String prefix,  DicomDumpOptions options) {
		// sb.Append(prefix).AppendLine(_tag.Entry.ToString());
	// }
	// #endregion
}