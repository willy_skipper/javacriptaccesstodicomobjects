﻿var DcmRoleSelection =
{
	"Disabled":0,
	"SCU":1,
	"SCP":2,
	"Both":3,
	"None":4
};

//public enum DcmPresContextResult : byte {
var DcmPresContextResult =
{
	"Proposed":255,
	"Accept":0,
	"RejectUser":1,
	"RejectNoReason":2,
	"RejectAbstractSyntaxNotSupported":3,
	"RejectTransferSyntaxesNotSupported":4
};

function DcmPresContext(pcid, abstractSyntax)
{
    /*byte*/ var _pcid;
    var _result = DcmPresContextResult;
    /*DicomUID*/ var _abstract;
    /*List<DicomTransferSyntax>*/ var _transfers = [];
    
    //this.DcmPresContext = function(/*byte*/ pcid, /*DicomUID*/ abstractSyntax) {
		_pcid = pcid;
		_result = DcmPresContextResult.Proposed;
		_abstract = abstractSyntax;
		//_transfers = new List<DicomTransferSyntax>();
	//}
	
	this.ID = function(){
		return _pcid;
	}
	
	function GetTSByNum()
	{	    
	    for(key in _transfers)
		{
		    return _transfers[key];		    
		}
	}
	
	this.SetResult = function(/*DcmPresContextResult*/ result) {
		this.SetResult1(result, /*_transfers[0]*/GetTSByNum());
	}
	
	this.SetResult1 = function(/*DcmPresContextResult*/ result, /*DicomTransferSyntax*/ acceptedTs) {
		_transfers = [];
		_transfers[acceptedTs.UID.UID] = acceptedTs;
		_result = result;
	}
	
	this.ClearTransfers = function() {
        _transfers = [];
	}	
	
	this.Result = function(){
		return _result;
	}
	
	this.AddTransfer = function(/*DicomTransferSyntax*/ ts) {			
		if(ts.UID.UID in _transfers){
            //super
        }
        else{
            _transfers[ts.UID.UID] = ts;
            //_transfers[_transfers.length] = ts;
        }
	}
	
	this.AcceptedTransferSyntax = function()
	{		
		//if (_transfers.length > 0){
			//return _transfers[0];
		//}
		//return null;		
		
		for(key in _transfers)
		{
		    return _transfers[key];
		}
		
		return null;
		
	}
    
    this.HasTransfer = function(/*DicomTransferSyntax*/ ts) 
    {
		return _transfers[ts.UID.UID];
	}	
	
	this.AbstractSyntax = function()
	{
		return _abstract;
	}

	this.GetTransfers = function ()
	{
	    return _transfers;
	}
	
}

function Implementation()
{
}
Implementation.ClassUID = new DicomUID("1.3.6.1.4.1.30071.6", "Implementation Class UID", DicomUidType.Unknown);
Implementation.Version = "drle 0.9";



function DcmAssociate()
{
    var _calledAe;
    var _callingAe;
    var _maxPdu;

    this.AsyncOpsInvoked;
    this.AsyncOpsPerformed;
    this.ImplementationClass;
    this.ImplementationVersion;
    
    var _appCtxNm;
    var _negotiateAsync;
    var _presContexts = [];  
    
    this.NegotiateAsyncOps = function(){
		return _negotiateAsync;		
    }

    this.MaximumPduLength = function(value)
    {
        if(value == undefined) { return _maxPdu; }
        else { _maxPdu = value; }
    }

    /// <summary>
    /// Gets or sets the Called AE title.
    /// </summary>
    this.CalledAE = function(value)
    {
        if(value == undefined) { return _calledAe; }
        else { _calledAe = value; }
    }

    /// <summary>
    /// Gets or sets the Calling AE title.
    /// </summary>
    this.CallingAE = function(value)
    {
        if(value == undefined) { return _callingAe; }
        else { _callingAe = value; }
    }

    
    //#region Public Constructor
	//public DcmAssociate() {
		this.MaximumPduLength(PDataTFStream.MaxPduSizeLimit);
		_appCtxNm = DicomUID.DICOMApplicationContextName;
		this.ImplementationClass = Implementation.ClassUID;
		this.ImplementationVersion = Implementation.Version;
		//_presContexts = new SortedList<byte, DcmPresContext>();
		_negotiateAsync = false;
		this.AsyncOpsInvoked = 1;
		this.AsyncOpsPerformed = 1;
	//}
	//#endregion  
    
    /// <summary>
	/// Adds a Presentation Context to the DICOM Associate.
	/// </summary>
	this.AddPresentationContext = function(/*byte*/ pcid, /*DicomUID*/ abstractSyntax) {
		_presContexts[pcid] = new DcmPresContext(pcid, abstractSyntax);	
		//_presContexts = _presContexts.filter(function(e){return e});	
	}

	this.AddPresentationContext1 = function(abstractSyntax) 
	{
		var pcid = 1;
		//for(var id in _presContexts.Keys) 
		//{
        //    if(isNaN(id))

        //    if (id >= pcid)
	    //    pcid = (byte)(id + 2);
        //}
        
	    this.AddPresentationContext(pcid, abstractSyntax);
        
        return pcid;
    }
    
    /// <summary>
	/// Adds a Transfer Syntax to the specified Presentation Context.
	/// </summary>
	/// <param name="pcid">Presentation Context ID</param>
	/// <param name="ts">Transfer Syntax</param>
	this.AddTransferSyntax = function(/*byte*/ pcid, /*DicomTransferSyntax*/ ts) {
		this.GetPresentationContext(pcid).AddTransfer(ts);
		//console.log(_presContexts);
		//this.JO = _presContexts;
	}
	
	/// <summary>
	/// Sets the result of the specified Presentation Context.
	/// </summary>
	/// <param name="pcid">Presentation Context ID</param>
	/// <param name="result">Result</param>
	this.SetPresentationContextResult = function(/*byte*/ pcid, /*DcmPresContextResult*/ result) {
		this.GetPresentationContext(pcid).SetResult(result);
	}

	this.GetPresentationContextResult = function (pcid)
	{
		return this.GetPresentationContext(pcid).Result();
    }
	
	this.GetPresentationContexts = function() {
		return _presContexts;
	}
    
	this.GetAbstractSyntax = function (/*byte*/ pcid)
	{
        return this.GetPresentationContext(pcid).AbstractSyntax();
	}

	this.GetAcceptedTransferSyntax = function (/*byte*/ pcid)
	{
	    return this.GetPresentationContext(pcid).AcceptedTransferSyntax();
    }
	

	this.GetPresentationContext = function(/*byte*/ pcid) 
	{
		/*DcmPresContext*/ ctx = null;
//		if (!_presContexts.TryGetValue(pcid, /*out*/ ctx)){
//			//throw new DicomNetworkException("Invalid Presentaion Context ID");
//		}

        if(pcid in _presContexts){
            return _presContexts[pcid];
        }
        else{
            //throw "Invalid Presentaion Context ID";
        }
	}
	
    this.SetAcceptedTransferSyntax = function(/*byte*/ pcid, /*DicomTransferSyntax*/ ts) 
    {
	    this.GetPresentationContext(pcid).ClearTransfers();
	    this.GetPresentationContext(pcid).AddTransfer(ts);
    }

    this.FindAbstractSyntax = function(/*DicomUID*/ abstractSyntax) 
    {
        for(var ctx in _presContexts) 
        {
            if (isNaN(ctx))
                continue;
            var ctx = _presContexts[ctx];
            if (ctx.AbstractSyntax().UID == abstractSyntax.UID && ctx.Result == DcmPresContextResult.Accept)
                return ctx.ID();
        }
        for(var ctx in _presContexts) 
        {
            if (isNaN(ctx))
                continue;
            var ctx = _presContexts[ctx];
            if (ctx.AbstractSyntax().UID == abstractSyntax.UID)
                return ctx.ID();
        }

        return 0;
    }

}