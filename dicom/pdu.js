function RawPDU()
{
	//#region Private members
	/*byte*/ var _type;
	/*Stream*/ var _is;
	/*EndianBinaryWriter*/ var _bw;
    /*EndianBinaryReader*/ var _br;

    var _ms;
	
	//ivan, binary writer pokusaj
	var _buffer;
	var _view;
	var _cnt = 0;
	
	var br;
	
    this.RawPDUFromByte = function(/*byte*/ type) 
    {
	    _type = type;
        _buffer = new ArrayBuffer(300); //HARD CODIRANO ! paziti
	    _view = new Uint8Array(_buffer);
    }

	this.RawPDUFromBuffer = function(/*byte[]*/ buffer) {
		_is = buffer;		
		br = new jDataView(buffer); //EndianBinaryReader.Create(_is, _encoding, Endian.Big);
		_type = br.getInt8();
		_br = new jDataView(buffer, 0, buffer.byteLength, false);
		//console.log("RawPDU byteLength = " + buffer.byteLength);

		//var mate = new DataView(buffer);
		//console.log("type=" + mate.getUint8(0, true));
		//console.log("type BE=" + mate.getUint8(0, false));
		//console.log("type 2 LE=" + mate.getInt8(0, true));
		//console.log("type 2 BE=" + mate.getInt8(0, false));

		
	    ////?
		//_buffer = new ArrayBuffer(300); //HARD CODIRANO ! paziti
		//_view = new Uint8Array(_buffer);
	}

	this.ReadPDU = function() 
	{
	    //_ms = new MemoryStream();
	    //BinaryReader br = EndianBinaryReader.Create(_is, _encoding, Endian.Big);

	    //br.ReadByte();
	    //uint len = br.ReadUInt32();	// PDU-Length

	    //byte[] data = br.ReadBytes((int)len);
	    //_ms = new MemoryStream(data, false);
	    //_br = EndianBinaryReader.Create(_ms, _encoding, Endian.Big);
	}

	this.WritePDU2 = function(/*Stream*/ s, buf) 
	{
	    //var vju = new Uint8Array(buf.buffer);

        //glista
	    var buffer = new Uint8Array(6);
	    buffer[0] = _type;
	    var /*uint*/ length = buf.byteLength+6;
	    buffer[2] = (length & 0xff000000) >> 24;
	    buffer[3] = (length & 0x00ff0000) >> 16;
	    buffer[4] = (length & 0x0000ff00) >> 8;
	    buffer[5] = (length & 0x000000ff);

        //lokalni bufacccho
	    var bufija = new ArrayBuffer(_cnt);
	    var view = new Uint8Array(bufija);
	    var len = bufija.byteLength;
	    for (var i = 0; i < len; i++) {
	        view[i] = _view[i];
	    }

	    ////kopy kat
	    //var a = "";
	    //for (var kk = 0; kk < view.byteLength; kk++) {
	    //    a = a + "0x" + view[kk].toString(16) + ",";
	    //}
	    //console.log(a);
	    

	    //spajanje 2 jarana view2 = buffer + view
	    len = _cnt + 6;
	    var view2 = new Uint8Array(len);
	    for (var i = 0; i < 6; i++) {
	        view2[i] = buffer[i];
	    }
	    for (var i = 6; i < len; i++) {
	        view2[i] = view[i-6];
	    }



	    //THE GREAT CONCLUSION!!!!
	    //console.log("Great consolusion");
	    // s.send(view2.buffer);
	    return view2.buffer;
    }
	
	this.WritePDU = function(network) {
		
        //****
	    var buffer = new Uint8Array(6);
	    buffer[0] = _type;
	    var /*uint*/ length = 169;
	    buffer[2] = (length & 0xff000000) >> 24;
	    buffer[3] = (length & 0x00ff0000) >> 16;
	    buffer[4] = (length & 0x0000ff00) >> 8;
	    buffer[5] = (length & 0x000000ff);
        //*****

		var buf = new ArrayBuffer(_cnt);
		var view = new Uint8Array(buf);
		
		for(var i=0; i < _cnt; i++)
		{
		    view[i]= _view[i];
		}

		if (network == undefined) {
		    return buf;
		}
		else {
		    network.send(buf);
		}
		
	}

	/// <summary>PDU type</summary>
	/*byte*/ this.Type = function()
	{
		return _type;
	}

	/// <summary>PDU length</summary>
	this.Length = function()
	{
		return _is.byteLength;
	}
	
	//ivan
	this.Is = function()
	{
		return _is;
	}


    /// <summary>
	/// Read byte from PDU
	/// </summary>
	/// <param name="name">Name of field</param>
	/// <returns>Field value</returns>
	this.ReadByte = function(/*String*/ name) {
		//CheckOffset(1, name);		
		return _br.getUint8();
	}
	this.ReadByteAlt = function(/*String*/ name) {
		//CheckOffset(1, name);		
		return br.getUint8();
	}
	
	this.ReadBytes = function(/*String*/ name, /*int*/ count) 
	{
		//CheckOffset(count, name);
		var byts = [count];
		for(var k=0; k < count; k++)
		{		
		    byts[k] = _br.getUint8();
		}
		
		return byts;
	}
	
	this.ReadBytesAlt = function(/*String*/ name, /*int*/ count) 
	{
		//CheckOffset(count, name);
		var byts = [count];
		for(var k=0; k < count; k++)
		{		
		    byts[k] = br.getUint8();
		}
		
		return byts;
	}

    //super fast verzija od ReadBytes - native code!
	this.ReadBytesAlt2 = function (/*String*/ name, /*int*/ count) {
	    
	    var byts = _br.buffer.slice(_br.tell(), count + _br.tell());
	    _br.seek(_br.tell()+count);

	    return byts;
	}
	
	
	/// <summary>
	/// Read ushort from PDU
	/// </summary>
	/// <param name="name">Name of field</param>
	/// <returns>Field value</returns>
	this.ReadUInt16 = function(/*String*/ name) {
		//CheckOffset(2, name);
		return _br.getUint16();
	}
	
	this.ReadUInt32 = function(/*String*/ name) {
		//CheckOffset(2, name);
		
		//var view8 = new Uint8Array(_is);
		
		return _br.getUint32(_br.tell(),false);
	}
	
	/// <summary>
	/// Skips ahead in PDU
	/// </summary>
	/// <param name="name">Name of field</param>
	/// <param name="count">Number of bytes to skip</param>
	this.SkipBytes = function(/*String*/ name, /*int*/ count) 
	{
		//CheckOffset(count, name);
		//_ms.Seek(count, SeekOrigin.Current);
		var cnt = _br.tell() + count;
		_br.seek(cnt);
	}
	
	/// <summary>
	/// Reads string from PDU
	/// </summary>
	/// <param name="name">Name of field</param>
	/// <param name="count">Length of string</param>
	/// <returns>Field value</returns>
	this.ReadString = function(/*String*/ name, /*int*/ count) {
		//CheckOffset(count, name);
		//char[] c = _br.ReadChars(count);
		return _br.getString(count);
	}
	
	this.Tell = function()
	{
	    return _br.tell();
	}
	this.WriteByteArray = function (value)
	{
	    var cnt = _cnt + value.byteLength;
	    var k = 0;
	    for (var i = _cnt; i < cnt; ++i) {
	        _view[i] = value[k];
	        //console.log("At position " + i + " written byte " + _view[i]);
	        //_cnt++;
	        k++;
	    }
	    _cnt = _cnt + value.byteLength;
	}
		
	this.Write = function(/*String name, byte value*/) 
	{
		var cnt = _cnt+arguments.length; 
		var k = 0;
		for (var i = _cnt; i < cnt; ++i) {
			_view[i] = arguments[k];			
			//console.log("At position " + i + " written byte " + _view[i]);
			//_cnt++;
			k++;
		}
		_cnt = _cnt + arguments.length;
	}
	
	this.WriteString = function(value)
	{                              
        var cnt = _cnt+value.length;
        var k = 0;
        for(var i=_cnt; i < cnt; i++)
        {
            _view[i] = value.charCodeAt(k);
            //console.log("At position " + i + " written byte " + _view[i]);
            k++;
            _cnt++;
        }
	}
	
	//globalni marker za pdu
	var _mark16_1=[];
	var _mark16_2=[];
	var _mark32_1=[];
	var _mark32_2=[];
	
	this.MarkLength16 = function()
	{	    
	    _mark16_1.push(_cnt); //pamtim gdje cu pisati ItemLenght podatak
	    _cnt += 2; //uvecavam za Int16 cnt
	    _mark16_2.push(_cnt); //pamtim odakle pocinjem brojiti bajtove
	}
	
	this.MarkLength32 = function()
	{	    
	    _mark32_1.push(_cnt); //pamtim gdje cu pisati ItemLenght podatak
	    _cnt += 4; //uvecavam za Int32 cnt
	    _mark32_2.push(_cnt); //pamtim odakle pocinjem brojiti bajtove
	}
	
	this.WriteLength16 = function()
	{
	    var ItemLength = _cnt - _mark16_2.pop(); //Length = trenutna pozicija countera - marker
	    var buff = new ArrayBuffer(2);
		var view16 = new Uint16Array(buff);
		view16[0] = ItemLength;
		var view8 = new Uint8Array(buff);
		//obrcem, zbog endiana [0] i [1] u [1] [0]
		var mark = _mark16_1.pop();
		var m = mark;
	    _view[mark] = view8[1];	 
	    ////console.log("_view[mark]:" + _view[mark] + ", value=" + view8[1]);   
	    //console.log("At position " + mark + " written Length byte " + _view[mark]);
	    _view[++mark] = view8[0];
	    ////console.log("_view[mark]:" + _view[mark] + ", value=" + view8[0]);   
	    //console.log("At position " + mark + " written Length byte " + _view[mark]);
	}
	
	this.WriteLength32 = function()
	{
	    var ItemLength = _cnt - _mark32_2.pop(); //Length = trenutna pozicija countera - marker
	    var buff = new ArrayBuffer(4);
		var view32 = new Uint32Array(buff);
		view32[0] = ItemLength;
		var view8 = new Uint8Array(buff);
		//obrcem, zbog endiana [0] i [1] u [1] [0]
		var mark = _mark32_1.pop();
	    _view[mark] = view8[3]; 
	    //console.log("At position " + mark + " written Length byte " + _view[mark]);
	    mark++;
	    _view[mark] = view8[2]; 
	    //console.log("At position " + mark + " written Length byte " + _view[mark]);
	    mark++;
	    _view[mark] = view8[1]; 
	    //console.log("At position " + mark + " written Length byte " + _view[mark]);
	    mark++;
	    _view[mark] = view8[0];
	    //console.log("At position " + mark + " written Length byte " + _view[mark]);    
	    
	    
	    /*console.log("ItemLength="+ItemLength);
	    console.log("ItemLength="+view32[0]);
	    var ttt = new jDataView(buff); //,0,buff.byteLength,false);
	    console.log("ItemLength="+ttt.getUint32());	    */
	}
	
	this.FixString = function(value,sz)
	{
	    var raz = sz - value.length;
	    for(var i=0; i < raz; i++)
	    {
	        value += " ";
	    }
	    //console.log("val len"+value.length);
	    
	    return value;
	}
	
	this.WriteInt16 = function(value)
	{	    
	    var buff = new ArrayBuffer(2);
		var view16 = new Uint16Array(buff);
		view16[0] = value;
		var view8 = new Uint8Array(buff);
		//obrcem, zbog endiana [0] i [1] u [1] [0]		
	    _view[_cnt] = view8[1];	    
	    _cnt++;
	    //console.log("_view[_cnt]:" + _view[_cnt] + ", value=" + view8[1]);
	    _view[_cnt] = view8[0];	    	    
	    //console.log("_view[_cnt]:" + _view[_cnt] + ", value=" + view8[0]);
	    _cnt++;
	}
	
	this.WriteInt32 = function(value)
	{	    
	    var buff = new ArrayBuffer(4);
		var view32 = new Uint32Array(buff);
		view32[0] = value;
		var view8 = new Uint8Array(buff);
		//obrcem, zbog endiana [0] i [1] u [1] [0]		
	    _view[_cnt] = view8[3]; 
	    //console.log("_view[_cnt]:" + _view[_cnt] + ", value=" + view8[3]);
	    _cnt++;	    
	    _view[_cnt] = view8[2];
	    //console.log("_view[_cnt]:" + _view[_cnt] + ", value=" + view8[2]);
	    _cnt++;	    
	    _view[_cnt] = view8[1];
	    //console.log("_view[_cnt]:" + _view[_cnt] + ", value=" + view8[1]);
	     _cnt++;	    
	    _view[_cnt] = view8[0];	    	    
	    //console.log("_view[_cnt]:" + _view[_cnt] + ", value=" + view8[0]);
	    _cnt++;
	}
}

function AAssociateAC(assoc)
{
    var _assoc = assoc;
    
    this.Read = function (/*RawPDU*/ raw)
    {
		//uint l = raw.Length;
		/*ushort*/var  c = 0;
		
		raw.SkipBytes("Ivan", 2);	    
		//var l = raw.Length();
		var l = raw.ReadUInt32("Length");
		raw.ReadUInt16("Version");
		raw.SkipBytes("Reserved", 2);
		var s  = raw.ReadString("Called AE", 16);
		//raw.SkipBytes("Reserved", 16);
		var s  = raw.ReadString("Called AE", 16);
		//raw.SkipBytes("Reserved", 16);
		raw.SkipBytes("Reserved", 32);	
			
		l -= 68;

		while (l > 0) {
			/*byte*/ var type = raw.ReadByte("Item-Type");
			l -= 1;

			if (type == 0x10) {
				// Application Context
				raw.SkipBytes("Reserved", 1);
				c = raw.ReadUInt16("Item-Length");
				raw.SkipBytes("Value", /*(int)*/c);
				l -= 3 + /*(uint)*/c;
			} 
			else if (type == 0x21) 
			{
				// Presentation Context
				raw.ReadByte("Reserved");
				/*ushort*/ var pl = raw.ReadUInt16("Presentation Context Item-Length");
				/*byte*/ var id = raw.ReadByte("Presentation Context ID");
				raw.ReadByte("Reserved");
				/*byte*/ var res = raw.ReadByte("Presentation Context Result/Reason");
				raw.ReadByte("Reserved");
				l -= /*(uint)*/pl + 3;
				pl -= 4;

				// Presentation Context Transfer Syntax
				raw.ReadByte("Presentation Context Item-Type (0x40)");
				raw.ReadByte("Reserved");
				/*ushort*/ var tl = raw.ReadUInt16("Presentation Context Item-Length");
				var tx = raw.ReadString("Presentation Context Syntax UID", tl);
				pl -= /*(ushort)*/(tl + 4);

                _assoc.SetPresentationContextResult(id, /*(DcmPresContextResult)*/res);
				_assoc.SetAcceptedTransferSyntax(id, DicomTransferSyntax.Lookup(tx));
			}
			else if (type == 0x50) 
			{
				// User Information
                raw.ReadByte("Reserved");
				/*ushort*/var il = raw.ReadUInt16("User Information Item-Length");
				l -= /*(uint)*/(il + 3);
				while (il > 0) 
				{
					/*byte*/ var ut = raw.ReadByte("User Item-Type");
					raw.ReadByte("Reserved");
					/*ushort*/ var ul = raw.ReadUInt16("User Item-Length");
					il -= /*(ushort)*/(ul + 4);
					if (ut == 0x51) {
						_assoc.MaximumPduLength = raw.ReadUInt32("Max PDU Length");
					} else if (ut == 0x52) {
						_assoc.ImplementationClass = DicomUID.Lookup(raw.ReadString("Implementation Class UID", ul));
					} else if (ut == 0x53) {
						_assoc.AsyncOpsInvoked = raw.ReadUInt16("Asynchronous Operations Invoked");
						_assoc.AsyncOpsPerformed = raw.ReadUInt16("Asynchronous Operations Performed");
					} else if (ut == 0x55) {
						_assoc.ImplementationVersion = raw.ReadString("Implementation Version", ul);
					} else {
						raw.SkipBytes("User Item Value", /*(int)*/ul);
					}
				}
			}
			
			else 
			{
				raw.SkipBytes("Reserved", 1);
				/*ushort*/ il = raw.ReadUInt16("User Item-Length");
				raw.SkipBytes("Unknown User Item", il);
                l -= /*(uint)*/(il + 3);
			}
		}
	}
    
    /*return RawPDU*/
    this.Write = function()     
    {
		var pdu = new RawPDU();
		pdu.RawPDUFromByte(/*(byte)*/0x02);        
        
        //ivan
        pdu.Write(/*"Pocetak",*/ 0x02,0x00);
        pdu.MarkLength32("PDU-Length");
		pdu.Write(/*"Version",*/ 0x00,0x01);
		pdu.Write(/*"Reserved",*/ 0x00, 0x00);
		pdu.WriteString(/*"Called AE",*/ pdu.FixString(_assoc.CalledAE,16));
		pdu.WriteString(/*"Calling AE",*/ pdu.FixString(_assoc.CallingAE, 16));
		pdu.Write(/*"Reserved",*/0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00); //32 bytea

//		// Application Context
		pdu.Write(/*"Item-Type", (byte)*/0x10); //_cnt = 74
		pdu.Write(/*"Reserved", (byte)*/0x00);
		pdu.MarkLength16("Item-Length");        
		pdu.WriteString(/*"Application Context Name",*/ DicomUID.DICOMApplicationContextName.UID);		
		pdu.WriteLength16();

        //console.log("PRC:"+_assoc.GetPresentationContexts().length);
        
        var len = _assoc.GetPresentationContexts().length;        

        for (var i = 0; i < len; i++)
        {
		    var pc = _assoc.GetPresentationContexts()[i];
		    if(pc == undefined){
		        continue;
		    }
//			// Presentation Context
			pdu.Write(/*"Item-Type", (byte)*/0x21);
			pdu.Write(/*"Reserved", (byte)*/0x00);
			pdu.MarkLength16("Item-Length");
			pdu.Write(/*"Presentation Context ID", (byte)*/pc.ID());
			pdu.Write(/*"Reserved", (byte)*/0x00);
			pdu.Write(/*"Result", (byte)*/pc.Result());
			pdu.Write(/*"Reserved", (byte)*/0x00);

//			// Transfer Syntax
			pdu.Write(/*"Item-Type", (byte)*/0x40);
			pdu.Write(/*"Reserved", (byte)*/0x00);
			pdu.MarkLength16("Item-Length");
			var ts = pc.AcceptedTransferSyntax();
			pdu.WriteString(/*"Transfer Syntax UID",*/ ts.UID.UID);
			pdu.WriteLength16();

			pdu.WriteLength16();
		}

//		// User Data Fields
		pdu.Write(/*"Item-Type", (byte)*/0x50);
		pdu.Write(/*"Reserved", (byte)*/0x00);
		pdu.MarkLength16("Item-Length");

//		// Maximum PDU
		pdu.Write(/*"Item-Type", (byte)*/0x51);
		pdu.Write(/*"Reserved", (byte)*/0x00);
		pdu.Write(/*"Item-Length", (ushort)*/0x00,0x04);
		//pdu.Write(/*"Max PDU Length", (uint)*/_assoc.MaximumPduLength);
		pdu.WriteInt32(_assoc.MaximumPduLength);

//		// Implementation Class UID
		pdu.Write(/*"Item-Type", (byte)*/0x52);
		pdu.Write(/*"Reserved", (byte)*/0x00);
		pdu.MarkLength16("Item-Length");
		pdu.WriteString(/*"Implementation Class UID",*/ Implementation.ClassUID.UID);
		pdu.WriteLength16();

//		// Asynchronous Operations Negotiation
		if (_assoc.NegotiateAsyncOps()) {            
			pdu.Write(/*"Item-Type", (byte)*/0x53);
			pdu.Write(/*"Reserved", (byte)*/0x00);
			pdu.Write(/*"Item-Length", (ushort)*/0x00, 0x04);
            //BUG popraviti, treba ici WriteInt, a ne Write
			pdu.Write(/*"Asynchronous Operations Invoked", (ushort)*/_assoc.AsyncOpsInvoked);
			pdu.Write(/*"Asynchronous Operations Performed", (ushort)*/_assoc.AsyncOpsPerformed);
		}

		// Implementation Version
		pdu.Write(/*"Item-Type", (byte)*/0x55);
		pdu.Write(/*"Reserved", (byte)*/0x00);
		pdu.MarkLength16("Item-Length");
		pdu.WriteString(/*"Implementation Version",*/ Implementation.Version);
		pdu.WriteLength16();

		pdu.WriteLength16();
		
		//ivan
		pdu.WriteLength32();

		return pdu;
	}
		
}


//function AAssociateRQ : PDU {
function AAssociateRQ(assoc)
{
	//private DcmAssociate _assoc;
    //var _assoc = new DcmAssociate();
    var _assoc = assoc;

	/// <summary>
	/// Initializes new A-ASSOCIATE-RQ
	/// </summary>
	/// <param name="assoc">Association parameters</param>
	this.AAssociateRQ = function(/*DcmAssociate*/ assoc) {
		_assoc = assoc;
	}
	
	this.GetDcmAssociate = function() {
		return _assoc;
	}

    //#region Write
    /// <summary>
    /// Writes A-ASSOCIATE-RQ to PDU buffer
    /// </summary>
    /// <returns>PDU buffer</returns>
	this.Write = function() 
	{
	    var pdu = new RawPDU();
	    pdu.RawPDUFromByte(/*(byte)*/0x01);

	    pdu.Write(/*"Pocetak",*/ 0x01, 0x00);
	    pdu.MarkLength32("PDU-Length");
	    pdu.Write(/*"Version",*/ 0x00,0x01);
	    pdu.Write(/*"Reserved",*/ 0x00, 0x00);
	    pdu.WriteString(/*"Called AE",*/ pdu.FixString(_assoc.CalledAE(),16));
	    pdu.WriteString(/*"Calling AE",*/ pdu.FixString(_assoc.CallingAE(), 16));
	    pdu.Write(/*"Reserved",*/0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00,
		                         0x00,0x00,0x00,0x00); //32 bytea

	    // Application Context
	    pdu.Write(/*"Item-Type", (byte)*/0x10);
	    pdu.Write(/*"Reserved", (byte)*/0x00);
	    pdu.MarkLength16("Item-Length");
	    pdu.WriteString(/*"Application Context Name",*/ DicomUID.DICOMApplicationContextName.UID);
	    pdu.WriteLength16();

	    var len = _assoc.GetPresentationContexts().length;

	    for(var i=0; i < len; i++) 
	    {
		    var pc = _assoc.GetPresentationContexts()[i];
		    if(pc == undefined){
		        continue;
		    }
	        // Presentation Context
            pdu.Write(/*"Item-Type", (byte)*/0x20);
	        pdu.Write(/*"Reserved", (byte)*/0x00);
	        pdu.MarkLength16("Item-Length");
	        pdu.Write(/*"Presentation Context ID", (byte)*/pc.ID());
	        pdu.Write(/*"Reserved", (byte)*/0x00,0x00,0x00);

	        // Abstract Syntax
	        pdu.Write(/*"Item-Type", (byte)*/0x30);
	        pdu.Write(/*"Reserved", (byte)*/0x00);
	        pdu.MarkLength16("Item-Length");	        
	        var as = pc.AbstractSyntax();
	        pdu.WriteString(/*"Abstract Syntax UID",*/ as.UID);
	        pdu.WriteLength16();

	        // Transfer Syntax
	        var txs = pc.GetTransfers();
	        for (_ts in txs)
	        {
	            var ts = txs[_ts];
	            if (ts.IsLossy == undefined) { continue; }

                pdu.Write(/*"Item-Type", (byte)*/0x40);
	            pdu.Write(/*"Reserved", (byte)*/0x00);
	            pdu.MarkLength16("Item-Length");
	            pdu.WriteString(/*"Transfer Syntax UID",*/ ts.UID.UID);
	            pdu.WriteLength16();
	        }

	        pdu.WriteLength16();
        }

        // User Data Fields
        pdu.Write(/*"Item-Type", (byte)*/0x50);
        pdu.Write(/*"Reserved", (byte)*/0x00);
        pdu.MarkLength16("Item-Length");

        // Maximum PDU
        pdu.Write(/*"Item-Type", (byte)*/0x51); 
        pdu.Write(/*"Reserved", (byte)*/0x00);
        pdu.Write(/*"Item-Length", (ushort)*/0x00,0x04);
        pdu.WriteInt32( _assoc.MaximumPduLength());

        // Implementation Class UID
        pdu.Write(/*"Item-Type", (byte)*/0x52);
        pdu.Write(/*"Reserved", (byte)*/0x00);
        pdu.MarkLength16("Item-Length");
        pdu.WriteString(/*"Implementation Class UID",*/ Implementation.ClassUID.UID);
        pdu.WriteLength16();

        // Asynchronous Operations Negotiation
        if (_assoc.NegotiateAsyncOps()) {
            pdu.Write(/*"Item-Type", (byte)*/0x53);
            pdu.Write(/*"Reserved", (byte)*/0x00);
            pdu.Write(/*"Item-Length", (ushort)*/0x00, 0x04);
            //BUG popraviti, treba ici WriteInt, a ne Write
            pdu.Write(/*"Asynchronous Operations Invoked", (ushort)*/_assoc.AsyncOpsInvoked);
            pdu.Write(/*"Asynchronous Operations Performed", (ushort)*/_assoc.AsyncOpsPerformed);
        }

        // Implementation Version
        pdu.Write(/*"Item-Type", (byte)*/0x55);
        pdu.Write(/*"Reserved", (byte)*/0x00); 
        pdu.MarkLength16("Item-Length");
        pdu.WriteString(/*"Implementation Version",*/ Implementation.Version);
        pdu.WriteLength16();

        pdu.WriteLength16();

	    //ivan
        pdu.WriteLength32();

        return pdu;
    }
    //#endregion

	//#region Read
	/// <summary>
	/// Reads A-ASSOCIATE-RQ from PDU buffer
	/// </summary>
	/// <param name="raw">PDU buffer</param>
	this.Read = function(/*RawPDU*/ raw) 
	{	
        raw.SkipBytes("Ivan", 2);	    
		//var l = raw.Length();
		var l = raw.ReadUInt32("Length");		
		raw.ReadUInt16("Version");
		raw.SkipBytes("Reserved", 2);				
		_assoc.CalledAE  = raw.ReadString("Called AE", 16);
		//console.log(_assoc.CalledAE);		
		_assoc.CallingAE = raw.ReadString("Calling AE", 16);
		//log.value = "Dosao mi je zahtjev od " + _assoc.CallingAE + "\n" + log.value;
		raw.SkipBytes("Reserved", 32);
		l -= 2 + 2 + 16 + 16 + 32;

		while (l > 0) 
		{
			/*byte*/ var type = raw.ReadByte("Item-Type");
			raw.SkipBytes("Reserved", 1);			
			/*ushort*/ var il = raw.ReadUInt16("Item-Length");

			l -= 4 + il; //(uint)il

			if (type == 0x10) 
			{
				// Application Context
				raw.SkipBytes("Application Context", il);
			}
			else if (type == 0x20) 
			{
				// Presentation Context
				/*byte*/ var id = raw.ReadByte("Presentation Context ID");
				raw.SkipBytes("Reserved", 3);
				il -= 4;

				while (il > 0) 
				{
					/*byte*/ var pt = raw.ReadByte("Presentation Context Item-Type");
					raw.SkipBytes("Reserved", 1);
					/*ushort*/ var pl = raw.ReadUInt16("Presentation Context Item-Length");
					/*string*/ var sx = raw.ReadString("Presentation Context Syntax UID", pl);
					if (pt == 0x30) 
					{
						_assoc.AddPresentationContext(id, DicomUID.Lookup(sx));
					} else if (pt == 0x40) 
					{
						_assoc.AddTransferSyntax(id, DicomTransferSyntax.Lookup(sx));
					}
					il -= /*(ushort)*/(4 + pl);
				}
			} else

			if (type == "0x50") {
				// User Information
				while (il > 0) {
					/*byte*/var ut = raw.ReadByte("User Information Item-Type");
					raw.SkipBytes("Reserved", 1);
					/*ushort*/ var ul = raw.ReadUInt16("User Information Item-Length");
					il -= /*(ushort)*/(4 + ul);
					if (ut == 0x51) {
						_assoc.MaximumPduLength = raw.ReadUInt32("Max PDU Length");
					} else if (ut == 0x52) {
						_assoc.ImplementationClass = new DicomUID(raw.ReadString("Implementation Class UID", ul), "Implementation Class UID", DicomUidType.Unknown);
					} else if (ut == 0x55) {
						_assoc.ImplementationVersion = raw.ReadString("Implementation Version", ul);
					} else if (ut == 0x53) {
						_assoc.AsyncOpsInvoked = raw.ReadUInt16("Asynchronous Operations Invoked");
						_assoc.AsyncOpsPerformed = raw.ReadUInt16("Asynchronous Operations Performed");
					} else if (ut == 0x54) {
						raw.SkipBytes("SCU/SCP Role Selection", ul);
					} else{
						console.error("Unhandled user item: 0x{0:x2} ({1} + 4 bytes)", ut, ul);
						raw.SkipBytes("Unhandled User Item", ul);
					}
				}
			}
		}
	}
	//#endregion
	
	
}


//#region A-Release-RQ
/// <summary>A-RELEASE-RQ</summary>
function AReleaseRQ()
{
	/// <summary>
	/// Writes A-RELEASE-RQ to PDU buffer
	/// </summary>
	/// <returns>PDU buffer</returns>
	this.Write = function(){
		var /*RawPDU*/ pdu = new RawPDU();
		pdu.RawPDUFromByte(/*(byte)*/0x05);
		pdu.Write("Reserved", /*(uint)*/0x00000000);
		return pdu;
	}

	/// <summary>
	/// Reads A-RELEASE-RQ from PDU buffer
	/// </summary>
	/// <param name="raw">PDU buffer</param>
	this.Read = function(/*RawPDU*/ raw) {
		raw.ReadUInt32("Reserved");
	}
}
//#endregion

//#region P-Data-TF
///<summary>P-DATA-TF</summary>
//public class PDataTF : PDU 
function PDataTF()
{
	/*List<PDV>*/ var _pdvs = [];

	/// <summary>
	/// Initializes new P-DATA-TF
	/// </summary>
//	public PDataTF() {
//	}

	/// <summary>PDVs in this P-DATA-TF</summary>
	this.PDVs = function()
	{
		return _pdvs;
	}

	/// <summary>Calculates the total length of the PDVs in this P-DATA-TF</summary>
	/// <returns>Length of PDVs</returns>
	this.GetLengthOfPDVs = function() 
	{
	    /*uint*/ var len = 0;
	    var pdvLen = _pdvs.length;
	    for (var i = 0; i < pdvLen; i++)
		{
		    len += _pdvs[i].PDVLength();
		}
		return len;
	}
	
	this.PDVs = function()
	{
		return _pdvs;
	}

	//#region Write
	/// <summary>
	/// Writes P-DATA-TF to PDU buffer
	/// </summary>
	/// <returns>PDU buffer</returns>
	this.Write = function() 
	{
	    /*RawPDU*/ pdu = new RawPDU(0x04);
	    pdu.RawPDUFromByte(0x04);
	    //var pdvLen = _pdvs.length;
		for(pd in _pdvs) 
		{
		    var num = parseInt(pd);
		    if(isNaN(num)){continue;}

		    var pdv = _pdvs[num];
			pdv.Write(pdu);
		}
		
		return pdu;
	}
	//#endregion

	//#region Read
	/// <summary>
	/// Reads P-DATA-TF from PDU buffer
	/// </summary>
	/// <param name="raw">PDU buffer</param>
	this.Read = function(/*RawPDU*/ raw) 
	{
	    raw.SkipBytes("Ivan",2);
	    var len = raw.ReadUInt32("PDU-Length");
		/*uint*/ //var len = raw.Length();
		/*uint*/ var read = 0;
		var cnt=0;
		while (read < len) 
		{
			var pdv = new PDV();
			read += pdv.Read(raw);
			_pdvs.push(pdv);
			//_pdvs[cnt] = pdv;
			cnt++;
		}
	}
	//#endregion
}
//#endregion


function PDV(/*byte*/ pcid, /*byte[]*/ value, /*bool*/ command, /*bool*/ last) 
{
	/*private byte*/ var _pcid;
	/*private byte[]*/ var _value = [0];
	/*private bool*/ var _command = false;
	/*private bool*/ var _last = false;
	
	//ivan
	//ivan
	this.Raw;
	this.ValueBuff;
	

	/// <summary>
	/// Initializes new PDV
	/// </summary>
	/// <param name="pcid">Presentation context ID</param>
	/// <param name="value">PDV data</param>
	/// <param name="command">Is command</param>
	/// <param name="last">Is last fragment of command or data</param>
	//public PDV(byte pcid, byte[] value, bool command, bool last) {
		_pcid = pcid;
		_value = value;
		_command = command;
		_last = last;	
		
	//}

	/// <summary>
	/// Initializes new PDV
	/// </summary>
//	public PDV() {
//	}

	/// <summary>Presentation context ID</summary>
	this.PCID = function(){
		return _pcid;
	}

	/// <summary>PDV data</summary>
	this.Value = function()
	{
	    return _value;	
	}

	/// <summary>PDV is command</summary>
	this.IsCommand = function()
	{
		return _command;		
	}

	/// <summary>PDV is last fragment of command or data</summary>
	this.IsLastFragment = function(value)
	{
	    if (value == undefined) {
	        return _last;
	    }
	    else {
	        _last = value;
	    }
	}

	/// <summary>Length of this PDV</summary>
	this.PDVLength = function()
	{
		return /*(uint)*/_value.length + 6;
	}

	//#region Write
	/// <summary>
	/// Writes PDV to PDU buffer
	/// </summary>
	/// <param name="pdu">PDU buffer</param>
	this.Write = function(/*RawPDU*/ pdu) 
	{
		/*byte*/ var mch = /*(byte)*/((_last ? 2 : 0) + (_command ? 1 : 0));
		pdu.MarkLength32("PDV-Length");
		pdu.Write(/*"Presentation Context ID", (byte)*/_pcid);
		pdu.Write(/*"Message Control Header", (byte)*/mch);
		pdu.WriteByteArray(/*"PDV Value",*/ _value);
		pdu.WriteLength32();
		return pdu;
	}
	//#endregion

	//#region Read
	/// <summary>
	/// Reads PDV from PDU buffer
	/// </summary>
	/// <param name="raw">PDU buffer</param>
	this.Read = function(/*RawPDU*/ raw) 
	{	    
		this.Raw = raw.Is();
	
	    /*uint*/ var len = raw.ReadUInt32("PDV-Length");
	    //console.log("PDV-Length="+len);
		_pcid = raw.ReadByte("Presentation Context ID");
		/*byte*/ var mch = raw.ReadByte("Message Control Header");
		_value = raw.ReadBytesAlt2("PDV Value", /*(int)*/len - 2);
		this.ValueBuff = _value;
		_command = (mch & 0x01) != 0;
		_last = (mch & 0x02) != 0;
		return len + 4;
	}
	//#endregion
}
	//#endregion