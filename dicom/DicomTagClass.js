﻿

function DicomTag(/*ushort*/ group, /*ushort*/ element, /* string */ creator)
{
		//#region Private Members
		var /*ushort*/ _g;
		var /*ushort*/ _e;
		var /*uint*/ _c;
		var /*string*/ _p;
		var /*DcmDictionaryEntry*/ _entry;
        //#endregion

		this.Name;

		//#region Public Constructors
		this.DicomTagFromCard = function(/*uint*/ card) 
		{
			_g = /*(ushort)*/(card >> 16);
			_e = /*(ushort)*/(card & 0xffff);
			_c = card;
			_p = ""; //String.Empty;
		}

		//public DicomTag(ushort group, ushort element) {
			_g = group;
			_e = element;
			_c = /*(uint)*/_g << 16 | /*(uint)*/_e;
			_p = "";
		//}

			this.Name = _g + ":" + _e;

//		public DicomTag(ushort group, ushort element, string creator) {
//			_g = group;
//			_e = element;
//			_c = (uint)_g << 16 | (uint)_e;
			_p = "";//ivan todo
//		}

//		private DicomTag(SerializationInfo info, StreamingContext context) {
//			_g = info.GetUInt16("Group");
//			_e = info.GetUInt16("Element");
//			_c = (uint)_g << 16 | (uint)_e;
//			_p = String.Empty;
//		}
//        //#endregion

        //#region Public Properties
        this.Group = function(){return _g;}
		this.Element = function(){return _e; }
		this.Card = function(){return _c; }
		this.PrivateGroup = function()
		{			
			if (!this.IsPrivate()){
			    return 0x00;
			}				
			if (this.Element() < 0xff00)
			{
			    return /*(byte)*/(0x00ff & this.Element());
			}
			
			return /*(byte)*/((this.Element() & 0xff00) >> 8);
			
		}
		this.PrivateCreator = function(){return _p; }
		this.Entry = function()
		{
		    if (_entry == null){
			    _entry = DcmDictionary.Lookup(this); //todo
			}
			
			return _entry;
		}
		

		this.IsPrivate = function()
		{
			return (_g & 1) == 1;
		}
		//#endregion
		
		//Operators !!!!
		this.Equals = function(/* DicomTag */ t2) {
		
			if (this == null && t2 == null)
				return true;
			if (this == null || t2 == null)
				return false;
		
			if (this.Group() == t2.Group() && this.Element() == t2.Element())
			{
				return true;
			}
			else return false;
			
		}
		
		this.LargerOrEqual = function(/* DicomTag */ t2)
		{
			if (this == null && t2 == null)
				return true;
			if (this == null || t2 == null)
				return false;
		
		
			if (this.Group() == t2.Group() && this.Element() >= t2.Element())
				return true;
			if (this.Group() > t2.Group())
				return true;
			return false;
		
		}

    }
	
		DicomTag.GetCard = function(/*ushort*/ group, /*ushort*/ element) 
		{
			return /*(uint)*/group << 16 | /*(uint)*/element;
		}


		DicomTag.IsPrivateGroup = function(/*ushort*/ group) 
		{
			return (group & 1) == 1;
		}

		DicomTag.GetPrivateCreatorTag = function(group, element) 
		{
			return new DicomTag(group, /*(ushort)*/(element >> 8));
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
	
	