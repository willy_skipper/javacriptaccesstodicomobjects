function DcmItemSequenceItem(/* long */ pos, /* uint */ length) //: DcmItem
{ 
	//var inherit;

	// #region Private Members
	var /* DcmDataset */ _dataset;
	// #endregion

	// #region Public Constructors
	// public DcmItemSequenceItem()
		// : base(DicomTags.Item, DicomVR.NONE) {
	// }

	// public DcmItemSequenceItem(long pos, uint length)
		// : base(DicomTags.Item, DicomVR.NONE, pos, Endian.LocalMachine) {
	// }
	// #endregion
	
	this.inherit = new DcmItem(DicomTags.Item, DicomVR.NONE, pos, Endian.LocalMachine);

	// #region Public Properties
	this.Dataset = function(value){
		if(value != undefined) {
			if (_dataset == null)
				_dataset = new DcmDataset(DicomTransferSyntax.ExplicitVRLittleEndian);
			return _dataset;
		}
		else {
			_dataset = value;
			this.inherit.Endian(_dataset.InternalTransferSyntax.Endian);
		}
	}

	this.StreamLength = function()
	{
		return _dataset.StreamLength;
	}
	// #endregion

	// #region DcmItem Overrides
	function CalculateWriteLength(/* DicomTransferSyntax */ syntax, /* DicomWriteOptions */ options) {
		var length = 4 + 4;
		length += Dataset().CalculateWriteLength(syntax, options & ~DicomWriteOptions.CalculateGroupLengths);
		if (!Flags.IsSet(options, DicomWriteOptions.ExplicitLengthSequenceItem))
			length += 4 + 4; // Sequence Item Delimitation Item
		return length;
	}

	/* protected override void ChangeEndianInternal() {
		Dataset().SelectByteOrder(Endian);
	}

	internal override void Preload() {
		if (_dataset != null)
			_dataset.PreloadDeferredBuffers();
	}

	internal override void Unload() {
		if (_dataset != null)
			_dataset.UnloadDeferredBuffers();
	}

	public override DcmItem Clone() {
		DcmItemSequenceItem si = new DcmItemSequenceItem(StreamPosition, StreamLength);
		si.Dataset = Dataset.Clone();
		return si;
	}

	public override void Dump(StringBuilder sb, string prefix, DicomDumpOptions options) {
		sb.AppendLine().Append(prefix).Append(" Item:").AppendLine();
		Dataset.Dump(sb, prefix + "  > ", options);
		sb.Length = sb.Length - 1;
	} */
	// #endregion
}

function DcmItemSequence(/* DicomTag */ tag, /* long */ pos, /* uint */ length, /* Endian */ endian) //: DcmItem {
{
	//var inherit;
	var /* List<DcmItemSequenceItem> */ _items = [];
	var _streamLength = 0xffffffff;

	this.type = "DcmItemSequence";

	// public DcmItemSequence(DicomTag tag) 
		// : base(tag, DicomVR.SQ) {
	// }

	// public DcmItemSequence(DicomTag tag, long pos, uint length, Endian endian)
		// : base(tag, DicomVR.SQ, pos, endian) {
		if (length != undefined) _streamLength = length;
	// }
	
	this.inherit = new DcmItem(tag, DicomVR.SQ, pos, endian);

	this.StreamLength = function()
	{
		 return _streamLength;
	}

	this.SequenceItems = function(){
		return _items;
	}

	//ivan nazvao drugacije
	this.AddSequenceItemFromDataset = function(/* DcmDataset */ itemDataset) {
		var /* DcmItemSequenceItem */ item = new DcmItemSequenceItem();
		item.Dataset(itemDataset);
		//AddSequenceItem(item);
		_items.push(item);
	}

	this.AddSequenceItem = function(/* DcmItemSequenceItem */ item) {
		_items.push(item);
	}

	function CalculateWriteLength(/* DicomTransferSyntax */ syntax, /* DicomWriteOptions */ options) 
	{
		var length = 0;
		length += 4; // element tag
		if (syntax.IsExplicitVR()) {
			length += 2; // vr
			length += 6; // length
		} else {
			length += 4; // length
		}
		for (/* DcmItemSequenceItem */ var item in _items) {
			length += item.CalculateWriteLength(syntax, options);
		}
		if (!Flags.IsSet(options, DicomWriteOptions.ExplicitLengthSequence))
			length += 4 + 4; // Sequence Delimitation Item
		return length;
	}

	/* protected override void ChangeEndianInternal() {
		foreach (DcmItemSequenceItem item in SequenceItems) {
			item.Endian = Endian;
		}
	}

	internal override void Preload() {
		foreach (DcmItemSequenceItem item in SequenceItems) {
			item.Preload();
		}
	}

	internal override void Unload() {
		foreach (DcmItemSequenceItem item in SequenceItems) {
			item.Unload();
		}
	}

	public override DcmItem Clone() {
		DcmItemSequence sq = new DcmItemSequence(Tag, StreamPosition, StreamLength, Endian);
		foreach (DcmItemSequenceItem si in SequenceItems) {
			sq.AddSequenceItem((DcmItemSequenceItem)si.Clone());
		}
		return sq;
	}

	public override void Dump(StringBuilder sb, string prefix, DicomDumpOptions options) {
		sb.Append(prefix);
		sb.AppendFormat("{0} {1} {2}", Tag.ToString(), VR.VR, Tag.Entry.Name);
		foreach (DcmItemSequenceItem item in SequenceItems) {
			item.Dump(sb, prefix, options);
		}
	} */
}

