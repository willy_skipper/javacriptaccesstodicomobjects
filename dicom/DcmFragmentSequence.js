function DcmFragmentSequence(/* DicomTag */ tag, /* DicomVR */ vr, /* long */ pos, /* Endian */ endian) //: DcmItem 
{
	//var inherit;

		// #region Private Members
		var  /* List<uint> */ _table = [];
		var  /* List<ByteBuffer> */ _fragments = [];
    // #endregion

		this.type = "DcmFragmentSequence";

		// #region Public Constructors
		// public DcmFragmentSequence(DicomTag tag, DicomVR vr) : base(tag, vr) {
		// }

		// public DcmFragmentSequence(DicomTag tag, DicomVR vr, long pos, Endian endian)
			// : base(tag, vr, pos, endian) {
		// }
		// #endregion
		
		this.inherit = new DcmItem(/* DicomTag */ tag, /* DicomVR */ vr, /* long */ pos, /* Endian */ endian);

		// #region Public Properties
		this.HasOffsetTable = function()
		{
			return _table != null;
		}

		this.OffsetTableBuffer = function()
		{
			
				var /* ByteBuffer */ offsets = new ByteBuffer();
				if (_table != null) {
					for(var offset in _table) {
						offsets.Writer.Write(offset);
					}
				}
				return offsets;
			
		}

		this.OffsetTable = function()
		{
		
			if (_table == null)
				_table = [];
			return _table;
	
		}

		this.Fragments = function(){
			return _fragments;
		}
		// #endregion

		// #region Public Methods
		this.SetOffsetTable = function(/* ByteBuffer */ table) {
			_table = [];
			
			for(var i=0; i < table.length; i++)			
				_table.push(table[i].ToUInt32s()); //todo jdataView
		}
		
		this.SetOffsetTable = function(/* List<uint> */ table) 
		{		
			for(var i=0; i < table.length; i++)				
				_table.push(table[i]);
		}

		function AddFragment(/* ByteBuffer */ fragment) {
			_fragments.push(fragment);
		}
		// #endregion

		// #region DcmItem Methods
		function CalculateWriteLength(/* DicomTransferSyntax */ syntax, /* DicomWriteOptions */ options) 
		{
			var length = 0;
			length += 4; // element tag
			if (syntax.IsExplicitVR()) {
				length += 2; // vr
				length += 6; // length
			} else {
				length += 4; // length
			}
			length += 4 + 4; // offset tag
			if (Flags.IsSet(options, DicomWriteOptions.WriteFragmentOffsetTable) && _table != null)
				length += _table.Count * 4;
				
			for (/* ByteBuffer */ var bb in _fragments) {
				length += 4; // item tag
				length += 4; // fragment length
				length += bb.Length;
			}
			
			return length;
		}

		function ChangeEndianInternal() {
			for (/* ByteBuffer */var bb in _fragments) {
				if (bb.Endian != Endian) {
					bb.Endian = Endian;
					bb.Swap(VR.UnitSize);
				}
			}
		}

		function Preload() {
			for(/* ByteBuffer */ var bb in _fragments) {
				//bb.Preload();
			}
		}
	
		function Unload() {
			for(/* ByteBuffer */ var bb in _fragments) {
				//bb.Unload();
			}
		}

		function Clone() 
		{
			var /* DcmFragmentSequence */ sq = new DcmFragmentSequence(Tag, VR, StreamPosition, Endian);
			sq.SetOffsetTable(OffsetTable);
			for(/* ByteBuffer */ var fragment in _fragments) {
				//sq.AddFragment(fragment.Clone());
			}
			return sq;
		}

/* 		public override void Dump(StringBuilder sb, string prefix, DicomDumpOptions options) {
			sb.Append(prefix);
			sb.AppendFormat("{0} {1} {2} {3}", Tag.ToString(), VR.VR, Tag.Entry.Name, HasOffsetTable ? "/w Offset Table" : "");
			for (int i = 0; i < _fragments.Count; i++) {
				sb.AppendLine().Append(prefix).AppendFormat(" Fragment {0}:  {1} bytes", i, _fragments[i].Length);
			}
		} */
		// #endregion
	}