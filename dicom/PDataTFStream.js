﻿function PDataTFStream(/*Stream*/ network, /*byte*/ pcid, /*int*/ max)
{
    var /*Stream*/ _network;
    var /*bool*/ _command;
    var /*int*/ _max;
    var /*byte*/ _pcid;
    var /*PDataTF*/ _pdu;
    var /*byte[]*/ _bytes = null;
    var /*int*/ _sent;
    var /*MemoryStream*/ _buffer;

    //constructor
    _network = network;
    _command = true;
    _pcid = pcid;
    _max = (max == 0) ? MaxPduSizeLimit : Math.min(max, PDataTFStream.MaxPduSizeLimit);
    _pdu = new PDataTF();
    _buffer = new ArrayBuffer(_max * 2);
    _buffer = new DataView(_buffer);
    _bufferLen = 0;
    _bufferOffset = 0;
    _bytesRead = 0;

    //ivan
    var bytesForSending = [];

    this.Buffer = function () {
        return _buffer;
    }

    this.IsCommand = function(value)
    {
        if(value == undefined) { return _command; }
        else 
        {
            CreatePDV();
            _command = value;
            WritePDU(true);
        }
    }

    this.BytesSent  = function()
    {
        return _sent;
    }

    this.Flush = function(/*bool*/ last) 
    {
        WritePDU(last);
        var sendMe = bytesForSendingToBuffer();
        var vju = new Uint8Array(sendMe);
        _network.send(sendMe);
    }

    function bytesForSendingToBuffer()
    {
        var len = bytesForSending.length;
        var buffTemp = bytesForSending[0];

        for (var i = 1; i < len; i++)
        {
            buffTemp = appendBuffer(buffTemp,bytesForSending[i]);
        }

        return buffTemp;
    }



    function CurrentPduSize() 
    {
        return 6 + _pdu.GetLengthOfPDVs();
    }

    function CreatePDV() 
    {
        var /*int*/ len = Math.min(GetBufferLength(), _max - CurrentPduSize() - 6);

        if (_bytes == null || _bytes.byteLength != len || _pdu.PDVs().length > 0) {
            _bytes = new Uint8Array(len);
        }

        //_sent = _buffer.Read(_bytes, 0, len);
        var uin = new Uint8Array(_buffer.buffer);
        var jj = 0;
        for (var i = _bytesRead; i < _bufferLen; i++) {
            _bytes[jj] = uin[i];
            jj++;
        }
        _bytesRead = i;
        _bufferOffset = _bufferLen;
        
        var /*PDV*/ pdv = new PDV(_pcid, _bytes, _command, false);
        _pdu.PDVs().push(pdv);

        return pdv.IsLastFragment();
    }

    function WritePDU(/*bool*/ last) 
    {
        var pdvs = _pdu.PDVs();
        var curPDUSize = CurrentPduSize();
        var getBuffLen = GetBufferLength();

        if (pdvs.length == 0 || ((curPDUSize + 6) < _max && getBuffLen > 0))
        {
            CreatePDV();
        }

		if (pdvs.length > 0)
		{
		    if (last)
		    {
		        pdvs[pdvs.length - 1].IsLastFragment(true);
            }
		    var /*RawPDU*/ raw = _pdu.Write();
		    var baff = pdvs[0].Value();		    

		    bytesForSending.push(raw.WritePDU2(_network, baff));
            //if (OnPduSent != null)
                //OnPduSent();
		    _pdu = new PDataTF();

		    //test
		    //_buffer = new ArrayBuffer(_max * 2);
		    //_buffer = new DataView(_buffer);
		    //_bufferLen = 0;
		    //_bufferOffset = 0;

        }
    }

    function AppendBuffer(type, /*byte[]*/ buffer, /*int*/ offset, /*int*/ count) 
    {
        var /*long*/ pos = _buffer.byteOffset;
        //_buffer.Seek(0, SeekOrigin.End);
        //_buffer.Write(buffer, offset, count);
        //_buffer.Position = pos;

        switch (type) {

            case "ushort":
                _buffer.setUint16(_bufferLen, buffer, true);
                _bufferLen += 2;
                //_bufferOffset += 2;
                break;

            case "byte":
                _buffer.setUint8(_bufferLen, buffer,true);
                _bufferLen += 1;
                //_bufferOffset += 1;
                break;

            case "uint":
                _buffer.setUint32(_bufferLen, buffer, true);
                _bufferLen += 4;
                //_bufferOffset += 4;
                break;
            case "buffer":
                for (var i = 0; i < buffer.byteLength; i++) {
                    _buffer.setUint8(_bufferLen, buffer[i], true);
                    _bufferLen += 1;
                    //_bufferOffset += 1;
                }
                break;
        }

        //var jjj = new Uint8Array(_buffer.buffer);

    }

    function GetBufferLength() 
    {
        return /*(int)*/(_bufferLen - _bufferOffset);
    }


    this.Write = function(type, /*byte[]*/ buffer, /*int*/ offset, /*int*/ count) 
    {
		AppendBuffer(type, buffer, offset, count);

		var currPduSize = CurrentPduSize();
		var buffLen = GetBufferLength();

		while ((currPduSize + 6 + buffLen) > _max)
        {
            WritePDU(false);
        }
    }

    this.Write2 = function(/*Stream*/ stream) 
    {
        var max = _max - 12;
        var length = stream.Length;
        var position = stream.Position;
        var /*byte[]*/ buffer = new Uint8Array(max);

        while (position < length) 
        {
            var count = Math.min(max, length - position);
            count = stream.Read(buffer, 0, count);
            AppendBuffer(buffer, 0, count);
            position += count;
            WritePDU(position == length);
        }

        _network.Flush();
    }

}

PDataTFStream.MaxPduSizeLimit = 4 * 1024 * 1024;