//malo proptoipova koji mi fale




	var DicomReadStatus =
	{
		// <summary>Read operation completed successfully</summary>
		"Success":0,
		/// <summary>Unknown error occurred during read operation</summary>
		"UnknownError":1,
		/// <summary>More data is needed to complete dataset</summary>
		"NeedMoreData":2,
		/// <summary>Found the tag we were looking for (internal)</summary>
		"SuccessEndRead":3
	};

	/// <summary>
	/// Reads a DICOM dataset from a stream
	/// </summary>
	function DicomStreamReader(/* Stream */ stream, bFile)
    {
		//#region var Members
	    var /* const uint */ UndefinedLength = 0xFFFFFFFF;

	    if (bFile == undefined) { bFile = false; }

		var /* Stream */ _stream = null;
		var /* BinaryReader */ _reader = null;
		var /* DicomTransferSyntax */ _syntax = null;
		var /* Encoding */ _encoding = DcmEncoding.Default();
		var /* Endian */ _endian;
		var /* bool */ _isFile;

		var /* uint */ _largeElementSize = 4096;

		var /* DcmDataset */ _dataset;

		var /* uint */ _privateCreatorCard = 0xffffffff;
		var /* string */ _privateCreatorId = "";

		var /* DicomTag */ _tag = null;
		var /* DicomVR */ _vr = null;
		var /* uint */ _len = UndefinedLength;
		var /* long */ _pos = 0;
		var /* long */ _offset = 0;

		var /* long */ _bytes = 0;
		var /* long */ _read = 0;
		var /* uint */ _need = 0;
		var /* long */ _remain = 0;

		var /* Stack<DcmDataset> */ _sds = [];
		var /* Stack<DcmItemSequence> */ _sqs = [];
		var /* DcmFragmentSequence */ _fragment = null;
	    // #endregion

	    //ivan
		var _bDataset;

		// #region Public Constructors
		/// <summary>
		/// Initializes a new DicomStreamReader with a source stream
		/// </summary>
		/// <param name="stream">Source stream</param>
		//this.DicomStreamReader = function(/* Stream */ stream) 
		//{			
            
		if (stream.byteLength == undefined)
		{
		    var streLen = stream.length;
		    //console.log("strema="+streLen);
		    _stream = new Uint8Array(streLen);
		    for (var zz = 0; zz < streLen; zz++) {
		        _stream[zz] = stream[zz];
		    }

		    _reader = new jDataView(_stream.buffer, 0, _stream.byteLength, false);
		}
		else
		{
		    _stream = stream;
		    _reader = new jDataView(_stream, 0, _stream.byteLength, false);
		}
			
		_isFile = bFile;
		TransferSyntax = DicomTransferSyntax.ExplicitVRLittleEndian;
		//ivan
        _syntax = TransferSyntax;


		this.Stream = _stream;

		// #region Public Properties
		/// <summary>
		/// Transfer syntax
		/// </summary>
		this.TransferSyntax = function(value)
		{
			if(value == undefined)
			{
				return _syntax; 
			}			
			else
			{
				_syntax = value;
				_endian = _syntax.Endian;
				// _reader = EndianBinaryReader.Create(_stream, _encoding, _endian);
				//_reader = new jDataView(_stream,0,_stream.byteLength, false);
			}
		}

		/// <summary>
		/// String encoding
		/// </summary>
		this.Encoding = function(value)
		{		
			if(value == undefined) { return _encoding; }
			else {
				_encoding = value;
				this.TransferSyntax(_syntax);
			}
		}

		/// <summary>
		/// DICOM dataset
		/// </summary>
		this.Dataset = function(value,bDataset)
		{
			if(value == undefined) { return _dataset; }
			else {
			    _dataset = value;

			    if (bDataset == undefined) bDataset = false;

			    _bDataset = bDataset;

			    if (bDataset) {
			        this.TransferSyntax(_dataset.InternalTransferSyntax());
			    } else {
			        this.TransferSyntax(_dataset.inherit.InternalTransferSyntax());
			    }
			}
		}

		/// <summary>
		/// Estimated size of dataset
		/// </summary>
		this.BytesEstimated = function(){
			return _bytes + _need;
		}

		/// <summary>
		/// Number of bytes read from stream
		/// </summary>
		this.BytesRead = function(){
			return _read;
		}

		/// <summary>
		/// Number of bytes remaining in stream
		/// </summary>
		this.BytesRemaining = function(){
			return _remain;
		}

		/// <summary>
		/// Number of bytes needed to complete current read operation
		/// </summary>
		this.BytesNeeded = function()
		{
			return _need;
		}

		/// <summary>
		/// Offset of this stream relative to a parent stream.
		/// </summary>
		this.PositionOffset = function(value)
		{
			if(value == undefined) { return _offset; }
			else { _offset = value; }
		}

		/// <summary>
		/// Minimum size, in bytes, of elements that should be defer loaded when
		/// using the DicomReadOptions.DeferLoadingLargeElements read option.
		/// </summary>
		this.LargeElementSize = function(){
			if(value == undefined) { return _largeElementSize; }
			else { _largeElementSize = value; }
		}
		// #endregion
		
		function CurrentBuffer(/* DicomReadOptions */ options) 
		{
			var /* ByteBuffer */ bb = null;

			if (_isFile)
			{
				 var delayLoad = false;
				 if (_len >= _largeElementSize && _vr != DicomVR.SQ) {
					 if (Flags.IsSet(options, DicomReadOptions.DeferLoadingLargeElements))
						 delayLoad = true;
					 else if (Flags.IsSet(options, DicomReadOptions.DeferLoadingPixelData) && _tag == DicomTags.PixelData)
						 delayLoad = true;
					 else if (Flags.IsSet(options, DicomReadOptions.DeferLoadingPixelData) && _fragment != null && _fragment.Tag == DicomTags.PixelData)
						 delayLoad = true;
				 }

				 if (delayLoad) 
				 {
					 //var FileStream fs = (FileStream)_stream;
					 //FileSegment segment = new FileSegment(fs.Name, fs.Position, _len);
					 //_stream.Seek(_len, SeekOrigin.Current);
					 bb = new ByteBuffer();
					 bb.ByteBuffer2(segment, _endian);
				 }
			}

            if (bb == null) {
                bb = new ByteBuffer();
                bb.ByteBuffer1(_endian);
                bb.CopyFrom(_reader, _len);
			}

            if (_vr.IsEncodedString()) {
                //bb.Encoding = _encoding;
            }

			return bb;
		}
		
		function NeedMoreData(/* long */ count) {
			_need = /* (uint) */(count - _remain);
			return DicomReadStatus.NeedMoreData;
		}


		/// <summary>
		/// Read dataset from stream
		/// </summary>
		/// <param name="stopAtTag">End parsing at this tag</param>
		/// <param name="options">DICOM read options</param>
		/// <returns>Status code</returns>
		this.Read = function (/* DicomTag */ stopAtTag, /* DicomReadOptions */ options, bFileMetaInfoOnly)
		{
			// Counters:
			//  _remain - bytes remaining in stream
			//  _bytes - estimates bytes to end of dataset
			//  _read - number of bytes read from stream
		    //try {

		    //if (stopAtTag != null && stopAtTag.Equals(DcmFileMetaInfo.StopTag))
		    if (bFileMetaInfoOnly != undefined)
		        _reader.seek(132);

				_need = 0;
				_remain = _stream.byteLength - _reader.tell();

				while (_remain > 0) 
				{
					var /* DicomReadStatus */ status = ParseTag(stopAtTag, options);
					if (status == DicomReadStatus.SuccessEndRead)
						return DicomReadStatus.Success;
					if (status != DicomReadStatus.Success)
						return status;

					status = ParseVR(options);
					if (status != DicomReadStatus.Success)
						return status;

					status = ParseLength(options);
					if (status != DicomReadStatus.Success)
						return status;
					
					if (_tag.IsPrivate()) 
					{
						if (_tag.Element() != 0x0000 && _tag.Element() <= 0x00ff) 
						{
							// handle UN var creator id
							if (_vr.VR() != DicomVR.LO.VR() && Flags.IsSet(options, DicomReadOptions.ForcevarCreatorToLO)) {
								//Dicom.Debug.Log.Warn("Converting var Creator VR from '{0}' to 'LO'", _vr.VR);
								_vr = DicomVR.LO;
							}
						}
					}

					if (_vr.VR() == DicomVR.UN.VR() && _syntax.IsExplicitVR && Flags.IsSet(options, DicomReadOptions.UseDictionaryForExplicitUN)) {
						_vr = _tag.Entry().DefaultVR();
					}

					if (_fragment != null) {
						status = InsertFragmentItem(options);
						if (status != DicomReadStatus.Success)
							return status;
					}
					else if (_sqs.length > 0 &&
								(_tag.Equals(DicomTags.Item) ||
								 _tag.Equals(DicomTags.ItemDelimitationItem) ||
								 _tag.Equals(DicomTags.SequenceDelimitationItem)) ) 
					{
						status = InsertSequenceItem(options);
						if (status != DicomReadStatus.Success)
							return status;
					}
					else {
						if (_sqs.Count() > 0) {
							var /* DcmItemSequence */ sq = _sqs.Peek();
							if (sq.StreamLength != UndefinedLength) {
								var end = sq.StreamPosition + 8 + sq.StreamLength();
								if (_syntax.IsExplicitVR)
									end += 2 + 2;
								if ((_reader.tell() - _offset) >= end) {
									if (_sds.Count() == _sqs.Count())
										_sds.pop();
									_sqs.pop();
								}
							}
						}

						if (_len == UndefinedLength) {
							if (_vr == DicomVR.SQ) {
								var /* DcmItemSequence */ sq = new DcmItemSequence(_tag, _pos, _len, _endian);
								InsertDatasetItem(sq, options);
								_sqs.push(sq);
							}
							else {
								_fragment = new DcmFragmentSequence(_tag, _vr, _pos, _endian);
								InsertDatasetItem(_fragment, options);
							}
						}
						else {
							if (_vr == DicomVR.SQ) {
								var /* DcmItemSequence */ sq = new DcmItemSequence(_tag, _pos, _len, _endian);
								InsertDatasetItem(sq, options);
								_sqs.Push(sq);
							}
							else {
								if (_len > _remain)
									return NeedMoreData(_len);

								//var ele = new DcmElement(_tag, _vr, _pos, _endian, CurrentBuffer(options));
								var /* DcmElement */ elem = DcmElement.Create3(_tag, _vr, _pos, _endian, CurrentBuffer(options));
								//console.log(elem);
								_remain -= _len;
								_read += _len;

							    //try{
							    //    console.log(elem.inherit.GetValueString());
							    //}
							    //catch (e)
							    //{
							    //    //console.log(elem.inherit.inherit.GetValueString(0));
							    //}

								InsertDatasetItem(elem, options);
							}
						}
					}

					_tag = null;
					_vr = null;
					_len = UndefinedLength;
				}

				return DicomReadStatus.Success;
			//}
			//catch (err) {
			//	 should never happen
			//	console.log(err);
			//	return DicomReadStatus.UnknownError;
			//}
		}

		function ParseTag(/* DicomTag */ stopAtTag, /* DicomReadOptions */ options){
			if (_tag == null) 
			{
				if (_remain >= 4) 
				{
					_pos = _reader.tell() + _offset;
					var g = _reader.getUint16(_reader.tell(), true);
					if (Flags.IsSet(options, DicomReadOptions.FileMetaInfoOnly) && g != 0x0002) 
					{
						//_stream.Seek(-2, SeekOrigin.Current);						
						_reader.seek(_reader.tell()-2);
						return DicomReadStatus.SuccessEndRead;
					}
					
					var e = _reader.getUint16(_reader.tell(), true);
					if (DicomTag.IsPrivateGroup(g) && e > 0x00ff) 
					{
						var card = DicomTag.GetCard(g, e);
						if ((card & 0xffffff00) != _privateCreatorCard) 
						{
							_privateCreatorCard = card & 0xffffff00;
							var /* DicomTag */ pct = DicomTag.GetPrivateCreatorTag(g, e);
							var /* DcmDataset */ ds = _dataset;
							if (_sds.length > 0 && _sds.length == _sqs.length) 
							{
								ds = _sds.Peek(); //peek - Peek does not remove the element from the Stack collection. It only gets the value�in other words, it 'peeks' at the value
								//ds = _sds[_sds.length]; //stoga dohva�amo zadnji element niza
								if (!ds.Contains(pct))
									ds = _dataset;
							}
							_privateCreatorId = ds.GetString(pct, /* String.Empty */ "");
						}
						_tag = new DicomTag(g, e, _privateCreatorId);
					}
					else 
					{
					    _tag = new DicomTag(g, e);
					    var b = _tag.Entry();

						if (g == 0xfffe) 
						{
							if (_tag == DicomTags.Item ||
								_tag == DicomTags.ItemDelimitationItem ||
								_tag == DicomTags.SequenceDelimitationItem)
								_vr = DicomVR.NONE;
						}
					}
					_remain -= 4;
					_bytes += 4;
					_read += 4;
				}
				else 
				{
					return NeedMoreData(4);
				}
			}

			if (_tag.Equals(DicomTags.ItemDelimitationItem) && Flags.IsSet(options, DicomReadOptions.SequenceItemOnly))
			{
				return DicomReadStatus.SuccessEndRead;
			}

			if (_tag.LargerOrEqual(stopAtTag)){
				return DicomReadStatus.SuccessEndRead;
			}

			return DicomReadStatus.Success;
		}

		function ParseVR(/* DicomReadOptions */ options) {
			if (_vr == null) {
				if (_syntax.IsExplicitVR) {
					if (_remain >= 2) {
					    _vr = DicomVR.Lookup1(_reader.getString(2,_reader.tell()));
						_remain -= 2;
						_bytes += 2;
						_read += 2;
					}
					else {
						return NeedMoreData(2);
					}
				}
				else {
					if (_tag.Element() == 0x0000)
						_vr = DicomVR.UL;
					else if (Flags.IsSet(options, DicomReadOptions.ForcePrivateCreatorToLO) &&
						_tag.IsPrivate() && _tag.Element() > 0x0000 && _tag.Element() <= 0x00ff)
						_vr = DicomVR.UN;
					else
						_vr = _tag.Entry().DefaultVR();
				}

				if (_vr == DicomVR.UN) {
					if (_tag.Element == 0x0000)
						_vr = DicomVR.UL; // is this needed?
					else if (_tag.IsPrivate()) {
						if (_tag.Element() <= 0x00ff) {
							// private creator id
						} else if (/* _stream.CanSeek && */ Flags.IsSet(options, DicomReadOptions.AllowSeekingForContext)) {
							// attempt to identify private sequence
							var pos = _reader.tell();
							if (_syntax.IsExplicitVR) {
								if (_remain >= 2)
									_reader.getUint16();
								else {
									_vr = null;
									_reader._offset = pos;
									return NeedMoreData(2);
								}
							}

							var l = 0;
							if (_remain >= 4) {
								l = _reader.getUint32(_reader.tell(),false);
								if (l == UndefinedLength)
									_vr = DicomVR.SQ;
							} else {
								_vr = null;
								_reader._offset = pos;
								return NeedMoreData(4);
							}

							_reader._offset = pos;
						}
					}
				}
			}
			return DicomReadStatus.Success;
		}

		function ParseLength(/* DicomReadOptions */ options) {
			if (_len == UndefinedLength) {
				if (_syntax.IsExplicitVR) {
					if (_tag.Equals(DicomTags.Item) ||
						_tag.Equals(DicomTags.ItemDelimitationItem) ||
						_tag.Equals(DicomTags.SequenceDelimitationItem) ) 
						{
						if (_remain >= 4) {
						    _len = _reader.getUint32(_reader.tell(), true);
							_remain -= 4;
							_bytes += 4;
							_read += 4;
						}
						else {
							return NeedMoreData(4);
						}
					}
					else {
						if (_vr.Is16BitLengthField()) {
							if (_remain >= 2) {
							    _len = _reader.getUint16(_reader.tell(), true);
								_remain -= 2;
								_bytes += 2;
								_read += 2;
							} else {
								return NeedMoreData(2);
							}
						}
						else {
							if (_remain >= 6) {
								_reader.getUint8();
								_reader.getUint8();
								_len = _reader.getUint32(_reader.tell(), true);
								_remain -= 6;
								_bytes += 6;
								_read += 6;
							}
							else {
								return NeedMoreData(6);
							}
						}
					}
				}
				else {
					if (_remain >= 4) {
						_len = _reader.getUint32(_reader.tell(),true);
						_remain -= 4;
						_bytes += 4;
						_read += 4;
					}
					else {
						return NeedMoreData(4);
					}
				}

				if (_len != UndefinedLength) 
				{
					if (_vr.VR() != DicomVR.SQ.VR() && !(_tag.Equals(DicomTags.Item) && _fragment == null))
						_bytes += _len;
				}
			}
			return DicomReadStatus.Success;
		}
		
		function InsertFragmentItem(/* DicomReadOptions */ options) {
			if (_tag.Equals(DicomTags.Item) ) 
			{
				if (_len > _remain)
					return NeedMoreData(_len);

				var /* ByteBuffer */ data = CurrentBuffer(options);
				_remain -= _len;
				_read += _len;

				if (!_fragment.HasOffsetTable)
					_fragment.SetOffsetTable(data);
				else
					_fragment.AddFragment(data);
			}
			else if (_tag == DicomTags.SequenceDelimitationItem) {
				_fragment = null;
			}
			else {
				// unexpected tag
				return DicomReadStatus.UnknownError;
			}
			return DicomReadStatus.Success;
		}
		
		function InsertSequenceItem(/* DicomReadOptions */ options) 
		{
			if (_tag.Equals(DicomTags.Item)) 
			{
				if (_len != UndefinedLength && _len > _remain)
					return NeedMoreData(_len);

				if (_sds.Count > _sqs.Count)
					_sds.Pop();

				var /* DcmItemSequenceItem */ si = new DcmItemSequenceItem(_pos, _len);

				if (_len != UndefinedLength || (_stream.CanSeek && Flags.IsSet(options, DicomReadOptions.AllowSeekingForContext))) 
				{
					if (_len == UndefinedLength)
						options |= DicomReadOptions.SequenceItemOnly;

					var /* DcmDataset */ ds = null;
					var /* DicomReadStatus */ status = ParseSequenceItemDataset(TransferSyntax, _len, ds, options);

					if (status == DicomReadStatus.NeedMoreData)
						return DicomReadStatus.NeedMoreData;

					if (status != DicomReadStatus.Success) {
						Dicom.Debug.Log.Warn("Unknown error while attempting to read sequence item.  Trying again with alternate encodings.");

						var /* DicomTransferSyntax[] */ syntaxes = [];
						if (TransferSyntax == DicomTransferSyntax.ExplicitVRBigEndian)
							syntaxes = [DicomTransferSyntax.ImplicitVRLittleEndian, DicomTransferSyntax.ExplicitVRLittleEndian];
						else if (TransferSyntax.IsExplicitVR)
							syntaxes =[DicomTransferSyntax.ImplicitVRLittleEndian, DicomTransferSyntax.ExplicitVRBigEndian];
						else
							syntaxes = [DicomTransferSyntax.ExplicitVRLittleEndian, DicomTransferSyntax.ExplicitVRBigEndian ];

						for(tx in syntaxes) {
							status = ParseSequenceItemDataset(tx, _len, ds, options);
							if (status == DicomReadStatus.Success)
								break;
						}
					}

					if (status != DicomReadStatus.Success)
						return DicomReadStatus.UnknownError;

					si.Dataset = ds;

					if (_len == UndefinedLength) {
						if (8 > _remain) {
							// need more data?
							_sds.Push(ds);
						}
						else {
							// skip delimitation item
							_stream.Seek(8, SeekOrigin.Current);
							_remain -= 8;
							_bytes += 8;
							_read += 8;
						}
					}
				}
				else {
					var /* DcmDataset */ ds = new DcmDataset(_pos + 8, _len, TransferSyntax);
					_sds.Push(ds);
				}

				_sqs.Peek().AddSequenceItem(si);
			}
			else if (_tag == DicomTags.ItemDelimitationItem) {
				if (_sds.Count == _sqs.Count)
					_sds.Pop();
			}
			else if (_tag == DicomTags.SequenceDelimitationItem) {
				if (_sds.Count == _sqs.Count)
					_sds.Pop();
				_sqs.Pop();
			}
			return DicomReadStatus.Success;
		}
	
		function ParseSequenceItemDataset(/* DicomTransferSyntax */ syntax, /* long */ len, /* out DcmDataset */ dataset, /* DicomReadOptions */ options) {
			var pos = _stream.Position;

			dataset = new DcmDataset(pos, len, syntax);

			var /* Stream */ stream = (len != UndefinedLength) ? new SegmentStream(_stream, _stream.Position, _len) : _stream;

			var /* DicomStreamReader */ idsr = new DicomStreamReader(stream);
			
			idsr.Dataset(dataset);
			idsr.Encoding(_encoding);
			if (len != UndefinedLength)
				idsr.PositionOffset(dataset.StreamPosition);

			var /* DicomReadStatus */ status = idsr.Read(null, options);

				//todo
			if (status != DicomReadStatus.Success) {
				_stream.Seek(pos, SeekOrigin.Begin);
				dataset = null;
			}
			else {
				if (len == UndefinedLength) {
					// rewind delimitation item tag
					_stream.Seek(-4, SeekOrigin.Current);

					len = _stream.Position - pos;
				}

				_remain -= len;
				_bytes += len;
				_read += len;
			}

			return status;
		}
	
		function InsertDatasetItem(/* DcmItem */ item, /* DicomReadOptions */ options) {
			if (_sds.length > 0 && _sds.length == _sqs.length) {
				var /* DcmDataset */ ds = _sds.Peek();

				if (_tag.Element() == 0x0000) {
					if (Flags.IsSet(options, DicomReadOptions.KeepGroupLengths))
						ds.AddItem(item);
				}
				else
					ds.AddItem(item);

				if (ds.StreamLength() != UndefinedLength) {
					var /* long */ end = ds.StreamPosition() + ds.StreamLength();
					if ((_reader.tell() - _offset) >= end)
						_sds.pop();
				}
			}
			else {
			    if (_tag.Element() == 0x0000) {
			        if (Flags.IsSet(options, DicomReadOptions.KeepGroupLengths)) {
			            if (_bDataset)
			                _dataset.AddItem(item);
			            else
			                _dataset.inherit.AddItem(item);
			        }
			    }
			    else {
			        if (_bDataset)
			            _dataset.AddItem(item);
                    else
			            _dataset.inherit.AddItem(item);

			    }
			}

			/* if (_tag.Equals(DicomTags.SpecificCharacterSet) && item.Name == "DcmCodeString") 
			{
				var  DcmCodeString  cs = item;
				if (cs.Length() > 0) {
					string[] values = cs.GetValues();
					for (int i = 0; i < values.Length; i++) {
						if (String.IsNullOrEmpty(values[i]))
							continue;
						_encoding = DcmEncoding.GetEncodingForSpecificCharacterSet(values[i]);
						break;
					}
				}
			} 
			*/
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	