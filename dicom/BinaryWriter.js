﻿function BinaryWriter()
{
    _max = 1024;
    var _buffer = new ArrayBuffer(_max * 2);
    _buffer = new DataView(_buffer);
    _bufferLen = 0;

    this.Write = function (type, /*byte[]*/ buffer)
    {
        var pos = _buffer.byteOffset;

        switch (type) {

            case "ushort":
                _buffer.setUint16(_bufferLen, buffer, true);
                _bufferLen += 2;
                break;

            case "byte":
                _buffer.setUint8(_bufferLen, buffer, true);
                _bufferLen += 1;
                break;

            case "uint":
                _buffer.setUint32(_bufferLen, buffer, true);
                _bufferLen += 4;
                break;
            case "buffer":
                for (var i = 0; i < buffer.byteLength; i++) {
                    _buffer.setUint8(_bufferLen, buffer[i], true);
                    _bufferLen += 1;
                }
                break;
        }
    }

    this.Buffer = function ()
    {
        var buff = new Uint8Array(_bufferLen);
        var buffO = new Uint8Array(_buffer.buffer);

        for (var i = 0; i < _bufferLen; i++)
            buff[i] = buffO[i];

        return buff;
    }

}