﻿var DcmState =
{
    "Success":1,
    "Cancel":2,
    "Pending":3,
    "Warning":4,
    "Failure":5
};

function DcmStatus(/*string*/ code, /*DcmState*/ status, /*string*/ desc)
{
    this.Code;

    /// <summary>State of this DICOM status code.</summary>
    this.State;

    /// <summary>Description.</summary>
    this.Description;

    this.ErrorComment = null;
		
    this.Mask;

    /// <summary>
    /// Initializes a new instance of the <see cref="DcmStatus"/> class.
    /// </summary>
    /// <param name="code">The code.</param>
    /// <param name="status">The status.</param>
    /// <param name="desc">The desc.</param>
    //this.DcmStatus = function(string code, DcmState status, string desc) 
    {
        this.Code = parseInt(code.replace(/x/gi, '0'), 16);

        var msb = code.toLowerCase();
        
        msb = msb.replace(/0/gi, 'F'); //, "g");
        msb = msb.replace(/1/gi, 'F'); //, "g");
        msb = msb.replace(/2/gi, 'F'); //, "g");
        msb = msb.replace(/3/gi, 'F'); //, "g");
        msb = msb.replace(/4/gi, 'F'); //, "g");
        msb = msb.replace(/5/gi, 'F'); //, "g");
        msb = msb.replace(/6/gi, 'F'); //, "g");
        msb = msb.replace(/7/gi, 'F'); //, "g");
        msb = msb.replace(/8/gi, 'F'); //, "g");
        msb = msb.replace(/9/gi, 'F'); //, "g");
        msb = msb.replace(/a/gi, 'F'); //, "g");
        msb = msb.replace(/b/gi, 'F'); //, "g");
        msb = msb.replace(/c/gi, 'F'); //, "g");
        msb = msb.replace(/d/gi, 'F'); //, "g");
        msb = msb.replace(/e/gi, 'F'); //, "g");
        //msb = msb.replace(/f/gi, 'F'); //, "g");
        msb = msb.replace(/x/gi, '0'); //, "g");

        this.Mask = parseInt(msb, 16);

        this.State = status;
        this.Description = desc;
    }



}

DcmStatus.Entries = [];
DcmStatus.QuickLookup = [];


    DcmStatus.Success = new DcmStatus("0000", DcmState.Success, "Success");

    /// <summary>Cancel: Cancel</summary>
    DcmStatus.Cancel = new DcmStatus("FE00", DcmState.Cancel, "Cancel");

    /// <summary>Pending: Pending</summary>
    DcmStatus.Pending = new DcmStatus("FF00", DcmState.Pending, "Pending");

    /// <summary>Warning: Attribute list error</summary>
    DcmStatus.AttributeListError = new DcmStatus("0107", DcmState.Warning, "Attribute list error");

    /// <summary>Warning: Attribute Value Out of Range</summary>
    DcmStatus.AttributeValueOutOfRange = new DcmStatus("0116", DcmState.Warning, "Attribute Value Out of Range");

    /// <summary>Failure: Refused: SOP class not supported</summary>
    DcmStatus.SOPClassNotSupported = new DcmStatus("0122", DcmState.Failure, "Refused: SOP class not supported");

    /// <summary>Failure: Class-instance conflict</summary>
    DcmStatus.ClassInstanceConflict = new DcmStatus("0119", DcmState.Failure, "Class-instance conflict");

    /// <summary>Failure: Duplicate SOP instance</summary>
    DcmStatus.DuplicateSOPInstance = new DcmStatus("0111", DcmState.Failure, "Duplicate SOP instance");

    /// <summary>Failure: Duplicate invocation</summary>
    DcmStatus.DuplicateInvocation = new DcmStatus("0210", DcmState.Failure, "Duplicate invocation");

    /// <summary>Failure: Invalid argument value</summary>
    DcmStatus.InvalidArgumentValue = new DcmStatus("0115", DcmState.Failure, "Invalid argument value");

    /// <summary>Failure: Invalid attribute value</summary>
    DcmStatus.InvalidAttributeValue = new DcmStatus("0106", DcmState.Failure, "Invalid attribute value");

    /// <summary>Failure: Invalid object instance</summary>
    DcmStatus.InvalidObjectInstance = new DcmStatus("0117", DcmState.Failure, "Invalid object instance");

    /// <summary>Failure: Missing attribute</summary>
    DcmStatus.MissingAttribute = new DcmStatus("0120", DcmState.Failure, "Missing attribute");

    /// <summary>Failure: Missing attribute value</summary>
    DcmStatus.MissingAttributeValue = new DcmStatus("0121", DcmState.Failure, "Missing attribute value");

    /// <summary>Failure: Mistyped argument</summary>
    DcmStatus.MistypedArgument = new DcmStatus("0212", DcmState.Failure, "Mistyped argument");

    /// <summary>Failure: No such argument</summary>
    DcmStatus.NoSuchArgument = new DcmStatus("0114", DcmState.Failure, "No such argument");

    /// <summary>Failure: No such event type</summary>
    DcmStatus.NoSuchEventType = new DcmStatus("0113", DcmState.Failure, "No such event type");

    /// <summary>Failure: No Such object instance</summary>
    DcmStatus.NoSuchObjectInstance = new DcmStatus("0112", DcmState.Failure, "No Such object instance");

    /// <summary>Failure: No Such SOP class</summary>
    DcmStatus.NoSuchSOPClass = new DcmStatus("0118", DcmState.Failure, "No Such SOP class");

    /// <summary>Failure: Processing failure</summary>
    DcmStatus.ProcessingFailure = new DcmStatus("0110", DcmState.Failure, "Processing failure");

    /// <summary>Failure: Resource limitation</summary>
    DcmStatus.ResourceLimitation = new DcmStatus("0213", DcmState.Failure, "Resource limitation");

    /// <summary>Failure: Unrecognized operation</summary>
    DcmStatus.UnrecognizedOperation = new DcmStatus("0211", DcmState.Failure, "Unrecognized operation");

    /// <summary>Failure: No such action type</summary>
    DcmStatus.NoSuchActionType = new DcmStatus("0123", DcmState.Failure, "No such action type");

    /// <summary>Storage Failure: Out of Resources</summary>
    DcmStatus.StorageStorageOutOfResources = new DcmStatus("A7xx", DcmState.Failure, "Out of Resources");

    /// <summary>Storage Failure: Data Set does not match SOP Class (Error)</summary>
    DcmStatus.StorageDataSetDoesNotMatchSOPClassError = new DcmStatus("A9xx", DcmState.Failure, "Data Set does not match SOP Class (Error)");

    /// <summary>Storage Failure: Cannot understand</summary>
    DcmStatus.StorageCannotUnderstand = new DcmStatus("Cxxx", DcmState.Failure, "Cannot understand");

    /// <summary>Storage Warning: Coercion of Data Elements</summary>
    DcmStatus.StorageCoercionOfDataElements = new DcmStatus("B000", DcmState.Warning, "Coercion of Data Elements");

    /// <summary>Storage Warning: Data Set does not match SOP Class (Warning)</summary>
    DcmStatus.StorageDataSetDoesNotMatchSOPClassWarning = new DcmStatus("B007", DcmState.Warning, "Data Set does not match SOP Class (Warning)");

    /// <summary>Storage Warning: Elements Discarded</summary>
    DcmStatus.StorageElementsDiscarded = new DcmStatus("B006", DcmState.Warning, "Elements Discarded");

    /// <summary>QueryRetrieve Failure: Out of Resources</summary>
    DcmStatus.QueryRetrieveOutOfResources = new DcmStatus("A700", DcmState.Failure, "Out of Resources");

    /// <summary>QueryRetrieve Failure: Unable to calculate number of matches</summary>
    DcmStatus.QueryRetrieveUnableToCalculateNumberOfMatches = new DcmStatus("A701", DcmState.Failure, "Unable to calculate number of matches");

    /// <summary>QueryRetrieve Failure: Unable to perform suboperations</summary>
    DcmStatus.QueryRetrieveUnableToPerformSuboperations = new DcmStatus("A702", DcmState.Failure, "Unable to perform suboperations");

    /// <summary>QueryRetrieve Failure: Move Destination unknown</summary>
    DcmStatus.QueryRetrieveMoveDestinationUnknown = new DcmStatus("A801", DcmState.Failure, "Move Destination unknown");

    /// <summary>QueryRetrieve Failure: Identifier does not match SOP Class</summary>
    DcmStatus.QueryRetrieveIdentifierDoesNotMatchSOPClass = new DcmStatus("A900", DcmState.Failure, "Identifier does not match SOP Class");

    /// <summary>QueryRetrieve Failure: Unable to process</summary>
    DcmStatus.QueryRetrieveUnableToProcess = new DcmStatus("Cxxx", DcmState.Failure, "Unable to process");

    /// <summary>QueryRetrieve Pending: Optional Keys Not Supported</summary>
    DcmStatus.QueryRetrieveOptionalKeysNotSupported = new DcmStatus("FF01", DcmState.Pending, "Optional Keys Not Supported");

    /// <summary>QueryRetrieve Warning: Sub-operations Complete - One or more Failures</summary>
    DcmStatus.QueryRetrieveSubOpsOneOrMoreFailures = new DcmStatus("B000", DcmState.Warning, "Sub-operations Complete - One or more Failures");

    /// <summary>PrintManagement Warning: Memory allocation not supported</summary>
    DcmStatus.PrintManagementMemoryAllocationNotSupported = new DcmStatus("B000", DcmState.Warning, "Memory allocation not supported");

    /// <summary>PrintManagement Warning: Film session printing (collation) is not supported</summary>
    DcmStatus.PrintManagementFilmSessionPrintingNotSupported = new DcmStatus("B601", DcmState.Warning, "Film session printing (collation) is not supported");

    /// <summary>PrintManagement Warning: Film session SOP instance hierarchy does not contain image box SOP instances (empty page)</summary>
    DcmStatus.PrintManagementFilmSessionEmptyPage = new DcmStatus("B602", DcmState.Warning, "Film session SOP instance hierarchy does not contain image box SOP instances (empty page)");

    /// <summary>PrintManagement Warning: Film box SOP instance hierarchy does not contain image box SOP instances (empty page)</summary>
    DcmStatus.PrintManagementFilmBoxEmptyPage = new DcmStatus("B603", DcmState.Warning, "Film box SOP instance hierarchy does not contain image box SOP instances (empty page)");

    /// <summary>PrintManagement Warning: Image size is larger than image box size, the image has been demagnified</summary>
    DcmStatus.PrintManagementImageDemagnified = new DcmStatus("B604", DcmState.Warning, "Image size is larger than image box size, the image has been demagnified");

    /// <summary>PrintManagement Warning: Requested min density or max density outside of printer's operating range</summary>
    DcmStatus.PrintManagementMinMaxDensityOutOfRange = new DcmStatus("B605", DcmState.Warning, "Requested min density or max density outside of printer's operating range");

    /// <summary>PrintManagement Warning: Image size is larger than the image box size, the Image has been cropped to fit</summary>
    DcmStatus.PrintManagementImageCropped = new DcmStatus("B609", DcmState.Warning, "Image size is larger than the image box size, the Image has been cropped to fit");

    /// <summary>PrintManagement Warning: Image size or combined print image size is larger than the image box size, image or combined print image has been decimated to fit</summary>
    DcmStatus.PrintManagementImageDecimated = new DcmStatus("B60A", DcmState.Warning, "Image size or combined print image size is larger than the image box size, image or combined print image has been decimated to fit");

    /// <summary>PrintManagement Failure: Film session SOP instance hierarchy does not contain film box SOP instances</summary>
    DcmStatus.PrintManagementFilmSessionEmpty = new DcmStatus("C600", DcmState.Failure, "Film session SOP instance hierarchy does not contain film box SOP instances");

    /// <summary>PrintManagement Failure: Unable to create Print Job SOP Instance; print queue is full</summary>
    DcmStatus.PrintManagementPrintQueueFull = new DcmStatus("C601", DcmState.Failure, "Unable to create Print Job SOP Instance; print queue is full");

    /// <summary>PrintManagement Failure: Image size is larger than image box size</summary>
    DcmStatus.PrintManagementImageLargerThanImageBox = new DcmStatus("C603", DcmState.Failure, "Image size is larger than image box size");

    /// <summary>PrintManagement Failure: Insufficient memory in printer to store the image</summary>
    DcmStatus.PrintManagementInsufficientMemoryInPrinter = new DcmStatus("C605", DcmState.Failure, "Insufficient memory in printer to store the image");

    /// <summary>PrintManagement Failure: Combined Print Image size is larger than the Image Box size</summary>
    DcmStatus.PrintManagementCombinedImageLargerThanImageBox = new DcmStatus("C613", DcmState.Failure, "Combined Print Image size is larger than the Image Box size");

    /// <summary>PrintManagement Failure: There is an existing film box that has not been printed and N-ACTION at the Film Session level is not supported.</summary>
    DcmStatus.PrintManagementExistingFilmBoxNotPrinted = new DcmStatus("C616", DcmState.Failure, "There is an existing film box that has not been printed and N-ACTION at the Film Session level is not supported.");

    /// <summary>MediaCreationManagement Failure: Refused because an Initiate Media Creation action has already been received for this SOP Instance</summary>
    DcmStatus.MediaCreationManagementDuplicateInitiateMediaCreation = new DcmStatus("A510", DcmState.Failure, "Refused because an Initiate Media Creation action has already been received for this SOP Instance");

    /// <summary>MediaCreationManagement Failure: Media creation request already completed</summary>
    DcmStatus.MediaCreationManagementMediaCreationRequestAlreadyCompleted = new DcmStatus("C201", DcmState.Failure, "Media creation request already completed");

    /// <summary>MediaCreationManagement Failure: Media creation request already in progress and cannot be interrupted</summary>
    DcmStatus.MediaCreationManagementMediaCreationRequestAlreadyInProgress = new DcmStatus("C202", DcmState.Failure, "Media creation request already in progress and cannot be interrupted");

    /// <summary>MediaCreationManagement Failure: Cancellation denied for unspecified reason</summary>
    DcmStatus.MediaCreationManagementCancellationDeniedForUnspecifiedReason = new DcmStatus("C203", DcmState.Failure, "Cancellation denied for unspecified reason");

    DcmStatus.Entries.push(DcmStatus.Success);
    DcmStatus.Entries.push(DcmStatus.Cancel);
    DcmStatus.Entries.push(DcmStatus.Pending);
    DcmStatus.Entries.push(DcmStatus.AttributeListError);
    DcmStatus.Entries.push(DcmStatus.AttributeValueOutOfRange);
    DcmStatus.Entries.push(DcmStatus.SOPClassNotSupported);
    DcmStatus.Entries.push(DcmStatus.ClassInstanceConflict);
    DcmStatus.Entries.push(DcmStatus.DuplicateSOPInstance);
    DcmStatus.Entries.push(DcmStatus.DuplicateInvocation);
    DcmStatus.Entries.push(DcmStatus.InvalidArgumentValue);
    DcmStatus.Entries.push(DcmStatus.InvalidAttributeValue);
    DcmStatus.Entries.push(DcmStatus.InvalidObjectInstance);
    DcmStatus.Entries.push(DcmStatus.MissingAttribute);
    DcmStatus.Entries.push(DcmStatus.MissingAttributeValue);
    DcmStatus.Entries.push(DcmStatus.MistypedArgument);
    DcmStatus.Entries.push(DcmStatus.NoSuchArgument);
    DcmStatus.Entries.push(DcmStatus.NoSuchEventType);
    DcmStatus.Entries.push(DcmStatus.NoSuchObjectInstance);
    DcmStatus.Entries.push(DcmStatus.NoSuchSOPClass);
    DcmStatus.Entries.push(DcmStatus.ProcessingFailure);
    DcmStatus.Entries.push(DcmStatus.ResourceLimitation);
    DcmStatus.Entries.push(DcmStatus.UnrecognizedOperation);
    DcmStatus.Entries.push(DcmStatus.NoSuchActionType);
    DcmStatus.Entries.push(DcmStatus.StorageStorageOutOfResources);
    DcmStatus.Entries.push(DcmStatus.StorageDataSetDoesNotMatchSOPClassError);
    DcmStatus.Entries.push(DcmStatus.StorageCannotUnderstand);
    DcmStatus.Entries.push(DcmStatus.StorageCoercionOfDataElements);
    DcmStatus.Entries.push(DcmStatus.StorageDataSetDoesNotMatchSOPClassWarning);
    DcmStatus.Entries.push(DcmStatus.StorageElementsDiscarded);
    DcmStatus.Entries.push(DcmStatus.QueryRetrieveOutOfResources);
    DcmStatus.Entries.push(DcmStatus.QueryRetrieveUnableToCalculateNumberOfMatches);
    DcmStatus.Entries.push(DcmStatus.QueryRetrieveUnableToPerformSuboperations);
    DcmStatus.Entries.push(DcmStatus.QueryRetrieveMoveDestinationUnknown);
    DcmStatus.Entries.push(DcmStatus.QueryRetrieveIdentifierDoesNotMatchSOPClass);
    DcmStatus.Entries.push(DcmStatus.QueryRetrieveUnableToProcess);
    DcmStatus.Entries.push(DcmStatus.QueryRetrieveOptionalKeysNotSupported);
    DcmStatus.Entries.push(DcmStatus.QueryRetrieveSubOpsOneOrMoreFailures);
    DcmStatus.Entries.push(DcmStatus.PrintManagementMemoryAllocationNotSupported);
    DcmStatus.Entries.push(DcmStatus.PrintManagementFilmSessionPrintingNotSupported);
    DcmStatus.Entries.push(DcmStatus.PrintManagementFilmSessionEmptyPage);
    DcmStatus.Entries.push(DcmStatus.PrintManagementFilmBoxEmptyPage);
    DcmStatus.Entries.push(DcmStatus.PrintManagementImageDemagnified);
    DcmStatus.Entries.push(DcmStatus.PrintManagementMinMaxDensityOutOfRange);
    DcmStatus.Entries.push(DcmStatus.PrintManagementImageCropped);
    DcmStatus.Entries.push(DcmStatus.PrintManagementImageDecimated);
    DcmStatus.Entries.push(DcmStatus.PrintManagementFilmSessionEmpty);
    DcmStatus.Entries.push(DcmStatus.PrintManagementPrintQueueFull);
    DcmStatus.Entries.push(DcmStatus.PrintManagementImageLargerThanImageBox);
    DcmStatus.Entries.push(DcmStatus.PrintManagementInsufficientMemoryInPrinter);
    DcmStatus.Entries.push(DcmStatus.PrintManagementCombinedImageLargerThanImageBox);
    DcmStatus.Entries.push(DcmStatus.PrintManagementExistingFilmBoxNotPrinted);
    DcmStatus.Entries.push(DcmStatus.MediaCreationManagementDuplicateInitiateMediaCreation);
    DcmStatus.Entries.push(DcmStatus.MediaCreationManagementMediaCreationRequestAlreadyCompleted);
    DcmStatus.Entries.push(DcmStatus.MediaCreationManagementMediaCreationRequestAlreadyInProgress);
    DcmStatus.Entries.push(DcmStatus.MediaCreationManagementCancellationDeniedForUnspecifiedReason);

    DcmStatus.Lookup = function(/*ushort*/ code) 
    {
        for (var i = 0; i < DcmStatus.Entries.length; i++)
        {
            var el = DcmStatus.Entries[i];
            if (el.Code == (code & el.Mask))
            {
                return el;
            }
        }

        return DcmStatus.ProcessingFailure;
    }


