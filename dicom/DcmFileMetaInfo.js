﻿
function DcmFileMetaInfo() //inherits DcmDataset
{

    this.inherit = new DcmDataset();
    this.inherit.DcmDataset(DicomTransferSyntax.ExplicitVRLittleEndian);    

    //#region Public Properties

    this.FileMetaInformationVersion = function(value)
    {
        if(value == undefined) {
            if (this.inherit.Contains(DicomTags.FileMetaInformationVersion)){
                return this.inherit.GetOB(DicomTags.FileMetaInformationVersion).GetValues();
            }
            return null;
        }
        else {
            if (this.inherit.AddElement(DicomTags.FileMetaInformationVersion, DicomVR.OB)){
                this.inherit.GetOB(DicomTags.FileMetaInformationVersion).inherit.SetValuesFromByte(value);
            }
        }
    }

    this.MediaStorageSOPClassUID = function(value)
    {
        if(value == undefined) {
            return this.inherit.GetUID(DicomTags.MediaStorageSOPClassUID);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.MediaStorageSOPClassUID, DcmValueType.DicomUID, value);
        }
    }

    this.MediaStorageSOPInstanceUID = function(value) {
        if(value == undefined)  {
            return this.inherit.GetUID(DicomTags.MediaStorageSOPInstanceUID);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.MediaStorageSOPInstanceUID, DcmValueType.DicomUID, value);
        }
    }

    this.TransferSyntax  = function(value){
        if(value == undefined)  {
            if (this.inherit.Contains(DicomTags.TransferSyntaxUID))
                return this.inherit.GetUI(DicomTags.TransferSyntaxUID).GetTS();
            return null;
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.TransferSyntaxUID, DcmValueType.DicomUID, value.UID);
        }
    }
    

    this.ImplementationClassUID = function(value) {
        if(value == undefined)  {
            return this.inherit.GetUID(DicomTags.ImplementationClassUID);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.ImplementationClassUID, DcmValueType.DicomUID, value);
        }
    }

    this.ImplementationVersionName  = function(value){
        if(value == undefined)  {
            return this.inherit.GetString(DicomTags.ImplementationVersionName, null);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.ImplementationVersionName, DcmValueType.String, value);
        }
    }

    this.SourceApplicationEntityTitle  = function(value){
        if(value == undefined)  {
            return this.inherit.GetString(DicomTags.SourceApplicationEntityTitle, null);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.SourceApplicationEntityTitle, DcmValueType.String, value.rtrim());
        }
    }

    this.PrivateInformationCreatorUID  = function(value){
        if(value == undefined)  {
            return this.inherit.GetUID(DicomTags.PrivateInformationCreatorUID);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.PrivateInformationCreatorUID, DcmValueType.DicomUID, value.UID);
        }
    }

    this.PrivateInformation = function (value) {
        if(value == undefined)  {
            if (this.inherit.Contains(DicomTags.PrivateInformation))
                return this.inherit.GetOB(DicomTags.PrivateInformation).inherit.inherit.GetValues();
            return null;
        }
        else {
            if (this.inherit.AddElement(DicomTags.PrivateInformation, DicomVR.OB))
                this.inherit.GetOB(DicomTags.PrivateInformation).inherit.inherit.SetValues(value);
        }
    }
		//#endregion

}

DcmFileMetaInfo.Version = new Uint8Array(2);
DcmFileMetaInfo.Version[0] = 0x00;
DcmFileMetaInfo.Version[1] = 0x01;
DcmFileMetaInfo.StopTag = new DicomTag(0x0002, 0xFFFF);