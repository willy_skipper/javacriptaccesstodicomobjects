﻿//String.prototype.Contains = function (val) {
//    if (this.search(val) != -1) { return true; }
//    else return false;
//}

var LOGGER =
{
    "MESSAGE": 0,
    "SUCCESS": 1,
    "ERROR": 2
};



String.IsNullOrEmpty = function (value)
{
    if (value == null || value == "") {
        return true;
    }
    else
        return false;
};

String.prototype.Empty = function () {
    return "";
};

String.prototype.rtrim = function ()
{
    return this.replace(/\s+$/, '');
};

String.prototype.rtrimWithNull = function () {
    return this.replace(/\s+$/, '\0');
};

String.prototype.Contains = function (it)
{
    return this.indexOf(it) != -1;
};

Array.prototype.Contains = function (obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}

Array.prototype.AddRange = function (range) {
    for (elem in range) {
        if (!isNaN(elem))
            this.push(range[elem]);
    }
}

Array.prototype.Count = function () {
    return this.length;
};

Array.prototype.Peek = function () {
    return this[this.length - 1];
};

function DateTime()
{
    
}

DateTime.Now = function ()
{
    return new Date(); //.format("dd/M/yy h:mm tt");
}

function appendBuffer(buffer1, buffer2) {
    var tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
    tmp.set(new Uint8Array(buffer1), 0);
    tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
    return tmp.buffer;
}

function string_of_enum(enu,value) 
{
    for (var k in enu) if (enu[k] == value) return k;
    return null;
}

// Converts an integer (unicode value) to a char
function IntToString(i) {
    return String.fromCharCode(i);
}

function memcpy(dst, dstOffset, src, srcOffset, length) {
    var dstU8 = new Uint8Array(dst, dstOffset, length);
    var srcU8 = new Uint8Array(src, srcOffset, length);
    dstU8.set(srcU8);
};

// Converts a char into to an integer (unicode value)
function StringToInt(a) {
    return a.charCodeAt();
}

function randomString() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}

//dodatne funkcije za string konverziju
function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
}
//function str2ab(str) {
//    var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
//    var bufView = new Uint16Array(buf);
//    for (var i = 0, strLen = str.length; i < strLen; i++) {
//        bufView[i] = str.charCodeAt(i);
//    }
//    return buf;
//}
function str2ab(str) {
    var strLen1 = str.length;

    if (!isEven(strLen1))
        strLen1++;

    var buf = new ArrayBuffer(strLen1); // + 1); 
    var bufView = new Uint8Array(buf);
    for (var i = 0, strLen = strLen1; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    bufView[strLen1 + 1] = 0;
    return buf;
}

function isEven(value) {
    if (value % 2 == 0)
        return true;
    else
        return false;
}

function getQueryString(key)
{
	var locationString = location.search.substr(1);
	var firstPart = locationString.split('&');
	for (value in firstPart){
		var secondPart = firstPart[value].split('=');
		//data[secondPart[0]] = secondPart[1];
		if (secondPart[0] == key){
			secondPart[0] = '';
			return decodeURIComponent(secondPart.join('=').substring(1));
		}
	}
}