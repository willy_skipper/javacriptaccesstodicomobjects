function DcmEncoding()
{

}
		DcmEncoding.EncodingCodePageMap = [];

		// DcmEncoding.DcmEncoding = function() 
		// {
			EncodingCodePageMap = [];
			DcmEncoding.EncodingCodePageMap["ISO_IR 100"] = 28591; // Latin Alphabet No. 1 Unextended
            DcmEncoding.EncodingCodePageMap["ISO_IR 101"] = 28592; // Latin Alphabet No. 2 Unextended
            DcmEncoding.EncodingCodePageMap["ISO_IR 109"] = 28593; // Latin Alphabet No. 3 Unextended
            DcmEncoding.EncodingCodePageMap["ISO_IR 110"] = 28594; // Latin Alphabet No. 4 Unextended
            DcmEncoding.EncodingCodePageMap["ISO_IR 144"] = 28595; // Cyrillic Unextended
            DcmEncoding.EncodingCodePageMap["ISO_IR 127"] = 28596; // Arabic Unextended
            DcmEncoding.EncodingCodePageMap["ISO_IR 126"] = 28597; // Greek Unextended
            DcmEncoding.EncodingCodePageMap["ISO_IR 138"] = 28598; // Hebrew Unextended
            DcmEncoding.EncodingCodePageMap["ISO_IR 148"] = 28599; // Latin Alphabet No. 5 (Turkish) Unextended
            DcmEncoding.EncodingCodePageMap["ISO_IR 13"] = 932; // JIS X 0201 (Shift JIS) Unextended
            DcmEncoding.EncodingCodePageMap["ISO_IR 166"] = 874; // TIS 620-2533 (Thai) Unextended
            DcmEncoding.EncodingCodePageMap["ISO_IR 192"] = 65001; // Unicode in UTF-8
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 6"] = 20127; // ASCII
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 100"] = 28591; // Latin Alphabet No. 1 Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 101"] = 28592; // Latin Alphabet No. 2 Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 109"] = 28593; // Latin Alphabet No. 3 Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 110"] = 28594; // Latin Alphabet No. 4 Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 144"] = 28595; // Cyrillic Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 127"] = 28596; // Arabic Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 126"] = 28597; // Greek Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 138"] = 28598; // Hebrew Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 148"] = 28599; // Latin Alphabet No. 5 (Turkish) Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 13"] = 50222; // JIS X 0201 (Shift JIS) Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 166"] = 874; // TIS 620-2533 (Thai) Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 87"] = 50222; // JIS X 0208 (Kanji) Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 159"] = 50222; // JIS X 0212 (Kanji) Extended
            DcmEncoding.EncodingCodePageMap["ISO 2022 IR 149"] = 20949; // KS X 1001 (Hangul and Hanja) Extended
			DcmEncoding.EncodingCodePageMap["GB18030"] = 54936; // Chinese (Simplified) Extended
		// }

		/// <summary>
		/// Default charset encoding (ISO 2022 IR 6)
		/// </summary>
		DcmEncoding.Default = function()
		{
			return 20127; //Encoding.ASCII;
		}

		DcmEncoding.GetEncodingForSpecificCharacterSet = function(/* string */ encoding) 
		{			
			return EncodingCodePageMap[encoding];

		}

		DcmEncoding.GetSpecificCharacterSetForEncoding = function(/* string */ encoding) 
		{
			return EncodingCodePageMap[encoding];
		}
	