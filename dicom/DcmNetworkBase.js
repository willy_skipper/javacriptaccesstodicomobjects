function DcmDimseProgress()
{
	//#region Private Members
    var _bytesTransfered;
    var _estimatedCommandLength;
    var _estimatedDatasetLength;
	//#endregion

    //#region Public Properties
    this.Started = DateTime.Now();

    this.BytesTransfered = 0;
    //function(value)
    //{
    //    if(value == undefined) { return _bytesTransfered; }
    //    else { _bytesTransfered = value; }
    //}

    this.EstimatedCommandLength = 0;
    //{
    //    if(value == undefined) { return _estimatedCommandLength; }
    //    else { _estimatedCommandLength = value; }
    //}

    this.EstimatedDatasetLength = 0;
    //{
    //    if(value == undefined) { return _estimatedDatasetLength; }
    //    else { _estimatedDatasetLength = value; }
    //}

    this.EstimatedBytesTotal = 0;
    //{
    //    return EstimatedCommandLength + EstimatedDatasetLength; }
    //}

    this.TimeElapsed = function ()
    {
        Math.abs(DateTime.Now() - this.Started);
    }
    //{
    //    return DateTime.Now.Subtract(Started); }
    //}
	//#endregion
}

function DcmDimseInfo()
{
	//#region Members
	/*public DcmCommand*/ this.Command;
	/*public DcmDataset*/ this.Dataset = null;
	/*public ChunkStream*/ this.CommandData;
	/*public ChunkStream*/ this.DatasetData;
	/*public DicomStreamReader*/ this.CommandReader;
	/*public DicomStreamReader*/ this.DatasetReader;
	/*public DcmDimseProgress*/ this.Progress;
	/*public string*/ this.DatasetFile = null;
	/*public FileStream*/ this.DatasetFileStream;
	/*public Stream*/ this.DatasetStream;
	/*public bool*/ this.IsNewDimse;
	//#endregion

	//#region Methods
	//public DcmDimseInfo() 
	//{
		this.Progress = new DcmDimseProgress();
		this.IsNewDimse = true;
	//}

	this.Close = function() 
	{
		this.CloseCommand();
		this.CloseDataset();
	}

	this.CloseCommand = function() 
	{
		this.CommandData = null;
		this.CommandReader = null;
	}

	this.CloseDataset = function() 
	{
//		DatasetStream = null;
//		DatasetData = null;
//		DatasetReader = null;
//		if (DatasetFileStream != null) {
//			DatasetFileStream.Dispose();
//			DatasetFileStream = null;
//		}
	}

	this.Abort = function()
	{
		this.Close();
//		if (DatasetFile != null)
//			if (File.Exists(DatasetFile)) 
//			{
//				try {
//					File.Delete(DatasetFile);
//				} catch 
//				{
//				}
//			}
	}

	this.Dispose = function() 
	{
		this.Abort();
		//GC.SuppressFinalize(this);
	}
	//#endregion
}

function DcmNetworkBase()
{
    //var wsclient = _wsclient;
    /*DcmAssociate*/ var _assoc;
    /*DcmDimseInfo*/ var _dimse;
    var _messageId = 1;
    var _enableStreamParse = false;

    var el = this;

    var parent = null;
    this.Parent = function (value)
    {
        parent = value;
    }

    this.Associate = function()
    {
        return _assoc;
    }

    this.NextMessageID = function ()
    {
        return _messageId++;
    }

    //#region Public Methods
    this.Connect = function (/*string*/ host, /*int*/ port)
    {
        var foo = {};
        foo.function = "connect";
        foo.host = host;
        foo.port = port;
        foo.data = (new Uint8Array(100)).buffer;
		
		console.log("Connect to " + host + " on port " + port);

        wsclient.send(JSON.stringify(foo), { compress: false });

		//_host = host;
        //_port = port;
        //_socketType = type;
        //_stop = false;
        //_isRunning = true;
        //_thread = new Thread(Connect);
        //_thread.IsBackground = true;
        //_thread.Start();

    }

    this.SocketClose = function ()
    {
        var foo = {};
        foo.function = "close";
        wsclient.send(JSON.stringify(foo), { compress: false });
		console.log("SocketClose");
    }

    //#endregion


    function OnReceiveAssociateRequest(/*DcmAssociate*/ association) 
    {
	    var /*DcmAssociateProfile*/ profile = DcmAssociateProfile.Find(association, true);
        profile.Apply(association);
        SendAssociateAccept(association);
    }

    function OnReceiveAssociateAccept(/*DcmAssociate*/ association)
    {
        //if (this.Parent != undefined)
        //{
        parent.OnReceiveAssociateAccept(association);
        //}
    }
	
	function OnReceiveCEchoResponse(/*byte*/ presentationID, /*ushort*/ messageIdRespondedTo, /*DcmStatus*/ status) {
		alert(status.Description);
		SendAbort(/*DcmAbortSource.ServiceProvider, DcmAbortReason.NotSpecified*/);
	}


    function OnReceiveAssociateRequestCEchoService(/*DcmAssociate*/ association) 
    {
		var len = association.GetPresentationContexts().length;        

        for(var i=0; i < len; i++) 
        {
            var pc = association.GetPresentationContexts()[i];
            if(pc == undefined){
                continue;
            }
                            
		    if (pc.AbstractSyntax() == DicomUID.VerificationSOPClass) 
		    {
			    if (pc.HasTransfer(DicomTransferSyntax.ImplicitVRLittleEndian)) {
				    pc.SetResult(DcmPresContextResult.Accept, DicomTransferSyntax.ImplicitVRLittleEndian);
			    }
			    else if (pc.HasTransfer(DicomTransferSyntax.ExplicitVRLittleEndian)) {
				    pc.SetResult(DcmPresContextResult.Accept, DicomTransferSyntax.ExplicitVRLittleEndian);
			    }
			    else {
				    pc.SetResult(DcmPresContextResult.RejectTransferSyntaxesNotSupported);
			    }
		    } else
		    {
			    pc.SetResult(DcmPresContextResult.RejectAbstractSyntaxNotSupported);
		    }
	    }
	    
	    SendAssociateAccept(association);
    }

    var selectedRow=null;
    function OnReceiveCFindResponse(presentationID, messageIdRespondedTo, dataset, status) 
    {
        //if(parent != null)
        //    parent.OnReceiveAssociateAccept(association);


        //Accession Number - 524368 
        //Study Description - 528432 
        //Modality - 524384 
        //Study Date - 524320 
        //Modalities in Study - 524385
        //Patient's Name - 1048592 
        //Study Instance UID - 2097165

        try {
            
            var stPat;
            try{
                stPat = dataset._items[1048592].inherit.GetValueString();
            }
            catch (e) {
                stPat = "";
            }

            var stAcc = dataset._items[524368].inherit.GetValueString();
            var stDescp= dataset._items[528432].inherit.GetValueString();
			var stUid = dataset._items[2097165].inherit.GetValueString();
            var stMod = dataset._items[524385].inherit.GetValue1(0);
            var stDate = dataset._items[524320].inherit.inherit.GetValueString();
			stDate = moment(stDate,"YYYYMMDD").format("DD.MM.YYYY");
			
			//$(".results").append('<a href="#" class="list-group-item">'+stPat+', ' + stDescp + ', ' + stDate + '<span class="badge">'+stMod+'</span></div>');
			
			$(".results").append('<tr class="cfindrow" data-uid="'+stUid+'"><th>'+stPat+'</th> <td>' + stDescp + '</td> <td>' + stDate + '</td> <td>'+stMod+'</td></tr>');
			
			$(".cfindrow").click(function(){
				cmoveuid = $(this).attr('data-uid');
				cmoveuid = cmoveuid.substring(0, cmoveuid.length-1);
				console.log(cmoveuid);
			});

        }
        catch (e)
        {
            console.log(e);
        }
                
    }
	
	function SendRawPDU(/*ArrayBuffer*/ pdu1)
	{	    
	    wsclient.send(pdu1.WritePDU(), { compress: false });	
	}
	
	this.SendAssociateRequest = function(/*DcmAssociate*/ associate) 
	{
		_assoc = associate;
	    //Log.Info("{0} -> Association request:\n{1}", LogID, Associate.ToString());
	    var /*AAssociateRQ*/ pdu = new AAssociateRQ(_assoc);
	    SendRawPDU(pdu.Write());
    }
	
	this.SendReleaseRequest = function() {
		//Log.Info("{0} -> Association release request", LogID);
		var /*AReleaseRQ*/ pdu = new AReleaseRQ();
		SendRawPDU(pdu.Write());
	}

	function SendAssociateAccept(/*DcmAssociate*/ associate) 
	{
		//Log.Info("{0} -> Association accept:\n{1}", LogID, Associate.ToString());
		/*AAssociateAC*/ var pdu = new AAssociateAC(associate);
		SendRawPDU(pdu.Write());
	}

	function SendAbort()
	{
	    //*** abort - radi          
	    var abort = jDataView.createBuffer(
            0x07, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x03, 0x01
        );
	    wsclient.send(abort);
		console.log("SendAbort");
	}

	function OnPreReceiveCStoreRequest(presentationID, messageID, affectedInstance,
			                           priority, moveAE, moveMessageID, fileName) 
	{
        //todo ako bude potrebno
    }

    this.ProcessNextPDU = function(buffer) 
    {
		/*RawPDU*/ var raw = new RawPDU(/*_network*/);
		raw.RawPDUFromBuffer(buffer);

		if (raw.Type() == 0x04) {
			if (_dimse == null) {
				_dimse = new DcmDimseInfo();
			}
		}

		raw.ReadPDU();
		var typ = raw.Type();

		//console.log("------------- PDU Type: " + typ + "----------------");
        //console.log("Bytes received: " + buffer.byteLength);
		//Logger("PDU bytes: " + buffer.byteLength);
		
		switch (typ) 
		{
		case 0x01:
				_assoc = new DcmAssociate();
				/*AAssociateRQ*/ var pdu = new AAssociateRQ(_assoc);
				pdu.Read(raw);	
				_assoc = pdu.GetDcmAssociate();				
				//Log.Info("{0} <- Association request:\n{1}", LogID, Associate.ToString());
				OnReceiveAssociateRequest(_assoc);
				return true;
					
				
		case 0x02: 
		        /*AAssociateAC*/ var pdu = new AAssociateAC(_assoc);
				pdu.Read(raw);
				//Log.Info("{0} <- Association accept:\n{1}", LogID, Associate.ToString());

                //ovo je default, ali treba hedlat i footlat
		        OnReceiveAssociateAccept(_assoc);

				return true;
				
		case 0x03: 
				//AAssociateRJ pdu = new AAssociateRJ();
				//pdu.Read(raw);
				//Log.Info("{0} <- Association reject [result: {1}; source: {2}; reason: {3}]", LogID, pdu.Result, pdu.Source, pdu.Reason);
				//OnReceiveAssociateReject(pdu.Result, pdu.Source, pdu.Reason);

		        var reject = jDataView.createBuffer(
    	            0x03, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x03, 0x01
                    );
		        wsclient.send(reject);
		        console.log("Sending reject");

		        return true;
				
		case 0x04: 
				var pdu = new PDataTF();
				pdu.Read(raw);
				//Log.Debug("{0} <- P-Data-TF", LogID);
				return ProcessPDataTF(pdu); 
				return true;
				
		case 0x05: 
				//AReleaseRQ pdu = new AReleaseRQ();
				//pdu.Read(raw);
				//Log.Info("{0} <- Association release request", LogID);
			    //OnReceiveReleaseRequest();

	    		var release = jDataView.createBuffer(
    	        0x06, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00,0x00,0x00
                );
	    		wsclient.send(release);
	    		Logger("Sending release");

				return true;
				
		case 0x06: 
				//AReleaseRP pdu = new AReleaseRP();
				//pdu.Read(raw);
				//Log.Info("{0} <- Association release response", LogID);
			    //OnReceiveReleaseResponse();

			    //release - radi
			    var release = jDataView.createBuffer(
			    	0x06, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00,0x00,0x00
			    );
			    wsclient.send(release);
			    console.log("Sending release");

				return true;
				
		case 0x07: 
				//AAbort pdu = new AAbort();
				//pdu.Read(raw);
				//Log.Info("{0} <- Association abort: {1} - {2}", LogID, pdu.Source, pdu.Reason);
			    //OnReceiveAbort(pdu.Source, pdu.Reason);

			    //*** abort - radi          
			    var abort = jDataView.createBuffer(
			    	0x07, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01,0x03,0x01
			    );
			    wsclient.send(abort);
			    console.log("Sending Abort");

				return true;
				
		case 0xFF: 
			    return false;
				    
		default:
				console.log("Unknown PDU type");
				
		}


	};
	
    var ff;
    var buff = new ArrayBuffer(0);
    
	function ProcessPDataTF(/*PDataTF*/ pdu) 
    {
	    var /* byte */ pcid = 0;
	    var pdvs = pdu.PDVs();
	    for(var i=0; i < pdvs.length; i++) 
	    {	
	        var pdv = pdvs[i];
	        pcid = pdv.PCID();
	        if (pdv.IsCommand()) 
	        {
	            if (_dimse.CommandData == null)
	            {
	                _dimse.CommandData = new ChunkStream();
	            }

	            _dimse.CommandData.AddChunk(pdv.Value());
				//console.log(ab2str(pdv.Value()));

	            if (_dimse.Command == null) 
	            {
	                _dimse.Command = new DcmCommand();
	            }

	            if (_dimse.CommandReader == null) {
	                _dimse.CommandReader = new DicomStreamReader(_dimse.CommandData.Stream());
	                _dimse.CommandReader.Dataset(_dimse.Command);
	            }

	            _dimse.CommandReader.Read(null, DicomReadOptions.Default);          

	            _dimse.Progress.BytesTransfered += pdv.Value().byteLength;
	            _dimse.Progress.EstimatedCommandLength = _dimse.CommandReader.BytesEstimated();

	            if (pdv.IsLastFragment()) 
	            {
	                _dimse.CloseCommand();

	                var isLast = true;
                    
	                if (_dimse.Command.Contains(DicomTags.DataSetType)) 
	                {
	                    if (_dimse.Command.inherit.GetUInt16(DicomTags.DataSetType, 0x0101) != 0x0101) {
	                        isLast = false;

	                        var /* DcmCommandField */ commandField = _dimse.Command.inherit.GetUInt16(DicomTags.CommandField, 0);

	                        if (commandField == DcmCommandField.CStoreRequest) 
	                        {
	                            //log.value = "CSTORE\n" + log.value ;

	                            var /*ushort*/ messageID = _dimse.Command.inherit.GetUInt16(DicomTags.MessageID, 1);
	                            var /*DcmPriority*/ priority = /*(DcmPriority)*/_dimse.Command.inherit.GetUInt16(DicomTags.Priority, 0);
	                            var /*DicomUID*/ affectedInstance = _dimse.Command.inherit.GetUID(DicomTags.AffectedSOPInstanceUID);
	                            var /*string*/ moveAE = _dimse.Command.inherit.GetString(DicomTags.MoveOriginatorApplicationEntityTitle, null);
	                            var /*ushort*/ moveMessageID = _dimse.Command.inherit.GetUInt16(DicomTags.MoveOriginatorMessageID, 1);
					            
	                            _dimse.DatasetFile = randomString()+".dcm";

	                            if (_dimse.DatasetFile != null)
	                            {
	                                var /*DcmPresContext*/ pres = _assoc.GetPresentationContext(pcid);

	                                /*var DicomFileFormat*/ ff = new DicomFileFormat();
	                                ff.FileMetaInfo().FileMetaInformationVersion(DcmFileMetaInfo.Version);
	                                ff.FileMetaInfo().MediaStorageSOPClassUID(pres.AbstractSyntax());
	                                ff.FileMetaInfo().MediaStorageSOPInstanceUID(affectedInstance);
	                                ff.FileMetaInfo().TransferSyntax(pres.AcceptedTransferSyntax());
	                                ff.FileMetaInfo().ImplementationClassUID(Implementation.ClassUID);
	                                ff.FileMetaInfo().ImplementationVersionName(Implementation.Version);
	                                ff.FileMetaInfo().SourceApplicationEntityTitle(_assoc.CalledAE);
	                                //ff.Save(_dimse.DatasetFile, DicomWriteOptions.Default);

	                                buff = ff.GetFileHeader(DicomWriteOptions.Default);

	                                //_dimse.DatasetFileStream = new FileStream(_dimse.DatasetFile, FileMode.Open);
	                                //_dimse.DatasetFileStream.Seek(0, SeekOrigin.End);
	                                //_dimse.DatasetStream = _dimse.DatasetFileStream;
	                            }
	                        }

	                    }
	                }
					
	                if (isLast) 
	                {
	                    if (_dimse.IsNewDimse)
	                    {
	                        //OnReceiveDimseBegin(pcid, _dimse.Command, _dimse.Dataset, _dimse.Progress);
	                    }
	                    //OnReceiveDimseProgress(pcid, _dimse.Command, _dimse.Dataset, _dimse.Progress);
	                    //OnReceiveDimse(pcid, _dimse.Command, _dimse.Dataset, _dimse.Progress);
	                    ProcessDimse(pcid);
	                    _dimse = null;
	                    return true;
	                }
	            }
	        }
	        else
	        {
	            if (_dimse.DatasetFile != null) 
	            {

	                buff = appendBuffer(buff, pdv.Value());
	            }
	            else 
	            {
	                if (_dimse.DatasetData == null) 
	                {
	                    _dimse.DatasetData = new ChunkStream();
	                    _dimse.DatasetStream = _dimse.DatasetData;
	                }
	                _dimse.DatasetData.AddChunk(pdv.Value());
					//This also shows cfind data
					//console.log(ab2str(pdv.Value()));
	            }

	            if (_dimse.Dataset == null) 
	            {
	                var /*DicomTransferSyntax*/ ts = _assoc.GetAcceptedTransferSyntax(pdv.PCID());
	                _dimse.Dataset = new DcmDataset();
	                _dimse.Dataset.DcmDataset(ts);
	            }

				//*** DATA PROCCESSING *********
	            if ((_enableStreamParse && !_dimse.Dataset.InternalTransferSyntax().IsDeflate) || pdv.IsLastFragment())
	            {
					//***** CTORE FILES are not processed here, see worker
					if (_dimse.DatasetFile != null){
						
						//for files
						
					}
					else //za cfind and rest
					{
					
		                if (_dimse.DatasetReader == null)
		                {
		                   if (_dimse.Dataset.InternalTransferSyntax().IsDeflate) {
		                       //// DicomStreamReader needs a seekable stream
		                       //var /*MemoryStream*/ ms = StreamUtility.Deflate(_dimse.DatasetStream, false);
		                       //_dimse.DatasetReader = new DicomStreamReader(ms);
		                   }
		                   else {
		                       _dimse.DatasetReader = new DicomStreamReader(_dimse.DatasetStream.Stream()/*, true*/); //_dimse.DatasetStream);
		                   }
	
		                   _dimse.DatasetReader.Dataset(_dimse.Dataset,true);
		                }
	
		                _dimse.Progress.BytesTransfered += pdv.Value().byteLength;
	
		                var  remaining = _dimse.DatasetReader.BytesRemaining() + pdv.Value().byteLength;
		                if (remaining >= _dimse.DatasetReader.BytesNeeded() || pdv.IsLastFragment()) 
		                {
		                   if (_dimse.DatasetReader.Read(null, DicomReadOptions.Default) != DicomReadStatus.Success && pdv.IsLastFragment()) 
		                   {
		                       // 
		                   }
	
		                   _dimse.Progress.EstimatedDatasetLength = /*(int)*/_dimse.DatasetReader.BytesEstimated();
		                }
					}
	            }

	            if (pdv.IsLastFragment()) 
	            {

					//save file to FS
					if (_dimse.DatasetFile != null)
					{					
		                //ff.SaveAsync(_dimse.DatasetFile, DicomWriteOptions.Default, buff, byteSum);
	
		                //console.log("Buff byte length = " + buff.byteLength);
						//if(buff != null)
						//	console.log(ab2str(buff));
	
		                //var blob1 = new Blob(new Uint8Array(buff));
						
						try{
		                	worker.postMessage(buff, [buff]); 
							Logger("File received", LOGGER.SUCCESS);
							
							buff = null;	
						}
						catch(err){
							console.log(err);	
						}     
						
					}
	                
	                _dimse.Close();

	                if (_dimse.IsNewDimse)
	                {
	                    //OnReceiveDimseBegin(pcid, _dimse.Command, _dimse.Dataset, _dimse.Progress);
	                }
	                //OnReceiveDimseProgress(pcid, _dimse.Command, _dimse.Dataset, _dimse.Progress);
	                //OnReceiveDimse(pcid, _dimse.Command, _dimse.Dataset, _dimse.Progress);
	                ProcessDimse(pcid);
	                _dimse = null;
	                return true;
	            }
	        }
			

	        if (_dimse.IsNewDimse) 
	        {
	            //OnReceiveDimseBegin(pcid, _dimse.Command, _dimse.Dataset, _dimse.Progress);
	            _dimse.IsNewDimse = false;
	        } 
	        else 
	        {
	            //OnReceiveDimseProgress(pcid, _dimse.Command, _dimse.Dataset, _dimse.Progress);
	        }
			
	    }

	    return true;

	}

	function ProcessDimse(/*byte*/ presentationID) 
	{
		//if (!Associate.HasPresentationContextID(presentationID) ||
		//		Associate.GetPresentationContextResult(presentationID) != DcmPresContextResult.Accept) 
		//{
		//    console.log(LogID + " -> Received DIMSE for unaccepted Presentation Context ID [pcid: "+presentationID+"]");
	    //   // SendAbort(DcmAbortSource.ServiceUser, DcmAbortReason.NotSpecified);
	    //    return true;
	    //}

	    if (_dimse.Command == null) {
	        //console.log(LogID + " -> Unable to process DIMSE; Command DataSet not received.");
	        SendAbort(/*DcmAbortSource.ServiceUser, DcmAbortReason.NotSpecified*/);
	        return true;
	    }

	    var /*DcmCommandField*/ commandField = _dimse.Command.CommandField();

	    if (commandField == DcmCommandField.CStoreRequest) 
	    {
	        var /*ushort*/ messageID = _dimse.Command.MessageID();
	        var priority = _dimse.Command.Priority();
	        var /*DicomUID*/ affectedInstance = _dimse.Command.AffectedSOPInstanceUID();
	        var moveAE = _dimse.Command.MoveOriginatorAE();
	        var moveMessageID = _dimse.Command.MoveOriginatorMessageID();
	        //Log.Info("{0} <- C-Store request [pc: {1}; id: {2}]{3}", 
            //LogID, presentationID, messageID, (_dimse.DatasetFile != null ? " (stream)" : ""));

	        //try {
	        //    OnReceiveCStoreRequest(presentationID, messageID, affectedInstance, priority, moveAE, moveMessageID, _dimse.Dataset, _dimse.DatasetFile);
	        //} finally {
	        //    OnPostReceiveCStoreRequest(presentationID, messageID, affectedInstance, _dimse.Dataset, _dimse.DatasetFile);
	        //}

	        SendCStoreResponse(presentationID, messageID, affectedInstance, DcmStatus.Success);
	        console.log("SendCStoreResponse");

	        return true;
	    }

	    if (commandField == DcmCommandField.CEchoRequest)
	    {
	        var /*ushort*/ messageID = _dimse.Command.MessageID();
	        var /*DcmPriority*/ priority = _dimse.Command.Priority();
	        //Log.Info("{0} <- C-Echo request [pc: {1}; id: {2}]",LogID, presentationID, messageID);
	        //OnReceiveCEchoRequest(presentationID, messageID, priority);
	        SendCEchoResponse(presentationID, messageID, DcmStatus.Success);
	        return true;
	    }

	    if (commandField == DcmCommandField.CEchoResponse)
	    {
	        var /*ushort*/ messageIdRespondedTo = _dimse.Command.MessageIDBeingRespondedTo;
	        var /*DcmStatus*/ status = _dimse.Command.Status();
	        if (status.State == DcmState.Success) {
	            //Log.Info("{0} <- C-Echo response [{1}]: {2}",LogID, messageIdRespondedTo, status);
	        }
	        else {
	            //Log.Info("{0} <- C-Echo response [{1}]: {2}\n\t=> {3}",LogID, messageIdRespondedTo, status, _dimse.Command.GetErrorString());
	        }
	        OnReceiveCEchoResponse(presentationID, messageIdRespondedTo, status);
	        return true;
	    }

	    if (commandField == DcmCommandField.CFindResponse) 
	    {
	        var messageIdRespondedTo = _dimse.Command.MessageIDBeingRespondedTo();
	        var status = _dimse.Command.Status();
	        
	        //Logger("CFindResponse State = " + status.State);

	        if (status.State == DcmState.Success) // || status.State == DcmState.Pending) 
	        {
	            el.SocketClose();
	            //Logger(LogID + " <- C-Find response " + messageIdRespondedTo);
	        }
	        else
	        {
	            //console.log("{0} <- C-Find response [id: {1}]: {2}=>\n\t {3}",
                //    LogID, messageIdRespondedTo, status, _dimse.Command.GetErrorString());
	        }

	        OnReceiveCFindResponse(presentationID, messageIdRespondedTo, _dimse.Dataset, status);
	        return true;
	    }
	    
	    if (commandField == DcmCommandField.CCancelRequest) {
	        //ushort messageIdRespondedTo = _dimse.Command.MessageIDBeingRespondedTo;
	        //Log.Info("{0} <- C-Cancel request [pc: {1}; id: {2}]", LogID, presentationID, messageIdRespondedTo);
	        //OnReceiveCCancelRequest(presentationID, messageIdRespondedTo);
	        return true;
	    }

	    return false;
	}

    //#region Commands
	function SendCStoreResponse( presentationID,  messageIdRespondedTo, affectedInstance, status) 
	{
	    var affectedClass = _assoc.GetAbstractSyntax(presentationID);
	    var command = CreateResponse(messageIdRespondedTo, DcmCommandField.CStoreResponse, affectedClass, status, false);
	    command.AffectedSOPInstanceUID = affectedInstance;	
	    SendDimse(presentationID, command, null);
	    //log.value += "C-STORE RESPONSE\n" + log.value;
    }

	function SendCEchoResponse(/*byte*/ presentationID, /*ushort*/ messageIdRespondedTo, /*DcmStatus*/ status) 
	{
		var /*DicomUID*/ affectedClass = _assoc.GetAbstractSyntax(presentationID);
	    var /*DcmCommand*/ command = CreateResponse(messageIdRespondedTo, DcmCommandField.CEchoResponse, affectedClass, status, false);
	    //Log.Info("{0} -> C-Echo response [id: {1}]", LogID, messageIdRespondedTo);
	    SendDimse(presentationID, command, null);
	}
	
	this.SendCEchoRequest  = function(/*byte*/ presentationID, /*ushort*/ messageID, /*DcmPriority*/ priority) {
		var /*DicomUID*/ affectedClass = _assoc.GetAbstractSyntax(presentationID);
		var /*DcmCommand*/ command = CreateRequest(messageID, DcmCommandField.CEchoRequest, affectedClass, priority, false);
		//Log.Info("{0} -> C-Echo request [pc: {1}; id: {2}]", LogID, presentationID, messageID);
		SendDimse(presentationID, command, null);
	}

	this.SendCFindRequest = function(presentationID, messageID, priority, dataset) 
    {
		var level = dataset.GetString(DicomTags.QueryRetrieveLevel, "UNKNOWN");
	    var /*DicomUID*/ affectedClass = this.Associate().GetAbstractSyntax(presentationID);
	    var /*DcmCommand*/ command = CreateRequest(messageID, DcmCommandField.CFindRequest, affectedClass, priority, true);
	    //Log.Info("{0} -> C-Find request [pc: {1}; id: {2}; lvl: {3}]", LogID, presentationID, messageID, level);
	    SendDimse(presentationID, command, dataset);
	}

	this.SendCMoveRequest = function(presentationID, messageID, destinationAE, priority, dataset) 
	{
		var level = dataset.GetString(DicomTags.QueryRetrieveLevel, "UNKNOWN");
	    var affectedClass = this.Associate().GetAbstractSyntax(presentationID);
	    var command = CreateRequest(messageID, DcmCommandField.CMoveRequest, affectedClass, priority, true);
	    command.MoveDestinationAE(destinationAE);
	    //Log.Info("{0} -> C-Move request [pc: {1}; id: {2}; lvl: {3}; dest: {4}]", LogID, presentationID, messageID, level, destinationAE);
	    SendDimse(presentationID, command, dataset);
    }
    

	function CreateResponse(/*ushort*/ messageIdRespondedTo, /*DcmCommandField*/ commandField, /*DicomUID*/ affectedClass, /*DcmStatus*/ status, /*bool*/ hasDataset) 
    {
		var /*DcmCommand*/ command = new DcmCommand();
	    command.AffectedSOPClassUID(affectedClass);
	    command.CommandField(commandField);
	    command.MessageIDBeingRespondedTo(messageIdRespondedTo);
	    command.HasDataset(hasDataset);
	    command.Status(status);
	    //if (!String.IsNullOrEmpty(status.ErrorComment))
	    //    command.ErrorComment = status.ErrorComment;
	    return command;
	}
    //#endregion

	function CreateRequest(messageID, commandField, affectedClass, priority, hasDataset) 
	{
		var /*DcmCommand*/ command = new DcmCommand();
	    command.AffectedSOPClassUID(affectedClass);
	    command.CommandField(commandField);
	    command.MessageID(messageID);
	    command.Priority(priority);
	    command.HasDataset(hasDataset);

	    return command;
    }


	function SendDimse(/*byte*/ pcid, /*DcmCommand*/ command, /*DcmDataset*/ dataset)
	{
	    _disableTimeout = true;

	    /*DicomTransferSyntax*/ ts = _assoc.GetAcceptedTransferSyntax(pcid);

	    //if (dataset != null && ts != dataset.InternalTransferSyntax) 
	    //{
	    //    if (ts.IsEncapsulated || dataset.InternalTransferSyntax.IsEncapsulated)
	    //    {
	    //        console.log("Unable to transcode encapsulated transfer syntax!");
	    //    }
	    //    dataset.ChangeTransferSyntax(ts, null);
	    //}

	    var progress = new DcmDimseProgress();

	    //progress.EstimatedCommandLength = command.CalculateWriteLength(DicomTransferSyntax.ImplicitVRLittleEndian, DicomWriteOptions.Default | DicomWriteOptions.CalculateGroupLengths);

	    //if (dataset != null)
	    //    progress.EstimatedDatasetLength = dataset.CalculateWriteLength(ts, DicomWriteOptions.Default);

	    var /*PDataTFStream*/ pdustream = new PDataTFStream(wsclient, pcid, /*(int)*/_assoc.MaximumPduLength);
	    
	    //pdustream.OnPduSent += delegate() 
	    //{
	    //    progress.BytesTransfered = pdustream.BytesSent;
	    //    OnSendDimseProgress(pcid, command, dataset, progress);
	    //};

	    //lock (_socket) 
	    //{
	        //OnSendDimseBegin(pcid, command, dataset, progress);

	        var /*DicomStreamWriter*/ dsw = new DicomStreamWriter(pdustream,false);
	        dsw.Write(command, DicomWriteOptions.Default | DicomWriteOptions.CalculateGroupLengths);

	        if (dataset != null)
	        {
	            pdustream.IsCommand(false);
	            dsw.Write(dataset, DicomWriteOptions.Default, true);
	        }

	        // flush last pdu
	        pdustream.Flush(true);

	        //OnSendDimse(pcid, command, dataset, progress);
	    //}

	    return true;

	}

}