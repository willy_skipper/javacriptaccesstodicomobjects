﻿

function DcmAssociateProfile() {

    var _name;
    var _description;
    var _notes;
    var _calledAe;
    var _callingAe;
    var _remoteImplUid;
    var _remoteVersion;
    var /*List<string>*/ _transfer = [];
    var /*List<string>*/ _abstract = [];

    _name = "DICOM Associate Profile";
    _description = String.Empty;
    _notes = String.Empty;
    _calledAe = "*";
    _callingAe = "*";
    _remoteImplUid = "*";
    _remoteVersion = "*";
    //TransferSyntaxes = new List<string>();
    //AbstractSyntaxes = new List<string>();

    this.Name = function(value) {
        if(value == undefined) { return _name; }
        else { _name = value; }
    }

    this.Description = function(value) {
        if(value == undefined) { return _description; }
        else { _description = value; }
    }

    this.Notes = function(value) {
        if(value == undefined) { return _notes; }
        else { _notes = value; }
    }

    this.CalledAE = function(value) {
        if(value == undefined) { return _calledAe; }
        else { _calledAe = value; }
    }

    this.CallingAE  = function(value){
        if(value == undefined) { return _callingAe; }
        else { _callingAe = value; }
    }

    this.RemoteImplUID = function(value) {
        if(value == undefined) { return _remoteImplUid; }
        else { _remoteImplUid = value; }
    }

    this.RemoteVersion  = function(value){
        if(value == undefined) { return _remoteVersion; }
        else { _remoteVersion = value; }
    }

    this.TransferSyntaxes  = function(value){
        if(value == undefined) { return _transfer; }
        else { _transfer = value; }
    }

    this.AbstractSyntaxes = function(value){
        if(value == undefined) { return _abstract; }
        else { _abstract = value; }
    }

    //methods

    this.Supports = function(/*DicomTransferSyntax*/ tx) 
    {
	    return TransferSyntaxes.Contains(tx.UID.UID);
    }

    this.Supports = function(/*DicomUID*/ ax) 
    {
	    return AbstractSyntaxes.Contains(ax.UID);
    }

    this.GetWeight = function(/*bool*/ matchImplementation) 
    {
        var weight = 0;

        if (CalledAE.Contains("*") || CalledAE.Contains("?")){
            weight++;
        }

        if (CallingAE.Contains("*") || CallingAE.Contains("?"))
            weight++;

        if (matchImplementation && RemoteImplUID.Contains("*") || RemoteImplUID.Contains("?"))
            weight++;

        if (matchImplementation && RemoteVersion.Contains("*") || RemoteVersion.Contains("?"))
            weight++;

        return weight;
    }

    this.SetDefault = function() 
    {
        TransferSyntaxes = [];
        TransferSyntaxes.AddRange(DcmAssociateProfile.SupportedTransferSyntaxes);
        AbstractSyntaxes = []; // new List<string>();
        AbstractSyntaxes.AddRange(DcmAssociateProfile.SupportedStorageSyntaxes);
    }

    this.SetDefaultStorage = function() 
    {
        TransferSyntaxes = [];
        TransferSyntaxes.AddRange(DcmAssociateProfile.SupportedTransferSyntaxes);
        AbstractSyntaxes = [];

        for(/*DicomUID*/ _uid in DicomUID.Entries.Values()) 
        {
            var uid = DicomUID.Entries.Values()[_uid];
            if (uid.Description == undefined) { continue; }

            if (uid.Type == DicomUidType.SOPClass){
                SupportedStorageSyntaxes.push(uid.UID);
            }
        }
    }

    this.Apply = function(/*DcmAssociate*/ associate) 
    {
        for(/*DcmPresContext*/ _pc in associate.GetPresentationContexts()) 
        {
            if (isNaN(_pc)) { continue; }
            var pc = associate.GetPresentationContexts()[_pc];
            

            if (pc.Result() == DcmPresContextResult.Proposed) 
            {
                if (AbstractSyntaxes.Contains(pc.AbstractSyntax().UID)) 
                {
                    var /*IList<DicomTransferSyntax>*/ txs = pc.GetTransfers();
                    for (_ts in txs) 
                    {
                        var ts = txs[_ts];
                        if (ts.IsLossy == undefined) { continue; }

                        if (TransferSyntaxes.Contains(ts.UID.UID))
                        {
                            var a = DicomUID.IsImageStorage(pc.AbstractSyntax());
                            var b = DicomTransferSyntax.IsImageCompression(ts);
                            if (!a && b)
                                continue;

                            pc.SetResult(DcmPresContextResult.Accept,ts);
                            break;
                        }
                    }

                    if (pc.Result() != DcmPresContextResult.Accept)
                        pc.SetResult(DcmPresContextResult.RejectTransferSyntaxesNotSupported);
                }
                else
                {

                pc.SetResult(DcmPresContextResult.RejectAbstractSyntaxNotSupported);
                }
            }
        }
    }
}

//public static vars
/*DcmAssociateProfile*/ DcmAssociateProfile.GenericStorage;
/*List<DcmAssociateProfile>*/ DcmAssociateProfile.Profiles = [];
/*List<string>*/ DcmAssociateProfile.SupportedTransferSyntaxes = [];
/*List<string>*/ DcmAssociateProfile.SupportedStorageSyntaxes = [];

DcmAssociateProfile.DcmAssociateProfile = function() 
{
    //DcmAssociateProfile.SupportedTransferSyntaxes = [];
    //transfer sintakse koje prihvacamo
    DcmAssociateProfile.SupportedTransferSyntaxes.push(DicomTransferSyntax.JPEG2000Lossless.UID.UID);
    DcmAssociateProfile.SupportedTransferSyntaxes.push(DicomTransferSyntax.JPEG2000Lossy.UID.UID);    
    DcmAssociateProfile.SupportedTransferSyntaxes.push(DicomTransferSyntax.JPEGProcess14SV1.UID.UID);
    DcmAssociateProfile.SupportedTransferSyntaxes.push(DicomTransferSyntax.RLELossless.UID.UID);
    DcmAssociateProfile.SupportedTransferSyntaxes.push(DicomTransferSyntax.ExplicitVRLittleEndian.UID.UID);
    DcmAssociateProfile.SupportedTransferSyntaxes.push(DicomTransferSyntax.ImplicitVRLittleEndian.UID.UID);

    //DcmAssociateProfile.SupportedStorageSyntaxes = new List<string>();
    DcmAssociateProfile.SupportedStorageSyntaxes.push(DicomUID.VerificationSOPClass.UID);
        
    for (/*DicomUID*/ _uid in DicomUID.Entries.Values()) 
    {
        //var num = parseInt(_uid);
        

        var uid = DicomUID.Entries.Values()[_uid];
        if (uid.Description == undefined) { continue; }

	    if (uid.Description.Contains("Storage"))
        {
	        DcmAssociateProfile.SupportedStorageSyntaxes.push(uid.UID);
	        //console.log(uid.UID);
        }
    }
    

    DcmAssociateProfile.GenericStorage = new DcmAssociateProfile();
    DcmAssociateProfile.GenericStorage.Name("Generic Profile");
    DcmAssociateProfile.GenericStorage.CalledAE("*");
    DcmAssociateProfile.GenericStorage.CallingAE("*");
    DcmAssociateProfile.GenericStorage.RemoteImplUID("*");
    DcmAssociateProfile.GenericStorage.RemoteVersion("*");
    DcmAssociateProfile.GenericStorage.SetDefault();

    //Profiles = new List<DcmAssociateProfile>();
}

DcmAssociateProfile.AddProfile = function(/*DcmAssociateProfile*/ profile) 
{
    if (profile != null) {
        Profiles.push(profile);
    }
}

DcmAssociateProfile.Find = function(/*DcmAssociate*/ associate, /*bool*/ matchImplentation) 
{
    var candidates = [];

    for (/*DcmAssociateProfile*/ _profile in DcmAssociateProfile.Profiles)
    {
        var num = parseInt(_profile);
        if(isNaN(num)){continue;}

        var profile = DicomUID.Entries.Values[num];

        if (!Wildcard.Match(profile.CalledAE(), associate.CalledAE) || !Wildcard.Match(profile.CallingAE, associate.CallingAE))
        {
            continue;
        } 

        if (matchImplentation &&
            (!Wildcard.Match(profile.RemoteImplUID(), associate.ImplementationClass.UID) ||
             !Wildcard.Match(profile.RemoteVersion(), associate.ImplementationVersion)))
        {
            continue;
        }

        candidates.push(profile);

    }

    if (candidates.length > 0) 
    {

        candidates.sort(function(/*DcmAssociateProfile*/ p1, /*DcmAssociateProfile*/ p2) 
        {
            return p1.GetWeight(matchImplentation) - p2.GetWeight(matchImplentation);
        });

        return candidates[0];
    }

    return DcmAssociateProfile.GenericStorage;
}

//inicijalizacija statike
DcmAssociateProfile.DcmAssociateProfile();