﻿function DicomTags()
{

}


		//#region Internal Dictionary Quick Tags
		/// <summary>(0000,0001) VR=UL VM=1 Length to End (Retired)</summary>
		DicomTags.LengthToEndRETIRED = new DicomTag(0x0000, 0x0001);

		/// <summary>(0000,0002) VR=UI VM=1 Affected SOP Class UID</summary>
		DicomTags.AffectedSOPClassUID = new DicomTag(0x0000, 0x0002);

		/// <summary>(0000,0003) VR=UI VM=1 Requested SOP Class UID</summary>
		DicomTags.RequestedSOPClassUID = new DicomTag(0x0000, 0x0003);

		/// <summary>(0000,0010) VR=CS VM=1 Recognition Code (Retired)</summary>
		DicomTags.RecognitionCodeRETIRED = new DicomTag(0x0000, 0x0010);

		/// <summary>(0000,0100) VR=US VM=1 Command Field</summary>
		DicomTags.CommandField = new DicomTag(0x0000, 0x0100);

		/// <summary>(0000,0110) VR=US VM=1 Message ID</summary>
		DicomTags.MessageID = new DicomTag(0x0000, 0x0110);

		/// <summary>(0000,0120) VR=US VM=1 Message ID Being Responded To</summary>
		DicomTags.MessageIDBeingRespondedTo = new DicomTag(0x0000, 0x0120);

		/// <summary>(0000,0200) VR=AE VM=1 Initiator (Retired)</summary>
		DicomTags.InitiatorRETIRED = new DicomTag(0x0000, 0x0200);

		/// <summary>(0000,0300) VR=AE VM=1 Receiver (Retired)</summary>
		DicomTags.ReceiverRETIRED = new DicomTag(0x0000, 0x0300);

		/// <summary>(0000,0400) VR=AE VM=1 Find Location (Retired)</summary>
		DicomTags.FindLocationRETIRED = new DicomTag(0x0000, 0x0400);

		/// <summary>(0000,0600) VR=AE VM=1 Move Destination</summary>
		DicomTags.MoveDestination = new DicomTag(0x0000, 0x0600);

		/// <summary>(0000,0700) VR=US VM=1 Priority</summary>
		DicomTags.Priority = new DicomTag(0x0000, 0x0700);

		/// <summary>(0000,0800) VR=US VM=1 Data Set Type</summary>
		DicomTags.DataSetType = new DicomTag(0x0000, 0x0800);

		/// <summary>(0000,0850) VR=US VM=1 Number of Matches (Retired)</summary>
		DicomTags.NumberOfMatchesRETIRED = new DicomTag(0x0000, 0x0850);

		/// <summary>(0000,0860) VR=US VM=1 Response Sequence Number (Retired)</summary>
		DicomTags.ResponseSequenceNumberRETIRED = new DicomTag(0x0000, 0x0860);

		/// <summary>(0000,0900) VR=US VM=1 Status</summary>
		DicomTags.Status = new DicomTag(0x0000, 0x0900);

		/// <summary>(0000,0901) VR=AT VM=1-n Offending Element</summary>
		DicomTags.OffendingElement = new DicomTag(0x0000, 0x0901);

		/// <summary>(0000,0902) VR=LO VM=1 Error Comment</summary>
		DicomTags.ErrorComment = new DicomTag(0x0000, 0x0902);

		/// <summary>(0000,0903) VR=US VM=1 Error ID</summary>
		DicomTags.ErrorID = new DicomTag(0x0000, 0x0903);

		/// <summary>(0000,1000) VR=UI VM=1 Affected SOP Instance UID</summary>
		DicomTags.AffectedSOPInstanceUID = new DicomTag(0x0000, 0x1000);

		/// <summary>(0000,1001) VR=UI VM=1 Requested SOP Instance UID</summary>
		DicomTags.RequestedSOPInstanceUID = new DicomTag(0x0000, 0x1001);

		/// <summary>(0000,1002) VR=US VM=1 Event Type ID</summary>
		DicomTags.EventTypeID = new DicomTag(0x0000, 0x1002);

		/// <summary>(0000,1005) VR=AT VM=1-n Attribute Identifier List</summary>
		DicomTags.AttributeIdentifierList = new DicomTag(0x0000, 0x1005);

		/// <summary>(0000,1008) VR=US VM=1 Action Type ID</summary>
		DicomTags.ActionTypeID = new DicomTag(0x0000, 0x1008);

		/// <summary>(0000,1020) VR=US VM=1 Number of Remaining Sub-operations</summary>
		DicomTags.NumberOfRemainingSuboperations = new DicomTag(0x0000, 0x1020);

		/// <summary>(0000,1021) VR=US VM=1 Number of Completed Sub-operations</summary>
		DicomTags.NumberOfCompletedSuboperations = new DicomTag(0x0000, 0x1021);

		/// <summary>(0000,1022) VR=US VM=1 Number of Failed Sub-operations</summary>
		DicomTags.NumberOfFailedSuboperations = new DicomTag(0x0000, 0x1022);

		/// <summary>(0000,1023) VR=US VM=1 Number of Warning Sub-operations</summary>
		DicomTags.NumberOfWarningSuboperations = new DicomTag(0x0000, 0x1023);

		/// <summary>(0000,1030) VR=AE VM=1 Move Originator Application Entity Title</summary>
		DicomTags.MoveOriginatorApplicationEntityTitle = new DicomTag(0x0000, 0x1030);

		/// <summary>(0000,1031) VR=US VM=1 Move Originator Message ID</summary>
		DicomTags.MoveOriginatorMessageID = new DicomTag(0x0000, 0x1031);

		/// <summary>(0000,4000) VR=AT VM=1 DIALOG Receiver (Retired)</summary>
		DicomTags.DIALOGReceiverRETIRED = new DicomTag(0x0000, 0x4000);

		/// <summary>(0000,4010) VR=AT VM=1 Terminal Type (Retired)</summary>
		DicomTags.TerminalTypeRETIRED = new DicomTag(0x0000, 0x4010);

		/// <summary>(0000,5010) VR=SH VM=1 Message Set ID (Retired)</summary>
		DicomTags.MessageSetIDRETIRED = new DicomTag(0x0000, 0x5010);

		/// <summary>(0000,5020) VR=SH VM=1 End Message ID (Retired)</summary>
		DicomTags.EndMessageIDRETIRED = new DicomTag(0x0000, 0x5020);

		/// <summary>(0000,5110) VR=AT VM=1 Display Format (Retired)</summary>
		DicomTags.DisplayFormatRETIRED = new DicomTag(0x0000, 0x5110);

		/// <summary>(0000,5120) VR=AT VM=1 Page Position ID (Retired)</summary>
		DicomTags.PagePositionIDRETIRED = new DicomTag(0x0000, 0x5120);

		/// <summary>(0000,5130) VR=CS VM=1 Text Format ID (Retired)</summary>
		DicomTags.TextFormatIDRETIRED = new DicomTag(0x0000, 0x5130);

		/// <summary>(0000,5140) VR=CS VM=1 Nor/Rev (Retired)</summary>
		DicomTags.NorRevRETIRED = new DicomTag(0x0000, 0x5140);

		/// <summary>(0000,5150) VR=CS VM=1 Add Gray Scale (Retired)</summary>
		DicomTags.AddGrayScaleRETIRED = new DicomTag(0x0000, 0x5150);

		/// <summary>(0000,5160) VR=CS VM=1 Borders (Retired)</summary>
		DicomTags.BordersRETIRED = new DicomTag(0x0000, 0x5160);

		/// <summary>(0000,5170) VR=IS VM=1 Copies (Retired)</summary>
		DicomTags.CopiesRETIRED = new DicomTag(0x0000, 0x5170);

		/// <summary>(0000,5180) VR=CS VM=1 Magnification Type (Retired)</summary>
		DicomTags.MagnificationTypeRETIRED = new DicomTag(0x0000, 0x5180);

		/// <summary>(0000,5190) VR=CS VM=1 Erase (Retired)</summary>
		DicomTags.EraseRETIRED = new DicomTag(0x0000, 0x5190);

		/// <summary>(0000,51a0) VR=CS VM=1 Print (Retired)</summary>
		DicomTags.PrintRETIRED = new DicomTag(0x0000, 0x51a0);

		/// <summary>(0000,51b0) VR=US VM=1-n Overlays (Retired)</summary>
		DicomTags.OverlaysRETIRED = new DicomTag(0x0000, 0x51b0);

		/// <summary>(0002,0001) VR=OB VM=1 File Meta Information Version</summary>
		DicomTags.FileMetaInformationVersion = new DicomTag(0x0002, 0x0001);

		/// <summary>(0002,0002) VR=UI VM=1 Media Storage SOP Class UID</summary>
		DicomTags.MediaStorageSOPClassUID = new DicomTag(0x0002, 0x0002);

		/// <summary>(0002,0003) VR=UI VM=1 Media Storage SOP Instance UID</summary>
		DicomTags.MediaStorageSOPInstanceUID = new DicomTag(0x0002, 0x0003);

		/// <summary>(0002,0010) VR=UI VM=1 Transfer Syntax UID</summary>
		DicomTags.TransferSyntaxUID = new DicomTag(0x0002, 0x0010);

		/// <summary>(0002,0012) VR=UI VM=1 Implementation Class UID</summary>
		DicomTags.ImplementationClassUID = new DicomTag(0x0002, 0x0012);

		/// <summary>(0002,0013) VR=SH VM=1 Implementation Version Name</summary>
		DicomTags.ImplementationVersionName = new DicomTag(0x0002, 0x0013);

		/// <summary>(0002,0016) VR=AE VM=1 Source Application Entity Title</summary>
		DicomTags.SourceApplicationEntityTitle = new DicomTag(0x0002, 0x0016);

		/// <summary>(0002,0100) VR=UI VM=1 Private Information Creator UID</summary>
		DicomTags.PrivateInformationCreatorUID = new DicomTag(0x0002, 0x0100);

		/// <summary>(0002,0102) VR=OB VM=1 Private Information</summary>
		DicomTags.PrivateInformation = new DicomTag(0x0002, 0x0102);

		/// <summary>(0004,1130) VR=CS VM=1 File-set ID</summary>
		DicomTags.FilesetID = new DicomTag(0x0004, 0x1130);

		/// <summary>(0004,1141) VR=CS VM=1-8 File-set Descriptor File ID</summary>
		DicomTags.FilesetDescriptorFileID = new DicomTag(0x0004, 0x1141);

		/// <summary>(0004,1142) VR=CS VM=1 Specific Character Set of File-set Descriptor File</summary>
		DicomTags.SpecificCharacterSetOfFilesetDescriptorFile = new DicomTag(0x0004, 0x1142);

		/// <summary>(0004,1200) VR=UL VM=1 Offset of the First Directory Record of the Root Directory Entity</summary>
		DicomTags.OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity = new DicomTag(0x0004, 0x1200);

		/// <summary>(0004,1202) VR=UL VM=1 Offset of the Last Directory Record of the Root Directory Entity</summary>
		DicomTags.OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity = new DicomTag(0x0004, 0x1202);

		/// <summary>(0004,1212) VR=US VM=1 File-set Consistency Flag</summary>
		DicomTags.FilesetConsistencyFlag = new DicomTag(0x0004, 0x1212);

		/// <summary>(0004,1220) VR=SQ VM=1 Directory Record Sequence</summary>
		DicomTags.DirectoryRecordSequence = new DicomTag(0x0004, 0x1220);

		/// <summary>(0004,1400) VR=UL VM=1 Offset of the Next Directory Record</summary>
		DicomTags.OffsetOfTheNextDirectoryRecord = new DicomTag(0x0004, 0x1400);

		/// <summary>(0004,1410) VR=US VM=1 Record In-use Flag</summary>
		DicomTags.RecordInuseFlag = new DicomTag(0x0004, 0x1410);

		/// <summary>(0004,1420) VR=UL VM=1 Offset of Referenced Lower-Level Directory Entity</summary>
		DicomTags.OffsetOfReferencedLowerLevelDirectoryEntity = new DicomTag(0x0004, 0x1420);

		/// <summary>(0004,1430) VR=CS VM=1 Directory Record Type</summary>
		DicomTags.DirectoryRecordType = new DicomTag(0x0004, 0x1430);

		/// <summary>(0004,1432) VR=UI VM=1 Private Record UID</summary>
		DicomTags.PrivateRecordUID = new DicomTag(0x0004, 0x1432);

		/// <summary>(0004,1500) VR=CS VM=1-8 Referenced File ID</summary>
		DicomTags.ReferencedFileID = new DicomTag(0x0004, 0x1500);

		/// <summary>(0004,1504) VR=UL VM=1 MRDR Directory Record Offset (Retired)</summary>
		DicomTags.MRDRDirectoryRecordOffsetRETIRED = new DicomTag(0x0004, 0x1504);

		/// <summary>(0004,1510) VR=UI VM=1 Referenced SOP Class UID in File</summary>
		DicomTags.ReferencedSOPClassUIDInFile = new DicomTag(0x0004, 0x1510);

		/// <summary>(0004,1511) VR=UI VM=1 Referenced SOP Instance UID in File</summary>
		DicomTags.ReferencedSOPInstanceUIDInFile = new DicomTag(0x0004, 0x1511);

		/// <summary>(0004,1512) VR=UI VM=1 Referenced Transfer Syntax UID in File</summary>
		DicomTags.ReferencedTransferSyntaxUIDInFile = new DicomTag(0x0004, 0x1512);

		/// <summary>(0004,151a) VR=UI VM=1-n Referenced Related General SOP Class UID in File</summary>
		DicomTags.ReferencedRelatedGeneralSOPClassUIDInFile = new DicomTag(0x0004, 0x151a);

		/// <summary>(0004,1600) VR=UL VM=1 Number of References (Retired)</summary>
		DicomTags.NumberOfReferencesRETIRED = new DicomTag(0x0004, 0x1600);

		/// <summary>(0008,0001) VR=UL VM=1 Length to End (Retired)</summary>
		DicomTags.LengthToEnd2RETIRED = new DicomTag(0x0008, 0x0001);

		/// <summary>(0008,0005) VR=CS VM=1-n Specific Character Set</summary>
		DicomTags.SpecificCharacterSet = new DicomTag(0x0008, 0x0005);

		/// <summary>(0008,0008) VR=CS VM=2-n Image Type</summary>
		DicomTags.ImageType = new DicomTag(0x0008, 0x0008);

		/// <summary>(0008,0010) VR=CS VM=1 Recognition Code (Retired)</summary>
		DicomTags.RecognitionCode2RETIRED = new DicomTag(0x0008, 0x0010);

		/// <summary>(0008,0012) VR=DA VM=1 Instance Creation Date</summary>
		DicomTags.InstanceCreationDate = new DicomTag(0x0008, 0x0012);

		/// <summary>(0008,0013) VR=TM VM=1 Instance Creation Time</summary>
		DicomTags.InstanceCreationTime = new DicomTag(0x0008, 0x0013);

		/// <summary>(0008,0014) VR=UI VM=1 Instance Creator UID</summary>
		DicomTags.InstanceCreatorUID = new DicomTag(0x0008, 0x0014);

		/// <summary>(0008,0016) VR=UI VM=1 SOP Class UID</summary>
		DicomTags.SOPClassUID = new DicomTag(0x0008, 0x0016);

		/// <summary>(0008,0018) VR=UI VM=1 SOP Instance UID</summary>
		DicomTags.SOPInstanceUID = new DicomTag(0x0008, 0x0018);

		/// <summary>(0008,001a) VR=UI VM=1-n Related General SOP Class UID</summary>
		DicomTags.RelatedGeneralSOPClassUID = new DicomTag(0x0008, 0x001a);

		/// <summary>(0008,001b) VR=UI VM=1 Original Specialized SOP Class UID</summary>
		DicomTags.OriginalSpecializedSOPClassUID = new DicomTag(0x0008, 0x001b);

		/// <summary>(0008,0020) VR=DA VM=1 Study Date</summary>
		DicomTags.StudyDate = new DicomTag(0x0008, 0x0020);

		/// <summary>(0008,0021) VR=DA VM=1 Series Date</summary>
		DicomTags.SeriesDate = new DicomTag(0x0008, 0x0021);

		/// <summary>(0008,0022) VR=DA VM=1 Acquisition Date</summary>
		DicomTags.AcquisitionDate = new DicomTag(0x0008, 0x0022);

		/// <summary>(0008,0023) VR=DA VM=1 Content Date</summary>
		DicomTags.ContentDate = new DicomTag(0x0008, 0x0023);

		/// <summary>(0008,0024) VR=DA VM=1 Overlay Date (Retired)</summary>
		DicomTags.OverlayDateRETIRED = new DicomTag(0x0008, 0x0024);

		/// <summary>(0008,0025) VR=DA VM=1 Curve Date (Retired)</summary>
		DicomTags.CurveDateRETIRED = new DicomTag(0x0008, 0x0025);

		/// <summary>(0008,002a) VR=DT VM=1 Acquisition DateTime</summary>
		DicomTags.AcquisitionDateTime = new DicomTag(0x0008, 0x002a);

		/// <summary>(0008,0030) VR=TM VM=1 Study Time</summary>
		DicomTags.StudyTime = new DicomTag(0x0008, 0x0030);

		/// <summary>(0008,0031) VR=TM VM=1 Series Time</summary>
		DicomTags.SeriesTime = new DicomTag(0x0008, 0x0031);

		/// <summary>(0008,0032) VR=TM VM=1 Acquisition Time</summary>
		DicomTags.AcquisitionTime = new DicomTag(0x0008, 0x0032);

		/// <summary>(0008,0033) VR=TM VM=1 Content Time</summary>
		DicomTags.ContentTime = new DicomTag(0x0008, 0x0033);

		/// <summary>(0008,0034) VR=TM VM=1 Overlay Time (Retired)</summary>
		DicomTags.OverlayTimeRETIRED = new DicomTag(0x0008, 0x0034);

		/// <summary>(0008,0035) VR=TM VM=1 Curve Time (Retired)</summary>
		DicomTags.CurveTimeRETIRED = new DicomTag(0x0008, 0x0035);

		/// <summary>(0008,0040) VR=US VM=1 Data Set Type (Retired)</summary>
		DicomTags.DataSetTypeRETIRED = new DicomTag(0x0008, 0x0040);

		/// <summary>(0008,0041) VR=LO VM=1 Data Set Subtype (Retired)</summary>
		DicomTags.DataSetSubtypeRETIRED = new DicomTag(0x0008, 0x0041);

		/// <summary>(0008,0042) VR=CS VM=1 Nuclear Medicine Series Type (Retired)</summary>
		DicomTags.NuclearMedicineSeriesTypeRETIRED = new DicomTag(0x0008, 0x0042);

		/// <summary>(0008,0050) VR=SH VM=1 Accession Number</summary>
		DicomTags.AccessionNumber = new DicomTag(0x0008, 0x0050);

		/// <summary>(0008,0052) VR=CS VM=1 Query/Retrieve Level</summary>
		DicomTags.QueryRetrieveLevel = new DicomTag(0x0008, 0x0052);

		/// <summary>(0008,0054) VR=AE VM=1-n Retrieve AE Title</summary>
		DicomTags.RetrieveAETitle = new DicomTag(0x0008, 0x0054);

		/// <summary>(0008,0056) VR=CS VM=1 Instance Availability</summary>
		DicomTags.InstanceAvailability = new DicomTag(0x0008, 0x0056);

		/// <summary>(0008,0058) VR=UI VM=1-n Failed SOP Instance UID List</summary>
		DicomTags.FailedSOPInstanceUIDList = new DicomTag(0x0008, 0x0058);

		/// <summary>(0008,0060) VR=CS VM=1 Modality</summary>
		DicomTags.Modality = new DicomTag(0x0008, 0x0060);

		/// <summary>(0008,0061) VR=CS VM=1-n Modalities in Study</summary>
		DicomTags.ModalitiesInStudy = new DicomTag(0x0008, 0x0061);

		/// <summary>(0008,0062) VR=UI VM=1-n SOP Classes in Study</summary>
		DicomTags.SOPClassesInStudy = new DicomTag(0x0008, 0x0062);

		/// <summary>(0008,0064) VR=CS VM=1 Conversion Type</summary>
		DicomTags.ConversionType = new DicomTag(0x0008, 0x0064);

		/// <summary>(0008,0068) VR=CS VM=1 Presentation Intent Type</summary>
		DicomTags.PresentationIntentType = new DicomTag(0x0008, 0x0068);

		/// <summary>(0008,0070) VR=LO VM=1 Manufacturer</summary>
		DicomTags.Manufacturer = new DicomTag(0x0008, 0x0070);

		/// <summary>(0008,0080) VR=LO VM=1 Institution Name</summary>
		DicomTags.InstitutionName = new DicomTag(0x0008, 0x0080);

		/// <summary>(0008,0081) VR=ST VM=1 Institution Address</summary>
		DicomTags.InstitutionAddress = new DicomTag(0x0008, 0x0081);

		/// <summary>(0008,0082) VR=SQ VM=1 Institution Code Sequence</summary>
		DicomTags.InstitutionCodeSequence = new DicomTag(0x0008, 0x0082);

		/// <summary>(0008,0090) VR=PN VM=1 Referring Physician's Name</summary>
		DicomTags.ReferringPhysiciansName = new DicomTag(0x0008, 0x0090);

		/// <summary>(0008,0092) VR=ST VM=1 Referring Physician's Address</summary>
		DicomTags.ReferringPhysiciansAddress = new DicomTag(0x0008, 0x0092);

		/// <summary>(0008,0094) VR=SH VM=1-n Referring Physician's Telephone Numbers</summary>
		DicomTags.ReferringPhysiciansTelephoneNumbers = new DicomTag(0x0008, 0x0094);

		/// <summary>(0008,0096) VR=SQ VM=1 Referring Physician Identification Sequence</summary>
		DicomTags.ReferringPhysicianIdentificationSequence = new DicomTag(0x0008, 0x0096);

		/// <summary>(0008,0100) VR=SH VM=1 Code Value</summary>
		DicomTags.CodeValue = new DicomTag(0x0008, 0x0100);

		/// <summary>(0008,0102) VR=SH VM=1 Coding Scheme Designator</summary>
		DicomTags.CodingSchemeDesignator = new DicomTag(0x0008, 0x0102);

		/// <summary>(0008,0103) VR=SH VM=1 Coding Scheme Version</summary>
		DicomTags.CodingSchemeVersion = new DicomTag(0x0008, 0x0103);

		/// <summary>(0008,0104) VR=LO VM=1 Code Meaning</summary>
		DicomTags.CodeMeaning = new DicomTag(0x0008, 0x0104);

		/// <summary>(0008,0105) VR=CS VM=1 Mapping Resource</summary>
		DicomTags.MappingResource = new DicomTag(0x0008, 0x0105);

		/// <summary>(0008,0106) VR=DT VM=1 Context Group Version</summary>
		DicomTags.ContextGroupVersion = new DicomTag(0x0008, 0x0106);

		/// <summary>(0008,0107) VR=DT VM=1 Context Group Local Version</summary>
		DicomTags.ContextGroupLocalVersion = new DicomTag(0x0008, 0x0107);

		/// <summary>(0008,010b) VR=CS VM=1 Context Group Extension Flag</summary>
		DicomTags.ContextGroupExtensionFlag = new DicomTag(0x0008, 0x010b);

		/// <summary>(0008,010c) VR=UI VM=1 Coding Scheme UID</summary>
		DicomTags.CodingSchemeUID = new DicomTag(0x0008, 0x010c);

		/// <summary>(0008,010d) VR=UI VM=1 Context Group Extension Creator UID</summary>
		DicomTags.ContextGroupExtensionCreatorUID = new DicomTag(0x0008, 0x010d);

		/// <summary>(0008,010f) VR=CS VM=1 Context Identifier</summary>
		DicomTags.ContextIdentifier = new DicomTag(0x0008, 0x010f);

		/// <summary>(0008,0110) VR=SQ VM=1 Coding Scheme Identification Sequence</summary>
		DicomTags.CodingSchemeIdentificationSequence = new DicomTag(0x0008, 0x0110);

		/// <summary>(0008,0112) VR=LO VM=1 Coding Scheme Registry</summary>
		DicomTags.CodingSchemeRegistry = new DicomTag(0x0008, 0x0112);

		/// <summary>(0008,0114) VR=ST VM=1 Coding Scheme External ID</summary>
		DicomTags.CodingSchemeExternalID = new DicomTag(0x0008, 0x0114);

		/// <summary>(0008,0115) VR=ST VM=1 Coding Scheme Name</summary>
		DicomTags.CodingSchemeName = new DicomTag(0x0008, 0x0115);

		/// <summary>(0008,0116) VR=ST VM=1 Coding Scheme Responsible Organization</summary>
		DicomTags.CodingSchemeResponsibleOrganization = new DicomTag(0x0008, 0x0116);

		/// <summary>(0008,0201) VR=SH VM=1 Timezone Offset From UTC</summary>
		DicomTags.TimezoneOffsetFromUTC = new DicomTag(0x0008, 0x0201);

		/// <summary>(0008,1000) VR=AE VM=1 Network ID (Retired)</summary>
		DicomTags.NetworkIDRETIRED = new DicomTag(0x0008, 0x1000);

		/// <summary>(0008,1010) VR=SH VM=1 Station Name</summary>
		DicomTags.StationName = new DicomTag(0x0008, 0x1010);

		/// <summary>(0008,1030) VR=LO VM=1 Study Description</summary>
		DicomTags.StudyDescription = new DicomTag(0x0008, 0x1030);

		/// <summary>(0008,1032) VR=SQ VM=1 Procedure Code Sequence</summary>
		DicomTags.ProcedureCodeSequence = new DicomTag(0x0008, 0x1032);

		/// <summary>(0008,103e) VR=LO VM=1 Series Description</summary>
		DicomTags.SeriesDescription = new DicomTag(0x0008, 0x103e);

		/// <summary>(0008,1040) VR=LO VM=1 Institutional Department Name</summary>
		DicomTags.InstitutionalDepartmentName = new DicomTag(0x0008, 0x1040);

		/// <summary>(0008,1048) VR=PN VM=1-n Physician(s) of Record</summary>
		DicomTags.PhysiciansOfRecord = new DicomTag(0x0008, 0x1048);

		/// <summary>(0008,1049) VR=SQ VM=1 Physician(s) of Record Identification Sequence</summary>
		DicomTags.PhysiciansOfRecordIdentificationSequence = new DicomTag(0x0008, 0x1049);

		/// <summary>(0008,1050) VR=PN VM=1-n Performing Physician's Name</summary>
		DicomTags.PerformingPhysiciansName = new DicomTag(0x0008, 0x1050);

		/// <summary>(0008,1052) VR=SQ VM=1 Performing Physician Identification Sequence</summary>
		DicomTags.PerformingPhysicianIdentificationSequence = new DicomTag(0x0008, 0x1052);

		/// <summary>(0008,1060) VR=PN VM=1-n Name of Physician(s) Reading Study</summary>
		DicomTags.NameOfPhysiciansReadingStudy = new DicomTag(0x0008, 0x1060);

		/// <summary>(0008,1062) VR=SQ VM=1 Physician(s) Reading Study Identification Sequence</summary>
		DicomTags.PhysiciansReadingStudyIdentificationSequence = new DicomTag(0x0008, 0x1062);

		/// <summary>(0008,1070) VR=PN VM=1-n Operators' Name</summary>
		DicomTags.OperatorsName = new DicomTag(0x0008, 0x1070);

		/// <summary>(0008,1072) VR=SQ VM=1 Operator Identification Sequence</summary>
		DicomTags.OperatorIdentificationSequence = new DicomTag(0x0008, 0x1072);

		/// <summary>(0008,1080) VR=LO VM=1-n Admitting Diagnoses Description</summary>
		DicomTags.AdmittingDiagnosesDescription = new DicomTag(0x0008, 0x1080);

		/// <summary>(0008,1084) VR=SQ VM=1 Admitting Diagnoses Code Sequence</summary>
		DicomTags.AdmittingDiagnosesCodeSequence = new DicomTag(0x0008, 0x1084);

		/// <summary>(0008,1090) VR=LO VM=1 Manufacturer's Model Name</summary>
		DicomTags.ManufacturersModelName = new DicomTag(0x0008, 0x1090);

		/// <summary>(0008,1100) VR=SQ VM=1 Referenced Results Sequence (Retired)</summary>
		DicomTags.ReferencedResultsSequenceRETIRED = new DicomTag(0x0008, 0x1100);

		/// <summary>(0008,1110) VR=SQ VM=1 Referenced Study Sequence</summary>
		DicomTags.ReferencedStudySequence = new DicomTag(0x0008, 0x1110);

		/// <summary>(0008,1111) VR=SQ VM=1 Referenced Performed Procedure Step Sequence</summary>
		DicomTags.ReferencedPerformedProcedureStepSequence = new DicomTag(0x0008, 0x1111);

		/// <summary>(0008,1115) VR=SQ VM=1 Referenced Series Sequence</summary>
		DicomTags.ReferencedSeriesSequence = new DicomTag(0x0008, 0x1115);

		/// <summary>(0008,1120) VR=SQ VM=1 Referenced Patient Sequence</summary>
		DicomTags.ReferencedPatientSequence = new DicomTag(0x0008, 0x1120);

		/// <summary>(0008,1125) VR=SQ VM=1 Referenced Visit Sequence</summary>
		DicomTags.ReferencedVisitSequence = new DicomTag(0x0008, 0x1125);

		/// <summary>(0008,1130) VR=SQ VM=1 Referenced Overlay Sequence (Retired)</summary>
		DicomTags.ReferencedOverlaySequenceRETIRED = new DicomTag(0x0008, 0x1130);

		/// <summary>(0008,113a) VR=SQ VM=1 Referenced Waveform Sequence</summary>
		DicomTags.ReferencedWaveformSequence = new DicomTag(0x0008, 0x113a);

		/// <summary>(0008,1140) VR=SQ VM=1 Referenced Image Sequence</summary>
		DicomTags.ReferencedImageSequence = new DicomTag(0x0008, 0x1140);

		/// <summary>(0008,1145) VR=SQ VM=1 Referenced Curve Sequence (Retired)</summary>
		DicomTags.ReferencedCurveSequenceRETIRED = new DicomTag(0x0008, 0x1145);

		/// <summary>(0008,114a) VR=SQ VM=1 Referenced Instance Sequence</summary>
		DicomTags.ReferencedInstanceSequence = new DicomTag(0x0008, 0x114a);

		/// <summary>(0008,114b) VR=SQ VM=1 Referenced Real World Value Mapping Instance Sequence</summary>
		DicomTags.ReferencedRealWorldValueMappingInstanceSequence = new DicomTag(0x0008, 0x114b);

		/// <summary>(0008,1150) VR=UI VM=1 Referenced SOP Class UID</summary>
		DicomTags.ReferencedSOPClassUID = new DicomTag(0x0008, 0x1150);

		/// <summary>(0008,1155) VR=UI VM=1 Referenced SOP Instance UID</summary>
		DicomTags.ReferencedSOPInstanceUID = new DicomTag(0x0008, 0x1155);

		/// <summary>(0008,115a) VR=UI VM=1-n SOP Classes Supported</summary>
		DicomTags.SOPClassesSupported = new DicomTag(0x0008, 0x115a);

		/// <summary>(0008,1160) VR=IS VM=1-n Referenced Frame Number</summary>
		DicomTags.ReferencedFrameNumber = new DicomTag(0x0008, 0x1160);

		/// <summary>(0008,1195) VR=UI VM=1 Transaction UID</summary>
		DicomTags.TransactionUID = new DicomTag(0x0008, 0x1195);

		/// <summary>(0008,1197) VR=US VM=1 Failure Reason</summary>
		DicomTags.FailureReason = new DicomTag(0x0008, 0x1197);

		/// <summary>(0008,1198) VR=SQ VM=1 Failed SOP Sequence</summary>
		DicomTags.FailedSOPSequence = new DicomTag(0x0008, 0x1198);

		/// <summary>(0008,1199) VR=SQ VM=1 Referenced SOP Sequence</summary>
		DicomTags.ReferencedSOPSequence = new DicomTag(0x0008, 0x1199);

		/// <summary>(0008,1200) VR=SQ VM=1 Studies Containing Other Referenced Instances Sequence</summary>
		DicomTags.StudiesContainingOtherReferencedInstancesSequence = new DicomTag(0x0008, 0x1200);

		/// <summary>(0008,1250) VR=SQ VM=1 Related Series Sequence</summary>
		DicomTags.RelatedSeriesSequence = new DicomTag(0x0008, 0x1250);

		/// <summary>(0008,2110) VR=CS VM=1 Lossy Image Compression (Retired) (Retired)</summary>
		DicomTags.LossyImageCompressionRetiredRETIRED = new DicomTag(0x0008, 0x2110);

		/// <summary>(0008,2111) VR=ST VM=1 Derivation Description</summary>
		DicomTags.DerivationDescription = new DicomTag(0x0008, 0x2111);

		/// <summary>(0008,2112) VR=SQ VM=1 Source Image Sequence</summary>
		DicomTags.SourceImageSequence = new DicomTag(0x0008, 0x2112);

		/// <summary>(0008,2120) VR=SH VM=1 Stage Name</summary>
		DicomTags.StageName = new DicomTag(0x0008, 0x2120);

		/// <summary>(0008,2122) VR=IS VM=1 Stage Number</summary>
		DicomTags.StageNumber = new DicomTag(0x0008, 0x2122);

		/// <summary>(0008,2124) VR=IS VM=1 Number of Stages</summary>
		DicomTags.NumberOfStages = new DicomTag(0x0008, 0x2124);

		/// <summary>(0008,2127) VR=SH VM=1 View Name</summary>
		DicomTags.ViewName = new DicomTag(0x0008, 0x2127);

		/// <summary>(0008,2128) VR=IS VM=1 View Number</summary>
		DicomTags.ViewNumber = new DicomTag(0x0008, 0x2128);

		/// <summary>(0008,2129) VR=IS VM=1 Number of Event Timers</summary>
		DicomTags.NumberOfEventTimers = new DicomTag(0x0008, 0x2129);

		/// <summary>(0008,212a) VR=IS VM=1 Number of Views in Stage</summary>
		DicomTags.NumberOfViewsInStage = new DicomTag(0x0008, 0x212a);

		/// <summary>(0008,2130) VR=DS VM=1-n Event Elapsed Time(s)</summary>
		DicomTags.EventElapsedTimes = new DicomTag(0x0008, 0x2130);

		/// <summary>(0008,2132) VR=LO VM=1-n Event Timer Name(s)</summary>
		DicomTags.EventTimerNames = new DicomTag(0x0008, 0x2132);

		/// <summary>(0008,2142) VR=IS VM=1 Start Trim</summary>
		DicomTags.StartTrim = new DicomTag(0x0008, 0x2142);

		/// <summary>(0008,2143) VR=IS VM=1 Stop Trim</summary>
		DicomTags.StopTrim = new DicomTag(0x0008, 0x2143);

		/// <summary>(0008,2144) VR=IS VM=1 Recommended Display Frame Rate</summary>
		DicomTags.RecommendedDisplayFrameRate = new DicomTag(0x0008, 0x2144);

		/// <summary>(0008,2200) VR=CS VM=1 Transducer Position (Retired)</summary>
		DicomTags.TransducerPositionRETIRED = new DicomTag(0x0008, 0x2200);

		/// <summary>(0008,2204) VR=CS VM=1 Transducer Orientation (Retired)</summary>
		DicomTags.TransducerOrientationRETIRED = new DicomTag(0x0008, 0x2204);

		/// <summary>(0008,2208) VR=CS VM=1 Anatomic Structure (Retired)</summary>
		DicomTags.AnatomicStructureRETIRED = new DicomTag(0x0008, 0x2208);

		/// <summary>(0008,2218) VR=SQ VM=1 Anatomic Region Sequence</summary>
		DicomTags.AnatomicRegionSequence = new DicomTag(0x0008, 0x2218);

		/// <summary>(0008,2220) VR=SQ VM=1 Anatomic Region Modifier Sequence</summary>
		DicomTags.AnatomicRegionModifierSequence = new DicomTag(0x0008, 0x2220);

		/// <summary>(0008,2228) VR=SQ VM=1 Primary Anatomic Structure Sequence</summary>
		DicomTags.PrimaryAnatomicStructureSequence = new DicomTag(0x0008, 0x2228);

		/// <summary>(0008,2229) VR=SQ VM=1 Anatomic Structure, Space or Region Sequence</summary>
		DicomTags.AnatomicStructureSpaceOrRegionSequence = new DicomTag(0x0008, 0x2229);

		/// <summary>(0008,2230) VR=SQ VM=1 Primary Anatomic Structure Modifier Sequence</summary>
		DicomTags.PrimaryAnatomicStructureModifierSequence = new DicomTag(0x0008, 0x2230);

		/// <summary>(0008,2240) VR=SQ VM=1 Transducer Position Sequence (Retired)</summary>
		DicomTags.TransducerPositionSequenceRETIRED = new DicomTag(0x0008, 0x2240);

		/// <summary>(0008,2242) VR=SQ VM=1 Transducer Position Modifier Sequence (Retired)</summary>
		DicomTags.TransducerPositionModifierSequenceRETIRED = new DicomTag(0x0008, 0x2242);

		/// <summary>(0008,2244) VR=SQ VM=1 Transducer Orientation Sequence (Retired)</summary>
		DicomTags.TransducerOrientationSequenceRETIRED = new DicomTag(0x0008, 0x2244);

		/// <summary>(0008,2246) VR=SQ VM=1 Transducer Orientation Modifier Sequence (Retired)</summary>
		DicomTags.TransducerOrientationModifierSequenceRETIRED = new DicomTag(0x0008, 0x2246);

		/// <summary>(0008,2251) VR=SQ VM=1 Anatomic Structure Space Or Region Code Sequence (Trial) (Retired)</summary>
		DicomTags.AnatomicStructureSpaceOrRegionCodeSequenceTrialRETIRED = new DicomTag(0x0008, 0x2251);

		/// <summary>(0008,2253) VR=SQ VM=1 Anatomic Portal Of Entrance Code Sequence (Trial) (Retired)</summary>
		DicomTags.AnatomicPortalOfEntranceCodeSequenceTrialRETIRED = new DicomTag(0x0008, 0x2253);

		/// <summary>(0008,2255) VR=SQ VM=1 Anatomic Approach Direction Code Sequence (Trial) (Retired)</summary>
		DicomTags.AnatomicApproachDirectionCodeSequenceTrialRETIRED = new DicomTag(0x0008, 0x2255);

		/// <summary>(0008,2256) VR=ST VM=1 Anatomic Perspective Description (Trial) (Retired)</summary>
		DicomTags.AnatomicPerspectiveDescriptionTrialRETIRED = new DicomTag(0x0008, 0x2256);

		/// <summary>(0008,2257) VR=SQ VM=1 Anatomic Perspective Code Sequence (Trial) (Retired)</summary>
		DicomTags.AnatomicPerspectiveCodeSequenceTrialRETIRED = new DicomTag(0x0008, 0x2257);

		/// <summary>(0008,2258) VR=ST VM=1 Anatomic Location Of Examining Instrument Description (Trial) (Retired)</summary>
		DicomTags.AnatomicLocationOfExaminingInstrumentDescriptionTrialRETIRED = new DicomTag(0x0008, 0x2258);

		/// <summary>(0008,2259) VR=SQ VM=1 Anatomic Location Of Examining Instrument Code Sequence (Trial) (Retired)</summary>
		DicomTags.AnatomicLocationOfExaminingInstrumentCodeSequenceTrialRETIRED = new DicomTag(0x0008, 0x2259);

		/// <summary>(0008,225a) VR=SQ VM=1 Anatomic Structure Space Or Region Modifier Code Sequence (Trial) (Retired)</summary>
		DicomTags.AnatomicStructureSpaceOrRegionModifierCodeSequenceTrialRETIRED = new DicomTag(0x0008, 0x225a);

		/// <summary>(0008,225c) VR=SQ VM=1 OnAxis Background Anatomic Structure Code Sequence (Trial) (Retired)</summary>
		DicomTags.OnAxisBackgroundAnatomicStructureCodeSequenceTrialRETIRED = new DicomTag(0x0008, 0x225c);

		/// <summary>(0008,3001) VR=SQ VM=1 Alternate Representation Sequence</summary>
		DicomTags.AlternateRepresentationSequence = new DicomTag(0x0008, 0x3001);

		/// <summary>(0008,3010) VR=UI VM=1 Irradiation Event UID</summary>
		DicomTags.IrradiationEventUID = new DicomTag(0x0008, 0x3010);

		/// <summary>(0008,4000) VR=LT VM=1 Identifying Comments (Retired)</summary>
		DicomTags.IdentifyingCommentsRETIRED = new DicomTag(0x0008, 0x4000);

		/// <summary>(0008,9007) VR=CS VM=4 Frame Type</summary>
		DicomTags.FrameType = new DicomTag(0x0008, 0x9007);

		/// <summary>(0008,9092) VR=SQ VM=1 Referenced Image Evidence Sequence</summary>
		DicomTags.ReferencedImageEvidenceSequence = new DicomTag(0x0008, 0x9092);

		/// <summary>(0008,9121) VR=SQ VM=1 Referenced Raw Data Sequence</summary>
		DicomTags.ReferencedRawDataSequence = new DicomTag(0x0008, 0x9121);

		/// <summary>(0008,9123) VR=UI VM=1 Creator-Version UID</summary>
		DicomTags.CreatorVersionUID = new DicomTag(0x0008, 0x9123);

		/// <summary>(0008,9124) VR=SQ VM=1 Derivation Image Sequence</summary>
		DicomTags.DerivationImageSequence = new DicomTag(0x0008, 0x9124);

		/// <summary>(0008,9154) VR=SQ VM=1 Source Image Evidence Sequence</summary>
		DicomTags.SourceImageEvidenceSequence = new DicomTag(0x0008, 0x9154);

		/// <summary>(0008,9205) VR=CS VM=1 Pixel Presentation</summary>
		DicomTags.PixelPresentation = new DicomTag(0x0008, 0x9205);

		/// <summary>(0008,9206) VR=CS VM=1 Volumetric Properties</summary>
		DicomTags.VolumetricProperties = new DicomTag(0x0008, 0x9206);

		/// <summary>(0008,9207) VR=CS VM=1 Volume Based Calculation Technique</summary>
		DicomTags.VolumeBasedCalculationTechnique = new DicomTag(0x0008, 0x9207);

		/// <summary>(0008,9208) VR=CS VM=1 Complex Image Component</summary>
		DicomTags.ComplexImageComponent = new DicomTag(0x0008, 0x9208);

		/// <summary>(0008,9209) VR=CS VM=1 Acquisition Contrast</summary>
		DicomTags.AcquisitionContrast = new DicomTag(0x0008, 0x9209);

		/// <summary>(0008,9215) VR=SQ VM=1 Derivation Code Sequence</summary>
		DicomTags.DerivationCodeSequence = new DicomTag(0x0008, 0x9215);

		/// <summary>(0008,9237) VR=SQ VM=1 Referenced Grayscale Presentation State Sequence</summary>
		DicomTags.ReferencedGrayscalePresentationStateSequence = new DicomTag(0x0008, 0x9237);

		/// <summary>(0008,9410) VR=SQ VM=1 Referenced Other Plane Sequence</summary>
		DicomTags.ReferencedOtherPlaneSequence = new DicomTag(0x0008, 0x9410);

		/// <summary>(0008,9458) VR=SQ VM=1 Frame Display Sequence</summary>
		DicomTags.FrameDisplaySequence = new DicomTag(0x0008, 0x9458);

		/// <summary>(0008,9459) VR=FL VM=1 Recommended Display Frame Rate in Float</summary>
		DicomTags.RecommendedDisplayFrameRateInFloat = new DicomTag(0x0008, 0x9459);

		/// <summary>(0008,9460) VR=CS VM=1 Skip Frame Range Flag</summary>
		DicomTags.SkipFrameRangeFlag = new DicomTag(0x0008, 0x9460);

		/// <summary>(0010,0010) VR=PN VM=1 Patient's Name</summary>
		DicomTags.PatientsName = new DicomTag(0x0010, 0x0010);

		/// <summary>(0010,0020) VR=LO VM=1 Patient ID</summary>
		DicomTags.PatientID = new DicomTag(0x0010, 0x0020);

		/// <summary>(0010,0021) VR=LO VM=1 Issuer of Patient ID</summary>
		DicomTags.IssuerOfPatientID = new DicomTag(0x0010, 0x0021);

		/// <summary>(0010,0022) VR=CS VM=1 Type of Patient ID</summary>
		DicomTags.TypeOfPatientID = new DicomTag(0x0010, 0x0022);

		/// <summary>(0010,0030) VR=DA VM=1 Patient's Birth Date</summary>
		DicomTags.PatientsBirthDate = new DicomTag(0x0010, 0x0030);

		/// <summary>(0010,0032) VR=TM VM=1 Patient's Birth Time</summary>
		DicomTags.PatientsBirthTime = new DicomTag(0x0010, 0x0032);

		/// <summary>(0010,0040) VR=CS VM=1 Patient's Sex</summary>
		DicomTags.PatientsSex = new DicomTag(0x0010, 0x0040);

		/// <summary>(0010,0050) VR=SQ VM=1 Patient's Insurance Plan Code Sequence</summary>
		DicomTags.PatientsInsurancePlanCodeSequence = new DicomTag(0x0010, 0x0050);

		/// <summary>(0010,0101) VR=SQ VM=1 Patient's Primary Language Code Sequence</summary>
		DicomTags.PatientsPrimaryLanguageCodeSequence = new DicomTag(0x0010, 0x0101);

		/// <summary>(0010,0102) VR=SQ VM=1 Patient's Primary Language Code Modifier Sequence</summary>
		DicomTags.PatientsPrimaryLanguageCodeModifierSequence = new DicomTag(0x0010, 0x0102);

		/// <summary>(0010,1000) VR=LO VM=1-n Other Patient IDs</summary>
		DicomTags.OtherPatientIDs = new DicomTag(0x0010, 0x1000);

		/// <summary>(0010,1001) VR=PN VM=1-n Other Patient Names</summary>
		DicomTags.OtherPatientNames = new DicomTag(0x0010, 0x1001);

		/// <summary>(0010,1002) VR=SQ VM=1 Other Patient IDs Sequence</summary>
		DicomTags.OtherPatientIDsSequence = new DicomTag(0x0010, 0x1002);

		/// <summary>(0010,1005) VR=PN VM=1 Patient's Birth Name</summary>
		DicomTags.PatientsBirthName = new DicomTag(0x0010, 0x1005);

		/// <summary>(0010,1010) VR=AS VM=1 Patient's Age</summary>
		DicomTags.PatientsAge = new DicomTag(0x0010, 0x1010);

		/// <summary>(0010,1020) VR=DS VM=1 Patient's Size</summary>
		DicomTags.PatientsSize = new DicomTag(0x0010, 0x1020);

		/// <summary>(0010,1030) VR=DS VM=1 Patient's Weight</summary>
		DicomTags.PatientsWeight = new DicomTag(0x0010, 0x1030);

		/// <summary>(0010,1040) VR=LO VM=1 Patient's Address</summary>
		DicomTags.PatientsAddress = new DicomTag(0x0010, 0x1040);

		/// <summary>(0010,1050) VR=LO VM=1-n Insurance Plan Identification (Retired)</summary>
		DicomTags.InsurancePlanIdentificationRETIRED = new DicomTag(0x0010, 0x1050);

		/// <summary>(0010,1060) VR=PN VM=1 Patient's Mother's Birth Name</summary>
		DicomTags.PatientsMothersBirthName = new DicomTag(0x0010, 0x1060);

		/// <summary>(0010,1080) VR=LO VM=1 Military Rank</summary>
		DicomTags.MilitaryRank = new DicomTag(0x0010, 0x1080);

		/// <summary>(0010,1081) VR=LO VM=1 Branch of Service</summary>
		DicomTags.BranchOfService = new DicomTag(0x0010, 0x1081);

		/// <summary>(0010,1090) VR=LO VM=1 Medical Record Locator</summary>
		DicomTags.MedicalRecordLocator = new DicomTag(0x0010, 0x1090);

		/// <summary>(0010,2000) VR=LO VM=1-n Medical Alerts</summary>
		DicomTags.MedicalAlerts = new DicomTag(0x0010, 0x2000);

		/// <summary>(0010,2110) VR=LO VM=1-n Allergies</summary>
		DicomTags.Allergies = new DicomTag(0x0010, 0x2110);

		/// <summary>(0010,2150) VR=LO VM=1 Country of Residence</summary>
		DicomTags.CountryOfResidence = new DicomTag(0x0010, 0x2150);

		/// <summary>(0010,2152) VR=LO VM=1 Region of Residence</summary>
		DicomTags.RegionOfResidence = new DicomTag(0x0010, 0x2152);

		/// <summary>(0010,2154) VR=SH VM=1-n Patient's Telephone Numbers</summary>
		DicomTags.PatientsTelephoneNumbers = new DicomTag(0x0010, 0x2154);

		/// <summary>(0010,2160) VR=SH VM=1 Ethnic Group</summary>
		DicomTags.EthnicGroup = new DicomTag(0x0010, 0x2160);

		/// <summary>(0010,2180) VR=SH VM=1 Occupation</summary>
		DicomTags.Occupation = new DicomTag(0x0010, 0x2180);

		/// <summary>(0010,21a0) VR=CS VM=1 Smoking Status</summary>
		DicomTags.SmokingStatus = new DicomTag(0x0010, 0x21a0);

		/// <summary>(0010,21b0) VR=LT VM=1 Additional Patient History</summary>
		DicomTags.AdditionalPatientHistory = new DicomTag(0x0010, 0x21b0);

		/// <summary>(0010,21c0) VR=US VM=1 Pregnancy Status</summary>
		DicomTags.PregnancyStatus = new DicomTag(0x0010, 0x21c0);

		/// <summary>(0010,21d0) VR=DA VM=1 Last Menstrual Date</summary>
		DicomTags.LastMenstrualDate = new DicomTag(0x0010, 0x21d0);

		/// <summary>(0010,21f0) VR=LO VM=1 Patient's Religious Preference</summary>
		DicomTags.PatientsReligiousPreference = new DicomTag(0x0010, 0x21f0);

		/// <summary>(0010,2201) VR=LO VM=1 Patient Species Description</summary>
		DicomTags.PatientSpeciesDescription = new DicomTag(0x0010, 0x2201);

		/// <summary>(0010,2202) VR=SQ VM=1 Patient Species Code Sequence</summary>
		DicomTags.PatientSpeciesCodeSequence = new DicomTag(0x0010, 0x2202);

		/// <summary>(0010,2203) VR=CS VM=1 Patient's Sex Neutered</summary>
		DicomTags.PatientsSexNeutered = new DicomTag(0x0010, 0x2203);

		/// <summary>(0010,2292) VR=LO VM=1 Patient Breed Description</summary>
		DicomTags.PatientBreedDescription = new DicomTag(0x0010, 0x2292);

		/// <summary>(0010,2293) VR=SQ VM=1 Patient Breed Code Sequence</summary>
		DicomTags.PatientBreedCodeSequence = new DicomTag(0x0010, 0x2293);

		/// <summary>(0010,2294) VR=SQ VM=1 Breed Registration Sequence</summary>
		DicomTags.BreedRegistrationSequence = new DicomTag(0x0010, 0x2294);

		/// <summary>(0010,2295) VR=LO VM=1 Breed Registration Number</summary>
		DicomTags.BreedRegistrationNumber = new DicomTag(0x0010, 0x2295);

		/// <summary>(0010,2296) VR=SQ VM=1 Breed Registry Code Sequence</summary>
		DicomTags.BreedRegistryCodeSequence = new DicomTag(0x0010, 0x2296);

		/// <summary>(0010,2297) VR=PN VM=1 Responsible Person</summary>
		DicomTags.ResponsiblePerson = new DicomTag(0x0010, 0x2297);

		/// <summary>(0010,2298) VR=CS VM=1 Responsible Person Role</summary>
		DicomTags.ResponsiblePersonRole = new DicomTag(0x0010, 0x2298);

		/// <summary>(0010,2299) VR=LO VM=1 Responsible Organization</summary>
		DicomTags.ResponsibleOrganization = new DicomTag(0x0010, 0x2299);

		/// <summary>(0010,4000) VR=LT VM=1 Patient Comments</summary>
		DicomTags.PatientComments = new DicomTag(0x0010, 0x4000);

		/// <summary>(0010,9431) VR=FL VM=1 Examined Body Thickness</summary>
		DicomTags.ExaminedBodyThickness = new DicomTag(0x0010, 0x9431);

		/// <summary>(0012,0010) VR=LO VM=1 Clinical Trial Sponsor Name</summary>
		DicomTags.ClinicalTrialSponsorName = new DicomTag(0x0012, 0x0010);

		/// <summary>(0012,0020) VR=LO VM=1 Clinical Trial Protocol ID</summary>
		DicomTags.ClinicalTrialProtocolID = new DicomTag(0x0012, 0x0020);

		/// <summary>(0012,0021) VR=LO VM=1 Clinical Trial Protocol Name</summary>
		DicomTags.ClinicalTrialProtocolName = new DicomTag(0x0012, 0x0021);

		/// <summary>(0012,0030) VR=LO VM=1 Clinical Trial Site ID</summary>
		DicomTags.ClinicalTrialSiteID = new DicomTag(0x0012, 0x0030);

		/// <summary>(0012,0031) VR=LO VM=1 Clinical Trial Site Name</summary>
		DicomTags.ClinicalTrialSiteName = new DicomTag(0x0012, 0x0031);

		/// <summary>(0012,0040) VR=LO VM=1 Clinical Trial Subject ID</summary>
		DicomTags.ClinicalTrialSubjectID = new DicomTag(0x0012, 0x0040);

		/// <summary>(0012,0042) VR=LO VM=1 Clinical Trial Subject Reading ID</summary>
		DicomTags.ClinicalTrialSubjectReadingID = new DicomTag(0x0012, 0x0042);

		/// <summary>(0012,0050) VR=LO VM=1 Clinical Trial Time Point ID</summary>
		DicomTags.ClinicalTrialTimePointID = new DicomTag(0x0012, 0x0050);

		/// <summary>(0012,0051) VR=ST VM=1 Clinical Trial Time Point Description</summary>
		DicomTags.ClinicalTrialTimePointDescription = new DicomTag(0x0012, 0x0051);

		/// <summary>(0012,0060) VR=LO VM=1 Clinical Trial Coordinating Center Name</summary>
		DicomTags.ClinicalTrialCoordinatingCenterName = new DicomTag(0x0012, 0x0060);

		/// <summary>(0012,0062) VR=CS VM=1 Patient Identity Removed</summary>
		DicomTags.PatientIdentityRemoved = new DicomTag(0x0012, 0x0062);

		/// <summary>(0012,0063) VR=LO VM=1-n De-identification Method</summary>
		DicomTags.DeidentificationMethod = new DicomTag(0x0012, 0x0063);

		/// <summary>(0012,0064) VR=SQ VM=1 De-identification Method Code Sequence</summary>
		DicomTags.DeidentificationMethodCodeSequence = new DicomTag(0x0012, 0x0064);

		/// <summary>(0012,0071) VR=LO VM=1 Clinical Trial Series ID</summary>
		DicomTags.ClinicalTrialSeriesID = new DicomTag(0x0012, 0x0071);

		/// <summary>(0012,0072) VR=LO VM=1 Clinical Trial Series Description</summary>
		DicomTags.ClinicalTrialSeriesDescription = new DicomTag(0x0012, 0x0072);

		/// <summary>(0018,0010) VR=LO VM=1 Contrast/Bolus Agent</summary>
		DicomTags.ContrastBolusAgent = new DicomTag(0x0018, 0x0010);

		/// <summary>(0018,0012) VR=SQ VM=1 Contrast/Bolus Agent Sequence</summary>
		DicomTags.ContrastBolusAgentSequence = new DicomTag(0x0018, 0x0012);

		/// <summary>(0018,0014) VR=SQ VM=1 Contrast/Bolus Administration Route Sequence</summary>
		DicomTags.ContrastBolusAdministrationRouteSequence = new DicomTag(0x0018, 0x0014);

		/// <summary>(0018,0015) VR=CS VM=1 Body Part Examined</summary>
		DicomTags.BodyPartExamined = new DicomTag(0x0018, 0x0015);

		/// <summary>(0018,0020) VR=CS VM=1-n Scanning Sequence</summary>
		DicomTags.ScanningSequence = new DicomTag(0x0018, 0x0020);

		/// <summary>(0018,0021) VR=CS VM=1-n Sequence Variant</summary>
		DicomTags.SequenceVariant = new DicomTag(0x0018, 0x0021);

		/// <summary>(0018,0022) VR=CS VM=1-n Scan Options</summary>
		DicomTags.ScanOptions = new DicomTag(0x0018, 0x0022);

		/// <summary>(0018,0023) VR=CS VM=1 MR Acquisition Type</summary>
		DicomTags.MRAcquisitionType = new DicomTag(0x0018, 0x0023);

		/// <summary>(0018,0024) VR=SH VM=1 Sequence Name</summary>
		DicomTags.SequenceName = new DicomTag(0x0018, 0x0024);

		/// <summary>(0018,0025) VR=CS VM=1 Angio Flag</summary>
		DicomTags.AngioFlag = new DicomTag(0x0018, 0x0025);

		/// <summary>(0018,0026) VR=SQ VM=1 Intervention Drug Information Sequence</summary>
		DicomTags.InterventionDrugInformationSequence = new DicomTag(0x0018, 0x0026);

		/// <summary>(0018,0027) VR=TM VM=1 Intervention Drug Stop Time</summary>
		DicomTags.InterventionDrugStopTime = new DicomTag(0x0018, 0x0027);

		/// <summary>(0018,0028) VR=DS VM=1 Intervention Drug Dose</summary>
		DicomTags.InterventionDrugDose = new DicomTag(0x0018, 0x0028);

		/// <summary>(0018,0029) VR=SQ VM=1 Intervention Drug Sequence</summary>
		DicomTags.InterventionDrugSequence = new DicomTag(0x0018, 0x0029);

		/// <summary>(0018,002a) VR=SQ VM=1 Additional Drug Sequence</summary>
		DicomTags.AdditionalDrugSequence = new DicomTag(0x0018, 0x002a);

		/// <summary>(0018,0030) VR=LO VM=1-n Radionuclide (Retired)</summary>
		DicomTags.RadionuclideRETIRED = new DicomTag(0x0018, 0x0030);

		/// <summary>(0018,0031) VR=LO VM=1 Radiopharmaceutical</summary>
		DicomTags.Radiopharmaceutical = new DicomTag(0x0018, 0x0031);

		/// <summary>(0018,0032) VR=DS VM=1 Energy Window Centerline (Retired)</summary>
		DicomTags.EnergyWindowCenterlineRETIRED = new DicomTag(0x0018, 0x0032);

		/// <summary>(0018,0033) VR=DS VM=1-n Energy Window Total Width (Retired)</summary>
		DicomTags.EnergyWindowTotalWidthRETIRED = new DicomTag(0x0018, 0x0033);

		/// <summary>(0018,0034) VR=LO VM=1 Intervention Drug Name</summary>
		DicomTags.InterventionDrugName = new DicomTag(0x0018, 0x0034);

		/// <summary>(0018,0035) VR=TM VM=1 Intervention Drug Start Time</summary>
		DicomTags.InterventionDrugStartTime = new DicomTag(0x0018, 0x0035);

		/// <summary>(0018,0036) VR=SQ VM=1 Intervention Sequence</summary>
		DicomTags.InterventionSequence = new DicomTag(0x0018, 0x0036);

		/// <summary>(0018,0037) VR=CS VM=1 Therapy Type (Retired)</summary>
		DicomTags.TherapyTypeRETIRED = new DicomTag(0x0018, 0x0037);

		/// <summary>(0018,0038) VR=CS VM=1 Intervention Status</summary>
		DicomTags.InterventionStatus = new DicomTag(0x0018, 0x0038);

		/// <summary>(0018,0039) VR=CS VM=1 Therapy Description (Retired)</summary>
		DicomTags.TherapyDescriptionRETIRED = new DicomTag(0x0018, 0x0039);

		/// <summary>(0018,003a) VR=ST VM=1 Intervention Description</summary>
		DicomTags.InterventionDescription = new DicomTag(0x0018, 0x003a);

		/// <summary>(0018,0040) VR=IS VM=1 Cine Rate</summary>
		DicomTags.CineRate = new DicomTag(0x0018, 0x0040);

		/// <summary>(0018,0050) VR=DS VM=1 Slice Thickness</summary>
		DicomTags.SliceThickness = new DicomTag(0x0018, 0x0050);

		/// <summary>(0018,0060) VR=DS VM=1 KVP</summary>
		DicomTags.KVP = new DicomTag(0x0018, 0x0060);

		/// <summary>(0018,0070) VR=IS VM=1 Counts Accumulated</summary>
		DicomTags.CountsAccumulated = new DicomTag(0x0018, 0x0070);

		/// <summary>(0018,0071) VR=CS VM=1 Acquisition Termination Condition</summary>
		DicomTags.AcquisitionTerminationCondition = new DicomTag(0x0018, 0x0071);

		/// <summary>(0018,0072) VR=DS VM=1 Effective Duration</summary>
		DicomTags.EffectiveDuration = new DicomTag(0x0018, 0x0072);

		/// <summary>(0018,0073) VR=CS VM=1 Acquisition Start Condition</summary>
		DicomTags.AcquisitionStartCondition = new DicomTag(0x0018, 0x0073);

		/// <summary>(0018,0074) VR=IS VM=1 Acquisition Start Condition Data</summary>
		DicomTags.AcquisitionStartConditionData = new DicomTag(0x0018, 0x0074);

		/// <summary>(0018,0075) VR=IS VM=1 Acquisition Termination Condition Data</summary>
		DicomTags.AcquisitionTerminationConditionData = new DicomTag(0x0018, 0x0075);

		/// <summary>(0018,0080) VR=DS VM=1 Repetition Time</summary>
		DicomTags.RepetitionTime = new DicomTag(0x0018, 0x0080);

		/// <summary>(0018,0081) VR=DS VM=1 Echo Time</summary>
		DicomTags.EchoTime = new DicomTag(0x0018, 0x0081);

		/// <summary>(0018,0082) VR=DS VM=1 Inversion Time</summary>
		DicomTags.InversionTime = new DicomTag(0x0018, 0x0082);

		/// <summary>(0018,0083) VR=DS VM=1 Number of Averages</summary>
		DicomTags.NumberOfAverages = new DicomTag(0x0018, 0x0083);

		/// <summary>(0018,0084) VR=DS VM=1 Imaging Frequency</summary>
		DicomTags.ImagingFrequency = new DicomTag(0x0018, 0x0084);

		/// <summary>(0018,0085) VR=SH VM=1 Imaged Nucleus</summary>
		DicomTags.ImagedNucleus = new DicomTag(0x0018, 0x0085);

		/// <summary>(0018,0086) VR=IS VM=1-n Echo Number(s)</summary>
		DicomTags.EchoNumbers = new DicomTag(0x0018, 0x0086);

		/// <summary>(0018,0087) VR=DS VM=1 Magnetic Field Strength</summary>
		DicomTags.MagneticFieldStrength = new DicomTag(0x0018, 0x0087);

		/// <summary>(0018,0088) VR=DS VM=1 Spacing Between Slices</summary>
		DicomTags.SpacingBetweenSlices = new DicomTag(0x0018, 0x0088);

		/// <summary>(0018,0089) VR=IS VM=1 Number of Phase Encoding Steps</summary>
		DicomTags.NumberOfPhaseEncodingSteps = new DicomTag(0x0018, 0x0089);

		/// <summary>(0018,0090) VR=DS VM=1 Data Collection Diameter</summary>
		DicomTags.DataCollectionDiameter = new DicomTag(0x0018, 0x0090);

		/// <summary>(0018,0091) VR=IS VM=1 Echo Train Length</summary>
		DicomTags.EchoTrainLength = new DicomTag(0x0018, 0x0091);

		/// <summary>(0018,0093) VR=DS VM=1 Percent Sampling</summary>
		DicomTags.PercentSampling = new DicomTag(0x0018, 0x0093);

		/// <summary>(0018,0094) VR=DS VM=1 Percent Phase Field of View</summary>
		DicomTags.PercentPhaseFieldOfView = new DicomTag(0x0018, 0x0094);

		/// <summary>(0018,0095) VR=DS VM=1 Pixel Bandwidth</summary>
		DicomTags.PixelBandwidth = new DicomTag(0x0018, 0x0095);

		/// <summary>(0018,1000) VR=LO VM=1 Device Serial Number</summary>
		DicomTags.DeviceSerialNumber = new DicomTag(0x0018, 0x1000);

		/// <summary>(0018,1002) VR=UI VM=1 Device UID</summary>
		DicomTags.DeviceUID = new DicomTag(0x0018, 0x1002);

		/// <summary>(0018,1003) VR=LO VM=1 Device ID</summary>
		DicomTags.DeviceID = new DicomTag(0x0018, 0x1003);

		/// <summary>(0018,1004) VR=LO VM=1 Plate ID</summary>
		DicomTags.PlateID = new DicomTag(0x0018, 0x1004);

		/// <summary>(0018,1005) VR=LO VM=1 Generator ID</summary>
		DicomTags.GeneratorID = new DicomTag(0x0018, 0x1005);

		/// <summary>(0018,1006) VR=LO VM=1 Grid ID</summary>
		DicomTags.GridID = new DicomTag(0x0018, 0x1006);

		/// <summary>(0018,1007) VR=LO VM=1 Cassette ID</summary>
		DicomTags.CassetteID = new DicomTag(0x0018, 0x1007);

		/// <summary>(0018,1008) VR=LO VM=1 Gantry ID</summary>
		DicomTags.GantryID = new DicomTag(0x0018, 0x1008);

		/// <summary>(0018,1010) VR=LO VM=1 Secondary Capture Device ID</summary>
		DicomTags.SecondaryCaptureDeviceID = new DicomTag(0x0018, 0x1010);

		/// <summary>(0018,1011) VR=LO VM=1 Hardcopy Creation Device ID (Retired)</summary>
		DicomTags.HardcopyCreationDeviceIDRETIRED = new DicomTag(0x0018, 0x1011);

		/// <summary>(0018,1012) VR=DA VM=1 Date of Secondary Capture</summary>
		DicomTags.DateOfSecondaryCapture = new DicomTag(0x0018, 0x1012);

		/// <summary>(0018,1014) VR=TM VM=1 Time of Secondary Capture</summary>
		DicomTags.TimeOfSecondaryCapture = new DicomTag(0x0018, 0x1014);

		/// <summary>(0018,1016) VR=LO VM=1 Secondary Capture Device Manufacturers</summary>
		DicomTags.SecondaryCaptureDeviceManufacturers = new DicomTag(0x0018, 0x1016);

		/// <summary>(0018,1017) VR=LO VM=1 Hardcopy Device Manufacturer (Retired)</summary>
		DicomTags.HardcopyDeviceManufacturerRETIRED = new DicomTag(0x0018, 0x1017);

		/// <summary>(0018,1018) VR=LO VM=1 Secondary Capture Device Manufacturer's Model Name</summary>
		DicomTags.SecondaryCaptureDeviceManufacturersModelName = new DicomTag(0x0018, 0x1018);

		/// <summary>(0018,1019) VR=LO VM=1-n Secondary Capture Device Software Version(s)</summary>
		DicomTags.SecondaryCaptureDeviceSoftwareVersions = new DicomTag(0x0018, 0x1019);

		/// <summary>(0018,101a) VR=LO VM=1-n Hardcopy Device Software Version (Retired)</summary>
		DicomTags.HardcopyDeviceSoftwareVersionRETIRED = new DicomTag(0x0018, 0x101a);

		/// <summary>(0018,101b) VR=LO VM=1 Hardcopy Device Manufacturer's Model Name (Retired)</summary>
		DicomTags.HardcopyDeviceManufacturersModelNameRETIRED = new DicomTag(0x0018, 0x101b);

		/// <summary>(0018,1020) VR=LO VM=1-n Software Version(s)</summary>
		DicomTags.SoftwareVersions = new DicomTag(0x0018, 0x1020);

		/// <summary>(0018,1022) VR=SH VM=1 Video Image Format Acquired</summary>
		DicomTags.VideoImageFormatAcquired = new DicomTag(0x0018, 0x1022);

		/// <summary>(0018,1023) VR=LO VM=1 Digital Image Format Acquired</summary>
		DicomTags.DigitalImageFormatAcquired = new DicomTag(0x0018, 0x1023);

		/// <summary>(0018,1030) VR=LO VM=1 Protocol Name</summary>
		DicomTags.ProtocolName = new DicomTag(0x0018, 0x1030);

		/// <summary>(0018,1040) VR=LO VM=1 Contrast/Bolus Route</summary>
		DicomTags.ContrastBolusRoute = new DicomTag(0x0018, 0x1040);

		/// <summary>(0018,1041) VR=DS VM=1 Contrast/Bolus Volume</summary>
		DicomTags.ContrastBolusVolume = new DicomTag(0x0018, 0x1041);

		/// <summary>(0018,1042) VR=TM VM=1 Contrast/Bolus Start Time</summary>
		DicomTags.ContrastBolusStartTime = new DicomTag(0x0018, 0x1042);

		/// <summary>(0018,1043) VR=TM VM=1 Contrast/Bolus Stop Time</summary>
		DicomTags.ContrastBolusStopTime = new DicomTag(0x0018, 0x1043);

		/// <summary>(0018,1044) VR=DS VM=1 Contrast/Bolus Total Dose</summary>
		DicomTags.ContrastBolusTotalDose = new DicomTag(0x0018, 0x1044);

		/// <summary>(0018,1045) VR=IS VM=1 Syringe Counts</summary>
		DicomTags.SyringeCounts = new DicomTag(0x0018, 0x1045);

		/// <summary>(0018,1046) VR=DS VM=1-n Contrast Flow Rate</summary>
		DicomTags.ContrastFlowRate = new DicomTag(0x0018, 0x1046);

		/// <summary>(0018,1047) VR=DS VM=1-n Contrast Flow Duration</summary>
		DicomTags.ContrastFlowDuration = new DicomTag(0x0018, 0x1047);

		/// <summary>(0018,1048) VR=CS VM=1 Contrast/Bolus Ingredient</summary>
		DicomTags.ContrastBolusIngredient = new DicomTag(0x0018, 0x1048);

		/// <summary>(0018,1049) VR=DS VM=1 Contrast/Bolus Ingredient Concentration</summary>
		DicomTags.ContrastBolusIngredientConcentration = new DicomTag(0x0018, 0x1049);

		/// <summary>(0018,1050) VR=DS VM=1 Spatial Resolution</summary>
		DicomTags.SpatialResolution = new DicomTag(0x0018, 0x1050);

		/// <summary>(0018,1060) VR=DS VM=1 Trigger Time</summary>
		DicomTags.TriggerTime = new DicomTag(0x0018, 0x1060);

		/// <summary>(0018,1061) VR=LO VM=1 Trigger Source or Type</summary>
		DicomTags.TriggerSourceOrType = new DicomTag(0x0018, 0x1061);

		/// <summary>(0018,1062) VR=IS VM=1 Nominal Interval</summary>
		DicomTags.NominalInterval = new DicomTag(0x0018, 0x1062);

		/// <summary>(0018,1063) VR=DS VM=1 Frame Time</summary>
		DicomTags.FrameTime = new DicomTag(0x0018, 0x1063);

		/// <summary>(0018,1064) VR=LO VM=1 Cardiac Framing Type</summary>
		DicomTags.CardiacFramingType = new DicomTag(0x0018, 0x1064);

		/// <summary>(0018,1065) VR=DS VM=1-n Frame Time Vector</summary>
		DicomTags.FrameTimeVector = new DicomTag(0x0018, 0x1065);

		/// <summary>(0018,1066) VR=DS VM=1 Frame Delay</summary>
		DicomTags.FrameDelay = new DicomTag(0x0018, 0x1066);

		/// <summary>(0018,1067) VR=DS VM=1 Image Trigger Delay</summary>
		DicomTags.ImageTriggerDelay = new DicomTag(0x0018, 0x1067);

		/// <summary>(0018,1068) VR=DS VM=1 Multiplex Group Time Offset</summary>
		DicomTags.MultiplexGroupTimeOffset = new DicomTag(0x0018, 0x1068);

		/// <summary>(0018,1069) VR=DS VM=1 Trigger Time Offset</summary>
		DicomTags.TriggerTimeOffset = new DicomTag(0x0018, 0x1069);

		/// <summary>(0018,106a) VR=CS VM=1 Synchronization Trigger</summary>
		DicomTags.SynchronizationTrigger = new DicomTag(0x0018, 0x106a);

		/// <summary>(0018,106c) VR=US VM=2 Synchronization Channel</summary>
		DicomTags.SynchronizationChannel = new DicomTag(0x0018, 0x106c);

		/// <summary>(0018,106e) VR=UL VM=1 Trigger Sample Position</summary>
		DicomTags.TriggerSamplePosition = new DicomTag(0x0018, 0x106e);

		/// <summary>(0018,1070) VR=LO VM=1 Radiopharmaceutical Route</summary>
		DicomTags.RadiopharmaceuticalRoute = new DicomTag(0x0018, 0x1070);

		/// <summary>(0018,1071) VR=DS VM=1 Radiopharmaceutical Volume</summary>
		DicomTags.RadiopharmaceuticalVolume = new DicomTag(0x0018, 0x1071);

		/// <summary>(0018,1072) VR=TM VM=1 Radiopharmaceutical Start Time</summary>
		DicomTags.RadiopharmaceuticalStartTime = new DicomTag(0x0018, 0x1072);

		/// <summary>(0018,1073) VR=TM VM=1 Radiopharmaceutical Stop Time</summary>
		DicomTags.RadiopharmaceuticalStopTime = new DicomTag(0x0018, 0x1073);

		/// <summary>(0018,1074) VR=DS VM=1 Radionuclide Total Dose</summary>
		DicomTags.RadionuclideTotalDose = new DicomTag(0x0018, 0x1074);

		/// <summary>(0018,1075) VR=DS VM=1 Radionuclide Half Life</summary>
		DicomTags.RadionuclideHalfLife = new DicomTag(0x0018, 0x1075);

		/// <summary>(0018,1076) VR=DS VM=1 Radionuclide Positron Fraction</summary>
		DicomTags.RadionuclidePositronFraction = new DicomTag(0x0018, 0x1076);

		/// <summary>(0018,1077) VR=DS VM=1 Radiopharmaceutical Specific Activity</summary>
		DicomTags.RadiopharmaceuticalSpecificActivity = new DicomTag(0x0018, 0x1077);

		/// <summary>(0018,1078) VR=DT VM=1 Radiopharmaceutical Start DateTime</summary>
		DicomTags.RadiopharmaceuticalStartDateTime = new DicomTag(0x0018, 0x1078);

		/// <summary>(0018,1079) VR=DT VM=1 Radiopharmaceutical Stop DateTime</summary>
		DicomTags.RadiopharmaceuticalStopDateTime = new DicomTag(0x0018, 0x1079);

		/// <summary>(0018,1080) VR=CS VM=1 Beat Rejection Flag</summary>
		DicomTags.BeatRejectionFlag = new DicomTag(0x0018, 0x1080);

		/// <summary>(0018,1081) VR=IS VM=1 Low R-R Value</summary>
		DicomTags.LowRRValue = new DicomTag(0x0018, 0x1081);

		/// <summary>(0018,1082) VR=IS VM=1 High R-R Value</summary>
		DicomTags.HighRRValue = new DicomTag(0x0018, 0x1082);

		/// <summary>(0018,1083) VR=IS VM=1 Intervals Acquired</summary>
		DicomTags.IntervalsAcquired = new DicomTag(0x0018, 0x1083);

		/// <summary>(0018,1084) VR=IS VM=1 Intervals Rejected</summary>
		DicomTags.IntervalsRejected = new DicomTag(0x0018, 0x1084);

		/// <summary>(0018,1085) VR=LO VM=1 PVC Rejection</summary>
		DicomTags.PVCRejection = new DicomTag(0x0018, 0x1085);

		/// <summary>(0018,1086) VR=IS VM=1 Skip Beats</summary>
		DicomTags.SkipBeats = new DicomTag(0x0018, 0x1086);

		/// <summary>(0018,1088) VR=IS VM=1 Heart Rate</summary>
		DicomTags.HeartRate = new DicomTag(0x0018, 0x1088);

		/// <summary>(0018,1090) VR=IS VM=1 Cardiac Number of Images</summary>
		DicomTags.CardiacNumberOfImages = new DicomTag(0x0018, 0x1090);

		/// <summary>(0018,1094) VR=IS VM=1 Trigger Window</summary>
		DicomTags.TriggerWindow = new DicomTag(0x0018, 0x1094);

		/// <summary>(0018,1100) VR=DS VM=1 Reconstruction Diameter</summary>
		DicomTags.ReconstructionDiameter = new DicomTag(0x0018, 0x1100);

		/// <summary>(0018,1110) VR=DS VM=1 Distance Source to Detector</summary>
		DicomTags.DistanceSourceToDetector = new DicomTag(0x0018, 0x1110);

		/// <summary>(0018,1111) VR=DS VM=1 Distance Source to Patient</summary>
		DicomTags.DistanceSourceToPatient = new DicomTag(0x0018, 0x1111);

		/// <summary>(0018,1114) VR=DS VM=1 Estimated Radiographic Magnification Factor</summary>
		DicomTags.EstimatedRadiographicMagnificationFactor = new DicomTag(0x0018, 0x1114);

		/// <summary>(0018,1120) VR=DS VM=1 Gantry/Detector Tilt</summary>
		DicomTags.GantryDetectorTilt = new DicomTag(0x0018, 0x1120);

		/// <summary>(0018,1121) VR=DS VM=1 Gantry/Detector Slew</summary>
		DicomTags.GantryDetectorSlew = new DicomTag(0x0018, 0x1121);

		/// <summary>(0018,1130) VR=DS VM=1 Table Height</summary>
		DicomTags.TableHeight = new DicomTag(0x0018, 0x1130);

		/// <summary>(0018,1131) VR=DS VM=1 Table Traverse</summary>
		DicomTags.TableTraverse = new DicomTag(0x0018, 0x1131);

		/// <summary>(0018,1134) VR=CS VM=1 Table Motion</summary>
		DicomTags.TableMotion = new DicomTag(0x0018, 0x1134);

		/// <summary>(0018,1135) VR=DS VM=1-n Table Vertical Increment</summary>
		DicomTags.TableVerticalIncrement = new DicomTag(0x0018, 0x1135);

		/// <summary>(0018,1136) VR=DS VM=1-n Table Lateral Increment</summary>
		DicomTags.TableLateralIncrement = new DicomTag(0x0018, 0x1136);

		/// <summary>(0018,1137) VR=DS VM=1-n Table Longitudinal Increment</summary>
		DicomTags.TableLongitudinalIncrement = new DicomTag(0x0018, 0x1137);

		/// <summary>(0018,1138) VR=DS VM=1 Table Angle</summary>
		DicomTags.TableAngle = new DicomTag(0x0018, 0x1138);

		/// <summary>(0018,113a) VR=CS VM=1 Table Type</summary>
		DicomTags.TableType = new DicomTag(0x0018, 0x113a);

		/// <summary>(0018,1140) VR=CS VM=1 Rotation Direction</summary>
		DicomTags.RotationDirection = new DicomTag(0x0018, 0x1140);

		/// <summary>(0018,1141) VR=DS VM=1 Angular Position (Retired)</summary>
		DicomTags.AngularPositionRETIRED = new DicomTag(0x0018, 0x1141);

		/// <summary>(0018,1142) VR=DS VM=1-n Radial Position</summary>
		DicomTags.RadialPosition = new DicomTag(0x0018, 0x1142);

		/// <summary>(0018,1143) VR=DS VM=1 Scan Arc</summary>
		DicomTags.ScanArc = new DicomTag(0x0018, 0x1143);

		/// <summary>(0018,1144) VR=DS VM=1 Angular Step</summary>
		DicomTags.AngularStep = new DicomTag(0x0018, 0x1144);

		/// <summary>(0018,1145) VR=DS VM=1 Center of Rotation Offset</summary>
		DicomTags.CenterOfRotationOffset = new DicomTag(0x0018, 0x1145);

		/// <summary>(0018,1146) VR=DS VM=1-n Rotation Offset (Retired)</summary>
		DicomTags.RotationOffsetRETIRED = new DicomTag(0x0018, 0x1146);

		/// <summary>(0018,1147) VR=CS VM=1 Field of View Shape</summary>
		DicomTags.FieldOfViewShape = new DicomTag(0x0018, 0x1147);

		/// <summary>(0018,1149) VR=IS VM=1-2 Field of View Dimension(s)</summary>
		DicomTags.FieldOfViewDimensions = new DicomTag(0x0018, 0x1149);

		/// <summary>(0018,1150) VR=IS VM=1 Exposure Time</summary>
		DicomTags.ExposureTime = new DicomTag(0x0018, 0x1150);

		/// <summary>(0018,1151) VR=IS VM=1 X-Ray Tube Current</summary>
		DicomTags.XRayTubeCurrent = new DicomTag(0x0018, 0x1151);

		/// <summary>(0018,1152) VR=IS VM=1 Exposure</summary>
		DicomTags.Exposure = new DicomTag(0x0018, 0x1152);

		/// <summary>(0018,1153) VR=IS VM=1 Exposure in uAs</summary>
		DicomTags.ExposureInMicroAs = new DicomTag(0x0018, 0x1153);

		/// <summary>(0018,1154) VR=DS VM=1 Average Pulse Width</summary>
		DicomTags.AveragePulseWidth = new DicomTag(0x0018, 0x1154);

		/// <summary>(0018,1155) VR=CS VM=1 Radiation Setting</summary>
		DicomTags.RadiationSetting = new DicomTag(0x0018, 0x1155);

		/// <summary>(0018,1156) VR=CS VM=1 Rectification Type</summary>
		DicomTags.RectificationType = new DicomTag(0x0018, 0x1156);

		/// <summary>(0018,115a) VR=CS VM=1 Radiation Mode</summary>
		DicomTags.RadiationMode = new DicomTag(0x0018, 0x115a);

		/// <summary>(0018,115e) VR=DS VM=1 Image and Fluoroscopy Area Dose Product</summary>
		DicomTags.ImageAndFluoroscopyAreaDoseProduct = new DicomTag(0x0018, 0x115e);

		/// <summary>(0018,1160) VR=SH VM=1 Filter Type</summary>
		DicomTags.FilterType = new DicomTag(0x0018, 0x1160);

		/// <summary>(0018,1161) VR=LO VM=1-n Type of Filters</summary>
		DicomTags.TypeOfFilters = new DicomTag(0x0018, 0x1161);

		/// <summary>(0018,1162) VR=DS VM=1 Intensifier Size</summary>
		DicomTags.IntensifierSize = new DicomTag(0x0018, 0x1162);

		/// <summary>(0018,1164) VR=DS VM=2 Imager Pixel Spacing</summary>
		DicomTags.ImagerPixelSpacing = new DicomTag(0x0018, 0x1164);

		/// <summary>(0018,1166) VR=CS VM=1-n Grid</summary>
		DicomTags.Grid = new DicomTag(0x0018, 0x1166);

		/// <summary>(0018,1170) VR=IS VM=1 Generator Power</summary>
		DicomTags.GeneratorPower = new DicomTag(0x0018, 0x1170);

		/// <summary>(0018,1180) VR=SH VM=1 Collimator/grid Name</summary>
		DicomTags.CollimatorgridName = new DicomTag(0x0018, 0x1180);

		/// <summary>(0018,1181) VR=CS VM=1 Collimator Type</summary>
		DicomTags.CollimatorType = new DicomTag(0x0018, 0x1181);

		/// <summary>(0018,1182) VR=IS VM=1-2 Focal Distance</summary>
		DicomTags.FocalDistance = new DicomTag(0x0018, 0x1182);

		/// <summary>(0018,1183) VR=DS VM=1-2 X Focus Center</summary>
		DicomTags.XFocusCenter = new DicomTag(0x0018, 0x1183);

		/// <summary>(0018,1184) VR=DS VM=1-2 Y Focus Center</summary>
		DicomTags.YFocusCenter = new DicomTag(0x0018, 0x1184);

		/// <summary>(0018,1190) VR=DS VM=1-n Focal Spot(s)</summary>
		DicomTags.FocalSpots = new DicomTag(0x0018, 0x1190);

		/// <summary>(0018,1191) VR=CS VM=1 Anode Target Material</summary>
		DicomTags.AnodeTargetMaterial = new DicomTag(0x0018, 0x1191);

		/// <summary>(0018,11a0) VR=DS VM=1 Body Part Thickness</summary>
		DicomTags.BodyPartThickness = new DicomTag(0x0018, 0x11a0);

		/// <summary>(0018,11a2) VR=DS VM=1 Compression Force</summary>
		DicomTags.CompressionForce = new DicomTag(0x0018, 0x11a2);

		/// <summary>(0018,1200) VR=DA VM=1-n Date of Last Calibration</summary>
		DicomTags.DateOfLastCalibration = new DicomTag(0x0018, 0x1200);

		/// <summary>(0018,1201) VR=TM VM=1-n Time of Last Calibration</summary>
		DicomTags.TimeOfLastCalibration = new DicomTag(0x0018, 0x1201);

		/// <summary>(0018,1210) VR=SH VM=1-n Convolution Kernel</summary>
		DicomTags.ConvolutionKernel = new DicomTag(0x0018, 0x1210);

		/// <summary>(0018,1240) VR=IS VM=1-n Upper/Lower Pixel Values (Retired)</summary>
		DicomTags.UpperLowerPixelValuesRETIRED = new DicomTag(0x0018, 0x1240);

		/// <summary>(0018,1242) VR=IS VM=1 Actual Frame Duration</summary>
		DicomTags.ActualFrameDuration = new DicomTag(0x0018, 0x1242);

		/// <summary>(0018,1243) VR=IS VM=1 Count Rate</summary>
		DicomTags.CountRate = new DicomTag(0x0018, 0x1243);

		/// <summary>(0018,1244) VR=US VM=1 Preferred Playback Sequencing</summary>
		DicomTags.PreferredPlaybackSequencing = new DicomTag(0x0018, 0x1244);

		/// <summary>(0018,1250) VR=SH VM=1 Receive Coil Name</summary>
		DicomTags.ReceiveCoilName = new DicomTag(0x0018, 0x1250);

		/// <summary>(0018,1251) VR=SH VM=1 Transmit Coil Name</summary>
		DicomTags.TransmitCoilName = new DicomTag(0x0018, 0x1251);

		/// <summary>(0018,1260) VR=SH VM=1 Plate Type</summary>
		DicomTags.PlateType = new DicomTag(0x0018, 0x1260);

		/// <summary>(0018,1261) VR=LO VM=1 Phosphor Type</summary>
		DicomTags.PhosphorType = new DicomTag(0x0018, 0x1261);

		/// <summary>(0018,1300) VR=DS VM=1 Scan Velocity</summary>
		DicomTags.ScanVelocity = new DicomTag(0x0018, 0x1300);

		/// <summary>(0018,1301) VR=CS VM=1-n Whole Body Technique</summary>
		DicomTags.WholeBodyTechnique = new DicomTag(0x0018, 0x1301);

		/// <summary>(0018,1302) VR=IS VM=1 Scan Length</summary>
		DicomTags.ScanLength = new DicomTag(0x0018, 0x1302);

		/// <summary>(0018,1310) VR=US VM=4 Acquisition Matrix</summary>
		DicomTags.AcquisitionMatrix = new DicomTag(0x0018, 0x1310);

		/// <summary>(0018,1312) VR=CS VM=1 In-plane Phase Encoding Direction</summary>
		DicomTags.InplanePhaseEncodingDirection = new DicomTag(0x0018, 0x1312);

		/// <summary>(0018,1314) VR=DS VM=1 Flip Angle</summary>
		DicomTags.FlipAngle = new DicomTag(0x0018, 0x1314);

		/// <summary>(0018,1315) VR=CS VM=1 Variable Flip Angle Flag</summary>
		DicomTags.VariableFlipAngleFlag = new DicomTag(0x0018, 0x1315);

		/// <summary>(0018,1316) VR=DS VM=1 SAR</summary>
		DicomTags.SAR = new DicomTag(0x0018, 0x1316);

		/// <summary>(0018,1318) VR=DS VM=1 dB/dt</summary>
		DicomTags.DBdt = new DicomTag(0x0018, 0x1318);

		/// <summary>(0018,1400) VR=LO VM=1 Acquisition Device Processing Description</summary>
		DicomTags.AcquisitionDeviceProcessingDescription = new DicomTag(0x0018, 0x1400);

		/// <summary>(0018,1401) VR=LO VM=1 Acquisition Device Processing Code</summary>
		DicomTags.AcquisitionDeviceProcessingCode = new DicomTag(0x0018, 0x1401);

		/// <summary>(0018,1402) VR=CS VM=1 Cassette Orientation</summary>
		DicomTags.CassetteOrientation = new DicomTag(0x0018, 0x1402);

		/// <summary>(0018,1403) VR=CS VM=1 Cassette Size</summary>
		DicomTags.CassetteSize = new DicomTag(0x0018, 0x1403);

		/// <summary>(0018,1404) VR=US VM=1 Exposures on Plate</summary>
		DicomTags.ExposuresOnPlate = new DicomTag(0x0018, 0x1404);

		/// <summary>(0018,1405) VR=IS VM=1 Relative X-Ray Exposure</summary>
		DicomTags.RelativeXRayExposure = new DicomTag(0x0018, 0x1405);

		/// <summary>(0018,1450) VR=DS VM=1 Column Angulation</summary>
		DicomTags.ColumnAngulation = new DicomTag(0x0018, 0x1450);

		/// <summary>(0018,1460) VR=DS VM=1 Tomo Layer Height</summary>
		DicomTags.TomoLayerHeight = new DicomTag(0x0018, 0x1460);

		/// <summary>(0018,1470) VR=DS VM=1 Tomo Angle</summary>
		DicomTags.TomoAngle = new DicomTag(0x0018, 0x1470);

		/// <summary>(0018,1480) VR=DS VM=1 Tomo Time</summary>
		DicomTags.TomoTime = new DicomTag(0x0018, 0x1480);

		/// <summary>(0018,1490) VR=CS VM=1 Tomo Type</summary>
		DicomTags.TomoType = new DicomTag(0x0018, 0x1490);

		/// <summary>(0018,1491) VR=CS VM=1 Tomo Class</summary>
		DicomTags.TomoClass = new DicomTag(0x0018, 0x1491);

		/// <summary>(0018,1495) VR=IS VM=1 Number of Tomosynthesis Source Images</summary>
		DicomTags.NumberOfTomosynthesisSourceImages = new DicomTag(0x0018, 0x1495);

		/// <summary>(0018,1500) VR=CS VM=1 Positioner Motion</summary>
		DicomTags.PositionerMotion = new DicomTag(0x0018, 0x1500);

		/// <summary>(0018,1508) VR=CS VM=1 Positioner Type</summary>
		DicomTags.PositionerType = new DicomTag(0x0018, 0x1508);

		/// <summary>(0018,1510) VR=DS VM=1 Positioner Primary Angle</summary>
		DicomTags.PositionerPrimaryAngle = new DicomTag(0x0018, 0x1510);

		/// <summary>(0018,1511) VR=DS VM=1 Positioner Secondary Angle</summary>
		DicomTags.PositionerSecondaryAngle = new DicomTag(0x0018, 0x1511);

		/// <summary>(0018,1520) VR=DS VM=1-n Positioner Primary Angle Increment</summary>
		DicomTags.PositionerPrimaryAngleIncrement = new DicomTag(0x0018, 0x1520);

		/// <summary>(0018,1521) VR=DS VM=1-n Positioner Secondary Angle Increment</summary>
		DicomTags.PositionerSecondaryAngleIncrement = new DicomTag(0x0018, 0x1521);

		/// <summary>(0018,1530) VR=DS VM=1 Detector Primary Angle</summary>
		DicomTags.DetectorPrimaryAngle = new DicomTag(0x0018, 0x1530);

		/// <summary>(0018,1531) VR=DS VM=1 Detector Secondary Angle</summary>
		DicomTags.DetectorSecondaryAngle = new DicomTag(0x0018, 0x1531);

		/// <summary>(0018,1600) VR=CS VM=1-3 Shutter Shape</summary>
		DicomTags.ShutterShape = new DicomTag(0x0018, 0x1600);

		/// <summary>(0018,1602) VR=IS VM=1 Shutter Left Vertical Edge</summary>
		DicomTags.ShutterLeftVerticalEdge = new DicomTag(0x0018, 0x1602);

		/// <summary>(0018,1604) VR=IS VM=1 Shutter Right Vertical Edge</summary>
		DicomTags.ShutterRightVerticalEdge = new DicomTag(0x0018, 0x1604);

		/// <summary>(0018,1606) VR=IS VM=1 Shutter Upper Horizontal Edge</summary>
		DicomTags.ShutterUpperHorizontalEdge = new DicomTag(0x0018, 0x1606);

		/// <summary>(0018,1608) VR=IS VM=1 Shutter Lower Horizontal Edge</summary>
		DicomTags.ShutterLowerHorizontalEdge = new DicomTag(0x0018, 0x1608);

		/// <summary>(0018,1610) VR=IS VM=2 Center of Circular Shutter</summary>
		DicomTags.CenterOfCircularShutter = new DicomTag(0x0018, 0x1610);

		/// <summary>(0018,1612) VR=IS VM=1 Radius of Circular Shutter</summary>
		DicomTags.RadiusOfCircularShutter = new DicomTag(0x0018, 0x1612);

		/// <summary>(0018,1620) VR=IS VM=2-2n Vertices of the Polygonal Shutter</summary>
		DicomTags.VerticesOfThePolygonalShutter = new DicomTag(0x0018, 0x1620);

		/// <summary>(0018,1622) VR=US VM=1 Shutter Presentation Value</summary>
		DicomTags.ShutterPresentationValue = new DicomTag(0x0018, 0x1622);

		/// <summary>(0018,1623) VR=US VM=1 Shutter Overlay Group</summary>
		DicomTags.ShutterOverlayGroup = new DicomTag(0x0018, 0x1623);

		/// <summary>(0018,1624) VR=US VM=3 Shutter Presentation Color CIELab Value</summary>
		DicomTags.ShutterPresentationColorCIELabValue = new DicomTag(0x0018, 0x1624);

		/// <summary>(0018,1700) VR=CS VM=1-3 Collimator Shape</summary>
		DicomTags.CollimatorShape = new DicomTag(0x0018, 0x1700);

		/// <summary>(0018,1702) VR=IS VM=1 Collimator Left Vertical Edge</summary>
		DicomTags.CollimatorLeftVerticalEdge = new DicomTag(0x0018, 0x1702);

		/// <summary>(0018,1704) VR=IS VM=1 Collimator Right Vertical Edge</summary>
		DicomTags.CollimatorRightVerticalEdge = new DicomTag(0x0018, 0x1704);

		/// <summary>(0018,1706) VR=IS VM=1 Collimator Upper Horizontal Edge</summary>
		DicomTags.CollimatorUpperHorizontalEdge = new DicomTag(0x0018, 0x1706);

		/// <summary>(0018,1708) VR=IS VM=1 Collimator Lower Horizontal Edge</summary>
		DicomTags.CollimatorLowerHorizontalEdge = new DicomTag(0x0018, 0x1708);

		/// <summary>(0018,1710) VR=IS VM=2 Center of Circular Collimator</summary>
		DicomTags.CenterOfCircularCollimator = new DicomTag(0x0018, 0x1710);

		/// <summary>(0018,1712) VR=IS VM=1 Radius of Circular Collimator</summary>
		DicomTags.RadiusOfCircularCollimator = new DicomTag(0x0018, 0x1712);

		/// <summary>(0018,1720) VR=IS VM=2-2n Vertices of the Polygonal Collimator</summary>
		DicomTags.VerticesOfThePolygonalCollimator = new DicomTag(0x0018, 0x1720);

		/// <summary>(0018,1800) VR=CS VM=1 Acquisition Time Synchronized</summary>
		DicomTags.AcquisitionTimeSynchronized = new DicomTag(0x0018, 0x1800);

		/// <summary>(0018,1801) VR=SH VM=1 Time Source</summary>
		DicomTags.TimeSource = new DicomTag(0x0018, 0x1801);

		/// <summary>(0018,1802) VR=CS VM=1 Time Distribution Protocol</summary>
		DicomTags.TimeDistributionProtocol = new DicomTag(0x0018, 0x1802);

		/// <summary>(0018,1803) VR=LO VM=1 NTP Source Address</summary>
		DicomTags.NTPSourceAddress = new DicomTag(0x0018, 0x1803);

		/// <summary>(0018,2001) VR=IS VM=1-n Page Number Vector</summary>
		DicomTags.PageNumberVector = new DicomTag(0x0018, 0x2001);

		/// <summary>(0018,2002) VR=SH VM=1-n Frame Label Vector</summary>
		DicomTags.FrameLabelVector = new DicomTag(0x0018, 0x2002);

		/// <summary>(0018,2003) VR=DS VM=1-n Frame Primary Angle Vector</summary>
		DicomTags.FramePrimaryAngleVector = new DicomTag(0x0018, 0x2003);

		/// <summary>(0018,2004) VR=DS VM=1-n Frame Secondary Angle Vector</summary>
		DicomTags.FrameSecondaryAngleVector = new DicomTag(0x0018, 0x2004);

		/// <summary>(0018,2005) VR=DS VM=1-n Slice Location Vector</summary>
		DicomTags.SliceLocationVector = new DicomTag(0x0018, 0x2005);

		/// <summary>(0018,2006) VR=SH VM=1-n Display Window Label Vector</summary>
		DicomTags.DisplayWindowLabelVector = new DicomTag(0x0018, 0x2006);

		/// <summary>(0018,2010) VR=DS VM=2 Nominal Scanned Pixel Spacing</summary>
		DicomTags.NominalScannedPixelSpacing = new DicomTag(0x0018, 0x2010);

		/// <summary>(0018,2020) VR=CS VM=1 Digitizing Device Transport Direction</summary>
		DicomTags.DigitizingDeviceTransportDirection = new DicomTag(0x0018, 0x2020);

		/// <summary>(0018,2030) VR=DS VM=1 Rotation of Scanned Film</summary>
		DicomTags.RotationOfScannedFilm = new DicomTag(0x0018, 0x2030);

		/// <summary>(0018,3100) VR=CS VM=1 IVUS Acquisition</summary>
		DicomTags.IVUSAcquisition = new DicomTag(0x0018, 0x3100);

		/// <summary>(0018,3101) VR=DS VM=1 IVUS Pullback Rate</summary>
		DicomTags.IVUSPullbackRate = new DicomTag(0x0018, 0x3101);

		/// <summary>(0018,3102) VR=DS VM=1 IVUS Gated Rate</summary>
		DicomTags.IVUSGatedRate = new DicomTag(0x0018, 0x3102);

		/// <summary>(0018,3103) VR=IS VM=1 IVUS Pullback Start Frame Number</summary>
		DicomTags.IVUSPullbackStartFrameNumber = new DicomTag(0x0018, 0x3103);

		/// <summary>(0018,3104) VR=IS VM=1 IVUS Pullback Stop Frame Number</summary>
		DicomTags.IVUSPullbackStopFrameNumber = new DicomTag(0x0018, 0x3104);

		/// <summary>(0018,3105) VR=IS VM=1-n Lesion Number</summary>
		DicomTags.LesionNumber = new DicomTag(0x0018, 0x3105);

		/// <summary>(0018,4000) VR=LT VM=1 Acquisition Comments (Retired)</summary>
		DicomTags.AcquisitionCommentsRETIRED = new DicomTag(0x0018, 0x4000);

		/// <summary>(0018,5000) VR=SH VM=1-n Output Power</summary>
		DicomTags.OutputPower = new DicomTag(0x0018, 0x5000);

		/// <summary>(0018,5010) VR=LO VM=3 Transducer Data</summary>
		DicomTags.TransducerData = new DicomTag(0x0018, 0x5010);

		/// <summary>(0018,5012) VR=DS VM=1 Focus Depth</summary>
		DicomTags.FocusDepth = new DicomTag(0x0018, 0x5012);

		/// <summary>(0018,5020) VR=LO VM=1 Processing Function</summary>
		DicomTags.ProcessingFunction = new DicomTag(0x0018, 0x5020);

		/// <summary>(0018,5021) VR=LO VM=1 Postprocessing Function (Retired)</summary>
		DicomTags.PostprocessingFunctionRETIRED = new DicomTag(0x0018, 0x5021);

		/// <summary>(0018,5022) VR=DS VM=1 Mechanical Index</summary>
		DicomTags.MechanicalIndex = new DicomTag(0x0018, 0x5022);

		/// <summary>(0018,5024) VR=DS VM=1 Bone Thermal Index</summary>
		DicomTags.BoneThermalIndex = new DicomTag(0x0018, 0x5024);

		/// <summary>(0018,5026) VR=DS VM=1 Cranial Thermal Index</summary>
		DicomTags.CranialThermalIndex = new DicomTag(0x0018, 0x5026);

		/// <summary>(0018,5027) VR=DS VM=1 Soft Tissue Thermal Index</summary>
		DicomTags.SoftTissueThermalIndex = new DicomTag(0x0018, 0x5027);

		/// <summary>(0018,5028) VR=DS VM=1 Soft Tissue-focus Thermal Index</summary>
		DicomTags.SoftTissuefocusThermalIndex = new DicomTag(0x0018, 0x5028);

		/// <summary>(0018,5029) VR=DS VM=1 Soft Tissue-surface Thermal Index</summary>
		DicomTags.SoftTissuesurfaceThermalIndex = new DicomTag(0x0018, 0x5029);

		/// <summary>(0018,5030) VR=DS VM=1 Dynamic Range (Retired)</summary>
		DicomTags.DynamicRangeRETIRED = new DicomTag(0x0018, 0x5030);

		/// <summary>(0018,5040) VR=DS VM=1 Total Gain (Retired)</summary>
		DicomTags.TotalGainRETIRED = new DicomTag(0x0018, 0x5040);

		/// <summary>(0018,5050) VR=IS VM=1 Depth of Scan Field</summary>
		DicomTags.DepthOfScanField = new DicomTag(0x0018, 0x5050);

		/// <summary>(0018,5100) VR=CS VM=1 Patient Position</summary>
		DicomTags.PatientPosition = new DicomTag(0x0018, 0x5100);

		/// <summary>(0018,5101) VR=CS VM=1 View Position</summary>
		DicomTags.ViewPosition = new DicomTag(0x0018, 0x5101);

		/// <summary>(0018,5104) VR=SQ VM=1 Projection Eponymous Name Code Sequence</summary>
		DicomTags.ProjectionEponymousNameCodeSequence = new DicomTag(0x0018, 0x5104);

		/// <summary>(0018,5210) VR=DS VM=6 Image Transformation Matrix (Retired)</summary>
		DicomTags.ImageTransformationMatrixRETIRED = new DicomTag(0x0018, 0x5210);

		/// <summary>(0018,5212) VR=DS VM=3 Image Translation Vector (Retired)</summary>
		DicomTags.ImageTranslationVectorRETIRED = new DicomTag(0x0018, 0x5212);

		/// <summary>(0018,6000) VR=DS VM=1 Sensitivity</summary>
		DicomTags.Sensitivity = new DicomTag(0x0018, 0x6000);

		/// <summary>(0018,6011) VR=SQ VM=1 Sequence of Ultrasound Regions</summary>
		DicomTags.SequenceOfUltrasoundRegions = new DicomTag(0x0018, 0x6011);

		/// <summary>(0018,6012) VR=US VM=1 Region Spatial Format</summary>
		DicomTags.RegionSpatialFormat = new DicomTag(0x0018, 0x6012);

		/// <summary>(0018,6014) VR=US VM=1 Region Data Type</summary>
		DicomTags.RegionDataType = new DicomTag(0x0018, 0x6014);

		/// <summary>(0018,6016) VR=UL VM=1 Region Flags</summary>
		DicomTags.RegionFlags = new DicomTag(0x0018, 0x6016);

		/// <summary>(0018,6018) VR=UL VM=1 Region Location Min X0</summary>
		DicomTags.RegionLocationMinX0 = new DicomTag(0x0018, 0x6018);

		/// <summary>(0018,601a) VR=UL VM=1 Region Location Min Y0</summary>
		DicomTags.RegionLocationMinY0 = new DicomTag(0x0018, 0x601a);

		/// <summary>(0018,601c) VR=UL VM=1 Region Location Max X1</summary>
		DicomTags.RegionLocationMaxX1 = new DicomTag(0x0018, 0x601c);

		/// <summary>(0018,601e) VR=UL VM=1 Region Location Max Y1</summary>
		DicomTags.RegionLocationMaxY1 = new DicomTag(0x0018, 0x601e);

		/// <summary>(0018,6020) VR=SL VM=1 Reference Pixel X0</summary>
		DicomTags.ReferencePixelX0 = new DicomTag(0x0018, 0x6020);

		/// <summary>(0018,6022) VR=SL VM=1 Reference Pixel Y0</summary>
		DicomTags.ReferencePixelY0 = new DicomTag(0x0018, 0x6022);

		/// <summary>(0018,6024) VR=US VM=1 Physical Units X Direction</summary>
		DicomTags.PhysicalUnitsXDirection = new DicomTag(0x0018, 0x6024);

		/// <summary>(0018,6026) VR=US VM=1 Physical Units Y Direction</summary>
		DicomTags.PhysicalUnitsYDirection = new DicomTag(0x0018, 0x6026);

		/// <summary>(0018,6028) VR=FD VM=1 Reference Pixel Physical Value X</summary>
		DicomTags.ReferencePixelPhysicalValueX = new DicomTag(0x0018, 0x6028);

		/// <summary>(0018,602a) VR=FD VM=1 Reference Pixel Physical Value Y</summary>
		DicomTags.ReferencePixelPhysicalValueY = new DicomTag(0x0018, 0x602a);

		/// <summary>(0018,602c) VR=FD VM=1 Physical Delta X</summary>
		DicomTags.PhysicalDeltaX = new DicomTag(0x0018, 0x602c);

		/// <summary>(0018,602e) VR=FD VM=1 Physical Delta Y</summary>
		DicomTags.PhysicalDeltaY = new DicomTag(0x0018, 0x602e);

		/// <summary>(0018,6030) VR=UL VM=1 Transducer Frequency</summary>
		DicomTags.TransducerFrequency = new DicomTag(0x0018, 0x6030);

		/// <summary>(0018,6031) VR=CS VM=1 Transducer Type</summary>
		DicomTags.TransducerType = new DicomTag(0x0018, 0x6031);

		/// <summary>(0018,6032) VR=UL VM=1 Pulse Repetition Frequency</summary>
		DicomTags.PulseRepetitionFrequency = new DicomTag(0x0018, 0x6032);

		/// <summary>(0018,6034) VR=FD VM=1 Doppler Correction Angle</summary>
		DicomTags.DopplerCorrectionAngle = new DicomTag(0x0018, 0x6034);

		/// <summary>(0018,6036) VR=FD VM=1 Steering Angle</summary>
		DicomTags.SteeringAngle = new DicomTag(0x0018, 0x6036);

		/// <summary>(0018,6038) VR=UL VM=1 Doppler Sample Volume X Position (Retired) (Retired)</summary>
		DicomTags.DopplerSampleVolumeXPositionRetiredRETIRED = new DicomTag(0x0018, 0x6038);

		/// <summary>(0018,6039) VR=SL VM=1 Doppler Sample Volume X Position</summary>
		DicomTags.DopplerSampleVolumeXPosition = new DicomTag(0x0018, 0x6039);

		/// <summary>(0018,603a) VR=UL VM=1 Doppler Sample Volume Y Position (Retired) (Retired)</summary>
		DicomTags.DopplerSampleVolumeYPositionRetiredRETIRED = new DicomTag(0x0018, 0x603a);

		/// <summary>(0018,603b) VR=SL VM=1 Doppler Sample Volume Y Position</summary>
		DicomTags.DopplerSampleVolumeYPosition = new DicomTag(0x0018, 0x603b);

		/// <summary>(0018,603c) VR=UL VM=1 TM-Line Position X0 (Retired) (Retired)</summary>
		DicomTags.TMLinePositionX0RetiredRETIRED = new DicomTag(0x0018, 0x603c);

		/// <summary>(0018,603d) VR=SL VM=1 TM-Line Position X0</summary>
		DicomTags.TMLinePositionX0 = new DicomTag(0x0018, 0x603d);

		/// <summary>(0018,603e) VR=UL VM=1 TM-Line Position Y0 (Retired) (Retired)</summary>
		DicomTags.TMLinePositionY0RetiredRETIRED = new DicomTag(0x0018, 0x603e);

		/// <summary>(0018,603f) VR=SL VM=1 TM-Line Position Y0</summary>
		DicomTags.TMLinePositionY0 = new DicomTag(0x0018, 0x603f);

		/// <summary>(0018,6040) VR=UL VM=1 TM-Line Position X1 (Retired) (Retired)</summary>
		DicomTags.TMLinePositionX1RetiredRETIRED = new DicomTag(0x0018, 0x6040);

		/// <summary>(0018,6041) VR=SL VM=1 TM-Line Position X1</summary>
		DicomTags.TMLinePositionX1 = new DicomTag(0x0018, 0x6041);

		/// <summary>(0018,6042) VR=UL VM=1 TM-Line Position Y1 (Retired) (Retired)</summary>
		DicomTags.TMLinePositionY1RetiredRETIRED = new DicomTag(0x0018, 0x6042);

		/// <summary>(0018,6043) VR=SL VM=1 TM-Line Position Y1</summary>
		DicomTags.TMLinePositionY1 = new DicomTag(0x0018, 0x6043);

		/// <summary>(0018,6044) VR=US VM=1 Pixel Component Organization</summary>
		DicomTags.PixelComponentOrganization = new DicomTag(0x0018, 0x6044);

		/// <summary>(0018,6046) VR=UL VM=1 Pixel Component Mask</summary>
		DicomTags.PixelComponentMask = new DicomTag(0x0018, 0x6046);

		/// <summary>(0018,6048) VR=UL VM=1 Pixel Component Range Start</summary>
		DicomTags.PixelComponentRangeStart = new DicomTag(0x0018, 0x6048);

		/// <summary>(0018,604a) VR=UL VM=1 Pixel Component Range Stop</summary>
		DicomTags.PixelComponentRangeStop = new DicomTag(0x0018, 0x604a);

		/// <summary>(0018,604c) VR=US VM=1 Pixel Component Physical Units</summary>
		DicomTags.PixelComponentPhysicalUnits = new DicomTag(0x0018, 0x604c);

		/// <summary>(0018,604e) VR=US VM=1 Pixel Component Data Type</summary>
		DicomTags.PixelComponentDataType = new DicomTag(0x0018, 0x604e);

		/// <summary>(0018,6050) VR=UL VM=1 Number of Table Break Points</summary>
		DicomTags.NumberOfTableBreakPoints = new DicomTag(0x0018, 0x6050);

		/// <summary>(0018,6052) VR=UL VM=1-n Table of X Break Points</summary>
		DicomTags.TableOfXBreakPoints = new DicomTag(0x0018, 0x6052);

		/// <summary>(0018,6054) VR=FD VM=1-n Table of Y Break Points</summary>
		DicomTags.TableOfYBreakPoints = new DicomTag(0x0018, 0x6054);

		/// <summary>(0018,6056) VR=UL VM=1 Number of Table Entries</summary>
		DicomTags.NumberOfTableEntries = new DicomTag(0x0018, 0x6056);

		/// <summary>(0018,6058) VR=UL VM=1-n Table of Pixel Values</summary>
		DicomTags.TableOfPixelValues = new DicomTag(0x0018, 0x6058);

		/// <summary>(0018,605a) VR=FL VM=1-n Table of Parameter Values</summary>
		DicomTags.TableOfParameterValues = new DicomTag(0x0018, 0x605a);

		/// <summary>(0018,6060) VR=FL VM=1-n R Wave Time Vector</summary>
		DicomTags.RWaveTimeVector = new DicomTag(0x0018, 0x6060);

		/// <summary>(0018,7000) VR=CS VM=1 Detector Conditions Nominal Flag</summary>
		DicomTags.DetectorConditionsNominalFlag = new DicomTag(0x0018, 0x7000);

		/// <summary>(0018,7001) VR=DS VM=1 Detector Temperature</summary>
		DicomTags.DetectorTemperature = new DicomTag(0x0018, 0x7001);

		/// <summary>(0018,7004) VR=CS VM=1 Detector Type</summary>
		DicomTags.DetectorType = new DicomTag(0x0018, 0x7004);

		/// <summary>(0018,7005) VR=CS VM=1 Detector Configuration</summary>
		DicomTags.DetectorConfiguration = new DicomTag(0x0018, 0x7005);

		/// <summary>(0018,7006) VR=LT VM=1 Detector Description</summary>
		DicomTags.DetectorDescription = new DicomTag(0x0018, 0x7006);

		/// <summary>(0018,7008) VR=LT VM=1 Detector Mode</summary>
		DicomTags.DetectorMode = new DicomTag(0x0018, 0x7008);

		/// <summary>(0018,700a) VR=SH VM=1 Detector ID</summary>
		DicomTags.DetectorID = new DicomTag(0x0018, 0x700a);

		/// <summary>(0018,700c) VR=DA VM=1 Date of Last Detector Calibration</summary>
		DicomTags.DateOfLastDetectorCalibration = new DicomTag(0x0018, 0x700c);

		/// <summary>(0018,700e) VR=TM VM=1 Time of Last Detector Calibration</summary>
		DicomTags.TimeOfLastDetectorCalibration = new DicomTag(0x0018, 0x700e);

		/// <summary>(0018,7010) VR=IS VM=1 Exposures on Detector Since Last Calibration</summary>
		DicomTags.ExposuresOnDetectorSinceLastCalibration = new DicomTag(0x0018, 0x7010);

		/// <summary>(0018,7011) VR=IS VM=1 Exposures on Detector Since Manufactured</summary>
		DicomTags.ExposuresOnDetectorSinceManufactured = new DicomTag(0x0018, 0x7011);

		/// <summary>(0018,7012) VR=DS VM=1 Detector Time Since Last Exposure</summary>
		DicomTags.DetectorTimeSinceLastExposure = new DicomTag(0x0018, 0x7012);

		/// <summary>(0018,7014) VR=DS VM=1 Detector Active Time</summary>
		DicomTags.DetectorActiveTime = new DicomTag(0x0018, 0x7014);

		/// <summary>(0018,7016) VR=DS VM=1 Detector Activation Offset From Exposure</summary>
		DicomTags.DetectorActivationOffsetFromExposure = new DicomTag(0x0018, 0x7016);

		/// <summary>(0018,701a) VR=DS VM=2 Detector Binning</summary>
		DicomTags.DetectorBinning = new DicomTag(0x0018, 0x701a);

		/// <summary>(0018,7020) VR=DS VM=2 Detector Element Physical Size</summary>
		DicomTags.DetectorElementPhysicalSize = new DicomTag(0x0018, 0x7020);

		/// <summary>(0018,7022) VR=DS VM=2 Detector Element Spacing</summary>
		DicomTags.DetectorElementSpacing = new DicomTag(0x0018, 0x7022);

		/// <summary>(0018,7024) VR=CS VM=1 Detector Active Shape</summary>
		DicomTags.DetectorActiveShape = new DicomTag(0x0018, 0x7024);

		/// <summary>(0018,7026) VR=DS VM=1-2 Detector Active Dimension(s)</summary>
		DicomTags.DetectorActiveDimensions = new DicomTag(0x0018, 0x7026);

		/// <summary>(0018,7028) VR=DS VM=2 Detector Active Origin</summary>
		DicomTags.DetectorActiveOrigin = new DicomTag(0x0018, 0x7028);

		/// <summary>(0018,702a) VR=LO VM=1 Detector Manufacturer Name</summary>
		DicomTags.DetectorManufacturerName = new DicomTag(0x0018, 0x702a);

		/// <summary>(0018,702b) VR=LO VM=1 Detector Manufacturer's Model Name</summary>
		DicomTags.DetectorManufacturersModelName = new DicomTag(0x0018, 0x702b);

		/// <summary>(0018,7030) VR=DS VM=2 Field of View Origin</summary>
		DicomTags.FieldOfViewOrigin = new DicomTag(0x0018, 0x7030);

		/// <summary>(0018,7032) VR=DS VM=1 Field of View Rotation</summary>
		DicomTags.FieldOfViewRotation = new DicomTag(0x0018, 0x7032);

		/// <summary>(0018,7034) VR=CS VM=1 Field of View Horizontal Flip</summary>
		DicomTags.FieldOfViewHorizontalFlip = new DicomTag(0x0018, 0x7034);

		/// <summary>(0018,7040) VR=LT VM=1 Grid Absorbing Material</summary>
		DicomTags.GridAbsorbingMaterial = new DicomTag(0x0018, 0x7040);

		/// <summary>(0018,7041) VR=LT VM=1 Grid Spacing Material</summary>
		DicomTags.GridSpacingMaterial = new DicomTag(0x0018, 0x7041);

		/// <summary>(0018,7042) VR=DS VM=1 Grid Thickness</summary>
		DicomTags.GridThickness = new DicomTag(0x0018, 0x7042);

		/// <summary>(0018,7044) VR=DS VM=1 Grid Pitch</summary>
		DicomTags.GridPitch = new DicomTag(0x0018, 0x7044);

		/// <summary>(0018,7046) VR=IS VM=2 Grid Aspect Ratio</summary>
		DicomTags.GridAspectRatio = new DicomTag(0x0018, 0x7046);

		/// <summary>(0018,7048) VR=DS VM=1 Grid Period</summary>
		DicomTags.GridPeriod = new DicomTag(0x0018, 0x7048);

		/// <summary>(0018,704c) VR=DS VM=1 Grid Focal Distance</summary>
		DicomTags.GridFocalDistance = new DicomTag(0x0018, 0x704c);

		/// <summary>(0018,7050) VR=CS VM=1-n Filter Material</summary>
		DicomTags.FilterMaterial = new DicomTag(0x0018, 0x7050);

		/// <summary>(0018,7052) VR=DS VM=1-n Filter Thickness Minimum</summary>
		DicomTags.FilterThicknessMinimum = new DicomTag(0x0018, 0x7052);

		/// <summary>(0018,7054) VR=DS VM=1-n Filter Thickness Maximum</summary>
		DicomTags.FilterThicknessMaximum = new DicomTag(0x0018, 0x7054);

		/// <summary>(0018,7060) VR=CS VM=1 Exposure Control Mode</summary>
		DicomTags.ExposureControlMode = new DicomTag(0x0018, 0x7060);

		/// <summary>(0018,7062) VR=LT VM=1 Exposure Control Mode Description</summary>
		DicomTags.ExposureControlModeDescription = new DicomTag(0x0018, 0x7062);

		/// <summary>(0018,7064) VR=CS VM=1 Exposure Status</summary>
		DicomTags.ExposureStatus = new DicomTag(0x0018, 0x7064);

		/// <summary>(0018,7065) VR=DS VM=1 Phototimer Setting</summary>
		DicomTags.PhototimerSetting = new DicomTag(0x0018, 0x7065);

		/// <summary>(0018,8150) VR=DS VM=1 Exposure Time in uS</summary>
		DicomTags.ExposureTimeInMicroS = new DicomTag(0x0018, 0x8150);

		/// <summary>(0018,8151) VR=DS VM=1 X-Ray Tube Current in uA</summary>
		DicomTags.XRayTubeCurrentInMicroA = new DicomTag(0x0018, 0x8151);

		/// <summary>(0018,9004) VR=CS VM=1 Content Qualification</summary>
		DicomTags.ContentQualification = new DicomTag(0x0018, 0x9004);

		/// <summary>(0018,9005) VR=SH VM=1 Pulse Sequence Name</summary>
		DicomTags.PulseSequenceName = new DicomTag(0x0018, 0x9005);

		/// <summary>(0018,9006) VR=SQ VM=1 MR Imaging Modifier Sequence</summary>
		DicomTags.MRImagingModifierSequence = new DicomTag(0x0018, 0x9006);

		/// <summary>(0018,9008) VR=CS VM=1 Echo Pulse Sequence</summary>
		DicomTags.EchoPulseSequence = new DicomTag(0x0018, 0x9008);

		/// <summary>(0018,9009) VR=CS VM=1 Inversion Recovery</summary>
		DicomTags.InversionRecovery = new DicomTag(0x0018, 0x9009);

		/// <summary>(0018,9010) VR=CS VM=1 Flow Compensation</summary>
		DicomTags.FlowCompensation = new DicomTag(0x0018, 0x9010);

		/// <summary>(0018,9011) VR=CS VM=1 Multiple Spin Echo</summary>
		DicomTags.MultipleSpinEcho = new DicomTag(0x0018, 0x9011);

		/// <summary>(0018,9012) VR=CS VM=1 Multi-planar Excitation</summary>
		DicomTags.MultiplanarExcitation = new DicomTag(0x0018, 0x9012);

		/// <summary>(0018,9014) VR=CS VM=1 Phase Contrast</summary>
		DicomTags.PhaseContrast = new DicomTag(0x0018, 0x9014);

		/// <summary>(0018,9015) VR=CS VM=1 Time of Flight Contrast</summary>
		DicomTags.TimeOfFlightContrast = new DicomTag(0x0018, 0x9015);

		/// <summary>(0018,9016) VR=CS VM=1 Spoiling</summary>
		DicomTags.Spoiling = new DicomTag(0x0018, 0x9016);

		/// <summary>(0018,9017) VR=CS VM=1 Steady State Pulse Sequence</summary>
		DicomTags.SteadyStatePulseSequence = new DicomTag(0x0018, 0x9017);

		/// <summary>(0018,9018) VR=CS VM=1 Echo Planar Pulse Sequence</summary>
		DicomTags.EchoPlanarPulseSequence = new DicomTag(0x0018, 0x9018);

		/// <summary>(0018,9019) VR=FD VM=1 Tag Angle First Axis</summary>
		DicomTags.TagAngleFirstAxis = new DicomTag(0x0018, 0x9019);

		/// <summary>(0018,9020) VR=CS VM=1 Magnetization Transfer</summary>
		DicomTags.MagnetizationTransfer = new DicomTag(0x0018, 0x9020);

		/// <summary>(0018,9021) VR=CS VM=1 T2 Preparation</summary>
		DicomTags.T2Preparation = new DicomTag(0x0018, 0x9021);

		/// <summary>(0018,9022) VR=CS VM=1 Blood Signal Nulling</summary>
		DicomTags.BloodSignalNulling = new DicomTag(0x0018, 0x9022);

		/// <summary>(0018,9024) VR=CS VM=1 Saturation Recovery</summary>
		DicomTags.SaturationRecovery = new DicomTag(0x0018, 0x9024);

		/// <summary>(0018,9025) VR=CS VM=1 Spectrally Selected Suppression</summary>
		DicomTags.SpectrallySelectedSuppression = new DicomTag(0x0018, 0x9025);

		/// <summary>(0018,9026) VR=CS VM=1 Spectrally Selected Excitation</summary>
		DicomTags.SpectrallySelectedExcitation = new DicomTag(0x0018, 0x9026);

		/// <summary>(0018,9027) VR=CS VM=1 Spatial Pre-saturation</summary>
		DicomTags.SpatialPresaturation = new DicomTag(0x0018, 0x9027);

		/// <summary>(0018,9028) VR=CS VM=1 Tagging</summary>
		DicomTags.Tagging = new DicomTag(0x0018, 0x9028);

		/// <summary>(0018,9029) VR=CS VM=1 Oversampling Phase</summary>
		DicomTags.OversamplingPhase = new DicomTag(0x0018, 0x9029);

		/// <summary>(0018,9030) VR=FD VM=1 Tag Spacing First Dimension</summary>
		DicomTags.TagSpacingFirstDimension = new DicomTag(0x0018, 0x9030);

		/// <summary>(0018,9032) VR=CS VM=1 Geometry of k-Space Traversal</summary>
		DicomTags.GeometryOfKSpaceTraversal = new DicomTag(0x0018, 0x9032);

		/// <summary>(0018,9033) VR=CS VM=1 Segmented k-Space Traversal</summary>
		DicomTags.SegmentedKSpaceTraversal = new DicomTag(0x0018, 0x9033);

		/// <summary>(0018,9034) VR=CS VM=1 Rectilinear Phase Encode Reordering</summary>
		DicomTags.RectilinearPhaseEncodeReordering = new DicomTag(0x0018, 0x9034);

		/// <summary>(0018,9035) VR=FD VM=1 Tag Thickness</summary>
		DicomTags.TagThickness = new DicomTag(0x0018, 0x9035);

		/// <summary>(0018,9036) VR=CS VM=1 Partial Fourier Direction</summary>
		DicomTags.PartialFourierDirection = new DicomTag(0x0018, 0x9036);

		/// <summary>(0018,9037) VR=CS VM=1 Cardiac Synchronization Technique</summary>
		DicomTags.CardiacSynchronizationTechnique = new DicomTag(0x0018, 0x9037);

		/// <summary>(0018,9041) VR=LO VM=1 Receive Coil Manufacturer Name</summary>
		DicomTags.ReceiveCoilManufacturerName = new DicomTag(0x0018, 0x9041);

		/// <summary>(0018,9042) VR=SQ VM=1 MR Receive Coil Sequence</summary>
		DicomTags.MRReceiveCoilSequence = new DicomTag(0x0018, 0x9042);

		/// <summary>(0018,9043) VR=CS VM=1 Receive Coil Type</summary>
		DicomTags.ReceiveCoilType = new DicomTag(0x0018, 0x9043);

		/// <summary>(0018,9044) VR=CS VM=1 Quadrature Receive Coil</summary>
		DicomTags.QuadratureReceiveCoil = new DicomTag(0x0018, 0x9044);

		/// <summary>(0018,9045) VR=SQ VM=1 Multi-Coil Definition Sequence</summary>
		DicomTags.MultiCoilDefinitionSequence = new DicomTag(0x0018, 0x9045);

		/// <summary>(0018,9046) VR=LO VM=1 Multi-Coil Configuration</summary>
		DicomTags.MultiCoilConfiguration = new DicomTag(0x0018, 0x9046);

		/// <summary>(0018,9047) VR=SH VM=1 Multi-Coil Element Name</summary>
		DicomTags.MultiCoilElementName = new DicomTag(0x0018, 0x9047);

		/// <summary>(0018,9048) VR=CS VM=1 Multi-Coil Element Used</summary>
		DicomTags.MultiCoilElementUsed = new DicomTag(0x0018, 0x9048);

		/// <summary>(0018,9049) VR=SQ VM=1 MR Transmit Coil Sequence</summary>
		DicomTags.MRTransmitCoilSequence = new DicomTag(0x0018, 0x9049);

		/// <summary>(0018,9050) VR=LO VM=1 Transmit Coil Manufacturer Name</summary>
		DicomTags.TransmitCoilManufacturerName = new DicomTag(0x0018, 0x9050);

		/// <summary>(0018,9051) VR=CS VM=1 Transmit Coil Type</summary>
		DicomTags.TransmitCoilType = new DicomTag(0x0018, 0x9051);

		/// <summary>(0018,9052) VR=FD VM=1-2 Spectral Width</summary>
		DicomTags.SpectralWidth = new DicomTag(0x0018, 0x9052);

		/// <summary>(0018,9053) VR=FD VM=1-2 Chemical Shift Reference</summary>
		DicomTags.ChemicalShiftReference = new DicomTag(0x0018, 0x9053);

		/// <summary>(0018,9054) VR=CS VM=1 Volume Localization Technique</summary>
		DicomTags.VolumeLocalizationTechnique = new DicomTag(0x0018, 0x9054);

		/// <summary>(0018,9058) VR=US VM=1 MR Acquisition Frequency Encoding Steps</summary>
		DicomTags.MRAcquisitionFrequencyEncodingSteps = new DicomTag(0x0018, 0x9058);

		/// <summary>(0018,9059) VR=CS VM=1 De-coupling</summary>
		DicomTags.Decoupling = new DicomTag(0x0018, 0x9059);

		/// <summary>(0018,9060) VR=CS VM=1-2 De-coupled Nucleus</summary>
		DicomTags.DecoupledNucleus = new DicomTag(0x0018, 0x9060);

		/// <summary>(0018,9061) VR=FD VM=1-2 De-coupling Frequency</summary>
		DicomTags.DecouplingFrequency = new DicomTag(0x0018, 0x9061);

		/// <summary>(0018,9062) VR=CS VM=1 De-coupling Method</summary>
		DicomTags.DecouplingMethod = new DicomTag(0x0018, 0x9062);

		/// <summary>(0018,9063) VR=FD VM=1-2 De-coupling Chemical Shift Reference</summary>
		DicomTags.DecouplingChemicalShiftReference = new DicomTag(0x0018, 0x9063);

		/// <summary>(0018,9064) VR=CS VM=1 k-space Filtering</summary>
		DicomTags.KspaceFiltering = new DicomTag(0x0018, 0x9064);

		/// <summary>(0018,9065) VR=CS VM=1-2 Time Domain Filtering</summary>
		DicomTags.TimeDomainFiltering = new DicomTag(0x0018, 0x9065);

		/// <summary>(0018,9066) VR=US VM=1-2 Number of Zero fills</summary>
		DicomTags.NumberOfZeroFills = new DicomTag(0x0018, 0x9066);

		/// <summary>(0018,9067) VR=CS VM=1 Baseline Correction</summary>
		DicomTags.BaselineCorrection = new DicomTag(0x0018, 0x9067);

		/// <summary>(0018,9069) VR=FD VM=1 Parallel Reduction Factor In-plane</summary>
		DicomTags.ParallelReductionFactorInplane = new DicomTag(0x0018, 0x9069);

		/// <summary>(0018,9070) VR=FD VM=1 Cardiac R-R Interval Specified</summary>
		DicomTags.CardiacRRIntervalSpecified = new DicomTag(0x0018, 0x9070);

		/// <summary>(0018,9073) VR=FD VM=1 Acquisition Duration</summary>
		DicomTags.AcquisitionDuration = new DicomTag(0x0018, 0x9073);

		/// <summary>(0018,9074) VR=DT VM=1 Frame Acquisition DateTime</summary>
		DicomTags.FrameAcquisitionDateTime = new DicomTag(0x0018, 0x9074);

		/// <summary>(0018,9075) VR=CS VM=1 Diffusion Directionality</summary>
		DicomTags.DiffusionDirectionality = new DicomTag(0x0018, 0x9075);

		/// <summary>(0018,9076) VR=SQ VM=1 Diffusion Gradient Direction Sequence</summary>
		DicomTags.DiffusionGradientDirectionSequence = new DicomTag(0x0018, 0x9076);

		/// <summary>(0018,9077) VR=CS VM=1 Parallel Acquisition</summary>
		DicomTags.ParallelAcquisition = new DicomTag(0x0018, 0x9077);

		/// <summary>(0018,9078) VR=CS VM=1 Parallel Acquisition Technique</summary>
		DicomTags.ParallelAcquisitionTechnique = new DicomTag(0x0018, 0x9078);

		/// <summary>(0018,9079) VR=FD VM=1-n Inversion Times</summary>
		DicomTags.InversionTimes = new DicomTag(0x0018, 0x9079);

		/// <summary>(0018,9080) VR=ST VM=1 Metabolite Map Description</summary>
		DicomTags.MetaboliteMapDescription = new DicomTag(0x0018, 0x9080);

		/// <summary>(0018,9081) VR=CS VM=1 Partial Fourier</summary>
		DicomTags.PartialFourier = new DicomTag(0x0018, 0x9081);

		/// <summary>(0018,9082) VR=FD VM=1 Effective Echo Time</summary>
		DicomTags.EffectiveEchoTime = new DicomTag(0x0018, 0x9082);

		/// <summary>(0018,9083) VR=SQ VM=1 Metabolite Map Code Sequence</summary>
		DicomTags.MetaboliteMapCodeSequence = new DicomTag(0x0018, 0x9083);

		/// <summary>(0018,9084) VR=SQ VM=1 Chemical Shift Sequence</summary>
		DicomTags.ChemicalShiftSequence = new DicomTag(0x0018, 0x9084);

		/// <summary>(0018,9085) VR=CS VM=1 Cardiac Signal Source</summary>
		DicomTags.CardiacSignalSource = new DicomTag(0x0018, 0x9085);

		/// <summary>(0018,9087) VR=FD VM=1 Diffusion b-value</summary>
		DicomTags.DiffusionBvalue = new DicomTag(0x0018, 0x9087);

		/// <summary>(0018,9089) VR=FD VM=3 Diffusion Gradient Orientation</summary>
		DicomTags.DiffusionGradientOrientation = new DicomTag(0x0018, 0x9089);

		/// <summary>(0018,9090) VR=FD VM=3 Velocity Encoding Direction</summary>
		DicomTags.VelocityEncodingDirection = new DicomTag(0x0018, 0x9090);

		/// <summary>(0018,9091) VR=FD VM=1 Velocity Encoding Minimum Value</summary>
		DicomTags.VelocityEncodingMinimumValue = new DicomTag(0x0018, 0x9091);

		/// <summary>(0018,9093) VR=US VM=1 Number of k-Space Trajectories</summary>
		DicomTags.NumberOfKSpaceTrajectories = new DicomTag(0x0018, 0x9093);

		/// <summary>(0018,9094) VR=CS VM=1 Coverage of k-Space</summary>
		DicomTags.CoverageOfKSpace = new DicomTag(0x0018, 0x9094);

		/// <summary>(0018,9095) VR=UL VM=1 Spectroscopy Acquisition Phase Rows</summary>
		DicomTags.SpectroscopyAcquisitionPhaseRows = new DicomTag(0x0018, 0x9095);

		/// <summary>(0018,9096) VR=FD VM=1 Parallel Reduction Factor In-plane (Retired) (Retired)</summary>
		DicomTags.ParallelReductionFactorInplaneRetiredRETIRED = new DicomTag(0x0018, 0x9096);

		/// <summary>(0018,9098) VR=FD VM=1-2 Transmitter Frequency</summary>
		DicomTags.TransmitterFrequency = new DicomTag(0x0018, 0x9098);

		/// <summary>(0018,9100) VR=CS VM=1-2 Resonant Nucleus</summary>
		DicomTags.ResonantNucleus = new DicomTag(0x0018, 0x9100);

		/// <summary>(0018,9101) VR=CS VM=1 Frequency Correction</summary>
		DicomTags.FrequencyCorrection = new DicomTag(0x0018, 0x9101);

		/// <summary>(0018,9103) VR=SQ VM=1 MR Spectroscopy FOV/Geometry Sequence</summary>
		DicomTags.MRSpectroscopyFOVGeometrySequence = new DicomTag(0x0018, 0x9103);

		/// <summary>(0018,9104) VR=FD VM=1 Slab Thickness</summary>
		DicomTags.SlabThickness = new DicomTag(0x0018, 0x9104);

		/// <summary>(0018,9105) VR=FD VM=3 Slab Orientation</summary>
		DicomTags.SlabOrientation = new DicomTag(0x0018, 0x9105);

		/// <summary>(0018,9106) VR=FD VM=3 Mid Slab Position</summary>
		DicomTags.MidSlabPosition = new DicomTag(0x0018, 0x9106);

		/// <summary>(0018,9107) VR=SQ VM=1 MR Spatial Saturation Sequence</summary>
		DicomTags.MRSpatialSaturationSequence = new DicomTag(0x0018, 0x9107);

		/// <summary>(0018,9112) VR=SQ VM=1 MR Timing and Related Parameters Sequence</summary>
		DicomTags.MRTimingAndRelatedParametersSequence = new DicomTag(0x0018, 0x9112);

		/// <summary>(0018,9114) VR=SQ VM=1 MR Echo Sequence</summary>
		DicomTags.MREchoSequence = new DicomTag(0x0018, 0x9114);

		/// <summary>(0018,9115) VR=SQ VM=1 MR Modifier Sequence</summary>
		DicomTags.MRModifierSequence = new DicomTag(0x0018, 0x9115);

		/// <summary>(0018,9117) VR=SQ VM=1 MR Diffusion Sequence</summary>
		DicomTags.MRDiffusionSequence = new DicomTag(0x0018, 0x9117);

		/// <summary>(0018,9118) VR=SQ VM=1 Cardiac Synchronization Sequence</summary>
		DicomTags.CardiacSynchronizationSequence = new DicomTag(0x0018, 0x9118);

		/// <summary>(0018,9119) VR=SQ VM=1 MR Averages Sequence</summary>
		DicomTags.MRAveragesSequence = new DicomTag(0x0018, 0x9119);

		/// <summary>(0018,9125) VR=SQ VM=1 MR FOV/Geometry Sequence</summary>
		DicomTags.MRFOVGeometrySequence = new DicomTag(0x0018, 0x9125);

		/// <summary>(0018,9126) VR=SQ VM=1 Volume Localization Sequence</summary>
		DicomTags.VolumeLocalizationSequence = new DicomTag(0x0018, 0x9126);

		/// <summary>(0018,9127) VR=UL VM=1 Spectroscopy Acquisition Data Columns</summary>
		DicomTags.SpectroscopyAcquisitionDataColumns = new DicomTag(0x0018, 0x9127);

		/// <summary>(0018,9147) VR=CS VM=1 Diffusion Anisotropy Type</summary>
		DicomTags.DiffusionAnisotropyType = new DicomTag(0x0018, 0x9147);

		/// <summary>(0018,9151) VR=DT VM=1 Frame Reference DateTime</summary>
		DicomTags.FrameReferenceDateTime = new DicomTag(0x0018, 0x9151);

		/// <summary>(0018,9152) VR=SQ VM=1 MR Metabolite Map Sequence</summary>
		DicomTags.MRMetaboliteMapSequence = new DicomTag(0x0018, 0x9152);

		/// <summary>(0018,9155) VR=FD VM=1 Parallel Reduction Factor out-of-plane</summary>
		DicomTags.ParallelReductionFactorOutofplane = new DicomTag(0x0018, 0x9155);

		/// <summary>(0018,9159) VR=UL VM=1 Spectroscopy Acquisition Out-of-plane Phase Steps</summary>
		DicomTags.SpectroscopyAcquisitionOutofplanePhaseSteps = new DicomTag(0x0018, 0x9159);

		/// <summary>(0018,9166) VR=CS VM=1 Bulk Motion Status (Retired)</summary>
		DicomTags.BulkMotionStatusRETIRED = new DicomTag(0x0018, 0x9166);

		/// <summary>(0018,9168) VR=FD VM=1 Parallel Reduction Factor Second In-plane</summary>
		DicomTags.ParallelReductionFactorSecondInplane = new DicomTag(0x0018, 0x9168);

		/// <summary>(0018,9169) VR=CS VM=1 Cardiac Beat Rejection Technique</summary>
		DicomTags.CardiacBeatRejectionTechnique = new DicomTag(0x0018, 0x9169);

		/// <summary>(0018,9170) VR=CS VM=1 Respiratory Motion Compensation Technique</summary>
		DicomTags.RespiratoryMotionCompensationTechnique = new DicomTag(0x0018, 0x9170);

		/// <summary>(0018,9171) VR=CS VM=1 Respiratory Signal Source</summary>
		DicomTags.RespiratorySignalSource = new DicomTag(0x0018, 0x9171);

		/// <summary>(0018,9172) VR=CS VM=1 Bulk Motion Compensation Technique</summary>
		DicomTags.BulkMotionCompensationTechnique = new DicomTag(0x0018, 0x9172);

		/// <summary>(0018,9173) VR=CS VM=1 Bulk Motion Signal Source</summary>
		DicomTags.BulkMotionSignalSource = new DicomTag(0x0018, 0x9173);

		/// <summary>(0018,9174) VR=CS VM=1 Applicable Safety Standard Agency</summary>
		DicomTags.ApplicableSafetyStandardAgency = new DicomTag(0x0018, 0x9174);

		/// <summary>(0018,9175) VR=LO VM=1 Applicable Safety Standard Description</summary>
		DicomTags.ApplicableSafetyStandardDescription = new DicomTag(0x0018, 0x9175);

		/// <summary>(0018,9176) VR=SQ VM=1 Operating Mode Sequence</summary>
		DicomTags.OperatingModeSequence = new DicomTag(0x0018, 0x9176);

		/// <summary>(0018,9177) VR=CS VM=1 Operating Mode Type</summary>
		DicomTags.OperatingModeType = new DicomTag(0x0018, 0x9177);

		/// <summary>(0018,9178) VR=CS VM=1 Operating Mode</summary>
		DicomTags.OperatingMode = new DicomTag(0x0018, 0x9178);

		/// <summary>(0018,9179) VR=CS VM=1 Specific Absorption Rate Definition</summary>
		DicomTags.SpecificAbsorptionRateDefinition = new DicomTag(0x0018, 0x9179);

		/// <summary>(0018,9180) VR=CS VM=1 Gradient Output Type</summary>
		DicomTags.GradientOutputType = new DicomTag(0x0018, 0x9180);

		/// <summary>(0018,9181) VR=FD VM=1 Specific Absorption Rate Value</summary>
		DicomTags.SpecificAbsorptionRateValue = new DicomTag(0x0018, 0x9181);

		/// <summary>(0018,9182) VR=FD VM=1 Gradient Output</summary>
		DicomTags.GradientOutput = new DicomTag(0x0018, 0x9182);

		/// <summary>(0018,9183) VR=CS VM=1 Flow Compensation Direction</summary>
		DicomTags.FlowCompensationDirection = new DicomTag(0x0018, 0x9183);

		/// <summary>(0018,9184) VR=FD VM=1 Tagging Delay</summary>
		DicomTags.TaggingDelay = new DicomTag(0x0018, 0x9184);

		/// <summary>(0018,9185) VR=ST VM=1 Respiratory Motion Compensation Technique Description</summary>
		DicomTags.RespiratoryMotionCompensationTechniqueDescription = new DicomTag(0x0018, 0x9185);

		/// <summary>(0018,9186) VR=SH VM=1 Respiratory Signal Source ID</summary>
		DicomTags.RespiratorySignalSourceID = new DicomTag(0x0018, 0x9186);

		/// <summary>(0018,9195) VR=FD VM=1 Chemical Shifts Minimum Integration Limit in Hz (Retired)</summary>
		DicomTags.ChemicalShiftsMinimumIntegrationLimitInHzRETIRED = new DicomTag(0x0018, 0x9195);

		/// <summary>(0018,9196) VR=FD VM=1 Chemical Shifts Maximum Integration Limit in Hz (Retired)</summary>
		DicomTags.ChemicalShiftsMaximumIntegrationLimitInHzRETIRED = new DicomTag(0x0018, 0x9196);

		/// <summary>(0018,9197) VR=SQ VM=1 MR Velocity Encoding Sequence</summary>
		DicomTags.MRVelocityEncodingSequence = new DicomTag(0x0018, 0x9197);

		/// <summary>(0018,9198) VR=CS VM=1 First Order Phase Correction</summary>
		DicomTags.FirstOrderPhaseCorrection = new DicomTag(0x0018, 0x9198);

		/// <summary>(0018,9199) VR=CS VM=1 Water Referenced Phase Correction</summary>
		DicomTags.WaterReferencedPhaseCorrection = new DicomTag(0x0018, 0x9199);

		/// <summary>(0018,9200) VR=CS VM=1 MR Spectroscopy Acquisition Type</summary>
		DicomTags.MRSpectroscopyAcquisitionType = new DicomTag(0x0018, 0x9200);

		/// <summary>(0018,9214) VR=CS VM=1 Respiratory Cycle Position</summary>
		DicomTags.RespiratoryCyclePosition = new DicomTag(0x0018, 0x9214);

		/// <summary>(0018,9217) VR=FD VM=1 Velocity Encoding Maximum Value</summary>
		DicomTags.VelocityEncodingMaximumValue = new DicomTag(0x0018, 0x9217);

		/// <summary>(0018,9218) VR=FD VM=1 Tag Spacing Second Dimension</summary>
		DicomTags.TagSpacingSecondDimension = new DicomTag(0x0018, 0x9218);

		/// <summary>(0018,9219) VR=SS VM=1 Tag Angle Second Axis</summary>
		DicomTags.TagAngleSecondAxis = new DicomTag(0x0018, 0x9219);

		/// <summary>(0018,9220) VR=FD VM=1 Frame Acquisition Duration</summary>
		DicomTags.FrameAcquisitionDuration = new DicomTag(0x0018, 0x9220);

		/// <summary>(0018,9226) VR=SQ VM=1 MR Image Frame Type Sequence</summary>
		DicomTags.MRImageFrameTypeSequence = new DicomTag(0x0018, 0x9226);

		/// <summary>(0018,9227) VR=SQ VM=1 MR Spectroscopy Frame Type Sequence</summary>
		DicomTags.MRSpectroscopyFrameTypeSequence = new DicomTag(0x0018, 0x9227);

		/// <summary>(0018,9231) VR=US VM=1 MR Acquisition Phase Encoding Steps in-plane</summary>
		DicomTags.MRAcquisitionPhaseEncodingStepsInplane = new DicomTag(0x0018, 0x9231);

		/// <summary>(0018,9232) VR=US VM=1 MR Acquisition Phase Encoding Steps out-of-plane</summary>
		DicomTags.MRAcquisitionPhaseEncodingStepsOutofplane = new DicomTag(0x0018, 0x9232);

		/// <summary>(0018,9234) VR=UL VM=1 Spectroscopy Acquisition Phase Columns</summary>
		DicomTags.SpectroscopyAcquisitionPhaseColumns = new DicomTag(0x0018, 0x9234);

		/// <summary>(0018,9236) VR=CS VM=1 Cardiac Cycle Position</summary>
		DicomTags.CardiacCyclePosition = new DicomTag(0x0018, 0x9236);

		/// <summary>(0018,9239) VR=SQ VM=1 Specific Absorption Rate Sequence</summary>
		DicomTags.SpecificAbsorptionRateSequence = new DicomTag(0x0018, 0x9239);

		/// <summary>(0018,9240) VR=US VM=1 RF Echo Train Length</summary>
		DicomTags.RFEchoTrainLength = new DicomTag(0x0018, 0x9240);

		/// <summary>(0018,9241) VR=US VM=1 Gradient Echo Train Length</summary>
		DicomTags.GradientEchoTrainLength = new DicomTag(0x0018, 0x9241);

		/// <summary>(0018,9295) VR=FD VM=1 Chemical Shifts Minimum Integration Limit in ppm</summary>
		DicomTags.ChemicalShiftsMinimumIntegrationLimitInPpm = new DicomTag(0x0018, 0x9295);

		/// <summary>(0018,9296) VR=FD VM=1 Chemical Shifts Maximum Integration Limit in ppm</summary>
		DicomTags.ChemicalShiftsMaximumIntegrationLimitInPpm = new DicomTag(0x0018, 0x9296);

		/// <summary>(0018,9301) VR=SQ VM=1 CT Acquisition Type Sequence</summary>
		DicomTags.CTAcquisitionTypeSequence = new DicomTag(0x0018, 0x9301);

		/// <summary>(0018,9302) VR=CS VM=1 Acquisition Type</summary>
		DicomTags.AcquisitionType = new DicomTag(0x0018, 0x9302);

		/// <summary>(0018,9303) VR=FD VM=1 Tube Angle</summary>
		DicomTags.TubeAngle = new DicomTag(0x0018, 0x9303);

		/// <summary>(0018,9304) VR=SQ VM=1 CT Acquisition Details Sequence</summary>
		DicomTags.CTAcquisitionDetailsSequence = new DicomTag(0x0018, 0x9304);

		/// <summary>(0018,9305) VR=FD VM=1 Revolution Time</summary>
		DicomTags.RevolutionTime = new DicomTag(0x0018, 0x9305);

		/// <summary>(0018,9306) VR=FD VM=1 Single Collimation Width</summary>
		DicomTags.SingleCollimationWidth = new DicomTag(0x0018, 0x9306);

		/// <summary>(0018,9307) VR=FD VM=1 Total Collimation Width</summary>
		DicomTags.TotalCollimationWidth = new DicomTag(0x0018, 0x9307);

		/// <summary>(0018,9308) VR=SQ VM=1 CT Table Dynamics Sequence</summary>
		DicomTags.CTTableDynamicsSequence = new DicomTag(0x0018, 0x9308);

		/// <summary>(0018,9309) VR=FD VM=1 Table Speed</summary>
		DicomTags.TableSpeed = new DicomTag(0x0018, 0x9309);

		/// <summary>(0018,9310) VR=FD VM=1 Table Feed per Rotation</summary>
		DicomTags.TableFeedPerRotation = new DicomTag(0x0018, 0x9310);

		/// <summary>(0018,9311) VR=FD VM=1 Spiral Pitch Factor</summary>
		DicomTags.SpiralPitchFactor = new DicomTag(0x0018, 0x9311);

		/// <summary>(0018,9312) VR=SQ VM=1 CT Geometry Sequence</summary>
		DicomTags.CTGeometrySequence = new DicomTag(0x0018, 0x9312);

		/// <summary>(0018,9313) VR=FD VM=3 Data Collection Center (Patient)</summary>
		DicomTags.DataCollectionCenterPatient = new DicomTag(0x0018, 0x9313);

		/// <summary>(0018,9314) VR=SQ VM=1 CT Reconstruction Sequence</summary>
		DicomTags.CTReconstructionSequence = new DicomTag(0x0018, 0x9314);

		/// <summary>(0018,9315) VR=CS VM=1 Reconstruction Algorithm</summary>
		DicomTags.ReconstructionAlgorithm = new DicomTag(0x0018, 0x9315);

		/// <summary>(0018,9316) VR=CS VM=1 Convolution Kernel Group</summary>
		DicomTags.ConvolutionKernelGroup = new DicomTag(0x0018, 0x9316);

		/// <summary>(0018,9317) VR=FD VM=2 Reconstruction Field of View</summary>
		DicomTags.ReconstructionFieldOfView = new DicomTag(0x0018, 0x9317);

		/// <summary>(0018,9318) VR=FD VM=3 Reconstruction Target Center (Patient)</summary>
		DicomTags.ReconstructionTargetCenterPatient = new DicomTag(0x0018, 0x9318);

		/// <summary>(0018,9319) VR=FD VM=1 Reconstruction Angle</summary>
		DicomTags.ReconstructionAngle = new DicomTag(0x0018, 0x9319);

		/// <summary>(0018,9320) VR=SH VM=1 Image Filter</summary>
		DicomTags.ImageFilter = new DicomTag(0x0018, 0x9320);

		/// <summary>(0018,9321) VR=SQ VM=1 CT Exposure Sequence</summary>
		DicomTags.CTExposureSequence = new DicomTag(0x0018, 0x9321);

		/// <summary>(0018,9322) VR=FD VM=2 Reconstruction Pixel Spacing</summary>
		DicomTags.ReconstructionPixelSpacing = new DicomTag(0x0018, 0x9322);

		/// <summary>(0018,9323) VR=CS VM=1 Exposure Modulation Type</summary>
		DicomTags.ExposureModulationType = new DicomTag(0x0018, 0x9323);

		/// <summary>(0018,9324) VR=FD VM=1 Estimated Dose Saving</summary>
		DicomTags.EstimatedDoseSaving = new DicomTag(0x0018, 0x9324);

		/// <summary>(0018,9325) VR=SQ VM=1 CT X-Ray Details Sequence</summary>
		DicomTags.CTXRayDetailsSequence = new DicomTag(0x0018, 0x9325);

		/// <summary>(0018,9326) VR=SQ VM=1 CT Position Sequence</summary>
		DicomTags.CTPositionSequence = new DicomTag(0x0018, 0x9326);

		/// <summary>(0018,9327) VR=FD VM=1 Table Position</summary>
		DicomTags.TablePosition = new DicomTag(0x0018, 0x9327);

		/// <summary>(0018,9328) VR=FD VM=1 Exposure Time in ms</summary>
		DicomTags.ExposureTimeInMs = new DicomTag(0x0018, 0x9328);

		/// <summary>(0018,9329) VR=SQ VM=1 CT Image Frame Type Sequence</summary>
		DicomTags.CTImageFrameTypeSequence = new DicomTag(0x0018, 0x9329);

		/// <summary>(0018,9330) VR=FD VM=1 X-Ray Tube Current in mA</summary>
		DicomTags.XRayTubeCurrentInMA = new DicomTag(0x0018, 0x9330);

		/// <summary>(0018,9332) VR=FD VM=1 Exposure in mAs</summary>
		DicomTags.ExposureInMAs = new DicomTag(0x0018, 0x9332);

		/// <summary>(0018,9333) VR=CS VM=1 Constant Volume Flag</summary>
		DicomTags.ConstantVolumeFlag = new DicomTag(0x0018, 0x9333);

		/// <summary>(0018,9334) VR=CS VM=1 Fluoroscopy Flag</summary>
		DicomTags.FluoroscopyFlag = new DicomTag(0x0018, 0x9334);

		/// <summary>(0018,9335) VR=FD VM=1 Distance Source to Data Collection Center</summary>
		DicomTags.DistanceSourceToDataCollectionCenter = new DicomTag(0x0018, 0x9335);

		/// <summary>(0018,9337) VR=US VM=1 Contrast/Bolus Agent Number</summary>
		DicomTags.ContrastBolusAgentNumber = new DicomTag(0x0018, 0x9337);

		/// <summary>(0018,9338) VR=SQ VM=1 Contrast/Bolus Ingredient Code Sequence</summary>
		DicomTags.ContrastBolusIngredientCodeSequence = new DicomTag(0x0018, 0x9338);

		/// <summary>(0018,9340) VR=SQ VM=1 Contrast Administration Profile Sequence</summary>
		DicomTags.ContrastAdministrationProfileSequence = new DicomTag(0x0018, 0x9340);

		/// <summary>(0018,9341) VR=SQ VM=1 Contrast/Bolus Usage Sequence</summary>
		DicomTags.ContrastBolusUsageSequence = new DicomTag(0x0018, 0x9341);

		/// <summary>(0018,9342) VR=CS VM=1 Contrast/Bolus Agent Administered</summary>
		DicomTags.ContrastBolusAgentAdministered = new DicomTag(0x0018, 0x9342);

		/// <summary>(0018,9343) VR=CS VM=1 Contrast/Bolus Agent Detected</summary>
		DicomTags.ContrastBolusAgentDetected = new DicomTag(0x0018, 0x9343);

		/// <summary>(0018,9344) VR=CS VM=1 Contrast/Bolus Agent Phase</summary>
		DicomTags.ContrastBolusAgentPhase = new DicomTag(0x0018, 0x9344);

		/// <summary>(0018,9345) VR=FD VM=1 CTDIvol</summary>
		DicomTags.CTDIvol = new DicomTag(0x0018, 0x9345);

		/// <summary>(0018,9346) VR=SQ VM=1 CTDI Phantom Type Code Sequence</summary>
		DicomTags.CTDIPhantomTypeCodeSequence = new DicomTag(0x0018, 0x9346);

		/// <summary>(0018,9351) VR=FL VM=1 Calcium Scoring Mass Factor Patient</summary>
		DicomTags.CalciumScoringMassFactorPatient = new DicomTag(0x0018, 0x9351);

		/// <summary>(0018,9352) VR=FL VM=3 Calcium Scoring Mass Factor Device</summary>
		DicomTags.CalciumScoringMassFactorDevice = new DicomTag(0x0018, 0x9352);

		/// <summary>(0018,9360) VR=SQ VM=1 CT Additional X-Ray Source Sequence</summary>
		DicomTags.CTAdditionalXRaySourceSequence = new DicomTag(0x0018, 0x9360);

		/// <summary>(0018,9401) VR=SQ VM=1 Projection Pixel Calibration Sequence</summary>
		DicomTags.ProjectionPixelCalibrationSequence = new DicomTag(0x0018, 0x9401);

		/// <summary>(0018,9402) VR=FL VM=1 Distance Source to Isocenter</summary>
		DicomTags.DistanceSourceToIsocenter = new DicomTag(0x0018, 0x9402);

		/// <summary>(0018,9403) VR=FL VM=1 Distance Object to Table Top</summary>
		DicomTags.DistanceObjectToTableTop = new DicomTag(0x0018, 0x9403);

		/// <summary>(0018,9404) VR=FL VM=2 Object Pixel Spacing in Center of Beam</summary>
		DicomTags.ObjectPixelSpacingInCenterOfBeam = new DicomTag(0x0018, 0x9404);

		/// <summary>(0018,9405) VR=SQ VM=1 Positioner Position Sequence</summary>
		DicomTags.PositionerPositionSequence = new DicomTag(0x0018, 0x9405);

		/// <summary>(0018,9406) VR=SQ VM=1 Table Position Sequence</summary>
		DicomTags.TablePositionSequence = new DicomTag(0x0018, 0x9406);

		/// <summary>(0018,9407) VR=SQ VM=1 Collimator Shape Sequence</summary>
		DicomTags.CollimatorShapeSequence = new DicomTag(0x0018, 0x9407);

		/// <summary>(0018,9412) VR=SQ VM=1 XA/XRF Frame Characteristics Sequence</summary>
		DicomTags.XAXRFFrameCharacteristicsSequence = new DicomTag(0x0018, 0x9412);

		/// <summary>(0018,9417) VR=SQ VM=1 Frame Acquisition Sequence</summary>
		DicomTags.FrameAcquisitionSequence = new DicomTag(0x0018, 0x9417);

		/// <summary>(0018,9420) VR=CS VM=1 X-Ray Receptor Type</summary>
		DicomTags.XRayReceptorType = new DicomTag(0x0018, 0x9420);

		/// <summary>(0018,9423) VR=LO VM=1 Acquisition Protocol Name</summary>
		DicomTags.AcquisitionProtocolName = new DicomTag(0x0018, 0x9423);

		/// <summary>(0018,9424) VR=LT VM=1 Acquisition Protocol Description</summary>
		DicomTags.AcquisitionProtocolDescription = new DicomTag(0x0018, 0x9424);

		/// <summary>(0018,9425) VR=CS VM=1 Contrast/Bolus Ingredient Opaque</summary>
		DicomTags.ContrastBolusIngredientOpaque = new DicomTag(0x0018, 0x9425);

		/// <summary>(0018,9426) VR=FL VM=1 Distance Receptor Plane to Detector Housing</summary>
		DicomTags.DistanceReceptorPlaneToDetectorHousing = new DicomTag(0x0018, 0x9426);

		/// <summary>(0018,9427) VR=CS VM=1 Intensifier Active Shape</summary>
		DicomTags.IntensifierActiveShape = new DicomTag(0x0018, 0x9427);

		/// <summary>(0018,9428) VR=FL VM=1-2 Intensifier Active Dimension(s)</summary>
		DicomTags.IntensifierActiveDimensions = new DicomTag(0x0018, 0x9428);

		/// <summary>(0018,9429) VR=FL VM=2 Physical Detector Size</summary>
		DicomTags.PhysicalDetectorSize = new DicomTag(0x0018, 0x9429);

		/// <summary>(0018,9430) VR=US VM=2 Position of Isocenter Projection</summary>
		DicomTags.PositionOfIsocenterProjection = new DicomTag(0x0018, 0x9430);

		/// <summary>(0018,9432) VR=SQ VM=1 Field of View Sequence</summary>
		DicomTags.FieldOfViewSequence = new DicomTag(0x0018, 0x9432);

		/// <summary>(0018,9433) VR=LO VM=1 Field of View Description</summary>
		DicomTags.FieldOfViewDescription = new DicomTag(0x0018, 0x9433);

		/// <summary>(0018,9434) VR=SQ VM=1 Exposure Control Sensing Regions Sequence</summary>
		DicomTags.ExposureControlSensingRegionsSequence = new DicomTag(0x0018, 0x9434);

		/// <summary>(0018,9435) VR=CS VM=1 Exposure Control Sensing Region Shape</summary>
		DicomTags.ExposureControlSensingRegionShape = new DicomTag(0x0018, 0x9435);

		/// <summary>(0018,9436) VR=SS VM=1 Exposure Control Sensing Region Left Vertical Edge</summary>
		DicomTags.ExposureControlSensingRegionLeftVerticalEdge = new DicomTag(0x0018, 0x9436);

		/// <summary>(0018,9437) VR=SS VM=1 Exposure Control Sensing Region Right Vertical Edge</summary>
		DicomTags.ExposureControlSensingRegionRightVerticalEdge = new DicomTag(0x0018, 0x9437);

		/// <summary>(0018,9438) VR=SS VM=1 Exposure Control Sensing Region Upper Horizontal Edge</summary>
		DicomTags.ExposureControlSensingRegionUpperHorizontalEdge = new DicomTag(0x0018, 0x9438);

		/// <summary>(0018,9439) VR=SS VM=1 Exposure Control Sensing Region Lower Horizontal Edge</summary>
		DicomTags.ExposureControlSensingRegionLowerHorizontalEdge = new DicomTag(0x0018, 0x9439);

		/// <summary>(0018,9440) VR=SS VM=2 Center of Circular Exposure Control Sensing Region</summary>
		DicomTags.CenterOfCircularExposureControlSensingRegion = new DicomTag(0x0018, 0x9440);

		/// <summary>(0018,9441) VR=US VM=1 Radius of Circular Exposure Control Sensing Region</summary>
		DicomTags.RadiusOfCircularExposureControlSensingRegion = new DicomTag(0x0018, 0x9441);

		/// <summary>(0018,9442) VR=SS VM=2-n Vertices of the Polygonal Exposure Control Sensing Region</summary>
		DicomTags.VerticesOfThePolygonalExposureControlSensingRegion = new DicomTag(0x0018, 0x9442);

		/// <summary>(0018,9445) VR=NONE VM=  (Retired)</summary>
		DicomTags.RETIRED = new DicomTag(0x0018, 0x9445);

		/// <summary>(0018,9447) VR=FL VM=1 Column Angulation (Patient)</summary>
		DicomTags.ColumnAngulationPatient = new DicomTag(0x0018, 0x9447);

		/// <summary>(0018,9449) VR=FL VM=1 Beam Angle</summary>
		DicomTags.BeamAngle = new DicomTag(0x0018, 0x9449);

		/// <summary>(0018,9451) VR=SQ VM=1 Frame Detector Parameters Sequence</summary>
		DicomTags.FrameDetectorParametersSequence = new DicomTag(0x0018, 0x9451);

		/// <summary>(0018,9452) VR=FL VM=1 Calculated Anatomy Thickness</summary>
		DicomTags.CalculatedAnatomyThickness = new DicomTag(0x0018, 0x9452);

		/// <summary>(0018,9455) VR=SQ VM=1 Calibration Sequence</summary>
		DicomTags.CalibrationSequence = new DicomTag(0x0018, 0x9455);

		/// <summary>(0018,9456) VR=SQ VM=1 Object Thickness Sequence</summary>
		DicomTags.ObjectThicknessSequence = new DicomTag(0x0018, 0x9456);

		/// <summary>(0018,9457) VR=CS VM=1 Plane Identification</summary>
		DicomTags.PlaneIdentification = new DicomTag(0x0018, 0x9457);

		/// <summary>(0018,9461) VR=FL VM=1-2 Field of View Dimension(s) in Float</summary>
		DicomTags.FieldOfViewDimensionsInFloat = new DicomTag(0x0018, 0x9461);

		/// <summary>(0018,9462) VR=SQ VM=1 Isocenter Reference System Sequence</summary>
		DicomTags.IsocenterReferenceSystemSequence = new DicomTag(0x0018, 0x9462);

		/// <summary>(0018,9463) VR=FL VM=1 Positioner Isocenter Primary Angle</summary>
		DicomTags.PositionerIsocenterPrimaryAngle = new DicomTag(0x0018, 0x9463);

		/// <summary>(0018,9464) VR=FL VM=1 Positioner Isocenter Secondary Angle</summary>
		DicomTags.PositionerIsocenterSecondaryAngle = new DicomTag(0x0018, 0x9464);

		/// <summary>(0018,9465) VR=FL VM=1 Positioner Isocenter Detector Rotation Angle</summary>
		DicomTags.PositionerIsocenterDetectorRotationAngle = new DicomTag(0x0018, 0x9465);

		/// <summary>(0018,9466) VR=FL VM=1 Table X Position to Isocenter</summary>
		DicomTags.TableXPositionToIsocenter = new DicomTag(0x0018, 0x9466);

		/// <summary>(0018,9467) VR=FL VM=1 Table Y Position to Isocenter</summary>
		DicomTags.TableYPositionToIsocenter = new DicomTag(0x0018, 0x9467);

		/// <summary>(0018,9468) VR=FL VM=1 Table Z Position to Isocenter</summary>
		DicomTags.TableZPositionToIsocenter = new DicomTag(0x0018, 0x9468);

		/// <summary>(0018,9469) VR=FL VM=1 Table Horizontal Rotation Angle</summary>
		DicomTags.TableHorizontalRotationAngle = new DicomTag(0x0018, 0x9469);

		/// <summary>(0018,9470) VR=FL VM=1 Table Head Tilt Angle</summary>
		DicomTags.TableHeadTiltAngle = new DicomTag(0x0018, 0x9470);

		/// <summary>(0018,9471) VR=FL VM=1 Table Cradle Tilt Angle</summary>
		DicomTags.TableCradleTiltAngle = new DicomTag(0x0018, 0x9471);

		/// <summary>(0018,9472) VR=SQ VM=1 Frame Display Shutter Sequence</summary>
		DicomTags.FrameDisplayShutterSequence = new DicomTag(0x0018, 0x9472);

		/// <summary>(0018,9473) VR=FL VM=1 Acquired Image Area Dose Product</summary>
		DicomTags.AcquiredImageAreaDoseProduct = new DicomTag(0x0018, 0x9473);

		/// <summary>(0018,9474) VR=CS VM=1 C-arm Positioner Tabletop Relationship</summary>
		DicomTags.CarmPositionerTabletopRelationship = new DicomTag(0x0018, 0x9474);

		/// <summary>(0018,9476) VR=SQ VM=1 X-Ray Geometry Sequence</summary>
		DicomTags.XRayGeometrySequence = new DicomTag(0x0018, 0x9476);

		/// <summary>(0018,9477) VR=SQ VM=1 Irradiation Event Identification Sequence</summary>
		DicomTags.IrradiationEventIdentificationSequence = new DicomTag(0x0018, 0x9477);

		/// <summary>(0018,9504) VR=SQ VM=1 X-Ray 3D Frame Type Sequence</summary>
		DicomTags.XRay3DFrameTypeSequence = new DicomTag(0x0018, 0x9504);

		/// <summary>(0018,9506) VR=SQ VM=1 Contributing Sources Sequence</summary>
		DicomTags.ContributingSourcesSequence = new DicomTag(0x0018, 0x9506);

		/// <summary>(0018,9507) VR=SQ VM=1 X-Ray 3D Acquisition Sequence</summary>
		DicomTags.XRay3DAcquisitionSequence = new DicomTag(0x0018, 0x9507);

		/// <summary>(0018,9508) VR=FL VM=1 Primary Positioner Scan Arc</summary>
		DicomTags.PrimaryPositionerScanArc = new DicomTag(0x0018, 0x9508);

		/// <summary>(0018,9509) VR=FL VM=1 Secondary Positioner Scan Arc</summary>
		DicomTags.SecondaryPositionerScanArc = new DicomTag(0x0018, 0x9509);

		/// <summary>(0018,9510) VR=FL VM=1 Primary Positioner Scan Start Angle</summary>
		DicomTags.PrimaryPositionerScanStartAngle = new DicomTag(0x0018, 0x9510);

		/// <summary>(0018,9511) VR=FL VM=1 Secondary Positioner Scan Start Angle</summary>
		DicomTags.SecondaryPositionerScanStartAngle = new DicomTag(0x0018, 0x9511);

		/// <summary>(0018,9514) VR=FL VM=1 Primary Positioner Increment</summary>
		DicomTags.PrimaryPositionerIncrement = new DicomTag(0x0018, 0x9514);

		/// <summary>(0018,9515) VR=FL VM=1 Secondary Positioner Increment</summary>
		DicomTags.SecondaryPositionerIncrement = new DicomTag(0x0018, 0x9515);

		/// <summary>(0018,9516) VR=DT VM=1 Start Acquisition DateTime</summary>
		DicomTags.StartAcquisitionDateTime = new DicomTag(0x0018, 0x9516);

		/// <summary>(0018,9517) VR=DT VM=1 End Acquisition DateTime</summary>
		DicomTags.EndAcquisitionDateTime = new DicomTag(0x0018, 0x9517);

		/// <summary>(0018,9524) VR=LO VM=1 Application Name</summary>
		DicomTags.ApplicationName = new DicomTag(0x0018, 0x9524);

		/// <summary>(0018,9525) VR=LO VM=1 Application Version</summary>
		DicomTags.ApplicationVersion = new DicomTag(0x0018, 0x9525);

		/// <summary>(0018,9526) VR=LO VM=1 Application Manufacturer</summary>
		DicomTags.ApplicationManufacturer = new DicomTag(0x0018, 0x9526);

		/// <summary>(0018,9527) VR=CS VM=1 Algorithm Type</summary>
		DicomTags.AlgorithmType = new DicomTag(0x0018, 0x9527);

		/// <summary>(0018,9528) VR=LO VM=1 Algorithm Description</summary>
		DicomTags.AlgorithmDescription = new DicomTag(0x0018, 0x9528);

		/// <summary>(0018,9530) VR=SQ VM=1 X-Ray 3D Reconstruction Sequence</summary>
		DicomTags.XRay3DReconstructionSequence = new DicomTag(0x0018, 0x9530);

		/// <summary>(0018,9531) VR=LO VM=1 Reconstruction Description</summary>
		DicomTags.ReconstructionDescription = new DicomTag(0x0018, 0x9531);

		/// <summary>(0018,9538) VR=SQ VM=1 Per Projection Acquisition Sequence</summary>
		DicomTags.PerProjectionAcquisitionSequence = new DicomTag(0x0018, 0x9538);

		/// <summary>(0018,9601) VR=SQ VM=1 Diffusion b-matrix Sequence</summary>
		DicomTags.DiffusionBmatrixSequence = new DicomTag(0x0018, 0x9601);

		/// <summary>(0018,9602) VR=FD VM=1 Diffusion b-value XX</summary>
		DicomTags.DiffusionBvalueXX = new DicomTag(0x0018, 0x9602);

		/// <summary>(0018,9603) VR=FD VM=1 Diffusion b-value XY</summary>
		DicomTags.DiffusionBvalueXY = new DicomTag(0x0018, 0x9603);

		/// <summary>(0018,9604) VR=FD VM=1 Diffusion b-value XZ</summary>
		DicomTags.DiffusionBvalueXZ = new DicomTag(0x0018, 0x9604);

		/// <summary>(0018,9605) VR=FD VM=1 Diffusion b-value YY</summary>
		DicomTags.DiffusionBvalueYY = new DicomTag(0x0018, 0x9605);

		/// <summary>(0018,9606) VR=FD VM=1 Diffusion b-value YZ</summary>
		DicomTags.DiffusionBvalueYZ = new DicomTag(0x0018, 0x9606);

		/// <summary>(0018,9607) VR=FD VM=1 Diffusion b-value ZZ</summary>
		DicomTags.DiffusionBvalueZZ = new DicomTag(0x0018, 0x9607);

		/// <summary>(0018,a001) VR=SQ VM=1 Contributing Equipment Sequence</summary>
		DicomTags.ContributingEquipmentSequence = new DicomTag(0x0018, 0xa001);

		/// <summary>(0018,a002) VR=DT VM=1 Contribution Date Time</summary>
		DicomTags.ContributionDateTime = new DicomTag(0x0018, 0xa002);

		/// <summary>(0018,a003) VR=ST VM=1 Contribution Description</summary>
		DicomTags.ContributionDescription = new DicomTag(0x0018, 0xa003);

		/// <summary>(0020,000d) VR=UI VM=1 Study Instance UID</summary>
		DicomTags.StudyInstanceUID = new DicomTag(0x0020, 0x000d);

		/// <summary>(0020,000e) VR=UI VM=1 Series Instance UID</summary>
		DicomTags.SeriesInstanceUID = new DicomTag(0x0020, 0x000e);

		/// <summary>(0020,0010) VR=SH VM=1 Study ID</summary>
		DicomTags.StudyID = new DicomTag(0x0020, 0x0010);

		/// <summary>(0020,0011) VR=IS VM=1 Series Number</summary>
		DicomTags.SeriesNumber = new DicomTag(0x0020, 0x0011);

		/// <summary>(0020,0012) VR=IS VM=1 Acquisition Number</summary>
		DicomTags.AcquisitionNumber = new DicomTag(0x0020, 0x0012);

		/// <summary>(0020,0013) VR=IS VM=1 Instance Number</summary>
		DicomTags.InstanceNumber = new DicomTag(0x0020, 0x0013);

		/// <summary>(0020,0014) VR=IS VM=1 Isotope Number (Retired)</summary>
		DicomTags.IsotopeNumberRETIRED = new DicomTag(0x0020, 0x0014);

		/// <summary>(0020,0015) VR=IS VM=1 Phase Number (Retired)</summary>
		DicomTags.PhaseNumberRETIRED = new DicomTag(0x0020, 0x0015);

		/// <summary>(0020,0016) VR=IS VM=1 Interval Number (Retired)</summary>
		DicomTags.IntervalNumberRETIRED = new DicomTag(0x0020, 0x0016);

		/// <summary>(0020,0017) VR=IS VM=1 Time Slot Number (Retired)</summary>
		DicomTags.TimeSlotNumberRETIRED = new DicomTag(0x0020, 0x0017);

		/// <summary>(0020,0018) VR=IS VM=1 Angle Number (Retired)</summary>
		DicomTags.AngleNumberRETIRED = new DicomTag(0x0020, 0x0018);

		/// <summary>(0020,0019) VR=IS VM=1 Item Number</summary>
		DicomTags.ItemNumber = new DicomTag(0x0020, 0x0019);

		/// <summary>(0020,0020) VR=CS VM=2 Patient Orientation</summary>
		DicomTags.PatientOrientation = new DicomTag(0x0020, 0x0020);

		/// <summary>(0020,0022) VR=IS VM=1 Overlay Number (Retired)</summary>
		DicomTags.OverlayNumberRETIRED = new DicomTag(0x0020, 0x0022);

		/// <summary>(0020,0024) VR=IS VM=1 Curve Number (Retired)</summary>
		DicomTags.CurveNumberRETIRED = new DicomTag(0x0020, 0x0024);

		/// <summary>(0020,0026) VR=IS VM=1 Lookup Table Number (Retired)</summary>
		DicomTags.LookupTableNumberRETIRED = new DicomTag(0x0020, 0x0026);

		/// <summary>(0020,0030) VR=DS VM=3 Image Position (Retired)</summary>
		DicomTags.ImagePositionRETIRED = new DicomTag(0x0020, 0x0030);

		/// <summary>(0020,0032) VR=DS VM=3 Image Position (Patient)</summary>
		DicomTags.ImagePositionPatient = new DicomTag(0x0020, 0x0032);

		/// <summary>(0020,0035) VR=DS VM=6 Image Orientation (Retired)</summary>
		DicomTags.ImageOrientationRETIRED = new DicomTag(0x0020, 0x0035);

		/// <summary>(0020,0037) VR=DS VM=6 Image Orientation (Patient)</summary>
		DicomTags.ImageOrientationPatient = new DicomTag(0x0020, 0x0037);

		/// <summary>(0020,0050) VR=DS VM=1 Location (Retired)</summary>
		DicomTags.LocationRETIRED = new DicomTag(0x0020, 0x0050);

		/// <summary>(0020,0052) VR=UI VM=1 Frame of Reference UID</summary>
		DicomTags.FrameOfReferenceUID = new DicomTag(0x0020, 0x0052);

		/// <summary>(0020,0060) VR=CS VM=1 Laterality</summary>
		DicomTags.Laterality = new DicomTag(0x0020, 0x0060);

		/// <summary>(0020,0062) VR=CS VM=1 Image Laterality</summary>
		DicomTags.ImageLaterality = new DicomTag(0x0020, 0x0062);

		/// <summary>(0020,0070) VR=LO VM=1 Image Geometry Type (Retired)</summary>
		DicomTags.ImageGeometryTypeRETIRED = new DicomTag(0x0020, 0x0070);

		/// <summary>(0020,0080) VR=CS VM=1-n Masking Image (Retired)</summary>
		DicomTags.MaskingImageRETIRED = new DicomTag(0x0020, 0x0080);

		/// <summary>(0020,0100) VR=IS VM=1 Temporal Position Identifier</summary>
		DicomTags.TemporalPositionIdentifier = new DicomTag(0x0020, 0x0100);

		/// <summary>(0020,0105) VR=IS VM=1 Number of Temporal Positions</summary>
		DicomTags.NumberOfTemporalPositions = new DicomTag(0x0020, 0x0105);

		/// <summary>(0020,0110) VR=DS VM=1 Temporal Resolution</summary>
		DicomTags.TemporalResolution = new DicomTag(0x0020, 0x0110);

		/// <summary>(0020,0200) VR=UI VM=1 Synchronization Frame of Reference UID</summary>
		DicomTags.SynchronizationFrameOfReferenceUID = new DicomTag(0x0020, 0x0200);

		/// <summary>(0020,1000) VR=IS VM=1 Series in Study (Retired)</summary>
		DicomTags.SeriesInStudyRETIRED = new DicomTag(0x0020, 0x1000);

		/// <summary>(0020,1001) VR=IS VM=1 Acquisitions in Series (Retired)</summary>
		DicomTags.AcquisitionsInSeriesRETIRED = new DicomTag(0x0020, 0x1001);

		/// <summary>(0020,1002) VR=IS VM=1 Images in Acquisition</summary>
		DicomTags.ImagesInAcquisition = new DicomTag(0x0020, 0x1002);

		/// <summary>(0020,1003) VR=IS VM=1 Images in Series (Retired)</summary>
		DicomTags.ImagesInSeriesRETIRED = new DicomTag(0x0020, 0x1003);

		/// <summary>(0020,1004) VR=IS VM=1 Acquisitions in Study (Retired)</summary>
		DicomTags.AcquisitionsInStudyRETIRED = new DicomTag(0x0020, 0x1004);

		/// <summary>(0020,1005) VR=IS VM=1 Images in Study (Retired)</summary>
		DicomTags.ImagesInStudyRETIRED = new DicomTag(0x0020, 0x1005);

		/// <summary>(0020,1020) VR=CS VM=1-n Reference (Retired)</summary>
		DicomTags.ReferenceRETIRED = new DicomTag(0x0020, 0x1020);

		/// <summary>(0020,1040) VR=LO VM=1 Position Reference Indicator</summary>
		DicomTags.PositionReferenceIndicator = new DicomTag(0x0020, 0x1040);

		/// <summary>(0020,1041) VR=DS VM=1 Slice Location</summary>
		DicomTags.SliceLocation = new DicomTag(0x0020, 0x1041);

		/// <summary>(0020,1070) VR=IS VM=1-n Other Study Numbers (Retired)</summary>
		DicomTags.OtherStudyNumbersRETIRED = new DicomTag(0x0020, 0x1070);

		/// <summary>(0020,1200) VR=IS VM=1 Number of Patient Related Studies</summary>
		DicomTags.NumberOfPatientRelatedStudies = new DicomTag(0x0020, 0x1200);

		/// <summary>(0020,1202) VR=IS VM=1 Number of Patient Related Series</summary>
		DicomTags.NumberOfPatientRelatedSeries = new DicomTag(0x0020, 0x1202);

		/// <summary>(0020,1204) VR=IS VM=1 Number of Patient Related Instances</summary>
		DicomTags.NumberOfPatientRelatedInstances = new DicomTag(0x0020, 0x1204);

		/// <summary>(0020,1206) VR=IS VM=1 Number of Study Related Series</summary>
		DicomTags.NumberOfStudyRelatedSeries = new DicomTag(0x0020, 0x1206);

		/// <summary>(0020,1208) VR=IS VM=1 Number of Study Related Instances</summary>
		DicomTags.NumberOfStudyRelatedInstances = new DicomTag(0x0020, 0x1208);

		/// <summary>(0020,1209) VR=IS VM=1 Number of Series Related Instances</summary>
		DicomTags.NumberOfSeriesRelatedInstances = new DicomTag(0x0020, 0x1209);

		/// <summary>(0020,3100) VR=CS VM=1-n Source Image IDs (Retired)</summary>
		DicomTags.SourceImageIDsRETIRED = new DicomTag(0x0020, 0x3100);

		/// <summary>(0020,3401) VR=CS VM=1 Modifying Device ID (Retired)</summary>
		DicomTags.ModifyingDeviceIDRETIRED = new DicomTag(0x0020, 0x3401);

		/// <summary>(0020,3402) VR=CS VM=1 Modified Image ID (Retired)</summary>
		DicomTags.ModifiedImageIDRETIRED = new DicomTag(0x0020, 0x3402);

		/// <summary>(0020,3403) VR=DA VM=1 Modified Image Date (Retired)</summary>
		DicomTags.ModifiedImageDateRETIRED = new DicomTag(0x0020, 0x3403);

		/// <summary>(0020,3404) VR=LO VM=1 Modifying Device Manufacturer (Retired)</summary>
		DicomTags.ModifyingDeviceManufacturerRETIRED = new DicomTag(0x0020, 0x3404);

		/// <summary>(0020,3405) VR=TM VM=1 Modified Image Time (Retired)</summary>
		DicomTags.ModifiedImageTimeRETIRED = new DicomTag(0x0020, 0x3405);

		/// <summary>(0020,3406) VR=LO VM=1 Modified Image Description (Retired)</summary>
		DicomTags.ModifiedImageDescriptionRETIRED = new DicomTag(0x0020, 0x3406);

		/// <summary>(0020,4000) VR=LT VM=1 Image Comments</summary>
		DicomTags.ImageComments = new DicomTag(0x0020, 0x4000);

		/// <summary>(0020,5000) VR=AT VM=1-n Original Image Identification (Retired)</summary>
		DicomTags.OriginalImageIdentificationRETIRED = new DicomTag(0x0020, 0x5000);

		/// <summary>(0020,5002) VR=CS VM=1-n Original Image Identification Nomenclature (Retired)</summary>
		DicomTags.OriginalImageIdentificationNomenclatureRETIRED = new DicomTag(0x0020, 0x5002);

		/// <summary>(0020,9056) VR=SH VM=1 Stack ID</summary>
		DicomTags.StackID = new DicomTag(0x0020, 0x9056);

		/// <summary>(0020,9057) VR=UL VM=1 In-Stack Position Number</summary>
		DicomTags.InStackPositionNumber = new DicomTag(0x0020, 0x9057);

		/// <summary>(0020,9071) VR=SQ VM=1 Frame Anatomy Sequence</summary>
		DicomTags.FrameAnatomySequence = new DicomTag(0x0020, 0x9071);

		/// <summary>(0020,9072) VR=CS VM=1 Frame Laterality</summary>
		DicomTags.FrameLaterality = new DicomTag(0x0020, 0x9072);

		/// <summary>(0020,9111) VR=SQ VM=1 Frame Content Sequence</summary>
		DicomTags.FrameContentSequence = new DicomTag(0x0020, 0x9111);

		/// <summary>(0020,9113) VR=SQ VM=1 Plane Position Sequence</summary>
		DicomTags.PlanePositionSequence = new DicomTag(0x0020, 0x9113);

		/// <summary>(0020,9116) VR=SQ VM=1 Plane Orientation Sequence</summary>
		DicomTags.PlaneOrientationSequence = new DicomTag(0x0020, 0x9116);

		/// <summary>(0020,9128) VR=UL VM=1 Temporal Position Index</summary>
		DicomTags.TemporalPositionIndex = new DicomTag(0x0020, 0x9128);

		/// <summary>(0020,9153) VR=FD VM=1 Nominal Cardiac Trigger Delay Time</summary>
		DicomTags.NominalCardiacTriggerDelayTime = new DicomTag(0x0020, 0x9153);

		/// <summary>(0020,9156) VR=US VM=1 Frame Acquisition Number</summary>
		DicomTags.FrameAcquisitionNumber = new DicomTag(0x0020, 0x9156);

		/// <summary>(0020,9157) VR=UL VM=1-n Dimension Index Values</summary>
		DicomTags.DimensionIndexValues = new DicomTag(0x0020, 0x9157);

		/// <summary>(0020,9158) VR=LT VM=1 Frame Comments</summary>
		DicomTags.FrameComments = new DicomTag(0x0020, 0x9158);

		/// <summary>(0020,9161) VR=UI VM=1 Concatenation UID</summary>
		DicomTags.ConcatenationUID = new DicomTag(0x0020, 0x9161);

		/// <summary>(0020,9162) VR=US VM=1 In-concatenation Number</summary>
		DicomTags.InconcatenationNumber = new DicomTag(0x0020, 0x9162);

		/// <summary>(0020,9163) VR=US VM=1 In-concatenation Total Number</summary>
		DicomTags.InconcatenationTotalNumber = new DicomTag(0x0020, 0x9163);

		/// <summary>(0020,9164) VR=UI VM=1 Dimension Organization UID</summary>
		DicomTags.DimensionOrganizationUID = new DicomTag(0x0020, 0x9164);

		/// <summary>(0020,9165) VR=AT VM=1 Dimension Index Pointer</summary>
		DicomTags.DimensionIndexPointer = new DicomTag(0x0020, 0x9165);

		/// <summary>(0020,9167) VR=AT VM=1 Functional Group Pointer</summary>
		DicomTags.FunctionalGroupPointer = new DicomTag(0x0020, 0x9167);

		/// <summary>(0020,9213) VR=LO VM=1 Dimension Index Private Creator</summary>
		DicomTags.DimensionIndexPrivateCreator = new DicomTag(0x0020, 0x9213);

		/// <summary>(0020,9221) VR=SQ VM=1 Dimension Organization Sequence</summary>
		DicomTags.DimensionOrganizationSequence = new DicomTag(0x0020, 0x9221);

		/// <summary>(0020,9222) VR=SQ VM=1 Dimension Index Sequence</summary>
		DicomTags.DimensionIndexSequence = new DicomTag(0x0020, 0x9222);

		/// <summary>(0020,9228) VR=UL VM=1 Concatenation Frame Offset Number</summary>
		DicomTags.ConcatenationFrameOffsetNumber = new DicomTag(0x0020, 0x9228);

		/// <summary>(0020,9238) VR=LO VM=1 Functional Group Private Creator</summary>
		DicomTags.FunctionalGroupPrivateCreator = new DicomTag(0x0020, 0x9238);

		/// <summary>(0020,9241) VR=FL VM=1 Nominal Percentage of Cardiac Phase</summary>
		DicomTags.NominalPercentageOfCardiacPhase = new DicomTag(0x0020, 0x9241);

		/// <summary>(0020,9245) VR=FL VM=1 Nominal Percentage of Respiratory Phase</summary>
		DicomTags.NominalPercentageOfRespiratoryPhase = new DicomTag(0x0020, 0x9245);

		/// <summary>(0020,9246) VR=FL VM=1 Starting Respiratory Amplitude</summary>
		DicomTags.StartingRespiratoryAmplitude = new DicomTag(0x0020, 0x9246);

		/// <summary>(0020,9247) VR=CS VM=1 Starting Respiratory Phase</summary>
		DicomTags.StartingRespiratoryPhase = new DicomTag(0x0020, 0x9247);

		/// <summary>(0020,9248) VR=FL VM=1 Ending Respiratory Amplitude</summary>
		DicomTags.EndingRespiratoryAmplitude = new DicomTag(0x0020, 0x9248);

		/// <summary>(0020,9249) VR=CS VM=1 Ending Respiratory Phase</summary>
		DicomTags.EndingRespiratoryPhase = new DicomTag(0x0020, 0x9249);

		/// <summary>(0020,9250) VR=CS VM=1 Respiratory Trigger Type</summary>
		DicomTags.RespiratoryTriggerType = new DicomTag(0x0020, 0x9250);

		/// <summary>(0020,9251) VR=FD VM=1 R - R Interval Time Nominal</summary>
		DicomTags.RRIntervalTimeNominal = new DicomTag(0x0020, 0x9251);

		/// <summary>(0020,9252) VR=FD VM=1 Actual Cardiac Trigger Delay Time</summary>
		DicomTags.ActualCardiacTriggerDelayTime = new DicomTag(0x0020, 0x9252);

		/// <summary>(0020,9253) VR=SQ VM=1 Respiratory Synchronization Sequence</summary>
		DicomTags.RespiratorySynchronizationSequence = new DicomTag(0x0020, 0x9253);

		/// <summary>(0020,9254) VR=FD VM=1 Respiratory Interval Time</summary>
		DicomTags.RespiratoryIntervalTime = new DicomTag(0x0020, 0x9254);

		/// <summary>(0020,9255) VR=FD VM=1 Nominal Respiratory Trigger Delay Time</summary>
		DicomTags.NominalRespiratoryTriggerDelayTime = new DicomTag(0x0020, 0x9255);

		/// <summary>(0020,9256) VR=FD VM=1 Respiratory Trigger Delay Threshold</summary>
		DicomTags.RespiratoryTriggerDelayThreshold = new DicomTag(0x0020, 0x9256);

		/// <summary>(0020,9257) VR=FD VM=1 Actual Respiratory Trigger Delay Time</summary>
		DicomTags.ActualRespiratoryTriggerDelayTime = new DicomTag(0x0020, 0x9257);

		/// <summary>(0020,9421) VR=LO VM=1 Dimension Description Label</summary>
		DicomTags.DimensionDescriptionLabel = new DicomTag(0x0020, 0x9421);

		/// <summary>(0020,9450) VR=SQ VM=1 Patient Orientation in Frame Sequence</summary>
		DicomTags.PatientOrientationInFrameSequence = new DicomTag(0x0020, 0x9450);

		/// <summary>(0020,9453) VR=LO VM=1 Frame Label</summary>
		DicomTags.FrameLabel = new DicomTag(0x0020, 0x9453);

		/// <summary>(0020,9518) VR=US VM=1-n Acquisition Index</summary>
		DicomTags.AcquisitionIndex = new DicomTag(0x0020, 0x9518);

		/// <summary>(0020,9529) VR=SQ VM=1 Contributing SOP Instances Reference Sequence</summary>
		DicomTags.ContributingSOPInstancesReferenceSequence = new DicomTag(0x0020, 0x9529);

		/// <summary>(0020,9536) VR=US VM=1 Reconstruction Index</summary>
		DicomTags.ReconstructionIndex = new DicomTag(0x0020, 0x9536);

		/// <summary>(0022,0001) VR=US VM=1 Light Path Filter Pass-Through Wavelength</summary>
		DicomTags.LightPathFilterPassThroughWavelength = new DicomTag(0x0022, 0x0001);

		/// <summary>(0022,0002) VR=US VM=2 Light Path Filter Pass Band</summary>
		DicomTags.LightPathFilterPassBand = new DicomTag(0x0022, 0x0002);

		/// <summary>(0022,0003) VR=US VM=1 Image Path Filter Pass-Through Wavelength</summary>
		DicomTags.ImagePathFilterPassThroughWavelength = new DicomTag(0x0022, 0x0003);

		/// <summary>(0022,0004) VR=US VM=2 Image Path Filter Pass Band</summary>
		DicomTags.ImagePathFilterPassBand = new DicomTag(0x0022, 0x0004);

		/// <summary>(0022,0005) VR=CS VM=1 Patient Eye Movement Commanded</summary>
		DicomTags.PatientEyeMovementCommanded = new DicomTag(0x0022, 0x0005);

		/// <summary>(0022,0006) VR=SQ VM=1 Patient Eye Movement Command Code Sequence</summary>
		DicomTags.PatientEyeMovementCommandCodeSequence = new DicomTag(0x0022, 0x0006);

		/// <summary>(0022,0007) VR=FL VM=1 Spherical Lens Power</summary>
		DicomTags.SphericalLensPower = new DicomTag(0x0022, 0x0007);

		/// <summary>(0022,0008) VR=FL VM=1 Cylinder Lens Power</summary>
		DicomTags.CylinderLensPower = new DicomTag(0x0022, 0x0008);

		/// <summary>(0022,0009) VR=FL VM=1 Cylinder Axis</summary>
		DicomTags.CylinderAxis = new DicomTag(0x0022, 0x0009);

		/// <summary>(0022,000a) VR=FL VM=1 Emmetropic Magnification</summary>
		DicomTags.EmmetropicMagnification = new DicomTag(0x0022, 0x000a);

		/// <summary>(0022,000b) VR=FL VM=1 Intra Ocular Pressure</summary>
		DicomTags.IntraOcularPressure = new DicomTag(0x0022, 0x000b);

		/// <summary>(0022,000c) VR=FL VM=1 Horizontal Field of View</summary>
		DicomTags.HorizontalFieldOfView = new DicomTag(0x0022, 0x000c);

		/// <summary>(0022,000d) VR=CS VM=1 Pupil Dilated</summary>
		DicomTags.PupilDilated = new DicomTag(0x0022, 0x000d);

		/// <summary>(0022,000e) VR=FL VM=1 Degree of Dilation</summary>
		DicomTags.DegreeOfDilation = new DicomTag(0x0022, 0x000e);

		/// <summary>(0022,0010) VR=FL VM=1 Stereo Baseline Angle</summary>
		DicomTags.StereoBaselineAngle = new DicomTag(0x0022, 0x0010);

		/// <summary>(0022,0011) VR=FL VM=1 Stereo Baseline Displacement</summary>
		DicomTags.StereoBaselineDisplacement = new DicomTag(0x0022, 0x0011);

		/// <summary>(0022,0012) VR=FL VM=1 Stereo Horizontal Pixel Offset</summary>
		DicomTags.StereoHorizontalPixelOffset = new DicomTag(0x0022, 0x0012);

		/// <summary>(0022,0013) VR=FL VM=1 Stereo Vertical Pixel Offset</summary>
		DicomTags.StereoVerticalPixelOffset = new DicomTag(0x0022, 0x0013);

		/// <summary>(0022,0014) VR=FL VM=1 Stereo Rotation</summary>
		DicomTags.StereoRotation = new DicomTag(0x0022, 0x0014);

		/// <summary>(0022,0015) VR=SQ VM=1 Acquisition Device Type Code Sequence</summary>
		DicomTags.AcquisitionDeviceTypeCodeSequence = new DicomTag(0x0022, 0x0015);

		/// <summary>(0022,0016) VR=SQ VM=1 Illumination Type Code Sequence</summary>
		DicomTags.IlluminationTypeCodeSequence = new DicomTag(0x0022, 0x0016);

		/// <summary>(0022,0017) VR=SQ VM=1 Light Path Filter Type Stack Code Sequence</summary>
		DicomTags.LightPathFilterTypeStackCodeSequence = new DicomTag(0x0022, 0x0017);

		/// <summary>(0022,0018) VR=SQ VM=1 Image Path Filter Type Stack Code Sequence</summary>
		DicomTags.ImagePathFilterTypeStackCodeSequence = new DicomTag(0x0022, 0x0018);

		/// <summary>(0022,0019) VR=SQ VM=1 Lenses Code Sequence</summary>
		DicomTags.LensesCodeSequence = new DicomTag(0x0022, 0x0019);

		/// <summary>(0022,001a) VR=SQ VM=1 Channel Description Code Sequence</summary>
		DicomTags.ChannelDescriptionCodeSequence = new DicomTag(0x0022, 0x001a);

		/// <summary>(0022,001b) VR=SQ VM=1 Refractive State Sequence</summary>
		DicomTags.RefractiveStateSequence = new DicomTag(0x0022, 0x001b);

		/// <summary>(0022,001c) VR=SQ VM=1 Mydriatic Agent Code Sequence</summary>
		DicomTags.MydriaticAgentCodeSequence = new DicomTag(0x0022, 0x001c);

		/// <summary>(0022,001d) VR=SQ VM=1 Relative Image Position Code Sequence</summary>
		DicomTags.RelativeImagePositionCodeSequence = new DicomTag(0x0022, 0x001d);

		/// <summary>(0022,0020) VR=SQ VM=1 Stereo Pairs Sequence</summary>
		DicomTags.StereoPairsSequence = new DicomTag(0x0022, 0x0020);

		/// <summary>(0022,0021) VR=SQ VM=1 Left Image Sequence</summary>
		DicomTags.LeftImageSequence = new DicomTag(0x0022, 0x0021);

		/// <summary>(0022,0022) VR=SQ VM=1 Right Image Sequence</summary>
		DicomTags.RightImageSequence = new DicomTag(0x0022, 0x0022);

		/// <summary>(0022,0030) VR=FL VM=1 Axial Length of the Eye</summary>
		DicomTags.AxialLengthOfTheEye = new DicomTag(0x0022, 0x0030);

		/// <summary>(0022,0031) VR=SQ VM=1 Ophthalmic Frame Location Sequence</summary>
		DicomTags.OphthalmicFrameLocationSequence = new DicomTag(0x0022, 0x0031);

		/// <summary>(0022,0032) VR=FL VM=2-2n Reference Coordinates</summary>
		DicomTags.ReferenceCoordinates = new DicomTag(0x0022, 0x0032);

		/// <summary>(0022,0035) VR=FL VM=1 Depth Spatial Resolution</summary>
		DicomTags.DepthSpatialResolution = new DicomTag(0x0022, 0x0035);

		/// <summary>(0022,0036) VR=FL VM=1 Maximum Depth Distortion</summary>
		DicomTags.MaximumDepthDistortion = new DicomTag(0x0022, 0x0036);

		/// <summary>(0022,0037) VR=FL VM=1 Along-scan Spatial Resolution</summary>
		DicomTags.AlongscanSpatialResolution = new DicomTag(0x0022, 0x0037);

		/// <summary>(0022,0038) VR=FL VM=1 Maximum Along-scan Distortion</summary>
		DicomTags.MaximumAlongscanDistortion = new DicomTag(0x0022, 0x0038);

		/// <summary>(0022,0039) VR=CS VM=1 Ophthalmic Image Orientation</summary>
		DicomTags.OphthalmicImageOrientation = new DicomTag(0x0022, 0x0039);

		/// <summary>(0022,0041) VR=FL VM=1 Depth of Transverse Image</summary>
		DicomTags.DepthOfTransverseImage = new DicomTag(0x0022, 0x0041);

		/// <summary>(0022,0042) VR=SQ VM=1 Mydriatic Agent Concentration Units Sequence</summary>
		DicomTags.MydriaticAgentConcentrationUnitsSequence = new DicomTag(0x0022, 0x0042);

		/// <summary>(0022,0048) VR=FL VM=1 Across-scan Spatial Resolution</summary>
		DicomTags.AcrossscanSpatialResolution = new DicomTag(0x0022, 0x0048);

		/// <summary>(0022,0049) VR=FL VM=1 Maximum Across-scan Distortion</summary>
		DicomTags.MaximumAcrossscanDistortion = new DicomTag(0x0022, 0x0049);

		/// <summary>(0022,004e) VR=DS VM=1 Mydriatic Agent Concentration</summary>
		DicomTags.MydriaticAgentConcentration = new DicomTag(0x0022, 0x004e);

		/// <summary>(0022,0055) VR=FL VM=1 Illumination Wave Length</summary>
		DicomTags.IlluminationWaveLength = new DicomTag(0x0022, 0x0055);

		/// <summary>(0022,0056) VR=FL VM=1 Illumination Power</summary>
		DicomTags.IlluminationPower = new DicomTag(0x0022, 0x0056);

		/// <summary>(0022,0057) VR=FL VM=1 Illumination Bandwidth</summary>
		DicomTags.IlluminationBandwidth = new DicomTag(0x0022, 0x0057);

		/// <summary>(0022,0058) VR=SQ VM=1 Mydriatic Agent Sequence</summary>
		DicomTags.MydriaticAgentSequence = new DicomTag(0x0022, 0x0058);

		/// <summary>(0028,0002) VR=US VM=1 Samples per Pixel</summary>
		DicomTags.SamplesPerPixel = new DicomTag(0x0028, 0x0002);

		/// <summary>(0028,0003) VR=US VM=1 Samples per Pixel Used</summary>
		DicomTags.SamplesPerPixelUsed = new DicomTag(0x0028, 0x0003);

		/// <summary>(0028,0004) VR=CS VM=1 Photometric Interpretation</summary>
		DicomTags.PhotometricInterpretation = new DicomTag(0x0028, 0x0004);

		/// <summary>(0028,0005) VR=US VM=1 Image Dimensions (Retired)</summary>
		DicomTags.ImageDimensionsRETIRED = new DicomTag(0x0028, 0x0005);

		/// <summary>(0028,0006) VR=US VM=1 Planar Configuration</summary>
		DicomTags.PlanarConfiguration = new DicomTag(0x0028, 0x0006);

		/// <summary>(0028,0008) VR=IS VM=1 Number of Frames</summary>
		DicomTags.NumberOfFrames = new DicomTag(0x0028, 0x0008);

		/// <summary>(0028,0009) VR=AT VM=1-n Frame Increment Pointer</summary>
		DicomTags.FrameIncrementPointer = new DicomTag(0x0028, 0x0009);

		/// <summary>(0028,000a) VR=AT VM=1-n Frame Dimension Pointer</summary>
		DicomTags.FrameDimensionPointer = new DicomTag(0x0028, 0x000a);

		/// <summary>(0028,0010) VR=US VM=1 Rows</summary>
		DicomTags.Rows = new DicomTag(0x0028, 0x0010);

		/// <summary>(0028,0011) VR=US VM=1 Columns</summary>
		DicomTags.Columns = new DicomTag(0x0028, 0x0011);

		/// <summary>(0028,0012) VR=US VM=1 Planes (Retired)</summary>
		DicomTags.PlanesRETIRED = new DicomTag(0x0028, 0x0012);

		/// <summary>(0028,0014) VR=US VM=1 Ultrasound Color Data Present</summary>
		DicomTags.UltrasoundColorDataPresent = new DicomTag(0x0028, 0x0014);

		/// <summary>(0028,0030) VR=DS VM=2 Pixel Spacing</summary>
		DicomTags.PixelSpacing = new DicomTag(0x0028, 0x0030);

		/// <summary>(0028,0031) VR=DS VM=2 Zoom Factor</summary>
		DicomTags.ZoomFactor = new DicomTag(0x0028, 0x0031);

		/// <summary>(0028,0032) VR=DS VM=2 Zoom Center</summary>
		DicomTags.ZoomCenter = new DicomTag(0x0028, 0x0032);

		/// <summary>(0028,0034) VR=IS VM=2 Pixel Aspect Ratio</summary>
		DicomTags.PixelAspectRatio = new DicomTag(0x0028, 0x0034);

		/// <summary>(0028,0040) VR=CS VM=1 Image Format (Retired)</summary>
		DicomTags.ImageFormatRETIRED = new DicomTag(0x0028, 0x0040);

		/// <summary>(0028,0050) VR=LO VM=1-n Manipulated Image (Retired)</summary>
		DicomTags.ManipulatedImageRETIRED = new DicomTag(0x0028, 0x0050);

		/// <summary>(0028,0051) VR=CS VM=1-n Corrected Image</summary>
		DicomTags.CorrectedImage = new DicomTag(0x0028, 0x0051);

		/// <summary>(0028,005f) VR=LO VM=1 Compression Recognition Code (Retired)</summary>
		DicomTags.CompressionRecognitionCodeRETIRED = new DicomTag(0x0028, 0x005f);

		/// <summary>(0028,0060) VR=CS VM=1 Compression Code (Retired)</summary>
		DicomTags.CompressionCodeRETIRED = new DicomTag(0x0028, 0x0060);

		/// <summary>(0028,0061) VR=SH VM=1 Compression Originator (Retired)</summary>
		DicomTags.CompressionOriginatorRETIRED = new DicomTag(0x0028, 0x0061);

		/// <summary>(0028,0062) VR=LO VM=1 Compression Label (Retired)</summary>
		DicomTags.CompressionLabelRETIRED = new DicomTag(0x0028, 0x0062);

		/// <summary>(0028,0063) VR=SH VM=1 Compression Description (Retired)</summary>
		DicomTags.CompressionDescriptionRETIRED = new DicomTag(0x0028, 0x0063);

		/// <summary>(0028,0065) VR=CS VM=1-n Compression Sequence (Retired)</summary>
		DicomTags.CompressionSequenceRETIRED = new DicomTag(0x0028, 0x0065);

		/// <summary>(0028,0066) VR=AT VM=1-n Compression Step Pointers (Retired)</summary>
		DicomTags.CompressionStepPointersRETIRED = new DicomTag(0x0028, 0x0066);

		/// <summary>(0028,0068) VR=US VM=1 Repeat Interval (Retired)</summary>
		DicomTags.RepeatIntervalRETIRED = new DicomTag(0x0028, 0x0068);

		/// <summary>(0028,0069) VR=US VM=1 Bits Grouped (Retired)</summary>
		DicomTags.BitsGroupedRETIRED = new DicomTag(0x0028, 0x0069);

		/// <summary>(0028,0070) VR=US VM=1-n Perimeter Table (Retired)</summary>
		DicomTags.PerimeterTableRETIRED = new DicomTag(0x0028, 0x0070);

		/// <summary>(0028,0071) VR=US/SS VM=1 Perimeter Value (Retired)</summary>
		DicomTags.PerimeterValueRETIRED = new DicomTag(0x0028, 0x0071);

		/// <summary>(0028,0080) VR=US VM=1 Predictor Rows (Retired)</summary>
		DicomTags.PredictorRowsRETIRED = new DicomTag(0x0028, 0x0080);

		/// <summary>(0028,0081) VR=US VM=1 Predictor Columns (Retired)</summary>
		DicomTags.PredictorColumnsRETIRED = new DicomTag(0x0028, 0x0081);

		/// <summary>(0028,0082) VR=US VM=1-n Predictor Constants (Retired)</summary>
		DicomTags.PredictorConstantsRETIRED = new DicomTag(0x0028, 0x0082);

		/// <summary>(0028,0090) VR=CS VM=1 Blocked Pixels (Retired)</summary>
		DicomTags.BlockedPixelsRETIRED = new DicomTag(0x0028, 0x0090);

		/// <summary>(0028,0091) VR=US VM=1 Block Rows (Retired)</summary>
		DicomTags.BlockRowsRETIRED = new DicomTag(0x0028, 0x0091);

		/// <summary>(0028,0092) VR=US VM=1 Block Columns (Retired)</summary>
		DicomTags.BlockColumnsRETIRED = new DicomTag(0x0028, 0x0092);

		/// <summary>(0028,0093) VR=US VM=1 Row Overlap (Retired)</summary>
		DicomTags.RowOverlapRETIRED = new DicomTag(0x0028, 0x0093);

		/// <summary>(0028,0094) VR=US VM=1 Column Overlap (Retired)</summary>
		DicomTags.ColumnOverlapRETIRED = new DicomTag(0x0028, 0x0094);

		/// <summary>(0028,0100) VR=US VM=1 Bits Allocated</summary>
		DicomTags.BitsAllocated = new DicomTag(0x0028, 0x0100);

		/// <summary>(0028,0101) VR=US VM=1 Bits Stored</summary>
		DicomTags.BitsStored = new DicomTag(0x0028, 0x0101);

		/// <summary>(0028,0102) VR=US VM=1 High Bit</summary>
		DicomTags.HighBit = new DicomTag(0x0028, 0x0102);

		/// <summary>(0028,0103) VR=US VM=1 Pixel Representation</summary>
		DicomTags.PixelRepresentation = new DicomTag(0x0028, 0x0103);

		/// <summary>(0028,0104) VR=US/SS VM=1 Smallest Valid Pixel Value (Retired)</summary>
		DicomTags.SmallestValidPixelValueRETIRED = new DicomTag(0x0028, 0x0104);

		/// <summary>(0028,0105) VR=US/SS VM=1 Largest Valid Pixel Value (Retired)</summary>
		DicomTags.LargestValidPixelValueRETIRED = new DicomTag(0x0028, 0x0105);

		/// <summary>(0028,0106) VR=US/SS VM=1 Smallest Image Pixel Value</summary>
		DicomTags.SmallestImagePixelValue = new DicomTag(0x0028, 0x0106);

		/// <summary>(0028,0107) VR=US/SS VM=1 Largest Image Pixel Value</summary>
		DicomTags.LargestImagePixelValue = new DicomTag(0x0028, 0x0107);

		/// <summary>(0028,0108) VR=US/SS VM=1 Smallest Pixel Value in Series</summary>
		DicomTags.SmallestPixelValueInSeries = new DicomTag(0x0028, 0x0108);

		/// <summary>(0028,0109) VR=US/SS VM=1 Largest Pixel Value in Series</summary>
		DicomTags.LargestPixelValueInSeries = new DicomTag(0x0028, 0x0109);

		/// <summary>(0028,0110) VR=US/SS VM=1 Smallest Image Pixel Value in Plane (Retired)</summary>
		DicomTags.SmallestImagePixelValueInPlaneRETIRED = new DicomTag(0x0028, 0x0110);

		/// <summary>(0028,0111) VR=US/SS VM=1 Largest Image Pixel Value in Plane (Retired)</summary>
		DicomTags.LargestImagePixelValueInPlaneRETIRED = new DicomTag(0x0028, 0x0111);

		/// <summary>(0028,0120) VR=US/SS VM=1 Pixel Padding Value</summary>
		DicomTags.PixelPaddingValue = new DicomTag(0x0028, 0x0120);

		/// <summary>(0028,0121) VR=US/SS VM=1 Pixel Padding Range Limit</summary>
		DicomTags.PixelPaddingRangeLimit = new DicomTag(0x0028, 0x0121);

		/// <summary>(0028,0200) VR=US VM=1 Image Location (Retired)</summary>
		DicomTags.ImageLocationRETIRED = new DicomTag(0x0028, 0x0200);

		/// <summary>(0028,0300) VR=CS VM=1 Quality Control Image</summary>
		DicomTags.QualityControlImage = new DicomTag(0x0028, 0x0300);

		/// <summary>(0028,0301) VR=CS VM=1 Burned In Annotation</summary>
		DicomTags.BurnedInAnnotation = new DicomTag(0x0028, 0x0301);

		/// <summary>(0028,0400) VR=LO VM=1 Transform Label (Retired)</summary>
		DicomTags.TransformLabelRETIRED = new DicomTag(0x0028, 0x0400);

		/// <summary>(0028,0401) VR=LO VM=1 Transform Version Number (Retired)</summary>
		DicomTags.TransformVersionNumberRETIRED = new DicomTag(0x0028, 0x0401);

		/// <summary>(0028,0402) VR=US VM=1 Number of Transform Steps (Retired)</summary>
		DicomTags.NumberOfTransformStepsRETIRED = new DicomTag(0x0028, 0x0402);

		/// <summary>(0028,0403) VR=LO VM=1-n Sequence of Compressed Data (Retired)</summary>
		DicomTags.SequenceOfCompressedDataRETIRED = new DicomTag(0x0028, 0x0403);

		/// <summary>(0028,0404) VR=AT VM=1-n Details of Coefficients (Retired)</summary>
		DicomTags.DetailsOfCoefficientsRETIRED = new DicomTag(0x0028, 0x0404);

		/// <summary>(0028,0400) VR=US VM=1 Rows For Nth Order Coefficients (Retired)</summary>
		DicomTags.RowsForNthOrderCoefficientsRETIRED = new DicomTag(0x0028, 0x0400);

		/// <summary>(0028,0401) VR=US VM=1 Columns For Nth Order Coefficients (Retired)</summary>
		DicomTags.ColumnsForNthOrderCoefficientsRETIRED = new DicomTag(0x0028, 0x0401);

		/// <summary>(0028,0402) VR=LO VM=1-n Coefficient Coding (Retired)</summary>
		DicomTags.CoefficientCodingRETIRED = new DicomTag(0x0028, 0x0402);

		/// <summary>(0028,0403) VR=AT VM=1-n Coefficient Coding Pointers (Retired)</summary>
		DicomTags.CoefficientCodingPointersRETIRED = new DicomTag(0x0028, 0x0403);

		/// <summary>(0028,0700) VR=LO VM=1 DCT Label (Retired)</summary>
		DicomTags.DCTLabelRETIRED = new DicomTag(0x0028, 0x0700);

		/// <summary>(0028,0701) VR=CS VM=1-n Data Block Description (Retired)</summary>
		DicomTags.DataBlockDescriptionRETIRED = new DicomTag(0x0028, 0x0701);

		/// <summary>(0028,0702) VR=AT VM=1-n Data Block (Retired)</summary>
		DicomTags.DataBlockRETIRED = new DicomTag(0x0028, 0x0702);

		/// <summary>(0028,0710) VR=US VM=1 Normalization Factor Format (Retired)</summary>
		DicomTags.NormalizationFactorFormatRETIRED = new DicomTag(0x0028, 0x0710);

		/// <summary>(0028,0720) VR=US VM=1 Zonal Map Number Format (Retired)</summary>
		DicomTags.ZonalMapNumberFormatRETIRED = new DicomTag(0x0028, 0x0720);

		/// <summary>(0028,0721) VR=AT VM=1-n Zonal Map Location (Retired)</summary>
		DicomTags.ZonalMapLocationRETIRED = new DicomTag(0x0028, 0x0721);

		/// <summary>(0028,0722) VR=US VM=1 Zonal Map Format (Retired)</summary>
		DicomTags.ZonalMapFormatRETIRED = new DicomTag(0x0028, 0x0722);

		/// <summary>(0028,0730) VR=US VM=1 Adaptive Map Format (Retired)</summary>
		DicomTags.AdaptiveMapFormatRETIRED = new DicomTag(0x0028, 0x0730);

		/// <summary>(0028,0740) VR=US VM=1 Code Number Format (Retired)</summary>
		DicomTags.CodeNumberFormatRETIRED = new DicomTag(0x0028, 0x0740);

		/// <summary>(0028,0800) VR=CS VM=1-n Code Label (Retired)</summary>
		DicomTags.CodeLabelRETIRED = new DicomTag(0x0028, 0x0800);

		/// <summary>(0028,0802) VR=US VM=1 Number of Table (Retired)</summary>
		DicomTags.NumberOfTableRETIRED = new DicomTag(0x0028, 0x0802);

		/// <summary>(0028,0803) VR=AT VM=1-n Code Table Location (Retired)</summary>
		DicomTags.CodeTableLocationRETIRED = new DicomTag(0x0028, 0x0803);

		/// <summary>(0028,0804) VR=US VM=1 Bits For Code Word (Retired)</summary>
		DicomTags.BitsForCodeWordRETIRED = new DicomTag(0x0028, 0x0804);

		/// <summary>(0028,0808) VR=AT VM=1-n Image Data Location (Retired)</summary>
		DicomTags.ImageDataLocationRETIRED = new DicomTag(0x0028, 0x0808);

		/// <summary>(0028,0a02) VR=CS VM=1 Pixel Spacing Calibration Type</summary>
		DicomTags.PixelSpacingCalibrationType = new DicomTag(0x0028, 0x0a02);

		/// <summary>(0028,0a04) VR=LO VM=1 Pixel Spacing Calibration Description</summary>
		DicomTags.PixelSpacingCalibrationDescription = new DicomTag(0x0028, 0x0a04);

		/// <summary>(0028,1040) VR=CS VM=1 Pixel Intensity Relationship</summary>
		DicomTags.PixelIntensityRelationship = new DicomTag(0x0028, 0x1040);

		/// <summary>(0028,1041) VR=SS VM=1 Pixel Intensity Relationship Sign</summary>
		DicomTags.PixelIntensityRelationshipSign = new DicomTag(0x0028, 0x1041);

		/// <summary>(0028,1050) VR=DS VM=1-n Window Center</summary>
		DicomTags.WindowCenter = new DicomTag(0x0028, 0x1050);

		/// <summary>(0028,1051) VR=DS VM=1-n Window Width</summary>
		DicomTags.WindowWidth = new DicomTag(0x0028, 0x1051);

		/// <summary>(0028,1052) VR=DS VM=1 Rescale Intercept</summary>
		DicomTags.RescaleIntercept = new DicomTag(0x0028, 0x1052);

		/// <summary>(0028,1053) VR=DS VM=1 Rescale Slope</summary>
		DicomTags.RescaleSlope = new DicomTag(0x0028, 0x1053);

		/// <summary>(0028,1054) VR=LO VM=1 Rescale Type</summary>
		DicomTags.RescaleType = new DicomTag(0x0028, 0x1054);

		/// <summary>(0028,1055) VR=LO VM=1-n Window Center &amp; Width Explanation</summary>
		DicomTags.WindowCenterWidthExplanation = new DicomTag(0x0028, 0x1055);

		/// <summary>(0028,1056) VR=CS VM=1 VOI LUT Function</summary>
		DicomTags.VOILUTFunction = new DicomTag(0x0028, 0x1056);

		/// <summary>(0028,1080) VR=CS VM=1 Gray Scale (Retired)</summary>
		DicomTags.GrayScaleRETIRED = new DicomTag(0x0028, 0x1080);

		/// <summary>(0028,1090) VR=CS VM=1 Recommended Viewing Mode</summary>
		DicomTags.RecommendedViewingMode = new DicomTag(0x0028, 0x1090);

		/// <summary>(0028,1100) VR=US/SS VM=3 Gray Lookup Table Descriptor (Retired)</summary>
		DicomTags.GrayLookupTableDescriptorRETIRED = new DicomTag(0x0028, 0x1100);

		/// <summary>(0028,1101) VR=US/SS VM=3 Red Palette Color Lookup Table Descriptor</summary>
		DicomTags.RedPaletteColorLookupTableDescriptor = new DicomTag(0x0028, 0x1101);

		/// <summary>(0028,1102) VR=US/SS VM=3 Green Palette Color Lookup Table Descriptor</summary>
		DicomTags.GreenPaletteColorLookupTableDescriptor = new DicomTag(0x0028, 0x1102);

		/// <summary>(0028,1103) VR=US/SS VM=3 Blue Palette Color Lookup Table Descriptor</summary>
		DicomTags.BluePaletteColorLookupTableDescriptor = new DicomTag(0x0028, 0x1103);

		/// <summary>(0028,1111) VR=US/SS VM=4 Large Red Palette Color Lookup Table Descriptor (Retired)</summary>
		DicomTags.LargeRedPaletteColorLookupTableDescriptorRETIRED = new DicomTag(0x0028, 0x1111);

		/// <summary>(0028,1112) VR=US/SS VM=4 Large Green Palette Color Lookup Table Descriptor (Retired)</summary>
		DicomTags.LargeGreenPaletteColorLookupTableDescriptorRETIRED = new DicomTag(0x0028, 0x1112);

		/// <summary>(0028,1113) VR=US/SS VM=4 Large Blue Palette Color Lookup Table Descriptor (Retired)</summary>
		DicomTags.LargeBluePaletteColorLookupTableDescriptorRETIRED = new DicomTag(0x0028, 0x1113);

		/// <summary>(0028,1199) VR=UI VM=1 Palette Color Lookup Table UID</summary>
		DicomTags.PaletteColorLookupTableUID = new DicomTag(0x0028, 0x1199);

		/// <summary>(0028,1200) VR=US/SS/OW VM=1-n Gray Lookup Table Data (Retired)</summary>
		DicomTags.GrayLookupTableDataRETIRED = new DicomTag(0x0028, 0x1200);

		/// <summary>(0028,1201) VR=OW VM=1 Red Palette Color Lookup Table Data</summary>
		DicomTags.RedPaletteColorLookupTableData = new DicomTag(0x0028, 0x1201);

		/// <summary>(0028,1202) VR=OW VM=1 Green Palette Color Lookup Table Data</summary>
		DicomTags.GreenPaletteColorLookupTableData = new DicomTag(0x0028, 0x1202);

		/// <summary>(0028,1203) VR=OW VM=1 Blue Palette Color Lookup Table Data</summary>
		DicomTags.BluePaletteColorLookupTableData = new DicomTag(0x0028, 0x1203);

		/// <summary>(0028,1211) VR=OW VM=1 Large Red Palette Color Lookup Table Data (Retired)</summary>
		DicomTags.LargeRedPaletteColorLookupTableDataRETIRED = new DicomTag(0x0028, 0x1211);

		/// <summary>(0028,1212) VR=OW VM=1 Large Green Palette Color Lookup Table Data (Retired)</summary>
		DicomTags.LargeGreenPaletteColorLookupTableDataRETIRED = new DicomTag(0x0028, 0x1212);

		/// <summary>(0028,1213) VR=OW VM=1 Large Blue Palette Color Lookup Table Data (Retired)</summary>
		DicomTags.LargeBluePaletteColorLookupTableDataRETIRED = new DicomTag(0x0028, 0x1213);

		/// <summary>(0028,1214) VR=UI VM=1 Large Palette Color Lookup Table UID (Retired)</summary>
		DicomTags.LargePaletteColorLookupTableUIDRETIRED = new DicomTag(0x0028, 0x1214);

		/// <summary>(0028,1221) VR=OW VM=1 Segmented Red Palette Color Lookup Table Data</summary>
		DicomTags.SegmentedRedPaletteColorLookupTableData = new DicomTag(0x0028, 0x1221);

		/// <summary>(0028,1222) VR=OW VM=1 Segmented Green Palette Color Lookup Table Data</summary>
		DicomTags.SegmentedGreenPaletteColorLookupTableData = new DicomTag(0x0028, 0x1222);

		/// <summary>(0028,1223) VR=OW VM=1 Segmented Blue Palette Color Lookup Table Data</summary>
		DicomTags.SegmentedBluePaletteColorLookupTableData = new DicomTag(0x0028, 0x1223);

		/// <summary>(0028,1300) VR=CS VM=1 Implant Present</summary>
		DicomTags.ImplantPresent = new DicomTag(0x0028, 0x1300);

		/// <summary>(0028,1350) VR=CS VM=1 Partial View</summary>
		DicomTags.PartialView = new DicomTag(0x0028, 0x1350);

		/// <summary>(0028,1351) VR=ST VM=1 Partial View Description</summary>
		DicomTags.PartialViewDescription = new DicomTag(0x0028, 0x1351);

		/// <summary>(0028,1352) VR=SQ VM=1 Partial View Code Sequence</summary>
		DicomTags.PartialViewCodeSequence = new DicomTag(0x0028, 0x1352);

		/// <summary>(0028,135a) VR=CS VM=1 Spatial Locations Preserved</summary>
		DicomTags.SpatialLocationsPreserved = new DicomTag(0x0028, 0x135a);

		/// <summary>(0028,2000) VR=OB VM=1 ICC Profile</summary>
		DicomTags.ICCProfile = new DicomTag(0x0028, 0x2000);

		/// <summary>(0028,2110) VR=CS VM=1 Lossy Image Compression</summary>
		DicomTags.LossyImageCompression = new DicomTag(0x0028, 0x2110);

		/// <summary>(0028,2112) VR=DS VM=1-n Lossy Image Compression Ratio</summary>
		DicomTags.LossyImageCompressionRatio = new DicomTag(0x0028, 0x2112);

		/// <summary>(0028,2114) VR=CS VM=1-n Lossy Image Compression Method</summary>
		DicomTags.LossyImageCompressionMethod = new DicomTag(0x0028, 0x2114);

		/// <summary>(0028,3000) VR=SQ VM=1 Modality LUT Sequence</summary>
		DicomTags.ModalityLUTSequence = new DicomTag(0x0028, 0x3000);

		/// <summary>(0028,3002) VR=US/SS VM=3 LUT Descriptor</summary>
		DicomTags.LUTDescriptor = new DicomTag(0x0028, 0x3002);

		/// <summary>(0028,3003) VR=LO VM=1 LUT Explanation</summary>
		DicomTags.LUTExplanation = new DicomTag(0x0028, 0x3003);

		/// <summary>(0028,3004) VR=LO VM=1 Modality LUT Type</summary>
		DicomTags.ModalityLUTType = new DicomTag(0x0028, 0x3004);

		/// <summary>(0028,3006) VR=US/SS/OW VM=1-n LUT Data</summary>
		DicomTags.LUTData = new DicomTag(0x0028, 0x3006);

		/// <summary>(0028,3010) VR=SQ VM=1 VOI LUT Sequence</summary>
		DicomTags.VOILUTSequence = new DicomTag(0x0028, 0x3010);

		/// <summary>(0028,3110) VR=SQ VM=1 Softcopy VOI LUT Sequence</summary>
		DicomTags.SoftcopyVOILUTSequence = new DicomTag(0x0028, 0x3110);

		/// <summary>(0028,4000) VR=LT VM=1 Image Presentation Comments (Retired)</summary>
		DicomTags.ImagePresentationCommentsRETIRED = new DicomTag(0x0028, 0x4000);

		/// <summary>(0028,5000) VR=SQ VM=1 Bi-Plane Acquisition Sequence (Retired)</summary>
		DicomTags.BiPlaneAcquisitionSequenceRETIRED = new DicomTag(0x0028, 0x5000);

		/// <summary>(0028,6010) VR=US VM=1 Representative Frame Number</summary>
		DicomTags.RepresentativeFrameNumber = new DicomTag(0x0028, 0x6010);

		/// <summary>(0028,6020) VR=US VM=1-n Frame Numbers of Interest (FOI)</summary>
		DicomTags.FrameNumbersOfInterestFOI = new DicomTag(0x0028, 0x6020);

		/// <summary>(0028,6022) VR=LO VM=1-n Frame(s) of Interest Description</summary>
		DicomTags.FramesOfInterestDescription = new DicomTag(0x0028, 0x6022);

		/// <summary>(0028,6023) VR=CS VM=1-n Frame of Interest Type</summary>
		DicomTags.FrameOfInterestType = new DicomTag(0x0028, 0x6023);

		/// <summary>(0028,6030) VR=US VM=1-n Mask Pointer(s) (Retired)</summary>
		DicomTags.MaskPointersRETIRED = new DicomTag(0x0028, 0x6030);

		/// <summary>(0028,6040) VR=US VM=1-n R Wave Pointer</summary>
		DicomTags.RWavePointer = new DicomTag(0x0028, 0x6040);

		/// <summary>(0028,6100) VR=SQ VM=1 Mask Subtraction Sequence</summary>
		DicomTags.MaskSubtractionSequence = new DicomTag(0x0028, 0x6100);

		/// <summary>(0028,6101) VR=CS VM=1 Mask Operation</summary>
		DicomTags.MaskOperation = new DicomTag(0x0028, 0x6101);

		/// <summary>(0028,6102) VR=US VM=2-2n Applicable Frame Range</summary>
		DicomTags.ApplicableFrameRange = new DicomTag(0x0028, 0x6102);

		/// <summary>(0028,6110) VR=US VM=1-n Mask Frame Numbers</summary>
		DicomTags.MaskFrameNumbers = new DicomTag(0x0028, 0x6110);

		/// <summary>(0028,6112) VR=US VM=1 Contrast Frame Averaging</summary>
		DicomTags.ContrastFrameAveraging = new DicomTag(0x0028, 0x6112);

		/// <summary>(0028,6114) VR=FL VM=2 Mask Sub-pixel Shift</summary>
		DicomTags.MaskSubpixelShift = new DicomTag(0x0028, 0x6114);

		/// <summary>(0028,6120) VR=SS VM=1 TID Offset</summary>
		DicomTags.TIDOffset = new DicomTag(0x0028, 0x6120);

		/// <summary>(0028,6190) VR=ST VM=1 Mask Operation Explanation</summary>
		DicomTags.MaskOperationExplanation = new DicomTag(0x0028, 0x6190);

		/// <summary>(0028,7fe0) VR=UT VM=1 Pixel Data Provider URL</summary>
		DicomTags.PixelDataProviderURL = new DicomTag(0x0028, 0x7fe0);

		/// <summary>(0028,9001) VR=UL VM=1 Data Point Rows</summary>
		DicomTags.DataPointRows = new DicomTag(0x0028, 0x9001);

		/// <summary>(0028,9002) VR=UL VM=1 Data Point Columns</summary>
		DicomTags.DataPointColumns = new DicomTag(0x0028, 0x9002);

		/// <summary>(0028,9003) VR=CS VM=1 Signal Domain Columns</summary>
		DicomTags.SignalDomainColumns = new DicomTag(0x0028, 0x9003);

		/// <summary>(0028,9099) VR=US VM=1 Largest Monochrome Pixel Value (Retired)</summary>
		DicomTags.LargestMonochromePixelValueRETIRED = new DicomTag(0x0028, 0x9099);

		/// <summary>(0028,9108) VR=CS VM=1 Data Representation</summary>
		DicomTags.DataRepresentation = new DicomTag(0x0028, 0x9108);

		/// <summary>(0028,9110) VR=SQ VM=1 Pixel Measures Sequence</summary>
		DicomTags.PixelMeasuresSequence = new DicomTag(0x0028, 0x9110);

		/// <summary>(0028,9132) VR=SQ VM=1 Frame VOI LUT Sequence</summary>
		DicomTags.FrameVOILUTSequence = new DicomTag(0x0028, 0x9132);

		/// <summary>(0028,9145) VR=SQ VM=1 Pixel Value Transformation Sequence</summary>
		DicomTags.PixelValueTransformationSequence = new DicomTag(0x0028, 0x9145);

		/// <summary>(0028,9235) VR=CS VM=1 Signal Domain Rows</summary>
		DicomTags.SignalDomainRows = new DicomTag(0x0028, 0x9235);

		/// <summary>(0028,9411) VR=FL VM=1 Display Filter Percentage</summary>
		DicomTags.DisplayFilterPercentage = new DicomTag(0x0028, 0x9411);

		/// <summary>(0028,9415) VR=SQ VM=1 Frame Pixel Shift Sequence</summary>
		DicomTags.FramePixelShiftSequence = new DicomTag(0x0028, 0x9415);

		/// <summary>(0028,9416) VR=US VM=1 Subtraction Item ID</summary>
		DicomTags.SubtractionItemID = new DicomTag(0x0028, 0x9416);

		/// <summary>(0028,9422) VR=SQ VM=1 Pixel Intensity Relationship LUT Sequence</summary>
		DicomTags.PixelIntensityRelationshipLUTSequence = new DicomTag(0x0028, 0x9422);

		/// <summary>(0028,9443) VR=SQ VM=1 Frame Pixel Data Properties Sequence</summary>
		DicomTags.FramePixelDataPropertiesSequence = new DicomTag(0x0028, 0x9443);

		/// <summary>(0028,9444) VR=CS VM=1 Geometrical Properties</summary>
		DicomTags.GeometricalProperties = new DicomTag(0x0028, 0x9444);

		/// <summary>(0028,9445) VR=FL VM=1 Geometric Maximum Distortion</summary>
		DicomTags.GeometricMaximumDistortion = new DicomTag(0x0028, 0x9445);

		/// <summary>(0028,9446) VR=CS VM=1-n Image Processing Applied</summary>
		DicomTags.ImageProcessingApplied = new DicomTag(0x0028, 0x9446);

		/// <summary>(0028,9454) VR=CS VM=1 Mask Selection Mode</summary>
		DicomTags.MaskSelectionMode = new DicomTag(0x0028, 0x9454);

		/// <summary>(0028,9474) VR=CS VM=1 LUT Function</summary>
		DicomTags.LUTFunction = new DicomTag(0x0028, 0x9474);

		/// <summary>(0028,9520) VR=DS VM=16 Image to Equipment Mapping Matrix</summary>
		DicomTags.ImageToEquipmentMappingMatrix = new DicomTag(0x0028, 0x9520);

		/// <summary>(0028,9537) VR=CS VM=1 Equipment Coordinate System Identification</summary>
		DicomTags.EquipmentCoordinateSystemIdentification = new DicomTag(0x0028, 0x9537);

		/// <summary>(0032,000a) VR=CS VM=1 Study Status ID (Retired)</summary>
		DicomTags.StudyStatusIDRETIRED = new DicomTag(0x0032, 0x000a);

		/// <summary>(0032,000c) VR=CS VM=1 Study Priority ID (Retired)</summary>
		DicomTags.StudyPriorityIDRETIRED = new DicomTag(0x0032, 0x000c);

		/// <summary>(0032,0012) VR=LO VM=1 Study ID Issuer (Retired)</summary>
		DicomTags.StudyIDIssuerRETIRED = new DicomTag(0x0032, 0x0012);

		/// <summary>(0032,0032) VR=DA VM=1 Study Verified Date (Retired)</summary>
		DicomTags.StudyVerifiedDateRETIRED = new DicomTag(0x0032, 0x0032);

		/// <summary>(0032,0033) VR=TM VM=1 Study Verified Time (Retired)</summary>
		DicomTags.StudyVerifiedTimeRETIRED = new DicomTag(0x0032, 0x0033);

		/// <summary>(0032,0034) VR=DA VM=1 Study Read Date (Retired)</summary>
		DicomTags.StudyReadDateRETIRED = new DicomTag(0x0032, 0x0034);

		/// <summary>(0032,0035) VR=TM VM=1 Study Read Time (Retired)</summary>
		DicomTags.StudyReadTimeRETIRED = new DicomTag(0x0032, 0x0035);

		/// <summary>(0032,1000) VR=DA VM=1 Scheduled Study Start Date (Retired)</summary>
		DicomTags.ScheduledStudyStartDateRETIRED = new DicomTag(0x0032, 0x1000);

		/// <summary>(0032,1001) VR=TM VM=1 Scheduled Study Start Time (Retired)</summary>
		DicomTags.ScheduledStudyStartTimeRETIRED = new DicomTag(0x0032, 0x1001);

		/// <summary>(0032,1010) VR=DA VM=1 Scheduled Study Stop Date (Retired)</summary>
		DicomTags.ScheduledStudyStopDateRETIRED = new DicomTag(0x0032, 0x1010);

		/// <summary>(0032,1011) VR=TM VM=1 Scheduled Study Stop Time (Retired)</summary>
		DicomTags.ScheduledStudyStopTimeRETIRED = new DicomTag(0x0032, 0x1011);

		/// <summary>(0032,1020) VR=LO VM=1 Scheduled Study Location (Retired)</summary>
		DicomTags.ScheduledStudyLocationRETIRED = new DicomTag(0x0032, 0x1020);

		/// <summary>(0032,1021) VR=AE VM=1-n Scheduled Study Location AE Title (Retired)</summary>
		DicomTags.ScheduledStudyLocationAETitleRETIRED = new DicomTag(0x0032, 0x1021);

		/// <summary>(0032,1030) VR=LO VM=1 Reason for Study (Retired)</summary>
		DicomTags.ReasonForStudyRETIRED = new DicomTag(0x0032, 0x1030);

		/// <summary>(0032,1031) VR=SQ VM=1 Requesting Physician Identification Sequence</summary>
		DicomTags.RequestingPhysicianIdentificationSequence = new DicomTag(0x0032, 0x1031);

		/// <summary>(0032,1032) VR=PN VM=1 Requesting Physician</summary>
		DicomTags.RequestingPhysician = new DicomTag(0x0032, 0x1032);

		/// <summary>(0032,1033) VR=LO VM=1 Requesting Service</summary>
		DicomTags.RequestingService = new DicomTag(0x0032, 0x1033);

		/// <summary>(0032,1040) VR=DA VM=1 Study Arrival Date (Retired)</summary>
		DicomTags.StudyArrivalDateRETIRED = new DicomTag(0x0032, 0x1040);

		/// <summary>(0032,1041) VR=TM VM=1 Study Arrival Time (Retired)</summary>
		DicomTags.StudyArrivalTimeRETIRED = new DicomTag(0x0032, 0x1041);

		/// <summary>(0032,1050) VR=DA VM=1 Study Completion Date (Retired)</summary>
		DicomTags.StudyCompletionDateRETIRED = new DicomTag(0x0032, 0x1050);

		/// <summary>(0032,1051) VR=TM VM=1 Study Completion Time (Retired)</summary>
		DicomTags.StudyCompletionTimeRETIRED = new DicomTag(0x0032, 0x1051);

		/// <summary>(0032,1055) VR=CS VM=1 Study Component Status ID (Retired)</summary>
		DicomTags.StudyComponentStatusIDRETIRED = new DicomTag(0x0032, 0x1055);

		/// <summary>(0032,1060) VR=LO VM=1 Requested Procedure Description</summary>
		DicomTags.RequestedProcedureDescription = new DicomTag(0x0032, 0x1060);

		/// <summary>(0032,1064) VR=SQ VM=1 Requested Procedure Code Sequence</summary>
		DicomTags.RequestedProcedureCodeSequence = new DicomTag(0x0032, 0x1064);

		/// <summary>(0032,1070) VR=LO VM=1 Requested Contrast Agent</summary>
		DicomTags.RequestedContrastAgent = new DicomTag(0x0032, 0x1070);

		/// <summary>(0032,4000) VR=LT VM=1 Study Comments (Retired)</summary>
		DicomTags.StudyCommentsRETIRED = new DicomTag(0x0032, 0x4000);

		/// <summary>(0038,0004) VR=SQ VM=1 Referenced Patient Alias Sequence</summary>
		DicomTags.ReferencedPatientAliasSequence = new DicomTag(0x0038, 0x0004);

		/// <summary>(0038,0008) VR=CS VM=1 Visit Status ID</summary>
		DicomTags.VisitStatusID = new DicomTag(0x0038, 0x0008);

		/// <summary>(0038,0010) VR=LO VM=1 Admission ID</summary>
		DicomTags.AdmissionID = new DicomTag(0x0038, 0x0010);

		/// <summary>(0038,0011) VR=LO VM=1 Issuer of Admission ID</summary>
		DicomTags.IssuerOfAdmissionID = new DicomTag(0x0038, 0x0011);

		/// <summary>(0038,0016) VR=LO VM=1 Route of Admissions</summary>
		DicomTags.RouteOfAdmissions = new DicomTag(0x0038, 0x0016);

		/// <summary>(0038,001a) VR=DA VM=1 Scheduled Admission Date (Retired)</summary>
		DicomTags.ScheduledAdmissionDateRETIRED = new DicomTag(0x0038, 0x001a);

		/// <summary>(0038,001b) VR=TM VM=1 Scheduled Admission Time (Retired)</summary>
		DicomTags.ScheduledAdmissionTimeRETIRED = new DicomTag(0x0038, 0x001b);

		/// <summary>(0038,001c) VR=DA VM=1 Scheduled Discharge Date (Retired)</summary>
		DicomTags.ScheduledDischargeDateRETIRED = new DicomTag(0x0038, 0x001c);

		/// <summary>(0038,001d) VR=TM VM=1 Scheduled Discharge Time (Retired)</summary>
		DicomTags.ScheduledDischargeTimeRETIRED = new DicomTag(0x0038, 0x001d);

		/// <summary>(0038,001e) VR=LO VM=1 Scheduled Patient Institution Residence (Retired)</summary>
		DicomTags.ScheduledPatientInstitutionResidenceRETIRED = new DicomTag(0x0038, 0x001e);

		/// <summary>(0038,0020) VR=DA VM=1 Admitting Date</summary>
		DicomTags.AdmittingDate = new DicomTag(0x0038, 0x0020);

		/// <summary>(0038,0021) VR=TM VM=1 Admitting Time</summary>
		DicomTags.AdmittingTime = new DicomTag(0x0038, 0x0021);

		/// <summary>(0038,0030) VR=DA VM=1 Discharge Date (Retired)</summary>
		DicomTags.DischargeDateRETIRED = new DicomTag(0x0038, 0x0030);

		/// <summary>(0038,0032) VR=TM VM=1 Discharge Time (Retired)</summary>
		DicomTags.DischargeTimeRETIRED = new DicomTag(0x0038, 0x0032);

		/// <summary>(0038,0040) VR=LO VM=1 Discharge Diagnosis Description (Retired)</summary>
		DicomTags.DischargeDiagnosisDescriptionRETIRED = new DicomTag(0x0038, 0x0040);

		/// <summary>(0038,0044) VR=SQ VM=1 Discharge Diagnosis Code Sequence (Retired)</summary>
		DicomTags.DischargeDiagnosisCodeSequenceRETIRED = new DicomTag(0x0038, 0x0044);

		/// <summary>(0038,0050) VR=LO VM=1 Special Needs</summary>
		DicomTags.SpecialNeeds = new DicomTag(0x0038, 0x0050);

		/// <summary>(0038,0060) VR=LO VM=1 Service Episode ID</summary>
		DicomTags.ServiceEpisodeID = new DicomTag(0x0038, 0x0060);

		/// <summary>(0038,0061) VR=LO VM=1 Issuer of Service Episode ID</summary>
		DicomTags.IssuerOfServiceEpisodeID = new DicomTag(0x0038, 0x0061);

		/// <summary>(0038,0062) VR=LO VM=1 Service Episode Description</summary>
		DicomTags.ServiceEpisodeDescription = new DicomTag(0x0038, 0x0062);

		/// <summary>(0038,0100) VR=SQ VM=1 Pertinent Documents Sequence</summary>
		DicomTags.PertinentDocumentsSequence = new DicomTag(0x0038, 0x0100);

		/// <summary>(0038,0300) VR=LO VM=1 Current Patient Location</summary>
		DicomTags.CurrentPatientLocation = new DicomTag(0x0038, 0x0300);

		/// <summary>(0038,0400) VR=LO VM=1 Patient's Institution Residence</summary>
		DicomTags.PatientsInstitutionResidence = new DicomTag(0x0038, 0x0400);

		/// <summary>(0038,0500) VR=LO VM=1 Patient State</summary>
		DicomTags.PatientState = new DicomTag(0x0038, 0x0500);

		/// <summary>(0038,0502) VR=SQ VM=1 Patient Clinical Trial Participation Sequence</summary>
		DicomTags.PatientClinicalTrialParticipationSequence = new DicomTag(0x0038, 0x0502);

		/// <summary>(0038,4000) VR=LT VM=1 Visit Comments</summary>
		DicomTags.VisitComments = new DicomTag(0x0038, 0x4000);

		/// <summary>(003a,0004) VR=CS VM=1 Waveform Originality</summary>
		DicomTags.WaveformOriginality = new DicomTag(0x003a, 0x0004);

		/// <summary>(003a,0005) VR=US VM=1 Number of Waveform Channels</summary>
		DicomTags.NumberOfWaveformChannels = new DicomTag(0x003a, 0x0005);

		/// <summary>(003a,0010) VR=UL VM=1 Number of Waveform Samples</summary>
		DicomTags.NumberOfWaveformSamples = new DicomTag(0x003a, 0x0010);

		/// <summary>(003a,001a) VR=DS VM=1 Sampling Frequency</summary>
		DicomTags.SamplingFrequency = new DicomTag(0x003a, 0x001a);

		/// <summary>(003a,0020) VR=SH VM=1 Multiplex Group Label</summary>
		DicomTags.MultiplexGroupLabel = new DicomTag(0x003a, 0x0020);

		/// <summary>(003a,0200) VR=SQ VM=1 Channel Definition Sequence</summary>
		DicomTags.ChannelDefinitionSequence = new DicomTag(0x003a, 0x0200);

		/// <summary>(003a,0202) VR=IS VM=1 Waveform Channel Number</summary>
		DicomTags.WaveformChannelNumber = new DicomTag(0x003a, 0x0202);

		/// <summary>(003a,0203) VR=SH VM=1 Channel Label</summary>
		DicomTags.ChannelLabel = new DicomTag(0x003a, 0x0203);

		/// <summary>(003a,0205) VR=CS VM=1-n Channel Status</summary>
		DicomTags.ChannelStatus = new DicomTag(0x003a, 0x0205);

		/// <summary>(003a,0208) VR=SQ VM=1 Channel Source Sequence</summary>
		DicomTags.ChannelSourceSequence = new DicomTag(0x003a, 0x0208);

		/// <summary>(003a,0209) VR=SQ VM=1 Channel Source Modifiers Sequence</summary>
		DicomTags.ChannelSourceModifiersSequence = new DicomTag(0x003a, 0x0209);

		/// <summary>(003a,020a) VR=SQ VM=1 Source Waveform Sequence</summary>
		DicomTags.SourceWaveformSequence = new DicomTag(0x003a, 0x020a);

		/// <summary>(003a,020c) VR=LO VM=1 Channel Derivation Description</summary>
		DicomTags.ChannelDerivationDescription = new DicomTag(0x003a, 0x020c);

		/// <summary>(003a,0210) VR=DS VM=1 Channel Sensitivity</summary>
		DicomTags.ChannelSensitivity = new DicomTag(0x003a, 0x0210);

		/// <summary>(003a,0211) VR=SQ VM=1 Channel Sensitivity Units Sequence</summary>
		DicomTags.ChannelSensitivityUnitsSequence = new DicomTag(0x003a, 0x0211);

		/// <summary>(003a,0212) VR=DS VM=1 Channel Sensitivity Correction Factor</summary>
		DicomTags.ChannelSensitivityCorrectionFactor = new DicomTag(0x003a, 0x0212);

		/// <summary>(003a,0213) VR=DS VM=1 Channel Baseline</summary>
		DicomTags.ChannelBaseline = new DicomTag(0x003a, 0x0213);

		/// <summary>(003a,0214) VR=DS VM=1 Channel Time Skew</summary>
		DicomTags.ChannelTimeSkew = new DicomTag(0x003a, 0x0214);

		/// <summary>(003a,0215) VR=DS VM=1 Channel Sample Skew</summary>
		DicomTags.ChannelSampleSkew = new DicomTag(0x003a, 0x0215);

		/// <summary>(003a,0218) VR=DS VM=1 Channel Offset</summary>
		DicomTags.ChannelOffset = new DicomTag(0x003a, 0x0218);

		/// <summary>(003a,021a) VR=US VM=1 Waveform Bits Stored</summary>
		DicomTags.WaveformBitsStored = new DicomTag(0x003a, 0x021a);

		/// <summary>(003a,0220) VR=DS VM=1 Filter Low Frequency</summary>
		DicomTags.FilterLowFrequency = new DicomTag(0x003a, 0x0220);

		/// <summary>(003a,0221) VR=DS VM=1 Filter High Frequency</summary>
		DicomTags.FilterHighFrequency = new DicomTag(0x003a, 0x0221);

		/// <summary>(003a,0222) VR=DS VM=1 Notch Filter Frequency</summary>
		DicomTags.NotchFilterFrequency = new DicomTag(0x003a, 0x0222);

		/// <summary>(003a,0223) VR=DS VM=1 Notch Filter Bandwidth</summary>
		DicomTags.NotchFilterBandwidth = new DicomTag(0x003a, 0x0223);

		/// <summary>(003a,0230) VR=FL VM=1 Waveform Data Display Scale</summary>
		DicomTags.WaveformDataDisplayScale = new DicomTag(0x003a, 0x0230);

		/// <summary>(003a,0231) VR=US VM=3 Waveform Display Background CIELab Value</summary>
		DicomTags.WaveformDisplayBackgroundCIELabValue = new DicomTag(0x003a, 0x0231);

		/// <summary>(003a,0240) VR=SQ VM=1 Waveform Presentation Group Sequence</summary>
		DicomTags.WaveformPresentationGroupSequence = new DicomTag(0x003a, 0x0240);

		/// <summary>(003a,0241) VR=US VM=1 Presentation Group Number</summary>
		DicomTags.PresentationGroupNumber = new DicomTag(0x003a, 0x0241);

		/// <summary>(003a,0242) VR=SQ VM=1 Channel Display Sequence</summary>
		DicomTags.ChannelDisplaySequence = new DicomTag(0x003a, 0x0242);

		/// <summary>(003a,0244) VR=US VM=3 Channel Recommended Display CIELab Value</summary>
		DicomTags.ChannelRecommendedDisplayCIELabValue = new DicomTag(0x003a, 0x0244);

		/// <summary>(003a,0245) VR=FL VM=1 Channel Position</summary>
		DicomTags.ChannelPosition = new DicomTag(0x003a, 0x0245);

		/// <summary>(003a,0246) VR=CS VM=1 Display Shading Flag</summary>
		DicomTags.DisplayShadingFlag = new DicomTag(0x003a, 0x0246);

		/// <summary>(003a,0247) VR=FL VM=1 Fractional Channel Display Scale</summary>
		DicomTags.FractionalChannelDisplayScale = new DicomTag(0x003a, 0x0247);

		/// <summary>(003a,0248) VR=FL VM=1 Absolute Channel Display Scale</summary>
		DicomTags.AbsoluteChannelDisplayScale = new DicomTag(0x003a, 0x0248);

		/// <summary>(003a,0300) VR=SQ VM=1 Multiplexed Audio Channels Description Code Sequence</summary>
		DicomTags.MultiplexedAudioChannelsDescriptionCodeSequence = new DicomTag(0x003a, 0x0300);

		/// <summary>(003a,0301) VR=IS VM=1 Channel Identification Code</summary>
		DicomTags.ChannelIdentificationCode = new DicomTag(0x003a, 0x0301);

		/// <summary>(003a,0302) VR=CS VM=1 Channel Mode</summary>
		DicomTags.ChannelMode = new DicomTag(0x003a, 0x0302);

		/// <summary>(0040,0001) VR=AE VM=1-n Scheduled Station AE Title</summary>
		DicomTags.ScheduledStationAETitle = new DicomTag(0x0040, 0x0001);

		/// <summary>(0040,0002) VR=DA VM=1 Scheduled Procedure Step Start Date</summary>
		DicomTags.ScheduledProcedureStepStartDate = new DicomTag(0x0040, 0x0002);

		/// <summary>(0040,0003) VR=TM VM=1 Scheduled Procedure Step Start Time</summary>
		DicomTags.ScheduledProcedureStepStartTime = new DicomTag(0x0040, 0x0003);

		/// <summary>(0040,0004) VR=DA VM=1 Scheduled Procedure Step End Date</summary>
		DicomTags.ScheduledProcedureStepEndDate = new DicomTag(0x0040, 0x0004);

		/// <summary>(0040,0005) VR=TM VM=1 Scheduled Procedure Step End Time</summary>
		DicomTags.ScheduledProcedureStepEndTime = new DicomTag(0x0040, 0x0005);

		/// <summary>(0040,0006) VR=PN VM=1 Scheduled Performing Physician's Name</summary>
		DicomTags.ScheduledPerformingPhysiciansName = new DicomTag(0x0040, 0x0006);

		/// <summary>(0040,0007) VR=LO VM=1 Scheduled Procedure Step Description</summary>
		DicomTags.ScheduledProcedureStepDescription = new DicomTag(0x0040, 0x0007);

		/// <summary>(0040,0008) VR=SQ VM=1 Scheduled Protocol Code Sequence</summary>
		DicomTags.ScheduledProtocolCodeSequence = new DicomTag(0x0040, 0x0008);

		/// <summary>(0040,0009) VR=SH VM=1 Scheduled Procedure Step ID</summary>
		DicomTags.ScheduledProcedureStepID = new DicomTag(0x0040, 0x0009);

		/// <summary>(0040,000a) VR=SQ VM=1 Stage Code Sequence</summary>
		DicomTags.StageCodeSequence = new DicomTag(0x0040, 0x000a);

		/// <summary>(0040,000b) VR=SQ VM=1 Scheduled Performing Physician Identification Sequence</summary>
		DicomTags.ScheduledPerformingPhysicianIdentificationSequence = new DicomTag(0x0040, 0x000b);

		/// <summary>(0040,0010) VR=SH VM=1-n Scheduled Station Name</summary>
		DicomTags.ScheduledStationName = new DicomTag(0x0040, 0x0010);

		/// <summary>(0040,0011) VR=SH VM=1 Scheduled Procedure Step Location</summary>
		DicomTags.ScheduledProcedureStepLocation = new DicomTag(0x0040, 0x0011);

		/// <summary>(0040,0012) VR=LO VM=1 Pre-Medication</summary>
		DicomTags.PreMedication = new DicomTag(0x0040, 0x0012);

		/// <summary>(0040,0020) VR=CS VM=1 Scheduled Procedure Step Status</summary>
		DicomTags.ScheduledProcedureStepStatus = new DicomTag(0x0040, 0x0020);

		/// <summary>(0040,0100) VR=SQ VM=1 Scheduled Procedure Step Sequence</summary>
		DicomTags.ScheduledProcedureStepSequence = new DicomTag(0x0040, 0x0100);

		/// <summary>(0040,0220) VR=SQ VM=1 Referenced Non-Image Composite SOP Instance Sequence</summary>
		DicomTags.ReferencedNonImageCompositeSOPInstanceSequence = new DicomTag(0x0040, 0x0220);

		/// <summary>(0040,0241) VR=AE VM=1 Performed Station AE Title</summary>
		DicomTags.PerformedStationAETitle = new DicomTag(0x0040, 0x0241);

		/// <summary>(0040,0242) VR=SH VM=1 Performed Station Name</summary>
		DicomTags.PerformedStationName = new DicomTag(0x0040, 0x0242);

		/// <summary>(0040,0243) VR=SH VM=1 Performed Location</summary>
		DicomTags.PerformedLocation = new DicomTag(0x0040, 0x0243);

		/// <summary>(0040,0244) VR=DA VM=1 Performed Procedure Step Start Date</summary>
		DicomTags.PerformedProcedureStepStartDate = new DicomTag(0x0040, 0x0244);

		/// <summary>(0040,0245) VR=TM VM=1 Performed Procedure Step Start Time</summary>
		DicomTags.PerformedProcedureStepStartTime = new DicomTag(0x0040, 0x0245);

		/// <summary>(0040,0250) VR=DA VM=1 Performed Procedure Step End Date</summary>
		DicomTags.PerformedProcedureStepEndDate = new DicomTag(0x0040, 0x0250);

		/// <summary>(0040,0251) VR=TM VM=1 Performed Procedure Step End Time</summary>
		DicomTags.PerformedProcedureStepEndTime = new DicomTag(0x0040, 0x0251);

		/// <summary>(0040,0252) VR=CS VM=1 Performed Procedure Step Status</summary>
		DicomTags.PerformedProcedureStepStatus = new DicomTag(0x0040, 0x0252);

		/// <summary>(0040,0253) VR=SH VM=1 Performed Procedure Step ID</summary>
		DicomTags.PerformedProcedureStepID = new DicomTag(0x0040, 0x0253);

		/// <summary>(0040,0254) VR=LO VM=1 Performed Procedure Step Description</summary>
		DicomTags.PerformedProcedureStepDescription = new DicomTag(0x0040, 0x0254);

		/// <summary>(0040,0255) VR=LO VM=1 Performed Procedure Type Description</summary>
		DicomTags.PerformedProcedureTypeDescription = new DicomTag(0x0040, 0x0255);

		/// <summary>(0040,0260) VR=SQ VM=1 Performed Protocol Code Sequence</summary>
		DicomTags.PerformedProtocolCodeSequence = new DicomTag(0x0040, 0x0260);

		/// <summary>(0040,0270) VR=SQ VM=1 Scheduled Step Attributes Sequence</summary>
		DicomTags.ScheduledStepAttributesSequence = new DicomTag(0x0040, 0x0270);

		/// <summary>(0040,0275) VR=SQ VM=1 Request Attributes Sequence</summary>
		DicomTags.RequestAttributesSequence = new DicomTag(0x0040, 0x0275);

		/// <summary>(0040,0280) VR=ST VM=1 Comments on the Performed Procedure Step</summary>
		DicomTags.CommentsOnThePerformedProcedureStep = new DicomTag(0x0040, 0x0280);

		/// <summary>(0040,0281) VR=SQ VM=1 Performed Procedure Step Discontinuation Reason Code Sequence</summary>
		DicomTags.PerformedProcedureStepDiscontinuationReasonCodeSequence = new DicomTag(0x0040, 0x0281);

		/// <summary>(0040,0293) VR=SQ VM=1 Quantity Sequence</summary>
		DicomTags.QuantitySequence = new DicomTag(0x0040, 0x0293);

		/// <summary>(0040,0294) VR=DS VM=1 Quantity</summary>
		DicomTags.Quantity = new DicomTag(0x0040, 0x0294);

		/// <summary>(0040,0295) VR=SQ VM=1 Measuring Units Sequence</summary>
		DicomTags.MeasuringUnitsSequence = new DicomTag(0x0040, 0x0295);

		/// <summary>(0040,0296) VR=SQ VM=1 Billing Item Sequence</summary>
		DicomTags.BillingItemSequence = new DicomTag(0x0040, 0x0296);

		/// <summary>(0040,0300) VR=US VM=1 Total Time of Fluoroscopy</summary>
		DicomTags.TotalTimeOfFluoroscopy = new DicomTag(0x0040, 0x0300);

		/// <summary>(0040,0301) VR=US VM=1 Total Number of Exposures</summary>
		DicomTags.TotalNumberOfExposures = new DicomTag(0x0040, 0x0301);

		/// <summary>(0040,0302) VR=US VM=1 Entrance Dose</summary>
		DicomTags.EntranceDose = new DicomTag(0x0040, 0x0302);

		/// <summary>(0040,0303) VR=US VM=1-2 Exposed Area</summary>
		DicomTags.ExposedArea = new DicomTag(0x0040, 0x0303);

		/// <summary>(0040,0306) VR=DS VM=1 Distance Source to Entrance</summary>
		DicomTags.DistanceSourceToEntrance = new DicomTag(0x0040, 0x0306);

		/// <summary>(0040,0307) VR=DS VM=1 Distance Source to Support (Retired)</summary>
		DicomTags.DistanceSourceToSupportRETIRED = new DicomTag(0x0040, 0x0307);

		/// <summary>(0040,030e) VR=SQ VM=1 Exposure Dose Sequence</summary>
		DicomTags.ExposureDoseSequence = new DicomTag(0x0040, 0x030e);

		/// <summary>(0040,0310) VR=ST VM=1 Comments on Radiation Dose</summary>
		DicomTags.CommentsOnRadiationDose = new DicomTag(0x0040, 0x0310);

		/// <summary>(0040,0312) VR=DS VM=1 X-Ray Output</summary>
		DicomTags.XRayOutput = new DicomTag(0x0040, 0x0312);

		/// <summary>(0040,0314) VR=DS VM=1 Half Value Layer</summary>
		DicomTags.HalfValueLayer = new DicomTag(0x0040, 0x0314);

		/// <summary>(0040,0316) VR=DS VM=1 Organ Dose</summary>
		DicomTags.OrganDose = new DicomTag(0x0040, 0x0316);

		/// <summary>(0040,0318) VR=CS VM=1 Organ Exposed</summary>
		DicomTags.OrganExposed = new DicomTag(0x0040, 0x0318);

		/// <summary>(0040,0320) VR=SQ VM=1 Billing Procedure Step Sequence</summary>
		DicomTags.BillingProcedureStepSequence = new DicomTag(0x0040, 0x0320);

		/// <summary>(0040,0321) VR=SQ VM=1 Film Consumption Sequence</summary>
		DicomTags.FilmConsumptionSequence = new DicomTag(0x0040, 0x0321);

		/// <summary>(0040,0324) VR=SQ VM=1 Billing Supplies and Devices Sequence</summary>
		DicomTags.BillingSuppliesAndDevicesSequence = new DicomTag(0x0040, 0x0324);

		/// <summary>(0040,0330) VR=SQ VM=1 Referenced Procedure Step Sequence (Retired)</summary>
		DicomTags.ReferencedProcedureStepSequenceRETIRED = new DicomTag(0x0040, 0x0330);

		/// <summary>(0040,0340) VR=SQ VM=1 Performed Series Sequence</summary>
		DicomTags.PerformedSeriesSequence = new DicomTag(0x0040, 0x0340);

		/// <summary>(0040,0400) VR=LT VM=1 Comments on the Scheduled Procedure Step</summary>
		DicomTags.CommentsOnTheScheduledProcedureStep = new DicomTag(0x0040, 0x0400);

		/// <summary>(0040,0440) VR=SQ VM=1 Protocol Context Sequence</summary>
		DicomTags.ProtocolContextSequence = new DicomTag(0x0040, 0x0440);

		/// <summary>(0040,0441) VR=SQ VM=1 Content Item Modifier Sequence</summary>
		DicomTags.ContentItemModifierSequence = new DicomTag(0x0040, 0x0441);

		/// <summary>(0040,050a) VR=LO VM=1 Specimen Accession Number</summary>
		DicomTags.SpecimenAccessionNumber = new DicomTag(0x0040, 0x050a);

		/// <summary>(0040,0550) VR=SQ VM=1 Specimen Sequence</summary>
		DicomTags.SpecimenSequence = new DicomTag(0x0040, 0x0550);

		/// <summary>(0040,0551) VR=LO VM=1 Specimen Identifier</summary>
		DicomTags.SpecimenIdentifier = new DicomTag(0x0040, 0x0551);

		/// <summary>(0040,0552) VR=SQ VM=1 Specimen Description Sequence - Trial (Retired)</summary>
		DicomTags.SpecimenDescriptionSequenceTrialRETIRED = new DicomTag(0x0040, 0x0552);

		/// <summary>(0040,0553) VR=ST VM=1 Specimen Description - Trial (Retired)</summary>
		DicomTags.SpecimenDescriptionTrialRETIRED = new DicomTag(0x0040, 0x0553);

		/// <summary>(0040,0555) VR=SQ VM=1 Acquisition Context Sequence</summary>
		DicomTags.AcquisitionContextSequence = new DicomTag(0x0040, 0x0555);

		/// <summary>(0040,0556) VR=ST VM=1 Acquisition Context Description</summary>
		DicomTags.AcquisitionContextDescription = new DicomTag(0x0040, 0x0556);

		/// <summary>(0040,059a) VR=SQ VM=1 Specimen Type Code Sequence</summary>
		DicomTags.SpecimenTypeCodeSequence = new DicomTag(0x0040, 0x059a);

		/// <summary>(0040,06fa) VR=LO VM=1 Slide Identifier</summary>
		DicomTags.SlideIdentifier = new DicomTag(0x0040, 0x06fa);

		/// <summary>(0040,071a) VR=SQ VM=1 Image Center Point Coordinates Sequence</summary>
		DicomTags.ImageCenterPointCoordinatesSequence = new DicomTag(0x0040, 0x071a);

		/// <summary>(0040,072a) VR=DS VM=1 X offset in Slide Coordinate System</summary>
		DicomTags.XOffsetInSlideCoordinateSystem = new DicomTag(0x0040, 0x072a);

		/// <summary>(0040,073a) VR=DS VM=1 Y offset in Slide Coordinate System</summary>
		DicomTags.YOffsetInSlideCoordinateSystem = new DicomTag(0x0040, 0x073a);

		/// <summary>(0040,074a) VR=DS VM=1 Z offset in Slide Coordinate System</summary>
		DicomTags.ZOffsetInSlideCoordinateSystem = new DicomTag(0x0040, 0x074a);

		/// <summary>(0040,08d8) VR=SQ VM=1 Pixel Spacing Sequence</summary>
		DicomTags.PixelSpacingSequence = new DicomTag(0x0040, 0x08d8);

		/// <summary>(0040,08da) VR=SQ VM=1 Coordinate System Axis Code Sequence</summary>
		DicomTags.CoordinateSystemAxisCodeSequence = new DicomTag(0x0040, 0x08da);

		/// <summary>(0040,08ea) VR=SQ VM=1 Measurement Units Code Sequence</summary>
		DicomTags.MeasurementUnitsCodeSequence = new DicomTag(0x0040, 0x08ea);

		/// <summary>(0040,09f8) VR=SQ VM=1 Vital Stain Code Sequence - Trial (Retired)</summary>
		DicomTags.VitalStainCodeSequenceTrialRETIRED = new DicomTag(0x0040, 0x09f8);

		/// <summary>(0040,1001) VR=SH VM=1 Requested Procedure ID</summary>
		DicomTags.RequestedProcedureID = new DicomTag(0x0040, 0x1001);

		/// <summary>(0040,1002) VR=LO VM=1 Reason for the Requested Procedure</summary>
		DicomTags.ReasonForTheRequestedProcedure = new DicomTag(0x0040, 0x1002);

		/// <summary>(0040,1003) VR=SH VM=1 Requested Procedure Priority</summary>
		DicomTags.RequestedProcedurePriority = new DicomTag(0x0040, 0x1003);

		/// <summary>(0040,1004) VR=LO VM=1 Patient Transport Arrangements</summary>
		DicomTags.PatientTransportArrangements = new DicomTag(0x0040, 0x1004);

		/// <summary>(0040,1005) VR=LO VM=1 Requested Procedure Location</summary>
		DicomTags.RequestedProcedureLocation = new DicomTag(0x0040, 0x1005);

		/// <summary>(0040,1006) VR=SH VM=1 Placer Order Number / Procedure (Retired)</summary>
		DicomTags.PlacerOrderNumberProcedureRETIRED = new DicomTag(0x0040, 0x1006);

		/// <summary>(0040,1007) VR=SH VM=1 Filler Order Number / Procedure (Retired)</summary>
		DicomTags.FillerOrderNumberProcedureRETIRED = new DicomTag(0x0040, 0x1007);

		/// <summary>(0040,1008) VR=LO VM=1 Confidentiality Code</summary>
		DicomTags.ConfidentialityCode = new DicomTag(0x0040, 0x1008);

		/// <summary>(0040,1009) VR=SH VM=1 Reporting Priority</summary>
		DicomTags.ReportingPriority = new DicomTag(0x0040, 0x1009);

		/// <summary>(0040,100a) VR=SQ VM=1 Reason for Requested Procedure Code Sequence</summary>
		DicomTags.ReasonForRequestedProcedureCodeSequence = new DicomTag(0x0040, 0x100a);

		/// <summary>(0040,1010) VR=PN VM=1-n Names of Intended Recipients of Results</summary>
		DicomTags.NamesOfIntendedRecipientsOfResults = new DicomTag(0x0040, 0x1010);

		/// <summary>(0040,1011) VR=SQ VM=1 Intended Recipients of Results Identification Sequence</summary>
		DicomTags.IntendedRecipientsOfResultsIdentificationSequence = new DicomTag(0x0040, 0x1011);

		/// <summary>(0040,1101) VR=SQ VM=1 Person Identification Code Sequence</summary>
		DicomTags.PersonIdentificationCodeSequence = new DicomTag(0x0040, 0x1101);

		/// <summary>(0040,1102) VR=ST VM=1 Person's Address</summary>
		DicomTags.PersonsAddress = new DicomTag(0x0040, 0x1102);

		/// <summary>(0040,1103) VR=LO VM=1-n Person's Telephone Numbers</summary>
		DicomTags.PersonsTelephoneNumbers = new DicomTag(0x0040, 0x1103);

		/// <summary>(0040,1400) VR=LT VM=1 Requested Procedure Comments</summary>
		DicomTags.RequestedProcedureComments = new DicomTag(0x0040, 0x1400);

		/// <summary>(0040,2001) VR=LO VM=1 Reason for the Imaging Service Request (Retired)</summary>
		DicomTags.ReasonForTheImagingServiceRequestRETIRED = new DicomTag(0x0040, 0x2001);

		/// <summary>(0040,2004) VR=DA VM=1 Issue Date of Imaging Service Request</summary>
		DicomTags.IssueDateOfImagingServiceRequest = new DicomTag(0x0040, 0x2004);

		/// <summary>(0040,2005) VR=TM VM=1 Issue Time of Imaging Service Request</summary>
		DicomTags.IssueTimeOfImagingServiceRequest = new DicomTag(0x0040, 0x2005);

		/// <summary>(0040,2006) VR=SH VM=1 Placer Order Number / Imaging Service Request (Retired) (Retired)</summary>
		DicomTags.PlacerOrderNumberImagingServiceRequestRetiredRETIRED = new DicomTag(0x0040, 0x2006);

		/// <summary>(0040,2007) VR=SH VM=1 Filler Order Number / Imaging Service Request (Retired) (Retired)</summary>
		DicomTags.FillerOrderNumberImagingServiceRequestRetiredRETIRED = new DicomTag(0x0040, 0x2007);

		/// <summary>(0040,2008) VR=PN VM=1 Order Entered By</summary>
		DicomTags.OrderEnteredBy = new DicomTag(0x0040, 0x2008);

		/// <summary>(0040,2009) VR=SH VM=1 Order Enterer's Location</summary>
		DicomTags.OrderEnterersLocation = new DicomTag(0x0040, 0x2009);

		/// <summary>(0040,2010) VR=SH VM=1 Order Callback Phone Number</summary>
		DicomTags.OrderCallbackPhoneNumber = new DicomTag(0x0040, 0x2010);

		/// <summary>(0040,2016) VR=LO VM=1 Placer Order Number / Imaging Service Request</summary>
		DicomTags.PlacerOrderNumberImagingServiceRequest = new DicomTag(0x0040, 0x2016);

		/// <summary>(0040,2017) VR=LO VM=1 Filler Order Number / Imaging Service Request</summary>
		DicomTags.FillerOrderNumberImagingServiceRequest = new DicomTag(0x0040, 0x2017);

		/// <summary>(0040,2400) VR=LT VM=1 Imaging Service Request Comments</summary>
		DicomTags.ImagingServiceRequestComments = new DicomTag(0x0040, 0x2400);

		/// <summary>(0040,3001) VR=LO VM=1 Confidentiality Constraint on Patient Data Description</summary>
		DicomTags.ConfidentialityConstraintOnPatientDataDescription = new DicomTag(0x0040, 0x3001);

		/// <summary>(0040,4001) VR=CS VM=1 General Purpose Scheduled Procedure Step Status</summary>
		DicomTags.GeneralPurposeScheduledProcedureStepStatus = new DicomTag(0x0040, 0x4001);

		/// <summary>(0040,4002) VR=CS VM=1 General Purpose Performed Procedure Step Status</summary>
		DicomTags.GeneralPurposePerformedProcedureStepStatus = new DicomTag(0x0040, 0x4002);

		/// <summary>(0040,4003) VR=CS VM=1 General Purpose Scheduled Procedure Step Priority</summary>
		DicomTags.GeneralPurposeScheduledProcedureStepPriority = new DicomTag(0x0040, 0x4003);

		/// <summary>(0040,4004) VR=SQ VM=1 Scheduled Processing Applications Code Sequence</summary>
		DicomTags.ScheduledProcessingApplicationsCodeSequence = new DicomTag(0x0040, 0x4004);

		/// <summary>(0040,4005) VR=DT VM=1 Scheduled Procedure Step Start Date and Time</summary>
		DicomTags.ScheduledProcedureStepStartDateAndTime = new DicomTag(0x0040, 0x4005);

		/// <summary>(0040,4006) VR=CS VM=1 Multiple Copies Flag</summary>
		DicomTags.MultipleCopiesFlag = new DicomTag(0x0040, 0x4006);

		/// <summary>(0040,4007) VR=SQ VM=1 Performed Processing Applications Code Sequence</summary>
		DicomTags.PerformedProcessingApplicationsCodeSequence = new DicomTag(0x0040, 0x4007);

		/// <summary>(0040,4009) VR=SQ VM=1 Human Performer Code Sequence</summary>
		DicomTags.HumanPerformerCodeSequence = new DicomTag(0x0040, 0x4009);

		/// <summary>(0040,4010) VR=DT VM=1 Scheduled Procedure Step Modification Date and Time</summary>
		DicomTags.ScheduledProcedureStepModificationDateAndTime = new DicomTag(0x0040, 0x4010);

		/// <summary>(0040,4011) VR=DT VM=1 Expected Completion Date and Time</summary>
		DicomTags.ExpectedCompletionDateAndTime = new DicomTag(0x0040, 0x4011);

		/// <summary>(0040,4015) VR=SQ VM=1 Resulting General Purpose Performed Procedure Steps Sequence</summary>
		DicomTags.ResultingGeneralPurposePerformedProcedureStepsSequence = new DicomTag(0x0040, 0x4015);

		/// <summary>(0040,4016) VR=SQ VM=1 Referenced General Purpose Scheduled Procedure Step Sequence</summary>
		DicomTags.ReferencedGeneralPurposeScheduledProcedureStepSequence = new DicomTag(0x0040, 0x4016);

		/// <summary>(0040,4018) VR=SQ VM=1 Scheduled Workitem Code Sequence</summary>
		DicomTags.ScheduledWorkitemCodeSequence = new DicomTag(0x0040, 0x4018);

		/// <summary>(0040,4019) VR=SQ VM=1 Performed Workitem Code Sequence</summary>
		DicomTags.PerformedWorkitemCodeSequence = new DicomTag(0x0040, 0x4019);

		/// <summary>(0040,4020) VR=CS VM=1 Input Availability Flag</summary>
		DicomTags.InputAvailabilityFlag = new DicomTag(0x0040, 0x4020);

		/// <summary>(0040,4021) VR=SQ VM=1 Input Information Sequence</summary>
		DicomTags.InputInformationSequence = new DicomTag(0x0040, 0x4021);

		/// <summary>(0040,4022) VR=SQ VM=1 Relevant Information Sequence</summary>
		DicomTags.RelevantInformationSequence = new DicomTag(0x0040, 0x4022);

		/// <summary>(0040,4023) VR=UI VM=1 Referenced General Purpose Scheduled Procedure Step Transaction UID</summary>
		DicomTags.ReferencedGeneralPurposeScheduledProcedureStepTransactionUID = new DicomTag(0x0040, 0x4023);

		/// <summary>(0040,4025) VR=SQ VM=1 Scheduled Station Name Code Sequence</summary>
		DicomTags.ScheduledStationNameCodeSequence = new DicomTag(0x0040, 0x4025);

		/// <summary>(0040,4026) VR=SQ VM=1 Scheduled Station Class Code Sequence</summary>
		DicomTags.ScheduledStationClassCodeSequence = new DicomTag(0x0040, 0x4026);

		/// <summary>(0040,4027) VR=SQ VM=1 Scheduled Station Geographic Location Code Sequence</summary>
		DicomTags.ScheduledStationGeographicLocationCodeSequence = new DicomTag(0x0040, 0x4027);

		/// <summary>(0040,4028) VR=SQ VM=1 Performed Station Name Code Sequence</summary>
		DicomTags.PerformedStationNameCodeSequence = new DicomTag(0x0040, 0x4028);

		/// <summary>(0040,4029) VR=SQ VM=1 Performed Station Class Code Sequence</summary>
		DicomTags.PerformedStationClassCodeSequence = new DicomTag(0x0040, 0x4029);

		/// <summary>(0040,4030) VR=SQ VM=1 Performed Station Geographic Location Code Sequence</summary>
		DicomTags.PerformedStationGeographicLocationCodeSequence = new DicomTag(0x0040, 0x4030);

		/// <summary>(0040,4031) VR=SQ VM=1 Requested Subsequent Workitem Code Sequence</summary>
		DicomTags.RequestedSubsequentWorkitemCodeSequence = new DicomTag(0x0040, 0x4031);

		/// <summary>(0040,4032) VR=SQ VM=1 Non-DICOM Output Code Sequence</summary>
		DicomTags.NonDICOMOutputCodeSequence = new DicomTag(0x0040, 0x4032);

		/// <summary>(0040,4033) VR=SQ VM=1 Output Information Sequence</summary>
		DicomTags.OutputInformationSequence = new DicomTag(0x0040, 0x4033);

		/// <summary>(0040,4034) VR=SQ VM=1 Scheduled Human Performers Sequence</summary>
		DicomTags.ScheduledHumanPerformersSequence = new DicomTag(0x0040, 0x4034);

		/// <summary>(0040,4035) VR=SQ VM=1 Actual Human Performers Sequence</summary>
		DicomTags.ActualHumanPerformersSequence = new DicomTag(0x0040, 0x4035);

		/// <summary>(0040,4036) VR=LO VM=1 Human Performer's Organization</summary>
		DicomTags.HumanPerformersOrganization = new DicomTag(0x0040, 0x4036);

		/// <summary>(0040,4037) VR=PN VM=1 Human Performer's Name</summary>
		DicomTags.HumanPerformersName = new DicomTag(0x0040, 0x4037);

		/// <summary>(0040,8302) VR=DS VM=1 Entrance Dose in mGy</summary>
		DicomTags.EntranceDoseInMGy = new DicomTag(0x0040, 0x8302);

		/// <summary>(0040,9094) VR=SQ VM=1 Referenced Image Real World Value Mapping Sequence</summary>
		DicomTags.ReferencedImageRealWorldValueMappingSequence = new DicomTag(0x0040, 0x9094);

		/// <summary>(0040,9096) VR=SQ VM=1 Real World Value Mapping Sequence</summary>
		DicomTags.RealWorldValueMappingSequence = new DicomTag(0x0040, 0x9096);

		/// <summary>(0040,9098) VR=SQ VM=1 Pixel Value Mapping Code Sequence</summary>
		DicomTags.PixelValueMappingCodeSequence = new DicomTag(0x0040, 0x9098);

		/// <summary>(0040,9210) VR=SH VM=1 LUT Label</summary>
		DicomTags.LUTLabel = new DicomTag(0x0040, 0x9210);

		/// <summary>(0040,9211) VR=US/SS VM=1 Real World Value Last Value Mapped</summary>
		DicomTags.RealWorldValueLastValueMapped = new DicomTag(0x0040, 0x9211);

		/// <summary>(0040,9212) VR=FD VM=1-n Real World Value LUT Data</summary>
		DicomTags.RealWorldValueLUTData = new DicomTag(0x0040, 0x9212);

		/// <summary>(0040,9216) VR=US/SS VM=1 Real World Value First Value Mapped</summary>
		DicomTags.RealWorldValueFirstValueMapped = new DicomTag(0x0040, 0x9216);

		/// <summary>(0040,9224) VR=FD VM=1 Real World Value Intercept</summary>
		DicomTags.RealWorldValueIntercept = new DicomTag(0x0040, 0x9224);

		/// <summary>(0040,9225) VR=FD VM=1 Real World Value Slope</summary>
		DicomTags.RealWorldValueSlope = new DicomTag(0x0040, 0x9225);

		/// <summary>(0040,a010) VR=CS VM=1 Relationship Type</summary>
		DicomTags.RelationshipType = new DicomTag(0x0040, 0xa010);

		/// <summary>(0040,a027) VR=LO VM=1 Verifying Organization</summary>
		DicomTags.VerifyingOrganization = new DicomTag(0x0040, 0xa027);

		/// <summary>(0040,a030) VR=DT VM=1 Verification Date Time</summary>
		DicomTags.VerificationDateTime = new DicomTag(0x0040, 0xa030);

		/// <summary>(0040,a032) VR=DT VM=1 Observation Date Time</summary>
		DicomTags.ObservationDateTime = new DicomTag(0x0040, 0xa032);

		/// <summary>(0040,a040) VR=CS VM=1 Value Type</summary>
		DicomTags.ValueType = new DicomTag(0x0040, 0xa040);

		/// <summary>(0040,a043) VR=SQ VM=1 Concept Name Code Sequence</summary>
		DicomTags.ConceptNameCodeSequence = new DicomTag(0x0040, 0xa043);

		/// <summary>(0040,a050) VR=CS VM=1 Continuity Of Content</summary>
		DicomTags.ContinuityOfContent = new DicomTag(0x0040, 0xa050);

		/// <summary>(0040,a073) VR=SQ VM=1 Verifying Observer Sequence</summary>
		DicomTags.VerifyingObserverSequence = new DicomTag(0x0040, 0xa073);

		/// <summary>(0040,a075) VR=PN VM=1 Verifying Observer Name</summary>
		DicomTags.VerifyingObserverName = new DicomTag(0x0040, 0xa075);

		/// <summary>(0040,a078) VR=SQ VM=1 Author Observer Sequence</summary>
		DicomTags.AuthorObserverSequence = new DicomTag(0x0040, 0xa078);

		/// <summary>(0040,a07a) VR=SQ VM=1 Participant Sequence</summary>
		DicomTags.ParticipantSequence = new DicomTag(0x0040, 0xa07a);

		/// <summary>(0040,a07c) VR=SQ VM=1 Custodial Organization Sequence</summary>
		DicomTags.CustodialOrganizationSequence = new DicomTag(0x0040, 0xa07c);

		/// <summary>(0040,a080) VR=CS VM=1 Participation Type</summary>
		DicomTags.ParticipationType = new DicomTag(0x0040, 0xa080);

		/// <summary>(0040,a082) VR=DT VM=1 Participation DateTime</summary>
		DicomTags.ParticipationDateTime = new DicomTag(0x0040, 0xa082);

		/// <summary>(0040,a084) VR=CS VM=1 Observer Type</summary>
		DicomTags.ObserverType = new DicomTag(0x0040, 0xa084);

		/// <summary>(0040,a088) VR=SQ VM=1 Verifying Observer Identification Code Sequence</summary>
		DicomTags.VerifyingObserverIdentificationCodeSequence = new DicomTag(0x0040, 0xa088);

		/// <summary>(0040,a090) VR=SQ VM=1 Equivalent CDA Document Sequence (Retired)</summary>
		DicomTags.EquivalentCDADocumentSequenceRETIRED = new DicomTag(0x0040, 0xa090);

		/// <summary>(0040,a0b0) VR=US VM=2-2n Referenced Waveform Channels</summary>
		DicomTags.ReferencedWaveformChannels = new DicomTag(0x0040, 0xa0b0);

		/// <summary>(0040,a120) VR=DT VM=1 DateTime</summary>
		DicomTags.DateTime = new DicomTag(0x0040, 0xa120);

		/// <summary>(0040,a121) VR=DA VM=1 Date</summary>
		DicomTags.Date = new DicomTag(0x0040, 0xa121);

		/// <summary>(0040,a122) VR=TM VM=1 Time</summary>
		DicomTags.Time = new DicomTag(0x0040, 0xa122);

		/// <summary>(0040,a123) VR=PN VM=1 Person Name</summary>
		DicomTags.PersonName = new DicomTag(0x0040, 0xa123);

		/// <summary>(0040,a124) VR=UI VM=1 UID</summary>
		DicomTags.UID = new DicomTag(0x0040, 0xa124);

		/// <summary>(0040,a130) VR=CS VM=1 Temporal Range Type</summary>
		DicomTags.TemporalRangeType = new DicomTag(0x0040, 0xa130);

		/// <summary>(0040,a132) VR=UL VM=1-n Referenced Sample Positions</summary>
		DicomTags.ReferencedSamplePositions = new DicomTag(0x0040, 0xa132);

		/// <summary>(0040,a136) VR=US VM=1-n Referenced Frame Numbers</summary>
		DicomTags.ReferencedFrameNumbers = new DicomTag(0x0040, 0xa136);

		/// <summary>(0040,a138) VR=DS VM=1-n Referenced Time Offsets</summary>
		DicomTags.ReferencedTimeOffsets = new DicomTag(0x0040, 0xa138);

		/// <summary>(0040,a13a) VR=DT VM=1-n Referenced DateTime</summary>
		DicomTags.ReferencedDateTime = new DicomTag(0x0040, 0xa13a);

		/// <summary>(0040,a160) VR=UT VM=1 Text Value</summary>
		DicomTags.TextValue = new DicomTag(0x0040, 0xa160);

		/// <summary>(0040,a168) VR=SQ VM=1 Concept Code Sequence</summary>
		DicomTags.ConceptCodeSequence = new DicomTag(0x0040, 0xa168);

		/// <summary>(0040,a170) VR=SQ VM=1 Purpose of Reference Code Sequence</summary>
		DicomTags.PurposeOfReferenceCodeSequence = new DicomTag(0x0040, 0xa170);

		/// <summary>(0040,a180) VR=US VM=1 Annotation Group Number</summary>
		DicomTags.AnnotationGroupNumber = new DicomTag(0x0040, 0xa180);

		/// <summary>(0040,a195) VR=SQ VM=1 Modifier Code Sequence</summary>
		DicomTags.ModifierCodeSequence = new DicomTag(0x0040, 0xa195);

		/// <summary>(0040,a300) VR=SQ VM=1 Measured Value Sequence</summary>
		DicomTags.MeasuredValueSequence = new DicomTag(0x0040, 0xa300);

		/// <summary>(0040,a301) VR=SQ VM=1 Numeric Value Qualifier Code Sequence</summary>
		DicomTags.NumericValueQualifierCodeSequence = new DicomTag(0x0040, 0xa301);

		/// <summary>(0040,a30a) VR=DS VM=1-n Numeric Value</summary>
		DicomTags.NumericValue = new DicomTag(0x0040, 0xa30a);

		/// <summary>(0040,a353) VR=ST VM=1 Address - Trial (Retired)</summary>
		DicomTags.AddressTrialRETIRED = new DicomTag(0x0040, 0xa353);

		/// <summary>(0040,a354) VR=LO VM=1 Telephone Number - Trial (Retired)</summary>
		DicomTags.TelephoneNumberTrialRETIRED = new DicomTag(0x0040, 0xa354);

		/// <summary>(0040,a360) VR=SQ VM=1 Predecessor Documents Sequence</summary>
		DicomTags.PredecessorDocumentsSequence = new DicomTag(0x0040, 0xa360);

		/// <summary>(0040,a370) VR=SQ VM=1 Referenced Request Sequence</summary>
		DicomTags.ReferencedRequestSequence = new DicomTag(0x0040, 0xa370);

		/// <summary>(0040,a372) VR=SQ VM=1 Performed Procedure Code Sequence</summary>
		DicomTags.PerformedProcedureCodeSequence = new DicomTag(0x0040, 0xa372);

		/// <summary>(0040,a375) VR=SQ VM=1 Current Requested Procedure Evidence Sequence</summary>
		DicomTags.CurrentRequestedProcedureEvidenceSequence = new DicomTag(0x0040, 0xa375);

		/// <summary>(0040,a385) VR=SQ VM=1 Pertinent Other Evidence Sequence</summary>
		DicomTags.PertinentOtherEvidenceSequence = new DicomTag(0x0040, 0xa385);

		/// <summary>(0040,a390) VR=SQ VM=1 HL7 Structured Document Reference Sequence</summary>
		DicomTags.HL7StructuredDocumentReferenceSequence = new DicomTag(0x0040, 0xa390);

		/// <summary>(0040,a491) VR=CS VM=1 Completion Flag</summary>
		DicomTags.CompletionFlag = new DicomTag(0x0040, 0xa491);

		/// <summary>(0040,a492) VR=LO VM=1 Completion Flag Description</summary>
		DicomTags.CompletionFlagDescription = new DicomTag(0x0040, 0xa492);

		/// <summary>(0040,a493) VR=CS VM=1 Verification Flag</summary>
		DicomTags.VerificationFlag = new DicomTag(0x0040, 0xa493);

		/// <summary>(0040,a494) VR=CS VM=1 Archive Requested</summary>
		DicomTags.ArchiveRequested = new DicomTag(0x0040, 0xa494);

		/// <summary>(0040,a504) VR=SQ VM=1 Content Template Sequence</summary>
		DicomTags.ContentTemplateSequence = new DicomTag(0x0040, 0xa504);

		/// <summary>(0040,a525) VR=SQ VM=1 Identical Documents Sequence</summary>
		DicomTags.IdenticalDocumentsSequence = new DicomTag(0x0040, 0xa525);

		/// <summary>(0040,a730) VR=SQ VM=1 Content Sequence</summary>
		DicomTags.ContentSequence = new DicomTag(0x0040, 0xa730);

		/// <summary>(0040,b020) VR=SQ VM=1 Annotation Sequence</summary>
		DicomTags.AnnotationSequence = new DicomTag(0x0040, 0xb020);

		/// <summary>(0040,db00) VR=CS VM=1 Template Identifier</summary>
		DicomTags.TemplateIdentifier = new DicomTag(0x0040, 0xdb00);

		/// <summary>(0040,db06) VR=DT VM=1 Template Version (Retired)</summary>
		DicomTags.TemplateVersionRETIRED = new DicomTag(0x0040, 0xdb06);

		/// <summary>(0040,db07) VR=DT VM=1 Template Local Version (Retired)</summary>
		DicomTags.TemplateLocalVersionRETIRED = new DicomTag(0x0040, 0xdb07);

		/// <summary>(0040,db0b) VR=CS VM=1 Template Extension Flag (Retired)</summary>
		DicomTags.TemplateExtensionFlagRETIRED = new DicomTag(0x0040, 0xdb0b);

		/// <summary>(0040,db0c) VR=UI VM=1 Template Extension Organization UID (Retired)</summary>
		DicomTags.TemplateExtensionOrganizationUIDRETIRED = new DicomTag(0x0040, 0xdb0c);

		/// <summary>(0040,db0d) VR=UI VM=1 Template Extension Creator UID (Retired)</summary>
		DicomTags.TemplateExtensionCreatorUIDRETIRED = new DicomTag(0x0040, 0xdb0d);

		/// <summary>(0040,db73) VR=UL VM=1-n Referenced Content Item Identifier</summary>
		DicomTags.ReferencedContentItemIdentifier = new DicomTag(0x0040, 0xdb73);

		/// <summary>(0040,e001) VR=ST VM=1 HL7 Instance Identifier</summary>
		DicomTags.HL7InstanceIdentifier = new DicomTag(0x0040, 0xe001);

		/// <summary>(0040,e004) VR=DT VM=1 HL7 Document Effective Time</summary>
		DicomTags.HL7DocumentEffectiveTime = new DicomTag(0x0040, 0xe004);

		/// <summary>(0040,e006) VR=SQ VM=1 HL7 Document Type Code Sequence</summary>
		DicomTags.HL7DocumentTypeCodeSequence = new DicomTag(0x0040, 0xe006);

		/// <summary>(0040,e010) VR=UT VM=1 Retrieve URI</summary>
		DicomTags.RetrieveURI = new DicomTag(0x0040, 0xe010);

		/// <summary>(0042,0010) VR=ST VM=1 Document Title</summary>
		DicomTags.DocumentTitle = new DicomTag(0x0042, 0x0010);

		/// <summary>(0042,0011) VR=OB VM=1 Encapsulated Document</summary>
		DicomTags.EncapsulatedDocument = new DicomTag(0x0042, 0x0011);

		/// <summary>(0042,0012) VR=LO VM=1 MIME Type of Encapsulated Document</summary>
		DicomTags.MIMETypeOfEncapsulatedDocument = new DicomTag(0x0042, 0x0012);

		/// <summary>(0042,0013) VR=SQ VM=1 Source Instance Sequence</summary>
		DicomTags.SourceInstanceSequence = new DicomTag(0x0042, 0x0013);

		/// <summary>(0042,0014) VR=LO VM=1-n List of MIME Types</summary>
		DicomTags.ListOfMIMETypes = new DicomTag(0x0042, 0x0014);

		/// <summary>(0044,0001) VR=ST VM=1 Product Package Identifier</summary>
		DicomTags.ProductPackageIdentifier = new DicomTag(0x0044, 0x0001);

		/// <summary>(0044,0002) VR=CS VM=1 Substance Administration Approval</summary>
		DicomTags.SubstanceAdministrationApproval = new DicomTag(0x0044, 0x0002);

		/// <summary>(0044,0003) VR=LT VM=1 Approval Status Further Description</summary>
		DicomTags.ApprovalStatusFurtherDescription = new DicomTag(0x0044, 0x0003);

		/// <summary>(0044,0004) VR=DT VM=1 Approval Status DateTime</summary>
		DicomTags.ApprovalStatusDateTime = new DicomTag(0x0044, 0x0004);

		/// <summary>(0044,0007) VR=SQ VM=1 Product Type Code Sequence</summary>
		DicomTags.ProductTypeCodeSequence = new DicomTag(0x0044, 0x0007);

		/// <summary>(0044,0008) VR=LO VM=1-n Product Name</summary>
		DicomTags.ProductName = new DicomTag(0x0044, 0x0008);

		/// <summary>(0044,0009) VR=LT VM=1 Product Description</summary>
		DicomTags.ProductDescription = new DicomTag(0x0044, 0x0009);

		/// <summary>(0044,000a) VR=LO VM=1 Product Lot Identifier</summary>
		DicomTags.ProductLotIdentifier = new DicomTag(0x0044, 0x000a);

		/// <summary>(0044,000b) VR=DT VM=1 Product Expiration DateTime</summary>
		DicomTags.ProductExpirationDateTime = new DicomTag(0x0044, 0x000b);

		/// <summary>(0044,0010) VR=DT VM=1 Substance Administration DateTime</summary>
		DicomTags.SubstanceAdministrationDateTime = new DicomTag(0x0044, 0x0010);

		/// <summary>(0044,0011) VR=LO VM=1 Substance Administration Notes</summary>
		DicomTags.SubstanceAdministrationNotes = new DicomTag(0x0044, 0x0011);

		/// <summary>(0044,0012) VR=LO VM=1 Substance Administration Device ID</summary>
		DicomTags.SubstanceAdministrationDeviceID = new DicomTag(0x0044, 0x0012);

		/// <summary>(0044,0013) VR=SQ VM=1 Product Parameter Sequence</summary>
		DicomTags.ProductParameterSequence = new DicomTag(0x0044, 0x0013);

		/// <summary>(0044,0019) VR=SQ VM=1 Substance Administration Parameter Sequence</summary>
		DicomTags.SubstanceAdministrationParameterSequence = new DicomTag(0x0044, 0x0019);

		/// <summary>(0050,0004) VR=CS VM=1 Calibration Image</summary>
		DicomTags.CalibrationImage = new DicomTag(0x0050, 0x0004);

		/// <summary>(0050,0010) VR=SQ VM=1 Device Sequence</summary>
		DicomTags.DeviceSequence = new DicomTag(0x0050, 0x0010);

		/// <summary>(0050,0014) VR=DS VM=1 Device Length</summary>
		DicomTags.DeviceLength = new DicomTag(0x0050, 0x0014);

		/// <summary>(0050,0016) VR=DS VM=1 Device Diameter</summary>
		DicomTags.DeviceDiameter = new DicomTag(0x0050, 0x0016);

		/// <summary>(0050,0017) VR=CS VM=1 Device Diameter Units</summary>
		DicomTags.DeviceDiameterUnits = new DicomTag(0x0050, 0x0017);

		/// <summary>(0050,0018) VR=DS VM=1 Device Volume</summary>
		DicomTags.DeviceVolume = new DicomTag(0x0050, 0x0018);

		/// <summary>(0050,0019) VR=DS VM=1 Intermarker Distance</summary>
		DicomTags.IntermarkerDistance = new DicomTag(0x0050, 0x0019);

		/// <summary>(0050,0020) VR=LO VM=1 Device Description</summary>
		DicomTags.DeviceDescription = new DicomTag(0x0050, 0x0020);

		/// <summary>(0054,0010) VR=US VM=1-n Energy Window Vector</summary>
		DicomTags.EnergyWindowVector = new DicomTag(0x0054, 0x0010);

		/// <summary>(0054,0011) VR=US VM=1 Number of Energy Windows</summary>
		DicomTags.NumberOfEnergyWindows = new DicomTag(0x0054, 0x0011);

		/// <summary>(0054,0012) VR=SQ VM=1 Energy Window Information Sequence</summary>
		DicomTags.EnergyWindowInformationSequence = new DicomTag(0x0054, 0x0012);

		/// <summary>(0054,0013) VR=SQ VM=1 Energy Window Range Sequence</summary>
		DicomTags.EnergyWindowRangeSequence = new DicomTag(0x0054, 0x0013);

		/// <summary>(0054,0014) VR=DS VM=1 Energy Window Lower Limit</summary>
		DicomTags.EnergyWindowLowerLimit = new DicomTag(0x0054, 0x0014);

		/// <summary>(0054,0015) VR=DS VM=1 Energy Window Upper Limit</summary>
		DicomTags.EnergyWindowUpperLimit = new DicomTag(0x0054, 0x0015);

		/// <summary>(0054,0016) VR=SQ VM=1 Radiopharmaceutical Information Sequence</summary>
		DicomTags.RadiopharmaceuticalInformationSequence = new DicomTag(0x0054, 0x0016);

		/// <summary>(0054,0017) VR=IS VM=1 Residual Syringe Counts</summary>
		DicomTags.ResidualSyringeCounts = new DicomTag(0x0054, 0x0017);

		/// <summary>(0054,0018) VR=SH VM=1 Energy Window Name</summary>
		DicomTags.EnergyWindowName = new DicomTag(0x0054, 0x0018);

		/// <summary>(0054,0020) VR=US VM=1-n Detector Vector</summary>
		DicomTags.DetectorVector = new DicomTag(0x0054, 0x0020);

		/// <summary>(0054,0021) VR=US VM=1 Number of Detectors</summary>
		DicomTags.NumberOfDetectors = new DicomTag(0x0054, 0x0021);

		/// <summary>(0054,0022) VR=SQ VM=1 Detector Information Sequence</summary>
		DicomTags.DetectorInformationSequence = new DicomTag(0x0054, 0x0022);

		/// <summary>(0054,0030) VR=US VM=1-n Phase Vector</summary>
		DicomTags.PhaseVector = new DicomTag(0x0054, 0x0030);

		/// <summary>(0054,0031) VR=US VM=1 Number of Phases</summary>
		DicomTags.NumberOfPhases = new DicomTag(0x0054, 0x0031);

		/// <summary>(0054,0032) VR=SQ VM=1 Phase Information Sequence</summary>
		DicomTags.PhaseInformationSequence = new DicomTag(0x0054, 0x0032);

		/// <summary>(0054,0033) VR=US VM=1 Number of Frames in Phase</summary>
		DicomTags.NumberOfFramesInPhase = new DicomTag(0x0054, 0x0033);

		/// <summary>(0054,0036) VR=IS VM=1 Phase Delay</summary>
		DicomTags.PhaseDelay = new DicomTag(0x0054, 0x0036);

		/// <summary>(0054,0038) VR=IS VM=1 Pause Between Frames</summary>
		DicomTags.PauseBetweenFrames = new DicomTag(0x0054, 0x0038);

		/// <summary>(0054,0039) VR=CS VM=1 Phase Description</summary>
		DicomTags.PhaseDescription = new DicomTag(0x0054, 0x0039);

		/// <summary>(0054,0050) VR=US VM=1-n Rotation Vector</summary>
		DicomTags.RotationVector = new DicomTag(0x0054, 0x0050);

		/// <summary>(0054,0051) VR=US VM=1 Number of Rotations</summary>
		DicomTags.NumberOfRotations = new DicomTag(0x0054, 0x0051);

		/// <summary>(0054,0052) VR=SQ VM=1 Rotation Information Sequence</summary>
		DicomTags.RotationInformationSequence = new DicomTag(0x0054, 0x0052);

		/// <summary>(0054,0053) VR=US VM=1 Number of Frames in Rotation</summary>
		DicomTags.NumberOfFramesInRotation = new DicomTag(0x0054, 0x0053);

		/// <summary>(0054,0060) VR=US VM=1-n R-R Interval Vector</summary>
		DicomTags.RRIntervalVector = new DicomTag(0x0054, 0x0060);

		/// <summary>(0054,0061) VR=US VM=1 Number of R-R Intervals</summary>
		DicomTags.NumberOfRRIntervals = new DicomTag(0x0054, 0x0061);

		/// <summary>(0054,0062) VR=SQ VM=1 Gated Information Sequence</summary>
		DicomTags.GatedInformationSequence = new DicomTag(0x0054, 0x0062);

		/// <summary>(0054,0063) VR=SQ VM=1 Data Information Sequence</summary>
		DicomTags.DataInformationSequence = new DicomTag(0x0054, 0x0063);

		/// <summary>(0054,0070) VR=US VM=1-n Time Slot Vector</summary>
		DicomTags.TimeSlotVector = new DicomTag(0x0054, 0x0070);

		/// <summary>(0054,0071) VR=US VM=1 Number of Time Slots</summary>
		DicomTags.NumberOfTimeSlots = new DicomTag(0x0054, 0x0071);

		/// <summary>(0054,0072) VR=SQ VM=1 Time Slot Information Sequence</summary>
		DicomTags.TimeSlotInformationSequence = new DicomTag(0x0054, 0x0072);

		/// <summary>(0054,0073) VR=DS VM=1 Time Slot Time</summary>
		DicomTags.TimeSlotTime = new DicomTag(0x0054, 0x0073);

		/// <summary>(0054,0080) VR=US VM=1-n Slice Vector</summary>
		DicomTags.SliceVector = new DicomTag(0x0054, 0x0080);

		/// <summary>(0054,0081) VR=US VM=1 Number of Slices</summary>
		DicomTags.NumberOfSlices = new DicomTag(0x0054, 0x0081);

		/// <summary>(0054,0090) VR=US VM=1-n Angular View Vector</summary>
		DicomTags.AngularViewVector = new DicomTag(0x0054, 0x0090);

		/// <summary>(0054,0100) VR=US VM=1-n Time Slice Vector</summary>
		DicomTags.TimeSliceVector = new DicomTag(0x0054, 0x0100);

		/// <summary>(0054,0101) VR=US VM=1 Number of Time Slices</summary>
		DicomTags.NumberOfTimeSlices = new DicomTag(0x0054, 0x0101);

		/// <summary>(0054,0200) VR=DS VM=1 Start Angle</summary>
		DicomTags.StartAngle = new DicomTag(0x0054, 0x0200);

		/// <summary>(0054,0202) VR=CS VM=1 Type of Detector Motion</summary>
		DicomTags.TypeOfDetectorMotion = new DicomTag(0x0054, 0x0202);

		/// <summary>(0054,0210) VR=IS VM=1-n Trigger Vector</summary>
		DicomTags.TriggerVector = new DicomTag(0x0054, 0x0210);

		/// <summary>(0054,0211) VR=US VM=1 Number of Triggers in Phase</summary>
		DicomTags.NumberOfTriggersInPhase = new DicomTag(0x0054, 0x0211);

		/// <summary>(0054,0220) VR=SQ VM=1 View Code Sequence</summary>
		DicomTags.ViewCodeSequence = new DicomTag(0x0054, 0x0220);

		/// <summary>(0054,0222) VR=SQ VM=1 View Modifier Code Sequence</summary>
		DicomTags.ViewModifierCodeSequence = new DicomTag(0x0054, 0x0222);

		/// <summary>(0054,0300) VR=SQ VM=1 Radionuclide Code Sequence</summary>
		DicomTags.RadionuclideCodeSequence = new DicomTag(0x0054, 0x0300);

		/// <summary>(0054,0302) VR=SQ VM=1 Administration Route Code Sequence</summary>
		DicomTags.AdministrationRouteCodeSequence = new DicomTag(0x0054, 0x0302);

		/// <summary>(0054,0304) VR=SQ VM=1 Radiopharmaceutical Code Sequence</summary>
		DicomTags.RadiopharmaceuticalCodeSequence = new DicomTag(0x0054, 0x0304);

		/// <summary>(0054,0306) VR=SQ VM=1 Calibration Data Sequence</summary>
		DicomTags.CalibrationDataSequence = new DicomTag(0x0054, 0x0306);

		/// <summary>(0054,0308) VR=US VM=1 Energy Window Number</summary>
		DicomTags.EnergyWindowNumber = new DicomTag(0x0054, 0x0308);

		/// <summary>(0054,0400) VR=SH VM=1 Image ID</summary>
		DicomTags.ImageID = new DicomTag(0x0054, 0x0400);

		/// <summary>(0054,0410) VR=SQ VM=1 Patient Orientation Code Sequence</summary>
		DicomTags.PatientOrientationCodeSequence = new DicomTag(0x0054, 0x0410);

		/// <summary>(0054,0412) VR=SQ VM=1 Patient Orientation Modifier Code Sequence</summary>
		DicomTags.PatientOrientationModifierCodeSequence = new DicomTag(0x0054, 0x0412);

		/// <summary>(0054,0414) VR=SQ VM=1 Patient Gantry Relationship Code Sequence</summary>
		DicomTags.PatientGantryRelationshipCodeSequence = new DicomTag(0x0054, 0x0414);

		/// <summary>(0054,0500) VR=CS VM=1 Slice Progression Direction</summary>
		DicomTags.SliceProgressionDirection = new DicomTag(0x0054, 0x0500);

		/// <summary>(0054,1000) VR=CS VM=2 Series Type</summary>
		DicomTags.SeriesType = new DicomTag(0x0054, 0x1000);

		/// <summary>(0054,1001) VR=CS VM=1 Units</summary>
		DicomTags.Units = new DicomTag(0x0054, 0x1001);

		/// <summary>(0054,1002) VR=CS VM=1 Counts Source</summary>
		DicomTags.CountsSource = new DicomTag(0x0054, 0x1002);

		/// <summary>(0054,1004) VR=CS VM=1 Reprojection Method</summary>
		DicomTags.ReprojectionMethod = new DicomTag(0x0054, 0x1004);

		/// <summary>(0054,1100) VR=CS VM=1 Randoms Correction Method</summary>
		DicomTags.RandomsCorrectionMethod = new DicomTag(0x0054, 0x1100);

		/// <summary>(0054,1101) VR=LO VM=1 Attenuation Correction Method</summary>
		DicomTags.AttenuationCorrectionMethod = new DicomTag(0x0054, 0x1101);

		/// <summary>(0054,1102) VR=CS VM=1 Decay Correction</summary>
		DicomTags.DecayCorrection = new DicomTag(0x0054, 0x1102);

		/// <summary>(0054,1103) VR=LO VM=1 Reconstruction Method</summary>
		DicomTags.ReconstructionMethod = new DicomTag(0x0054, 0x1103);

		/// <summary>(0054,1104) VR=LO VM=1 Detector Lines of Response Used</summary>
		DicomTags.DetectorLinesOfResponseUsed = new DicomTag(0x0054, 0x1104);

		/// <summary>(0054,1105) VR=LO VM=1 Scatter Correction Method</summary>
		DicomTags.ScatterCorrectionMethod = new DicomTag(0x0054, 0x1105);

		/// <summary>(0054,1200) VR=DS VM=1 Axial Acceptance</summary>
		DicomTags.AxialAcceptance = new DicomTag(0x0054, 0x1200);

		/// <summary>(0054,1201) VR=IS VM=2 Axial Mash</summary>
		DicomTags.AxialMash = new DicomTag(0x0054, 0x1201);

		/// <summary>(0054,1202) VR=IS VM=1 Transverse Mash</summary>
		DicomTags.TransverseMash = new DicomTag(0x0054, 0x1202);

		/// <summary>(0054,1203) VR=DS VM=2 Detector Element Size</summary>
		DicomTags.DetectorElementSize = new DicomTag(0x0054, 0x1203);

		/// <summary>(0054,1210) VR=DS VM=1 Coincidence Window Width</summary>
		DicomTags.CoincidenceWindowWidth = new DicomTag(0x0054, 0x1210);

		/// <summary>(0054,1220) VR=CS VM=1-n Secondary Counts Type</summary>
		DicomTags.SecondaryCountsType = new DicomTag(0x0054, 0x1220);

		/// <summary>(0054,1300) VR=DS VM=1 Frame Reference Time</summary>
		DicomTags.FrameReferenceTime = new DicomTag(0x0054, 0x1300);

		/// <summary>(0054,1310) VR=IS VM=1 Primary (Prompts) Counts Accumulated</summary>
		DicomTags.PrimaryPromptsCountsAccumulated = new DicomTag(0x0054, 0x1310);

		/// <summary>(0054,1311) VR=IS VM=1-n Secondary Counts Accumulated</summary>
		DicomTags.SecondaryCountsAccumulated = new DicomTag(0x0054, 0x1311);

		/// <summary>(0054,1320) VR=DS VM=1 Slice Sensitivity Factor</summary>
		DicomTags.SliceSensitivityFactor = new DicomTag(0x0054, 0x1320);

		/// <summary>(0054,1321) VR=DS VM=1 Decay Factor</summary>
		DicomTags.DecayFactor = new DicomTag(0x0054, 0x1321);

		/// <summary>(0054,1322) VR=DS VM=1 Dose Calibration Factor</summary>
		DicomTags.DoseCalibrationFactor = new DicomTag(0x0054, 0x1322);

		/// <summary>(0054,1323) VR=DS VM=1 Scatter Fraction Factor</summary>
		DicomTags.ScatterFractionFactor = new DicomTag(0x0054, 0x1323);

		/// <summary>(0054,1324) VR=DS VM=1 Dead Time Factor</summary>
		DicomTags.DeadTimeFactor = new DicomTag(0x0054, 0x1324);

		/// <summary>(0054,1330) VR=US VM=1 Image Index</summary>
		DicomTags.ImageIndex = new DicomTag(0x0054, 0x1330);

		/// <summary>(0054,1400) VR=CS VM=1-n Counts Included (Retired)</summary>
		DicomTags.CountsIncludedRETIRED = new DicomTag(0x0054, 0x1400);

		/// <summary>(0054,1401) VR=CS VM=1 Dead Time Correction Flag (Retired)</summary>
		DicomTags.DeadTimeCorrectionFlagRETIRED = new DicomTag(0x0054, 0x1401);

		/// <summary>(0060,3000) VR=SQ VM=1 Histogram Sequence</summary>
		DicomTags.HistogramSequence = new DicomTag(0x0060, 0x3000);

		/// <summary>(0060,3002) VR=US VM=1 Histogram Number of Bins</summary>
		DicomTags.HistogramNumberOfBins = new DicomTag(0x0060, 0x3002);

		/// <summary>(0060,3004) VR=US/SS VM=1 Histogram First Bin Value</summary>
		DicomTags.HistogramFirstBinValue = new DicomTag(0x0060, 0x3004);

		/// <summary>(0060,3006) VR=US/SS VM=1 Histogram Last Bin Value</summary>
		DicomTags.HistogramLastBinValue = new DicomTag(0x0060, 0x3006);

		/// <summary>(0060,3008) VR=US VM=1 Histogram Bin Width</summary>
		DicomTags.HistogramBinWidth = new DicomTag(0x0060, 0x3008);

		/// <summary>(0060,3010) VR=LO VM=1 Histogram Explanation</summary>
		DicomTags.HistogramExplanation = new DicomTag(0x0060, 0x3010);

		/// <summary>(0060,3020) VR=UL VM=1-n Histogram Data</summary>
		DicomTags.HistogramData = new DicomTag(0x0060, 0x3020);

		/// <summary>(0062,0001) VR=CS VM=1 Segmentation Type</summary>
		DicomTags.SegmentationType = new DicomTag(0x0062, 0x0001);

		/// <summary>(0062,0002) VR=SQ VM=1 Segment Sequence</summary>
		DicomTags.SegmentSequence = new DicomTag(0x0062, 0x0002);

		/// <summary>(0062,0003) VR=SQ VM=1 Segmented Property Category Code Sequence</summary>
		DicomTags.SegmentedPropertyCategoryCodeSequence = new DicomTag(0x0062, 0x0003);

		/// <summary>(0062,0004) VR=US VM=1 Segment Number</summary>
		DicomTags.SegmentNumber = new DicomTag(0x0062, 0x0004);

		/// <summary>(0062,0005) VR=LO VM=1 Segment Label</summary>
		DicomTags.SegmentLabel = new DicomTag(0x0062, 0x0005);

		/// <summary>(0062,0006) VR=ST VM=1 Segment Description</summary>
		DicomTags.SegmentDescription = new DicomTag(0x0062, 0x0006);

		/// <summary>(0062,0008) VR=CS VM=1 Segment Algorithm Type</summary>
		DicomTags.SegmentAlgorithmType = new DicomTag(0x0062, 0x0008);

		/// <summary>(0062,0009) VR=LO VM=1 Segment Algorithm Name</summary>
		DicomTags.SegmentAlgorithmName = new DicomTag(0x0062, 0x0009);

		/// <summary>(0062,000a) VR=SQ VM=1 Segment Identification Sequence</summary>
		DicomTags.SegmentIdentificationSequence = new DicomTag(0x0062, 0x000a);

		/// <summary>(0062,000b) VR=US VM=1-n Referenced Segment Number</summary>
		DicomTags.ReferencedSegmentNumber = new DicomTag(0x0062, 0x000b);

		/// <summary>(0062,000c) VR=US VM=1 Recommended Display Grayscale Value</summary>
		DicomTags.RecommendedDisplayGrayscaleValue = new DicomTag(0x0062, 0x000c);

		/// <summary>(0062,000d) VR=US VM=3 Recommended Display CIELab Value</summary>
		DicomTags.RecommendedDisplayCIELabValue = new DicomTag(0x0062, 0x000d);

		/// <summary>(0062,000e) VR=US VM=1 Maximum Fractional Value</summary>
		DicomTags.MaximumFractionalValue = new DicomTag(0x0062, 0x000e);

		/// <summary>(0062,000f) VR=SQ VM=1 Segmented Property Type Code Sequence</summary>
		DicomTags.SegmentedPropertyTypeCodeSequence = new DicomTag(0x0062, 0x000f);

		/// <summary>(0062,0010) VR=CS VM=1 Segmentation Fractional Type</summary>
		DicomTags.SegmentationFractionalType = new DicomTag(0x0062, 0x0010);

		/// <summary>(0064,0002) VR=SQ VM=1 Deformable Registration Sequence</summary>
		DicomTags.DeformableRegistrationSequence = new DicomTag(0x0064, 0x0002);

		/// <summary>(0064,0003) VR=UI VM=1 Source Frame of Reference UID</summary>
		DicomTags.SourceFrameOfReferenceUID = new DicomTag(0x0064, 0x0003);

		/// <summary>(0064,0005) VR=SQ VM=1 Deformable Registration Grid Sequence</summary>
		DicomTags.DeformableRegistrationGridSequence = new DicomTag(0x0064, 0x0005);

		/// <summary>(0064,0007) VR=UL VM=3 Grid Dimensions</summary>
		DicomTags.GridDimensions = new DicomTag(0x0064, 0x0007);

		/// <summary>(0064,0008) VR=FD VM=3 Grid Resolution</summary>
		DicomTags.GridResolution = new DicomTag(0x0064, 0x0008);

		/// <summary>(0064,0009) VR=OF VM=1 Vector Grid Data</summary>
		DicomTags.VectorGridData = new DicomTag(0x0064, 0x0009);

		/// <summary>(0064,000f) VR=SQ VM=1 Pre Deformation Matrix Registration Sequence</summary>
		DicomTags.PreDeformationMatrixRegistrationSequence = new DicomTag(0x0064, 0x000f);

		/// <summary>(0064,0010) VR=SQ VM=1 Post Deformation Matrix Registration Sequence</summary>
		DicomTags.PostDeformationMatrixRegistrationSequence = new DicomTag(0x0064, 0x0010);

		/// <summary>(0070,0001) VR=SQ VM=1 Graphic Annotation Sequence</summary>
		DicomTags.GraphicAnnotationSequence = new DicomTag(0x0070, 0x0001);

		/// <summary>(0070,0002) VR=CS VM=1 Graphic Layer</summary>
		DicomTags.GraphicLayer = new DicomTag(0x0070, 0x0002);

		/// <summary>(0070,0003) VR=CS VM=1 Bounding Box Annotation Units</summary>
		DicomTags.BoundingBoxAnnotationUnits = new DicomTag(0x0070, 0x0003);

		/// <summary>(0070,0004) VR=CS VM=1 Anchor Point Annotation Units</summary>
		DicomTags.AnchorPointAnnotationUnits = new DicomTag(0x0070, 0x0004);

		/// <summary>(0070,0005) VR=CS VM=1 Graphic Annotation Units</summary>
		DicomTags.GraphicAnnotationUnits = new DicomTag(0x0070, 0x0005);

		/// <summary>(0070,0006) VR=ST VM=1 Unformatted Text Value</summary>
		DicomTags.UnformattedTextValue = new DicomTag(0x0070, 0x0006);

		/// <summary>(0070,0008) VR=SQ VM=1 Text Object Sequence</summary>
		DicomTags.TextObjectSequence = new DicomTag(0x0070, 0x0008);

		/// <summary>(0070,0009) VR=SQ VM=1 Graphic Object Sequence</summary>
		DicomTags.GraphicObjectSequence = new DicomTag(0x0070, 0x0009);

		/// <summary>(0070,0010) VR=FL VM=2 Bounding Box Top Left Hand Corner</summary>
		DicomTags.BoundingBoxTopLeftHandCorner = new DicomTag(0x0070, 0x0010);

		/// <summary>(0070,0011) VR=FL VM=2 Bounding Box Bottom Right Hand Corner</summary>
		DicomTags.BoundingBoxBottomRightHandCorner = new DicomTag(0x0070, 0x0011);

		/// <summary>(0070,0012) VR=CS VM=1 Bounding Box Text Horizontal Justification</summary>
		DicomTags.BoundingBoxTextHorizontalJustification = new DicomTag(0x0070, 0x0012);

		/// <summary>(0070,0014) VR=FL VM=2 Anchor Point</summary>
		DicomTags.AnchorPoint = new DicomTag(0x0070, 0x0014);

		/// <summary>(0070,0015) VR=CS VM=1 Anchor Point Visibility</summary>
		DicomTags.AnchorPointVisibility = new DicomTag(0x0070, 0x0015);

		/// <summary>(0070,0020) VR=US VM=1 Graphic Dimensions</summary>
		DicomTags.GraphicDimensions = new DicomTag(0x0070, 0x0020);

		/// <summary>(0070,0021) VR=US VM=1 Number of Graphic Points</summary>
		DicomTags.NumberOfGraphicPoints = new DicomTag(0x0070, 0x0021);

		/// <summary>(0070,0022) VR=FL VM=2-n Graphic Data</summary>
		DicomTags.GraphicData = new DicomTag(0x0070, 0x0022);

		/// <summary>(0070,0023) VR=CS VM=1 Graphic Type</summary>
		DicomTags.GraphicType = new DicomTag(0x0070, 0x0023);

		/// <summary>(0070,0024) VR=CS VM=1 Graphic Filled</summary>
		DicomTags.GraphicFilled = new DicomTag(0x0070, 0x0024);

		/// <summary>(0070,0040) VR=IS VM=1 Image Rotation (Retired) (Retired)</summary>
		DicomTags.ImageRotationRetiredRETIRED = new DicomTag(0x0070, 0x0040);

		/// <summary>(0070,0041) VR=CS VM=1 Image Horizontal Flip</summary>
		DicomTags.ImageHorizontalFlip = new DicomTag(0x0070, 0x0041);

		/// <summary>(0070,0042) VR=US VM=1 Image Rotation</summary>
		DicomTags.ImageRotation = new DicomTag(0x0070, 0x0042);

		/// <summary>(0070,0050) VR=US VM=2 Displayed Area Top Left Hand Corner (Trial) (Retired)</summary>
		DicomTags.DisplayedAreaTopLeftHandCornerTrialRETIRED = new DicomTag(0x0070, 0x0050);

		/// <summary>(0070,0051) VR=US VM=2 Displayed Area Bottom Right Hand Corner (Trial) (Retired)</summary>
		DicomTags.DisplayedAreaBottomRightHandCornerTrialRETIRED = new DicomTag(0x0070, 0x0051);

		/// <summary>(0070,0052) VR=SL VM=2 Displayed Area Top Left Hand Corner</summary>
		DicomTags.DisplayedAreaTopLeftHandCorner = new DicomTag(0x0070, 0x0052);

		/// <summary>(0070,0053) VR=SL VM=2 Displayed Area Bottom Right Hand Corner</summary>
		DicomTags.DisplayedAreaBottomRightHandCorner = new DicomTag(0x0070, 0x0053);

		/// <summary>(0070,005a) VR=SQ VM=1 Displayed Area Selection Sequence</summary>
		DicomTags.DisplayedAreaSelectionSequence = new DicomTag(0x0070, 0x005a);

		/// <summary>(0070,0060) VR=SQ VM=1 Graphic Layer Sequence</summary>
		DicomTags.GraphicLayerSequence = new DicomTag(0x0070, 0x0060);

		/// <summary>(0070,0062) VR=IS VM=1 Graphic Layer Order</summary>
		DicomTags.GraphicLayerOrder = new DicomTag(0x0070, 0x0062);

		/// <summary>(0070,0066) VR=US VM=1 Graphic Layer Recommended Display Grayscale Value</summary>
		DicomTags.GraphicLayerRecommendedDisplayGrayscaleValue = new DicomTag(0x0070, 0x0066);

		/// <summary>(0070,0067) VR=US VM=3 Graphic Layer Recommended Display RGB Value (Retired)</summary>
		DicomTags.GraphicLayerRecommendedDisplayRGBValueRETIRED = new DicomTag(0x0070, 0x0067);

		/// <summary>(0070,0068) VR=LO VM=1 Graphic Layer Description</summary>
		DicomTags.GraphicLayerDescription = new DicomTag(0x0070, 0x0068);

		/// <summary>(0070,0080) VR=CS VM=1 Content Label</summary>
		DicomTags.ContentLabel = new DicomTag(0x0070, 0x0080);

		/// <summary>(0070,0081) VR=LO VM=1 Content Description</summary>
		DicomTags.ContentDescription = new DicomTag(0x0070, 0x0081);

		/// <summary>(0070,0082) VR=DA VM=1 Presentation Creation Date</summary>
		DicomTags.PresentationCreationDate = new DicomTag(0x0070, 0x0082);

		/// <summary>(0070,0083) VR=TM VM=1 Presentation Creation Time</summary>
		DicomTags.PresentationCreationTime = new DicomTag(0x0070, 0x0083);

		/// <summary>(0070,0084) VR=PN VM=1 Content Creator's Name</summary>
		DicomTags.ContentCreatorsName = new DicomTag(0x0070, 0x0084);

		/// <summary>(0070,0086) VR=SQ VM=1 Content Creator's Identification Code Sequence</summary>
		DicomTags.ContentCreatorsIdentificationCodeSequence = new DicomTag(0x0070, 0x0086);

		/// <summary>(0070,0100) VR=CS VM=1 Presentation Size Mode</summary>
		DicomTags.PresentationSizeMode = new DicomTag(0x0070, 0x0100);

		/// <summary>(0070,0101) VR=DS VM=2 Presentation Pixel Spacing</summary>
		DicomTags.PresentationPixelSpacing = new DicomTag(0x0070, 0x0101);

		/// <summary>(0070,0102) VR=IS VM=2 Presentation Pixel Aspect Ratio</summary>
		DicomTags.PresentationPixelAspectRatio = new DicomTag(0x0070, 0x0102);

		/// <summary>(0070,0103) VR=FL VM=1 Presentation Pixel Magnification Ratio</summary>
		DicomTags.PresentationPixelMagnificationRatio = new DicomTag(0x0070, 0x0103);

		/// <summary>(0070,0306) VR=CS VM=1 Shape Type</summary>
		DicomTags.ShapeType = new DicomTag(0x0070, 0x0306);

		/// <summary>(0070,0308) VR=SQ VM=1 Registration Sequence</summary>
		DicomTags.RegistrationSequence = new DicomTag(0x0070, 0x0308);

		/// <summary>(0070,0309) VR=SQ VM=1 Matrix Registration Sequence</summary>
		DicomTags.MatrixRegistrationSequence = new DicomTag(0x0070, 0x0309);

		/// <summary>(0070,030a) VR=SQ VM=1 Matrix Sequence</summary>
		DicomTags.MatrixSequence = new DicomTag(0x0070, 0x030a);

		/// <summary>(0070,030c) VR=CS VM=1 Frame of Reference Transformation Matrix Type</summary>
		DicomTags.FrameOfReferenceTransformationMatrixType = new DicomTag(0x0070, 0x030c);

		/// <summary>(0070,030d) VR=SQ VM=1 Registration Type Code Sequence</summary>
		DicomTags.RegistrationTypeCodeSequence = new DicomTag(0x0070, 0x030d);

		/// <summary>(0070,030f) VR=ST VM=1 Fiducial Description</summary>
		DicomTags.FiducialDescription = new DicomTag(0x0070, 0x030f);

		/// <summary>(0070,0310) VR=SH VM=1 Fiducial Identifier</summary>
		DicomTags.FiducialIdentifier = new DicomTag(0x0070, 0x0310);

		/// <summary>(0070,0311) VR=SQ VM=1 Fiducial Identifier Code Sequence</summary>
		DicomTags.FiducialIdentifierCodeSequence = new DicomTag(0x0070, 0x0311);

		/// <summary>(0070,0312) VR=FD VM=1 Contour Uncertainty Radius</summary>
		DicomTags.ContourUncertaintyRadius = new DicomTag(0x0070, 0x0312);

		/// <summary>(0070,0314) VR=SQ VM=1 Used Fiducials Sequence</summary>
		DicomTags.UsedFiducialsSequence = new DicomTag(0x0070, 0x0314);

		/// <summary>(0070,0318) VR=SQ VM=1 Graphic Coordinates Data Sequence</summary>
		DicomTags.GraphicCoordinatesDataSequence = new DicomTag(0x0070, 0x0318);

		/// <summary>(0070,031a) VR=UI VM=1 Fiducial UID</summary>
		DicomTags.FiducialUID = new DicomTag(0x0070, 0x031a);

		/// <summary>(0070,031c) VR=SQ VM=1 Fiducial Set Sequence</summary>
		DicomTags.FiducialSetSequence = new DicomTag(0x0070, 0x031c);

		/// <summary>(0070,031e) VR=SQ VM=1 Fiducial Sequence</summary>
		DicomTags.FiducialSequence = new DicomTag(0x0070, 0x031e);

		/// <summary>(0070,0401) VR=US VM=3 Graphic Layer Recommended Display CIELab Value</summary>
		DicomTags.GraphicLayerRecommendedDisplayCIELabValue = new DicomTag(0x0070, 0x0401);

		/// <summary>(0070,0402) VR=SQ VM=1 Blending Sequence</summary>
		DicomTags.BlendingSequence = new DicomTag(0x0070, 0x0402);

		/// <summary>(0070,0403) VR=FL VM=1 Relative Opacity</summary>
		DicomTags.RelativeOpacity = new DicomTag(0x0070, 0x0403);

		/// <summary>(0070,0404) VR=SQ VM=1 Referenced Spatial Registration Sequence</summary>
		DicomTags.ReferencedSpatialRegistrationSequence = new DicomTag(0x0070, 0x0404);

		/// <summary>(0070,0405) VR=CS VM=1 Blending Position</summary>
		DicomTags.BlendingPosition = new DicomTag(0x0070, 0x0405);

		/// <summary>(0072,0002) VR=SH VM=1 Hanging Protocol Name</summary>
		DicomTags.HangingProtocolName = new DicomTag(0x0072, 0x0002);

		/// <summary>(0072,0004) VR=LO VM=1 Hanging Protocol Description</summary>
		DicomTags.HangingProtocolDescription = new DicomTag(0x0072, 0x0004);

		/// <summary>(0072,0006) VR=CS VM=1 Hanging Protocol Level</summary>
		DicomTags.HangingProtocolLevel = new DicomTag(0x0072, 0x0006);

		/// <summary>(0072,0008) VR=LO VM=1 Hanging Protocol Creator</summary>
		DicomTags.HangingProtocolCreator = new DicomTag(0x0072, 0x0008);

		/// <summary>(0072,000a) VR=DT VM=1 Hanging Protocol Creation DateTime</summary>
		DicomTags.HangingProtocolCreationDateTime = new DicomTag(0x0072, 0x000a);

		/// <summary>(0072,000c) VR=SQ VM=1 Hanging Protocol Definition Sequence</summary>
		DicomTags.HangingProtocolDefinitionSequence = new DicomTag(0x0072, 0x000c);

		/// <summary>(0072,000e) VR=SQ VM=1 Hanging Protocol User Identification Code Sequence</summary>
		DicomTags.HangingProtocolUserIdentificationCodeSequence = new DicomTag(0x0072, 0x000e);

		/// <summary>(0072,0010) VR=LO VM=1 Hanging Protocol User Group Name</summary>
		DicomTags.HangingProtocolUserGroupName = new DicomTag(0x0072, 0x0010);

		/// <summary>(0072,0012) VR=SQ VM=1 Source Hanging Protocol Sequence</summary>
		DicomTags.SourceHangingProtocolSequence = new DicomTag(0x0072, 0x0012);

		/// <summary>(0072,0014) VR=US VM=1 Number of Priors Referenced</summary>
		DicomTags.NumberOfPriorsReferenced = new DicomTag(0x0072, 0x0014);

		/// <summary>(0072,0020) VR=SQ VM=1 Image Sets Sequence</summary>
		DicomTags.ImageSetsSequence = new DicomTag(0x0072, 0x0020);

		/// <summary>(0072,0022) VR=SQ VM=1 Image Set Selector Sequence</summary>
		DicomTags.ImageSetSelectorSequence = new DicomTag(0x0072, 0x0022);

		/// <summary>(0072,0024) VR=CS VM=1 Image Set Selector Usage Flag</summary>
		DicomTags.ImageSetSelectorUsageFlag = new DicomTag(0x0072, 0x0024);

		/// <summary>(0072,0026) VR=AT VM=1 Selector Attribute</summary>
		DicomTags.SelectorAttribute = new DicomTag(0x0072, 0x0026);

		/// <summary>(0072,0028) VR=US VM=1 Selector Value Number</summary>
		DicomTags.SelectorValueNumber = new DicomTag(0x0072, 0x0028);

		/// <summary>(0072,0030) VR=SQ VM=1 Time Based Image Sets Sequence</summary>
		DicomTags.TimeBasedImageSetsSequence = new DicomTag(0x0072, 0x0030);

		/// <summary>(0072,0032) VR=US VM=1 Image Set Number</summary>
		DicomTags.ImageSetNumber = new DicomTag(0x0072, 0x0032);

		/// <summary>(0072,0034) VR=CS VM=1 Image Set Selector Category</summary>
		DicomTags.ImageSetSelectorCategory = new DicomTag(0x0072, 0x0034);

		/// <summary>(0072,0038) VR=US VM=2 Relative Time</summary>
		DicomTags.RelativeTime = new DicomTag(0x0072, 0x0038);

		/// <summary>(0072,003a) VR=CS VM=1 Relative Time Units</summary>
		DicomTags.RelativeTimeUnits = new DicomTag(0x0072, 0x003a);

		/// <summary>(0072,003c) VR=SS VM=2 Abstract Prior Value</summary>
		DicomTags.AbstractPriorValue = new DicomTag(0x0072, 0x003c);

		/// <summary>(0072,003e) VR=SQ VM=1 Abstract Prior Code Sequence</summary>
		DicomTags.AbstractPriorCodeSequence = new DicomTag(0x0072, 0x003e);

		/// <summary>(0072,0040) VR=LO VM=1 Image Set Label</summary>
		DicomTags.ImageSetLabel = new DicomTag(0x0072, 0x0040);

		/// <summary>(0072,0050) VR=CS VM=1 Selector Attribute VR</summary>
		DicomTags.SelectorAttributeVR = new DicomTag(0x0072, 0x0050);

		/// <summary>(0072,0052) VR=AT VM=1 Selector Sequence Pointer</summary>
		DicomTags.SelectorSequencePointer = new DicomTag(0x0072, 0x0052);

		/// <summary>(0072,0054) VR=LO VM=1 Selector Sequence Pointer Private Creator</summary>
		DicomTags.SelectorSequencePointerPrivateCreator = new DicomTag(0x0072, 0x0054);

		/// <summary>(0072,0056) VR=LO VM=1 Selector Attribute Private Creator</summary>
		DicomTags.SelectorAttributePrivateCreator = new DicomTag(0x0072, 0x0056);

		/// <summary>(0072,0060) VR=AT VM=1-n Selector AT Value</summary>
		DicomTags.SelectorATValue = new DicomTag(0x0072, 0x0060);

		/// <summary>(0072,0062) VR=CS VM=1-n Selector CS Value</summary>
		DicomTags.SelectorCSValue = new DicomTag(0x0072, 0x0062);

		/// <summary>(0072,0064) VR=IS VM=1-n Selector IS Value</summary>
		DicomTags.SelectorISValue = new DicomTag(0x0072, 0x0064);

		/// <summary>(0072,0066) VR=LO VM=1-n Selector LO Value</summary>
		DicomTags.SelectorLOValue = new DicomTag(0x0072, 0x0066);

		/// <summary>(0072,0068) VR=LT VM=1 Selector LT Value</summary>
		DicomTags.SelectorLTValue = new DicomTag(0x0072, 0x0068);

		/// <summary>(0072,006a) VR=PN VM=1-n Selector PN Value</summary>
		DicomTags.SelectorPNValue = new DicomTag(0x0072, 0x006a);

		/// <summary>(0072,006c) VR=SH VM=1-n Selector SH Value</summary>
		DicomTags.SelectorSHValue = new DicomTag(0x0072, 0x006c);

		/// <summary>(0072,006e) VR=ST VM=1 Selector ST Value</summary>
		DicomTags.SelectorSTValue = new DicomTag(0x0072, 0x006e);

		/// <summary>(0072,0070) VR=UT VM=1 Selector UT Value</summary>
		DicomTags.SelectorUTValue = new DicomTag(0x0072, 0x0070);

		/// <summary>(0072,0072) VR=DS VM=1-n Selector DS Value</summary>
		DicomTags.SelectorDSValue = new DicomTag(0x0072, 0x0072);

		/// <summary>(0072,0074) VR=FD VM=1-n Selector FD Value</summary>
		DicomTags.SelectorFDValue = new DicomTag(0x0072, 0x0074);

		/// <summary>(0072,0076) VR=FL VM=1-n Selector FL Value</summary>
		DicomTags.SelectorFLValue = new DicomTag(0x0072, 0x0076);

		/// <summary>(0072,0078) VR=UL VM=1-n Selector UL Value</summary>
		DicomTags.SelectorULValue = new DicomTag(0x0072, 0x0078);

		/// <summary>(0072,007a) VR=US VM=1-n Selector US Value</summary>
		DicomTags.SelectorUSValue = new DicomTag(0x0072, 0x007a);

		/// <summary>(0072,007c) VR=SL VM=1-n Selector SL Value</summary>
		DicomTags.SelectorSLValue = new DicomTag(0x0072, 0x007c);

		/// <summary>(0072,007e) VR=SS VM=1-n Selector SS Value</summary>
		DicomTags.SelectorSSValue = new DicomTag(0x0072, 0x007e);

		/// <summary>(0072,0080) VR=SQ VM=1 Selector Code Sequence Value</summary>
		DicomTags.SelectorCodeSequenceValue = new DicomTag(0x0072, 0x0080);

		/// <summary>(0072,0100) VR=US VM=1 Number of Screens</summary>
		DicomTags.NumberOfScreens = new DicomTag(0x0072, 0x0100);

		/// <summary>(0072,0102) VR=SQ VM=1 Nominal Screen Definition Sequence</summary>
		DicomTags.NominalScreenDefinitionSequence = new DicomTag(0x0072, 0x0102);

		/// <summary>(0072,0104) VR=US VM=1 Number of Vertical Pixels</summary>
		DicomTags.NumberOfVerticalPixels = new DicomTag(0x0072, 0x0104);

		/// <summary>(0072,0106) VR=US VM=1 Number of Horizontal Pixels</summary>
		DicomTags.NumberOfHorizontalPixels = new DicomTag(0x0072, 0x0106);

		/// <summary>(0072,0108) VR=FD VM=4 Display Environment Spatial Position</summary>
		DicomTags.DisplayEnvironmentSpatialPosition = new DicomTag(0x0072, 0x0108);

		/// <summary>(0072,010a) VR=US VM=1 Screen Minimum Grayscale Bit Depth</summary>
		DicomTags.ScreenMinimumGrayscaleBitDepth = new DicomTag(0x0072, 0x010a);

		/// <summary>(0072,010c) VR=US VM=1 Screen Minimum Color Bit Depth</summary>
		DicomTags.ScreenMinimumColorBitDepth = new DicomTag(0x0072, 0x010c);

		/// <summary>(0072,010e) VR=US VM=1 Application Maximum Repaint Time</summary>
		DicomTags.ApplicationMaximumRepaintTime = new DicomTag(0x0072, 0x010e);

		/// <summary>(0072,0200) VR=SQ VM=1 Display Sets Sequence</summary>
		DicomTags.DisplaySetsSequence = new DicomTag(0x0072, 0x0200);

		/// <summary>(0072,0202) VR=US VM=1 Display Set Number</summary>
		DicomTags.DisplaySetNumber = new DicomTag(0x0072, 0x0202);

		/// <summary>(0072,0203) VR=LO VM=1 Display Set Label</summary>
		DicomTags.DisplaySetLabel = new DicomTag(0x0072, 0x0203);

		/// <summary>(0072,0204) VR=US VM=1 Display Set Presentation Group</summary>
		DicomTags.DisplaySetPresentationGroup = new DicomTag(0x0072, 0x0204);

		/// <summary>(0072,0206) VR=LO VM=1 Display Set Presentation Group Description</summary>
		DicomTags.DisplaySetPresentationGroupDescription = new DicomTag(0x0072, 0x0206);

		/// <summary>(0072,0208) VR=CS VM=1 Partial Data Display Handling</summary>
		DicomTags.PartialDataDisplayHandling = new DicomTag(0x0072, 0x0208);

		/// <summary>(0072,0210) VR=SQ VM=1 Synchronized Scrolling Sequence</summary>
		DicomTags.SynchronizedScrollingSequence = new DicomTag(0x0072, 0x0210);

		/// <summary>(0072,0212) VR=US VM=2-n Display Set Scrolling Group</summary>
		DicomTags.DisplaySetScrollingGroup = new DicomTag(0x0072, 0x0212);

		/// <summary>(0072,0214) VR=SQ VM=1 Navigation Indicator Sequence</summary>
		DicomTags.NavigationIndicatorSequence = new DicomTag(0x0072, 0x0214);

		/// <summary>(0072,0216) VR=US VM=1 Navigation Display Set</summary>
		DicomTags.NavigationDisplaySet = new DicomTag(0x0072, 0x0216);

		/// <summary>(0072,0218) VR=US VM=1-n Reference Display Sets</summary>
		DicomTags.ReferenceDisplaySets = new DicomTag(0x0072, 0x0218);

		/// <summary>(0072,0300) VR=SQ VM=1 Image Boxes Sequence</summary>
		DicomTags.ImageBoxesSequence = new DicomTag(0x0072, 0x0300);

		/// <summary>(0072,0302) VR=US VM=1 Image Box Number</summary>
		DicomTags.ImageBoxNumber = new DicomTag(0x0072, 0x0302);

		/// <summary>(0072,0304) VR=CS VM=1 Image Box Layout Type</summary>
		DicomTags.ImageBoxLayoutType = new DicomTag(0x0072, 0x0304);

		/// <summary>(0072,0306) VR=US VM=1 Image Box Tile Horizontal Dimension</summary>
		DicomTags.ImageBoxTileHorizontalDimension = new DicomTag(0x0072, 0x0306);

		/// <summary>(0072,0308) VR=US VM=1 Image Box Tile Vertical Dimension</summary>
		DicomTags.ImageBoxTileVerticalDimension = new DicomTag(0x0072, 0x0308);

		/// <summary>(0072,0310) VR=CS VM=1 Image Box Scroll Direction</summary>
		DicomTags.ImageBoxScrollDirection = new DicomTag(0x0072, 0x0310);

		/// <summary>(0072,0312) VR=CS VM=1 Image Box Small Scroll Type</summary>
		DicomTags.ImageBoxSmallScrollType = new DicomTag(0x0072, 0x0312);

		/// <summary>(0072,0314) VR=US VM=1 Image Box Small Scroll Amount</summary>
		DicomTags.ImageBoxSmallScrollAmount = new DicomTag(0x0072, 0x0314);

		/// <summary>(0072,0316) VR=CS VM=1 Image Box Large Scroll Type</summary>
		DicomTags.ImageBoxLargeScrollType = new DicomTag(0x0072, 0x0316);

		/// <summary>(0072,0318) VR=US VM=1 Image Box Large Scroll Amount</summary>
		DicomTags.ImageBoxLargeScrollAmount = new DicomTag(0x0072, 0x0318);

		/// <summary>(0072,0320) VR=US VM=1 Image Box Overlap Priority</summary>
		DicomTags.ImageBoxOverlapPriority = new DicomTag(0x0072, 0x0320);

		/// <summary>(0072,0330) VR=FD VM=1 Cine Relative to Real-Time</summary>
		DicomTags.CineRelativeToRealTime = new DicomTag(0x0072, 0x0330);

		/// <summary>(0072,0400) VR=SQ VM=1 Filter Operations Sequence</summary>
		DicomTags.FilterOperationsSequence = new DicomTag(0x0072, 0x0400);

		/// <summary>(0072,0402) VR=CS VM=1 Filter-by Category</summary>
		DicomTags.FilterbyCategory = new DicomTag(0x0072, 0x0402);

		/// <summary>(0072,0404) VR=CS VM=1 Filter-by Attribute Presence</summary>
		DicomTags.FilterbyAttributePresence = new DicomTag(0x0072, 0x0404);

		/// <summary>(0072,0406) VR=CS VM=1 Filter-by Operator</summary>
		DicomTags.FilterbyOperator = new DicomTag(0x0072, 0x0406);

		/// <summary>(0072,0500) VR=CS VM=1 Blending Operation Type</summary>
		DicomTags.BlendingOperationType = new DicomTag(0x0072, 0x0500);

		/// <summary>(0072,0510) VR=CS VM=1 Reformatting Operation Type</summary>
		DicomTags.ReformattingOperationType = new DicomTag(0x0072, 0x0510);

		/// <summary>(0072,0512) VR=FD VM=1 Reformatting Thickness</summary>
		DicomTags.ReformattingThickness = new DicomTag(0x0072, 0x0512);

		/// <summary>(0072,0514) VR=FD VM=1 Reformatting Interval</summary>
		DicomTags.ReformattingInterval = new DicomTag(0x0072, 0x0514);

		/// <summary>(0072,0516) VR=CS VM=1 Reformatting Operation Initial View Direction</summary>
		DicomTags.ReformattingOperationInitialViewDirection = new DicomTag(0x0072, 0x0516);

		/// <summary>(0072,0520) VR=CS VM=1-n 3D Rendering Type</summary>
		DicomTags.RenderingType3D = new DicomTag(0x0072, 0x0520);

		/// <summary>(0072,0600) VR=SQ VM=1 Sorting Operations Sequence</summary>
		DicomTags.SortingOperationsSequence = new DicomTag(0x0072, 0x0600);

		/// <summary>(0072,0602) VR=CS VM=1 Sort-by Category</summary>
		DicomTags.SortbyCategory = new DicomTag(0x0072, 0x0602);

		/// <summary>(0072,0604) VR=CS VM=1 Sorting Direction</summary>
		DicomTags.SortingDirection = new DicomTag(0x0072, 0x0604);

		/// <summary>(0072,0700) VR=CS VM=2 Display Set Patient Orientation</summary>
		DicomTags.DisplaySetPatientOrientation = new DicomTag(0x0072, 0x0700);

		/// <summary>(0072,0702) VR=CS VM=1 VOI Type</summary>
		DicomTags.VOIType = new DicomTag(0x0072, 0x0702);

		/// <summary>(0072,0704) VR=CS VM=1 Pseudo-color Type</summary>
		DicomTags.PseudocolorType = new DicomTag(0x0072, 0x0704);

		/// <summary>(0072,0706) VR=CS VM=1 Show Grayscale Inverted</summary>
		DicomTags.ShowGrayscaleInverted = new DicomTag(0x0072, 0x0706);

		/// <summary>(0072,0710) VR=CS VM=1 Show Image True Size Flag</summary>
		DicomTags.ShowImageTrueSizeFlag = new DicomTag(0x0072, 0x0710);

		/// <summary>(0072,0712) VR=CS VM=1 Show Graphic Annotation Flag</summary>
		DicomTags.ShowGraphicAnnotationFlag = new DicomTag(0x0072, 0x0712);

		/// <summary>(0072,0714) VR=CS VM=1 Show Patient Demographics Flag</summary>
		DicomTags.ShowPatientDemographicsFlag = new DicomTag(0x0072, 0x0714);

		/// <summary>(0072,0716) VR=CS VM=1 Show Acquisition Techniques Flag</summary>
		DicomTags.ShowAcquisitionTechniquesFlag = new DicomTag(0x0072, 0x0716);

		/// <summary>(0072,0717) VR=CS VM=1 Display Set Horizontal Justification</summary>
		DicomTags.DisplaySetHorizontalJustification = new DicomTag(0x0072, 0x0717);

		/// <summary>(0072,0718) VR=CS VM=1 Display Set Vertical Justification</summary>
		DicomTags.DisplaySetVerticalJustification = new DicomTag(0x0072, 0x0718);

		/// <summary>(0074,1000) VR=CS VM=1 Unified Procedure Step State</summary>
		DicomTags.UnifiedProcedureStepState = new DicomTag(0x0074, 0x1000);

		/// <summary>(0074,1002) VR=SQ VM=1 UPS Progress Information Sequence</summary>
		DicomTags.UPSProgressInformationSequence = new DicomTag(0x0074, 0x1002);

		/// <summary>(0074,1004) VR=DS VM=1 Unified Procedure Step Progress</summary>
		DicomTags.UnifiedProcedureStepProgress = new DicomTag(0x0074, 0x1004);

		/// <summary>(0074,1006) VR=ST VM=1 Unified Procedure Step Progress Description</summary>
		DicomTags.UnifiedProcedureStepProgressDescription = new DicomTag(0x0074, 0x1006);

		/// <summary>(0074,1008) VR=SQ VM=1 Unified Procedure Step Communications URI Sequence</summary>
		DicomTags.UnifiedProcedureStepCommunicationsURISequence = new DicomTag(0x0074, 0x1008);

		/// <summary>(0074,100a) VR=ST VM=1 Contact URI</summary>
		DicomTags.ContactURI = new DicomTag(0x0074, 0x100a);

		/// <summary>(0074,100c) VR=LO VM=1 Contact Display Name</summary>
		DicomTags.ContactDisplayName = new DicomTag(0x0074, 0x100c);

		/// <summary>(0074,100e) VR=SQ VM=1 Unified Procedure Step Discontinuation Reason Code Sequence</summary>
		DicomTags.UnifiedProcedureStepDiscontinuationReasonCodeSequence = new DicomTag(0x0074, 0x100e);

		/// <summary>(0074,1020) VR=SQ VM=1 Beam Task Sequence</summary>
		DicomTags.BeamTaskSequence = new DicomTag(0x0074, 0x1020);

		/// <summary>(0074,1022) VR=CS VM=1 Beam Task Type</summary>
		DicomTags.BeamTaskType = new DicomTag(0x0074, 0x1022);

		/// <summary>(0074,1024) VR=IS VM=1 Beam Order Index</summary>
		DicomTags.BeamOrderIndex = new DicomTag(0x0074, 0x1024);

		/// <summary>(0074,1030) VR=SQ VM=1 Delivery Verification Image Sequence</summary>
		DicomTags.DeliveryVerificationImageSequence = new DicomTag(0x0074, 0x1030);

		/// <summary>(0074,1032) VR=CS VM=1 Verification Image Timing</summary>
		DicomTags.VerificationImageTiming = new DicomTag(0x0074, 0x1032);

		/// <summary>(0074,1034) VR=CS VM=1 Double Exposure Flag</summary>
		DicomTags.DoubleExposureFlag = new DicomTag(0x0074, 0x1034);

		/// <summary>(0074,1036) VR=CS VM=1 Double Exposure Ordering</summary>
		DicomTags.DoubleExposureOrdering = new DicomTag(0x0074, 0x1036);

		/// <summary>(0074,1038) VR=DS VM=1 Double Exposure Meterset</summary>
		DicomTags.DoubleExposureMeterset = new DicomTag(0x0074, 0x1038);

		/// <summary>(0074,103a) VR=DS VM=4 Double Exposure Field Delta</summary>
		DicomTags.DoubleExposureFieldDelta = new DicomTag(0x0074, 0x103a);

		/// <summary>(0074,1040) VR=SQ VM=1 Related Reference RT Image Sequence</summary>
		DicomTags.RelatedReferenceRTImageSequence = new DicomTag(0x0074, 0x1040);

		/// <summary>(0074,1042) VR=SQ VM=1 General Machine Verification Sequence</summary>
		DicomTags.GeneralMachineVerificationSequence = new DicomTag(0x0074, 0x1042);

		/// <summary>(0074,1044) VR=SQ VM=1 Conventional Machine Verification Sequence</summary>
		DicomTags.ConventionalMachineVerificationSequence = new DicomTag(0x0074, 0x1044);

		/// <summary>(0074,1046) VR=SQ VM=1 Ion Machine Verification Sequence</summary>
		DicomTags.IonMachineVerificationSequence = new DicomTag(0x0074, 0x1046);

		/// <summary>(0074,1048) VR=SQ VM=1 Failed Attributes Sequence</summary>
		DicomTags.FailedAttributesSequence = new DicomTag(0x0074, 0x1048);

		/// <summary>(0074,104a) VR=SQ VM=1 Overridden Attributes Sequence</summary>
		DicomTags.OverriddenAttributesSequence = new DicomTag(0x0074, 0x104a);

		/// <summary>(0074,104c) VR=SQ VM=1 Conventional Control Point Verification Sequence</summary>
		DicomTags.ConventionalControlPointVerificationSequence = new DicomTag(0x0074, 0x104c);

		/// <summary>(0074,104e) VR=SQ VM=1 Ion Control Point Verification Sequence</summary>
		DicomTags.IonControlPointVerificationSequence = new DicomTag(0x0074, 0x104e);

		/// <summary>(0074,1050) VR=SQ VM=1 Attribute Occurrence Sequence</summary>
		DicomTags.AttributeOccurrenceSequence = new DicomTag(0x0074, 0x1050);

		/// <summary>(0074,1052) VR=AT VM=1 Attribute Occurrence Pointer</summary>
		DicomTags.AttributeOccurrencePointer = new DicomTag(0x0074, 0x1052);

		/// <summary>(0074,1054) VR=UL VM=1 Attribute Item Selector</summary>
		DicomTags.AttributeItemSelector = new DicomTag(0x0074, 0x1054);

		/// <summary>(0074,1056) VR=LO VM=1 Attribute Occurrence Private Creator</summary>
		DicomTags.AttributeOccurrencePrivateCreator = new DicomTag(0x0074, 0x1056);

		/// <summary>(0074,1200) VR=CS VM=1 Scheduled Procedure Step Priority</summary>
		DicomTags.ScheduledProcedureStepPriority = new DicomTag(0x0074, 0x1200);

		/// <summary>(0074,1202) VR=LO VM=1 Worklist Label</summary>
		DicomTags.WorklistLabel = new DicomTag(0x0074, 0x1202);

		/// <summary>(0074,1204) VR=LO VM=1 Procedure Step Label</summary>
		DicomTags.ProcedureStepLabel = new DicomTag(0x0074, 0x1204);

		/// <summary>(0074,1210) VR=SQ VM=1 Scheduled Processing Parameters Sequence</summary>
		DicomTags.ScheduledProcessingParametersSequence = new DicomTag(0x0074, 0x1210);

		/// <summary>(0074,1212) VR=SQ VM=1 Performed Processing Parameters Sequence</summary>
		DicomTags.PerformedProcessingParametersSequence = new DicomTag(0x0074, 0x1212);

		/// <summary>(0074,1216) VR=SQ VM=1 UPS Performed Procedure Sequence</summary>
		DicomTags.UPSPerformedProcedureSequence = new DicomTag(0x0074, 0x1216);

		/// <summary>(0074,1220) VR=SQ VM=1 Related Procedure Step Sequence</summary>
		DicomTags.RelatedProcedureStepSequence = new DicomTag(0x0074, 0x1220);

		/// <summary>(0074,1222) VR=LO VM=1 Procedure Step Relationship Type</summary>
		DicomTags.ProcedureStepRelationshipType = new DicomTag(0x0074, 0x1222);

		/// <summary>(0074,1230) VR=LO VM=1 Deletion Lock</summary>
		DicomTags.DeletionLock = new DicomTag(0x0074, 0x1230);

		/// <summary>(0074,1234) VR=AE VM=1 Receiving AE</summary>
		DicomTags.ReceivingAE = new DicomTag(0x0074, 0x1234);

		/// <summary>(0074,1236) VR=AE VM=1 Requesting AE</summary>
		DicomTags.RequestingAE = new DicomTag(0x0074, 0x1236);

		/// <summary>(0074,1238) VR=LT VM=1 Reason for Cancellation</summary>
		DicomTags.ReasonForCancellation = new DicomTag(0x0074, 0x1238);

		/// <summary>(0074,1242) VR=CS VM=1 SCP Status</summary>
		DicomTags.SCPStatus = new DicomTag(0x0074, 0x1242);

		/// <summary>(0074,1244) VR=CS VM=1 Subscription List Status</summary>
		DicomTags.SubscriptionListStatus = new DicomTag(0x0074, 0x1244);

		/// <summary>(0074,1246) VR=CS VM=1 UPS List Status</summary>
		DicomTags.UPSListStatus = new DicomTag(0x0074, 0x1246);

		/// <summary>(0088,0130) VR=SH VM=1 Storage Media File-set ID</summary>
		DicomTags.StorageMediaFilesetID = new DicomTag(0x0088, 0x0130);

		/// <summary>(0088,0140) VR=UI VM=1 Storage Media File-set UID</summary>
		DicomTags.StorageMediaFilesetUID = new DicomTag(0x0088, 0x0140);

		/// <summary>(0088,0200) VR=SQ VM=1 Icon Image Sequence</summary>
		DicomTags.IconImageSequence = new DicomTag(0x0088, 0x0200);

		/// <summary>(0088,0904) VR=LO VM=1 Topic Title (Retired)</summary>
		DicomTags.TopicTitleRETIRED = new DicomTag(0x0088, 0x0904);

		/// <summary>(0088,0906) VR=ST VM=1 Topic Subject (Retired)</summary>
		DicomTags.TopicSubjectRETIRED = new DicomTag(0x0088, 0x0906);

		/// <summary>(0088,0910) VR=LO VM=1 Topic Author (Retired)</summary>
		DicomTags.TopicAuthorRETIRED = new DicomTag(0x0088, 0x0910);

		/// <summary>(0088,0912) VR=LO VM=1-32 Topic Keywords (Retired)</summary>
		DicomTags.TopicKeywordsRETIRED = new DicomTag(0x0088, 0x0912);

		/// <summary>(0100,0410) VR=CS VM=1 SOP Instance Status</summary>
		DicomTags.SOPInstanceStatus = new DicomTag(0x0100, 0x0410);

		/// <summary>(0100,0420) VR=DT VM=1 SOP Authorization Date and Time</summary>
		DicomTags.SOPAuthorizationDateAndTime = new DicomTag(0x0100, 0x0420);

		/// <summary>(0100,0424) VR=LT VM=1 SOP Authorization Comment</summary>
		DicomTags.SOPAuthorizationComment = new DicomTag(0x0100, 0x0424);

		/// <summary>(0100,0426) VR=LO VM=1 Authorization Equipment Certification Number</summary>
		DicomTags.AuthorizationEquipmentCertificationNumber = new DicomTag(0x0100, 0x0426);

		/// <summary>(0400,0005) VR=US VM=1 MAC ID Number</summary>
		DicomTags.MACIDNumber = new DicomTag(0x0400, 0x0005);

		/// <summary>(0400,0010) VR=UI VM=1 MAC Calculation Transfer Syntax UID</summary>
		DicomTags.MACCalculationTransferSyntaxUID = new DicomTag(0x0400, 0x0010);

		/// <summary>(0400,0015) VR=CS VM=1 MAC Algorithm</summary>
		DicomTags.MACAlgorithm = new DicomTag(0x0400, 0x0015);

		/// <summary>(0400,0020) VR=AT VM=1-n Data Elements Signed</summary>
		DicomTags.DataElementsSigned = new DicomTag(0x0400, 0x0020);

		/// <summary>(0400,0100) VR=UI VM=1 Digital Signature UID</summary>
		DicomTags.DigitalSignatureUID = new DicomTag(0x0400, 0x0100);

		/// <summary>(0400,0105) VR=DT VM=1 Digital Signature DateTime</summary>
		DicomTags.DigitalSignatureDateTime = new DicomTag(0x0400, 0x0105);

		/// <summary>(0400,0110) VR=CS VM=1 Certificate Type</summary>
		DicomTags.CertificateType = new DicomTag(0x0400, 0x0110);

		/// <summary>(0400,0115) VR=OB VM=1 Certificate of Signer</summary>
		DicomTags.CertificateOfSigner = new DicomTag(0x0400, 0x0115);

		/// <summary>(0400,0120) VR=OB VM=1 Signature</summary>
		DicomTags.Signature = new DicomTag(0x0400, 0x0120);

		/// <summary>(0400,0305) VR=CS VM=1 Certified Timestamp Type</summary>
		DicomTags.CertifiedTimestampType = new DicomTag(0x0400, 0x0305);

		/// <summary>(0400,0310) VR=OB VM=1 Certified Timestamp</summary>
		DicomTags.CertifiedTimestamp = new DicomTag(0x0400, 0x0310);

		/// <summary>(0400,0401) VR=SQ VM=1 Digital Signature Purpose Code Sequence</summary>
		DicomTags.DigitalSignaturePurposeCodeSequence = new DicomTag(0x0400, 0x0401);

		/// <summary>(0400,0402) VR=SQ VM=1 Referenced Digital Signature Sequence</summary>
		DicomTags.ReferencedDigitalSignatureSequence = new DicomTag(0x0400, 0x0402);

		/// <summary>(0400,0403) VR=SQ VM=1 Referenced SOP Instance MAC Sequence</summary>
		DicomTags.ReferencedSOPInstanceMACSequence = new DicomTag(0x0400, 0x0403);

		/// <summary>(0400,0404) VR=OB VM=1 MAC</summary>
		DicomTags.MAC = new DicomTag(0x0400, 0x0404);

		/// <summary>(0400,0500) VR=SQ VM=1 Encrypted Attributes Sequence</summary>
		DicomTags.EncryptedAttributesSequence = new DicomTag(0x0400, 0x0500);

		/// <summary>(0400,0510) VR=UI VM=1 Encrypted Content Transfer Syntax UID</summary>
		DicomTags.EncryptedContentTransferSyntaxUID = new DicomTag(0x0400, 0x0510);

		/// <summary>(0400,0520) VR=OB VM=1 Encrypted Content</summary>
		DicomTags.EncryptedContent = new DicomTag(0x0400, 0x0520);

		/// <summary>(0400,0550) VR=SQ VM=1 Modified Attributes Sequence</summary>
		DicomTags.ModifiedAttributesSequence = new DicomTag(0x0400, 0x0550);

		/// <summary>(0400,0561) VR=SQ VM=1 Original Attributes Sequence</summary>
		DicomTags.OriginalAttributesSequence = new DicomTag(0x0400, 0x0561);

		/// <summary>(0400,0562) VR=DT VM=1 Attribute Modification DateTime</summary>
		DicomTags.AttributeModificationDateTime = new DicomTag(0x0400, 0x0562);

		/// <summary>(0400,0563) VR=LO VM=1 Modifying System</summary>
		DicomTags.ModifyingSystem = new DicomTag(0x0400, 0x0563);

		/// <summary>(0400,0564) VR=LO VM=1 Source of Previous Values</summary>
		DicomTags.SourceOfPreviousValues = new DicomTag(0x0400, 0x0564);

		/// <summary>(0400,0565) VR=CS VM=1 Reason for the Attribute Modification</summary>
		DicomTags.ReasonForTheAttributeModification = new DicomTag(0x0400, 0x0565);

		/// <summary>(1000,0000) VR=US VM=3 Escape Triplet (Retired)</summary>
		DicomTags.EscapeTripletRETIRED = new DicomTag(0x1000, 0x0000);

		/// <summary>(1000,0001) VR=US VM=3 Run Length Triplet (Retired)</summary>
		DicomTags.RunLengthTripletRETIRED = new DicomTag(0x1000, 0x0001);

		/// <summary>(1000,0002) VR=US VM=1 Huffman Table Size (Retired)</summary>
		DicomTags.HuffmanTableSizeRETIRED = new DicomTag(0x1000, 0x0002);

		/// <summary>(1000,0003) VR=US VM=3 Huffman Table Triplet (Retired)</summary>
		DicomTags.HuffmanTableTripletRETIRED = new DicomTag(0x1000, 0x0003);

		/// <summary>(1000,0004) VR=US VM=1 Shift Table Size (Retired)</summary>
		DicomTags.ShiftTableSizeRETIRED = new DicomTag(0x1000, 0x0004);

		/// <summary>(1000,0005) VR=US VM=3 Shift Table Triplet (Retired)</summary>
		DicomTags.ShiftTableTripletRETIRED = new DicomTag(0x1000, 0x0005);

		/// <summary>(1010,0000) VR=US VM=1-n Zonal Map (Retired)</summary>
		DicomTags.ZonalMapRETIRED = new DicomTag(0x1010, 0x0000);

		/// <summary>(2000,0010) VR=IS VM=1 Number of Copies</summary>
		DicomTags.NumberOfCopies = new DicomTag(0x2000, 0x0010);

		/// <summary>(2000,001e) VR=SQ VM=1 Printer Configuration Sequence</summary>
		DicomTags.PrinterConfigurationSequence = new DicomTag(0x2000, 0x001e);

		/// <summary>(2000,0020) VR=CS VM=1 Print Priority</summary>
		DicomTags.PrintPriority = new DicomTag(0x2000, 0x0020);

		/// <summary>(2000,0030) VR=CS VM=1 Medium Type</summary>
		DicomTags.MediumType = new DicomTag(0x2000, 0x0030);

		/// <summary>(2000,0040) VR=CS VM=1 Film Destination</summary>
		DicomTags.FilmDestination = new DicomTag(0x2000, 0x0040);

		/// <summary>(2000,0050) VR=LO VM=1 Film Session Label</summary>
		DicomTags.FilmSessionLabel = new DicomTag(0x2000, 0x0050);

		/// <summary>(2000,0060) VR=IS VM=1 Memory Allocation</summary>
		DicomTags.MemoryAllocation = new DicomTag(0x2000, 0x0060);

		/// <summary>(2000,0061) VR=IS VM=1 Maximum Memory Allocation</summary>
		DicomTags.MaximumMemoryAllocation = new DicomTag(0x2000, 0x0061);

		/// <summary>(2000,0062) VR=CS VM=1 Color Image Printing Flag (Retired)</summary>
		DicomTags.ColorImagePrintingFlagRETIRED = new DicomTag(0x2000, 0x0062);

		/// <summary>(2000,0063) VR=CS VM=1 Collation Flag (Retired)</summary>
		DicomTags.CollationFlagRETIRED = new DicomTag(0x2000, 0x0063);

		/// <summary>(2000,0065) VR=CS VM=1 Annotation Flag (Retired)</summary>
		DicomTags.AnnotationFlagRETIRED = new DicomTag(0x2000, 0x0065);

		/// <summary>(2000,0067) VR=CS VM=1 Image Overlay Flag (Retired)</summary>
		DicomTags.ImageOverlayFlagRETIRED = new DicomTag(0x2000, 0x0067);

		/// <summary>(2000,0069) VR=CS VM=1 Presentation LUT Flag (Retired)</summary>
		DicomTags.PresentationLUTFlagRETIRED = new DicomTag(0x2000, 0x0069);

		/// <summary>(2000,006a) VR=CS VM=1 Image Box Presentation LUT Flag (Retired)</summary>
		DicomTags.ImageBoxPresentationLUTFlagRETIRED = new DicomTag(0x2000, 0x006a);

		/// <summary>(2000,00a0) VR=US VM=1 Memory Bit Depth</summary>
		DicomTags.MemoryBitDepth = new DicomTag(0x2000, 0x00a0);

		/// <summary>(2000,00a1) VR=US VM=1 Printing Bit Depth</summary>
		DicomTags.PrintingBitDepth = new DicomTag(0x2000, 0x00a1);

		/// <summary>(2000,00a2) VR=SQ VM=1 Media Installed Sequence</summary>
		DicomTags.MediaInstalledSequence = new DicomTag(0x2000, 0x00a2);

		/// <summary>(2000,00a4) VR=SQ VM=1 Other Media Available Sequence</summary>
		DicomTags.OtherMediaAvailableSequence = new DicomTag(0x2000, 0x00a4);

		/// <summary>(2000,00a8) VR=SQ VM=1 Supported Image Display Formats Sequence</summary>
		DicomTags.SupportedImageDisplayFormatsSequence = new DicomTag(0x2000, 0x00a8);

		/// <summary>(2000,0500) VR=SQ VM=1 Referenced Film Box Sequence</summary>
		DicomTags.ReferencedFilmBoxSequence = new DicomTag(0x2000, 0x0500);

		/// <summary>(2000,0510) VR=SQ VM=1 Referenced Stored Print Sequence (Retired)</summary>
		DicomTags.ReferencedStoredPrintSequenceRETIRED = new DicomTag(0x2000, 0x0510);

		/// <summary>(2010,0010) VR=ST VM=1 Image Display Format</summary>
		DicomTags.ImageDisplayFormat = new DicomTag(0x2010, 0x0010);

		/// <summary>(2010,0030) VR=CS VM=1 Annotation Display Format ID</summary>
		DicomTags.AnnotationDisplayFormatID = new DicomTag(0x2010, 0x0030);

		/// <summary>(2010,0040) VR=CS VM=1 Film Orientation</summary>
		DicomTags.FilmOrientation = new DicomTag(0x2010, 0x0040);

		/// <summary>(2010,0050) VR=CS VM=1 Film Size ID</summary>
		DicomTags.FilmSizeID = new DicomTag(0x2010, 0x0050);

		/// <summary>(2010,0052) VR=CS VM=1 Printer Resolution ID</summary>
		DicomTags.PrinterResolutionID = new DicomTag(0x2010, 0x0052);

		/// <summary>(2010,0054) VR=CS VM=1 Default Printer Resolution ID</summary>
		DicomTags.DefaultPrinterResolutionID = new DicomTag(0x2010, 0x0054);

		/// <summary>(2010,0060) VR=CS VM=1 Magnification Type</summary>
		DicomTags.MagnificationType = new DicomTag(0x2010, 0x0060);

		/// <summary>(2010,0080) VR=CS VM=1 Smoothing Type</summary>
		DicomTags.SmoothingType = new DicomTag(0x2010, 0x0080);

		/// <summary>(2010,00a6) VR=CS VM=1 Default Magnification Type</summary>
		DicomTags.DefaultMagnificationType = new DicomTag(0x2010, 0x00a6);

		/// <summary>(2010,00a7) VR=CS VM=1-n Other Magnification Types Available</summary>
		DicomTags.OtherMagnificationTypesAvailable = new DicomTag(0x2010, 0x00a7);

		/// <summary>(2010,00a8) VR=CS VM=1 Default Smoothing Type</summary>
		DicomTags.DefaultSmoothingType = new DicomTag(0x2010, 0x00a8);

		/// <summary>(2010,00a9) VR=CS VM=1-n Other Smoothing Types Available</summary>
		DicomTags.OtherSmoothingTypesAvailable = new DicomTag(0x2010, 0x00a9);

		/// <summary>(2010,0100) VR=CS VM=1 Border Density</summary>
		DicomTags.BorderDensity = new DicomTag(0x2010, 0x0100);

		/// <summary>(2010,0110) VR=CS VM=1 Empty Image Density</summary>
		DicomTags.EmptyImageDensity = new DicomTag(0x2010, 0x0110);

		/// <summary>(2010,0120) VR=US VM=1 Min Density</summary>
		DicomTags.MinDensity = new DicomTag(0x2010, 0x0120);

		/// <summary>(2010,0130) VR=US VM=1 Max Density</summary>
		DicomTags.MaxDensity = new DicomTag(0x2010, 0x0130);

		/// <summary>(2010,0140) VR=CS VM=1 Trim</summary>
		DicomTags.Trim = new DicomTag(0x2010, 0x0140);

		/// <summary>(2010,0150) VR=ST VM=1 Configuration Information</summary>
		DicomTags.ConfigurationInformation = new DicomTag(0x2010, 0x0150);

		/// <summary>(2010,0152) VR=LT VM=1 Configuration Information Description</summary>
		DicomTags.ConfigurationInformationDescription = new DicomTag(0x2010, 0x0152);

		/// <summary>(2010,0154) VR=IS VM=1 Maximum Collated Films</summary>
		DicomTags.MaximumCollatedFilms = new DicomTag(0x2010, 0x0154);

		/// <summary>(2010,015e) VR=US VM=1 Illumination</summary>
		DicomTags.Illumination = new DicomTag(0x2010, 0x015e);

		/// <summary>(2010,0160) VR=US VM=1 Reflected Ambient Light</summary>
		DicomTags.ReflectedAmbientLight = new DicomTag(0x2010, 0x0160);

		/// <summary>(2010,0376) VR=DS VM=2 Printer Pixel Spacing</summary>
		DicomTags.PrinterPixelSpacing = new DicomTag(0x2010, 0x0376);

		/// <summary>(2010,0500) VR=SQ VM=1 Referenced Film Session Sequence</summary>
		DicomTags.ReferencedFilmSessionSequence = new DicomTag(0x2010, 0x0500);

		/// <summary>(2010,0510) VR=SQ VM=1 Referenced Image Box Sequence</summary>
		DicomTags.ReferencedImageBoxSequence = new DicomTag(0x2010, 0x0510);

		/// <summary>(2010,0520) VR=SQ VM=1 Referenced Basic Annotation Box Sequence</summary>
		DicomTags.ReferencedBasicAnnotationBoxSequence = new DicomTag(0x2010, 0x0520);

		/// <summary>(2020,0010) VR=US VM=1 Image Box Position</summary>
		DicomTags.ImageBoxPosition = new DicomTag(0x2020, 0x0010);

		/// <summary>(2020,0020) VR=CS VM=1 Polarity</summary>
		DicomTags.Polarity = new DicomTag(0x2020, 0x0020);

		/// <summary>(2020,0030) VR=DS VM=1 Requested Image Size</summary>
		DicomTags.RequestedImageSize = new DicomTag(0x2020, 0x0030);

		/// <summary>(2020,0040) VR=CS VM=1 Requested Decimate/Crop Behavior</summary>
		DicomTags.RequestedDecimateCropBehavior = new DicomTag(0x2020, 0x0040);

		/// <summary>(2020,0050) VR=CS VM=1 Requested Resolution ID</summary>
		DicomTags.RequestedResolutionID = new DicomTag(0x2020, 0x0050);

		/// <summary>(2020,00a0) VR=CS VM=1 Requested Image Size Flag</summary>
		DicomTags.RequestedImageSizeFlag = new DicomTag(0x2020, 0x00a0);

		/// <summary>(2020,00a2) VR=CS VM=1 Decimate/Crop Result</summary>
		DicomTags.DecimateCropResult = new DicomTag(0x2020, 0x00a2);

		/// <summary>(2020,0110) VR=SQ VM=1 Basic Grayscale Image Sequence</summary>
		DicomTags.BasicGrayscaleImageSequence = new DicomTag(0x2020, 0x0110);

		/// <summary>(2020,0111) VR=SQ VM=1 Basic Color Image Sequence</summary>
		DicomTags.BasicColorImageSequence = new DicomTag(0x2020, 0x0111);

		/// <summary>(2020,0130) VR=SQ VM=1 Referenced Image Overlay Box Sequence (Retired)</summary>
		DicomTags.ReferencedImageOverlayBoxSequenceRETIRED = new DicomTag(0x2020, 0x0130);

		/// <summary>(2020,0140) VR=SQ VM=1 Referenced VOI LUT Box Sequence (Retired)</summary>
		DicomTags.ReferencedVOILUTBoxSequenceRETIRED = new DicomTag(0x2020, 0x0140);

		/// <summary>(2030,0010) VR=US VM=1 Annotation Position</summary>
		DicomTags.AnnotationPosition = new DicomTag(0x2030, 0x0010);

		/// <summary>(2030,0020) VR=LO VM=1 Text String</summary>
		DicomTags.TextString = new DicomTag(0x2030, 0x0020);

		/// <summary>(2040,0010) VR=SQ VM=1 Referenced Overlay Plane Sequence (Retired)</summary>
		DicomTags.ReferencedOverlayPlaneSequenceRETIRED = new DicomTag(0x2040, 0x0010);

		/// <summary>(2040,0011) VR=US VM=1-99 Referenced Overlay Plane Groups (Retired)</summary>
		DicomTags.ReferencedOverlayPlaneGroupsRETIRED = new DicomTag(0x2040, 0x0011);

		/// <summary>(2040,0020) VR=SQ VM=1 Overlay Pixel Data Sequence (Retired)</summary>
		DicomTags.OverlayPixelDataSequenceRETIRED = new DicomTag(0x2040, 0x0020);

		/// <summary>(2040,0060) VR=CS VM=1 Overlay Magnification Type (Retired)</summary>
		DicomTags.OverlayMagnificationTypeRETIRED = new DicomTag(0x2040, 0x0060);

		/// <summary>(2040,0070) VR=CS VM=1 Overlay Smoothing Type (Retired)</summary>
		DicomTags.OverlaySmoothingTypeRETIRED = new DicomTag(0x2040, 0x0070);

		/// <summary>(2040,0072) VR=CS VM=1 Overlay or Image Magnification (Retired)</summary>
		DicomTags.OverlayOrImageMagnificationRETIRED = new DicomTag(0x2040, 0x0072);

		/// <summary>(2040,0074) VR=US VM=1 Magnify to Number of Columns (Retired)</summary>
		DicomTags.MagnifyToNumberOfColumnsRETIRED = new DicomTag(0x2040, 0x0074);

		/// <summary>(2040,0080) VR=CS VM=1 Overlay Foreground Density (Retired)</summary>
		DicomTags.OverlayForegroundDensityRETIRED = new DicomTag(0x2040, 0x0080);

		/// <summary>(2040,0082) VR=CS VM=1 Overlay Background Density (Retired)</summary>
		DicomTags.OverlayBackgroundDensityRETIRED = new DicomTag(0x2040, 0x0082);

		/// <summary>(2040,0090) VR=CS VM=1 Overlay Mode (Retired)</summary>
		DicomTags.OverlayModeRETIRED = new DicomTag(0x2040, 0x0090);

		/// <summary>(2040,0100) VR=CS VM=1 Threshold Density (Retired)</summary>
		DicomTags.ThresholdDensityRETIRED = new DicomTag(0x2040, 0x0100);

		/// <summary>(2040,0500) VR=SQ VM=1 Referenced Image Box Sequence (Retired) (Retired)</summary>
		DicomTags.ReferencedImageBoxSequenceRetiredRETIRED = new DicomTag(0x2040, 0x0500);

		/// <summary>(2050,0010) VR=SQ VM=1 Presentation LUT Sequence</summary>
		DicomTags.PresentationLUTSequence = new DicomTag(0x2050, 0x0010);

		/// <summary>(2050,0020) VR=CS VM=1 Presentation LUT Shape</summary>
		DicomTags.PresentationLUTShape = new DicomTag(0x2050, 0x0020);

		/// <summary>(2050,0500) VR=SQ VM=1 Referenced Presentation LUT Sequence</summary>
		DicomTags.ReferencedPresentationLUTSequence = new DicomTag(0x2050, 0x0500);

		/// <summary>(2100,0010) VR=SH VM=1 Print Job ID (Retired)</summary>
		DicomTags.PrintJobIDRETIRED = new DicomTag(0x2100, 0x0010);

		/// <summary>(2100,0020) VR=CS VM=1 Execution Status</summary>
		DicomTags.ExecutionStatus = new DicomTag(0x2100, 0x0020);

		/// <summary>(2100,0030) VR=CS VM=1 Execution Status Info</summary>
		DicomTags.ExecutionStatusInfo = new DicomTag(0x2100, 0x0030);

		/// <summary>(2100,0040) VR=DA VM=1 Creation Date</summary>
		DicomTags.CreationDate = new DicomTag(0x2100, 0x0040);

		/// <summary>(2100,0050) VR=TM VM=1 Creation Time</summary>
		DicomTags.CreationTime = new DicomTag(0x2100, 0x0050);

		/// <summary>(2100,0070) VR=AE VM=1 Originator</summary>
		DicomTags.Originator = new DicomTag(0x2100, 0x0070);

		/// <summary>(2100,0140) VR=AE VM=1 Destination AE (Retired)</summary>
		DicomTags.DestinationAERETIRED = new DicomTag(0x2100, 0x0140);

		/// <summary>(2100,0160) VR=SH VM=1 Owner ID</summary>
		DicomTags.OwnerID = new DicomTag(0x2100, 0x0160);

		/// <summary>(2100,0170) VR=IS VM=1 Number of Films</summary>
		DicomTags.NumberOfFilms = new DicomTag(0x2100, 0x0170);

		/// <summary>(2100,0500) VR=SQ VM=1 Referenced Print Job Sequence (Pull Stored Print) (Retired)</summary>
		DicomTags.ReferencedPrintJobSequencePullStoredPrintRETIRED = new DicomTag(0x2100, 0x0500);

		/// <summary>(2110,0010) VR=CS VM=1 Printer Status</summary>
		DicomTags.PrinterStatus = new DicomTag(0x2110, 0x0010);

		/// <summary>(2110,0020) VR=CS VM=1 Printer Status Info</summary>
		DicomTags.PrinterStatusInfo = new DicomTag(0x2110, 0x0020);

		/// <summary>(2110,0030) VR=LO VM=1 Printer Name</summary>
		DicomTags.PrinterName = new DicomTag(0x2110, 0x0030);

		/// <summary>(2110,0099) VR=SH VM=1 Print Queue ID (Retired)</summary>
		DicomTags.PrintQueueIDRETIRED = new DicomTag(0x2110, 0x0099);

		/// <summary>(2120,0010) VR=CS VM=1 Queue Status (Retired)</summary>
		DicomTags.QueueStatusRETIRED = new DicomTag(0x2120, 0x0010);

		/// <summary>(2120,0050) VR=SQ VM=1 Print Job Description Sequence (Retired)</summary>
		DicomTags.PrintJobDescriptionSequenceRETIRED = new DicomTag(0x2120, 0x0050);

		/// <summary>(2120,0070) VR=SQ VM=1 Referenced Print Job Sequence (Retired)</summary>
		DicomTags.ReferencedPrintJobSequenceRETIRED = new DicomTag(0x2120, 0x0070);

		/// <summary>(2130,0010) VR=SQ VM=1 Print Management Capabilities Sequence (Retired)</summary>
		DicomTags.PrintManagementCapabilitiesSequenceRETIRED = new DicomTag(0x2130, 0x0010);

		/// <summary>(2130,0015) VR=SQ VM=1 Printer Characteristics Sequence (Retired)</summary>
		DicomTags.PrinterCharacteristicsSequenceRETIRED = new DicomTag(0x2130, 0x0015);

		/// <summary>(2130,0030) VR=SQ VM=1 Film Box Content Sequence (Retired)</summary>
		DicomTags.FilmBoxContentSequenceRETIRED = new DicomTag(0x2130, 0x0030);

		/// <summary>(2130,0040) VR=SQ VM=1 Image Box Content Sequence (Retired)</summary>
		DicomTags.ImageBoxContentSequenceRETIRED = new DicomTag(0x2130, 0x0040);

		/// <summary>(2130,0050) VR=SQ VM=1 Annotation Content Sequence (Retired)</summary>
		DicomTags.AnnotationContentSequenceRETIRED = new DicomTag(0x2130, 0x0050);

		/// <summary>(2130,0060) VR=SQ VM=1 Image Overlay Box Content Sequence (Retired)</summary>
		DicomTags.ImageOverlayBoxContentSequenceRETIRED = new DicomTag(0x2130, 0x0060);

		/// <summary>(2130,0080) VR=SQ VM=1 Presentation LUT Content Sequence (Retired)</summary>
		DicomTags.PresentationLUTContentSequenceRETIRED = new DicomTag(0x2130, 0x0080);

		/// <summary>(2130,00a0) VR=SQ VM=1 Proposed Study Sequence (Retired)</summary>
		DicomTags.ProposedStudySequenceRETIRED = new DicomTag(0x2130, 0x00a0);

		/// <summary>(2130,00c0) VR=SQ VM=1 Original Image Sequence (Retired)</summary>
		DicomTags.OriginalImageSequenceRETIRED = new DicomTag(0x2130, 0x00c0);

		/// <summary>(2200,0001) VR=CS VM=1 Label Using Information Extracted From Instances</summary>
		DicomTags.LabelUsingInformationExtractedFromInstances = new DicomTag(0x2200, 0x0001);

		/// <summary>(2200,0002) VR=UT VM=1 Label Text</summary>
		DicomTags.LabelText = new DicomTag(0x2200, 0x0002);

		/// <summary>(2200,0003) VR=CS VM=1 Label Style Selection</summary>
		DicomTags.LabelStyleSelection = new DicomTag(0x2200, 0x0003);

		/// <summary>(2200,0004) VR=LT VM=1 Media Disposition</summary>
		DicomTags.MediaDisposition = new DicomTag(0x2200, 0x0004);

		/// <summary>(2200,0005) VR=LT VM=1 Barcode Value</summary>
		DicomTags.BarcodeValue = new DicomTag(0x2200, 0x0005);

		/// <summary>(2200,0006) VR=CS VM=1 Barcode Symbology</summary>
		DicomTags.BarcodeSymbology = new DicomTag(0x2200, 0x0006);

		/// <summary>(2200,0007) VR=CS VM=1 Allow Media Splitting</summary>
		DicomTags.AllowMediaSplitting = new DicomTag(0x2200, 0x0007);

		/// <summary>(2200,0008) VR=CS VM=1 Include Non-DICOM Objects</summary>
		DicomTags.IncludeNonDICOMObjects = new DicomTag(0x2200, 0x0008);

		/// <summary>(2200,0009) VR=CS VM=1 Include Display Application</summary>
		DicomTags.IncludeDisplayApplication = new DicomTag(0x2200, 0x0009);

		/// <summary>(2200,000a) VR=CS VM=1 Preserve Composite Instances After Media Creation</summary>
		DicomTags.PreserveCompositeInstancesAfterMediaCreation = new DicomTag(0x2200, 0x000a);

		/// <summary>(2200,000b) VR=US VM=1 Total Number of Pieces of Media Created</summary>
		DicomTags.TotalNumberOfPiecesOfMediaCreated = new DicomTag(0x2200, 0x000b);

		/// <summary>(2200,000c) VR=LO VM=1 Requested Media Application Profile</summary>
		DicomTags.RequestedMediaApplicationProfile = new DicomTag(0x2200, 0x000c);

		/// <summary>(2200,000d) VR=SQ VM=1 Referenced Storage Media Sequence</summary>
		DicomTags.ReferencedStorageMediaSequence = new DicomTag(0x2200, 0x000d);

		/// <summary>(2200,000e) VR=AT VM=1-n Failure Attributes</summary>
		DicomTags.FailureAttributes = new DicomTag(0x2200, 0x000e);

		/// <summary>(2200,000f) VR=CS VM=1 Allow Lossy Compression</summary>
		DicomTags.AllowLossyCompression = new DicomTag(0x2200, 0x000f);

		/// <summary>(2200,0020) VR=CS VM=1 Request Priority</summary>
		DicomTags.RequestPriority = new DicomTag(0x2200, 0x0020);

		/// <summary>(3002,0002) VR=SH VM=1 RT Image Label</summary>
		DicomTags.RTImageLabel = new DicomTag(0x3002, 0x0002);

		/// <summary>(3002,0003) VR=LO VM=1 RT Image Name</summary>
		DicomTags.RTImageName = new DicomTag(0x3002, 0x0003);

		/// <summary>(3002,0004) VR=ST VM=1 RT Image Description</summary>
		DicomTags.RTImageDescription = new DicomTag(0x3002, 0x0004);

		/// <summary>(3002,000a) VR=CS VM=1 Reported Values Origin</summary>
		DicomTags.ReportedValuesOrigin = new DicomTag(0x3002, 0x000a);

		/// <summary>(3002,000c) VR=CS VM=1 RT Image Plane</summary>
		DicomTags.RTImagePlane = new DicomTag(0x3002, 0x000c);

		/// <summary>(3002,000d) VR=DS VM=3 X-Ray Image Receptor Translation</summary>
		DicomTags.XRayImageReceptorTranslation = new DicomTag(0x3002, 0x000d);

		/// <summary>(3002,000e) VR=DS VM=1 X-Ray Image Receptor Angle</summary>
		DicomTags.XRayImageReceptorAngle = new DicomTag(0x3002, 0x000e);

		/// <summary>(3002,0010) VR=DS VM=6 RT Image Orientation</summary>
		DicomTags.RTImageOrientation = new DicomTag(0x3002, 0x0010);

		/// <summary>(3002,0011) VR=DS VM=2 Image Plane Pixel Spacing</summary>
		DicomTags.ImagePlanePixelSpacing = new DicomTag(0x3002, 0x0011);

		/// <summary>(3002,0012) VR=DS VM=2 RT Image Position</summary>
		DicomTags.RTImagePosition = new DicomTag(0x3002, 0x0012);

		/// <summary>(3002,0020) VR=SH VM=1 Radiation Machine Name</summary>
		DicomTags.RadiationMachineName = new DicomTag(0x3002, 0x0020);

		/// <summary>(3002,0022) VR=DS VM=1 Radiation Machine SAD</summary>
		DicomTags.RadiationMachineSAD = new DicomTag(0x3002, 0x0022);

		/// <summary>(3002,0024) VR=DS VM=1 Radiation Machine SSD</summary>
		DicomTags.RadiationMachineSSD = new DicomTag(0x3002, 0x0024);

		/// <summary>(3002,0026) VR=DS VM=1 RT Image SID</summary>
		DicomTags.RTImageSID = new DicomTag(0x3002, 0x0026);

		/// <summary>(3002,0028) VR=DS VM=1 Source to Reference Object Distance</summary>
		DicomTags.SourceToReferenceObjectDistance = new DicomTag(0x3002, 0x0028);

		/// <summary>(3002,0029) VR=IS VM=1 Fraction Number</summary>
		DicomTags.FractionNumber = new DicomTag(0x3002, 0x0029);

		/// <summary>(3002,0030) VR=SQ VM=1 Exposure Sequence</summary>
		DicomTags.ExposureSequence = new DicomTag(0x3002, 0x0030);

		/// <summary>(3002,0032) VR=DS VM=1 Meterset Exposure</summary>
		DicomTags.MetersetExposure = new DicomTag(0x3002, 0x0032);

		/// <summary>(3002,0034) VR=DS VM=4 Diaphragm Position</summary>
		DicomTags.DiaphragmPosition = new DicomTag(0x3002, 0x0034);

		/// <summary>(3002,0040) VR=SQ VM=1 Fluence Map Sequence</summary>
		DicomTags.FluenceMapSequence = new DicomTag(0x3002, 0x0040);

		/// <summary>(3002,0041) VR=CS VM=1 Fluence Data Source</summary>
		DicomTags.FluenceDataSource = new DicomTag(0x3002, 0x0041);

		/// <summary>(3002,0042) VR=DS VM=1 Fluence Data Scale</summary>
		DicomTags.FluenceDataScale = new DicomTag(0x3002, 0x0042);

		/// <summary>(3004,0001) VR=CS VM=1 DVH Type</summary>
		DicomTags.DVHType = new DicomTag(0x3004, 0x0001);

		/// <summary>(3004,0002) VR=CS VM=1 Dose Units</summary>
		DicomTags.DoseUnits = new DicomTag(0x3004, 0x0002);

		/// <summary>(3004,0004) VR=CS VM=1 Dose Type</summary>
		DicomTags.DoseType = new DicomTag(0x3004, 0x0004);

		/// <summary>(3004,0006) VR=LO VM=1 Dose Comment</summary>
		DicomTags.DoseComment = new DicomTag(0x3004, 0x0006);

		/// <summary>(3004,0008) VR=DS VM=3 Normalization Point</summary>
		DicomTags.NormalizationPoint = new DicomTag(0x3004, 0x0008);

		/// <summary>(3004,000a) VR=CS VM=1 Dose Summation Type</summary>
		DicomTags.DoseSummationType = new DicomTag(0x3004, 0x000a);

		/// <summary>(3004,000c) VR=DS VM=2-n Grid Frame Offset Vector</summary>
		DicomTags.GridFrameOffsetVector = new DicomTag(0x3004, 0x000c);

		/// <summary>(3004,000e) VR=DS VM=1 Dose Grid Scaling</summary>
		DicomTags.DoseGridScaling = new DicomTag(0x3004, 0x000e);

		/// <summary>(3004,0010) VR=SQ VM=1 RT Dose ROI Sequence</summary>
		DicomTags.RTDoseROISequence = new DicomTag(0x3004, 0x0010);

		/// <summary>(3004,0012) VR=DS VM=1 Dose Value</summary>
		DicomTags.DoseValue = new DicomTag(0x3004, 0x0012);

		/// <summary>(3004,0014) VR=CS VM=1-3 Tissue Heterogeneity Correction</summary>
		DicomTags.TissueHeterogeneityCorrection = new DicomTag(0x3004, 0x0014);

		/// <summary>(3004,0040) VR=DS VM=3 DVH Normalization Point</summary>
		DicomTags.DVHNormalizationPoint = new DicomTag(0x3004, 0x0040);

		/// <summary>(3004,0042) VR=DS VM=1 DVH Normalization Dose Value</summary>
		DicomTags.DVHNormalizationDoseValue = new DicomTag(0x3004, 0x0042);

		/// <summary>(3004,0050) VR=SQ VM=1 DVH Sequence</summary>
		DicomTags.DVHSequence = new DicomTag(0x3004, 0x0050);

		/// <summary>(3004,0052) VR=DS VM=1 DVH Dose Scaling</summary>
		DicomTags.DVHDoseScaling = new DicomTag(0x3004, 0x0052);

		/// <summary>(3004,0054) VR=CS VM=1 DVH Volume Units</summary>
		DicomTags.DVHVolumeUnits = new DicomTag(0x3004, 0x0054);

		/// <summary>(3004,0056) VR=IS VM=1 DVH Number of Bins</summary>
		DicomTags.DVHNumberOfBins = new DicomTag(0x3004, 0x0056);

		/// <summary>(3004,0058) VR=DS VM=2-2n DVH Data</summary>
		DicomTags.DVHData = new DicomTag(0x3004, 0x0058);

		/// <summary>(3004,0060) VR=SQ VM=1 DVH Referenced ROI Sequence</summary>
		DicomTags.DVHReferencedROISequence = new DicomTag(0x3004, 0x0060);

		/// <summary>(3004,0062) VR=CS VM=1 DVH ROI Contribution Type</summary>
		DicomTags.DVHROIContributionType = new DicomTag(0x3004, 0x0062);

		/// <summary>(3004,0070) VR=DS VM=1 DVH Minimum Dose</summary>
		DicomTags.DVHMinimumDose = new DicomTag(0x3004, 0x0070);

		/// <summary>(3004,0072) VR=DS VM=1 DVH Maximum Dose</summary>
		DicomTags.DVHMaximumDose = new DicomTag(0x3004, 0x0072);

		/// <summary>(3004,0074) VR=DS VM=1 DVH Mean Dose</summary>
		DicomTags.DVHMeanDose = new DicomTag(0x3004, 0x0074);

		/// <summary>(3006,0002) VR=SH VM=1 Structure Set Label</summary>
		DicomTags.StructureSetLabel = new DicomTag(0x3006, 0x0002);

		/// <summary>(3006,0004) VR=LO VM=1 Structure Set Name</summary>
		DicomTags.StructureSetName = new DicomTag(0x3006, 0x0004);

		/// <summary>(3006,0006) VR=ST VM=1 Structure Set Description</summary>
		DicomTags.StructureSetDescription = new DicomTag(0x3006, 0x0006);

		/// <summary>(3006,0008) VR=DA VM=1 Structure Set Date</summary>
		DicomTags.StructureSetDate = new DicomTag(0x3006, 0x0008);

		/// <summary>(3006,0009) VR=TM VM=1 Structure Set Time</summary>
		DicomTags.StructureSetTime = new DicomTag(0x3006, 0x0009);

		/// <summary>(3006,0010) VR=SQ VM=1 Referenced Frame of Reference Sequence</summary>
		DicomTags.ReferencedFrameOfReferenceSequence = new DicomTag(0x3006, 0x0010);

		/// <summary>(3006,0012) VR=SQ VM=1 RT Referenced Study Sequence</summary>
		DicomTags.RTReferencedStudySequence = new DicomTag(0x3006, 0x0012);

		/// <summary>(3006,0014) VR=SQ VM=1 RT Referenced Series Sequence</summary>
		DicomTags.RTReferencedSeriesSequence = new DicomTag(0x3006, 0x0014);

		/// <summary>(3006,0016) VR=SQ VM=1 Contour Image Sequence</summary>
		DicomTags.ContourImageSequence = new DicomTag(0x3006, 0x0016);

		/// <summary>(3006,0020) VR=SQ VM=1 Structure Set ROI Sequence</summary>
		DicomTags.StructureSetROISequence = new DicomTag(0x3006, 0x0020);

		/// <summary>(3006,0022) VR=IS VM=1 ROI Number</summary>
		DicomTags.ROINumber = new DicomTag(0x3006, 0x0022);

		/// <summary>(3006,0024) VR=UI VM=1 Referenced Frame of Reference UID</summary>
		DicomTags.ReferencedFrameOfReferenceUID = new DicomTag(0x3006, 0x0024);

		/// <summary>(3006,0026) VR=LO VM=1 ROI Name</summary>
		DicomTags.ROIName = new DicomTag(0x3006, 0x0026);

		/// <summary>(3006,0028) VR=ST VM=1 ROI Description</summary>
		DicomTags.ROIDescription = new DicomTag(0x3006, 0x0028);

		/// <summary>(3006,002a) VR=IS VM=3 ROI Display Color</summary>
		DicomTags.ROIDisplayColor = new DicomTag(0x3006, 0x002a);

		/// <summary>(3006,002c) VR=DS VM=1 ROI Volume</summary>
		DicomTags.ROIVolume = new DicomTag(0x3006, 0x002c);

		/// <summary>(3006,0030) VR=SQ VM=1 RT Related ROI Sequence</summary>
		DicomTags.RTRelatedROISequence = new DicomTag(0x3006, 0x0030);

		/// <summary>(3006,0033) VR=CS VM=1 RT ROI Relationship</summary>
		DicomTags.RTROIRelationship = new DicomTag(0x3006, 0x0033);

		/// <summary>(3006,0036) VR=CS VM=1 ROI Generation Algorithm</summary>
		DicomTags.ROIGenerationAlgorithm = new DicomTag(0x3006, 0x0036);

		/// <summary>(3006,0038) VR=LO VM=1 ROI Generation Description</summary>
		DicomTags.ROIGenerationDescription = new DicomTag(0x3006, 0x0038);

		/// <summary>(3006,0039) VR=SQ VM=1 ROI Contour Sequence</summary>
		DicomTags.ROIContourSequence = new DicomTag(0x3006, 0x0039);

		/// <summary>(3006,0040) VR=SQ VM=1 Contour Sequence</summary>
		DicomTags.ContourSequence = new DicomTag(0x3006, 0x0040);

		/// <summary>(3006,0042) VR=CS VM=1 Contour Geometric Type</summary>
		DicomTags.ContourGeometricType = new DicomTag(0x3006, 0x0042);

		/// <summary>(3006,0044) VR=DS VM=1 Contour Slab Thickness</summary>
		DicomTags.ContourSlabThickness = new DicomTag(0x3006, 0x0044);

		/// <summary>(3006,0045) VR=DS VM=3 Contour Offset Vector</summary>
		DicomTags.ContourOffsetVector = new DicomTag(0x3006, 0x0045);

		/// <summary>(3006,0046) VR=IS VM=1 Number of Contour Points</summary>
		DicomTags.NumberOfContourPoints = new DicomTag(0x3006, 0x0046);

		/// <summary>(3006,0048) VR=IS VM=1 Contour Number</summary>
		DicomTags.ContourNumber = new DicomTag(0x3006, 0x0048);

		/// <summary>(3006,0049) VR=IS VM=1-n Attached Contours</summary>
		DicomTags.AttachedContours = new DicomTag(0x3006, 0x0049);

		/// <summary>(3006,0050) VR=DS VM=3-3n Contour Data</summary>
		DicomTags.ContourData = new DicomTag(0x3006, 0x0050);

		/// <summary>(3006,0080) VR=SQ VM=1 RT ROI Observations Sequence</summary>
		DicomTags.RTROIObservationsSequence = new DicomTag(0x3006, 0x0080);

		/// <summary>(3006,0082) VR=IS VM=1 Observation Number</summary>
		DicomTags.ObservationNumber = new DicomTag(0x3006, 0x0082);

		/// <summary>(3006,0084) VR=IS VM=1 Referenced ROI Number</summary>
		DicomTags.ReferencedROINumber = new DicomTag(0x3006, 0x0084);

		/// <summary>(3006,0085) VR=SH VM=1 ROI Observation Label</summary>
		DicomTags.ROIObservationLabel = new DicomTag(0x3006, 0x0085);

		/// <summary>(3006,0086) VR=SQ VM=1 RT ROI Identification Code Sequence</summary>
		DicomTags.RTROIIdentificationCodeSequence = new DicomTag(0x3006, 0x0086);

		/// <summary>(3006,0088) VR=ST VM=1 ROI Observation Description</summary>
		DicomTags.ROIObservationDescription = new DicomTag(0x3006, 0x0088);

		/// <summary>(3006,00a0) VR=SQ VM=1 Related RT ROI Observations Sequence</summary>
		DicomTags.RelatedRTROIObservationsSequence = new DicomTag(0x3006, 0x00a0);

		/// <summary>(3006,00a4) VR=CS VM=1 RT ROI Interpreted Type</summary>
		DicomTags.RTROIInterpretedType = new DicomTag(0x3006, 0x00a4);

		/// <summary>(3006,00a6) VR=PN VM=1 ROI Interpreter</summary>
		DicomTags.ROIInterpreter = new DicomTag(0x3006, 0x00a6);

		/// <summary>(3006,00b0) VR=SQ VM=1 ROI Physical Properties Sequence</summary>
		DicomTags.ROIPhysicalPropertiesSequence = new DicomTag(0x3006, 0x00b0);

		/// <summary>(3006,00b2) VR=CS VM=1 ROI Physical Property</summary>
		DicomTags.ROIPhysicalProperty = new DicomTag(0x3006, 0x00b2);

		/// <summary>(3006,00b4) VR=DS VM=1 ROI Physical Property Value</summary>
		DicomTags.ROIPhysicalPropertyValue = new DicomTag(0x3006, 0x00b4);

		/// <summary>(3006,00b6) VR=SQ VM=1 ROI Elemental Composition Sequence</summary>
		DicomTags.ROIElementalCompositionSequence = new DicomTag(0x3006, 0x00b6);

		/// <summary>(3006,00b7) VR=US VM=1 ROI Elemental Composition Atomic Number</summary>
		DicomTags.ROIElementalCompositionAtomicNumber = new DicomTag(0x3006, 0x00b7);

		/// <summary>(3006,00b8) VR=FL VM=1 ROI Elemental Composition Atomic Mass Fraction</summary>
		DicomTags.ROIElementalCompositionAtomicMassFraction = new DicomTag(0x3006, 0x00b8);

		/// <summary>(3006,00c0) VR=SQ VM=1 Frame of Reference Relationship Sequence</summary>
		DicomTags.FrameOfReferenceRelationshipSequence = new DicomTag(0x3006, 0x00c0);

		/// <summary>(3006,00c2) VR=UI VM=1 Related Frame of Reference UID</summary>
		DicomTags.RelatedFrameOfReferenceUID = new DicomTag(0x3006, 0x00c2);

		/// <summary>(3006,00c4) VR=CS VM=1 Frame of Reference Transformation Type</summary>
		DicomTags.FrameOfReferenceTransformationType = new DicomTag(0x3006, 0x00c4);

		/// <summary>(3006,00c6) VR=DS VM=16 Frame of Reference Transformation Matrix</summary>
		DicomTags.FrameOfReferenceTransformationMatrix = new DicomTag(0x3006, 0x00c6);

		/// <summary>(3006,00c8) VR=LO VM=1 Frame of Reference Transformation Comment</summary>
		DicomTags.FrameOfReferenceTransformationComment = new DicomTag(0x3006, 0x00c8);

		/// <summary>(3008,0010) VR=SQ VM=1 Measured Dose Reference Sequence</summary>
		DicomTags.MeasuredDoseReferenceSequence = new DicomTag(0x3008, 0x0010);

		/// <summary>(3008,0012) VR=ST VM=1 Measured Dose Description</summary>
		DicomTags.MeasuredDoseDescription = new DicomTag(0x3008, 0x0012);

		/// <summary>(3008,0014) VR=CS VM=1 Measured Dose Type</summary>
		DicomTags.MeasuredDoseType = new DicomTag(0x3008, 0x0014);

		/// <summary>(3008,0016) VR=DS VM=1 Measured Dose Value</summary>
		DicomTags.MeasuredDoseValue = new DicomTag(0x3008, 0x0016);

		/// <summary>(3008,0020) VR=SQ VM=1 Treatment Session Beam Sequence</summary>
		DicomTags.TreatmentSessionBeamSequence = new DicomTag(0x3008, 0x0020);

		/// <summary>(3008,0021) VR=SQ VM=1 Treatment Session Ion Beam Sequence</summary>
		DicomTags.TreatmentSessionIonBeamSequence = new DicomTag(0x3008, 0x0021);

		/// <summary>(3008,0022) VR=IS VM=1 Current Fraction Number</summary>
		DicomTags.CurrentFractionNumber = new DicomTag(0x3008, 0x0022);

		/// <summary>(3008,0024) VR=DA VM=1 Treatment Control Point Date</summary>
		DicomTags.TreatmentControlPointDate = new DicomTag(0x3008, 0x0024);

		/// <summary>(3008,0025) VR=TM VM=1 Treatment Control Point Time</summary>
		DicomTags.TreatmentControlPointTime = new DicomTag(0x3008, 0x0025);

		/// <summary>(3008,002a) VR=CS VM=1 Treatment Termination Status</summary>
		DicomTags.TreatmentTerminationStatus = new DicomTag(0x3008, 0x002a);

		/// <summary>(3008,002b) VR=SH VM=1 Treatment Termination Code</summary>
		DicomTags.TreatmentTerminationCode = new DicomTag(0x3008, 0x002b);

		/// <summary>(3008,002c) VR=CS VM=1 Treatment Verification Status</summary>
		DicomTags.TreatmentVerificationStatus = new DicomTag(0x3008, 0x002c);

		/// <summary>(3008,0030) VR=SQ VM=1 Referenced Treatment Record Sequence</summary>
		DicomTags.ReferencedTreatmentRecordSequence = new DicomTag(0x3008, 0x0030);

		/// <summary>(3008,0032) VR=DS VM=1 Specified Primary Meterset</summary>
		DicomTags.SpecifiedPrimaryMeterset = new DicomTag(0x3008, 0x0032);

		/// <summary>(3008,0033) VR=DS VM=1 Specified Secondary Meterset</summary>
		DicomTags.SpecifiedSecondaryMeterset = new DicomTag(0x3008, 0x0033);

		/// <summary>(3008,0036) VR=DS VM=1 Delivered Primary Meterset</summary>
		DicomTags.DeliveredPrimaryMeterset = new DicomTag(0x3008, 0x0036);

		/// <summary>(3008,0037) VR=DS VM=1 Delivered Secondary Meterset</summary>
		DicomTags.DeliveredSecondaryMeterset = new DicomTag(0x3008, 0x0037);

		/// <summary>(3008,003a) VR=DS VM=1 Specified Treatment Time</summary>
		DicomTags.SpecifiedTreatmentTime = new DicomTag(0x3008, 0x003a);

		/// <summary>(3008,003b) VR=DS VM=1 Delivered Treatment Time</summary>
		DicomTags.DeliveredTreatmentTime = new DicomTag(0x3008, 0x003b);

		/// <summary>(3008,0040) VR=SQ VM=1 Control Point Delivery Sequence</summary>
		DicomTags.ControlPointDeliverySequence = new DicomTag(0x3008, 0x0040);

		/// <summary>(3008,0041) VR=SQ VM=1 Ion Control Point Delivery Sequence</summary>
		DicomTags.IonControlPointDeliverySequence = new DicomTag(0x3008, 0x0041);

		/// <summary>(3008,0042) VR=DS VM=1 Specified Meterset</summary>
		DicomTags.SpecifiedMeterset = new DicomTag(0x3008, 0x0042);

		/// <summary>(3008,0044) VR=DS VM=1 Delivered Meterset</summary>
		DicomTags.DeliveredMeterset = new DicomTag(0x3008, 0x0044);

		/// <summary>(3008,0045) VR=FL VM=1 Meterset Rate Set</summary>
		DicomTags.MetersetRateSet = new DicomTag(0x3008, 0x0045);

		/// <summary>(3008,0046) VR=FL VM=1 Meterset Rate Delivered</summary>
		DicomTags.MetersetRateDelivered = new DicomTag(0x3008, 0x0046);

		/// <summary>(3008,0047) VR=FL VM=1-n Scan Spot Metersets Delivered</summary>
		DicomTags.ScanSpotMetersetsDelivered = new DicomTag(0x3008, 0x0047);

		/// <summary>(3008,0048) VR=DS VM=1 Dose Rate Delivered</summary>
		DicomTags.DoseRateDelivered = new DicomTag(0x3008, 0x0048);

		/// <summary>(3008,0050) VR=SQ VM=1 Treatment Summary Calculated Dose Reference Sequence</summary>
		DicomTags.TreatmentSummaryCalculatedDoseReferenceSequence = new DicomTag(0x3008, 0x0050);

		/// <summary>(3008,0052) VR=DS VM=1 Cumulative Dose to Dose Reference</summary>
		DicomTags.CumulativeDoseToDoseReference = new DicomTag(0x3008, 0x0052);

		/// <summary>(3008,0054) VR=DA VM=1 First Treatment Date</summary>
		DicomTags.FirstTreatmentDate = new DicomTag(0x3008, 0x0054);

		/// <summary>(3008,0056) VR=DA VM=1 Most Recent Treatment Date</summary>
		DicomTags.MostRecentTreatmentDate = new DicomTag(0x3008, 0x0056);

		/// <summary>(3008,005a) VR=IS VM=1 Number of Fractions Delivered</summary>
		DicomTags.NumberOfFractionsDelivered = new DicomTag(0x3008, 0x005a);

		/// <summary>(3008,0060) VR=SQ VM=1 Override Sequence</summary>
		DicomTags.OverrideSequence = new DicomTag(0x3008, 0x0060);

		/// <summary>(3008,0061) VR=AT VM=1 Parameter Sequence Pointer</summary>
		DicomTags.ParameterSequencePointer = new DicomTag(0x3008, 0x0061);

		/// <summary>(3008,0062) VR=AT VM=1 Override Parameter Pointer</summary>
		DicomTags.OverrideParameterPointer = new DicomTag(0x3008, 0x0062);

		/// <summary>(3008,0063) VR=IS VM=1 Parameter Item Index</summary>
		DicomTags.ParameterItemIndex = new DicomTag(0x3008, 0x0063);

		/// <summary>(3008,0064) VR=IS VM=1 Measured Dose Reference Number</summary>
		DicomTags.MeasuredDoseReferenceNumber = new DicomTag(0x3008, 0x0064);

		/// <summary>(3008,0065) VR=AT VM=1 Parameter Pointer</summary>
		DicomTags.ParameterPointer = new DicomTag(0x3008, 0x0065);

		/// <summary>(3008,0066) VR=ST VM=1 Override Reason</summary>
		DicomTags.OverrideReason = new DicomTag(0x3008, 0x0066);

		/// <summary>(3008,0068) VR=SQ VM=1 Corrected Parameter Sequence</summary>
		DicomTags.CorrectedParameterSequence = new DicomTag(0x3008, 0x0068);

		/// <summary>(3008,006a) VR=FL VM=1 Correction Value</summary>
		DicomTags.CorrectionValue = new DicomTag(0x3008, 0x006a);

		/// <summary>(3008,0070) VR=SQ VM=1 Calculated Dose Reference Sequence</summary>
		DicomTags.CalculatedDoseReferenceSequence = new DicomTag(0x3008, 0x0070);

		/// <summary>(3008,0072) VR=IS VM=1 Calculated Dose Reference Number</summary>
		DicomTags.CalculatedDoseReferenceNumber = new DicomTag(0x3008, 0x0072);

		/// <summary>(3008,0074) VR=ST VM=1 Calculated Dose Reference Description</summary>
		DicomTags.CalculatedDoseReferenceDescription = new DicomTag(0x3008, 0x0074);

		/// <summary>(3008,0076) VR=DS VM=1 Calculated Dose Reference Dose Value</summary>
		DicomTags.CalculatedDoseReferenceDoseValue = new DicomTag(0x3008, 0x0076);

		/// <summary>(3008,0078) VR=DS VM=1 Start Meterset</summary>
		DicomTags.StartMeterset = new DicomTag(0x3008, 0x0078);

		/// <summary>(3008,007a) VR=DS VM=1 End Meterset</summary>
		DicomTags.EndMeterset = new DicomTag(0x3008, 0x007a);

		/// <summary>(3008,0080) VR=SQ VM=1 Referenced Measured Dose Reference Sequence</summary>
		DicomTags.ReferencedMeasuredDoseReferenceSequence = new DicomTag(0x3008, 0x0080);

		/// <summary>(3008,0082) VR=IS VM=1 Referenced Measured Dose Reference Number</summary>
		DicomTags.ReferencedMeasuredDoseReferenceNumber = new DicomTag(0x3008, 0x0082);

		/// <summary>(3008,0090) VR=SQ VM=1 Referenced Calculated Dose Reference Sequence</summary>
		DicomTags.ReferencedCalculatedDoseReferenceSequence = new DicomTag(0x3008, 0x0090);

		/// <summary>(3008,0092) VR=IS VM=1 Referenced Calculated Dose Reference Number</summary>
		DicomTags.ReferencedCalculatedDoseReferenceNumber = new DicomTag(0x3008, 0x0092);

		/// <summary>(3008,00a0) VR=SQ VM=1 Beam Limiting Device Leaf Pairs Sequence</summary>
		DicomTags.BeamLimitingDeviceLeafPairsSequence = new DicomTag(0x3008, 0x00a0);

		/// <summary>(3008,00b0) VR=SQ VM=1 Recorded Wedge Sequence</summary>
		DicomTags.RecordedWedgeSequence = new DicomTag(0x3008, 0x00b0);

		/// <summary>(3008,00c0) VR=SQ VM=1 Recorded Compensator Sequence</summary>
		DicomTags.RecordedCompensatorSequence = new DicomTag(0x3008, 0x00c0);

		/// <summary>(3008,00d0) VR=SQ VM=1 Recorded Block Sequence</summary>
		DicomTags.RecordedBlockSequence = new DicomTag(0x3008, 0x00d0);

		/// <summary>(3008,00e0) VR=SQ VM=1 Treatment Summary Measured Dose Reference Sequence</summary>
		DicomTags.TreatmentSummaryMeasuredDoseReferenceSequence = new DicomTag(0x3008, 0x00e0);

		/// <summary>(3008,00f0) VR=SQ VM=1 Recorded Snout Sequence</summary>
		DicomTags.RecordedSnoutSequence = new DicomTag(0x3008, 0x00f0);

		/// <summary>(3008,00f2) VR=SQ VM=1 Recorded Range Shifter Sequence</summary>
		DicomTags.RecordedRangeShifterSequence = new DicomTag(0x3008, 0x00f2);

		/// <summary>(3008,00f4) VR=SQ VM=1 Recorded Lateral Spreading Device Sequence</summary>
		DicomTags.RecordedLateralSpreadingDeviceSequence = new DicomTag(0x3008, 0x00f4);

		/// <summary>(3008,00f6) VR=SQ VM=1 Recorded Range Modulator Sequence</summary>
		DicomTags.RecordedRangeModulatorSequence = new DicomTag(0x3008, 0x00f6);

		/// <summary>(3008,0100) VR=SQ VM=1 Recorded Source Sequence</summary>
		DicomTags.RecordedSourceSequence = new DicomTag(0x3008, 0x0100);

		/// <summary>(3008,0105) VR=LO VM=1 Source Serial Number</summary>
		DicomTags.SourceSerialNumber = new DicomTag(0x3008, 0x0105);

		/// <summary>(3008,0110) VR=SQ VM=1 Treatment Session Application Setup Sequence</summary>
		DicomTags.TreatmentSessionApplicationSetupSequence = new DicomTag(0x3008, 0x0110);

		/// <summary>(3008,0116) VR=CS VM=1 Application Setup Check</summary>
		DicomTags.ApplicationSetupCheck = new DicomTag(0x3008, 0x0116);

		/// <summary>(3008,0120) VR=SQ VM=1 Recorded Brachy Accessory Device Sequence</summary>
		DicomTags.RecordedBrachyAccessoryDeviceSequence = new DicomTag(0x3008, 0x0120);

		/// <summary>(3008,0122) VR=IS VM=1 Referenced Brachy Accessory Device Number</summary>
		DicomTags.ReferencedBrachyAccessoryDeviceNumber = new DicomTag(0x3008, 0x0122);

		/// <summary>(3008,0130) VR=SQ VM=1 Recorded Channel Sequence</summary>
		DicomTags.RecordedChannelSequence = new DicomTag(0x3008, 0x0130);

		/// <summary>(3008,0132) VR=DS VM=1 Specified Channel Total Time</summary>
		DicomTags.SpecifiedChannelTotalTime = new DicomTag(0x3008, 0x0132);

		/// <summary>(3008,0134) VR=DS VM=1 Delivered Channel Total Time</summary>
		DicomTags.DeliveredChannelTotalTime = new DicomTag(0x3008, 0x0134);

		/// <summary>(3008,0136) VR=IS VM=1 Specified Number of Pulses</summary>
		DicomTags.SpecifiedNumberOfPulses = new DicomTag(0x3008, 0x0136);

		/// <summary>(3008,0138) VR=IS VM=1 Delivered Number of Pulses</summary>
		DicomTags.DeliveredNumberOfPulses = new DicomTag(0x3008, 0x0138);

		/// <summary>(3008,013a) VR=DS VM=1 Specified Pulse Repetition Interval</summary>
		DicomTags.SpecifiedPulseRepetitionInterval = new DicomTag(0x3008, 0x013a);

		/// <summary>(3008,013c) VR=DS VM=1 Delivered Pulse Repetition Interval</summary>
		DicomTags.DeliveredPulseRepetitionInterval = new DicomTag(0x3008, 0x013c);

		/// <summary>(3008,0140) VR=SQ VM=1 Recorded Source Applicator Sequence</summary>
		DicomTags.RecordedSourceApplicatorSequence = new DicomTag(0x3008, 0x0140);

		/// <summary>(3008,0142) VR=IS VM=1 Referenced Source Applicator Number</summary>
		DicomTags.ReferencedSourceApplicatorNumber = new DicomTag(0x3008, 0x0142);

		/// <summary>(3008,0150) VR=SQ VM=1 Recorded Channel Shield Sequence</summary>
		DicomTags.RecordedChannelShieldSequence = new DicomTag(0x3008, 0x0150);

		/// <summary>(3008,0152) VR=IS VM=1 Referenced Channel Shield Number</summary>
		DicomTags.ReferencedChannelShieldNumber = new DicomTag(0x3008, 0x0152);

		/// <summary>(3008,0160) VR=SQ VM=1 Brachy Control Point Delivered Sequence</summary>
		DicomTags.BrachyControlPointDeliveredSequence = new DicomTag(0x3008, 0x0160);

		/// <summary>(3008,0162) VR=DA VM=1 Safe Position Exit Date</summary>
		DicomTags.SafePositionExitDate = new DicomTag(0x3008, 0x0162);

		/// <summary>(3008,0164) VR=TM VM=1 Safe Position Exit Time</summary>
		DicomTags.SafePositionExitTime = new DicomTag(0x3008, 0x0164);

		/// <summary>(3008,0166) VR=DA VM=1 Safe Position Return Date</summary>
		DicomTags.SafePositionReturnDate = new DicomTag(0x3008, 0x0166);

		/// <summary>(3008,0168) VR=TM VM=1 Safe Position Return Time</summary>
		DicomTags.SafePositionReturnTime = new DicomTag(0x3008, 0x0168);

		/// <summary>(3008,0200) VR=CS VM=1 Current Treatment Status</summary>
		DicomTags.CurrentTreatmentStatus = new DicomTag(0x3008, 0x0200);

		/// <summary>(3008,0202) VR=ST VM=1 Treatment Status Comment</summary>
		DicomTags.TreatmentStatusComment = new DicomTag(0x3008, 0x0202);

		/// <summary>(3008,0220) VR=SQ VM=1 Fraction Group Summary Sequence</summary>
		DicomTags.FractionGroupSummarySequence = new DicomTag(0x3008, 0x0220);

		/// <summary>(3008,0223) VR=IS VM=1 Referenced Fraction Number</summary>
		DicomTags.ReferencedFractionNumber = new DicomTag(0x3008, 0x0223);

		/// <summary>(3008,0224) VR=CS VM=1 Fraction Group Type</summary>
		DicomTags.FractionGroupType = new DicomTag(0x3008, 0x0224);

		/// <summary>(3008,0230) VR=CS VM=1 Beam Stopper Position</summary>
		DicomTags.BeamStopperPosition = new DicomTag(0x3008, 0x0230);

		/// <summary>(3008,0240) VR=SQ VM=1 Fraction Status Summary Sequence</summary>
		DicomTags.FractionStatusSummarySequence = new DicomTag(0x3008, 0x0240);

		/// <summary>(3008,0250) VR=DA VM=1 Treatment Date</summary>
		DicomTags.TreatmentDate = new DicomTag(0x3008, 0x0250);

		/// <summary>(3008,0251) VR=TM VM=1 Treatment Time</summary>
		DicomTags.TreatmentTime = new DicomTag(0x3008, 0x0251);

		/// <summary>(300a,0002) VR=SH VM=1 RT Plan Label</summary>
		DicomTags.RTPlanLabel = new DicomTag(0x300a, 0x0002);

		/// <summary>(300a,0003) VR=LO VM=1 RT Plan Name</summary>
		DicomTags.RTPlanName = new DicomTag(0x300a, 0x0003);

		/// <summary>(300a,0004) VR=ST VM=1 RT Plan Description</summary>
		DicomTags.RTPlanDescription = new DicomTag(0x300a, 0x0004);

		/// <summary>(300a,0006) VR=DA VM=1 RT Plan Date</summary>
		DicomTags.RTPlanDate = new DicomTag(0x300a, 0x0006);

		/// <summary>(300a,0007) VR=TM VM=1 RT Plan Time</summary>
		DicomTags.RTPlanTime = new DicomTag(0x300a, 0x0007);

		/// <summary>(300a,0009) VR=LO VM=1-n Treatment Protocols</summary>
		DicomTags.TreatmentProtocols = new DicomTag(0x300a, 0x0009);

		/// <summary>(300a,000a) VR=CS VM=1 Plan Intent</summary>
		DicomTags.PlanIntent = new DicomTag(0x300a, 0x000a);

		/// <summary>(300a,000b) VR=LO VM=1-n Treatment Sites</summary>
		DicomTags.TreatmentSites = new DicomTag(0x300a, 0x000b);

		/// <summary>(300a,000c) VR=CS VM=1 RT Plan Geometry</summary>
		DicomTags.RTPlanGeometry = new DicomTag(0x300a, 0x000c);

		/// <summary>(300a,000e) VR=ST VM=1 Prescription Description</summary>
		DicomTags.PrescriptionDescription = new DicomTag(0x300a, 0x000e);

		/// <summary>(300a,0010) VR=SQ VM=1 Dose Reference Sequence</summary>
		DicomTags.DoseReferenceSequence = new DicomTag(0x300a, 0x0010);

		/// <summary>(300a,0012) VR=IS VM=1 Dose Reference Number</summary>
		DicomTags.DoseReferenceNumber = new DicomTag(0x300a, 0x0012);

		/// <summary>(300a,0013) VR=UI VM=1 Dose Reference UID</summary>
		DicomTags.DoseReferenceUID = new DicomTag(0x300a, 0x0013);

		/// <summary>(300a,0014) VR=CS VM=1 Dose Reference Structure Type</summary>
		DicomTags.DoseReferenceStructureType = new DicomTag(0x300a, 0x0014);

		/// <summary>(300a,0015) VR=CS VM=1 Nominal Beam Energy Unit</summary>
		DicomTags.NominalBeamEnergyUnit = new DicomTag(0x300a, 0x0015);

		/// <summary>(300a,0016) VR=LO VM=1 Dose Reference Description</summary>
		DicomTags.DoseReferenceDescription = new DicomTag(0x300a, 0x0016);

		/// <summary>(300a,0018) VR=DS VM=3 Dose Reference Point Coordinates</summary>
		DicomTags.DoseReferencePointCoordinates = new DicomTag(0x300a, 0x0018);

		/// <summary>(300a,001a) VR=DS VM=1 Nominal Prior Dose</summary>
		DicomTags.NominalPriorDose = new DicomTag(0x300a, 0x001a);

		/// <summary>(300a,0020) VR=CS VM=1 Dose Reference Type</summary>
		DicomTags.DoseReferenceType = new DicomTag(0x300a, 0x0020);

		/// <summary>(300a,0021) VR=DS VM=1 Constraint Weight</summary>
		DicomTags.ConstraintWeight = new DicomTag(0x300a, 0x0021);

		/// <summary>(300a,0022) VR=DS VM=1 Delivery Warning Dose</summary>
		DicomTags.DeliveryWarningDose = new DicomTag(0x300a, 0x0022);

		/// <summary>(300a,0023) VR=DS VM=1 Delivery Maximum Dose</summary>
		DicomTags.DeliveryMaximumDose = new DicomTag(0x300a, 0x0023);

		/// <summary>(300a,0025) VR=DS VM=1 Target Minimum Dose</summary>
		DicomTags.TargetMinimumDose = new DicomTag(0x300a, 0x0025);

		/// <summary>(300a,0026) VR=DS VM=1 Target Prescription Dose</summary>
		DicomTags.TargetPrescriptionDose = new DicomTag(0x300a, 0x0026);

		/// <summary>(300a,0027) VR=DS VM=1 Target Maximum Dose</summary>
		DicomTags.TargetMaximumDose = new DicomTag(0x300a, 0x0027);

		/// <summary>(300a,0028) VR=DS VM=1 Target Underdose Volume Fraction</summary>
		DicomTags.TargetUnderdoseVolumeFraction = new DicomTag(0x300a, 0x0028);

		/// <summary>(300a,002a) VR=DS VM=1 Organ at Risk Full-volume Dose</summary>
		DicomTags.OrganAtRiskFullvolumeDose = new DicomTag(0x300a, 0x002a);

		/// <summary>(300a,002b) VR=DS VM=1 Organ at Risk Limit Dose</summary>
		DicomTags.OrganAtRiskLimitDose = new DicomTag(0x300a, 0x002b);

		/// <summary>(300a,002c) VR=DS VM=1 Organ at Risk Maximum Dose</summary>
		DicomTags.OrganAtRiskMaximumDose = new DicomTag(0x300a, 0x002c);

		/// <summary>(300a,002d) VR=DS VM=1 Organ at Risk Overdose Volume Fraction</summary>
		DicomTags.OrganAtRiskOverdoseVolumeFraction = new DicomTag(0x300a, 0x002d);

		/// <summary>(300a,0040) VR=SQ VM=1 Tolerance Table Sequence</summary>
		DicomTags.ToleranceTableSequence = new DicomTag(0x300a, 0x0040);

		/// <summary>(300a,0042) VR=IS VM=1 Tolerance Table Number</summary>
		DicomTags.ToleranceTableNumber = new DicomTag(0x300a, 0x0042);

		/// <summary>(300a,0043) VR=SH VM=1 Tolerance Table Label</summary>
		DicomTags.ToleranceTableLabel = new DicomTag(0x300a, 0x0043);

		/// <summary>(300a,0044) VR=DS VM=1 Gantry Angle Tolerance</summary>
		DicomTags.GantryAngleTolerance = new DicomTag(0x300a, 0x0044);

		/// <summary>(300a,0046) VR=DS VM=1 Beam Limiting Device Angle Tolerance</summary>
		DicomTags.BeamLimitingDeviceAngleTolerance = new DicomTag(0x300a, 0x0046);

		/// <summary>(300a,0048) VR=SQ VM=1 Beam Limiting Device Tolerance Sequence</summary>
		DicomTags.BeamLimitingDeviceToleranceSequence = new DicomTag(0x300a, 0x0048);

		/// <summary>(300a,004a) VR=DS VM=1 Beam Limiting Device Position Tolerance</summary>
		DicomTags.BeamLimitingDevicePositionTolerance = new DicomTag(0x300a, 0x004a);

		/// <summary>(300a,004b) VR=FL VM=1 Snout Position Tolerance</summary>
		DicomTags.SnoutPositionTolerance = new DicomTag(0x300a, 0x004b);

		/// <summary>(300a,004c) VR=DS VM=1 Patient Support Angle Tolerance</summary>
		DicomTags.PatientSupportAngleTolerance = new DicomTag(0x300a, 0x004c);

		/// <summary>(300a,004e) VR=DS VM=1 Table Top Eccentric Angle Tolerance</summary>
		DicomTags.TableTopEccentricAngleTolerance = new DicomTag(0x300a, 0x004e);

		/// <summary>(300a,004f) VR=FL VM=1 Table Top Pitch Angle Tolerance</summary>
		DicomTags.TableTopPitchAngleTolerance = new DicomTag(0x300a, 0x004f);

		/// <summary>(300a,0050) VR=FL VM=1 Table Top Roll Angle Tolerance</summary>
		DicomTags.TableTopRollAngleTolerance = new DicomTag(0x300a, 0x0050);

		/// <summary>(300a,0051) VR=DS VM=1 Table Top Vertical Position Tolerance</summary>
		DicomTags.TableTopVerticalPositionTolerance = new DicomTag(0x300a, 0x0051);

		/// <summary>(300a,0052) VR=DS VM=1 Table Top Longitudinal Position Tolerance</summary>
		DicomTags.TableTopLongitudinalPositionTolerance = new DicomTag(0x300a, 0x0052);

		/// <summary>(300a,0053) VR=DS VM=1 Table Top Lateral Position Tolerance</summary>
		DicomTags.TableTopLateralPositionTolerance = new DicomTag(0x300a, 0x0053);

		/// <summary>(300a,0055) VR=CS VM=1 RT Plan Relationship</summary>
		DicomTags.RTPlanRelationship = new DicomTag(0x300a, 0x0055);

		/// <summary>(300a,0070) VR=SQ VM=1 Fraction Group Sequence</summary>
		DicomTags.FractionGroupSequence = new DicomTag(0x300a, 0x0070);

		/// <summary>(300a,0071) VR=IS VM=1 Fraction Group Number</summary>
		DicomTags.FractionGroupNumber = new DicomTag(0x300a, 0x0071);

		/// <summary>(300a,0072) VR=LO VM=1 Fraction Group Description</summary>
		DicomTags.FractionGroupDescription = new DicomTag(0x300a, 0x0072);

		/// <summary>(300a,0078) VR=IS VM=1 Number of Fractions Planned</summary>
		DicomTags.NumberOfFractionsPlanned = new DicomTag(0x300a, 0x0078);

		/// <summary>(300a,0079) VR=IS VM=1 Number of Fraction Pattern Digits Per Day</summary>
		DicomTags.NumberOfFractionPatternDigitsPerDay = new DicomTag(0x300a, 0x0079);

		/// <summary>(300a,007a) VR=IS VM=1 Repeat Fraction Cycle Length</summary>
		DicomTags.RepeatFractionCycleLength = new DicomTag(0x300a, 0x007a);

		/// <summary>(300a,007b) VR=LT VM=1 Fraction Pattern</summary>
		DicomTags.FractionPattern = new DicomTag(0x300a, 0x007b);

		/// <summary>(300a,0080) VR=IS VM=1 Number of Beams</summary>
		DicomTags.NumberOfBeams = new DicomTag(0x300a, 0x0080);

		/// <summary>(300a,0082) VR=DS VM=3 Beam Dose Specification Point</summary>
		DicomTags.BeamDoseSpecificationPoint = new DicomTag(0x300a, 0x0082);

		/// <summary>(300a,0084) VR=DS VM=1 Beam Dose</summary>
		DicomTags.BeamDose = new DicomTag(0x300a, 0x0084);

		/// <summary>(300a,0086) VR=DS VM=1 Beam Meterset</summary>
		DicomTags.BeamMeterset = new DicomTag(0x300a, 0x0086);

		/// <summary>(300a,0088) VR=FL VM=1 Beam Dose Point Depth</summary>
		DicomTags.BeamDosePointDepth = new DicomTag(0x300a, 0x0088);

		/// <summary>(300a,0089) VR=FL VM=1 Beam Dose Point Equivalent Depth</summary>
		DicomTags.BeamDosePointEquivalentDepth = new DicomTag(0x300a, 0x0089);

		/// <summary>(300a,008a) VR=FL VM=1 Beam Dose Point SSD</summary>
		DicomTags.BeamDosePointSSD = new DicomTag(0x300a, 0x008a);

		/// <summary>(300a,00a0) VR=IS VM=1 Number of Brachy Application Setups</summary>
		DicomTags.NumberOfBrachyApplicationSetups = new DicomTag(0x300a, 0x00a0);

		/// <summary>(300a,00a2) VR=DS VM=3 Brachy Application Setup Dose Specification Point</summary>
		DicomTags.BrachyApplicationSetupDoseSpecificationPoint = new DicomTag(0x300a, 0x00a2);

		/// <summary>(300a,00a4) VR=DS VM=1 Brachy Application Setup Dose</summary>
		DicomTags.BrachyApplicationSetupDose = new DicomTag(0x300a, 0x00a4);

		/// <summary>(300a,00b0) VR=SQ VM=1 Beam Sequence</summary>
		DicomTags.BeamSequence = new DicomTag(0x300a, 0x00b0);

		/// <summary>(300a,00b2) VR=SH VM=1 Treatment Machine Name</summary>
		DicomTags.TreatmentMachineName = new DicomTag(0x300a, 0x00b2);

		/// <summary>(300a,00b3) VR=CS VM=1 Primary Dosimeter Unit</summary>
		DicomTags.PrimaryDosimeterUnit = new DicomTag(0x300a, 0x00b3);

		/// <summary>(300a,00b4) VR=DS VM=1 Source-Axis Distance</summary>
		DicomTags.SourceAxisDistance = new DicomTag(0x300a, 0x00b4);

		/// <summary>(300a,00b6) VR=SQ VM=1 Beam Limiting Device Sequence</summary>
		DicomTags.BeamLimitingDeviceSequence = new DicomTag(0x300a, 0x00b6);

		/// <summary>(300a,00b8) VR=CS VM=1 RT Beam Limiting Device Type</summary>
		DicomTags.RTBeamLimitingDeviceType = new DicomTag(0x300a, 0x00b8);

		/// <summary>(300a,00ba) VR=DS VM=1 Source to Beam Limiting Device Distance</summary>
		DicomTags.SourceToBeamLimitingDeviceDistance = new DicomTag(0x300a, 0x00ba);

		/// <summary>(300a,00bb) VR=FL VM=1 Isocenter to Beam Limiting Device Distance</summary>
		DicomTags.IsocenterToBeamLimitingDeviceDistance = new DicomTag(0x300a, 0x00bb);

		/// <summary>(300a,00bc) VR=IS VM=1 Number of Leaf/Jaw Pairs</summary>
		DicomTags.NumberOfLeafJawPairs = new DicomTag(0x300a, 0x00bc);

		/// <summary>(300a,00be) VR=DS VM=3-n Leaf Position Boundaries</summary>
		DicomTags.LeafPositionBoundaries = new DicomTag(0x300a, 0x00be);

		/// <summary>(300a,00c0) VR=IS VM=1 Beam Number</summary>
		DicomTags.BeamNumber = new DicomTag(0x300a, 0x00c0);

		/// <summary>(300a,00c2) VR=LO VM=1 Beam Name</summary>
		DicomTags.BeamName = new DicomTag(0x300a, 0x00c2);

		/// <summary>(300a,00c3) VR=ST VM=1 Beam Description</summary>
		DicomTags.BeamDescription = new DicomTag(0x300a, 0x00c3);

		/// <summary>(300a,00c4) VR=CS VM=1 Beam Type</summary>
		DicomTags.BeamType = new DicomTag(0x300a, 0x00c4);

		/// <summary>(300a,00c6) VR=CS VM=1 Radiation Type</summary>
		DicomTags.RadiationType = new DicomTag(0x300a, 0x00c6);

		/// <summary>(300a,00c7) VR=CS VM=1 High-Dose Technique Type</summary>
		DicomTags.HighDoseTechniqueType = new DicomTag(0x300a, 0x00c7);

		/// <summary>(300a,00c8) VR=IS VM=1 Reference Image Number</summary>
		DicomTags.ReferenceImageNumber = new DicomTag(0x300a, 0x00c8);

		/// <summary>(300a,00ca) VR=SQ VM=1 Planned Verification Image Sequence</summary>
		DicomTags.PlannedVerificationImageSequence = new DicomTag(0x300a, 0x00ca);

		/// <summary>(300a,00cc) VR=LO VM=1-n Imaging Device-Specific Acquisition Parameters</summary>
		DicomTags.ImagingDeviceSpecificAcquisitionParameters = new DicomTag(0x300a, 0x00cc);

		/// <summary>(300a,00ce) VR=CS VM=1 Treatment Delivery Type</summary>
		DicomTags.TreatmentDeliveryType = new DicomTag(0x300a, 0x00ce);

		/// <summary>(300a,00d0) VR=IS VM=1 Number of Wedges</summary>
		DicomTags.NumberOfWedges = new DicomTag(0x300a, 0x00d0);

		/// <summary>(300a,00d1) VR=SQ VM=1 Wedge Sequence</summary>
		DicomTags.WedgeSequence = new DicomTag(0x300a, 0x00d1);

		/// <summary>(300a,00d2) VR=IS VM=1 Wedge Number</summary>
		DicomTags.WedgeNumber = new DicomTag(0x300a, 0x00d2);

		/// <summary>(300a,00d3) VR=CS VM=1 Wedge Type</summary>
		DicomTags.WedgeType = new DicomTag(0x300a, 0x00d3);

		/// <summary>(300a,00d4) VR=SH VM=1 Wedge ID</summary>
		DicomTags.WedgeID = new DicomTag(0x300a, 0x00d4);

		/// <summary>(300a,00d5) VR=IS VM=1 Wedge Angle</summary>
		DicomTags.WedgeAngle = new DicomTag(0x300a, 0x00d5);

		/// <summary>(300a,00d6) VR=DS VM=1 Wedge Factor</summary>
		DicomTags.WedgeFactor = new DicomTag(0x300a, 0x00d6);

		/// <summary>(300a,00d7) VR=FL VM=1 Total Wedge Tray Water-Equivalent Thickness</summary>
		DicomTags.TotalWedgeTrayWaterEquivalentThickness = new DicomTag(0x300a, 0x00d7);

		/// <summary>(300a,00d8) VR=DS VM=1 Wedge Orientation</summary>
		DicomTags.WedgeOrientation = new DicomTag(0x300a, 0x00d8);

		/// <summary>(300a,00d9) VR=FL VM=1 Isocenter to Wedge Tray Distance</summary>
		DicomTags.IsocenterToWedgeTrayDistance = new DicomTag(0x300a, 0x00d9);

		/// <summary>(300a,00da) VR=DS VM=1 Source to Wedge Tray Distance</summary>
		DicomTags.SourceToWedgeTrayDistance = new DicomTag(0x300a, 0x00da);

		/// <summary>(300a,00db) VR=FL VM=1 Wedge Thin Edge Position</summary>
		DicomTags.WedgeThinEdgePosition = new DicomTag(0x300a, 0x00db);

		/// <summary>(300a,00dc) VR=SH VM=1 Bolus ID</summary>
		DicomTags.BolusID = new DicomTag(0x300a, 0x00dc);

		/// <summary>(300a,00dd) VR=ST VM=1 Bolus Description</summary>
		DicomTags.BolusDescription = new DicomTag(0x300a, 0x00dd);

		/// <summary>(300a,00e0) VR=IS VM=1 Number of Compensators</summary>
		DicomTags.NumberOfCompensators = new DicomTag(0x300a, 0x00e0);

		/// <summary>(300a,00e1) VR=SH VM=1 Material ID</summary>
		DicomTags.MaterialID = new DicomTag(0x300a, 0x00e1);

		/// <summary>(300a,00e2) VR=DS VM=1 Total Compensator Tray Factor</summary>
		DicomTags.TotalCompensatorTrayFactor = new DicomTag(0x300a, 0x00e2);

		/// <summary>(300a,00e3) VR=SQ VM=1 Compensator Sequence</summary>
		DicomTags.CompensatorSequence = new DicomTag(0x300a, 0x00e3);

		/// <summary>(300a,00e4) VR=IS VM=1 Compensator Number</summary>
		DicomTags.CompensatorNumber = new DicomTag(0x300a, 0x00e4);

		/// <summary>(300a,00e5) VR=SH VM=1 Compensator ID</summary>
		DicomTags.CompensatorID = new DicomTag(0x300a, 0x00e5);

		/// <summary>(300a,00e6) VR=DS VM=1 Source to Compensator Tray Distance</summary>
		DicomTags.SourceToCompensatorTrayDistance = new DicomTag(0x300a, 0x00e6);

		/// <summary>(300a,00e7) VR=IS VM=1 Compensator Rows</summary>
		DicomTags.CompensatorRows = new DicomTag(0x300a, 0x00e7);

		/// <summary>(300a,00e8) VR=IS VM=1 Compensator Columns</summary>
		DicomTags.CompensatorColumns = new DicomTag(0x300a, 0x00e8);

		/// <summary>(300a,00e9) VR=DS VM=2 Compensator Pixel Spacing</summary>
		DicomTags.CompensatorPixelSpacing = new DicomTag(0x300a, 0x00e9);

		/// <summary>(300a,00ea) VR=DS VM=2 Compensator Position</summary>
		DicomTags.CompensatorPosition = new DicomTag(0x300a, 0x00ea);

		/// <summary>(300a,00eb) VR=DS VM=1-n Compensator Transmission Data</summary>
		DicomTags.CompensatorTransmissionData = new DicomTag(0x300a, 0x00eb);

		/// <summary>(300a,00ec) VR=DS VM=1-n Compensator Thickness Data</summary>
		DicomTags.CompensatorThicknessData = new DicomTag(0x300a, 0x00ec);

		/// <summary>(300a,00ed) VR=IS VM=1 Number of Boli</summary>
		DicomTags.NumberOfBoli = new DicomTag(0x300a, 0x00ed);

		/// <summary>(300a,00ee) VR=CS VM=1 Compensator Type</summary>
		DicomTags.CompensatorType = new DicomTag(0x300a, 0x00ee);

		/// <summary>(300a,00f0) VR=IS VM=1 Number of Blocks</summary>
		DicomTags.NumberOfBlocks = new DicomTag(0x300a, 0x00f0);

		/// <summary>(300a,00f2) VR=DS VM=1 Total Block Tray Factor</summary>
		DicomTags.TotalBlockTrayFactor = new DicomTag(0x300a, 0x00f2);

		/// <summary>(300a,00f3) VR=FL VM=1 Total Block Tray Water-Equivalent Thickness</summary>
		DicomTags.TotalBlockTrayWaterEquivalentThickness = new DicomTag(0x300a, 0x00f3);

		/// <summary>(300a,00f4) VR=SQ VM=1 Block Sequence</summary>
		DicomTags.BlockSequence = new DicomTag(0x300a, 0x00f4);

		/// <summary>(300a,00f5) VR=SH VM=1 Block Tray ID</summary>
		DicomTags.BlockTrayID = new DicomTag(0x300a, 0x00f5);

		/// <summary>(300a,00f6) VR=DS VM=1 Source to Block Tray Distance</summary>
		DicomTags.SourceToBlockTrayDistance = new DicomTag(0x300a, 0x00f6);

		/// <summary>(300a,00f7) VR=FL VM=1 Isocenter to Block Tray Distance</summary>
		DicomTags.IsocenterToBlockTrayDistance = new DicomTag(0x300a, 0x00f7);

		/// <summary>(300a,00f8) VR=CS VM=1 Block Type</summary>
		DicomTags.BlockType = new DicomTag(0x300a, 0x00f8);

		/// <summary>(300a,00f9) VR=LO VM=1 Accessory Code</summary>
		DicomTags.AccessoryCode = new DicomTag(0x300a, 0x00f9);

		/// <summary>(300a,00fa) VR=CS VM=1 Block Divergence</summary>
		DicomTags.BlockDivergence = new DicomTag(0x300a, 0x00fa);

		/// <summary>(300a,00fb) VR=CS VM=1 Block Mounting Position</summary>
		DicomTags.BlockMountingPosition = new DicomTag(0x300a, 0x00fb);

		/// <summary>(300a,00fc) VR=IS VM=1 Block Number</summary>
		DicomTags.BlockNumber = new DicomTag(0x300a, 0x00fc);

		/// <summary>(300a,00fe) VR=LO VM=1 Block Name</summary>
		DicomTags.BlockName = new DicomTag(0x300a, 0x00fe);

		/// <summary>(300a,0100) VR=DS VM=1 Block Thickness</summary>
		DicomTags.BlockThickness = new DicomTag(0x300a, 0x0100);

		/// <summary>(300a,0102) VR=DS VM=1 Block Transmission</summary>
		DicomTags.BlockTransmission = new DicomTag(0x300a, 0x0102);

		/// <summary>(300a,0104) VR=IS VM=1 Block Number of Points</summary>
		DicomTags.BlockNumberOfPoints = new DicomTag(0x300a, 0x0104);

		/// <summary>(300a,0106) VR=DS VM=2-2n Block Data</summary>
		DicomTags.BlockData = new DicomTag(0x300a, 0x0106);

		/// <summary>(300a,0107) VR=SQ VM=1 Applicator Sequence</summary>
		DicomTags.ApplicatorSequence = new DicomTag(0x300a, 0x0107);

		/// <summary>(300a,0108) VR=SH VM=1 Applicator ID</summary>
		DicomTags.ApplicatorID = new DicomTag(0x300a, 0x0108);

		/// <summary>(300a,0109) VR=CS VM=1 Applicator Type</summary>
		DicomTags.ApplicatorType = new DicomTag(0x300a, 0x0109);

		/// <summary>(300a,010a) VR=LO VM=1 Applicator Description</summary>
		DicomTags.ApplicatorDescription = new DicomTag(0x300a, 0x010a);

		/// <summary>(300a,010c) VR=DS VM=1 Cumulative Dose Reference Coefficient</summary>
		DicomTags.CumulativeDoseReferenceCoefficient = new DicomTag(0x300a, 0x010c);

		/// <summary>(300a,010e) VR=DS VM=1 Final Cumulative Meterset Weight</summary>
		DicomTags.FinalCumulativeMetersetWeight = new DicomTag(0x300a, 0x010e);

		/// <summary>(300a,0110) VR=IS VM=1 Number of Control Points</summary>
		DicomTags.NumberOfControlPoints = new DicomTag(0x300a, 0x0110);

		/// <summary>(300a,0111) VR=SQ VM=1 Control Point Sequence</summary>
		DicomTags.ControlPointSequence = new DicomTag(0x300a, 0x0111);

		/// <summary>(300a,0112) VR=IS VM=1 Control Point Index</summary>
		DicomTags.ControlPointIndex = new DicomTag(0x300a, 0x0112);

		/// <summary>(300a,0114) VR=DS VM=1 Nominal Beam Energy</summary>
		DicomTags.NominalBeamEnergy = new DicomTag(0x300a, 0x0114);

		/// <summary>(300a,0115) VR=DS VM=1 Dose Rate Set</summary>
		DicomTags.DoseRateSet = new DicomTag(0x300a, 0x0115);

		/// <summary>(300a,0116) VR=SQ VM=1 Wedge Position Sequence</summary>
		DicomTags.WedgePositionSequence = new DicomTag(0x300a, 0x0116);

		/// <summary>(300a,0118) VR=CS VM=1 Wedge Position</summary>
		DicomTags.WedgePosition = new DicomTag(0x300a, 0x0118);

		/// <summary>(300a,011a) VR=SQ VM=1 Beam Limiting Device Position Sequence</summary>
		DicomTags.BeamLimitingDevicePositionSequence = new DicomTag(0x300a, 0x011a);

		/// <summary>(300a,011c) VR=DS VM=2-2n Leaf/Jaw Positions</summary>
		DicomTags.LeafJawPositions = new DicomTag(0x300a, 0x011c);

		/// <summary>(300a,011e) VR=DS VM=1 Gantry Angle</summary>
		DicomTags.GantryAngle = new DicomTag(0x300a, 0x011e);

		/// <summary>(300a,011f) VR=CS VM=1 Gantry Rotation Direction</summary>
		DicomTags.GantryRotationDirection = new DicomTag(0x300a, 0x011f);

		/// <summary>(300a,0120) VR=DS VM=1 Beam Limiting Device Angle</summary>
		DicomTags.BeamLimitingDeviceAngle = new DicomTag(0x300a, 0x0120);

		/// <summary>(300a,0121) VR=CS VM=1 Beam Limiting Device Rotation Direction</summary>
		DicomTags.BeamLimitingDeviceRotationDirection = new DicomTag(0x300a, 0x0121);

		/// <summary>(300a,0122) VR=DS VM=1 Patient Support Angle</summary>
		DicomTags.PatientSupportAngle = new DicomTag(0x300a, 0x0122);

		/// <summary>(300a,0123) VR=CS VM=1 Patient Support Rotation Direction</summary>
		DicomTags.PatientSupportRotationDirection = new DicomTag(0x300a, 0x0123);

		/// <summary>(300a,0124) VR=DS VM=1 Table Top Eccentric Axis Distance</summary>
		DicomTags.TableTopEccentricAxisDistance = new DicomTag(0x300a, 0x0124);

		/// <summary>(300a,0125) VR=DS VM=1 Table Top Eccentric Angle</summary>
		DicomTags.TableTopEccentricAngle = new DicomTag(0x300a, 0x0125);

		/// <summary>(300a,0126) VR=CS VM=1 Table Top Eccentric Rotation Direction</summary>
		DicomTags.TableTopEccentricRotationDirection = new DicomTag(0x300a, 0x0126);

		/// <summary>(300a,0128) VR=DS VM=1 Table Top Vertical Position</summary>
		DicomTags.TableTopVerticalPosition = new DicomTag(0x300a, 0x0128);

		/// <summary>(300a,0129) VR=DS VM=1 Table Top Longitudinal Position</summary>
		DicomTags.TableTopLongitudinalPosition = new DicomTag(0x300a, 0x0129);

		/// <summary>(300a,012a) VR=DS VM=1 Table Top Lateral Position</summary>
		DicomTags.TableTopLateralPosition = new DicomTag(0x300a, 0x012a);

		/// <summary>(300a,012c) VR=DS VM=3 Isocenter Position</summary>
		DicomTags.IsocenterPosition = new DicomTag(0x300a, 0x012c);

		/// <summary>(300a,012e) VR=DS VM=3 Surface Entry Point</summary>
		DicomTags.SurfaceEntryPoint = new DicomTag(0x300a, 0x012e);

		/// <summary>(300a,0130) VR=DS VM=1 Source to Surface Distance</summary>
		DicomTags.SourceToSurfaceDistance = new DicomTag(0x300a, 0x0130);

		/// <summary>(300a,0134) VR=DS VM=1 Cumulative Meterset Weight</summary>
		DicomTags.CumulativeMetersetWeight = new DicomTag(0x300a, 0x0134);

		/// <summary>(300a,0140) VR=FL VM=1 Table Top Pitch Angle</summary>
		DicomTags.TableTopPitchAngle = new DicomTag(0x300a, 0x0140);

		/// <summary>(300a,0142) VR=CS VM=1 Table Top Pitch Rotation Direction</summary>
		DicomTags.TableTopPitchRotationDirection = new DicomTag(0x300a, 0x0142);

		/// <summary>(300a,0144) VR=FL VM=1 Table Top Roll Angle</summary>
		DicomTags.TableTopRollAngle = new DicomTag(0x300a, 0x0144);

		/// <summary>(300a,0146) VR=CS VM=1 Table Top Roll Rotation Direction</summary>
		DicomTags.TableTopRollRotationDirection = new DicomTag(0x300a, 0x0146);

		/// <summary>(300a,0148) VR=FL VM=1 Head Fixation Angle</summary>
		DicomTags.HeadFixationAngle = new DicomTag(0x300a, 0x0148);

		/// <summary>(300a,014a) VR=FL VM=1 Gantry Pitch Angle</summary>
		DicomTags.GantryPitchAngle = new DicomTag(0x300a, 0x014a);

		/// <summary>(300a,014c) VR=CS VM=1 Gantry Pitch Rotation Direction</summary>
		DicomTags.GantryPitchRotationDirection = new DicomTag(0x300a, 0x014c);

		/// <summary>(300a,014e) VR=FL VM=1 Gantry Pitch Angle Tolerance</summary>
		DicomTags.GantryPitchAngleTolerance = new DicomTag(0x300a, 0x014e);

		/// <summary>(300a,0180) VR=SQ VM=1 Patient Setup Sequence</summary>
		DicomTags.PatientSetupSequence = new DicomTag(0x300a, 0x0180);

		/// <summary>(300a,0182) VR=IS VM=1 Patient Setup Number</summary>
		DicomTags.PatientSetupNumber = new DicomTag(0x300a, 0x0182);

		/// <summary>(300a,0183) VR=LO VM=1 Patient Setup Label</summary>
		DicomTags.PatientSetupLabel = new DicomTag(0x300a, 0x0183);

		/// <summary>(300a,0184) VR=LO VM=1 Patient Additional Position</summary>
		DicomTags.PatientAdditionalPosition = new DicomTag(0x300a, 0x0184);

		/// <summary>(300a,0190) VR=SQ VM=1 Fixation Device Sequence</summary>
		DicomTags.FixationDeviceSequence = new DicomTag(0x300a, 0x0190);

		/// <summary>(300a,0192) VR=CS VM=1 Fixation Device Type</summary>
		DicomTags.FixationDeviceType = new DicomTag(0x300a, 0x0192);

		/// <summary>(300a,0194) VR=SH VM=1 Fixation Device Label</summary>
		DicomTags.FixationDeviceLabel = new DicomTag(0x300a, 0x0194);

		/// <summary>(300a,0196) VR=ST VM=1 Fixation Device Description</summary>
		DicomTags.FixationDeviceDescription = new DicomTag(0x300a, 0x0196);

		/// <summary>(300a,0198) VR=SH VM=1 Fixation Device Position</summary>
		DicomTags.FixationDevicePosition = new DicomTag(0x300a, 0x0198);

		/// <summary>(300a,0199) VR=FL VM=1 Fixation Device Pitch Angle</summary>
		DicomTags.FixationDevicePitchAngle = new DicomTag(0x300a, 0x0199);

		/// <summary>(300a,019a) VR=FL VM=1 Fixation Device Roll Angle</summary>
		DicomTags.FixationDeviceRollAngle = new DicomTag(0x300a, 0x019a);

		/// <summary>(300a,01a0) VR=SQ VM=1 Shielding Device Sequence</summary>
		DicomTags.ShieldingDeviceSequence = new DicomTag(0x300a, 0x01a0);

		/// <summary>(300a,01a2) VR=CS VM=1 Shielding Device Type</summary>
		DicomTags.ShieldingDeviceType = new DicomTag(0x300a, 0x01a2);

		/// <summary>(300a,01a4) VR=SH VM=1 Shielding Device Label</summary>
		DicomTags.ShieldingDeviceLabel = new DicomTag(0x300a, 0x01a4);

		/// <summary>(300a,01a6) VR=ST VM=1 Shielding Device Description</summary>
		DicomTags.ShieldingDeviceDescription = new DicomTag(0x300a, 0x01a6);

		/// <summary>(300a,01a8) VR=SH VM=1 Shielding Device Position</summary>
		DicomTags.ShieldingDevicePosition = new DicomTag(0x300a, 0x01a8);

		/// <summary>(300a,01b0) VR=CS VM=1 Setup Technique</summary>
		DicomTags.SetupTechnique = new DicomTag(0x300a, 0x01b0);

		/// <summary>(300a,01b2) VR=ST VM=1 Setup Technique Description</summary>
		DicomTags.SetupTechniqueDescription = new DicomTag(0x300a, 0x01b2);

		/// <summary>(300a,01b4) VR=SQ VM=1 Setup Device Sequence</summary>
		DicomTags.SetupDeviceSequence = new DicomTag(0x300a, 0x01b4);

		/// <summary>(300a,01b6) VR=CS VM=1 Setup Device Type</summary>
		DicomTags.SetupDeviceType = new DicomTag(0x300a, 0x01b6);

		/// <summary>(300a,01b8) VR=SH VM=1 Setup Device Label</summary>
		DicomTags.SetupDeviceLabel = new DicomTag(0x300a, 0x01b8);

		/// <summary>(300a,01ba) VR=ST VM=1 Setup Device Description</summary>
		DicomTags.SetupDeviceDescription = new DicomTag(0x300a, 0x01ba);

		/// <summary>(300a,01bc) VR=DS VM=1 Setup Device Parameter</summary>
		DicomTags.SetupDeviceParameter = new DicomTag(0x300a, 0x01bc);

		/// <summary>(300a,01d0) VR=ST VM=1 Setup Reference Description</summary>
		DicomTags.SetupReferenceDescription = new DicomTag(0x300a, 0x01d0);

		/// <summary>(300a,01d2) VR=DS VM=1 Table Top Vertical Setup Displacement</summary>
		DicomTags.TableTopVerticalSetupDisplacement = new DicomTag(0x300a, 0x01d2);

		/// <summary>(300a,01d4) VR=DS VM=1 Table Top Longitudinal Setup Displacement</summary>
		DicomTags.TableTopLongitudinalSetupDisplacement = new DicomTag(0x300a, 0x01d4);

		/// <summary>(300a,01d6) VR=DS VM=1 Table Top Lateral Setup Displacement</summary>
		DicomTags.TableTopLateralSetupDisplacement = new DicomTag(0x300a, 0x01d6);

		/// <summary>(300a,0200) VR=CS VM=1 Brachy Treatment Technique</summary>
		DicomTags.BrachyTreatmentTechnique = new DicomTag(0x300a, 0x0200);

		/// <summary>(300a,0202) VR=CS VM=1 Brachy Treatment Type</summary>
		DicomTags.BrachyTreatmentType = new DicomTag(0x300a, 0x0202);

		/// <summary>(300a,0206) VR=SQ VM=1 Treatment Machine Sequence</summary>
		DicomTags.TreatmentMachineSequence = new DicomTag(0x300a, 0x0206);

		/// <summary>(300a,0210) VR=SQ VM=1 Source Sequence</summary>
		DicomTags.SourceSequence = new DicomTag(0x300a, 0x0210);

		/// <summary>(300a,0212) VR=IS VM=1 Source Number</summary>
		DicomTags.SourceNumber = new DicomTag(0x300a, 0x0212);

		/// <summary>(300a,0214) VR=CS VM=1 Source Type</summary>
		DicomTags.SourceType = new DicomTag(0x300a, 0x0214);

		/// <summary>(300a,0216) VR=LO VM=1 Source Manufacturer</summary>
		DicomTags.SourceManufacturer = new DicomTag(0x300a, 0x0216);

		/// <summary>(300a,0218) VR=DS VM=1 Active Source Diameter</summary>
		DicomTags.ActiveSourceDiameter = new DicomTag(0x300a, 0x0218);

		/// <summary>(300a,021a) VR=DS VM=1 Active Source Length</summary>
		DicomTags.ActiveSourceLength = new DicomTag(0x300a, 0x021a);

		/// <summary>(300a,0222) VR=DS VM=1 Source Encapsulation Nominal Thickness</summary>
		DicomTags.SourceEncapsulationNominalThickness = new DicomTag(0x300a, 0x0222);

		/// <summary>(300a,0224) VR=DS VM=1 Source Encapsulation Nominal Transmission</summary>
		DicomTags.SourceEncapsulationNominalTransmission = new DicomTag(0x300a, 0x0224);

		/// <summary>(300a,0226) VR=LO VM=1 Source Isotope Name</summary>
		DicomTags.SourceIsotopeName = new DicomTag(0x300a, 0x0226);

		/// <summary>(300a,0228) VR=DS VM=1 Source Isotope Half Life</summary>
		DicomTags.SourceIsotopeHalfLife = new DicomTag(0x300a, 0x0228);

		/// <summary>(300a,0229) VR=CS VM=1 Source Strength Units</summary>
		DicomTags.SourceStrengthUnits = new DicomTag(0x300a, 0x0229);

		/// <summary>(300a,022a) VR=DS VM=1 Reference Air Kerma Rate</summary>
		DicomTags.ReferenceAirKermaRate = new DicomTag(0x300a, 0x022a);

		/// <summary>(300a,022b) VR=DS VM=1 Source Strength</summary>
		DicomTags.SourceStrength = new DicomTag(0x300a, 0x022b);

		/// <summary>(300a,022c) VR=DA VM=1 Source Strength Reference Date</summary>
		DicomTags.SourceStrengthReferenceDate = new DicomTag(0x300a, 0x022c);

		/// <summary>(300a,022e) VR=TM VM=1 Source Strength Reference Time</summary>
		DicomTags.SourceStrengthReferenceTime = new DicomTag(0x300a, 0x022e);

		/// <summary>(300a,0230) VR=SQ VM=1 Application Setup Sequence</summary>
		DicomTags.ApplicationSetupSequence = new DicomTag(0x300a, 0x0230);

		/// <summary>(300a,0232) VR=CS VM=1 Application Setup Type</summary>
		DicomTags.ApplicationSetupType = new DicomTag(0x300a, 0x0232);

		/// <summary>(300a,0234) VR=IS VM=1 Application Setup Number</summary>
		DicomTags.ApplicationSetupNumber = new DicomTag(0x300a, 0x0234);

		/// <summary>(300a,0236) VR=LO VM=1 Application Setup Name</summary>
		DicomTags.ApplicationSetupName = new DicomTag(0x300a, 0x0236);

		/// <summary>(300a,0238) VR=LO VM=1 Application Setup Manufacturer</summary>
		DicomTags.ApplicationSetupManufacturer = new DicomTag(0x300a, 0x0238);

		/// <summary>(300a,0240) VR=IS VM=1 Template Number</summary>
		DicomTags.TemplateNumber = new DicomTag(0x300a, 0x0240);

		/// <summary>(300a,0242) VR=SH VM=1 Template Type</summary>
		DicomTags.TemplateType = new DicomTag(0x300a, 0x0242);

		/// <summary>(300a,0244) VR=LO VM=1 Template Name</summary>
		DicomTags.TemplateName = new DicomTag(0x300a, 0x0244);

		/// <summary>(300a,0250) VR=DS VM=1 Total Reference Air Kerma</summary>
		DicomTags.TotalReferenceAirKerma = new DicomTag(0x300a, 0x0250);

		/// <summary>(300a,0260) VR=SQ VM=1 Brachy Accessory Device Sequence</summary>
		DicomTags.BrachyAccessoryDeviceSequence = new DicomTag(0x300a, 0x0260);

		/// <summary>(300a,0262) VR=IS VM=1 Brachy Accessory Device Number</summary>
		DicomTags.BrachyAccessoryDeviceNumber = new DicomTag(0x300a, 0x0262);

		/// <summary>(300a,0263) VR=SH VM=1 Brachy Accessory Device ID</summary>
		DicomTags.BrachyAccessoryDeviceID = new DicomTag(0x300a, 0x0263);

		/// <summary>(300a,0264) VR=CS VM=1 Brachy Accessory Device Type</summary>
		DicomTags.BrachyAccessoryDeviceType = new DicomTag(0x300a, 0x0264);

		/// <summary>(300a,0266) VR=LO VM=1 Brachy Accessory Device Name</summary>
		DicomTags.BrachyAccessoryDeviceName = new DicomTag(0x300a, 0x0266);

		/// <summary>(300a,026a) VR=DS VM=1 Brachy Accessory Device Nominal Thickness</summary>
		DicomTags.BrachyAccessoryDeviceNominalThickness = new DicomTag(0x300a, 0x026a);

		/// <summary>(300a,026c) VR=DS VM=1 Brachy Accessory Device Nominal Transmission</summary>
		DicomTags.BrachyAccessoryDeviceNominalTransmission = new DicomTag(0x300a, 0x026c);

		/// <summary>(300a,0280) VR=SQ VM=1 Channel Sequence</summary>
		DicomTags.ChannelSequence = new DicomTag(0x300a, 0x0280);

		/// <summary>(300a,0282) VR=IS VM=1 Channel Number</summary>
		DicomTags.ChannelNumber = new DicomTag(0x300a, 0x0282);

		/// <summary>(300a,0284) VR=DS VM=1 Channel Length</summary>
		DicomTags.ChannelLength = new DicomTag(0x300a, 0x0284);

		/// <summary>(300a,0286) VR=DS VM=1 Channel Total Time</summary>
		DicomTags.ChannelTotalTime = new DicomTag(0x300a, 0x0286);

		/// <summary>(300a,0288) VR=CS VM=1 Source Movement Type</summary>
		DicomTags.SourceMovementType = new DicomTag(0x300a, 0x0288);

		/// <summary>(300a,028a) VR=IS VM=1 Number of Pulses</summary>
		DicomTags.NumberOfPulses = new DicomTag(0x300a, 0x028a);

		/// <summary>(300a,028c) VR=DS VM=1 Pulse Repetition Interval</summary>
		DicomTags.PulseRepetitionInterval = new DicomTag(0x300a, 0x028c);

		/// <summary>(300a,0290) VR=IS VM=1 Source Applicator Number</summary>
		DicomTags.SourceApplicatorNumber = new DicomTag(0x300a, 0x0290);

		/// <summary>(300a,0291) VR=SH VM=1 Source Applicator ID</summary>
		DicomTags.SourceApplicatorID = new DicomTag(0x300a, 0x0291);

		/// <summary>(300a,0292) VR=CS VM=1 Source Applicator Type</summary>
		DicomTags.SourceApplicatorType = new DicomTag(0x300a, 0x0292);

		/// <summary>(300a,0294) VR=LO VM=1 Source Applicator Name</summary>
		DicomTags.SourceApplicatorName = new DicomTag(0x300a, 0x0294);

		/// <summary>(300a,0296) VR=DS VM=1 Source Applicator Length</summary>
		DicomTags.SourceApplicatorLength = new DicomTag(0x300a, 0x0296);

		/// <summary>(300a,0298) VR=LO VM=1 Source Applicator Manufacturer</summary>
		DicomTags.SourceApplicatorManufacturer = new DicomTag(0x300a, 0x0298);

		/// <summary>(300a,029c) VR=DS VM=1 Source Applicator Wall Nominal Thickness</summary>
		DicomTags.SourceApplicatorWallNominalThickness = new DicomTag(0x300a, 0x029c);

		/// <summary>(300a,029e) VR=DS VM=1 Source Applicator Wall Nominal Transmission</summary>
		DicomTags.SourceApplicatorWallNominalTransmission = new DicomTag(0x300a, 0x029e);

		/// <summary>(300a,02a0) VR=DS VM=1 Source Applicator Step Size</summary>
		DicomTags.SourceApplicatorStepSize = new DicomTag(0x300a, 0x02a0);

		/// <summary>(300a,02a2) VR=IS VM=1 Transfer Tube Number</summary>
		DicomTags.TransferTubeNumber = new DicomTag(0x300a, 0x02a2);

		/// <summary>(300a,02a4) VR=DS VM=1 Transfer Tube Length</summary>
		DicomTags.TransferTubeLength = new DicomTag(0x300a, 0x02a4);

		/// <summary>(300a,02b0) VR=SQ VM=1 Channel Shield Sequence</summary>
		DicomTags.ChannelShieldSequence = new DicomTag(0x300a, 0x02b0);

		/// <summary>(300a,02b2) VR=IS VM=1 Channel Shield Number</summary>
		DicomTags.ChannelShieldNumber = new DicomTag(0x300a, 0x02b2);

		/// <summary>(300a,02b3) VR=SH VM=1 Channel Shield ID</summary>
		DicomTags.ChannelShieldID = new DicomTag(0x300a, 0x02b3);

		/// <summary>(300a,02b4) VR=LO VM=1 Channel Shield Name</summary>
		DicomTags.ChannelShieldName = new DicomTag(0x300a, 0x02b4);

		/// <summary>(300a,02b8) VR=DS VM=1 Channel Shield Nominal Thickness</summary>
		DicomTags.ChannelShieldNominalThickness = new DicomTag(0x300a, 0x02b8);

		/// <summary>(300a,02ba) VR=DS VM=1 Channel Shield Nominal Transmission</summary>
		DicomTags.ChannelShieldNominalTransmission = new DicomTag(0x300a, 0x02ba);

		/// <summary>(300a,02c8) VR=DS VM=1 Final Cumulative Time Weight</summary>
		DicomTags.FinalCumulativeTimeWeight = new DicomTag(0x300a, 0x02c8);

		/// <summary>(300a,02d0) VR=SQ VM=1 Brachy Control Point Sequence</summary>
		DicomTags.BrachyControlPointSequence = new DicomTag(0x300a, 0x02d0);

		/// <summary>(300a,02d2) VR=DS VM=1 Control Point Relative Position</summary>
		DicomTags.ControlPointRelativePosition = new DicomTag(0x300a, 0x02d2);

		/// <summary>(300a,02d4) VR=DS VM=3 Control Point 3D Position</summary>
		DicomTags.ControlPoint3DPosition = new DicomTag(0x300a, 0x02d4);

		/// <summary>(300a,02d6) VR=DS VM=1 Cumulative Time Weight</summary>
		DicomTags.CumulativeTimeWeight = new DicomTag(0x300a, 0x02d6);

		/// <summary>(300a,02e0) VR=CS VM=1 Compensator Divergence</summary>
		DicomTags.CompensatorDivergence = new DicomTag(0x300a, 0x02e0);

		/// <summary>(300a,02e1) VR=CS VM=1 Compensator Mounting Position</summary>
		DicomTags.CompensatorMountingPosition = new DicomTag(0x300a, 0x02e1);

		/// <summary>(300a,02e2) VR=DS VM=1-n Source to Compensator Distance</summary>
		DicomTags.SourceToCompensatorDistance = new DicomTag(0x300a, 0x02e2);

		/// <summary>(300a,02e3) VR=FL VM=1 Total Compensator Tray Water-Equivalent Thickness</summary>
		DicomTags.TotalCompensatorTrayWaterEquivalentThickness = new DicomTag(0x300a, 0x02e3);

		/// <summary>(300a,02e4) VR=FL VM=1 Isocenter to Compensator Tray Distance</summary>
		DicomTags.IsocenterToCompensatorTrayDistance = new DicomTag(0x300a, 0x02e4);

		/// <summary>(300a,02e5) VR=FL VM=1 Compensator Column Offset</summary>
		DicomTags.CompensatorColumnOffset = new DicomTag(0x300a, 0x02e5);

		/// <summary>(300a,02e6) VR=FL VM=1-n Isocenter to Compensator Distances</summary>
		DicomTags.IsocenterToCompensatorDistances = new DicomTag(0x300a, 0x02e6);

		/// <summary>(300a,02e7) VR=FL VM=1 Compensator Relative Stopping Power Ratio</summary>
		DicomTags.CompensatorRelativeStoppingPowerRatio = new DicomTag(0x300a, 0x02e7);

		/// <summary>(300a,02e8) VR=FL VM=1 Compensator Milling Tool Diameter</summary>
		DicomTags.CompensatorMillingToolDiameter = new DicomTag(0x300a, 0x02e8);

		/// <summary>(300a,02ea) VR=SQ VM=1 Ion Range Compensator Sequence</summary>
		DicomTags.IonRangeCompensatorSequence = new DicomTag(0x300a, 0x02ea);

		/// <summary>(300a,02eb) VR=LT VM=1 Compensator Description</summary>
		DicomTags.CompensatorDescription = new DicomTag(0x300a, 0x02eb);

		/// <summary>(300a,0302) VR=IS VM=1 Radiation Mass Number</summary>
		DicomTags.RadiationMassNumber = new DicomTag(0x300a, 0x0302);

		/// <summary>(300a,0304) VR=IS VM=1 Radiation Atomic Number</summary>
		DicomTags.RadiationAtomicNumber = new DicomTag(0x300a, 0x0304);

		/// <summary>(300a,0306) VR=SS VM=1 Radiation Charge State</summary>
		DicomTags.RadiationChargeState = new DicomTag(0x300a, 0x0306);

		/// <summary>(300a,0308) VR=CS VM=1 Scan Mode</summary>
		DicomTags.ScanMode = new DicomTag(0x300a, 0x0308);

		/// <summary>(300a,030a) VR=FL VM=2 Virtual Source-Axis Distances</summary>
		DicomTags.VirtualSourceAxisDistances = new DicomTag(0x300a, 0x030a);

		/// <summary>(300a,030c) VR=SQ VM=1 Snout Sequence</summary>
		DicomTags.SnoutSequence = new DicomTag(0x300a, 0x030c);

		/// <summary>(300a,030d) VR=FL VM=1 Snout Position</summary>
		DicomTags.SnoutPosition = new DicomTag(0x300a, 0x030d);

		/// <summary>(300a,030f) VR=SH VM=1 Snout ID</summary>
		DicomTags.SnoutID = new DicomTag(0x300a, 0x030f);

		/// <summary>(300a,0312) VR=IS VM=1 Number of Range Shifters</summary>
		DicomTags.NumberOfRangeShifters = new DicomTag(0x300a, 0x0312);

		/// <summary>(300a,0314) VR=SQ VM=1 Range Shifter Sequence</summary>
		DicomTags.RangeShifterSequence = new DicomTag(0x300a, 0x0314);

		/// <summary>(300a,0316) VR=IS VM=1 Range Shifter Number</summary>
		DicomTags.RangeShifterNumber = new DicomTag(0x300a, 0x0316);

		/// <summary>(300a,0318) VR=SH VM=1 Range Shifter ID</summary>
		DicomTags.RangeShifterID = new DicomTag(0x300a, 0x0318);

		/// <summary>(300a,0320) VR=CS VM=1 Range Shifter Type</summary>
		DicomTags.RangeShifterType = new DicomTag(0x300a, 0x0320);

		/// <summary>(300a,0322) VR=LO VM=1 Range Shifter Description</summary>
		DicomTags.RangeShifterDescription = new DicomTag(0x300a, 0x0322);

		/// <summary>(300a,0330) VR=IS VM=1 Number of Lateral Spreading Devices</summary>
		DicomTags.NumberOfLateralSpreadingDevices = new DicomTag(0x300a, 0x0330);

		/// <summary>(300a,0332) VR=SQ VM=1 Lateral Spreading Device Sequence</summary>
		DicomTags.LateralSpreadingDeviceSequence = new DicomTag(0x300a, 0x0332);

		/// <summary>(300a,0334) VR=IS VM=1 Lateral Spreading Device Number</summary>
		DicomTags.LateralSpreadingDeviceNumber = new DicomTag(0x300a, 0x0334);

		/// <summary>(300a,0336) VR=SH VM=1 Lateral Spreading Device ID</summary>
		DicomTags.LateralSpreadingDeviceID = new DicomTag(0x300a, 0x0336);

		/// <summary>(300a,0338) VR=CS VM=1 Lateral Spreading Device Type</summary>
		DicomTags.LateralSpreadingDeviceType = new DicomTag(0x300a, 0x0338);

		/// <summary>(300a,033a) VR=LO VM=1 Lateral Spreading Device Description</summary>
		DicomTags.LateralSpreadingDeviceDescription = new DicomTag(0x300a, 0x033a);

		/// <summary>(300a,033c) VR=FL VM=1 Lateral Spreading Device Water Equivalent Thickness</summary>
		DicomTags.LateralSpreadingDeviceWaterEquivalentThickness = new DicomTag(0x300a, 0x033c);

		/// <summary>(300a,0340) VR=IS VM=1 Number of Range Modulators</summary>
		DicomTags.NumberOfRangeModulators = new DicomTag(0x300a, 0x0340);

		/// <summary>(300a,0342) VR=SQ VM=1 Range Modulator Sequence</summary>
		DicomTags.RangeModulatorSequence = new DicomTag(0x300a, 0x0342);

		/// <summary>(300a,0344) VR=IS VM=1 Range Modulator Number</summary>
		DicomTags.RangeModulatorNumber = new DicomTag(0x300a, 0x0344);

		/// <summary>(300a,0346) VR=SH VM=1 Range Modulator ID</summary>
		DicomTags.RangeModulatorID = new DicomTag(0x300a, 0x0346);

		/// <summary>(300a,0348) VR=CS VM=1 Range Modulator Type</summary>
		DicomTags.RangeModulatorType = new DicomTag(0x300a, 0x0348);

		/// <summary>(300a,034a) VR=LO VM=1 Range Modulator Description</summary>
		DicomTags.RangeModulatorDescription = new DicomTag(0x300a, 0x034a);

		/// <summary>(300a,034c) VR=SH VM=1 Beam Current Modulation ID</summary>
		DicomTags.BeamCurrentModulationID = new DicomTag(0x300a, 0x034c);

		/// <summary>(300a,0350) VR=CS VM=1 Patient Support Type</summary>
		DicomTags.PatientSupportType = new DicomTag(0x300a, 0x0350);

		/// <summary>(300a,0352) VR=SH VM=1 Patient Support ID</summary>
		DicomTags.PatientSupportID = new DicomTag(0x300a, 0x0352);

		/// <summary>(300a,0354) VR=LO VM=1 Patient Support Accessory Code</summary>
		DicomTags.PatientSupportAccessoryCode = new DicomTag(0x300a, 0x0354);

		/// <summary>(300a,0356) VR=FL VM=1 Fixation Light Azimuthal Angle</summary>
		DicomTags.FixationLightAzimuthalAngle = new DicomTag(0x300a, 0x0356);

		/// <summary>(300a,0358) VR=FL VM=1 Fixation Light Polar Angle</summary>
		DicomTags.FixationLightPolarAngle = new DicomTag(0x300a, 0x0358);

		/// <summary>(300a,035a) VR=FL VM=1 Meterset Rate</summary>
		DicomTags.MetersetRate = new DicomTag(0x300a, 0x035a);

		/// <summary>(300a,0360) VR=SQ VM=1 Range Shifter Settings Sequence</summary>
		DicomTags.RangeShifterSettingsSequence = new DicomTag(0x300a, 0x0360);

		/// <summary>(300a,0362) VR=LO VM=1 Range Shifter Setting</summary>
		DicomTags.RangeShifterSetting = new DicomTag(0x300a, 0x0362);

		/// <summary>(300a,0364) VR=FL VM=1 Isocenter to Range Shifter Distance</summary>
		DicomTags.IsocenterToRangeShifterDistance = new DicomTag(0x300a, 0x0364);

		/// <summary>(300a,0366) VR=FL VM=1 Range Shifter Water Equivalent Thickness</summary>
		DicomTags.RangeShifterWaterEquivalentThickness = new DicomTag(0x300a, 0x0366);

		/// <summary>(300a,0370) VR=SQ VM=1 Lateral Spreading Device Settings Sequence</summary>
		DicomTags.LateralSpreadingDeviceSettingsSequence = new DicomTag(0x300a, 0x0370);

		/// <summary>(300a,0372) VR=LO VM=1 Lateral Spreading Device Setting</summary>
		DicomTags.LateralSpreadingDeviceSetting = new DicomTag(0x300a, 0x0372);

		/// <summary>(300a,0374) VR=FL VM=1 Isocenter to Lateral Spreading Device Distance</summary>
		DicomTags.IsocenterToLateralSpreadingDeviceDistance = new DicomTag(0x300a, 0x0374);

		/// <summary>(300a,0380) VR=SQ VM=1 Range Modulator Settings Sequence</summary>
		DicomTags.RangeModulatorSettingsSequence = new DicomTag(0x300a, 0x0380);

		/// <summary>(300a,0382) VR=FL VM=1 Range Modulator Gating Start Value</summary>
		DicomTags.RangeModulatorGatingStartValue = new DicomTag(0x300a, 0x0382);

		/// <summary>(300a,0384) VR=FL VM=1 Range Modulator Gating Stop Value</summary>
		DicomTags.RangeModulatorGatingStopValue = new DicomTag(0x300a, 0x0384);

		/// <summary>(300a,0386) VR=FL VM=1 Range Modulator Gating Start Water Equivalent Thickness</summary>
		DicomTags.RangeModulatorGatingStartWaterEquivalentThickness = new DicomTag(0x300a, 0x0386);

		/// <summary>(300a,0388) VR=FL VM=1 Range Modulator Gating Stop Water Equivalent Thickness</summary>
		DicomTags.RangeModulatorGatingStopWaterEquivalentThickness = new DicomTag(0x300a, 0x0388);

		/// <summary>(300a,038a) VR=FL VM=1 Isocenter to Range Modulator Distance</summary>
		DicomTags.IsocenterToRangeModulatorDistance = new DicomTag(0x300a, 0x038a);

		/// <summary>(300a,0390) VR=SH VM=1 Scan Spot Tune ID</summary>
		DicomTags.ScanSpotTuneID = new DicomTag(0x300a, 0x0390);

		/// <summary>(300a,0392) VR=IS VM=1 Number of Scan Spot Positions</summary>
		DicomTags.NumberOfScanSpotPositions = new DicomTag(0x300a, 0x0392);

		/// <summary>(300a,0394) VR=FL VM=1-n Scan Spot Position Map</summary>
		DicomTags.ScanSpotPositionMap = new DicomTag(0x300a, 0x0394);

		/// <summary>(300a,0396) VR=FL VM=1-n Scan Spot Meterset Weights</summary>
		DicomTags.ScanSpotMetersetWeights = new DicomTag(0x300a, 0x0396);

		/// <summary>(300a,0398) VR=FL VM=2 Scanning Spot Size</summary>
		DicomTags.ScanningSpotSize = new DicomTag(0x300a, 0x0398);

		/// <summary>(300a,039a) VR=IS VM=1 Number of Paintings</summary>
		DicomTags.NumberOfPaintings = new DicomTag(0x300a, 0x039a);

		/// <summary>(300a,03a0) VR=SQ VM=1 Ion Tolerance Table Sequence</summary>
		DicomTags.IonToleranceTableSequence = new DicomTag(0x300a, 0x03a0);

		/// <summary>(300a,03a2) VR=SQ VM=1 Ion Beam Sequence</summary>
		DicomTags.IonBeamSequence = new DicomTag(0x300a, 0x03a2);

		/// <summary>(300a,03a4) VR=SQ VM=1 Ion Beam Limiting Device Sequence</summary>
		DicomTags.IonBeamLimitingDeviceSequence = new DicomTag(0x300a, 0x03a4);

		/// <summary>(300a,03a6) VR=SQ VM=1 Ion Block Sequence</summary>
		DicomTags.IonBlockSequence = new DicomTag(0x300a, 0x03a6);

		/// <summary>(300a,03a8) VR=SQ VM=1 Ion Control Point Sequence</summary>
		DicomTags.IonControlPointSequence = new DicomTag(0x300a, 0x03a8);

		/// <summary>(300a,03aa) VR=SQ VM=1 Ion Wedge Sequence</summary>
		DicomTags.IonWedgeSequence = new DicomTag(0x300a, 0x03aa);

		/// <summary>(300a,03ac) VR=SQ VM=1 Ion Wedge Position Sequence</summary>
		DicomTags.IonWedgePositionSequence = new DicomTag(0x300a, 0x03ac);

		/// <summary>(300a,0401) VR=SQ VM=1 Referenced Setup Image Sequence</summary>
		DicomTags.ReferencedSetupImageSequence = new DicomTag(0x300a, 0x0401);

		/// <summary>(300a,0402) VR=ST VM=1 Setup Image Comment</summary>
		DicomTags.SetupImageComment = new DicomTag(0x300a, 0x0402);

		/// <summary>(300a,0410) VR=SQ VM=1 Motion Synchronization Sequence</summary>
		DicomTags.MotionSynchronizationSequence = new DicomTag(0x300a, 0x0410);

		/// <summary>(300a,0412) VR=FL VM=3 Control Point Orientation</summary>
		DicomTags.ControlPointOrientation = new DicomTag(0x300a, 0x0412);

		/// <summary>(300a,0420) VR=SQ VM=1 General Accessory Sequence</summary>
		DicomTags.GeneralAccessorySequence = new DicomTag(0x300a, 0x0420);

		/// <summary>(300a,0421) VR=CS VM=1 General Accessory ID</summary>
		DicomTags.GeneralAccessoryID = new DicomTag(0x300a, 0x0421);

		/// <summary>(300a,0422) VR=ST VM=1 General Accessory Description</summary>
		DicomTags.GeneralAccessoryDescription = new DicomTag(0x300a, 0x0422);

		/// <summary>(300a,0423) VR=SH VM=1 General Accessory Type</summary>
		DicomTags.GeneralAccessoryType = new DicomTag(0x300a, 0x0423);

		/// <summary>(300a,0424) VR=IS VM=1 General Accessory Number</summary>
		DicomTags.GeneralAccessoryNumber = new DicomTag(0x300a, 0x0424);

		/// <summary>(300c,0002) VR=SQ VM=1 Referenced RT Plan Sequence</summary>
		DicomTags.ReferencedRTPlanSequence = new DicomTag(0x300c, 0x0002);

		/// <summary>(300c,0004) VR=SQ VM=1 Referenced Beam Sequence</summary>
		DicomTags.ReferencedBeamSequence = new DicomTag(0x300c, 0x0004);

		/// <summary>(300c,0006) VR=IS VM=1 Referenced Beam Number</summary>
		DicomTags.ReferencedBeamNumber = new DicomTag(0x300c, 0x0006);

		/// <summary>(300c,0007) VR=IS VM=1 Referenced Reference Image Number</summary>
		DicomTags.ReferencedReferenceImageNumber = new DicomTag(0x300c, 0x0007);

		/// <summary>(300c,0008) VR=DS VM=1 Start Cumulative Meterset Weight</summary>
		DicomTags.StartCumulativeMetersetWeight = new DicomTag(0x300c, 0x0008);

		/// <summary>(300c,0009) VR=DS VM=1 End Cumulative Meterset Weight</summary>
		DicomTags.EndCumulativeMetersetWeight = new DicomTag(0x300c, 0x0009);

		/// <summary>(300c,000a) VR=SQ VM=1 Referenced Brachy Application Setup Sequence</summary>
		DicomTags.ReferencedBrachyApplicationSetupSequence = new DicomTag(0x300c, 0x000a);

		/// <summary>(300c,000c) VR=IS VM=1 Referenced Brachy Application Setup Number</summary>
		DicomTags.ReferencedBrachyApplicationSetupNumber = new DicomTag(0x300c, 0x000c);

		/// <summary>(300c,000e) VR=IS VM=1 Referenced Source Number</summary>
		DicomTags.ReferencedSourceNumber = new DicomTag(0x300c, 0x000e);

		/// <summary>(300c,0020) VR=SQ VM=1 Referenced Fraction Group Sequence</summary>
		DicomTags.ReferencedFractionGroupSequence = new DicomTag(0x300c, 0x0020);

		/// <summary>(300c,0022) VR=IS VM=1 Referenced Fraction Group Number</summary>
		DicomTags.ReferencedFractionGroupNumber = new DicomTag(0x300c, 0x0022);

		/// <summary>(300c,0040) VR=SQ VM=1 Referenced Verification Image Sequence</summary>
		DicomTags.ReferencedVerificationImageSequence = new DicomTag(0x300c, 0x0040);

		/// <summary>(300c,0042) VR=SQ VM=1 Referenced Reference Image Sequence</summary>
		DicomTags.ReferencedReferenceImageSequence = new DicomTag(0x300c, 0x0042);

		/// <summary>(300c,0050) VR=SQ VM=1 Referenced Dose Reference Sequence</summary>
		DicomTags.ReferencedDoseReferenceSequence = new DicomTag(0x300c, 0x0050);

		/// <summary>(300c,0051) VR=IS VM=1 Referenced Dose Reference Number</summary>
		DicomTags.ReferencedDoseReferenceNumber = new DicomTag(0x300c, 0x0051);

		/// <summary>(300c,0055) VR=SQ VM=1 Brachy Referenced Dose Reference Sequence</summary>
		DicomTags.BrachyReferencedDoseReferenceSequence = new DicomTag(0x300c, 0x0055);

		/// <summary>(300c,0060) VR=SQ VM=1 Referenced Structure Set Sequence</summary>
		DicomTags.ReferencedStructureSetSequence = new DicomTag(0x300c, 0x0060);

		/// <summary>(300c,006a) VR=IS VM=1 Referenced Patient Setup Number</summary>
		DicomTags.ReferencedPatientSetupNumber = new DicomTag(0x300c, 0x006a);

		/// <summary>(300c,0080) VR=SQ VM=1 Referenced Dose Sequence</summary>
		DicomTags.ReferencedDoseSequence = new DicomTag(0x300c, 0x0080);

		/// <summary>(300c,00a0) VR=IS VM=1 Referenced Tolerance Table Number</summary>
		DicomTags.ReferencedToleranceTableNumber = new DicomTag(0x300c, 0x00a0);

		/// <summary>(300c,00b0) VR=SQ VM=1 Referenced Bolus Sequence</summary>
		DicomTags.ReferencedBolusSequence = new DicomTag(0x300c, 0x00b0);

		/// <summary>(300c,00c0) VR=IS VM=1 Referenced Wedge Number</summary>
		DicomTags.ReferencedWedgeNumber = new DicomTag(0x300c, 0x00c0);

		/// <summary>(300c,00d0) VR=IS VM=1 Referenced Compensator Number</summary>
		DicomTags.ReferencedCompensatorNumber = new DicomTag(0x300c, 0x00d0);

		/// <summary>(300c,00e0) VR=IS VM=1 Referenced Block Number</summary>
		DicomTags.ReferencedBlockNumber = new DicomTag(0x300c, 0x00e0);

		/// <summary>(300c,00f0) VR=IS VM=1 Referenced Control Point Index</summary>
		DicomTags.ReferencedControlPointIndex = new DicomTag(0x300c, 0x00f0);

		/// <summary>(300c,00f2) VR=SQ VM=1 Referenced Control Point Sequence</summary>
		DicomTags.ReferencedControlPointSequence = new DicomTag(0x300c, 0x00f2);

		/// <summary>(300c,00f4) VR=IS VM=1 Referenced Start Control Point Index</summary>
		DicomTags.ReferencedStartControlPointIndex = new DicomTag(0x300c, 0x00f4);

		/// <summary>(300c,00f6) VR=IS VM=1 Referenced Stop Control Point Index</summary>
		DicomTags.ReferencedStopControlPointIndex = new DicomTag(0x300c, 0x00f6);

		/// <summary>(300c,0100) VR=IS VM=1 Referenced Range Shifter Number</summary>
		DicomTags.ReferencedRangeShifterNumber = new DicomTag(0x300c, 0x0100);

		/// <summary>(300c,0102) VR=IS VM=1 Referenced Lateral Spreading Device Number</summary>
		DicomTags.ReferencedLateralSpreadingDeviceNumber = new DicomTag(0x300c, 0x0102);

		/// <summary>(300c,0104) VR=IS VM=1 Referenced Range Modulator Number</summary>
		DicomTags.ReferencedRangeModulatorNumber = new DicomTag(0x300c, 0x0104);

		/// <summary>(300e,0002) VR=CS VM=1 Approval Status</summary>
		DicomTags.ApprovalStatus = new DicomTag(0x300e, 0x0002);

		/// <summary>(300e,0004) VR=DA VM=1 Review Date</summary>
		DicomTags.ReviewDate = new DicomTag(0x300e, 0x0004);

		/// <summary>(300e,0005) VR=TM VM=1 Review Time</summary>
		DicomTags.ReviewTime = new DicomTag(0x300e, 0x0005);

		/// <summary>(300e,0008) VR=PN VM=1 Reviewer Name</summary>
		DicomTags.ReviewerName = new DicomTag(0x300e, 0x0008);

		/// <summary>(4000,0010) VR=LT VM=1 Arbitrary (Retired)</summary>
		DicomTags.ArbitraryRETIRED = new DicomTag(0x4000, 0x0010);

		/// <summary>(4000,4000) VR=LT VM=1 Text Comments (Retired)</summary>
		DicomTags.TextCommentsRETIRED = new DicomTag(0x4000, 0x4000);

		/// <summary>(4008,0040) VR=SH VM=1 Results ID (Retired)</summary>
		DicomTags.ResultsIDRETIRED = new DicomTag(0x4008, 0x0040);

		/// <summary>(4008,0042) VR=LO VM=1 Results ID Issuer (Retired)</summary>
		DicomTags.ResultsIDIssuerRETIRED = new DicomTag(0x4008, 0x0042);

		/// <summary>(4008,0050) VR=SQ VM=1 Referenced Interpretation Sequence (Retired)</summary>
		DicomTags.ReferencedInterpretationSequenceRETIRED = new DicomTag(0x4008, 0x0050);

		/// <summary>(4008,0100) VR=DA VM=1 Interpretation Recorded Date (Retired)</summary>
		DicomTags.InterpretationRecordedDateRETIRED = new DicomTag(0x4008, 0x0100);

		/// <summary>(4008,0101) VR=TM VM=1 Interpretation Recorded Time (Retired)</summary>
		DicomTags.InterpretationRecordedTimeRETIRED = new DicomTag(0x4008, 0x0101);

		/// <summary>(4008,0102) VR=PN VM=1 Interpretation Recorder (Retired)</summary>
		DicomTags.InterpretationRecorderRETIRED = new DicomTag(0x4008, 0x0102);

		/// <summary>(4008,0103) VR=LO VM=1 Reference to Recorded Sound (Retired)</summary>
		DicomTags.ReferenceToRecordedSoundRETIRED = new DicomTag(0x4008, 0x0103);

		/// <summary>(4008,0108) VR=DA VM=1 Interpretation Transcription Date (Retired)</summary>
		DicomTags.InterpretationTranscriptionDateRETIRED = new DicomTag(0x4008, 0x0108);

		/// <summary>(4008,0109) VR=TM VM=1 Interpretation Transcription Time (Retired)</summary>
		DicomTags.InterpretationTranscriptionTimeRETIRED = new DicomTag(0x4008, 0x0109);

		/// <summary>(4008,010a) VR=PN VM=1 Interpretation Transcriber (Retired)</summary>
		DicomTags.InterpretationTranscriberRETIRED = new DicomTag(0x4008, 0x010a);

		/// <summary>(4008,010b) VR=ST VM=1 Interpretation Text (Retired)</summary>
		DicomTags.InterpretationTextRETIRED = new DicomTag(0x4008, 0x010b);

		/// <summary>(4008,010c) VR=PN VM=1 Interpretation Author (Retired)</summary>
		DicomTags.InterpretationAuthorRETIRED = new DicomTag(0x4008, 0x010c);

		/// <summary>(4008,0111) VR=SQ VM=1 Interpretation Approver Sequence (Retired)</summary>
		DicomTags.InterpretationApproverSequenceRETIRED = new DicomTag(0x4008, 0x0111);

		/// <summary>(4008,0112) VR=DA VM=1 Interpretation Approval Date (Retired)</summary>
		DicomTags.InterpretationApprovalDateRETIRED = new DicomTag(0x4008, 0x0112);

		/// <summary>(4008,0113) VR=TM VM=1 Interpretation Approval Time (Retired)</summary>
		DicomTags.InterpretationApprovalTimeRETIRED = new DicomTag(0x4008, 0x0113);

		/// <summary>(4008,0114) VR=PN VM=1 Physician Approving Interpretation (Retired)</summary>
		DicomTags.PhysicianApprovingInterpretationRETIRED = new DicomTag(0x4008, 0x0114);

		/// <summary>(4008,0115) VR=LT VM=1 Interpretation Diagnosis Description (Retired)</summary>
		DicomTags.InterpretationDiagnosisDescriptionRETIRED = new DicomTag(0x4008, 0x0115);

		/// <summary>(4008,0117) VR=SQ VM=1 Interpretation Diagnosis Code Sequence (Retired)</summary>
		DicomTags.InterpretationDiagnosisCodeSequenceRETIRED = new DicomTag(0x4008, 0x0117);

		/// <summary>(4008,0118) VR=SQ VM=1 Results Distribution List Sequence (Retired)</summary>
		DicomTags.ResultsDistributionListSequenceRETIRED = new DicomTag(0x4008, 0x0118);

		/// <summary>(4008,0119) VR=PN VM=1 Distribution Name (Retired)</summary>
		DicomTags.DistributionNameRETIRED = new DicomTag(0x4008, 0x0119);

		/// <summary>(4008,011a) VR=LO VM=1 Distribution Address (Retired)</summary>
		DicomTags.DistributionAddressRETIRED = new DicomTag(0x4008, 0x011a);

		/// <summary>(4008,0200) VR=SH VM=1 Interpretation ID (Retired)</summary>
		DicomTags.InterpretationIDRETIRED = new DicomTag(0x4008, 0x0200);

		/// <summary>(4008,0202) VR=LO VM=1 Interpretation ID Issuer (Retired)</summary>
		DicomTags.InterpretationIDIssuerRETIRED = new DicomTag(0x4008, 0x0202);

		/// <summary>(4008,0210) VR=CS VM=1 Interpretation Type ID (Retired)</summary>
		DicomTags.InterpretationTypeIDRETIRED = new DicomTag(0x4008, 0x0210);

		/// <summary>(4008,0212) VR=CS VM=1 Interpretation Status ID (Retired)</summary>
		DicomTags.InterpretationStatusIDRETIRED = new DicomTag(0x4008, 0x0212);

		/// <summary>(4008,0300) VR=ST VM=1 Impressions (Retired)</summary>
		DicomTags.ImpressionsRETIRED = new DicomTag(0x4008, 0x0300);

		/// <summary>(4008,4000) VR=ST VM=1 Results Comments (Retired)</summary>
		DicomTags.ResultsCommentsRETIRED = new DicomTag(0x4008, 0x4000);

		/// <summary>(4ffe,0001) VR=SQ VM=1 MAC Parameters Sequence</summary>
		DicomTags.MACParametersSequence = new DicomTag(0x4ffe, 0x0001);

		/// <summary>(5000,0005) VR=US VM=1 Curve Dimensions (Retired)</summary>
		DicomTags.CurveDimensionsRETIRED = new DicomTag(0x5000, 0x0005);

		/// <summary>(5000,0010) VR=US VM=1 Number of Points (Retired)</summary>
		DicomTags.NumberOfPointsRETIRED = new DicomTag(0x5000, 0x0010);

		/// <summary>(5000,0020) VR=CS VM=1 Type of Data (Retired)</summary>
		DicomTags.TypeOfDataRETIRED = new DicomTag(0x5000, 0x0020);

		/// <summary>(5000,0022) VR=LO VM=1 Curve Description (Retired)</summary>
		DicomTags.CurveDescriptionRETIRED = new DicomTag(0x5000, 0x0022);

		/// <summary>(5000,0030) VR=SH VM=1-n Axis Units (Retired)</summary>
		DicomTags.AxisUnitsRETIRED = new DicomTag(0x5000, 0x0030);

		/// <summary>(5000,0040) VR=SH VM=1-n Axis Labels (Retired)</summary>
		DicomTags.AxisLabelsRETIRED = new DicomTag(0x5000, 0x0040);

		/// <summary>(5000,0103) VR=US VM=1 Data Value Representation (Retired)</summary>
		DicomTags.DataValueRepresentationRETIRED = new DicomTag(0x5000, 0x0103);

		/// <summary>(5000,0104) VR=US VM=1-n Minimum Coordinate Value (Retired)</summary>
		DicomTags.MinimumCoordinateValueRETIRED = new DicomTag(0x5000, 0x0104);

		/// <summary>(5000,0105) VR=US VM=1-n Maximum Coordinate Value (Retired)</summary>
		DicomTags.MaximumCoordinateValueRETIRED = new DicomTag(0x5000, 0x0105);

		/// <summary>(5000,0106) VR=SH VM=1-n Curve Range (Retired)</summary>
		DicomTags.CurveRangeRETIRED = new DicomTag(0x5000, 0x0106);

		/// <summary>(5000,0110) VR=US VM=1-n Curve Data Descriptor (Retired)</summary>
		DicomTags.CurveDataDescriptorRETIRED = new DicomTag(0x5000, 0x0110);

		/// <summary>(5000,0112) VR=US VM=1-n Coordinate Start Value (Retired)</summary>
		DicomTags.CoordinateStartValueRETIRED = new DicomTag(0x5000, 0x0112);

		/// <summary>(5000,0114) VR=US VM=1-n Coordinate Step Value (Retired)</summary>
		DicomTags.CoordinateStepValueRETIRED = new DicomTag(0x5000, 0x0114);

		/// <summary>(5000,1001) VR=CS VM=1 Curve Activation Layer (Retired)</summary>
		DicomTags.CurveActivationLayerRETIRED = new DicomTag(0x5000, 0x1001);

		/// <summary>(5000,2000) VR=US VM=1 Audio Type (Retired)</summary>
		DicomTags.AudioTypeRETIRED = new DicomTag(0x5000, 0x2000);

		/// <summary>(5000,2002) VR=US VM=1 Audio Sample Format (Retired)</summary>
		DicomTags.AudioSampleFormatRETIRED = new DicomTag(0x5000, 0x2002);

		/// <summary>(5000,2004) VR=US VM=1 Number of Channels (Retired)</summary>
		DicomTags.NumberOfChannelsRETIRED = new DicomTag(0x5000, 0x2004);

		/// <summary>(5000,2006) VR=UL VM=1 Number of Samples (Retired)</summary>
		DicomTags.NumberOfSamplesRETIRED = new DicomTag(0x5000, 0x2006);

		/// <summary>(5000,2008) VR=UL VM=1 Sample Rate (Retired)</summary>
		DicomTags.SampleRateRETIRED = new DicomTag(0x5000, 0x2008);

		/// <summary>(5000,200a) VR=UL VM=1 Total Time (Retired)</summary>
		DicomTags.TotalTimeRETIRED = new DicomTag(0x5000, 0x200a);

		/// <summary>(5000,200c) VR=OB/OW VM=1 Audio Sample Data (Retired)</summary>
		DicomTags.AudioSampleDataRETIRED = new DicomTag(0x5000, 0x200c);

		/// <summary>(5000,200e) VR=LT VM=1 Audio Comments (Retired)</summary>
		DicomTags.AudioCommentsRETIRED = new DicomTag(0x5000, 0x200e);

		/// <summary>(5000,2500) VR=LO VM=1 Curve Label (Retired)</summary>
		DicomTags.CurveLabelRETIRED = new DicomTag(0x5000, 0x2500);

		/// <summary>(5000,2600) VR=SQ VM=1 Curve Referenced Overlay Sequence (Retired)</summary>
		DicomTags.CurveReferencedOverlaySequenceRETIRED = new DicomTag(0x5000, 0x2600);

		/// <summary>(5000,2610) VR=US VM=1 Curve Referenced Overlay Group (Retired)</summary>
		DicomTags.CurveReferencedOverlayGroupRETIRED = new DicomTag(0x5000, 0x2610);

		/// <summary>(5000,3000) VR=OB/OW VM=1 Curve Data (Retired)</summary>
		DicomTags.CurveDataRETIRED = new DicomTag(0x5000, 0x3000);

		/// <summary>(5200,9229) VR=SQ VM=1 Shared Functional Groups Sequence</summary>
		DicomTags.SharedFunctionalGroupsSequence = new DicomTag(0x5200, 0x9229);

		/// <summary>(5200,9230) VR=SQ VM=1 Per-frame Functional Groups Sequence</summary>
		DicomTags.PerframeFunctionalGroupsSequence = new DicomTag(0x5200, 0x9230);

		/// <summary>(5400,0100) VR=SQ VM=1 Waveform Sequence</summary>
		DicomTags.WaveformSequence = new DicomTag(0x5400, 0x0100);

		/// <summary>(5400,0110) VR=OB/OW VM=1 Channel Minimum Value</summary>
		DicomTags.ChannelMinimumValue = new DicomTag(0x5400, 0x0110);

		/// <summary>(5400,0112) VR=OB/OW VM=1 Channel Maximum Value</summary>
		DicomTags.ChannelMaximumValue = new DicomTag(0x5400, 0x0112);

		/// <summary>(5400,1004) VR=US VM=1 Waveform Bits Allocated</summary>
		DicomTags.WaveformBitsAllocated = new DicomTag(0x5400, 0x1004);

		/// <summary>(5400,1006) VR=CS VM=1 Waveform Sample Interpretation</summary>
		DicomTags.WaveformSampleInterpretation = new DicomTag(0x5400, 0x1006);

		/// <summary>(5400,100a) VR=OB/OW VM=1 Waveform Padding Value</summary>
		DicomTags.WaveformPaddingValue = new DicomTag(0x5400, 0x100a);

		/// <summary>(5400,1010) VR=OB/OW VM=1 Waveform Data</summary>
		DicomTags.WaveformData = new DicomTag(0x5400, 0x1010);

		/// <summary>(5600,0010) VR=OF VM=1 First Order Phase Correction Angle</summary>
		DicomTags.FirstOrderPhaseCorrectionAngle = new DicomTag(0x5600, 0x0010);

		/// <summary>(5600,0020) VR=OF VM=1 Spectroscopy Data</summary>
		DicomTags.SpectroscopyData = new DicomTag(0x5600, 0x0020);

		/// <summary>(6000,0010) VR=US VM=1 Overlay Rows</summary>
		DicomTags.OverlayRows = new DicomTag(0x6000, 0x0010);

		/// <summary>(6000,0011) VR=US VM=1 Overlay Columns</summary>
		DicomTags.OverlayColumns = new DicomTag(0x6000, 0x0011);

		/// <summary>(6000,0012) VR=US VM=1 Overlay Planes (Retired)</summary>
		DicomTags.OverlayPlanesRETIRED = new DicomTag(0x6000, 0x0012);

		/// <summary>(6000,0015) VR=IS VM=1 Number of Frames in Overlay</summary>
		DicomTags.NumberOfFramesInOverlay = new DicomTag(0x6000, 0x0015);

		/// <summary>(6000,0022) VR=LO VM=1 Overlay Description</summary>
		DicomTags.OverlayDescription = new DicomTag(0x6000, 0x0022);

		/// <summary>(6000,0040) VR=CS VM=1 Overlay Type</summary>
		DicomTags.OverlayType = new DicomTag(0x6000, 0x0040);

		/// <summary>(6000,0045) VR=LO VM=1 Overlay Subtype</summary>
		DicomTags.OverlaySubtype = new DicomTag(0x6000, 0x0045);

		/// <summary>(6000,0050) VR=SS VM=2 Overlay Origin</summary>
		DicomTags.OverlayOrigin = new DicomTag(0x6000, 0x0050);

		/// <summary>(6000,0051) VR=US VM=1 Image Frame Origin</summary>
		DicomTags.ImageFrameOrigin = new DicomTag(0x6000, 0x0051);

		/// <summary>(6000,0052) VR=US VM=1 Overlay Plane Origin (Retired)</summary>
		DicomTags.OverlayPlaneOriginRETIRED = new DicomTag(0x6000, 0x0052);

		/// <summary>(6000,0060) VR=CS VM=1 Overlay Compression Code (Retired)</summary>
		DicomTags.OverlayCompressionCodeRETIRED = new DicomTag(0x6000, 0x0060);

		/// <summary>(6000,0061) VR=SH VM=1 Overlay Compression Originator (Retired)</summary>
		DicomTags.OverlayCompressionOriginatorRETIRED = new DicomTag(0x6000, 0x0061);

		/// <summary>(6000,0062) VR=SH VM=1 Overlay Compression Label (Retired)</summary>
		DicomTags.OverlayCompressionLabelRETIRED = new DicomTag(0x6000, 0x0062);

		/// <summary>(6000,0063) VR=CS VM=1 Overlay Compression Description (Retired)</summary>
		DicomTags.OverlayCompressionDescriptionRETIRED = new DicomTag(0x6000, 0x0063);

		/// <summary>(6000,0066) VR=AT VM=1-n Overlay Compression Step Pointers (Retired)</summary>
		DicomTags.OverlayCompressionStepPointersRETIRED = new DicomTag(0x6000, 0x0066);

		/// <summary>(6000,0068) VR=US VM=1 Overlay Repeat Interval (Retired)</summary>
		DicomTags.OverlayRepeatIntervalRETIRED = new DicomTag(0x6000, 0x0068);

		/// <summary>(6000,0069) VR=US VM=1 Overlay Bits Grouped (Retired)</summary>
		DicomTags.OverlayBitsGroupedRETIRED = new DicomTag(0x6000, 0x0069);

		/// <summary>(6000,0100) VR=US VM=1 Overlay Bits Allocated</summary>
		DicomTags.OverlayBitsAllocated = new DicomTag(0x6000, 0x0100);

		/// <summary>(6000,0102) VR=US VM=1 Overlay Bit Position</summary>
		DicomTags.OverlayBitPosition = new DicomTag(0x6000, 0x0102);

		/// <summary>(6000,0110) VR=CS VM=1 Overlay Format (Retired)</summary>
		DicomTags.OverlayFormatRETIRED = new DicomTag(0x6000, 0x0110);

		/// <summary>(6000,0200) VR=US VM=1 Overlay Location (Retired)</summary>
		DicomTags.OverlayLocationRETIRED = new DicomTag(0x6000, 0x0200);

		/// <summary>(6000,0800) VR=CS VM=1-n Overlay Code Label (Retired)</summary>
		DicomTags.OverlayCodeLabelRETIRED = new DicomTag(0x6000, 0x0800);

		/// <summary>(6000,0802) VR=US VM=1 Overlay Number of Tables (Retired)</summary>
		DicomTags.OverlayNumberOfTablesRETIRED = new DicomTag(0x6000, 0x0802);

		/// <summary>(6000,0803) VR=AT VM=1-n Overlay Code Table Location (Retired)</summary>
		DicomTags.OverlayCodeTableLocationRETIRED = new DicomTag(0x6000, 0x0803);

		/// <summary>(6000,0804) VR=US VM=1 Overlay Bits For Code Word (Retired)</summary>
		DicomTags.OverlayBitsForCodeWordRETIRED = new DicomTag(0x6000, 0x0804);

		/// <summary>(6000,1001) VR=CS VM=1 Overlay Activation Layer</summary>
		DicomTags.OverlayActivationLayer = new DicomTag(0x6000, 0x1001);

		/// <summary>(6000,1100) VR=US VM=1 Overlay Descriptor - Gray (Retired)</summary>
		DicomTags.OverlayDescriptorGrayRETIRED = new DicomTag(0x6000, 0x1100);

		/// <summary>(6000,1101) VR=US VM=1 Overlay Descriptor - Red (Retired)</summary>
		DicomTags.OverlayDescriptorRedRETIRED = new DicomTag(0x6000, 0x1101);

		/// <summary>(6000,1102) VR=US VM=1 Overlay Descriptor - Green (Retired)</summary>
		DicomTags.OverlayDescriptorGreenRETIRED = new DicomTag(0x6000, 0x1102);

		/// <summary>(6000,1103) VR=US VM=1 Overlay Descriptor - Blue (Retired)</summary>
		DicomTags.OverlayDescriptorBlueRETIRED = new DicomTag(0x6000, 0x1103);

		/// <summary>(6000,1200) VR=US VM=1-n Overlays - Gray (Retired)</summary>
		DicomTags.OverlaysGrayRETIRED = new DicomTag(0x6000, 0x1200);

		/// <summary>(6000,1201) VR=US VM=1-n Overlays - Red (Retired)</summary>
		DicomTags.OverlaysRedRETIRED = new DicomTag(0x6000, 0x1201);

		/// <summary>(6000,1202) VR=US VM=1-n Overlays - Green (Retired)</summary>
		DicomTags.OverlaysGreenRETIRED = new DicomTag(0x6000, 0x1202);

		/// <summary>(6000,1203) VR=US VM=1-n Overlays - Blue (Retired)</summary>
		DicomTags.OverlaysBlueRETIRED = new DicomTag(0x6000, 0x1203);

		/// <summary>(6000,1301) VR=IS VM=1 ROI Area</summary>
		DicomTags.ROIArea = new DicomTag(0x6000, 0x1301);

		/// <summary>(6000,1302) VR=DS VM=1 ROI Mean</summary>
		DicomTags.ROIMean = new DicomTag(0x6000, 0x1302);

		/// <summary>(6000,1303) VR=DS VM=1 ROI Standard Deviation</summary>
		DicomTags.ROIStandardDeviation = new DicomTag(0x6000, 0x1303);

		/// <summary>(6000,1500) VR=LO VM=1 Overlay Label</summary>
		DicomTags.OverlayLabel = new DicomTag(0x6000, 0x1500);

		/// <summary>(6000,3000) VR=OB/OW VM=1 Overlay Data</summary>
		DicomTags.OverlayData = new DicomTag(0x6000, 0x3000);

		/// <summary>(6000,4000) VR=LT VM=1 Overlay Comments (Retired)</summary>
		DicomTags.OverlayCommentsRETIRED = new DicomTag(0x6000, 0x4000);

		/// <summary>(7fe0,0010) VR=OB/OW VM=1 Pixel Data</summary>
		DicomTags.PixelData = new DicomTag(0x7fe0, 0x0010);

		/// <summary>(7fe0,0020) VR=OW VM=1 Coefficients SDVN (Retired)</summary>
		DicomTags.CoefficientsSDVNRETIRED = new DicomTag(0x7fe0, 0x0020);

		/// <summary>(7fe0,0030) VR=OW VM=1 Coefficients SDHN (Retired)</summary>
		DicomTags.CoefficientsSDHNRETIRED = new DicomTag(0x7fe0, 0x0030);

		/// <summary>(7fe0,0040) VR=OW VM=1 Coefficients SDDN (Retired)</summary>
		DicomTags.CoefficientsSDDNRETIRED = new DicomTag(0x7fe0, 0x0040);

		/// <summary>(7f00,0010) VR=OB/OW VM=1 Variable Pixel Data (Retired)</summary>
		DicomTags.VariablePixelDataRETIRED = new DicomTag(0x7f00, 0x0010);

		/// <summary>(7f00,0011) VR=US VM=1 Variable Next Data Group (Retired)</summary>
		DicomTags.VariableNextDataGroupRETIRED = new DicomTag(0x7f00, 0x0011);

		/// <summary>(7f00,0020) VR=OW VM=1 Variable Coefficients SDVN (Retired)</summary>
		DicomTags.VariableCoefficientsSDVNRETIRED = new DicomTag(0x7f00, 0x0020);

		/// <summary>(7f00,0030) VR=OW VM=1 Variable Coefficients SDHN (Retired)</summary>
		DicomTags.VariableCoefficientsSDHNRETIRED = new DicomTag(0x7f00, 0x0030);

		/// <summary>(7f00,0040) VR=OW VM=1 Variable Coefficients SDDN (Retired)</summary>
		DicomTags.VariableCoefficientsSDDNRETIRED = new DicomTag(0x7f00, 0x0040);

		/// <summary>(fffa,fffa) VR=SQ VM=1 Digital Signatures Sequence</summary>
		DicomTags.DigitalSignaturesSequence = new DicomTag(0xfffa, 0xfffa);

		/// <summary>(fffc,fffc) VR=OB VM=1 Data Set Trailing Padding</summary>
		DicomTags.DataSetTrailingPadding = new DicomTag(0xfffc, 0xfffc);

		/// <summary>(fffe,e000) VR=NONE VM=1 Item</summary>
		DicomTags.Item = new DicomTag(0xfffe, 0xe000);

		/// <summary>(fffe,e00d) VR=NONE VM=1 Item Delimitation Item</summary>
		DicomTags.ItemDelimitationItem = new DicomTag(0xfffe, 0xe00d);

		/// <summary>(fffe,e0dd) VR=NONE VM=1 Sequence Delimitation Item</summary>
		DicomTags.SequenceDelimitationItem = new DicomTag(0xfffe, 0xe0dd);
		//#endregion
	
	
	
function DicomConstTags()
{

}
		/// <summary>(0000,0002) VR=UI VM=1 Affected SOP Class UID</summary>
		DicomConstTags.AffectedSOPClassUID = 0x00000002;

		/// <summary>(0000,0003) VR=UI VM=1 Requested SOP Class UID</summary>
		DicomConstTags.RequestedSOPClassUID = 0x00000003;

		/// <summary>(0000,0100) VR=US VM=1 Command Field</summary>
		DicomConstTags.CommandField = 0x00000100;

		/// <summary>(0000,0110) VR=US VM=1 Message ID</summary>
		DicomConstTags.MessageID = 0x00000110;

		/// <summary>(0000,0120) VR=US VM=1 Message ID Being Responded To</summary>
		DicomConstTags.MessageIDBeingRespondedTo = 0x00000120;

		/// <summary>(0000,0600) VR=AE VM=1 Move Destination</summary>
		DicomConstTags.MoveDestination = 0x00000600;

		/// <summary>(0000,0700) VR=US VM=1 Priority</summary>
		DicomConstTags.Priority = 0x00000700;

		/// <summary>(0000,0800) VR=US VM=1 Data Set Type</summary>
		DicomConstTags.DataSetType = 0x00000800;

		/// <summary>(0000,0900) VR=US VM=1 Status</summary>
		DicomConstTags.Status = 0x00000900;

		/// <summary>(0000,0901) VR=AT VM=1-n Offending Element</summary>
		DicomConstTags.OffendingElement = 0x00000901;

		/// <summary>(0000,0902) VR=LO VM=1 Error Comment</summary>
		DicomConstTags.ErrorComment = 0x00000902;

		/// <summary>(0000,0903) VR=US VM=1 Error ID</summary>
		DicomConstTags.ErrorID = 0x00000903;

		/// <summary>(0000,1000) VR=UI VM=1 Affected SOP Instance UID</summary>
		DicomConstTags.AffectedSOPInstanceUID = 0x00001000;

		/// <summary>(0000,1001) VR=UI VM=1 Requested SOP Instance UID</summary>
		DicomConstTags.RequestedSOPInstanceUID = 0x00001001;

		/// <summary>(0000,1002) VR=US VM=1 Event Type ID</summary>
		DicomConstTags.EventTypeID = 0x00001002;

		/// <summary>(0000,1005) VR=AT VM=1-n Attribute Identifier List</summary>
		DicomConstTags.AttributeIdentifierList = 0x00001005;

		/// <summary>(0000,1008) VR=US VM=1 Action Type ID</summary>
		DicomConstTags.ActionTypeID = 0x00001008;

		/// <summary>(0000,1020) VR=US VM=1 Number of Remaining Sub-operations</summary>
		DicomConstTags.NumberOfRemainingSuboperations = 0x00001020;

		/// <summary>(0000,1021) VR=US VM=1 Number of Completed Sub-operations</summary>
		DicomConstTags.NumberOfCompletedSuboperations = 0x00001021;

		/// <summary>(0000,1022) VR=US VM=1 Number of Failed Sub-operations</summary>
		DicomConstTags.NumberOfFailedSuboperations = 0x00001022;

		/// <summary>(0000,1023) VR=US VM=1 Number of Warning Sub-operations</summary>
		DicomConstTags.NumberOfWarningSuboperations = 0x00001023;

		/// <summary>(0000,1030) VR=AE VM=1 Move Originator Application Entity Title</summary>
		DicomConstTags.MoveOriginatorApplicationEntityTitle = 0x00001030;

		/// <summary>(0000,1031) VR=US VM=1 Move Originator Message ID</summary>
		DicomConstTags.MoveOriginatorMessageID = 0x00001031;

		/// <summary>(0002,0001) VR=OB VM=1 File Meta Information Version</summary>
		DicomConstTags.FileMetaInformationVersion = 0x00020001;

		/// <summary>(0002,0002) VR=UI VM=1 Media Storage SOP Class UID</summary>
		DicomConstTags.MediaStorageSOPClassUID = 0x00020002;

		/// <summary>(0002,0003) VR=UI VM=1 Media Storage SOP Instance UID</summary>
		DicomConstTags.MediaStorageSOPInstanceUID = 0x00020003;

		/// <summary>(0002,0010) VR=UI VM=1 Transfer Syntax UID</summary>
		DicomConstTags.TransferSyntaxUID = 0x00020010;

		/// <summary>(0002,0012) VR=UI VM=1 Implementation Class UID</summary>
		DicomConstTags.ImplementationClassUID = 0x00020012;

		/// <summary>(0002,0013) VR=SH VM=1 Implementation Version Name</summary>
		DicomConstTags.ImplementationVersionName = 0x00020013;

		/// <summary>(0002,0016) VR=AE VM=1 Source Application Entity Title</summary>
		DicomConstTags.SourceApplicationEntityTitle = 0x00020016;

		/// <summary>(0002,0100) VR=UI VM=1 Private Information Creator UID</summary>
		DicomConstTags.PrivateInformationCreatorUID = 0x00020100;

		/// <summary>(0002,0102) VR=OB VM=1 Private Information</summary>
		DicomConstTags.PrivateInformation = 0x00020102;

		/// <summary>(0004,1130) VR=CS VM=1 File-set ID</summary>
		DicomConstTags.FilesetID = 0x00041130;

		/// <summary>(0004,1141) VR=CS VM=1-8 File-set Descriptor File ID</summary>
		DicomConstTags.FilesetDescriptorFileID = 0x00041141;

		/// <summary>(0004,1142) VR=CS VM=1 Specific Character Set of File-set Descriptor File</summary>
		DicomConstTags.SpecificCharacterSetOfFilesetDescriptorFile = 0x00041142;

		/// <summary>(0004,1200) VR=UL VM=1 Offset of the First Directory Record of the Root Directory Entity</summary>
		DicomConstTags.OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity = 0x00041200;

		/// <summary>(0004,1202) VR=UL VM=1 Offset of the Last Directory Record of the Root Directory Entity</summary>
		DicomConstTags.OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity = 0x00041202;

		/// <summary>(0004,1212) VR=US VM=1 File-set Consistency Flag</summary>
		DicomConstTags.FilesetConsistencyFlag = 0x00041212;

		/// <summary>(0004,1220) VR=SQ VM=1 Directory Record Sequence</summary>
		DicomConstTags.DirectoryRecordSequence = 0x00041220;

		/// <summary>(0004,1400) VR=UL VM=1 Offset of the Next Directory Record</summary>
		DicomConstTags.OffsetOfTheNextDirectoryRecord = 0x00041400;

		/// <summary>(0004,1410) VR=US VM=1 Record In-use Flag</summary>
		DicomConstTags.RecordInuseFlag = 0x00041410;

		/// <summary>(0004,1420) VR=UL VM=1 Offset of Referenced Lower-Level Directory Entity</summary>
		DicomConstTags.OffsetOfReferencedLowerLevelDirectoryEntity = 0x00041420;

		/// <summary>(0004,1430) VR=CS VM=1 Directory Record Type</summary>
		DicomConstTags.DirectoryRecordType = 0x00041430;

		/// <summary>(0004,1432) VR=UI VM=1 Private Record UID</summary>
		DicomConstTags.PrivateRecordUID = 0x00041432;

		/// <summary>(0004,1500) VR=CS VM=1-8 Referenced File ID</summary>
		DicomConstTags.ReferencedFileID = 0x00041500;

		/// <summary>(0004,1510) VR=UI VM=1 Referenced SOP Class UID in File</summary>
		DicomConstTags.ReferencedSOPClassUIDInFile = 0x00041510;

		/// <summary>(0004,1511) VR=UI VM=1 Referenced SOP Instance UID in File</summary>
		DicomConstTags.ReferencedSOPInstanceUIDInFile = 0x00041511;

		/// <summary>(0004,1512) VR=UI VM=1 Referenced Transfer Syntax UID in File</summary>
		DicomConstTags.ReferencedTransferSyntaxUIDInFile = 0x00041512;

		/// <summary>(0004,151a) VR=UI VM=1-n Referenced Related General SOP Class UID in File</summary>
		DicomConstTags.ReferencedRelatedGeneralSOPClassUIDInFile = 0x0004151a;

		/// <summary>(0008,0005) VR=CS VM=1-n Specific Character Set</summary>
		DicomConstTags.SpecificCharacterSet = 0x00080005;

		/// <summary>(0008,0008) VR=CS VM=2-n Image Type</summary>
		DicomConstTags.ImageType = 0x00080008;

		/// <summary>(0008,0012) VR=DA VM=1 Instance Creation Date</summary>
		DicomConstTags.InstanceCreationDate = 0x00080012;

		/// <summary>(0008,0013) VR=TM VM=1 Instance Creation Time</summary>
		DicomConstTags.InstanceCreationTime = 0x00080013;

		/// <summary>(0008,0014) VR=UI VM=1 Instance Creator UID</summary>
		DicomConstTags.InstanceCreatorUID = 0x00080014;

		/// <summary>(0008,0016) VR=UI VM=1 SOP Class UID</summary>
		DicomConstTags.SOPClassUID = 0x00080016;

		/// <summary>(0008,0018) VR=UI VM=1 SOP Instance UID</summary>
		DicomConstTags.SOPInstanceUID = 0x00080018;

		/// <summary>(0008,001a) VR=UI VM=1-n Related General SOP Class UID</summary>
		DicomConstTags.RelatedGeneralSOPClassUID = 0x0008001a;

		/// <summary>(0008,001b) VR=UI VM=1 Original Specialized SOP Class UID</summary>
		DicomConstTags.OriginalSpecializedSOPClassUID = 0x0008001b;

		/// <summary>(0008,0020) VR=DA VM=1 Study Date</summary>
		DicomConstTags.StudyDate = 0x00080020;

		/// <summary>(0008,0021) VR=DA VM=1 Series Date</summary>
		DicomConstTags.SeriesDate = 0x00080021;

		/// <summary>(0008,0022) VR=DA VM=1 Acquisition Date</summary>
		DicomConstTags.AcquisitionDate = 0x00080022;

		/// <summary>(0008,0023) VR=DA VM=1 Content Date</summary>
		DicomConstTags.ContentDate = 0x00080023;

		/// <summary>(0008,002a) VR=DT VM=1 Acquisition DateTime</summary>
		DicomConstTags.AcquisitionDateTime = 0x0008002a;

		/// <summary>(0008,0030) VR=TM VM=1 Study Time</summary>
		DicomConstTags.StudyTime = 0x00080030;

		/// <summary>(0008,0031) VR=TM VM=1 Series Time</summary>
		DicomConstTags.SeriesTime = 0x00080031;

		/// <summary>(0008,0032) VR=TM VM=1 Acquisition Time</summary>
		DicomConstTags.AcquisitionTime = 0x00080032;

		/// <summary>(0008,0033) VR=TM VM=1 Content Time</summary>
		DicomConstTags.ContentTime = 0x00080033;

		/// <summary>(0008,0050) VR=SH VM=1 Accession Number</summary>
		DicomConstTags.AccessionNumber = 0x00080050;

		/// <summary>(0008,0052) VR=CS VM=1 Query/Retrieve Level</summary>
		DicomConstTags.QueryRetrieveLevel = 0x00080052;

		/// <summary>(0008,0054) VR=AE VM=1-n Retrieve AE Title</summary>
		DicomConstTags.RetrieveAETitle = 0x00080054;

		/// <summary>(0008,0056) VR=CS VM=1 Instance Availability</summary>
		DicomConstTags.InstanceAvailability = 0x00080056;

		/// <summary>(0008,0058) VR=UI VM=1-n Failed SOP Instance UID List</summary>
		DicomConstTags.FailedSOPInstanceUIDList = 0x00080058;

		/// <summary>(0008,0060) VR=CS VM=1 Modality</summary>
		DicomConstTags.Modality = 0x00080060;

		/// <summary>(0008,0061) VR=CS VM=1-n Modalities in Study</summary>
		DicomConstTags.ModalitiesInStudy = 0x00080061;

		/// <summary>(0008,0062) VR=UI VM=1-n SOP Classes in Study</summary>
		DicomConstTags.SOPClassesInStudy = 0x00080062;

		/// <summary>(0008,0064) VR=CS VM=1 Conversion Type</summary>
		DicomConstTags.ConversionType = 0x00080064;

		/// <summary>(0008,0068) VR=CS VM=1 Presentation Intent Type</summary>
		DicomConstTags.PresentationIntentType = 0x00080068;

		/// <summary>(0008,0070) VR=LO VM=1 Manufacturer</summary>
		DicomConstTags.Manufacturer = 0x00080070;

		/// <summary>(0008,0080) VR=LO VM=1 Institution Name</summary>
		DicomConstTags.InstitutionName = 0x00080080;

		/// <summary>(0008,0081) VR=ST VM=1 Institution Address</summary>
		DicomConstTags.InstitutionAddress = 0x00080081;

		/// <summary>(0008,0082) VR=SQ VM=1 Institution Code Sequence</summary>
		DicomConstTags.InstitutionCodeSequence = 0x00080082;

		/// <summary>(0008,0090) VR=PN VM=1 Referring Physician's Name</summary>
		DicomConstTags.ReferringPhysiciansName = 0x00080090;

		/// <summary>(0008,0092) VR=ST VM=1 Referring Physician's Address</summary>
		DicomConstTags.ReferringPhysiciansAddress = 0x00080092;

		/// <summary>(0008,0094) VR=SH VM=1-n Referring Physician's Telephone Numbers</summary>
		DicomConstTags.ReferringPhysiciansTelephoneNumbers = 0x00080094;

		/// <summary>(0008,0096) VR=SQ VM=1 Referring Physician Identification Sequence</summary>
		DicomConstTags.ReferringPhysicianIdentificationSequence = 0x00080096;

		/// <summary>(0008,0100) VR=SH VM=1 Code Value</summary>
		DicomConstTags.CodeValue = 0x00080100;

		/// <summary>(0008,0102) VR=SH VM=1 Coding Scheme Designator</summary>
		DicomConstTags.CodingSchemeDesignator = 0x00080102;

		/// <summary>(0008,0103) VR=SH VM=1 Coding Scheme Version</summary>
		DicomConstTags.CodingSchemeVersion = 0x00080103;

		/// <summary>(0008,0104) VR=LO VM=1 Code Meaning</summary>
		DicomConstTags.CodeMeaning = 0x00080104;

		/// <summary>(0008,0105) VR=CS VM=1 Mapping Resource</summary>
		DicomConstTags.MappingResource = 0x00080105;

		/// <summary>(0008,0106) VR=DT VM=1 Context Group Version</summary>
		DicomConstTags.ContextGroupVersion = 0x00080106;

		/// <summary>(0008,0107) VR=DT VM=1 Context Group Local Version</summary>
		DicomConstTags.ContextGroupLocalVersion = 0x00080107;

		/// <summary>(0008,010b) VR=CS VM=1 Context Group Extension Flag</summary>
		DicomConstTags.ContextGroupExtensionFlag = 0x0008010b;

		/// <summary>(0008,010c) VR=UI VM=1 Coding Scheme UID</summary>
		DicomConstTags.CodingSchemeUID = 0x0008010c;

		/// <summary>(0008,010d) VR=UI VM=1 Context Group Extension Creator UID</summary>
		DicomConstTags.ContextGroupExtensionCreatorUID = 0x0008010d;

		/// <summary>(0008,010f) VR=CS VM=1 Context Identifier</summary>
		DicomConstTags.ContextIdentifier = 0x0008010f;

		/// <summary>(0008,0110) VR=SQ VM=1 Coding Scheme Identification Sequence</summary>
		DicomConstTags.CodingSchemeIdentificationSequence = 0x00080110;

		/// <summary>(0008,0112) VR=LO VM=1 Coding Scheme Registry</summary>
		DicomConstTags.CodingSchemeRegistry = 0x00080112;

		/// <summary>(0008,0114) VR=ST VM=1 Coding Scheme External ID</summary>
		DicomConstTags.CodingSchemeExternalID = 0x00080114;

		/// <summary>(0008,0115) VR=ST VM=1 Coding Scheme Name</summary>
		DicomConstTags.CodingSchemeName = 0x00080115;

		/// <summary>(0008,0116) VR=ST VM=1 Coding Scheme Responsible Organization</summary>
		DicomConstTags.CodingSchemeResponsibleOrganization = 0x00080116;

		/// <summary>(0008,0201) VR=SH VM=1 Timezone Offset From UTC</summary>
		DicomConstTags.TimezoneOffsetFromUTC = 0x00080201;

		/// <summary>(0008,1010) VR=SH VM=1 Station Name</summary>
		DicomConstTags.StationName = 0x00081010;

		/// <summary>(0008,1030) VR=LO VM=1 Study Description</summary>
		DicomConstTags.StudyDescription = 0x00081030;

		/// <summary>(0008,1032) VR=SQ VM=1 Procedure Code Sequence</summary>
		DicomConstTags.ProcedureCodeSequence = 0x00081032;

		/// <summary>(0008,103e) VR=LO VM=1 Series Description</summary>
		DicomConstTags.SeriesDescription = 0x0008103e;

		/// <summary>(0008,1040) VR=LO VM=1 Institutional Department Name</summary>
		DicomConstTags.InstitutionalDepartmentName = 0x00081040;

		/// <summary>(0008,1048) VR=PN VM=1-n Physician(s) of Record</summary>
		DicomConstTags.PhysiciansOfRecord = 0x00081048;

		/// <summary>(0008,1049) VR=SQ VM=1 Physician(s) of Record Identification Sequence</summary>
		DicomConstTags.PhysiciansOfRecordIdentificationSequence = 0x00081049;

		/// <summary>(0008,1050) VR=PN VM=1-n Performing Physician's Name</summary>
		DicomConstTags.PerformingPhysiciansName = 0x00081050;

		/// <summary>(0008,1052) VR=SQ VM=1 Performing Physician Identification Sequence</summary>
		DicomConstTags.PerformingPhysicianIdentificationSequence = 0x00081052;

		/// <summary>(0008,1060) VR=PN VM=1-n Name of Physician(s) Reading Study</summary>
		DicomConstTags.NameOfPhysiciansReadingStudy = 0x00081060;

		/// <summary>(0008,1062) VR=SQ VM=1 Physician(s) Reading Study Identification Sequence</summary>
		DicomConstTags.PhysiciansReadingStudyIdentificationSequence = 0x00081062;

		/// <summary>(0008,1070) VR=PN VM=1-n Operators' Name</summary>
		DicomConstTags.OperatorsName = 0x00081070;

		/// <summary>(0008,1072) VR=SQ VM=1 Operator Identification Sequence</summary>
		DicomConstTags.OperatorIdentificationSequence = 0x00081072;

		/// <summary>(0008,1080) VR=LO VM=1-n Admitting Diagnoses Description</summary>
		DicomConstTags.AdmittingDiagnosesDescription = 0x00081080;

		/// <summary>(0008,1084) VR=SQ VM=1 Admitting Diagnoses Code Sequence</summary>
		DicomConstTags.AdmittingDiagnosesCodeSequence = 0x00081084;

		/// <summary>(0008,1090) VR=LO VM=1 Manufacturer's Model Name</summary>
		DicomConstTags.ManufacturersModelName = 0x00081090;

		/// <summary>(0008,1110) VR=SQ VM=1 Referenced Study Sequence</summary>
		DicomConstTags.ReferencedStudySequence = 0x00081110;

		/// <summary>(0008,1111) VR=SQ VM=1 Referenced Performed Procedure Step Sequence</summary>
		DicomConstTags.ReferencedPerformedProcedureStepSequence = 0x00081111;

		/// <summary>(0008,1115) VR=SQ VM=1 Referenced Series Sequence</summary>
		DicomConstTags.ReferencedSeriesSequence = 0x00081115;

		/// <summary>(0008,1120) VR=SQ VM=1 Referenced Patient Sequence</summary>
		DicomConstTags.ReferencedPatientSequence = 0x00081120;

		/// <summary>(0008,1125) VR=SQ VM=1 Referenced Visit Sequence</summary>
		DicomConstTags.ReferencedVisitSequence = 0x00081125;

		/// <summary>(0008,113a) VR=SQ VM=1 Referenced Waveform Sequence</summary>
		DicomConstTags.ReferencedWaveformSequence = 0x0008113a;

		/// <summary>(0008,1140) VR=SQ VM=1 Referenced Image Sequence</summary>
		DicomConstTags.ReferencedImageSequence = 0x00081140;

		/// <summary>(0008,114a) VR=SQ VM=1 Referenced Instance Sequence</summary>
		DicomConstTags.ReferencedInstanceSequence = 0x0008114a;

		/// <summary>(0008,114b) VR=SQ VM=1 Referenced Real World Value Mapping Instance Sequence</summary>
		DicomConstTags.ReferencedRealWorldValueMappingInstanceSequence = 0x0008114b;

		/// <summary>(0008,1150) VR=UI VM=1 Referenced SOP Class UID</summary>
		DicomConstTags.ReferencedSOPClassUID = 0x00081150;

		/// <summary>(0008,1155) VR=UI VM=1 Referenced SOP Instance UID</summary>
		DicomConstTags.ReferencedSOPInstanceUID = 0x00081155;

		/// <summary>(0008,115a) VR=UI VM=1-n SOP Classes Supported</summary>
		DicomConstTags.SOPClassesSupported = 0x0008115a;

		/// <summary>(0008,1160) VR=IS VM=1-n Referenced Frame Number</summary>
		DicomConstTags.ReferencedFrameNumber = 0x00081160;

		/// <summary>(0008,1195) VR=UI VM=1 Transaction UID</summary>
		DicomConstTags.TransactionUID = 0x00081195;

		/// <summary>(0008,1197) VR=US VM=1 Failure Reason</summary>
		DicomConstTags.FailureReason = 0x00081197;

		/// <summary>(0008,1198) VR=SQ VM=1 Failed SOP Sequence</summary>
		DicomConstTags.FailedSOPSequence = 0x00081198;

		/// <summary>(0008,1199) VR=SQ VM=1 Referenced SOP Sequence</summary>
		DicomConstTags.ReferencedSOPSequence = 0x00081199;

		/// <summary>(0008,1200) VR=SQ VM=1 Studies Containing Other Referenced Instances Sequence</summary>
		DicomConstTags.StudiesContainingOtherReferencedInstancesSequence = 0x00081200;

		/// <summary>(0008,1250) VR=SQ VM=1 Related Series Sequence</summary>
		DicomConstTags.RelatedSeriesSequence = 0x00081250;

		/// <summary>(0008,2111) VR=ST VM=1 Derivation Description</summary>
		DicomConstTags.DerivationDescription = 0x00082111;

		/// <summary>(0008,2112) VR=SQ VM=1 Source Image Sequence</summary>
		DicomConstTags.SourceImageSequence = 0x00082112;

		/// <summary>(0008,2120) VR=SH VM=1 Stage Name</summary>
		DicomConstTags.StageName = 0x00082120;

		/// <summary>(0008,2122) VR=IS VM=1 Stage Number</summary>
		DicomConstTags.StageNumber = 0x00082122;

		/// <summary>(0008,2124) VR=IS VM=1 Number of Stages</summary>
		DicomConstTags.NumberOfStages = 0x00082124;

		/// <summary>(0008,2127) VR=SH VM=1 View Name</summary>
		DicomConstTags.ViewName = 0x00082127;

		/// <summary>(0008,2128) VR=IS VM=1 View Number</summary>
		DicomConstTags.ViewNumber = 0x00082128;

		/// <summary>(0008,2129) VR=IS VM=1 Number of Event Timers</summary>
		DicomConstTags.NumberOfEventTimers = 0x00082129;

		/// <summary>(0008,212a) VR=IS VM=1 Number of Views in Stage</summary>
		DicomConstTags.NumberOfViewsInStage = 0x0008212a;

		/// <summary>(0008,2130) VR=DS VM=1-n Event Elapsed Time(s)</summary>
		DicomConstTags.EventElapsedTimes = 0x00082130;

		/// <summary>(0008,2132) VR=LO VM=1-n Event Timer Name(s)</summary>
		DicomConstTags.EventTimerNames = 0x00082132;

		/// <summary>(0008,2142) VR=IS VM=1 Start Trim</summary>
		DicomConstTags.StartTrim = 0x00082142;

		/// <summary>(0008,2143) VR=IS VM=1 Stop Trim</summary>
		DicomConstTags.StopTrim = 0x00082143;

		/// <summary>(0008,2144) VR=IS VM=1 Recommended Display Frame Rate</summary>
		DicomConstTags.RecommendedDisplayFrameRate = 0x00082144;

		/// <summary>(0008,2218) VR=SQ VM=1 Anatomic Region Sequence</summary>
		DicomConstTags.AnatomicRegionSequence = 0x00082218;

		/// <summary>(0008,2220) VR=SQ VM=1 Anatomic Region Modifier Sequence</summary>
		DicomConstTags.AnatomicRegionModifierSequence = 0x00082220;

		/// <summary>(0008,2228) VR=SQ VM=1 Primary Anatomic Structure Sequence</summary>
		DicomConstTags.PrimaryAnatomicStructureSequence = 0x00082228;

		/// <summary>(0008,2229) VR=SQ VM=1 Anatomic Structure, Space or Region Sequence</summary>
		DicomConstTags.AnatomicStructureSpaceOrRegionSequence = 0x00082229;

		/// <summary>(0008,2230) VR=SQ VM=1 Primary Anatomic Structure Modifier Sequence</summary>
		DicomConstTags.PrimaryAnatomicStructureModifierSequence = 0x00082230;

		/// <summary>(0008,3001) VR=SQ VM=1 Alternate Representation Sequence</summary>
		DicomConstTags.AlternateRepresentationSequence = 0x00083001;

		/// <summary>(0008,3010) VR=UI VM=1 Irradiation Event UID</summary>
		DicomConstTags.IrradiationEventUID = 0x00083010;

		/// <summary>(0008,9007) VR=CS VM=4 Frame Type</summary>
		DicomConstTags.FrameType = 0x00089007;

		/// <summary>(0008,9092) VR=SQ VM=1 Referenced Image Evidence Sequence</summary>
		DicomConstTags.ReferencedImageEvidenceSequence = 0x00089092;

		/// <summary>(0008,9121) VR=SQ VM=1 Referenced Raw Data Sequence</summary>
		DicomConstTags.ReferencedRawDataSequence = 0x00089121;

		/// <summary>(0008,9123) VR=UI VM=1 Creator-Version UID</summary>
		DicomConstTags.CreatorVersionUID = 0x00089123;

		/// <summary>(0008,9124) VR=SQ VM=1 Derivation Image Sequence</summary>
		DicomConstTags.DerivationImageSequence = 0x00089124;

		/// <summary>(0008,9154) VR=SQ VM=1 Source Image Evidence Sequence</summary>
		DicomConstTags.SourceImageEvidenceSequence = 0x00089154;

		/// <summary>(0008,9205) VR=CS VM=1 Pixel Presentation</summary>
		DicomConstTags.PixelPresentation = 0x00089205;

		/// <summary>(0008,9206) VR=CS VM=1 Volumetric Properties</summary>
		DicomConstTags.VolumetricProperties = 0x00089206;

		/// <summary>(0008,9207) VR=CS VM=1 Volume Based Calculation Technique</summary>
		DicomConstTags.VolumeBasedCalculationTechnique = 0x00089207;

		/// <summary>(0008,9208) VR=CS VM=1 Complex Image Component</summary>
		DicomConstTags.ComplexImageComponent = 0x00089208;

		/// <summary>(0008,9209) VR=CS VM=1 Acquisition Contrast</summary>
		DicomConstTags.AcquisitionContrast = 0x00089209;

		/// <summary>(0008,9215) VR=SQ VM=1 Derivation Code Sequence</summary>
		DicomConstTags.DerivationCodeSequence = 0x00089215;

		/// <summary>(0008,9237) VR=SQ VM=1 Referenced Grayscale Presentation State Sequence</summary>
		DicomConstTags.ReferencedGrayscalePresentationStateSequence = 0x00089237;

		/// <summary>(0008,9410) VR=SQ VM=1 Referenced Other Plane Sequence</summary>
		DicomConstTags.ReferencedOtherPlaneSequence = 0x00089410;

		/// <summary>(0008,9458) VR=SQ VM=1 Frame Display Sequence</summary>
		DicomConstTags.FrameDisplaySequence = 0x00089458;

		/// <summary>(0008,9459) VR=FL VM=1 Recommended Display Frame Rate in Float</summary>
		DicomConstTags.RecommendedDisplayFrameRateInFloat = 0x00089459;

		/// <summary>(0008,9460) VR=CS VM=1 Skip Frame Range Flag</summary>
		DicomConstTags.SkipFrameRangeFlag = 0x00089460;

		/// <summary>(0010,0010) VR=PN VM=1 Patient's Name</summary>
		DicomConstTags.PatientsName = 0x00100010;

		/// <summary>(0010,0020) VR=LO VM=1 Patient ID</summary>
		DicomConstTags.PatientID = 0x00100020;

		/// <summary>(0010,0021) VR=LO VM=1 Issuer of Patient ID</summary>
		DicomConstTags.IssuerOfPatientID = 0x00100021;

		/// <summary>(0010,0022) VR=CS VM=1 Type of Patient ID</summary>
		DicomConstTags.TypeOfPatientID = 0x00100022;

		/// <summary>(0010,0030) VR=DA VM=1 Patient's Birth Date</summary>
		DicomConstTags.PatientsBirthDate = 0x00100030;

		/// <summary>(0010,0032) VR=TM VM=1 Patient's Birth Time</summary>
		DicomConstTags.PatientsBirthTime = 0x00100032;

		/// <summary>(0010,0040) VR=CS VM=1 Patient's Sex</summary>
		DicomConstTags.PatientsSex = 0x00100040;

		/// <summary>(0010,0050) VR=SQ VM=1 Patient's Insurance Plan Code Sequence</summary>
		DicomConstTags.PatientsInsurancePlanCodeSequence = 0x00100050;

		/// <summary>(0010,0101) VR=SQ VM=1 Patient's Primary Language Code Sequence</summary>
		DicomConstTags.PatientsPrimaryLanguageCodeSequence = 0x00100101;

		/// <summary>(0010,0102) VR=SQ VM=1 Patient's Primary Language Code Modifier Sequence</summary>
		DicomConstTags.PatientsPrimaryLanguageCodeModifierSequence = 0x00100102;

		/// <summary>(0010,1000) VR=LO VM=1-n Other Patient IDs</summary>
		DicomConstTags.OtherPatientIDs = 0x00101000;

		/// <summary>(0010,1001) VR=PN VM=1-n Other Patient Names</summary>
		DicomConstTags.OtherPatientNames = 0x00101001;

		/// <summary>(0010,1002) VR=SQ VM=1 Other Patient IDs Sequence</summary>
		DicomConstTags.OtherPatientIDsSequence = 0x00101002;

		/// <summary>(0010,1005) VR=PN VM=1 Patient's Birth Name</summary>
		DicomConstTags.PatientsBirthName = 0x00101005;

		/// <summary>(0010,1010) VR=AS VM=1 Patient's Age</summary>
		DicomConstTags.PatientsAge = 0x00101010;

		/// <summary>(0010,1020) VR=DS VM=1 Patient's Size</summary>
		DicomConstTags.PatientsSize = 0x00101020;

		/// <summary>(0010,1030) VR=DS VM=1 Patient's Weight</summary>
		DicomConstTags.PatientsWeight = 0x00101030;

		/// <summary>(0010,1040) VR=LO VM=1 Patient's Address</summary>
		DicomConstTags.PatientsAddress = 0x00101040;

		/// <summary>(0010,1060) VR=PN VM=1 Patient's Mother's Birth Name</summary>
		DicomConstTags.PatientsMothersBirthName = 0x00101060;

		/// <summary>(0010,1080) VR=LO VM=1 Military Rank</summary>
		DicomConstTags.MilitaryRank = 0x00101080;

		/// <summary>(0010,1081) VR=LO VM=1 Branch of Service</summary>
		DicomConstTags.BranchOfService = 0x00101081;

		/// <summary>(0010,1090) VR=LO VM=1 Medical Record Locator</summary>
		DicomConstTags.MedicalRecordLocator = 0x00101090;

		/// <summary>(0010,2000) VR=LO VM=1-n Medical Alerts</summary>
		DicomConstTags.MedicalAlerts = 0x00102000;

		/// <summary>(0010,2110) VR=LO VM=1-n Allergies</summary>
		DicomConstTags.Allergies = 0x00102110;

		/// <summary>(0010,2150) VR=LO VM=1 Country of Residence</summary>
		DicomConstTags.CountryOfResidence = 0x00102150;

		/// <summary>(0010,2152) VR=LO VM=1 Region of Residence</summary>
		DicomConstTags.RegionOfResidence = 0x00102152;

		/// <summary>(0010,2154) VR=SH VM=1-n Patient's Telephone Numbers</summary>
		DicomConstTags.PatientsTelephoneNumbers = 0x00102154;

		/// <summary>(0010,2160) VR=SH VM=1 Ethnic Group</summary>
		DicomConstTags.EthnicGroup = 0x00102160;

		/// <summary>(0010,2180) VR=SH VM=1 Occupation</summary>
		DicomConstTags.Occupation = 0x00102180;

		/// <summary>(0010,21a0) VR=CS VM=1 Smoking Status</summary>
		DicomConstTags.SmokingStatus = 0x001021a0;

		/// <summary>(0010,21b0) VR=LT VM=1 Additional Patient History</summary>
		DicomConstTags.AdditionalPatientHistory = 0x001021b0;

		/// <summary>(0010,21c0) VR=US VM=1 Pregnancy Status</summary>
		DicomConstTags.PregnancyStatus = 0x001021c0;

		/// <summary>(0010,21d0) VR=DA VM=1 Last Menstrual Date</summary>
		DicomConstTags.LastMenstrualDate = 0x001021d0;

		/// <summary>(0010,21f0) VR=LO VM=1 Patient's Religious Preference</summary>
		DicomConstTags.PatientsReligiousPreference = 0x001021f0;

		/// <summary>(0010,2201) VR=LO VM=1 Patient Species Description</summary>
		DicomConstTags.PatientSpeciesDescription = 0x00102201;

		/// <summary>(0010,2202) VR=SQ VM=1 Patient Species Code Sequence</summary>
		DicomConstTags.PatientSpeciesCodeSequence = 0x00102202;

		/// <summary>(0010,2203) VR=CS VM=1 Patient's Sex Neutered</summary>
		DicomConstTags.PatientsSexNeutered = 0x00102203;

		/// <summary>(0010,2292) VR=LO VM=1 Patient Breed Description</summary>
		DicomConstTags.PatientBreedDescription = 0x00102292;

		/// <summary>(0010,2293) VR=SQ VM=1 Patient Breed Code Sequence</summary>
		DicomConstTags.PatientBreedCodeSequence = 0x00102293;

		/// <summary>(0010,2294) VR=SQ VM=1 Breed Registration Sequence</summary>
		DicomConstTags.BreedRegistrationSequence = 0x00102294;

		/// <summary>(0010,2295) VR=LO VM=1 Breed Registration Number</summary>
		DicomConstTags.BreedRegistrationNumber = 0x00102295;

		/// <summary>(0010,2296) VR=SQ VM=1 Breed Registry Code Sequence</summary>
		DicomConstTags.BreedRegistryCodeSequence = 0x00102296;

		/// <summary>(0010,2297) VR=PN VM=1 Responsible Person</summary>
		DicomConstTags.ResponsiblePerson = 0x00102297;

		/// <summary>(0010,2298) VR=CS VM=1 Responsible Person Role</summary>
		DicomConstTags.ResponsiblePersonRole = 0x00102298;

		/// <summary>(0010,2299) VR=LO VM=1 Responsible Organization</summary>
		DicomConstTags.ResponsibleOrganization = 0x00102299;

		/// <summary>(0010,4000) VR=LT VM=1 Patient Comments</summary>
		DicomConstTags.PatientComments = 0x00104000;

		/// <summary>(0010,9431) VR=FL VM=1 Examined Body Thickness</summary>
		DicomConstTags.ExaminedBodyThickness = 0x00109431;

		/// <summary>(0012,0010) VR=LO VM=1 Clinical Trial Sponsor Name</summary>
		DicomConstTags.ClinicalTrialSponsorName = 0x00120010;

		/// <summary>(0012,0020) VR=LO VM=1 Clinical Trial Protocol ID</summary>
		DicomConstTags.ClinicalTrialProtocolID = 0x00120020;

		/// <summary>(0012,0021) VR=LO VM=1 Clinical Trial Protocol Name</summary>
		DicomConstTags.ClinicalTrialProtocolName = 0x00120021;

		/// <summary>(0012,0030) VR=LO VM=1 Clinical Trial Site ID</summary>
		DicomConstTags.ClinicalTrialSiteID = 0x00120030;

		/// <summary>(0012,0031) VR=LO VM=1 Clinical Trial Site Name</summary>
		DicomConstTags.ClinicalTrialSiteName = 0x00120031;

		/// <summary>(0012,0040) VR=LO VM=1 Clinical Trial Subject ID</summary>
		DicomConstTags.ClinicalTrialSubjectID = 0x00120040;

		/// <summary>(0012,0042) VR=LO VM=1 Clinical Trial Subject Reading ID</summary>
		DicomConstTags.ClinicalTrialSubjectReadingID = 0x00120042;

		/// <summary>(0012,0050) VR=LO VM=1 Clinical Trial Time Point ID</summary>
		DicomConstTags.ClinicalTrialTimePointID = 0x00120050;

		/// <summary>(0012,0051) VR=ST VM=1 Clinical Trial Time Point Description</summary>
		DicomConstTags.ClinicalTrialTimePointDescription = 0x00120051;

		/// <summary>(0012,0060) VR=LO VM=1 Clinical Trial Coordinating Center Name</summary>
		DicomConstTags.ClinicalTrialCoordinatingCenterName = 0x00120060;

		/// <summary>(0012,0062) VR=CS VM=1 Patient Identity Removed</summary>
		DicomConstTags.PatientIdentityRemoved = 0x00120062;

		/// <summary>(0012,0063) VR=LO VM=1-n De-identification Method</summary>
		DicomConstTags.DeidentificationMethod = 0x00120063;

		/// <summary>(0012,0064) VR=SQ VM=1 De-identification Method Code Sequence</summary>
		DicomConstTags.DeidentificationMethodCodeSequence = 0x00120064;

		/// <summary>(0012,0071) VR=LO VM=1 Clinical Trial Series ID</summary>
		DicomConstTags.ClinicalTrialSeriesID = 0x00120071;

		/// <summary>(0012,0072) VR=LO VM=1 Clinical Trial Series Description</summary>
		DicomConstTags.ClinicalTrialSeriesDescription = 0x00120072;

		/// <summary>(0018,0010) VR=LO VM=1 Contrast/Bolus Agent</summary>
		DicomConstTags.ContrastBolusAgent = 0x00180010;

		/// <summary>(0018,0012) VR=SQ VM=1 Contrast/Bolus Agent Sequence</summary>
		DicomConstTags.ContrastBolusAgentSequence = 0x00180012;

		/// <summary>(0018,0014) VR=SQ VM=1 Contrast/Bolus Administration Route Sequence</summary>
		DicomConstTags.ContrastBolusAdministrationRouteSequence = 0x00180014;

		/// <summary>(0018,0015) VR=CS VM=1 Body Part Examined</summary>
		DicomConstTags.BodyPartExamined = 0x00180015;

		/// <summary>(0018,0020) VR=CS VM=1-n Scanning Sequence</summary>
		DicomConstTags.ScanningSequence = 0x00180020;

		/// <summary>(0018,0021) VR=CS VM=1-n Sequence Variant</summary>
		DicomConstTags.SequenceVariant = 0x00180021;

		/// <summary>(0018,0022) VR=CS VM=1-n Scan Options</summary>
		DicomConstTags.ScanOptions = 0x00180022;

		/// <summary>(0018,0023) VR=CS VM=1 MR Acquisition Type</summary>
		DicomConstTags.MRAcquisitionType = 0x00180023;

		/// <summary>(0018,0024) VR=SH VM=1 Sequence Name</summary>
		DicomConstTags.SequenceName = 0x00180024;

		/// <summary>(0018,0025) VR=CS VM=1 Angio Flag</summary>
		DicomConstTags.AngioFlag = 0x00180025;

		/// <summary>(0018,0026) VR=SQ VM=1 Intervention Drug Information Sequence</summary>
		DicomConstTags.InterventionDrugInformationSequence = 0x00180026;

		/// <summary>(0018,0027) VR=TM VM=1 Intervention Drug Stop Time</summary>
		DicomConstTags.InterventionDrugStopTime = 0x00180027;

		/// <summary>(0018,0028) VR=DS VM=1 Intervention Drug Dose</summary>
		DicomConstTags.InterventionDrugDose = 0x00180028;

		/// <summary>(0018,0029) VR=SQ VM=1 Intervention Drug Sequence</summary>
		DicomConstTags.InterventionDrugSequence = 0x00180029;

		/// <summary>(0018,002a) VR=SQ VM=1 Additional Drug Sequence</summary>
		DicomConstTags.AdditionalDrugSequence = 0x0018002a;

		/// <summary>(0018,0031) VR=LO VM=1 Radiopharmaceutical</summary>
		DicomConstTags.Radiopharmaceutical = 0x00180031;

		/// <summary>(0018,0034) VR=LO VM=1 Intervention Drug Name</summary>
		DicomConstTags.InterventionDrugName = 0x00180034;

		/// <summary>(0018,0035) VR=TM VM=1 Intervention Drug Start Time</summary>
		DicomConstTags.InterventionDrugStartTime = 0x00180035;

		/// <summary>(0018,0036) VR=SQ VM=1 Intervention Sequence</summary>
		DicomConstTags.InterventionSequence = 0x00180036;

		/// <summary>(0018,0038) VR=CS VM=1 Intervention Status</summary>
		DicomConstTags.InterventionStatus = 0x00180038;

		/// <summary>(0018,003a) VR=ST VM=1 Intervention Description</summary>
		DicomConstTags.InterventionDescription = 0x0018003a;

		/// <summary>(0018,0040) VR=IS VM=1 Cine Rate</summary>
		DicomConstTags.CineRate = 0x00180040;

		/// <summary>(0018,0050) VR=DS VM=1 Slice Thickness</summary>
		DicomConstTags.SliceThickness = 0x00180050;

		/// <summary>(0018,0060) VR=DS VM=1 KVP</summary>
		DicomConstTags.KVP = 0x00180060;

		/// <summary>(0018,0070) VR=IS VM=1 Counts Accumulated</summary>
		DicomConstTags.CountsAccumulated = 0x00180070;

		/// <summary>(0018,0071) VR=CS VM=1 Acquisition Termination Condition</summary>
		DicomConstTags.AcquisitionTerminationCondition = 0x00180071;

		/// <summary>(0018,0072) VR=DS VM=1 Effective Duration</summary>
		DicomConstTags.EffectiveDuration = 0x00180072;

		/// <summary>(0018,0073) VR=CS VM=1 Acquisition Start Condition</summary>
		DicomConstTags.AcquisitionStartCondition = 0x00180073;

		/// <summary>(0018,0074) VR=IS VM=1 Acquisition Start Condition Data</summary>
		DicomConstTags.AcquisitionStartConditionData = 0x00180074;

		/// <summary>(0018,0075) VR=IS VM=1 Acquisition Termination Condition Data</summary>
		DicomConstTags.AcquisitionTerminationConditionData = 0x00180075;

		/// <summary>(0018,0080) VR=DS VM=1 Repetition Time</summary>
		DicomConstTags.RepetitionTime = 0x00180080;

		/// <summary>(0018,0081) VR=DS VM=1 Echo Time</summary>
		DicomConstTags.EchoTime = 0x00180081;

		/// <summary>(0018,0082) VR=DS VM=1 Inversion Time</summary>
		DicomConstTags.InversionTime = 0x00180082;

		/// <summary>(0018,0083) VR=DS VM=1 Number of Averages</summary>
		DicomConstTags.NumberOfAverages = 0x00180083;

		/// <summary>(0018,0084) VR=DS VM=1 Imaging Frequency</summary>
		DicomConstTags.ImagingFrequency = 0x00180084;

		/// <summary>(0018,0085) VR=SH VM=1 Imaged Nucleus</summary>
		DicomConstTags.ImagedNucleus = 0x00180085;

		/// <summary>(0018,0086) VR=IS VM=1-n Echo Number(s)</summary>
		DicomConstTags.EchoNumbers = 0x00180086;

		/// <summary>(0018,0087) VR=DS VM=1 Magnetic Field Strength</summary>
		DicomConstTags.MagneticFieldStrength = 0x00180087;

		/// <summary>(0018,0088) VR=DS VM=1 Spacing Between Slices</summary>
		DicomConstTags.SpacingBetweenSlices = 0x00180088;

		/// <summary>(0018,0089) VR=IS VM=1 Number of Phase Encoding Steps</summary>
		DicomConstTags.NumberOfPhaseEncodingSteps = 0x00180089;

		/// <summary>(0018,0090) VR=DS VM=1 Data Collection Diameter</summary>
		DicomConstTags.DataCollectionDiameter = 0x00180090;

		/// <summary>(0018,0091) VR=IS VM=1 Echo Train Length</summary>
		DicomConstTags.EchoTrainLength = 0x00180091;

		/// <summary>(0018,0093) VR=DS VM=1 Percent Sampling</summary>
		DicomConstTags.PercentSampling = 0x00180093;

		/// <summary>(0018,0094) VR=DS VM=1 Percent Phase Field of View</summary>
		DicomConstTags.PercentPhaseFieldOfView = 0x00180094;

		/// <summary>(0018,0095) VR=DS VM=1 Pixel Bandwidth</summary>
		DicomConstTags.PixelBandwidth = 0x00180095;

		/// <summary>(0018,1000) VR=LO VM=1 Device Serial Number</summary>
		DicomConstTags.DeviceSerialNumber = 0x00181000;

		/// <summary>(0018,1002) VR=UI VM=1 Device UID</summary>
		DicomConstTags.DeviceUID = 0x00181002;

		/// <summary>(0018,1003) VR=LO VM=1 Device ID</summary>
		DicomConstTags.DeviceID = 0x00181003;

		/// <summary>(0018,1004) VR=LO VM=1 Plate ID</summary>
		DicomConstTags.PlateID = 0x00181004;

		/// <summary>(0018,1005) VR=LO VM=1 Generator ID</summary>
		DicomConstTags.GeneratorID = 0x00181005;

		/// <summary>(0018,1006) VR=LO VM=1 Grid ID</summary>
		DicomConstTags.GridID = 0x00181006;

		/// <summary>(0018,1007) VR=LO VM=1 Cassette ID</summary>
		DicomConstTags.CassetteID = 0x00181007;

		/// <summary>(0018,1008) VR=LO VM=1 Gantry ID</summary>
		DicomConstTags.GantryID = 0x00181008;

		/// <summary>(0018,1010) VR=LO VM=1 Secondary Capture Device ID</summary>
		DicomConstTags.SecondaryCaptureDeviceID = 0x00181010;

		/// <summary>(0018,1012) VR=DA VM=1 Date of Secondary Capture</summary>
		DicomConstTags.DateOfSecondaryCapture = 0x00181012;

		/// <summary>(0018,1014) VR=TM VM=1 Time of Secondary Capture</summary>
		DicomConstTags.TimeOfSecondaryCapture = 0x00181014;

		/// <summary>(0018,1016) VR=LO VM=1 Secondary Capture Device Manufacturers</summary>
		DicomConstTags.SecondaryCaptureDeviceManufacturers = 0x00181016;

		/// <summary>(0018,1018) VR=LO VM=1 Secondary Capture Device Manufacturer's Model Name</summary>
		DicomConstTags.SecondaryCaptureDeviceManufacturersModelName = 0x00181018;

		/// <summary>(0018,1019) VR=LO VM=1-n Secondary Capture Device Software Version(s)</summary>
		DicomConstTags.SecondaryCaptureDeviceSoftwareVersions = 0x00181019;

		/// <summary>(0018,1020) VR=LO VM=1-n Software Version(s)</summary>
		DicomConstTags.SoftwareVersions = 0x00181020;

		/// <summary>(0018,1022) VR=SH VM=1 Video Image Format Acquired</summary>
		DicomConstTags.VideoImageFormatAcquired = 0x00181022;

		/// <summary>(0018,1023) VR=LO VM=1 Digital Image Format Acquired</summary>
		DicomConstTags.DigitalImageFormatAcquired = 0x00181023;

		/// <summary>(0018,1030) VR=LO VM=1 Protocol Name</summary>
		DicomConstTags.ProtocolName = 0x00181030;

		/// <summary>(0018,1040) VR=LO VM=1 Contrast/Bolus Route</summary>
		DicomConstTags.ContrastBolusRoute = 0x00181040;

		/// <summary>(0018,1041) VR=DS VM=1 Contrast/Bolus Volume</summary>
		DicomConstTags.ContrastBolusVolume = 0x00181041;

		/// <summary>(0018,1042) VR=TM VM=1 Contrast/Bolus Start Time</summary>
		DicomConstTags.ContrastBolusStartTime = 0x00181042;

		/// <summary>(0018,1043) VR=TM VM=1 Contrast/Bolus Stop Time</summary>
		DicomConstTags.ContrastBolusStopTime = 0x00181043;

		/// <summary>(0018,1044) VR=DS VM=1 Contrast/Bolus Total Dose</summary>
		DicomConstTags.ContrastBolusTotalDose = 0x00181044;

		/// <summary>(0018,1045) VR=IS VM=1 Syringe Counts</summary>
		DicomConstTags.SyringeCounts = 0x00181045;

		/// <summary>(0018,1046) VR=DS VM=1-n Contrast Flow Rate</summary>
		DicomConstTags.ContrastFlowRate = 0x00181046;

		/// <summary>(0018,1047) VR=DS VM=1-n Contrast Flow Duration</summary>
		DicomConstTags.ContrastFlowDuration = 0x00181047;

		/// <summary>(0018,1048) VR=CS VM=1 Contrast/Bolus Ingredient</summary>
		DicomConstTags.ContrastBolusIngredient = 0x00181048;

		/// <summary>(0018,1049) VR=DS VM=1 Contrast/Bolus Ingredient Concentration</summary>
		DicomConstTags.ContrastBolusIngredientConcentration = 0x00181049;

		/// <summary>(0018,1050) VR=DS VM=1 Spatial Resolution</summary>
		DicomConstTags.SpatialResolution = 0x00181050;

		/// <summary>(0018,1060) VR=DS VM=1 Trigger Time</summary>
		DicomConstTags.TriggerTime = 0x00181060;

		/// <summary>(0018,1061) VR=LO VM=1 Trigger Source or Type</summary>
		DicomConstTags.TriggerSourceOrType = 0x00181061;

		/// <summary>(0018,1062) VR=IS VM=1 Nominal Interval</summary>
		DicomConstTags.NominalInterval = 0x00181062;

		/// <summary>(0018,1063) VR=DS VM=1 Frame Time</summary>
		DicomConstTags.FrameTime = 0x00181063;

		/// <summary>(0018,1064) VR=LO VM=1 Cardiac Framing Type</summary>
		DicomConstTags.CardiacFramingType = 0x00181064;

		/// <summary>(0018,1065) VR=DS VM=1-n Frame Time Vector</summary>
		DicomConstTags.FrameTimeVector = 0x00181065;

		/// <summary>(0018,1066) VR=DS VM=1 Frame Delay</summary>
		DicomConstTags.FrameDelay = 0x00181066;

		/// <summary>(0018,1067) VR=DS VM=1 Image Trigger Delay</summary>
		DicomConstTags.ImageTriggerDelay = 0x00181067;

		/// <summary>(0018,1068) VR=DS VM=1 Multiplex Group Time Offset</summary>
		DicomConstTags.MultiplexGroupTimeOffset = 0x00181068;

		/// <summary>(0018,1069) VR=DS VM=1 Trigger Time Offset</summary>
		DicomConstTags.TriggerTimeOffset = 0x00181069;

		/// <summary>(0018,106a) VR=CS VM=1 Synchronization Trigger</summary>
		DicomConstTags.SynchronizationTrigger = 0x0018106a;

		/// <summary>(0018,106c) VR=US VM=2 Synchronization Channel</summary>
		DicomConstTags.SynchronizationChannel = 0x0018106c;

		/// <summary>(0018,106e) VR=UL VM=1 Trigger Sample Position</summary>
		DicomConstTags.TriggerSamplePosition = 0x0018106e;

		/// <summary>(0018,1070) VR=LO VM=1 Radiopharmaceutical Route</summary>
		DicomConstTags.RadiopharmaceuticalRoute = 0x00181070;

		/// <summary>(0018,1071) VR=DS VM=1 Radiopharmaceutical Volume</summary>
		DicomConstTags.RadiopharmaceuticalVolume = 0x00181071;

		/// <summary>(0018,1072) VR=TM VM=1 Radiopharmaceutical Start Time</summary>
		DicomConstTags.RadiopharmaceuticalStartTime = 0x00181072;

		/// <summary>(0018,1073) VR=TM VM=1 Radiopharmaceutical Stop Time</summary>
		DicomConstTags.RadiopharmaceuticalStopTime = 0x00181073;

		/// <summary>(0018,1074) VR=DS VM=1 Radionuclide Total Dose</summary>
		DicomConstTags.RadionuclideTotalDose = 0x00181074;

		/// <summary>(0018,1075) VR=DS VM=1 Radionuclide Half Life</summary>
		DicomConstTags.RadionuclideHalfLife = 0x00181075;

		/// <summary>(0018,1076) VR=DS VM=1 Radionuclide Positron Fraction</summary>
		DicomConstTags.RadionuclidePositronFraction = 0x00181076;

		/// <summary>(0018,1077) VR=DS VM=1 Radiopharmaceutical Specific Activity</summary>
		DicomConstTags.RadiopharmaceuticalSpecificActivity = 0x00181077;

		/// <summary>(0018,1078) VR=DT VM=1 Radiopharmaceutical Start DateTime</summary>
		DicomConstTags.RadiopharmaceuticalStartDateTime = 0x00181078;

		/// <summary>(0018,1079) VR=DT VM=1 Radiopharmaceutical Stop DateTime</summary>
		DicomConstTags.RadiopharmaceuticalStopDateTime = 0x00181079;

		/// <summary>(0018,1080) VR=CS VM=1 Beat Rejection Flag</summary>
		DicomConstTags.BeatRejectionFlag = 0x00181080;

		/// <summary>(0018,1081) VR=IS VM=1 Low R-R Value</summary>
		DicomConstTags.LowRRValue = 0x00181081;

		/// <summary>(0018,1082) VR=IS VM=1 High R-R Value</summary>
		DicomConstTags.HighRRValue = 0x00181082;

		/// <summary>(0018,1083) VR=IS VM=1 Intervals Acquired</summary>
		DicomConstTags.IntervalsAcquired = 0x00181083;

		/// <summary>(0018,1084) VR=IS VM=1 Intervals Rejected</summary>
		DicomConstTags.IntervalsRejected = 0x00181084;

		/// <summary>(0018,1085) VR=LO VM=1 PVC Rejection</summary>
		DicomConstTags.PVCRejection = 0x00181085;

		/// <summary>(0018,1086) VR=IS VM=1 Skip Beats</summary>
		DicomConstTags.SkipBeats = 0x00181086;

		/// <summary>(0018,1088) VR=IS VM=1 Heart Rate</summary>
		DicomConstTags.HeartRate = 0x00181088;

		/// <summary>(0018,1090) VR=IS VM=1 Cardiac Number of Images</summary>
		DicomConstTags.CardiacNumberOfImages = 0x00181090;

		/// <summary>(0018,1094) VR=IS VM=1 Trigger Window</summary>
		DicomConstTags.TriggerWindow = 0x00181094;

		/// <summary>(0018,1100) VR=DS VM=1 Reconstruction Diameter</summary>
		DicomConstTags.ReconstructionDiameter = 0x00181100;

		/// <summary>(0018,1110) VR=DS VM=1 Distance Source to Detector</summary>
		DicomConstTags.DistanceSourceToDetector = 0x00181110;

		/// <summary>(0018,1111) VR=DS VM=1 Distance Source to Patient</summary>
		DicomConstTags.DistanceSourceToPatient = 0x00181111;

		/// <summary>(0018,1114) VR=DS VM=1 Estimated Radiographic Magnification Factor</summary>
		DicomConstTags.EstimatedRadiographicMagnificationFactor = 0x00181114;

		/// <summary>(0018,1120) VR=DS VM=1 Gantry/Detector Tilt</summary>
		DicomConstTags.GantryDetectorTilt = 0x00181120;

		/// <summary>(0018,1121) VR=DS VM=1 Gantry/Detector Slew</summary>
		DicomConstTags.GantryDetectorSlew = 0x00181121;

		/// <summary>(0018,1130) VR=DS VM=1 Table Height</summary>
		DicomConstTags.TableHeight = 0x00181130;

		/// <summary>(0018,1131) VR=DS VM=1 Table Traverse</summary>
		DicomConstTags.TableTraverse = 0x00181131;

		/// <summary>(0018,1134) VR=CS VM=1 Table Motion</summary>
		DicomConstTags.TableMotion = 0x00181134;

		/// <summary>(0018,1135) VR=DS VM=1-n Table Vertical Increment</summary>
		DicomConstTags.TableVerticalIncrement = 0x00181135;

		/// <summary>(0018,1136) VR=DS VM=1-n Table Lateral Increment</summary>
		DicomConstTags.TableLateralIncrement = 0x00181136;

		/// <summary>(0018,1137) VR=DS VM=1-n Table Longitudinal Increment</summary>
		DicomConstTags.TableLongitudinalIncrement = 0x00181137;

		/// <summary>(0018,1138) VR=DS VM=1 Table Angle</summary>
		DicomConstTags.TableAngle = 0x00181138;

		/// <summary>(0018,113a) VR=CS VM=1 Table Type</summary>
		DicomConstTags.TableType = 0x0018113a;

		/// <summary>(0018,1140) VR=CS VM=1 Rotation Direction</summary>
		DicomConstTags.RotationDirection = 0x00181140;

		/// <summary>(0018,1142) VR=DS VM=1-n Radial Position</summary>
		DicomConstTags.RadialPosition = 0x00181142;

		/// <summary>(0018,1143) VR=DS VM=1 Scan Arc</summary>
		DicomConstTags.ScanArc = 0x00181143;

		/// <summary>(0018,1144) VR=DS VM=1 Angular Step</summary>
		DicomConstTags.AngularStep = 0x00181144;

		/// <summary>(0018,1145) VR=DS VM=1 Center of Rotation Offset</summary>
		DicomConstTags.CenterOfRotationOffset = 0x00181145;

		/// <summary>(0018,1147) VR=CS VM=1 Field of View Shape</summary>
		DicomConstTags.FieldOfViewShape = 0x00181147;

		/// <summary>(0018,1149) VR=IS VM=1-2 Field of View Dimension(s)</summary>
		DicomConstTags.FieldOfViewDimensions = 0x00181149;

		/// <summary>(0018,1150) VR=IS VM=1 Exposure Time</summary>
		DicomConstTags.ExposureTime = 0x00181150;

		/// <summary>(0018,1151) VR=IS VM=1 X-Ray Tube Current</summary>
		DicomConstTags.XRayTubeCurrent = 0x00181151;

		/// <summary>(0018,1152) VR=IS VM=1 Exposure</summary>
		DicomConstTags.Exposure = 0x00181152;

		/// <summary>(0018,1153) VR=IS VM=1 Exposure in uAs</summary>
		DicomConstTags.ExposureInMicroAs = 0x00181153;

		/// <summary>(0018,1154) VR=DS VM=1 Average Pulse Width</summary>
		DicomConstTags.AveragePulseWidth = 0x00181154;

		/// <summary>(0018,1155) VR=CS VM=1 Radiation Setting</summary>
		DicomConstTags.RadiationSetting = 0x00181155;

		/// <summary>(0018,1156) VR=CS VM=1 Rectification Type</summary>
		DicomConstTags.RectificationType = 0x00181156;

		/// <summary>(0018,115a) VR=CS VM=1 Radiation Mode</summary>
		DicomConstTags.RadiationMode = 0x0018115a;

		/// <summary>(0018,115e) VR=DS VM=1 Image and Fluoroscopy Area Dose Product</summary>
		DicomConstTags.ImageAndFluoroscopyAreaDoseProduct = 0x0018115e;

		/// <summary>(0018,1160) VR=SH VM=1 Filter Type</summary>
		DicomConstTags.FilterType = 0x00181160;

		/// <summary>(0018,1161) VR=LO VM=1-n Type of Filters</summary>
		DicomConstTags.TypeOfFilters = 0x00181161;

		/// <summary>(0018,1162) VR=DS VM=1 Intensifier Size</summary>
		DicomConstTags.IntensifierSize = 0x00181162;

		/// <summary>(0018,1164) VR=DS VM=2 Imager Pixel Spacing</summary>
		DicomConstTags.ImagerPixelSpacing = 0x00181164;

		/// <summary>(0018,1166) VR=CS VM=1-n Grid</summary>
		DicomConstTags.Grid = 0x00181166;

		/// <summary>(0018,1170) VR=IS VM=1 Generator Power</summary>
		DicomConstTags.GeneratorPower = 0x00181170;

		/// <summary>(0018,1180) VR=SH VM=1 Collimator/grid Name</summary>
		DicomConstTags.CollimatorgridName = 0x00181180;

		/// <summary>(0018,1181) VR=CS VM=1 Collimator Type</summary>
		DicomConstTags.CollimatorType = 0x00181181;

		/// <summary>(0018,1182) VR=IS VM=1-2 Focal Distance</summary>
		DicomConstTags.FocalDistance = 0x00181182;

		/// <summary>(0018,1183) VR=DS VM=1-2 X Focus Center</summary>
		DicomConstTags.XFocusCenter = 0x00181183;

		/// <summary>(0018,1184) VR=DS VM=1-2 Y Focus Center</summary>
		DicomConstTags.YFocusCenter = 0x00181184;

		/// <summary>(0018,1190) VR=DS VM=1-n Focal Spot(s)</summary>
		DicomConstTags.FocalSpots = 0x00181190;

		/// <summary>(0018,1191) VR=CS VM=1 Anode Target Material</summary>
		DicomConstTags.AnodeTargetMaterial = 0x00181191;

		/// <summary>(0018,11a0) VR=DS VM=1 Body Part Thickness</summary>
		DicomConstTags.BodyPartThickness = 0x001811a0;

		/// <summary>(0018,11a2) VR=DS VM=1 Compression Force</summary>
		DicomConstTags.CompressionForce = 0x001811a2;

		/// <summary>(0018,1200) VR=DA VM=1-n Date of Last Calibration</summary>
		DicomConstTags.DateOfLastCalibration = 0x00181200;

		/// <summary>(0018,1201) VR=TM VM=1-n Time of Last Calibration</summary>
		DicomConstTags.TimeOfLastCalibration = 0x00181201;

		/// <summary>(0018,1210) VR=SH VM=1-n Convolution Kernel</summary>
		DicomConstTags.ConvolutionKernel = 0x00181210;

		/// <summary>(0018,1242) VR=IS VM=1 Actual Frame Duration</summary>
		DicomConstTags.ActualFrameDuration = 0x00181242;

		/// <summary>(0018,1243) VR=IS VM=1 Count Rate</summary>
		DicomConstTags.CountRate = 0x00181243;

		/// <summary>(0018,1244) VR=US VM=1 Preferred Playback Sequencing</summary>
		DicomConstTags.PreferredPlaybackSequencing = 0x00181244;

		/// <summary>(0018,1250) VR=SH VM=1 Receive Coil Name</summary>
		DicomConstTags.ReceiveCoilName = 0x00181250;

		/// <summary>(0018,1251) VR=SH VM=1 Transmit Coil Name</summary>
		DicomConstTags.TransmitCoilName = 0x00181251;

		/// <summary>(0018,1260) VR=SH VM=1 Plate Type</summary>
		DicomConstTags.PlateType = 0x00181260;

		/// <summary>(0018,1261) VR=LO VM=1 Phosphor Type</summary>
		DicomConstTags.PhosphorType = 0x00181261;

		/// <summary>(0018,1300) VR=DS VM=1 Scan Velocity</summary>
		DicomConstTags.ScanVelocity = 0x00181300;

		/// <summary>(0018,1301) VR=CS VM=1-n Whole Body Technique</summary>
		DicomConstTags.WholeBodyTechnique = 0x00181301;

		/// <summary>(0018,1302) VR=IS VM=1 Scan Length</summary>
		DicomConstTags.ScanLength = 0x00181302;

		/// <summary>(0018,1310) VR=US VM=4 Acquisition Matrix</summary>
		DicomConstTags.AcquisitionMatrix = 0x00181310;

		/// <summary>(0018,1312) VR=CS VM=1 In-plane Phase Encoding Direction</summary>
		DicomConstTags.InplanePhaseEncodingDirection = 0x00181312;

		/// <summary>(0018,1314) VR=DS VM=1 Flip Angle</summary>
		DicomConstTags.FlipAngle = 0x00181314;

		/// <summary>(0018,1315) VR=CS VM=1 Variable Flip Angle Flag</summary>
		DicomConstTags.VariableFlipAngleFlag = 0x00181315;

		/// <summary>(0018,1316) VR=DS VM=1 SAR</summary>
		DicomConstTags.SAR = 0x00181316;

		/// <summary>(0018,1318) VR=DS VM=1 dB/dt</summary>
		DicomConstTags.DBdt = 0x00181318;

		/// <summary>(0018,1400) VR=LO VM=1 Acquisition Device Processing Description</summary>
		DicomConstTags.AcquisitionDeviceProcessingDescription = 0x00181400;

		/// <summary>(0018,1401) VR=LO VM=1 Acquisition Device Processing Code</summary>
		DicomConstTags.AcquisitionDeviceProcessingCode = 0x00181401;

		/// <summary>(0018,1402) VR=CS VM=1 Cassette Orientation</summary>
		DicomConstTags.CassetteOrientation = 0x00181402;

		/// <summary>(0018,1403) VR=CS VM=1 Cassette Size</summary>
		DicomConstTags.CassetteSize = 0x00181403;

		/// <summary>(0018,1404) VR=US VM=1 Exposures on Plate</summary>
		DicomConstTags.ExposuresOnPlate = 0x00181404;

		/// <summary>(0018,1405) VR=IS VM=1 Relative X-Ray Exposure</summary>
		DicomConstTags.RelativeXRayExposure = 0x00181405;

		/// <summary>(0018,1450) VR=DS VM=1 Column Angulation</summary>
		DicomConstTags.ColumnAngulation = 0x00181450;

		/// <summary>(0018,1460) VR=DS VM=1 Tomo Layer Height</summary>
		DicomConstTags.TomoLayerHeight = 0x00181460;

		/// <summary>(0018,1470) VR=DS VM=1 Tomo Angle</summary>
		DicomConstTags.TomoAngle = 0x00181470;

		/// <summary>(0018,1480) VR=DS VM=1 Tomo Time</summary>
		DicomConstTags.TomoTime = 0x00181480;

		/// <summary>(0018,1490) VR=CS VM=1 Tomo Type</summary>
		DicomConstTags.TomoType = 0x00181490;

		/// <summary>(0018,1491) VR=CS VM=1 Tomo Class</summary>
		DicomConstTags.TomoClass = 0x00181491;

		/// <summary>(0018,1495) VR=IS VM=1 Number of Tomosynthesis Source Images</summary>
		DicomConstTags.NumberOfTomosynthesisSourceImages = 0x00181495;

		/// <summary>(0018,1500) VR=CS VM=1 Positioner Motion</summary>
		DicomConstTags.PositionerMotion = 0x00181500;

		/// <summary>(0018,1508) VR=CS VM=1 Positioner Type</summary>
		DicomConstTags.PositionerType = 0x00181508;

		/// <summary>(0018,1510) VR=DS VM=1 Positioner Primary Angle</summary>
		DicomConstTags.PositionerPrimaryAngle = 0x00181510;

		/// <summary>(0018,1511) VR=DS VM=1 Positioner Secondary Angle</summary>
		DicomConstTags.PositionerSecondaryAngle = 0x00181511;

		/// <summary>(0018,1520) VR=DS VM=1-n Positioner Primary Angle Increment</summary>
		DicomConstTags.PositionerPrimaryAngleIncrement = 0x00181520;

		/// <summary>(0018,1521) VR=DS VM=1-n Positioner Secondary Angle Increment</summary>
		DicomConstTags.PositionerSecondaryAngleIncrement = 0x00181521;

		/// <summary>(0018,1530) VR=DS VM=1 Detector Primary Angle</summary>
		DicomConstTags.DetectorPrimaryAngle = 0x00181530;

		/// <summary>(0018,1531) VR=DS VM=1 Detector Secondary Angle</summary>
		DicomConstTags.DetectorSecondaryAngle = 0x00181531;

		/// <summary>(0018,1600) VR=CS VM=1-3 Shutter Shape</summary>
		DicomConstTags.ShutterShape = 0x00181600;

		/// <summary>(0018,1602) VR=IS VM=1 Shutter Left Vertical Edge</summary>
		DicomConstTags.ShutterLeftVerticalEdge = 0x00181602;

		/// <summary>(0018,1604) VR=IS VM=1 Shutter Right Vertical Edge</summary>
		DicomConstTags.ShutterRightVerticalEdge = 0x00181604;

		/// <summary>(0018,1606) VR=IS VM=1 Shutter Upper Horizontal Edge</summary>
		DicomConstTags.ShutterUpperHorizontalEdge = 0x00181606;

		/// <summary>(0018,1608) VR=IS VM=1 Shutter Lower Horizontal Edge</summary>
		DicomConstTags.ShutterLowerHorizontalEdge = 0x00181608;

		/// <summary>(0018,1610) VR=IS VM=2 Center of Circular Shutter</summary>
		DicomConstTags.CenterOfCircularShutter = 0x00181610;

		/// <summary>(0018,1612) VR=IS VM=1 Radius of Circular Shutter</summary>
		DicomConstTags.RadiusOfCircularShutter = 0x00181612;

		/// <summary>(0018,1620) VR=IS VM=2-2n Vertices of the Polygonal Shutter</summary>
		DicomConstTags.VerticesOfThePolygonalShutter = 0x00181620;

		/// <summary>(0018,1622) VR=US VM=1 Shutter Presentation Value</summary>
		DicomConstTags.ShutterPresentationValue = 0x00181622;

		/// <summary>(0018,1623) VR=US VM=1 Shutter Overlay Group</summary>
		DicomConstTags.ShutterOverlayGroup = 0x00181623;

		/// <summary>(0018,1624) VR=US VM=3 Shutter Presentation Color CIELab Value</summary>
		DicomConstTags.ShutterPresentationColorCIELabValue = 0x00181624;

		/// <summary>(0018,1700) VR=CS VM=1-3 Collimator Shape</summary>
		DicomConstTags.CollimatorShape = 0x00181700;

		/// <summary>(0018,1702) VR=IS VM=1 Collimator Left Vertical Edge</summary>
		DicomConstTags.CollimatorLeftVerticalEdge = 0x00181702;

		/// <summary>(0018,1704) VR=IS VM=1 Collimator Right Vertical Edge</summary>
		DicomConstTags.CollimatorRightVerticalEdge = 0x00181704;

		/// <summary>(0018,1706) VR=IS VM=1 Collimator Upper Horizontal Edge</summary>
		DicomConstTags.CollimatorUpperHorizontalEdge = 0x00181706;

		/// <summary>(0018,1708) VR=IS VM=1 Collimator Lower Horizontal Edge</summary>
		DicomConstTags.CollimatorLowerHorizontalEdge = 0x00181708;

		/// <summary>(0018,1710) VR=IS VM=2 Center of Circular Collimator</summary>
		DicomConstTags.CenterOfCircularCollimator = 0x00181710;

		/// <summary>(0018,1712) VR=IS VM=1 Radius of Circular Collimator</summary>
		DicomConstTags.RadiusOfCircularCollimator = 0x00181712;

		/// <summary>(0018,1720) VR=IS VM=2-2n Vertices of the Polygonal Collimator</summary>
		DicomConstTags.VerticesOfThePolygonalCollimator = 0x00181720;

		/// <summary>(0018,1800) VR=CS VM=1 Acquisition Time Synchronized</summary>
		DicomConstTags.AcquisitionTimeSynchronized = 0x00181800;

		/// <summary>(0018,1801) VR=SH VM=1 Time Source</summary>
		DicomConstTags.TimeSource = 0x00181801;

		/// <summary>(0018,1802) VR=CS VM=1 Time Distribution Protocol</summary>
		DicomConstTags.TimeDistributionProtocol = 0x00181802;

		/// <summary>(0018,1803) VR=LO VM=1 NTP Source Address</summary>
		DicomConstTags.NTPSourceAddress = 0x00181803;

		/// <summary>(0018,2001) VR=IS VM=1-n Page Number Vector</summary>
		DicomConstTags.PageNumberVector = 0x00182001;

		/// <summary>(0018,2002) VR=SH VM=1-n Frame Label Vector</summary>
		DicomConstTags.FrameLabelVector = 0x00182002;

		/// <summary>(0018,2003) VR=DS VM=1-n Frame Primary Angle Vector</summary>
		DicomConstTags.FramePrimaryAngleVector = 0x00182003;

		/// <summary>(0018,2004) VR=DS VM=1-n Frame Secondary Angle Vector</summary>
		DicomConstTags.FrameSecondaryAngleVector = 0x00182004;

		/// <summary>(0018,2005) VR=DS VM=1-n Slice Location Vector</summary>
		DicomConstTags.SliceLocationVector = 0x00182005;

		/// <summary>(0018,2006) VR=SH VM=1-n Display Window Label Vector</summary>
		DicomConstTags.DisplayWindowLabelVector = 0x00182006;

		/// <summary>(0018,2010) VR=DS VM=2 Nominal Scanned Pixel Spacing</summary>
		DicomConstTags.NominalScannedPixelSpacing = 0x00182010;

		/// <summary>(0018,2020) VR=CS VM=1 Digitizing Device Transport Direction</summary>
		DicomConstTags.DigitizingDeviceTransportDirection = 0x00182020;

		/// <summary>(0018,2030) VR=DS VM=1 Rotation of Scanned Film</summary>
		DicomConstTags.RotationOfScannedFilm = 0x00182030;

		/// <summary>(0018,3100) VR=CS VM=1 IVUS Acquisition</summary>
		DicomConstTags.IVUSAcquisition = 0x00183100;

		/// <summary>(0018,3101) VR=DS VM=1 IVUS Pullback Rate</summary>
		DicomConstTags.IVUSPullbackRate = 0x00183101;

		/// <summary>(0018,3102) VR=DS VM=1 IVUS Gated Rate</summary>
		DicomConstTags.IVUSGatedRate = 0x00183102;

		/// <summary>(0018,3103) VR=IS VM=1 IVUS Pullback Start Frame Number</summary>
		DicomConstTags.IVUSPullbackStartFrameNumber = 0x00183103;

		/// <summary>(0018,3104) VR=IS VM=1 IVUS Pullback Stop Frame Number</summary>
		DicomConstTags.IVUSPullbackStopFrameNumber = 0x00183104;

		/// <summary>(0018,3105) VR=IS VM=1-n Lesion Number</summary>
		DicomConstTags.LesionNumber = 0x00183105;

		/// <summary>(0018,5000) VR=SH VM=1-n Output Power</summary>
		DicomConstTags.OutputPower = 0x00185000;

		/// <summary>(0018,5010) VR=LO VM=3 Transducer Data</summary>
		DicomConstTags.TransducerData = 0x00185010;

		/// <summary>(0018,5012) VR=DS VM=1 Focus Depth</summary>
		DicomConstTags.FocusDepth = 0x00185012;

		/// <summary>(0018,5020) VR=LO VM=1 Processing Function</summary>
		DicomConstTags.ProcessingFunction = 0x00185020;

		/// <summary>(0018,5022) VR=DS VM=1 Mechanical Index</summary>
		DicomConstTags.MechanicalIndex = 0x00185022;

		/// <summary>(0018,5024) VR=DS VM=1 Bone Thermal Index</summary>
		DicomConstTags.BoneThermalIndex = 0x00185024;

		/// <summary>(0018,5026) VR=DS VM=1 Cranial Thermal Index</summary>
		DicomConstTags.CranialThermalIndex = 0x00185026;

		/// <summary>(0018,5027) VR=DS VM=1 Soft Tissue Thermal Index</summary>
		DicomConstTags.SoftTissueThermalIndex = 0x00185027;

		/// <summary>(0018,5028) VR=DS VM=1 Soft Tissue-focus Thermal Index</summary>
		DicomConstTags.SoftTissuefocusThermalIndex = 0x00185028;

		/// <summary>(0018,5029) VR=DS VM=1 Soft Tissue-surface Thermal Index</summary>
		DicomConstTags.SoftTissuesurfaceThermalIndex = 0x00185029;

		/// <summary>(0018,5050) VR=IS VM=1 Depth of Scan Field</summary>
		DicomConstTags.DepthOfScanField = 0x00185050;

		/// <summary>(0018,5100) VR=CS VM=1 Patient Position</summary>
		DicomConstTags.PatientPosition = 0x00185100;

		/// <summary>(0018,5101) VR=CS VM=1 View Position</summary>
		DicomConstTags.ViewPosition = 0x00185101;

		/// <summary>(0018,5104) VR=SQ VM=1 Projection Eponymous Name Code Sequence</summary>
		DicomConstTags.ProjectionEponymousNameCodeSequence = 0x00185104;

		/// <summary>(0018,6000) VR=DS VM=1 Sensitivity</summary>
		DicomConstTags.Sensitivity = 0x00186000;

		/// <summary>(0018,6011) VR=SQ VM=1 Sequence of Ultrasound Regions</summary>
		DicomConstTags.SequenceOfUltrasoundRegions = 0x00186011;

		/// <summary>(0018,6012) VR=US VM=1 Region Spatial Format</summary>
		DicomConstTags.RegionSpatialFormat = 0x00186012;

		/// <summary>(0018,6014) VR=US VM=1 Region Data Type</summary>
		DicomConstTags.RegionDataType = 0x00186014;

		/// <summary>(0018,6016) VR=UL VM=1 Region Flags</summary>
		DicomConstTags.RegionFlags = 0x00186016;

		/// <summary>(0018,6018) VR=UL VM=1 Region Location Min X0</summary>
		DicomConstTags.RegionLocationMinX0 = 0x00186018;

		/// <summary>(0018,601a) VR=UL VM=1 Region Location Min Y0</summary>
		DicomConstTags.RegionLocationMinY0 = 0x0018601a;

		/// <summary>(0018,601c) VR=UL VM=1 Region Location Max X1</summary>
		DicomConstTags.RegionLocationMaxX1 = 0x0018601c;

		/// <summary>(0018,601e) VR=UL VM=1 Region Location Max Y1</summary>
		DicomConstTags.RegionLocationMaxY1 = 0x0018601e;

		/// <summary>(0018,6020) VR=SL VM=1 Reference Pixel X0</summary>
		DicomConstTags.ReferencePixelX0 = 0x00186020;

		/// <summary>(0018,6022) VR=SL VM=1 Reference Pixel Y0</summary>
		DicomConstTags.ReferencePixelY0 = 0x00186022;

		/// <summary>(0018,6024) VR=US VM=1 Physical Units X Direction</summary>
		DicomConstTags.PhysicalUnitsXDirection = 0x00186024;

		/// <summary>(0018,6026) VR=US VM=1 Physical Units Y Direction</summary>
		DicomConstTags.PhysicalUnitsYDirection = 0x00186026;

		/// <summary>(0018,6028) VR=FD VM=1 Reference Pixel Physical Value X</summary>
		DicomConstTags.ReferencePixelPhysicalValueX = 0x00186028;

		/// <summary>(0018,602a) VR=FD VM=1 Reference Pixel Physical Value Y</summary>
		DicomConstTags.ReferencePixelPhysicalValueY = 0x0018602a;

		/// <summary>(0018,602c) VR=FD VM=1 Physical Delta X</summary>
		DicomConstTags.PhysicalDeltaX = 0x0018602c;

		/// <summary>(0018,602e) VR=FD VM=1 Physical Delta Y</summary>
		DicomConstTags.PhysicalDeltaY = 0x0018602e;

		/// <summary>(0018,6030) VR=UL VM=1 Transducer Frequency</summary>
		DicomConstTags.TransducerFrequency = 0x00186030;

		/// <summary>(0018,6031) VR=CS VM=1 Transducer Type</summary>
		DicomConstTags.TransducerType = 0x00186031;

		/// <summary>(0018,6032) VR=UL VM=1 Pulse Repetition Frequency</summary>
		DicomConstTags.PulseRepetitionFrequency = 0x00186032;

		/// <summary>(0018,6034) VR=FD VM=1 Doppler Correction Angle</summary>
		DicomConstTags.DopplerCorrectionAngle = 0x00186034;

		/// <summary>(0018,6036) VR=FD VM=1 Steering Angle</summary>
		DicomConstTags.SteeringAngle = 0x00186036;

		/// <summary>(0018,6039) VR=SL VM=1 Doppler Sample Volume X Position</summary>
		DicomConstTags.DopplerSampleVolumeXPosition = 0x00186039;

		/// <summary>(0018,603b) VR=SL VM=1 Doppler Sample Volume Y Position</summary>
		DicomConstTags.DopplerSampleVolumeYPosition = 0x0018603b;

		/// <summary>(0018,603d) VR=SL VM=1 TM-Line Position X0</summary>
		DicomConstTags.TMLinePositionX0 = 0x0018603d;

		/// <summary>(0018,603f) VR=SL VM=1 TM-Line Position Y0</summary>
		DicomConstTags.TMLinePositionY0 = 0x0018603f;

		/// <summary>(0018,6041) VR=SL VM=1 TM-Line Position X1</summary>
		DicomConstTags.TMLinePositionX1 = 0x00186041;

		/// <summary>(0018,6043) VR=SL VM=1 TM-Line Position Y1</summary>
		DicomConstTags.TMLinePositionY1 = 0x00186043;

		/// <summary>(0018,6044) VR=US VM=1 Pixel Component Organization</summary>
		DicomConstTags.PixelComponentOrganization = 0x00186044;

		/// <summary>(0018,6046) VR=UL VM=1 Pixel Component Mask</summary>
		DicomConstTags.PixelComponentMask = 0x00186046;

		/// <summary>(0018,6048) VR=UL VM=1 Pixel Component Range Start</summary>
		DicomConstTags.PixelComponentRangeStart = 0x00186048;

		/// <summary>(0018,604a) VR=UL VM=1 Pixel Component Range Stop</summary>
		DicomConstTags.PixelComponentRangeStop = 0x0018604a;

		/// <summary>(0018,604c) VR=US VM=1 Pixel Component Physical Units</summary>
		DicomConstTags.PixelComponentPhysicalUnits = 0x0018604c;

		/// <summary>(0018,604e) VR=US VM=1 Pixel Component Data Type</summary>
		DicomConstTags.PixelComponentDataType = 0x0018604e;

		/// <summary>(0018,6050) VR=UL VM=1 Number of Table Break Points</summary>
		DicomConstTags.NumberOfTableBreakPoints = 0x00186050;

		/// <summary>(0018,6052) VR=UL VM=1-n Table of X Break Points</summary>
		DicomConstTags.TableOfXBreakPoints = 0x00186052;

		/// <summary>(0018,6054) VR=FD VM=1-n Table of Y Break Points</summary>
		DicomConstTags.TableOfYBreakPoints = 0x00186054;

		/// <summary>(0018,6056) VR=UL VM=1 Number of Table Entries</summary>
		DicomConstTags.NumberOfTableEntries = 0x00186056;

		/// <summary>(0018,6058) VR=UL VM=1-n Table of Pixel Values</summary>
		DicomConstTags.TableOfPixelValues = 0x00186058;

		/// <summary>(0018,605a) VR=FL VM=1-n Table of Parameter Values</summary>
		DicomConstTags.TableOfParameterValues = 0x0018605a;

		/// <summary>(0018,6060) VR=FL VM=1-n R Wave Time Vector</summary>
		DicomConstTags.RWaveTimeVector = 0x00186060;

		/// <summary>(0018,7000) VR=CS VM=1 Detector Conditions Nominal Flag</summary>
		DicomConstTags.DetectorConditionsNominalFlag = 0x00187000;

		/// <summary>(0018,7001) VR=DS VM=1 Detector Temperature</summary>
		DicomConstTags.DetectorTemperature = 0x00187001;

		/// <summary>(0018,7004) VR=CS VM=1 Detector Type</summary>
		DicomConstTags.DetectorType = 0x00187004;

		/// <summary>(0018,7005) VR=CS VM=1 Detector Configuration</summary>
		DicomConstTags.DetectorConfiguration = 0x00187005;

		/// <summary>(0018,7006) VR=LT VM=1 Detector Description</summary>
		DicomConstTags.DetectorDescription = 0x00187006;

		/// <summary>(0018,7008) VR=LT VM=1 Detector Mode</summary>
		DicomConstTags.DetectorMode = 0x00187008;

		/// <summary>(0018,700a) VR=SH VM=1 Detector ID</summary>
		DicomConstTags.DetectorID = 0x0018700a;

		/// <summary>(0018,700c) VR=DA VM=1 Date of Last Detector Calibration</summary>
		DicomConstTags.DateOfLastDetectorCalibration = 0x0018700c;

		/// <summary>(0018,700e) VR=TM VM=1 Time of Last Detector Calibration</summary>
		DicomConstTags.TimeOfLastDetectorCalibration = 0x0018700e;

		/// <summary>(0018,7010) VR=IS VM=1 Exposures on Detector Since Last Calibration</summary>
		DicomConstTags.ExposuresOnDetectorSinceLastCalibration = 0x00187010;

		/// <summary>(0018,7011) VR=IS VM=1 Exposures on Detector Since Manufactured</summary>
		DicomConstTags.ExposuresOnDetectorSinceManufactured = 0x00187011;

		/// <summary>(0018,7012) VR=DS VM=1 Detector Time Since Last Exposure</summary>
		DicomConstTags.DetectorTimeSinceLastExposure = 0x00187012;

		/// <summary>(0018,7014) VR=DS VM=1 Detector Active Time</summary>
		DicomConstTags.DetectorActiveTime = 0x00187014;

		/// <summary>(0018,7016) VR=DS VM=1 Detector Activation Offset From Exposure</summary>
		DicomConstTags.DetectorActivationOffsetFromExposure = 0x00187016;

		/// <summary>(0018,701a) VR=DS VM=2 Detector Binning</summary>
		DicomConstTags.DetectorBinning = 0x0018701a;

		/// <summary>(0018,7020) VR=DS VM=2 Detector Element Physical Size</summary>
		DicomConstTags.DetectorElementPhysicalSize = 0x00187020;

		/// <summary>(0018,7022) VR=DS VM=2 Detector Element Spacing</summary>
		DicomConstTags.DetectorElementSpacing = 0x00187022;

		/// <summary>(0018,7024) VR=CS VM=1 Detector Active Shape</summary>
		DicomConstTags.DetectorActiveShape = 0x00187024;

		/// <summary>(0018,7026) VR=DS VM=1-2 Detector Active Dimension(s)</summary>
		DicomConstTags.DetectorActiveDimensions = 0x00187026;

		/// <summary>(0018,7028) VR=DS VM=2 Detector Active Origin</summary>
		DicomConstTags.DetectorActiveOrigin = 0x00187028;

		/// <summary>(0018,702a) VR=LO VM=1 Detector Manufacturer Name</summary>
		DicomConstTags.DetectorManufacturerName = 0x0018702a;

		/// <summary>(0018,702b) VR=LO VM=1 Detector Manufacturer's Model Name</summary>
		DicomConstTags.DetectorManufacturersModelName = 0x0018702b;

		/// <summary>(0018,7030) VR=DS VM=2 Field of View Origin</summary>
		DicomConstTags.FieldOfViewOrigin = 0x00187030;

		/// <summary>(0018,7032) VR=DS VM=1 Field of View Rotation</summary>
		DicomConstTags.FieldOfViewRotation = 0x00187032;

		/// <summary>(0018,7034) VR=CS VM=1 Field of View Horizontal Flip</summary>
		DicomConstTags.FieldOfViewHorizontalFlip = 0x00187034;

		/// <summary>(0018,7040) VR=LT VM=1 Grid Absorbing Material</summary>
		DicomConstTags.GridAbsorbingMaterial = 0x00187040;

		/// <summary>(0018,7041) VR=LT VM=1 Grid Spacing Material</summary>
		DicomConstTags.GridSpacingMaterial = 0x00187041;

		/// <summary>(0018,7042) VR=DS VM=1 Grid Thickness</summary>
		DicomConstTags.GridThickness = 0x00187042;

		/// <summary>(0018,7044) VR=DS VM=1 Grid Pitch</summary>
		DicomConstTags.GridPitch = 0x00187044;

		/// <summary>(0018,7046) VR=IS VM=2 Grid Aspect Ratio</summary>
		DicomConstTags.GridAspectRatio = 0x00187046;

		/// <summary>(0018,7048) VR=DS VM=1 Grid Period</summary>
		DicomConstTags.GridPeriod = 0x00187048;

		/// <summary>(0018,704c) VR=DS VM=1 Grid Focal Distance</summary>
		DicomConstTags.GridFocalDistance = 0x0018704c;

		/// <summary>(0018,7050) VR=CS VM=1-n Filter Material</summary>
		DicomConstTags.FilterMaterial = 0x00187050;

		/// <summary>(0018,7052) VR=DS VM=1-n Filter Thickness Minimum</summary>
		DicomConstTags.FilterThicknessMinimum = 0x00187052;

		/// <summary>(0018,7054) VR=DS VM=1-n Filter Thickness Maximum</summary>
		DicomConstTags.FilterThicknessMaximum = 0x00187054;

		/// <summary>(0018,7060) VR=CS VM=1 Exposure Control Mode</summary>
		DicomConstTags.ExposureControlMode = 0x00187060;

		/// <summary>(0018,7062) VR=LT VM=1 Exposure Control Mode Description</summary>
		DicomConstTags.ExposureControlModeDescription = 0x00187062;

		/// <summary>(0018,7064) VR=CS VM=1 Exposure Status</summary>
		DicomConstTags.ExposureStatus = 0x00187064;

		/// <summary>(0018,7065) VR=DS VM=1 Phototimer Setting</summary>
		DicomConstTags.PhototimerSetting = 0x00187065;

		/// <summary>(0018,8150) VR=DS VM=1 Exposure Time in uS</summary>
		DicomConstTags.ExposureTimeInMicroS = 0x00188150;

		/// <summary>(0018,8151) VR=DS VM=1 X-Ray Tube Current in uA</summary>
		DicomConstTags.XRayTubeCurrentInMicroA = 0x00188151;

		/// <summary>(0018,9004) VR=CS VM=1 Content Qualification</summary>
		DicomConstTags.ContentQualification = 0x00189004;

		/// <summary>(0018,9005) VR=SH VM=1 Pulse Sequence Name</summary>
		DicomConstTags.PulseSequenceName = 0x00189005;

		/// <summary>(0018,9006) VR=SQ VM=1 MR Imaging Modifier Sequence</summary>
		DicomConstTags.MRImagingModifierSequence = 0x00189006;

		/// <summary>(0018,9008) VR=CS VM=1 Echo Pulse Sequence</summary>
		DicomConstTags.EchoPulseSequence = 0x00189008;

		/// <summary>(0018,9009) VR=CS VM=1 Inversion Recovery</summary>
		DicomConstTags.InversionRecovery = 0x00189009;

		/// <summary>(0018,9010) VR=CS VM=1 Flow Compensation</summary>
		DicomConstTags.FlowCompensation = 0x00189010;

		/// <summary>(0018,9011) VR=CS VM=1 Multiple Spin Echo</summary>
		DicomConstTags.MultipleSpinEcho = 0x00189011;

		/// <summary>(0018,9012) VR=CS VM=1 Multi-planar Excitation</summary>
		DicomConstTags.MultiplanarExcitation = 0x00189012;

		/// <summary>(0018,9014) VR=CS VM=1 Phase Contrast</summary>
		DicomConstTags.PhaseContrast = 0x00189014;

		/// <summary>(0018,9015) VR=CS VM=1 Time of Flight Contrast</summary>
		DicomConstTags.TimeOfFlightContrast = 0x00189015;

		/// <summary>(0018,9016) VR=CS VM=1 Spoiling</summary>
		DicomConstTags.Spoiling = 0x00189016;

		/// <summary>(0018,9017) VR=CS VM=1 Steady State Pulse Sequence</summary>
		DicomConstTags.SteadyStatePulseSequence = 0x00189017;

		/// <summary>(0018,9018) VR=CS VM=1 Echo Planar Pulse Sequence</summary>
		DicomConstTags.EchoPlanarPulseSequence = 0x00189018;

		/// <summary>(0018,9019) VR=FD VM=1 Tag Angle First Axis</summary>
		DicomConstTags.TagAngleFirstAxis = 0x00189019;

		/// <summary>(0018,9020) VR=CS VM=1 Magnetization Transfer</summary>
		DicomConstTags.MagnetizationTransfer = 0x00189020;

		/// <summary>(0018,9021) VR=CS VM=1 T2 Preparation</summary>
		DicomConstTags.T2Preparation = 0x00189021;

		/// <summary>(0018,9022) VR=CS VM=1 Blood Signal Nulling</summary>
		DicomConstTags.BloodSignalNulling = 0x00189022;

		/// <summary>(0018,9024) VR=CS VM=1 Saturation Recovery</summary>
		DicomConstTags.SaturationRecovery = 0x00189024;

		/// <summary>(0018,9025) VR=CS VM=1 Spectrally Selected Suppression</summary>
		DicomConstTags.SpectrallySelectedSuppression = 0x00189025;

		/// <summary>(0018,9026) VR=CS VM=1 Spectrally Selected Excitation</summary>
		DicomConstTags.SpectrallySelectedExcitation = 0x00189026;

		/// <summary>(0018,9027) VR=CS VM=1 Spatial Pre-saturation</summary>
		DicomConstTags.SpatialPresaturation = 0x00189027;

		/// <summary>(0018,9028) VR=CS VM=1 Tagging</summary>
		DicomConstTags.Tagging = 0x00189028;

		/// <summary>(0018,9029) VR=CS VM=1 Oversampling Phase</summary>
		DicomConstTags.OversamplingPhase = 0x00189029;

		/// <summary>(0018,9030) VR=FD VM=1 Tag Spacing First Dimension</summary>
		DicomConstTags.TagSpacingFirstDimension = 0x00189030;

		/// <summary>(0018,9032) VR=CS VM=1 Geometry of k-Space Traversal</summary>
		DicomConstTags.GeometryOfKSpaceTraversal = 0x00189032;

		/// <summary>(0018,9033) VR=CS VM=1 Segmented k-Space Traversal</summary>
		DicomConstTags.SegmentedKSpaceTraversal = 0x00189033;

		/// <summary>(0018,9034) VR=CS VM=1 Rectilinear Phase Encode Reordering</summary>
		DicomConstTags.RectilinearPhaseEncodeReordering = 0x00189034;

		/// <summary>(0018,9035) VR=FD VM=1 Tag Thickness</summary>
		DicomConstTags.TagThickness = 0x00189035;

		/// <summary>(0018,9036) VR=CS VM=1 Partial Fourier Direction</summary>
		DicomConstTags.PartialFourierDirection = 0x00189036;

		/// <summary>(0018,9037) VR=CS VM=1 Cardiac Synchronization Technique</summary>
		DicomConstTags.CardiacSynchronizationTechnique = 0x00189037;

		/// <summary>(0018,9041) VR=LO VM=1 Receive Coil Manufacturer Name</summary>
		DicomConstTags.ReceiveCoilManufacturerName = 0x00189041;

		/// <summary>(0018,9042) VR=SQ VM=1 MR Receive Coil Sequence</summary>
		DicomConstTags.MRReceiveCoilSequence = 0x00189042;

		/// <summary>(0018,9043) VR=CS VM=1 Receive Coil Type</summary>
		DicomConstTags.ReceiveCoilType = 0x00189043;

		/// <summary>(0018,9044) VR=CS VM=1 Quadrature Receive Coil</summary>
		DicomConstTags.QuadratureReceiveCoil = 0x00189044;

		/// <summary>(0018,9045) VR=SQ VM=1 Multi-Coil Definition Sequence</summary>
		DicomConstTags.MultiCoilDefinitionSequence = 0x00189045;

		/// <summary>(0018,9046) VR=LO VM=1 Multi-Coil Configuration</summary>
		DicomConstTags.MultiCoilConfiguration = 0x00189046;

		/// <summary>(0018,9047) VR=SH VM=1 Multi-Coil Element Name</summary>
		DicomConstTags.MultiCoilElementName = 0x00189047;

		/// <summary>(0018,9048) VR=CS VM=1 Multi-Coil Element Used</summary>
		DicomConstTags.MultiCoilElementUsed = 0x00189048;

		/// <summary>(0018,9049) VR=SQ VM=1 MR Transmit Coil Sequence</summary>
		DicomConstTags.MRTransmitCoilSequence = 0x00189049;

		/// <summary>(0018,9050) VR=LO VM=1 Transmit Coil Manufacturer Name</summary>
		DicomConstTags.TransmitCoilManufacturerName = 0x00189050;

		/// <summary>(0018,9051) VR=CS VM=1 Transmit Coil Type</summary>
		DicomConstTags.TransmitCoilType = 0x00189051;

		/// <summary>(0018,9052) VR=FD VM=1-2 Spectral Width</summary>
		DicomConstTags.SpectralWidth = 0x00189052;

		/// <summary>(0018,9053) VR=FD VM=1-2 Chemical Shift Reference</summary>
		DicomConstTags.ChemicalShiftReference = 0x00189053;

		/// <summary>(0018,9054) VR=CS VM=1 Volume Localization Technique</summary>
		DicomConstTags.VolumeLocalizationTechnique = 0x00189054;

		/// <summary>(0018,9058) VR=US VM=1 MR Acquisition Frequency Encoding Steps</summary>
		DicomConstTags.MRAcquisitionFrequencyEncodingSteps = 0x00189058;

		/// <summary>(0018,9059) VR=CS VM=1 De-coupling</summary>
		DicomConstTags.Decoupling = 0x00189059;

		/// <summary>(0018,9060) VR=CS VM=1-2 De-coupled Nucleus</summary>
		DicomConstTags.DecoupledNucleus = 0x00189060;

		/// <summary>(0018,9061) VR=FD VM=1-2 De-coupling Frequency</summary>
		DicomConstTags.DecouplingFrequency = 0x00189061;

		/// <summary>(0018,9062) VR=CS VM=1 De-coupling Method</summary>
		DicomConstTags.DecouplingMethod = 0x00189062;

		/// <summary>(0018,9063) VR=FD VM=1-2 De-coupling Chemical Shift Reference</summary>
		DicomConstTags.DecouplingChemicalShiftReference = 0x00189063;

		/// <summary>(0018,9064) VR=CS VM=1 k-space Filtering</summary>
		DicomConstTags.KspaceFiltering = 0x00189064;

		/// <summary>(0018,9065) VR=CS VM=1-2 Time Domain Filtering</summary>
		DicomConstTags.TimeDomainFiltering = 0x00189065;

		/// <summary>(0018,9066) VR=US VM=1-2 Number of Zero fills</summary>
		DicomConstTags.NumberOfZeroFills = 0x00189066;

		/// <summary>(0018,9067) VR=CS VM=1 Baseline Correction</summary>
		DicomConstTags.BaselineCorrection = 0x00189067;

		/// <summary>(0018,9069) VR=FD VM=1 Parallel Reduction Factor In-plane</summary>
		DicomConstTags.ParallelReductionFactorInplane = 0x00189069;

		/// <summary>(0018,9070) VR=FD VM=1 Cardiac R-R Interval Specified</summary>
		DicomConstTags.CardiacRRIntervalSpecified = 0x00189070;

		/// <summary>(0018,9073) VR=FD VM=1 Acquisition Duration</summary>
		DicomConstTags.AcquisitionDuration = 0x00189073;

		/// <summary>(0018,9074) VR=DT VM=1 Frame Acquisition DateTime</summary>
		DicomConstTags.FrameAcquisitionDateTime = 0x00189074;

		/// <summary>(0018,9075) VR=CS VM=1 Diffusion Directionality</summary>
		DicomConstTags.DiffusionDirectionality = 0x00189075;

		/// <summary>(0018,9076) VR=SQ VM=1 Diffusion Gradient Direction Sequence</summary>
		DicomConstTags.DiffusionGradientDirectionSequence = 0x00189076;

		/// <summary>(0018,9077) VR=CS VM=1 Parallel Acquisition</summary>
		DicomConstTags.ParallelAcquisition = 0x00189077;

		/// <summary>(0018,9078) VR=CS VM=1 Parallel Acquisition Technique</summary>
		DicomConstTags.ParallelAcquisitionTechnique = 0x00189078;

		/// <summary>(0018,9079) VR=FD VM=1-n Inversion Times</summary>
		DicomConstTags.InversionTimes = 0x00189079;

		/// <summary>(0018,9080) VR=ST VM=1 Metabolite Map Description</summary>
		DicomConstTags.MetaboliteMapDescription = 0x00189080;

		/// <summary>(0018,9081) VR=CS VM=1 Partial Fourier</summary>
		DicomConstTags.PartialFourier = 0x00189081;

		/// <summary>(0018,9082) VR=FD VM=1 Effective Echo Time</summary>
		DicomConstTags.EffectiveEchoTime = 0x00189082;

		/// <summary>(0018,9083) VR=SQ VM=1 Metabolite Map Code Sequence</summary>
		DicomConstTags.MetaboliteMapCodeSequence = 0x00189083;

		/// <summary>(0018,9084) VR=SQ VM=1 Chemical Shift Sequence</summary>
		DicomConstTags.ChemicalShiftSequence = 0x00189084;

		/// <summary>(0018,9085) VR=CS VM=1 Cardiac Signal Source</summary>
		DicomConstTags.CardiacSignalSource = 0x00189085;

		/// <summary>(0018,9087) VR=FD VM=1 Diffusion b-value</summary>
		DicomConstTags.DiffusionBvalue = 0x00189087;

		/// <summary>(0018,9089) VR=FD VM=3 Diffusion Gradient Orientation</summary>
		DicomConstTags.DiffusionGradientOrientation = 0x00189089;

		/// <summary>(0018,9090) VR=FD VM=3 Velocity Encoding Direction</summary>
		DicomConstTags.VelocityEncodingDirection = 0x00189090;

		/// <summary>(0018,9091) VR=FD VM=1 Velocity Encoding Minimum Value</summary>
		DicomConstTags.VelocityEncodingMinimumValue = 0x00189091;

		/// <summary>(0018,9093) VR=US VM=1 Number of k-Space Trajectories</summary>
		DicomConstTags.NumberOfKSpaceTrajectories = 0x00189093;

		/// <summary>(0018,9094) VR=CS VM=1 Coverage of k-Space</summary>
		DicomConstTags.CoverageOfKSpace = 0x00189094;

		/// <summary>(0018,9095) VR=UL VM=1 Spectroscopy Acquisition Phase Rows</summary>
		DicomConstTags.SpectroscopyAcquisitionPhaseRows = 0x00189095;

		/// <summary>(0018,9098) VR=FD VM=1-2 Transmitter Frequency</summary>
		DicomConstTags.TransmitterFrequency = 0x00189098;

		/// <summary>(0018,9100) VR=CS VM=1-2 Resonant Nucleus</summary>
		DicomConstTags.ResonantNucleus = 0x00189100;

		/// <summary>(0018,9101) VR=CS VM=1 Frequency Correction</summary>
		DicomConstTags.FrequencyCorrection = 0x00189101;

		/// <summary>(0018,9103) VR=SQ VM=1 MR Spectroscopy FOV/Geometry Sequence</summary>
		DicomConstTags.MRSpectroscopyFOVGeometrySequence = 0x00189103;

		/// <summary>(0018,9104) VR=FD VM=1 Slab Thickness</summary>
		DicomConstTags.SlabThickness = 0x00189104;

		/// <summary>(0018,9105) VR=FD VM=3 Slab Orientation</summary>
		DicomConstTags.SlabOrientation = 0x00189105;

		/// <summary>(0018,9106) VR=FD VM=3 Mid Slab Position</summary>
		DicomConstTags.MidSlabPosition = 0x00189106;

		/// <summary>(0018,9107) VR=SQ VM=1 MR Spatial Saturation Sequence</summary>
		DicomConstTags.MRSpatialSaturationSequence = 0x00189107;

		/// <summary>(0018,9112) VR=SQ VM=1 MR Timing and Related Parameters Sequence</summary>
		DicomConstTags.MRTimingAndRelatedParametersSequence = 0x00189112;

		/// <summary>(0018,9114) VR=SQ VM=1 MR Echo Sequence</summary>
		DicomConstTags.MREchoSequence = 0x00189114;

		/// <summary>(0018,9115) VR=SQ VM=1 MR Modifier Sequence</summary>
		DicomConstTags.MRModifierSequence = 0x00189115;

		/// <summary>(0018,9117) VR=SQ VM=1 MR Diffusion Sequence</summary>
		DicomConstTags.MRDiffusionSequence = 0x00189117;

		/// <summary>(0018,9118) VR=SQ VM=1 Cardiac Synchronization Sequence</summary>
		DicomConstTags.CardiacSynchronizationSequence = 0x00189118;

		/// <summary>(0018,9119) VR=SQ VM=1 MR Averages Sequence</summary>
		DicomConstTags.MRAveragesSequence = 0x00189119;

		/// <summary>(0018,9125) VR=SQ VM=1 MR FOV/Geometry Sequence</summary>
		DicomConstTags.MRFOVGeometrySequence = 0x00189125;

		/// <summary>(0018,9126) VR=SQ VM=1 Volume Localization Sequence</summary>
		DicomConstTags.VolumeLocalizationSequence = 0x00189126;

		/// <summary>(0018,9127) VR=UL VM=1 Spectroscopy Acquisition Data Columns</summary>
		DicomConstTags.SpectroscopyAcquisitionDataColumns = 0x00189127;

		/// <summary>(0018,9147) VR=CS VM=1 Diffusion Anisotropy Type</summary>
		DicomConstTags.DiffusionAnisotropyType = 0x00189147;

		/// <summary>(0018,9151) VR=DT VM=1 Frame Reference DateTime</summary>
		DicomConstTags.FrameReferenceDateTime = 0x00189151;

		/// <summary>(0018,9152) VR=SQ VM=1 MR Metabolite Map Sequence</summary>
		DicomConstTags.MRMetaboliteMapSequence = 0x00189152;

		/// <summary>(0018,9155) VR=FD VM=1 Parallel Reduction Factor out-of-plane</summary>
		DicomConstTags.ParallelReductionFactorOutofplane = 0x00189155;

		/// <summary>(0018,9159) VR=UL VM=1 Spectroscopy Acquisition Out-of-plane Phase Steps</summary>
		DicomConstTags.SpectroscopyAcquisitionOutofplanePhaseSteps = 0x00189159;

		/// <summary>(0018,9168) VR=FD VM=1 Parallel Reduction Factor Second In-plane</summary>
		DicomConstTags.ParallelReductionFactorSecondInplane = 0x00189168;

		/// <summary>(0018,9169) VR=CS VM=1 Cardiac Beat Rejection Technique</summary>
		DicomConstTags.CardiacBeatRejectionTechnique = 0x00189169;

		/// <summary>(0018,9170) VR=CS VM=1 Respiratory Motion Compensation Technique</summary>
		DicomConstTags.RespiratoryMotionCompensationTechnique = 0x00189170;

		/// <summary>(0018,9171) VR=CS VM=1 Respiratory Signal Source</summary>
		DicomConstTags.RespiratorySignalSource = 0x00189171;

		/// <summary>(0018,9172) VR=CS VM=1 Bulk Motion Compensation Technique</summary>
		DicomConstTags.BulkMotionCompensationTechnique = 0x00189172;

		/// <summary>(0018,9173) VR=CS VM=1 Bulk Motion Signal Source</summary>
		DicomConstTags.BulkMotionSignalSource = 0x00189173;

		/// <summary>(0018,9174) VR=CS VM=1 Applicable Safety Standard Agency</summary>
		DicomConstTags.ApplicableSafetyStandardAgency = 0x00189174;

		/// <summary>(0018,9175) VR=LO VM=1 Applicable Safety Standard Description</summary>
		DicomConstTags.ApplicableSafetyStandardDescription = 0x00189175;

		/// <summary>(0018,9176) VR=SQ VM=1 Operating Mode Sequence</summary>
		DicomConstTags.OperatingModeSequence = 0x00189176;

		/// <summary>(0018,9177) VR=CS VM=1 Operating Mode Type</summary>
		DicomConstTags.OperatingModeType = 0x00189177;

		/// <summary>(0018,9178) VR=CS VM=1 Operating Mode</summary>
		DicomConstTags.OperatingMode = 0x00189178;

		/// <summary>(0018,9179) VR=CS VM=1 Specific Absorption Rate Definition</summary>
		DicomConstTags.SpecificAbsorptionRateDefinition = 0x00189179;

		/// <summary>(0018,9180) VR=CS VM=1 Gradient Output Type</summary>
		DicomConstTags.GradientOutputType = 0x00189180;

		/// <summary>(0018,9181) VR=FD VM=1 Specific Absorption Rate Value</summary>
		DicomConstTags.SpecificAbsorptionRateValue = 0x00189181;

		/// <summary>(0018,9182) VR=FD VM=1 Gradient Output</summary>
		DicomConstTags.GradientOutput = 0x00189182;

		/// <summary>(0018,9183) VR=CS VM=1 Flow Compensation Direction</summary>
		DicomConstTags.FlowCompensationDirection = 0x00189183;

		/// <summary>(0018,9184) VR=FD VM=1 Tagging Delay</summary>
		DicomConstTags.TaggingDelay = 0x00189184;

		/// <summary>(0018,9185) VR=ST VM=1 Respiratory Motion Compensation Technique Description</summary>
		DicomConstTags.RespiratoryMotionCompensationTechniqueDescription = 0x00189185;

		/// <summary>(0018,9186) VR=SH VM=1 Respiratory Signal Source ID</summary>
		DicomConstTags.RespiratorySignalSourceID = 0x00189186;

		/// <summary>(0018,9197) VR=SQ VM=1 MR Velocity Encoding Sequence</summary>
		DicomConstTags.MRVelocityEncodingSequence = 0x00189197;

		/// <summary>(0018,9198) VR=CS VM=1 First Order Phase Correction</summary>
		DicomConstTags.FirstOrderPhaseCorrection = 0x00189198;

		/// <summary>(0018,9199) VR=CS VM=1 Water Referenced Phase Correction</summary>
		DicomConstTags.WaterReferencedPhaseCorrection = 0x00189199;

		/// <summary>(0018,9200) VR=CS VM=1 MR Spectroscopy Acquisition Type</summary>
		DicomConstTags.MRSpectroscopyAcquisitionType = 0x00189200;

		/// <summary>(0018,9214) VR=CS VM=1 Respiratory Cycle Position</summary>
		DicomConstTags.RespiratoryCyclePosition = 0x00189214;

		/// <summary>(0018,9217) VR=FD VM=1 Velocity Encoding Maximum Value</summary>
		DicomConstTags.VelocityEncodingMaximumValue = 0x00189217;

		/// <summary>(0018,9218) VR=FD VM=1 Tag Spacing Second Dimension</summary>
		DicomConstTags.TagSpacingSecondDimension = 0x00189218;

		/// <summary>(0018,9219) VR=SS VM=1 Tag Angle Second Axis</summary>
		DicomConstTags.TagAngleSecondAxis = 0x00189219;

		/// <summary>(0018,9220) VR=FD VM=1 Frame Acquisition Duration</summary>
		DicomConstTags.FrameAcquisitionDuration = 0x00189220;

		/// <summary>(0018,9226) VR=SQ VM=1 MR Image Frame Type Sequence</summary>
		DicomConstTags.MRImageFrameTypeSequence = 0x00189226;

		/// <summary>(0018,9227) VR=SQ VM=1 MR Spectroscopy Frame Type Sequence</summary>
		DicomConstTags.MRSpectroscopyFrameTypeSequence = 0x00189227;

		/// <summary>(0018,9231) VR=US VM=1 MR Acquisition Phase Encoding Steps in-plane</summary>
		DicomConstTags.MRAcquisitionPhaseEncodingStepsInplane = 0x00189231;

		/// <summary>(0018,9232) VR=US VM=1 MR Acquisition Phase Encoding Steps out-of-plane</summary>
		DicomConstTags.MRAcquisitionPhaseEncodingStepsOutofplane = 0x00189232;

		/// <summary>(0018,9234) VR=UL VM=1 Spectroscopy Acquisition Phase Columns</summary>
		DicomConstTags.SpectroscopyAcquisitionPhaseColumns = 0x00189234;

		/// <summary>(0018,9236) VR=CS VM=1 Cardiac Cycle Position</summary>
		DicomConstTags.CardiacCyclePosition = 0x00189236;

		/// <summary>(0018,9239) VR=SQ VM=1 Specific Absorption Rate Sequence</summary>
		DicomConstTags.SpecificAbsorptionRateSequence = 0x00189239;

		/// <summary>(0018,9240) VR=US VM=1 RF Echo Train Length</summary>
		DicomConstTags.RFEchoTrainLength = 0x00189240;

		/// <summary>(0018,9241) VR=US VM=1 Gradient Echo Train Length</summary>
		DicomConstTags.GradientEchoTrainLength = 0x00189241;

		/// <summary>(0018,9295) VR=FD VM=1 Chemical Shifts Minimum Integration Limit in ppm</summary>
		DicomConstTags.ChemicalShiftsMinimumIntegrationLimitInPpm = 0x00189295;

		/// <summary>(0018,9296) VR=FD VM=1 Chemical Shifts Maximum Integration Limit in ppm</summary>
		DicomConstTags.ChemicalShiftsMaximumIntegrationLimitInPpm = 0x00189296;

		/// <summary>(0018,9301) VR=SQ VM=1 CT Acquisition Type Sequence</summary>
		DicomConstTags.CTAcquisitionTypeSequence = 0x00189301;

		/// <summary>(0018,9302) VR=CS VM=1 Acquisition Type</summary>
		DicomConstTags.AcquisitionType = 0x00189302;

		/// <summary>(0018,9303) VR=FD VM=1 Tube Angle</summary>
		DicomConstTags.TubeAngle = 0x00189303;

		/// <summary>(0018,9304) VR=SQ VM=1 CT Acquisition Details Sequence</summary>
		DicomConstTags.CTAcquisitionDetailsSequence = 0x00189304;

		/// <summary>(0018,9305) VR=FD VM=1 Revolution Time</summary>
		DicomConstTags.RevolutionTime = 0x00189305;

		/// <summary>(0018,9306) VR=FD VM=1 Single Collimation Width</summary>
		DicomConstTags.SingleCollimationWidth = 0x00189306;

		/// <summary>(0018,9307) VR=FD VM=1 Total Collimation Width</summary>
		DicomConstTags.TotalCollimationWidth = 0x00189307;

		/// <summary>(0018,9308) VR=SQ VM=1 CT Table Dynamics Sequence</summary>
		DicomConstTags.CTTableDynamicsSequence = 0x00189308;

		/// <summary>(0018,9309) VR=FD VM=1 Table Speed</summary>
		DicomConstTags.TableSpeed = 0x00189309;

		/// <summary>(0018,9310) VR=FD VM=1 Table Feed per Rotation</summary>
		DicomConstTags.TableFeedPerRotation = 0x00189310;

		/// <summary>(0018,9311) VR=FD VM=1 Spiral Pitch Factor</summary>
		DicomConstTags.SpiralPitchFactor = 0x00189311;

		/// <summary>(0018,9312) VR=SQ VM=1 CT Geometry Sequence</summary>
		DicomConstTags.CTGeometrySequence = 0x00189312;

		/// <summary>(0018,9313) VR=FD VM=3 Data Collection Center (Patient)</summary>
		DicomConstTags.DataCollectionCenterPatient = 0x00189313;

		/// <summary>(0018,9314) VR=SQ VM=1 CT Reconstruction Sequence</summary>
		DicomConstTags.CTReconstructionSequence = 0x00189314;

		/// <summary>(0018,9315) VR=CS VM=1 Reconstruction Algorithm</summary>
		DicomConstTags.ReconstructionAlgorithm = 0x00189315;

		/// <summary>(0018,9316) VR=CS VM=1 Convolution Kernel Group</summary>
		DicomConstTags.ConvolutionKernelGroup = 0x00189316;

		/// <summary>(0018,9317) VR=FD VM=2 Reconstruction Field of View</summary>
		DicomConstTags.ReconstructionFieldOfView = 0x00189317;

		/// <summary>(0018,9318) VR=FD VM=3 Reconstruction Target Center (Patient)</summary>
		DicomConstTags.ReconstructionTargetCenterPatient = 0x00189318;

		/// <summary>(0018,9319) VR=FD VM=1 Reconstruction Angle</summary>
		DicomConstTags.ReconstructionAngle = 0x00189319;

		/// <summary>(0018,9320) VR=SH VM=1 Image Filter</summary>
		DicomConstTags.ImageFilter = 0x00189320;

		/// <summary>(0018,9321) VR=SQ VM=1 CT Exposure Sequence</summary>
		DicomConstTags.CTExposureSequence = 0x00189321;

		/// <summary>(0018,9322) VR=FD VM=2 Reconstruction Pixel Spacing</summary>
		DicomConstTags.ReconstructionPixelSpacing = 0x00189322;

		/// <summary>(0018,9323) VR=CS VM=1 Exposure Modulation Type</summary>
		DicomConstTags.ExposureModulationType = 0x00189323;

		/// <summary>(0018,9324) VR=FD VM=1 Estimated Dose Saving</summary>
		DicomConstTags.EstimatedDoseSaving = 0x00189324;

		/// <summary>(0018,9325) VR=SQ VM=1 CT X-Ray Details Sequence</summary>
		DicomConstTags.CTXRayDetailsSequence = 0x00189325;

		/// <summary>(0018,9326) VR=SQ VM=1 CT Position Sequence</summary>
		DicomConstTags.CTPositionSequence = 0x00189326;

		/// <summary>(0018,9327) VR=FD VM=1 Table Position</summary>
		DicomConstTags.TablePosition = 0x00189327;

		/// <summary>(0018,9328) VR=FD VM=1 Exposure Time in ms</summary>
		DicomConstTags.ExposureTimeInMs = 0x00189328;

		/// <summary>(0018,9329) VR=SQ VM=1 CT Image Frame Type Sequence</summary>
		DicomConstTags.CTImageFrameTypeSequence = 0x00189329;

		/// <summary>(0018,9330) VR=FD VM=1 X-Ray Tube Current in mA</summary>
		DicomConstTags.XRayTubeCurrentInMA = 0x00189330;

		/// <summary>(0018,9332) VR=FD VM=1 Exposure in mAs</summary>
		DicomConstTags.ExposureInMAs = 0x00189332;

		/// <summary>(0018,9333) VR=CS VM=1 Constant Volume Flag</summary>
		DicomConstTags.ConstantVolumeFlag = 0x00189333;

		/// <summary>(0018,9334) VR=CS VM=1 Fluoroscopy Flag</summary>
		DicomConstTags.FluoroscopyFlag = 0x00189334;

		/// <summary>(0018,9335) VR=FD VM=1 Distance Source to Data Collection Center</summary>
		DicomConstTags.DistanceSourceToDataCollectionCenter = 0x00189335;

		/// <summary>(0018,9337) VR=US VM=1 Contrast/Bolus Agent Number</summary>
		DicomConstTags.ContrastBolusAgentNumber = 0x00189337;

		/// <summary>(0018,9338) VR=SQ VM=1 Contrast/Bolus Ingredient Code Sequence</summary>
		DicomConstTags.ContrastBolusIngredientCodeSequence = 0x00189338;

		/// <summary>(0018,9340) VR=SQ VM=1 Contrast Administration Profile Sequence</summary>
		DicomConstTags.ContrastAdministrationProfileSequence = 0x00189340;

		/// <summary>(0018,9341) VR=SQ VM=1 Contrast/Bolus Usage Sequence</summary>
		DicomConstTags.ContrastBolusUsageSequence = 0x00189341;

		/// <summary>(0018,9342) VR=CS VM=1 Contrast/Bolus Agent Administered</summary>
		DicomConstTags.ContrastBolusAgentAdministered = 0x00189342;

		/// <summary>(0018,9343) VR=CS VM=1 Contrast/Bolus Agent Detected</summary>
		DicomConstTags.ContrastBolusAgentDetected = 0x00189343;

		/// <summary>(0018,9344) VR=CS VM=1 Contrast/Bolus Agent Phase</summary>
		DicomConstTags.ContrastBolusAgentPhase = 0x00189344;

		/// <summary>(0018,9345) VR=FD VM=1 CTDIvol</summary>
		DicomConstTags.CTDIvol = 0x00189345;

		/// <summary>(0018,9346) VR=SQ VM=1 CTDI Phantom Type Code Sequence</summary>
		DicomConstTags.CTDIPhantomTypeCodeSequence = 0x00189346;

		/// <summary>(0018,9351) VR=FL VM=1 Calcium Scoring Mass Factor Patient</summary>
		DicomConstTags.CalciumScoringMassFactorPatient = 0x00189351;

		/// <summary>(0018,9352) VR=FL VM=3 Calcium Scoring Mass Factor Device</summary>
		DicomConstTags.CalciumScoringMassFactorDevice = 0x00189352;

		/// <summary>(0018,9360) VR=SQ VM=1 CT Additional X-Ray Source Sequence</summary>
		DicomConstTags.CTAdditionalXRaySourceSequence = 0x00189360;

		/// <summary>(0018,9401) VR=SQ VM=1 Projection Pixel Calibration Sequence</summary>
		DicomConstTags.ProjectionPixelCalibrationSequence = 0x00189401;

		/// <summary>(0018,9402) VR=FL VM=1 Distance Source to Isocenter</summary>
		DicomConstTags.DistanceSourceToIsocenter = 0x00189402;

		/// <summary>(0018,9403) VR=FL VM=1 Distance Object to Table Top</summary>
		DicomConstTags.DistanceObjectToTableTop = 0x00189403;

		/// <summary>(0018,9404) VR=FL VM=2 Object Pixel Spacing in Center of Beam</summary>
		DicomConstTags.ObjectPixelSpacingInCenterOfBeam = 0x00189404;

		/// <summary>(0018,9405) VR=SQ VM=1 Positioner Position Sequence</summary>
		DicomConstTags.PositionerPositionSequence = 0x00189405;

		/// <summary>(0018,9406) VR=SQ VM=1 Table Position Sequence</summary>
		DicomConstTags.TablePositionSequence = 0x00189406;

		/// <summary>(0018,9407) VR=SQ VM=1 Collimator Shape Sequence</summary>
		DicomConstTags.CollimatorShapeSequence = 0x00189407;

		/// <summary>(0018,9412) VR=SQ VM=1 XA/XRF Frame Characteristics Sequence</summary>
		DicomConstTags.XAXRFFrameCharacteristicsSequence = 0x00189412;

		/// <summary>(0018,9417) VR=SQ VM=1 Frame Acquisition Sequence</summary>
		DicomConstTags.FrameAcquisitionSequence = 0x00189417;

		/// <summary>(0018,9420) VR=CS VM=1 X-Ray Receptor Type</summary>
		DicomConstTags.XRayReceptorType = 0x00189420;

		/// <summary>(0018,9423) VR=LO VM=1 Acquisition Protocol Name</summary>
		DicomConstTags.AcquisitionProtocolName = 0x00189423;

		/// <summary>(0018,9424) VR=LT VM=1 Acquisition Protocol Description</summary>
		DicomConstTags.AcquisitionProtocolDescription = 0x00189424;

		/// <summary>(0018,9425) VR=CS VM=1 Contrast/Bolus Ingredient Opaque</summary>
		DicomConstTags.ContrastBolusIngredientOpaque = 0x00189425;

		/// <summary>(0018,9426) VR=FL VM=1 Distance Receptor Plane to Detector Housing</summary>
		DicomConstTags.DistanceReceptorPlaneToDetectorHousing = 0x00189426;

		/// <summary>(0018,9427) VR=CS VM=1 Intensifier Active Shape</summary>
		DicomConstTags.IntensifierActiveShape = 0x00189427;

		/// <summary>(0018,9428) VR=FL VM=1-2 Intensifier Active Dimension(s)</summary>
		DicomConstTags.IntensifierActiveDimensions = 0x00189428;

		/// <summary>(0018,9429) VR=FL VM=2 Physical Detector Size</summary>
		DicomConstTags.PhysicalDetectorSize = 0x00189429;

		/// <summary>(0018,9430) VR=US VM=2 Position of Isocenter Projection</summary>
		DicomConstTags.PositionOfIsocenterProjection = 0x00189430;

		/// <summary>(0018,9432) VR=SQ VM=1 Field of View Sequence</summary>
		DicomConstTags.FieldOfViewSequence = 0x00189432;

		/// <summary>(0018,9433) VR=LO VM=1 Field of View Description</summary>
		DicomConstTags.FieldOfViewDescription = 0x00189433;

		/// <summary>(0018,9434) VR=SQ VM=1 Exposure Control Sensing Regions Sequence</summary>
		DicomConstTags.ExposureControlSensingRegionsSequence = 0x00189434;

		/// <summary>(0018,9435) VR=CS VM=1 Exposure Control Sensing Region Shape</summary>
		DicomConstTags.ExposureControlSensingRegionShape = 0x00189435;

		/// <summary>(0018,9436) VR=SS VM=1 Exposure Control Sensing Region Left Vertical Edge</summary>
		DicomConstTags.ExposureControlSensingRegionLeftVerticalEdge = 0x00189436;

		/// <summary>(0018,9437) VR=SS VM=1 Exposure Control Sensing Region Right Vertical Edge</summary>
		DicomConstTags.ExposureControlSensingRegionRightVerticalEdge = 0x00189437;

		/// <summary>(0018,9438) VR=SS VM=1 Exposure Control Sensing Region Upper Horizontal Edge</summary>
		DicomConstTags.ExposureControlSensingRegionUpperHorizontalEdge = 0x00189438;

		/// <summary>(0018,9439) VR=SS VM=1 Exposure Control Sensing Region Lower Horizontal Edge</summary>
		DicomConstTags.ExposureControlSensingRegionLowerHorizontalEdge = 0x00189439;

		/// <summary>(0018,9440) VR=SS VM=2 Center of Circular Exposure Control Sensing Region</summary>
		DicomConstTags.CenterOfCircularExposureControlSensingRegion = 0x00189440;

		/// <summary>(0018,9441) VR=US VM=1 Radius of Circular Exposure Control Sensing Region</summary>
		DicomConstTags.RadiusOfCircularExposureControlSensingRegion = 0x00189441;

		/// <summary>(0018,9442) VR=SS VM=2-n Vertices of the Polygonal Exposure Control Sensing Region</summary>
		DicomConstTags.VerticesOfThePolygonalExposureControlSensingRegion = 0x00189442;

		/// <summary>(0018,9447) VR=FL VM=1 Column Angulation (Patient)</summary>
		DicomConstTags.ColumnAngulationPatient = 0x00189447;

		/// <summary>(0018,9449) VR=FL VM=1 Beam Angle</summary>
		DicomConstTags.BeamAngle = 0x00189449;

		/// <summary>(0018,9451) VR=SQ VM=1 Frame Detector Parameters Sequence</summary>
		DicomConstTags.FrameDetectorParametersSequence = 0x00189451;

		/// <summary>(0018,9452) VR=FL VM=1 Calculated Anatomy Thickness</summary>
		DicomConstTags.CalculatedAnatomyThickness = 0x00189452;

		/// <summary>(0018,9455) VR=SQ VM=1 Calibration Sequence</summary>
		DicomConstTags.CalibrationSequence = 0x00189455;

		/// <summary>(0018,9456) VR=SQ VM=1 Object Thickness Sequence</summary>
		DicomConstTags.ObjectThicknessSequence = 0x00189456;

		/// <summary>(0018,9457) VR=CS VM=1 Plane Identification</summary>
		DicomConstTags.PlaneIdentification = 0x00189457;

		/// <summary>(0018,9461) VR=FL VM=1-2 Field of View Dimension(s) in Float</summary>
		DicomConstTags.FieldOfViewDimensionsInFloat = 0x00189461;

		/// <summary>(0018,9462) VR=SQ VM=1 Isocenter Reference System Sequence</summary>
		DicomConstTags.IsocenterReferenceSystemSequence = 0x00189462;

		/// <summary>(0018,9463) VR=FL VM=1 Positioner Isocenter Primary Angle</summary>
		DicomConstTags.PositionerIsocenterPrimaryAngle = 0x00189463;

		/// <summary>(0018,9464) VR=FL VM=1 Positioner Isocenter Secondary Angle</summary>
		DicomConstTags.PositionerIsocenterSecondaryAngle = 0x00189464;

		/// <summary>(0018,9465) VR=FL VM=1 Positioner Isocenter Detector Rotation Angle</summary>
		DicomConstTags.PositionerIsocenterDetectorRotationAngle = 0x00189465;

		/// <summary>(0018,9466) VR=FL VM=1 Table X Position to Isocenter</summary>
		DicomConstTags.TableXPositionToIsocenter = 0x00189466;

		/// <summary>(0018,9467) VR=FL VM=1 Table Y Position to Isocenter</summary>
		DicomConstTags.TableYPositionToIsocenter = 0x00189467;

		/// <summary>(0018,9468) VR=FL VM=1 Table Z Position to Isocenter</summary>
		DicomConstTags.TableZPositionToIsocenter = 0x00189468;

		/// <summary>(0018,9469) VR=FL VM=1 Table Horizontal Rotation Angle</summary>
		DicomConstTags.TableHorizontalRotationAngle = 0x00189469;

		/// <summary>(0018,9470) VR=FL VM=1 Table Head Tilt Angle</summary>
		DicomConstTags.TableHeadTiltAngle = 0x00189470;

		/// <summary>(0018,9471) VR=FL VM=1 Table Cradle Tilt Angle</summary>
		DicomConstTags.TableCradleTiltAngle = 0x00189471;

		/// <summary>(0018,9472) VR=SQ VM=1 Frame Display Shutter Sequence</summary>
		DicomConstTags.FrameDisplayShutterSequence = 0x00189472;

		/// <summary>(0018,9473) VR=FL VM=1 Acquired Image Area Dose Product</summary>
		DicomConstTags.AcquiredImageAreaDoseProduct = 0x00189473;

		/// <summary>(0018,9474) VR=CS VM=1 C-arm Positioner Tabletop Relationship</summary>
		DicomConstTags.CarmPositionerTabletopRelationship = 0x00189474;

		/// <summary>(0018,9476) VR=SQ VM=1 X-Ray Geometry Sequence</summary>
		DicomConstTags.XRayGeometrySequence = 0x00189476;

		/// <summary>(0018,9477) VR=SQ VM=1 Irradiation Event Identification Sequence</summary>
		DicomConstTags.IrradiationEventIdentificationSequence = 0x00189477;

		/// <summary>(0018,9504) VR=SQ VM=1 X-Ray 3D Frame Type Sequence</summary>
		DicomConstTags.XRay3DFrameTypeSequence = 0x00189504;

		/// <summary>(0018,9506) VR=SQ VM=1 Contributing Sources Sequence</summary>
		DicomConstTags.ContributingSourcesSequence = 0x00189506;

		/// <summary>(0018,9507) VR=SQ VM=1 X-Ray 3D Acquisition Sequence</summary>
		DicomConstTags.XRay3DAcquisitionSequence = 0x00189507;

		/// <summary>(0018,9508) VR=FL VM=1 Primary Positioner Scan Arc</summary>
		DicomConstTags.PrimaryPositionerScanArc = 0x00189508;

		/// <summary>(0018,9509) VR=FL VM=1 Secondary Positioner Scan Arc</summary>
		DicomConstTags.SecondaryPositionerScanArc = 0x00189509;

		/// <summary>(0018,9510) VR=FL VM=1 Primary Positioner Scan Start Angle</summary>
		DicomConstTags.PrimaryPositionerScanStartAngle = 0x00189510;

		/// <summary>(0018,9511) VR=FL VM=1 Secondary Positioner Scan Start Angle</summary>
		DicomConstTags.SecondaryPositionerScanStartAngle = 0x00189511;

		/// <summary>(0018,9514) VR=FL VM=1 Primary Positioner Increment</summary>
		DicomConstTags.PrimaryPositionerIncrement = 0x00189514;

		/// <summary>(0018,9515) VR=FL VM=1 Secondary Positioner Increment</summary>
		DicomConstTags.SecondaryPositionerIncrement = 0x00189515;

		/// <summary>(0018,9516) VR=DT VM=1 Start Acquisition DateTime</summary>
		DicomConstTags.StartAcquisitionDateTime = 0x00189516;

		/// <summary>(0018,9517) VR=DT VM=1 End Acquisition DateTime</summary>
		DicomConstTags.EndAcquisitionDateTime = 0x00189517;

		/// <summary>(0018,9524) VR=LO VM=1 Application Name</summary>
		DicomConstTags.ApplicationName = 0x00189524;

		/// <summary>(0018,9525) VR=LO VM=1 Application Version</summary>
		DicomConstTags.ApplicationVersion = 0x00189525;

		/// <summary>(0018,9526) VR=LO VM=1 Application Manufacturer</summary>
		DicomConstTags.ApplicationManufacturer = 0x00189526;

		/// <summary>(0018,9527) VR=CS VM=1 Algorithm Type</summary>
		DicomConstTags.AlgorithmType = 0x00189527;

		/// <summary>(0018,9528) VR=LO VM=1 Algorithm Description</summary>
		DicomConstTags.AlgorithmDescription = 0x00189528;

		/// <summary>(0018,9530) VR=SQ VM=1 X-Ray 3D Reconstruction Sequence</summary>
		DicomConstTags.XRay3DReconstructionSequence = 0x00189530;

		/// <summary>(0018,9531) VR=LO VM=1 Reconstruction Description</summary>
		DicomConstTags.ReconstructionDescription = 0x00189531;

		/// <summary>(0018,9538) VR=SQ VM=1 Per Projection Acquisition Sequence</summary>
		DicomConstTags.PerProjectionAcquisitionSequence = 0x00189538;

		/// <summary>(0018,9601) VR=SQ VM=1 Diffusion b-matrix Sequence</summary>
		DicomConstTags.DiffusionBmatrixSequence = 0x00189601;

		/// <summary>(0018,9602) VR=FD VM=1 Diffusion b-value XX</summary>
		DicomConstTags.DiffusionBvalueXX = 0x00189602;

		/// <summary>(0018,9603) VR=FD VM=1 Diffusion b-value XY</summary>
		DicomConstTags.DiffusionBvalueXY = 0x00189603;

		/// <summary>(0018,9604) VR=FD VM=1 Diffusion b-value XZ</summary>
		DicomConstTags.DiffusionBvalueXZ = 0x00189604;

		/// <summary>(0018,9605) VR=FD VM=1 Diffusion b-value YY</summary>
		DicomConstTags.DiffusionBvalueYY = 0x00189605;

		/// <summary>(0018,9606) VR=FD VM=1 Diffusion b-value YZ</summary>
		DicomConstTags.DiffusionBvalueYZ = 0x00189606;

		/// <summary>(0018,9607) VR=FD VM=1 Diffusion b-value ZZ</summary>
		DicomConstTags.DiffusionBvalueZZ = 0x00189607;

		/// <summary>(0018,a001) VR=SQ VM=1 Contributing Equipment Sequence</summary>
		DicomConstTags.ContributingEquipmentSequence = 0x0018a001;

		/// <summary>(0018,a002) VR=DT VM=1 Contribution Date Time</summary>
		DicomConstTags.ContributionDateTime = 0x0018a002;

		/// <summary>(0018,a003) VR=ST VM=1 Contribution Description</summary>
		DicomConstTags.ContributionDescription = 0x0018a003;

		/// <summary>(0020,000d) VR=UI VM=1 Study Instance UID</summary>
		DicomConstTags.StudyInstanceUID = 0x0020000d;

		/// <summary>(0020,000e) VR=UI VM=1 Series Instance UID</summary>
		DicomConstTags.SeriesInstanceUID = 0x0020000e;

		/// <summary>(0020,0010) VR=SH VM=1 Study ID</summary>
		DicomConstTags.StudyID = 0x00200010;

		/// <summary>(0020,0011) VR=IS VM=1 Series Number</summary>
		DicomConstTags.SeriesNumber = 0x00200011;

		/// <summary>(0020,0012) VR=IS VM=1 Acquisition Number</summary>
		DicomConstTags.AcquisitionNumber = 0x00200012;

		/// <summary>(0020,0013) VR=IS VM=1 Instance Number</summary>
		DicomConstTags.InstanceNumber = 0x00200013;

		/// <summary>(0020,0019) VR=IS VM=1 Item Number</summary>
		DicomConstTags.ItemNumber = 0x00200019;

		/// <summary>(0020,0020) VR=CS VM=2 Patient Orientation</summary>
		DicomConstTags.PatientOrientation = 0x00200020;

		/// <summary>(0020,0032) VR=DS VM=3 Image Position (Patient)</summary>
		DicomConstTags.ImagePositionPatient = 0x00200032;

		/// <summary>(0020,0037) VR=DS VM=6 Image Orientation (Patient)</summary>
		DicomConstTags.ImageOrientationPatient = 0x00200037;

		/// <summary>(0020,0052) VR=UI VM=1 Frame of Reference UID</summary>
		DicomConstTags.FrameOfReferenceUID = 0x00200052;

		/// <summary>(0020,0060) VR=CS VM=1 Laterality</summary>
		DicomConstTags.Laterality = 0x00200060;

		/// <summary>(0020,0062) VR=CS VM=1 Image Laterality</summary>
		DicomConstTags.ImageLaterality = 0x00200062;

		/// <summary>(0020,0100) VR=IS VM=1 Temporal Position Identifier</summary>
		DicomConstTags.TemporalPositionIdentifier = 0x00200100;

		/// <summary>(0020,0105) VR=IS VM=1 Number of Temporal Positions</summary>
		DicomConstTags.NumberOfTemporalPositions = 0x00200105;

		/// <summary>(0020,0110) VR=DS VM=1 Temporal Resolution</summary>
		DicomConstTags.TemporalResolution = 0x00200110;

		/// <summary>(0020,0200) VR=UI VM=1 Synchronization Frame of Reference UID</summary>
		DicomConstTags.SynchronizationFrameOfReferenceUID = 0x00200200;

		/// <summary>(0020,1002) VR=IS VM=1 Images in Acquisition</summary>
		DicomConstTags.ImagesInAcquisition = 0x00201002;

		/// <summary>(0020,1040) VR=LO VM=1 Position Reference Indicator</summary>
		DicomConstTags.PositionReferenceIndicator = 0x00201040;

		/// <summary>(0020,1041) VR=DS VM=1 Slice Location</summary>
		DicomConstTags.SliceLocation = 0x00201041;

		/// <summary>(0020,1200) VR=IS VM=1 Number of Patient Related Studies</summary>
		DicomConstTags.NumberOfPatientRelatedStudies = 0x00201200;

		/// <summary>(0020,1202) VR=IS VM=1 Number of Patient Related Series</summary>
		DicomConstTags.NumberOfPatientRelatedSeries = 0x00201202;

		/// <summary>(0020,1204) VR=IS VM=1 Number of Patient Related Instances</summary>
		DicomConstTags.NumberOfPatientRelatedInstances = 0x00201204;

		/// <summary>(0020,1206) VR=IS VM=1 Number of Study Related Series</summary>
		DicomConstTags.NumberOfStudyRelatedSeries = 0x00201206;

		/// <summary>(0020,1208) VR=IS VM=1 Number of Study Related Instances</summary>
		DicomConstTags.NumberOfStudyRelatedInstances = 0x00201208;

		/// <summary>(0020,1209) VR=IS VM=1 Number of Series Related Instances</summary>
		DicomConstTags.NumberOfSeriesRelatedInstances = 0x00201209;

		/// <summary>(0020,4000) VR=LT VM=1 Image Comments</summary>
		DicomConstTags.ImageComments = 0x00204000;

		/// <summary>(0020,9056) VR=SH VM=1 Stack ID</summary>
		DicomConstTags.StackID = 0x00209056;

		/// <summary>(0020,9057) VR=UL VM=1 In-Stack Position Number</summary>
		DicomConstTags.InStackPositionNumber = 0x00209057;

		/// <summary>(0020,9071) VR=SQ VM=1 Frame Anatomy Sequence</summary>
		DicomConstTags.FrameAnatomySequence = 0x00209071;

		/// <summary>(0020,9072) VR=CS VM=1 Frame Laterality</summary>
		DicomConstTags.FrameLaterality = 0x00209072;

		/// <summary>(0020,9111) VR=SQ VM=1 Frame Content Sequence</summary>
		DicomConstTags.FrameContentSequence = 0x00209111;

		/// <summary>(0020,9113) VR=SQ VM=1 Plane Position Sequence</summary>
		DicomConstTags.PlanePositionSequence = 0x00209113;

		/// <summary>(0020,9116) VR=SQ VM=1 Plane Orientation Sequence</summary>
		DicomConstTags.PlaneOrientationSequence = 0x00209116;

		/// <summary>(0020,9128) VR=UL VM=1 Temporal Position Index</summary>
		DicomConstTags.TemporalPositionIndex = 0x00209128;

		/// <summary>(0020,9153) VR=FD VM=1 Nominal Cardiac Trigger Delay Time</summary>
		DicomConstTags.NominalCardiacTriggerDelayTime = 0x00209153;

		/// <summary>(0020,9156) VR=US VM=1 Frame Acquisition Number</summary>
		DicomConstTags.FrameAcquisitionNumber = 0x00209156;

		/// <summary>(0020,9157) VR=UL VM=1-n Dimension Index Values</summary>
		DicomConstTags.DimensionIndexValues = 0x00209157;

		/// <summary>(0020,9158) VR=LT VM=1 Frame Comments</summary>
		DicomConstTags.FrameComments = 0x00209158;

		/// <summary>(0020,9161) VR=UI VM=1 Concatenation UID</summary>
		DicomConstTags.ConcatenationUID = 0x00209161;

		/// <summary>(0020,9162) VR=US VM=1 In-concatenation Number</summary>
		DicomConstTags.InconcatenationNumber = 0x00209162;

		/// <summary>(0020,9163) VR=US VM=1 In-concatenation Total Number</summary>
		DicomConstTags.InconcatenationTotalNumber = 0x00209163;

		/// <summary>(0020,9164) VR=UI VM=1 Dimension Organization UID</summary>
		DicomConstTags.DimensionOrganizationUID = 0x00209164;

		/// <summary>(0020,9165) VR=AT VM=1 Dimension Index Pointer</summary>
		DicomConstTags.DimensionIndexPointer = 0x00209165;

		/// <summary>(0020,9167) VR=AT VM=1 Functional Group Pointer</summary>
		DicomConstTags.FunctionalGroupPointer = 0x00209167;

		/// <summary>(0020,9213) VR=LO VM=1 Dimension Index Private Creator</summary>
		DicomConstTags.DimensionIndexPrivateCreator = 0x00209213;

		/// <summary>(0020,9221) VR=SQ VM=1 Dimension Organization Sequence</summary>
		DicomConstTags.DimensionOrganizationSequence = 0x00209221;

		/// <summary>(0020,9222) VR=SQ VM=1 Dimension Index Sequence</summary>
		DicomConstTags.DimensionIndexSequence = 0x00209222;

		/// <summary>(0020,9228) VR=UL VM=1 Concatenation Frame Offset Number</summary>
		DicomConstTags.ConcatenationFrameOffsetNumber = 0x00209228;

		/// <summary>(0020,9238) VR=LO VM=1 Functional Group Private Creator</summary>
		DicomConstTags.FunctionalGroupPrivateCreator = 0x00209238;

		/// <summary>(0020,9241) VR=FL VM=1 Nominal Percentage of Cardiac Phase</summary>
		DicomConstTags.NominalPercentageOfCardiacPhase = 0x00209241;

		/// <summary>(0020,9245) VR=FL VM=1 Nominal Percentage of Respiratory Phase</summary>
		DicomConstTags.NominalPercentageOfRespiratoryPhase = 0x00209245;

		/// <summary>(0020,9246) VR=FL VM=1 Starting Respiratory Amplitude</summary>
		DicomConstTags.StartingRespiratoryAmplitude = 0x00209246;

		/// <summary>(0020,9247) VR=CS VM=1 Starting Respiratory Phase</summary>
		DicomConstTags.StartingRespiratoryPhase = 0x00209247;

		/// <summary>(0020,9248) VR=FL VM=1 Ending Respiratory Amplitude</summary>
		DicomConstTags.EndingRespiratoryAmplitude = 0x00209248;

		/// <summary>(0020,9249) VR=CS VM=1 Ending Respiratory Phase</summary>
		DicomConstTags.EndingRespiratoryPhase = 0x00209249;

		/// <summary>(0020,9250) VR=CS VM=1 Respiratory Trigger Type</summary>
		DicomConstTags.RespiratoryTriggerType = 0x00209250;

		/// <summary>(0020,9251) VR=FD VM=1 R - R Interval Time Nominal</summary>
		DicomConstTags.RRIntervalTimeNominal = 0x00209251;

		/// <summary>(0020,9252) VR=FD VM=1 Actual Cardiac Trigger Delay Time</summary>
		DicomConstTags.ActualCardiacTriggerDelayTime = 0x00209252;

		/// <summary>(0020,9253) VR=SQ VM=1 Respiratory Synchronization Sequence</summary>
		DicomConstTags.RespiratorySynchronizationSequence = 0x00209253;

		/// <summary>(0020,9254) VR=FD VM=1 Respiratory Interval Time</summary>
		DicomConstTags.RespiratoryIntervalTime = 0x00209254;

		/// <summary>(0020,9255) VR=FD VM=1 Nominal Respiratory Trigger Delay Time</summary>
		DicomConstTags.NominalRespiratoryTriggerDelayTime = 0x00209255;

		/// <summary>(0020,9256) VR=FD VM=1 Respiratory Trigger Delay Threshold</summary>
		DicomConstTags.RespiratoryTriggerDelayThreshold = 0x00209256;

		/// <summary>(0020,9257) VR=FD VM=1 Actual Respiratory Trigger Delay Time</summary>
		DicomConstTags.ActualRespiratoryTriggerDelayTime = 0x00209257;

		/// <summary>(0020,9421) VR=LO VM=1 Dimension Description Label</summary>
		DicomConstTags.DimensionDescriptionLabel = 0x00209421;

		/// <summary>(0020,9450) VR=SQ VM=1 Patient Orientation in Frame Sequence</summary>
		DicomConstTags.PatientOrientationInFrameSequence = 0x00209450;

		/// <summary>(0020,9453) VR=LO VM=1 Frame Label</summary>
		DicomConstTags.FrameLabel = 0x00209453;

		/// <summary>(0020,9518) VR=US VM=1-n Acquisition Index</summary>
		DicomConstTags.AcquisitionIndex = 0x00209518;

		/// <summary>(0020,9529) VR=SQ VM=1 Contributing SOP Instances Reference Sequence</summary>
		DicomConstTags.ContributingSOPInstancesReferenceSequence = 0x00209529;

		/// <summary>(0020,9536) VR=US VM=1 Reconstruction Index</summary>
		DicomConstTags.ReconstructionIndex = 0x00209536;

		/// <summary>(0022,0001) VR=US VM=1 Light Path Filter Pass-Through Wavelength</summary>
		DicomConstTags.LightPathFilterPassThroughWavelength = 0x00220001;

		/// <summary>(0022,0002) VR=US VM=2 Light Path Filter Pass Band</summary>
		DicomConstTags.LightPathFilterPassBand = 0x00220002;

		/// <summary>(0022,0003) VR=US VM=1 Image Path Filter Pass-Through Wavelength</summary>
		DicomConstTags.ImagePathFilterPassThroughWavelength = 0x00220003;

		/// <summary>(0022,0004) VR=US VM=2 Image Path Filter Pass Band</summary>
		DicomConstTags.ImagePathFilterPassBand = 0x00220004;

		/// <summary>(0022,0005) VR=CS VM=1 Patient Eye Movement Commanded</summary>
		DicomConstTags.PatientEyeMovementCommanded = 0x00220005;

		/// <summary>(0022,0006) VR=SQ VM=1 Patient Eye Movement Command Code Sequence</summary>
		DicomConstTags.PatientEyeMovementCommandCodeSequence = 0x00220006;

		/// <summary>(0022,0007) VR=FL VM=1 Spherical Lens Power</summary>
		DicomConstTags.SphericalLensPower = 0x00220007;

		/// <summary>(0022,0008) VR=FL VM=1 Cylinder Lens Power</summary>
		DicomConstTags.CylinderLensPower = 0x00220008;

		/// <summary>(0022,0009) VR=FL VM=1 Cylinder Axis</summary>
		DicomConstTags.CylinderAxis = 0x00220009;

		/// <summary>(0022,000a) VR=FL VM=1 Emmetropic Magnification</summary>
		DicomConstTags.EmmetropicMagnification = 0x0022000a;

		/// <summary>(0022,000b) VR=FL VM=1 Intra Ocular Pressure</summary>
		DicomConstTags.IntraOcularPressure = 0x0022000b;

		/// <summary>(0022,000c) VR=FL VM=1 Horizontal Field of View</summary>
		DicomConstTags.HorizontalFieldOfView = 0x0022000c;

		/// <summary>(0022,000d) VR=CS VM=1 Pupil Dilated</summary>
		DicomConstTags.PupilDilated = 0x0022000d;

		/// <summary>(0022,000e) VR=FL VM=1 Degree of Dilation</summary>
		DicomConstTags.DegreeOfDilation = 0x0022000e;

		/// <summary>(0022,0010) VR=FL VM=1 Stereo Baseline Angle</summary>
		DicomConstTags.StereoBaselineAngle = 0x00220010;

		/// <summary>(0022,0011) VR=FL VM=1 Stereo Baseline Displacement</summary>
		DicomConstTags.StereoBaselineDisplacement = 0x00220011;

		/// <summary>(0022,0012) VR=FL VM=1 Stereo Horizontal Pixel Offset</summary>
		DicomConstTags.StereoHorizontalPixelOffset = 0x00220012;

		/// <summary>(0022,0013) VR=FL VM=1 Stereo Vertical Pixel Offset</summary>
		DicomConstTags.StereoVerticalPixelOffset = 0x00220013;

		/// <summary>(0022,0014) VR=FL VM=1 Stereo Rotation</summary>
		DicomConstTags.StereoRotation = 0x00220014;

		/// <summary>(0022,0015) VR=SQ VM=1 Acquisition Device Type Code Sequence</summary>
		DicomConstTags.AcquisitionDeviceTypeCodeSequence = 0x00220015;

		/// <summary>(0022,0016) VR=SQ VM=1 Illumination Type Code Sequence</summary>
		DicomConstTags.IlluminationTypeCodeSequence = 0x00220016;

		/// <summary>(0022,0017) VR=SQ VM=1 Light Path Filter Type Stack Code Sequence</summary>
		DicomConstTags.LightPathFilterTypeStackCodeSequence = 0x00220017;

		/// <summary>(0022,0018) VR=SQ VM=1 Image Path Filter Type Stack Code Sequence</summary>
		DicomConstTags.ImagePathFilterTypeStackCodeSequence = 0x00220018;

		/// <summary>(0022,0019) VR=SQ VM=1 Lenses Code Sequence</summary>
		DicomConstTags.LensesCodeSequence = 0x00220019;

		/// <summary>(0022,001a) VR=SQ VM=1 Channel Description Code Sequence</summary>
		DicomConstTags.ChannelDescriptionCodeSequence = 0x0022001a;

		/// <summary>(0022,001b) VR=SQ VM=1 Refractive State Sequence</summary>
		DicomConstTags.RefractiveStateSequence = 0x0022001b;

		/// <summary>(0022,001c) VR=SQ VM=1 Mydriatic Agent Code Sequence</summary>
		DicomConstTags.MydriaticAgentCodeSequence = 0x0022001c;

		/// <summary>(0022,001d) VR=SQ VM=1 Relative Image Position Code Sequence</summary>
		DicomConstTags.RelativeImagePositionCodeSequence = 0x0022001d;

		/// <summary>(0022,0020) VR=SQ VM=1 Stereo Pairs Sequence</summary>
		DicomConstTags.StereoPairsSequence = 0x00220020;

		/// <summary>(0022,0021) VR=SQ VM=1 Left Image Sequence</summary>
		DicomConstTags.LeftImageSequence = 0x00220021;

		/// <summary>(0022,0022) VR=SQ VM=1 Right Image Sequence</summary>
		DicomConstTags.RightImageSequence = 0x00220022;

		/// <summary>(0022,0030) VR=FL VM=1 Axial Length of the Eye</summary>
		DicomConstTags.AxialLengthOfTheEye = 0x00220030;

		/// <summary>(0022,0031) VR=SQ VM=1 Ophthalmic Frame Location Sequence</summary>
		DicomConstTags.OphthalmicFrameLocationSequence = 0x00220031;

		/// <summary>(0022,0032) VR=FL VM=2-2n Reference Coordinates</summary>
		DicomConstTags.ReferenceCoordinates = 0x00220032;

		/// <summary>(0022,0035) VR=FL VM=1 Depth Spatial Resolution</summary>
		DicomConstTags.DepthSpatialResolution = 0x00220035;

		/// <summary>(0022,0036) VR=FL VM=1 Maximum Depth Distortion</summary>
		DicomConstTags.MaximumDepthDistortion = 0x00220036;

		/// <summary>(0022,0037) VR=FL VM=1 Along-scan Spatial Resolution</summary>
		DicomConstTags.AlongscanSpatialResolution = 0x00220037;

		/// <summary>(0022,0038) VR=FL VM=1 Maximum Along-scan Distortion</summary>
		DicomConstTags.MaximumAlongscanDistortion = 0x00220038;

		/// <summary>(0022,0039) VR=CS VM=1 Ophthalmic Image Orientation</summary>
		DicomConstTags.OphthalmicImageOrientation = 0x00220039;

		/// <summary>(0022,0041) VR=FL VM=1 Depth of Transverse Image</summary>
		DicomConstTags.DepthOfTransverseImage = 0x00220041;

		/// <summary>(0022,0042) VR=SQ VM=1 Mydriatic Agent Concentration Units Sequence</summary>
		DicomConstTags.MydriaticAgentConcentrationUnitsSequence = 0x00220042;

		/// <summary>(0022,0048) VR=FL VM=1 Across-scan Spatial Resolution</summary>
		DicomConstTags.AcrossscanSpatialResolution = 0x00220048;

		/// <summary>(0022,0049) VR=FL VM=1 Maximum Across-scan Distortion</summary>
		DicomConstTags.MaximumAcrossscanDistortion = 0x00220049;

		/// <summary>(0022,004e) VR=DS VM=1 Mydriatic Agent Concentration</summary>
		DicomConstTags.MydriaticAgentConcentration = 0x0022004e;

		/// <summary>(0022,0055) VR=FL VM=1 Illumination Wave Length</summary>
		DicomConstTags.IlluminationWaveLength = 0x00220055;

		/// <summary>(0022,0056) VR=FL VM=1 Illumination Power</summary>
		DicomConstTags.IlluminationPower = 0x00220056;

		/// <summary>(0022,0057) VR=FL VM=1 Illumination Bandwidth</summary>
		DicomConstTags.IlluminationBandwidth = 0x00220057;

		/// <summary>(0022,0058) VR=SQ VM=1 Mydriatic Agent Sequence</summary>
		DicomConstTags.MydriaticAgentSequence = 0x00220058;

		/// <summary>(0028,0002) VR=US VM=1 Samples per Pixel</summary>
		DicomConstTags.SamplesPerPixel = 0x00280002;

		/// <summary>(0028,0003) VR=US VM=1 Samples per Pixel Used</summary>
		DicomConstTags.SamplesPerPixelUsed = 0x00280003;

		/// <summary>(0028,0004) VR=CS VM=1 Photometric Interpretation</summary>
		DicomConstTags.PhotometricInterpretation = 0x00280004;

		/// <summary>(0028,0006) VR=US VM=1 Planar Configuration</summary>
		DicomConstTags.PlanarConfiguration = 0x00280006;

		/// <summary>(0028,0008) VR=IS VM=1 Number of Frames</summary>
		DicomConstTags.NumberOfFrames = 0x00280008;

		/// <summary>(0028,0009) VR=AT VM=1-n Frame Increment Pointer</summary>
		DicomConstTags.FrameIncrementPointer = 0x00280009;

		/// <summary>(0028,000a) VR=AT VM=1-n Frame Dimension Pointer</summary>
		DicomConstTags.FrameDimensionPointer = 0x0028000a;

		/// <summary>(0028,0010) VR=US VM=1 Rows</summary>
		DicomConstTags.Rows = 0x00280010;

		/// <summary>(0028,0011) VR=US VM=1 Columns</summary>
		DicomConstTags.Columns = 0x00280011;

		/// <summary>(0028,0014) VR=US VM=1 Ultrasound Color Data Present</summary>
		DicomConstTags.UltrasoundColorDataPresent = 0x00280014;

		/// <summary>(0028,0030) VR=DS VM=2 Pixel Spacing</summary>
		DicomConstTags.PixelSpacing = 0x00280030;

		/// <summary>(0028,0031) VR=DS VM=2 Zoom Factor</summary>
		DicomConstTags.ZoomFactor = 0x00280031;

		/// <summary>(0028,0032) VR=DS VM=2 Zoom Center</summary>
		DicomConstTags.ZoomCenter = 0x00280032;

		/// <summary>(0028,0034) VR=IS VM=2 Pixel Aspect Ratio</summary>
		DicomConstTags.PixelAspectRatio = 0x00280034;

		/// <summary>(0028,0051) VR=CS VM=1-n Corrected Image</summary>
		DicomConstTags.CorrectedImage = 0x00280051;

		/// <summary>(0028,0100) VR=US VM=1 Bits Allocated</summary>
		DicomConstTags.BitsAllocated = 0x00280100;

		/// <summary>(0028,0101) VR=US VM=1 Bits Stored</summary>
		DicomConstTags.BitsStored = 0x00280101;

		/// <summary>(0028,0102) VR=US VM=1 High Bit</summary>
		DicomConstTags.HighBit = 0x00280102;

		/// <summary>(0028,0103) VR=US VM=1 Pixel Representation</summary>
		DicomConstTags.PixelRepresentation = 0x00280103;

		/// <summary>(0028,0106) VR=US/SS VM=1 Smallest Image Pixel Value</summary>
		DicomConstTags.SmallestImagePixelValue = 0x00280106;

		/// <summary>(0028,0107) VR=US/SS VM=1 Largest Image Pixel Value</summary>
		DicomConstTags.LargestImagePixelValue = 0x00280107;

		/// <summary>(0028,0108) VR=US/SS VM=1 Smallest Pixel Value in Series</summary>
		DicomConstTags.SmallestPixelValueInSeries = 0x00280108;

		/// <summary>(0028,0109) VR=US/SS VM=1 Largest Pixel Value in Series</summary>
		DicomConstTags.LargestPixelValueInSeries = 0x00280109;

		/// <summary>(0028,0120) VR=US/SS VM=1 Pixel Padding Value</summary>
		DicomConstTags.PixelPaddingValue = 0x00280120;

		/// <summary>(0028,0121) VR=US/SS VM=1 Pixel Padding Range Limit</summary>
		DicomConstTags.PixelPaddingRangeLimit = 0x00280121;

		/// <summary>(0028,0300) VR=CS VM=1 Quality Control Image</summary>
		DicomConstTags.QualityControlImage = 0x00280300;

		/// <summary>(0028,0301) VR=CS VM=1 Burned In Annotation</summary>
		DicomConstTags.BurnedInAnnotation = 0x00280301;

		/// <summary>(0028,0a02) VR=CS VM=1 Pixel Spacing Calibration Type</summary>
		DicomConstTags.PixelSpacingCalibrationType = 0x00280a02;

		/// <summary>(0028,0a04) VR=LO VM=1 Pixel Spacing Calibration Description</summary>
		DicomConstTags.PixelSpacingCalibrationDescription = 0x00280a04;

		/// <summary>(0028,1040) VR=CS VM=1 Pixel Intensity Relationship</summary>
		DicomConstTags.PixelIntensityRelationship = 0x00281040;

		/// <summary>(0028,1041) VR=SS VM=1 Pixel Intensity Relationship Sign</summary>
		DicomConstTags.PixelIntensityRelationshipSign = 0x00281041;

		/// <summary>(0028,1050) VR=DS VM=1-n Window Center</summary>
		DicomConstTags.WindowCenter = 0x00281050;

		/// <summary>(0028,1051) VR=DS VM=1-n Window Width</summary>
		DicomConstTags.WindowWidth = 0x00281051;

		/// <summary>(0028,1052) VR=DS VM=1 Rescale Intercept</summary>
		DicomConstTags.RescaleIntercept = 0x00281052;

		/// <summary>(0028,1053) VR=DS VM=1 Rescale Slope</summary>
		DicomConstTags.RescaleSlope = 0x00281053;

		/// <summary>(0028,1054) VR=LO VM=1 Rescale Type</summary>
		DicomConstTags.RescaleType = 0x00281054;

		/// <summary>(0028,1055) VR=LO VM=1-n Window Center & Width Explanation</summary>
		DicomConstTags.WindowCenterWidthExplanation = 0x00281055;

		/// <summary>(0028,1056) VR=CS VM=1 VOI LUT Function</summary>
		DicomConstTags.VOILUTFunction = 0x00281056;

		/// <summary>(0028,1090) VR=CS VM=1 Recommended Viewing Mode</summary>
		DicomConstTags.RecommendedViewingMode = 0x00281090;

		/// <summary>(0028,1101) VR=US/SS VM=3 Red Palette Color Lookup Table Descriptor</summary>
		DicomConstTags.RedPaletteColorLookupTableDescriptor = 0x00281101;

		/// <summary>(0028,1102) VR=US/SS VM=3 Green Palette Color Lookup Table Descriptor</summary>
		DicomConstTags.GreenPaletteColorLookupTableDescriptor = 0x00281102;

		/// <summary>(0028,1103) VR=US/SS VM=3 Blue Palette Color Lookup Table Descriptor</summary>
		DicomConstTags.BluePaletteColorLookupTableDescriptor = 0x00281103;

		/// <summary>(0028,1199) VR=UI VM=1 Palette Color Lookup Table UID</summary>
		DicomConstTags.PaletteColorLookupTableUID = 0x00281199;

		/// <summary>(0028,1201) VR=OW VM=1 Red Palette Color Lookup Table Data</summary>
		DicomConstTags.RedPaletteColorLookupTableData = 0x00281201;

		/// <summary>(0028,1202) VR=OW VM=1 Green Palette Color Lookup Table Data</summary>
		DicomConstTags.GreenPaletteColorLookupTableData = 0x00281202;

		/// <summary>(0028,1203) VR=OW VM=1 Blue Palette Color Lookup Table Data</summary>
		DicomConstTags.BluePaletteColorLookupTableData = 0x00281203;

		/// <summary>(0028,1221) VR=OW VM=1 Segmented Red Palette Color Lookup Table Data</summary>
		DicomConstTags.SegmentedRedPaletteColorLookupTableData = 0x00281221;

		/// <summary>(0028,1222) VR=OW VM=1 Segmented Green Palette Color Lookup Table Data</summary>
		DicomConstTags.SegmentedGreenPaletteColorLookupTableData = 0x00281222;

		/// <summary>(0028,1223) VR=OW VM=1 Segmented Blue Palette Color Lookup Table Data</summary>
		DicomConstTags.SegmentedBluePaletteColorLookupTableData = 0x00281223;

		/// <summary>(0028,1300) VR=CS VM=1 Implant Present</summary>
		DicomConstTags.ImplantPresent = 0x00281300;

		/// <summary>(0028,1350) VR=CS VM=1 Partial View</summary>
		DicomConstTags.PartialView = 0x00281350;

		/// <summary>(0028,1351) VR=ST VM=1 Partial View Description</summary>
		DicomConstTags.PartialViewDescription = 0x00281351;

		/// <summary>(0028,1352) VR=SQ VM=1 Partial View Code Sequence</summary>
		DicomConstTags.PartialViewCodeSequence = 0x00281352;

		/// <summary>(0028,135a) VR=CS VM=1 Spatial Locations Preserved</summary>
		DicomConstTags.SpatialLocationsPreserved = 0x0028135a;

		/// <summary>(0028,2000) VR=OB VM=1 ICC Profile</summary>
		DicomConstTags.ICCProfile = 0x00282000;

		/// <summary>(0028,2110) VR=CS VM=1 Lossy Image Compression</summary>
		DicomConstTags.LossyImageCompression = 0x00282110;

		/// <summary>(0028,2112) VR=DS VM=1-n Lossy Image Compression Ratio</summary>
		DicomConstTags.LossyImageCompressionRatio = 0x00282112;

		/// <summary>(0028,2114) VR=CS VM=1-n Lossy Image Compression Method</summary>
		DicomConstTags.LossyImageCompressionMethod = 0x00282114;

		/// <summary>(0028,3000) VR=SQ VM=1 Modality LUT Sequence</summary>
		DicomConstTags.ModalityLUTSequence = 0x00283000;

		/// <summary>(0028,3002) VR=US/SS VM=3 LUT Descriptor</summary>
		DicomConstTags.LUTDescriptor = 0x00283002;

		/// <summary>(0028,3003) VR=LO VM=1 LUT Explanation</summary>
		DicomConstTags.LUTExplanation = 0x00283003;

		/// <summary>(0028,3004) VR=LO VM=1 Modality LUT Type</summary>
		DicomConstTags.ModalityLUTType = 0x00283004;

		/// <summary>(0028,3006) VR=US/SS/OW VM=1-n LUT Data</summary>
		DicomConstTags.LUTData = 0x00283006;

		/// <summary>(0028,3010) VR=SQ VM=1 VOI LUT Sequence</summary>
		DicomConstTags.VOILUTSequence = 0x00283010;

		/// <summary>(0028,3110) VR=SQ VM=1 Softcopy VOI LUT Sequence</summary>
		DicomConstTags.SoftcopyVOILUTSequence = 0x00283110;

		/// <summary>(0028,6010) VR=US VM=1 Representative Frame Number</summary>
		DicomConstTags.RepresentativeFrameNumber = 0x00286010;

		/// <summary>(0028,6020) VR=US VM=1-n Frame Numbers of Interest (FOI)</summary>
		DicomConstTags.FrameNumbersOfInterestFOI = 0x00286020;

		/// <summary>(0028,6022) VR=LO VM=1-n Frame(s) of Interest Description</summary>
		DicomConstTags.FramesOfInterestDescription = 0x00286022;

		/// <summary>(0028,6023) VR=CS VM=1-n Frame of Interest Type</summary>
		DicomConstTags.FrameOfInterestType = 0x00286023;

		/// <summary>(0028,6040) VR=US VM=1-n R Wave Pointer</summary>
		DicomConstTags.RWavePointer = 0x00286040;

		/// <summary>(0028,6100) VR=SQ VM=1 Mask Subtraction Sequence</summary>
		DicomConstTags.MaskSubtractionSequence = 0x00286100;

		/// <summary>(0028,6101) VR=CS VM=1 Mask Operation</summary>
		DicomConstTags.MaskOperation = 0x00286101;

		/// <summary>(0028,6102) VR=US VM=2-2n Applicable Frame Range</summary>
		DicomConstTags.ApplicableFrameRange = 0x00286102;

		/// <summary>(0028,6110) VR=US VM=1-n Mask Frame Numbers</summary>
		DicomConstTags.MaskFrameNumbers = 0x00286110;

		/// <summary>(0028,6112) VR=US VM=1 Contrast Frame Averaging</summary>
		DicomConstTags.ContrastFrameAveraging = 0x00286112;

		/// <summary>(0028,6114) VR=FL VM=2 Mask Sub-pixel Shift</summary>
		DicomConstTags.MaskSubpixelShift = 0x00286114;

		/// <summary>(0028,6120) VR=SS VM=1 TID Offset</summary>
		DicomConstTags.TIDOffset = 0x00286120;

		/// <summary>(0028,6190) VR=ST VM=1 Mask Operation Explanation</summary>
		DicomConstTags.MaskOperationExplanation = 0x00286190;

		/// <summary>(0028,7fe0) VR=UT VM=1 Pixel Data Provider URL</summary>
		DicomConstTags.PixelDataProviderURL = 0x00287fe0;

		/// <summary>(0028,9001) VR=UL VM=1 Data Point Rows</summary>
		DicomConstTags.DataPointRows = 0x00289001;

		/// <summary>(0028,9002) VR=UL VM=1 Data Point Columns</summary>
		DicomConstTags.DataPointColumns = 0x00289002;

		/// <summary>(0028,9003) VR=CS VM=1 Signal Domain Columns</summary>
		DicomConstTags.SignalDomainColumns = 0x00289003;

		/// <summary>(0028,9108) VR=CS VM=1 Data Representation</summary>
		DicomConstTags.DataRepresentation = 0x00289108;

		/// <summary>(0028,9110) VR=SQ VM=1 Pixel Measures Sequence</summary>
		DicomConstTags.PixelMeasuresSequence = 0x00289110;

		/// <summary>(0028,9132) VR=SQ VM=1 Frame VOI LUT Sequence</summary>
		DicomConstTags.FrameVOILUTSequence = 0x00289132;

		/// <summary>(0028,9145) VR=SQ VM=1 Pixel Value Transformation Sequence</summary>
		DicomConstTags.PixelValueTransformationSequence = 0x00289145;

		/// <summary>(0028,9235) VR=CS VM=1 Signal Domain Rows</summary>
		DicomConstTags.SignalDomainRows = 0x00289235;

		/// <summary>(0028,9411) VR=FL VM=1 Display Filter Percentage</summary>
		DicomConstTags.DisplayFilterPercentage = 0x00289411;

		/// <summary>(0028,9415) VR=SQ VM=1 Frame Pixel Shift Sequence</summary>
		DicomConstTags.FramePixelShiftSequence = 0x00289415;

		/// <summary>(0028,9416) VR=US VM=1 Subtraction Item ID</summary>
		DicomConstTags.SubtractionItemID = 0x00289416;

		/// <summary>(0028,9422) VR=SQ VM=1 Pixel Intensity Relationship LUT Sequence</summary>
		DicomConstTags.PixelIntensityRelationshipLUTSequence = 0x00289422;

		/// <summary>(0028,9443) VR=SQ VM=1 Frame Pixel Data Properties Sequence</summary>
		DicomConstTags.FramePixelDataPropertiesSequence = 0x00289443;

		/// <summary>(0028,9444) VR=CS VM=1 Geometrical Properties</summary>
		DicomConstTags.GeometricalProperties = 0x00289444;

		/// <summary>(0028,9445) VR=FL VM=1 Geometric Maximum Distortion</summary>
		DicomConstTags.GeometricMaximumDistortion = 0x00289445;

		/// <summary>(0028,9446) VR=CS VM=1-n Image Processing Applied</summary>
		DicomConstTags.ImageProcessingApplied = 0x00289446;

		/// <summary>(0028,9454) VR=CS VM=1 Mask Selection Mode</summary>
		DicomConstTags.MaskSelectionMode = 0x00289454;

		/// <summary>(0028,9474) VR=CS VM=1 LUT Function</summary>
		DicomConstTags.LUTFunction = 0x00289474;

		/// <summary>(0028,9520) VR=DS VM=16 Image to Equipment Mapping Matrix</summary>
		DicomConstTags.ImageToEquipmentMappingMatrix = 0x00289520;

		/// <summary>(0028,9537) VR=CS VM=1 Equipment Coordinate System Identification</summary>
		DicomConstTags.EquipmentCoordinateSystemIdentification = 0x00289537;

		/// <summary>(0032,1031) VR=SQ VM=1 Requesting Physician Identification Sequence</summary>
		DicomConstTags.RequestingPhysicianIdentificationSequence = 0x00321031;

		/// <summary>(0032,1032) VR=PN VM=1 Requesting Physician</summary>
		DicomConstTags.RequestingPhysician = 0x00321032;

		/// <summary>(0032,1033) VR=LO VM=1 Requesting Service</summary>
		DicomConstTags.RequestingService = 0x00321033;

		/// <summary>(0032,1060) VR=LO VM=1 Requested Procedure Description</summary>
		DicomConstTags.RequestedProcedureDescription = 0x00321060;

		/// <summary>(0032,1064) VR=SQ VM=1 Requested Procedure Code Sequence</summary>
		DicomConstTags.RequestedProcedureCodeSequence = 0x00321064;

		/// <summary>(0032,1070) VR=LO VM=1 Requested Contrast Agent</summary>
		DicomConstTags.RequestedContrastAgent = 0x00321070;

		/// <summary>(0038,0004) VR=SQ VM=1 Referenced Patient Alias Sequence</summary>
		DicomConstTags.ReferencedPatientAliasSequence = 0x00380004;

		/// <summary>(0038,0008) VR=CS VM=1 Visit Status ID</summary>
		DicomConstTags.VisitStatusID = 0x00380008;

		/// <summary>(0038,0010) VR=LO VM=1 Admission ID</summary>
		DicomConstTags.AdmissionID = 0x00380010;

		/// <summary>(0038,0011) VR=LO VM=1 Issuer of Admission ID</summary>
		DicomConstTags.IssuerOfAdmissionID = 0x00380011;

		/// <summary>(0038,0016) VR=LO VM=1 Route of Admissions</summary>
		DicomConstTags.RouteOfAdmissions = 0x00380016;

		/// <summary>(0038,0020) VR=DA VM=1 Admitting Date</summary>
		DicomConstTags.AdmittingDate = 0x00380020;

		/// <summary>(0038,0021) VR=TM VM=1 Admitting Time</summary>
		DicomConstTags.AdmittingTime = 0x00380021;

		/// <summary>(0038,0050) VR=LO VM=1 Special Needs</summary>
		DicomConstTags.SpecialNeeds = 0x00380050;

		/// <summary>(0038,0060) VR=LO VM=1 Service Episode ID</summary>
		DicomConstTags.ServiceEpisodeID = 0x00380060;

		/// <summary>(0038,0061) VR=LO VM=1 Issuer of Service Episode ID</summary>
		DicomConstTags.IssuerOfServiceEpisodeID = 0x00380061;

		/// <summary>(0038,0062) VR=LO VM=1 Service Episode Description</summary>
		DicomConstTags.ServiceEpisodeDescription = 0x00380062;

		/// <summary>(0038,0100) VR=SQ VM=1 Pertinent Documents Sequence</summary>
		DicomConstTags.PertinentDocumentsSequence = 0x00380100;

		/// <summary>(0038,0300) VR=LO VM=1 Current Patient Location</summary>
		DicomConstTags.CurrentPatientLocation = 0x00380300;

		/// <summary>(0038,0400) VR=LO VM=1 Patient's Institution Residence</summary>
		DicomConstTags.PatientsInstitutionResidence = 0x00380400;

		/// <summary>(0038,0500) VR=LO VM=1 Patient State</summary>
		DicomConstTags.PatientState = 0x00380500;

		/// <summary>(0038,0502) VR=SQ VM=1 Patient Clinical Trial Participation Sequence</summary>
		DicomConstTags.PatientClinicalTrialParticipationSequence = 0x00380502;

		/// <summary>(0038,4000) VR=LT VM=1 Visit Comments</summary>
		DicomConstTags.VisitComments = 0x00384000;

		/// <summary>(003a,0004) VR=CS VM=1 Waveform Originality</summary>
		DicomConstTags.WaveformOriginality = 0x003a0004;

		/// <summary>(003a,0005) VR=US VM=1 Number of Waveform Channels</summary>
		DicomConstTags.NumberOfWaveformChannels = 0x003a0005;

		/// <summary>(003a,0010) VR=UL VM=1 Number of Waveform Samples</summary>
		DicomConstTags.NumberOfWaveformSamples = 0x003a0010;

		/// <summary>(003a,001a) VR=DS VM=1 Sampling Frequency</summary>
		DicomConstTags.SamplingFrequency = 0x003a001a;

		/// <summary>(003a,0020) VR=SH VM=1 Multiplex Group Label</summary>
		DicomConstTags.MultiplexGroupLabel = 0x003a0020;

		/// <summary>(003a,0200) VR=SQ VM=1 Channel Definition Sequence</summary>
		DicomConstTags.ChannelDefinitionSequence = 0x003a0200;

		/// <summary>(003a,0202) VR=IS VM=1 Waveform Channel Number</summary>
		DicomConstTags.WaveformChannelNumber = 0x003a0202;

		/// <summary>(003a,0203) VR=SH VM=1 Channel Label</summary>
		DicomConstTags.ChannelLabel = 0x003a0203;

		/// <summary>(003a,0205) VR=CS VM=1-n Channel Status</summary>
		DicomConstTags.ChannelStatus = 0x003a0205;

		/// <summary>(003a,0208) VR=SQ VM=1 Channel Source Sequence</summary>
		DicomConstTags.ChannelSourceSequence = 0x003a0208;

		/// <summary>(003a,0209) VR=SQ VM=1 Channel Source Modifiers Sequence</summary>
		DicomConstTags.ChannelSourceModifiersSequence = 0x003a0209;

		/// <summary>(003a,020a) VR=SQ VM=1 Source Waveform Sequence</summary>
		DicomConstTags.SourceWaveformSequence = 0x003a020a;

		/// <summary>(003a,020c) VR=LO VM=1 Channel Derivation Description</summary>
		DicomConstTags.ChannelDerivationDescription = 0x003a020c;

		/// <summary>(003a,0210) VR=DS VM=1 Channel Sensitivity</summary>
		DicomConstTags.ChannelSensitivity = 0x003a0210;

		/// <summary>(003a,0211) VR=SQ VM=1 Channel Sensitivity Units Sequence</summary>
		DicomConstTags.ChannelSensitivityUnitsSequence = 0x003a0211;

		/// <summary>(003a,0212) VR=DS VM=1 Channel Sensitivity Correction Factor</summary>
		DicomConstTags.ChannelSensitivityCorrectionFactor = 0x003a0212;

		/// <summary>(003a,0213) VR=DS VM=1 Channel Baseline</summary>
		DicomConstTags.ChannelBaseline = 0x003a0213;

		/// <summary>(003a,0214) VR=DS VM=1 Channel Time Skew</summary>
		DicomConstTags.ChannelTimeSkew = 0x003a0214;

		/// <summary>(003a,0215) VR=DS VM=1 Channel Sample Skew</summary>
		DicomConstTags.ChannelSampleSkew = 0x003a0215;

		/// <summary>(003a,0218) VR=DS VM=1 Channel Offset</summary>
		DicomConstTags.ChannelOffset = 0x003a0218;

		/// <summary>(003a,021a) VR=US VM=1 Waveform Bits Stored</summary>
		DicomConstTags.WaveformBitsStored = 0x003a021a;

		/// <summary>(003a,0220) VR=DS VM=1 Filter Low Frequency</summary>
		DicomConstTags.FilterLowFrequency = 0x003a0220;

		/// <summary>(003a,0221) VR=DS VM=1 Filter High Frequency</summary>
		DicomConstTags.FilterHighFrequency = 0x003a0221;

		/// <summary>(003a,0222) VR=DS VM=1 Notch Filter Frequency</summary>
		DicomConstTags.NotchFilterFrequency = 0x003a0222;

		/// <summary>(003a,0223) VR=DS VM=1 Notch Filter Bandwidth</summary>
		DicomConstTags.NotchFilterBandwidth = 0x003a0223;

		/// <summary>(003a,0230) VR=FL VM=1 Waveform Data Display Scale</summary>
		DicomConstTags.WaveformDataDisplayScale = 0x003a0230;

		/// <summary>(003a,0231) VR=US VM=3 Waveform Display Background CIELab Value</summary>
		DicomConstTags.WaveformDisplayBackgroundCIELabValue = 0x003a0231;

		/// <summary>(003a,0240) VR=SQ VM=1 Waveform Presentation Group Sequence</summary>
		DicomConstTags.WaveformPresentationGroupSequence = 0x003a0240;

		/// <summary>(003a,0241) VR=US VM=1 Presentation Group Number</summary>
		DicomConstTags.PresentationGroupNumber = 0x003a0241;

		/// <summary>(003a,0242) VR=SQ VM=1 Channel Display Sequence</summary>
		DicomConstTags.ChannelDisplaySequence = 0x003a0242;

		/// <summary>(003a,0244) VR=US VM=3 Channel Recommended Display CIELab Value</summary>
		DicomConstTags.ChannelRecommendedDisplayCIELabValue = 0x003a0244;

		/// <summary>(003a,0245) VR=FL VM=1 Channel Position</summary>
		DicomConstTags.ChannelPosition = 0x003a0245;

		/// <summary>(003a,0246) VR=CS VM=1 Display Shading Flag</summary>
		DicomConstTags.DisplayShadingFlag = 0x003a0246;

		/// <summary>(003a,0247) VR=FL VM=1 Fractional Channel Display Scale</summary>
		DicomConstTags.FractionalChannelDisplayScale = 0x003a0247;

		/// <summary>(003a,0248) VR=FL VM=1 Absolute Channel Display Scale</summary>
		DicomConstTags.AbsoluteChannelDisplayScale = 0x003a0248;

		/// <summary>(003a,0300) VR=SQ VM=1 Multiplexed Audio Channels Description Code Sequence</summary>
		DicomConstTags.MultiplexedAudioChannelsDescriptionCodeSequence = 0x003a0300;

		/// <summary>(003a,0301) VR=IS VM=1 Channel Identification Code</summary>
		DicomConstTags.ChannelIdentificationCode = 0x003a0301;

		/// <summary>(003a,0302) VR=CS VM=1 Channel Mode</summary>
		DicomConstTags.ChannelMode = 0x003a0302;

		/// <summary>(0040,0001) VR=AE VM=1-n Scheduled Station AE Title</summary>
		DicomConstTags.ScheduledStationAETitle = 0x00400001;

		/// <summary>(0040,0002) VR=DA VM=1 Scheduled Procedure Step Start Date</summary>
		DicomConstTags.ScheduledProcedureStepStartDate = 0x00400002;

		/// <summary>(0040,0003) VR=TM VM=1 Scheduled Procedure Step Start Time</summary>
		DicomConstTags.ScheduledProcedureStepStartTime = 0x00400003;

		/// <summary>(0040,0004) VR=DA VM=1 Scheduled Procedure Step End Date</summary>
		DicomConstTags.ScheduledProcedureStepEndDate = 0x00400004;

		/// <summary>(0040,0005) VR=TM VM=1 Scheduled Procedure Step End Time</summary>
		DicomConstTags.ScheduledProcedureStepEndTime = 0x00400005;

		/// <summary>(0040,0006) VR=PN VM=1 Scheduled Performing Physician's Name</summary>
		DicomConstTags.ScheduledPerformingPhysiciansName = 0x00400006;

		/// <summary>(0040,0007) VR=LO VM=1 Scheduled Procedure Step Description</summary>
		DicomConstTags.ScheduledProcedureStepDescription = 0x00400007;

		/// <summary>(0040,0008) VR=SQ VM=1 Scheduled Protocol Code Sequence</summary>
		DicomConstTags.ScheduledProtocolCodeSequence = 0x00400008;

		/// <summary>(0040,0009) VR=SH VM=1 Scheduled Procedure Step ID</summary>
		DicomConstTags.ScheduledProcedureStepID = 0x00400009;

		/// <summary>(0040,000a) VR=SQ VM=1 Stage Code Sequence</summary>
		DicomConstTags.StageCodeSequence = 0x0040000a;

		/// <summary>(0040,000b) VR=SQ VM=1 Scheduled Performing Physician Identification Sequence</summary>
		DicomConstTags.ScheduledPerformingPhysicianIdentificationSequence = 0x0040000b;

		/// <summary>(0040,0010) VR=SH VM=1-n Scheduled Station Name</summary>
		DicomConstTags.ScheduledStationName = 0x00400010;

		/// <summary>(0040,0011) VR=SH VM=1 Scheduled Procedure Step Location</summary>
		DicomConstTags.ScheduledProcedureStepLocation = 0x00400011;

		/// <summary>(0040,0012) VR=LO VM=1 Pre-Medication</summary>
		DicomConstTags.PreMedication = 0x00400012;

		/// <summary>(0040,0020) VR=CS VM=1 Scheduled Procedure Step Status</summary>
		DicomConstTags.ScheduledProcedureStepStatus = 0x00400020;

		/// <summary>(0040,0100) VR=SQ VM=1 Scheduled Procedure Step Sequence</summary>
		DicomConstTags.ScheduledProcedureStepSequence = 0x00400100;

		/// <summary>(0040,0220) VR=SQ VM=1 Referenced Non-Image Composite SOP Instance Sequence</summary>
		DicomConstTags.ReferencedNonImageCompositeSOPInstanceSequence = 0x00400220;

		/// <summary>(0040,0241) VR=AE VM=1 Performed Station AE Title</summary>
		DicomConstTags.PerformedStationAETitle = 0x00400241;

		/// <summary>(0040,0242) VR=SH VM=1 Performed Station Name</summary>
		DicomConstTags.PerformedStationName = 0x00400242;

		/// <summary>(0040,0243) VR=SH VM=1 Performed Location</summary>
		DicomConstTags.PerformedLocation = 0x00400243;

		/// <summary>(0040,0244) VR=DA VM=1 Performed Procedure Step Start Date</summary>
		DicomConstTags.PerformedProcedureStepStartDate = 0x00400244;

		/// <summary>(0040,0245) VR=TM VM=1 Performed Procedure Step Start Time</summary>
		DicomConstTags.PerformedProcedureStepStartTime = 0x00400245;

		/// <summary>(0040,0250) VR=DA VM=1 Performed Procedure Step End Date</summary>
		DicomConstTags.PerformedProcedureStepEndDate = 0x00400250;

		/// <summary>(0040,0251) VR=TM VM=1 Performed Procedure Step End Time</summary>
		DicomConstTags.PerformedProcedureStepEndTime = 0x00400251;

		/// <summary>(0040,0252) VR=CS VM=1 Performed Procedure Step Status</summary>
		DicomConstTags.PerformedProcedureStepStatus = 0x00400252;

		/// <summary>(0040,0253) VR=SH VM=1 Performed Procedure Step ID</summary>
		DicomConstTags.PerformedProcedureStepID = 0x00400253;

		/// <summary>(0040,0254) VR=LO VM=1 Performed Procedure Step Description</summary>
		DicomConstTags.PerformedProcedureStepDescription = 0x00400254;

		/// <summary>(0040,0255) VR=LO VM=1 Performed Procedure Type Description</summary>
		DicomConstTags.PerformedProcedureTypeDescription = 0x00400255;

		/// <summary>(0040,0260) VR=SQ VM=1 Performed Protocol Code Sequence</summary>
		DicomConstTags.PerformedProtocolCodeSequence = 0x00400260;

		/// <summary>(0040,0270) VR=SQ VM=1 Scheduled Step Attributes Sequence</summary>
		DicomConstTags.ScheduledStepAttributesSequence = 0x00400270;

		/// <summary>(0040,0275) VR=SQ VM=1 Request Attributes Sequence</summary>
		DicomConstTags.RequestAttributesSequence = 0x00400275;

		/// <summary>(0040,0280) VR=ST VM=1 Comments on the Performed Procedure Step</summary>
		DicomConstTags.CommentsOnThePerformedProcedureStep = 0x00400280;

		/// <summary>(0040,0281) VR=SQ VM=1 Performed Procedure Step Discontinuation Reason Code Sequence</summary>
		DicomConstTags.PerformedProcedureStepDiscontinuationReasonCodeSequence = 0x00400281;

		/// <summary>(0040,0293) VR=SQ VM=1 Quantity Sequence</summary>
		DicomConstTags.QuantitySequence = 0x00400293;

		/// <summary>(0040,0294) VR=DS VM=1 Quantity</summary>
		DicomConstTags.Quantity = 0x00400294;

		/// <summary>(0040,0295) VR=SQ VM=1 Measuring Units Sequence</summary>
		DicomConstTags.MeasuringUnitsSequence = 0x00400295;

		/// <summary>(0040,0296) VR=SQ VM=1 Billing Item Sequence</summary>
		DicomConstTags.BillingItemSequence = 0x00400296;

		/// <summary>(0040,0300) VR=US VM=1 Total Time of Fluoroscopy</summary>
		DicomConstTags.TotalTimeOfFluoroscopy = 0x00400300;

		/// <summary>(0040,0301) VR=US VM=1 Total Number of Exposures</summary>
		DicomConstTags.TotalNumberOfExposures = 0x00400301;

		/// <summary>(0040,0302) VR=US VM=1 Entrance Dose</summary>
		DicomConstTags.EntranceDose = 0x00400302;

		/// <summary>(0040,0303) VR=US VM=1-2 Exposed Area</summary>
		DicomConstTags.ExposedArea = 0x00400303;

		/// <summary>(0040,0306) VR=DS VM=1 Distance Source to Entrance</summary>
		DicomConstTags.DistanceSourceToEntrance = 0x00400306;

		/// <summary>(0040,030e) VR=SQ VM=1 Exposure Dose Sequence</summary>
		DicomConstTags.ExposureDoseSequence = 0x0040030e;

		/// <summary>(0040,0310) VR=ST VM=1 Comments on Radiation Dose</summary>
		DicomConstTags.CommentsOnRadiationDose = 0x00400310;

		/// <summary>(0040,0312) VR=DS VM=1 X-Ray Output</summary>
		DicomConstTags.XRayOutput = 0x00400312;

		/// <summary>(0040,0314) VR=DS VM=1 Half Value Layer</summary>
		DicomConstTags.HalfValueLayer = 0x00400314;

		/// <summary>(0040,0316) VR=DS VM=1 Organ Dose</summary>
		DicomConstTags.OrganDose = 0x00400316;

		/// <summary>(0040,0318) VR=CS VM=1 Organ Exposed</summary>
		DicomConstTags.OrganExposed = 0x00400318;

		/// <summary>(0040,0320) VR=SQ VM=1 Billing Procedure Step Sequence</summary>
		DicomConstTags.BillingProcedureStepSequence = 0x00400320;

		/// <summary>(0040,0321) VR=SQ VM=1 Film Consumption Sequence</summary>
		DicomConstTags.FilmConsumptionSequence = 0x00400321;

		/// <summary>(0040,0324) VR=SQ VM=1 Billing Supplies and Devices Sequence</summary>
		DicomConstTags.BillingSuppliesAndDevicesSequence = 0x00400324;

		/// <summary>(0040,0340) VR=SQ VM=1 Performed Series Sequence</summary>
		DicomConstTags.PerformedSeriesSequence = 0x00400340;

		/// <summary>(0040,0400) VR=LT VM=1 Comments on the Scheduled Procedure Step</summary>
		DicomConstTags.CommentsOnTheScheduledProcedureStep = 0x00400400;

		/// <summary>(0040,0440) VR=SQ VM=1 Protocol Context Sequence</summary>
		DicomConstTags.ProtocolContextSequence = 0x00400440;

		/// <summary>(0040,0441) VR=SQ VM=1 Content Item Modifier Sequence</summary>
		DicomConstTags.ContentItemModifierSequence = 0x00400441;

		/// <summary>(0040,050a) VR=LO VM=1 Specimen Accession Number</summary>
		DicomConstTags.SpecimenAccessionNumber = 0x0040050a;

		/// <summary>(0040,0550) VR=SQ VM=1 Specimen Sequence</summary>
		DicomConstTags.SpecimenSequence = 0x00400550;

		/// <summary>(0040,0551) VR=LO VM=1 Specimen Identifier</summary>
		DicomConstTags.SpecimenIdentifier = 0x00400551;

		/// <summary>(0040,0555) VR=SQ VM=1 Acquisition Context Sequence</summary>
		DicomConstTags.AcquisitionContextSequence = 0x00400555;

		/// <summary>(0040,0556) VR=ST VM=1 Acquisition Context Description</summary>
		DicomConstTags.AcquisitionContextDescription = 0x00400556;

		/// <summary>(0040,059a) VR=SQ VM=1 Specimen Type Code Sequence</summary>
		DicomConstTags.SpecimenTypeCodeSequence = 0x0040059a;

		/// <summary>(0040,06fa) VR=LO VM=1 Slide Identifier</summary>
		DicomConstTags.SlideIdentifier = 0x004006fa;

		/// <summary>(0040,071a) VR=SQ VM=1 Image Center Point Coordinates Sequence</summary>
		DicomConstTags.ImageCenterPointCoordinatesSequence = 0x0040071a;

		/// <summary>(0040,072a) VR=DS VM=1 X offset in Slide Coordinate System</summary>
		DicomConstTags.XOffsetInSlideCoordinateSystem = 0x0040072a;

		/// <summary>(0040,073a) VR=DS VM=1 Y offset in Slide Coordinate System</summary>
		DicomConstTags.YOffsetInSlideCoordinateSystem = 0x0040073a;

		/// <summary>(0040,074a) VR=DS VM=1 Z offset in Slide Coordinate System</summary>
		DicomConstTags.ZOffsetInSlideCoordinateSystem = 0x0040074a;

		/// <summary>(0040,08d8) VR=SQ VM=1 Pixel Spacing Sequence</summary>
		DicomConstTags.PixelSpacingSequence = 0x004008d8;

		/// <summary>(0040,08da) VR=SQ VM=1 Coordinate System Axis Code Sequence</summary>
		DicomConstTags.CoordinateSystemAxisCodeSequence = 0x004008da;

		/// <summary>(0040,08ea) VR=SQ VM=1 Measurement Units Code Sequence</summary>
		DicomConstTags.MeasurementUnitsCodeSequence = 0x004008ea;

		/// <summary>(0040,1001) VR=SH VM=1 Requested Procedure ID</summary>
		DicomConstTags.RequestedProcedureID = 0x00401001;

		/// <summary>(0040,1002) VR=LO VM=1 Reason for the Requested Procedure</summary>
		DicomConstTags.ReasonForTheRequestedProcedure = 0x00401002;

		/// <summary>(0040,1003) VR=SH VM=1 Requested Procedure Priority</summary>
		DicomConstTags.RequestedProcedurePriority = 0x00401003;

		/// <summary>(0040,1004) VR=LO VM=1 Patient Transport Arrangements</summary>
		DicomConstTags.PatientTransportArrangements = 0x00401004;

		/// <summary>(0040,1005) VR=LO VM=1 Requested Procedure Location</summary>
		DicomConstTags.RequestedProcedureLocation = 0x00401005;

		/// <summary>(0040,1008) VR=LO VM=1 Confidentiality Code</summary>
		DicomConstTags.ConfidentialityCode = 0x00401008;

		/// <summary>(0040,1009) VR=SH VM=1 Reporting Priority</summary>
		DicomConstTags.ReportingPriority = 0x00401009;

		/// <summary>(0040,100a) VR=SQ VM=1 Reason for Requested Procedure Code Sequence</summary>
		DicomConstTags.ReasonForRequestedProcedureCodeSequence = 0x0040100a;

		/// <summary>(0040,1010) VR=PN VM=1-n Names of Intended Recipients of Results</summary>
		DicomConstTags.NamesOfIntendedRecipientsOfResults = 0x00401010;

		/// <summary>(0040,1011) VR=SQ VM=1 Intended Recipients of Results Identification Sequence</summary>
		DicomConstTags.IntendedRecipientsOfResultsIdentificationSequence = 0x00401011;

		/// <summary>(0040,1101) VR=SQ VM=1 Person Identification Code Sequence</summary>
		DicomConstTags.PersonIdentificationCodeSequence = 0x00401101;

		/// <summary>(0040,1102) VR=ST VM=1 Person's Address</summary>
		DicomConstTags.PersonsAddress = 0x00401102;

		/// <summary>(0040,1103) VR=LO VM=1-n Person's Telephone Numbers</summary>
		DicomConstTags.PersonsTelephoneNumbers = 0x00401103;

		/// <summary>(0040,1400) VR=LT VM=1 Requested Procedure Comments</summary>
		DicomConstTags.RequestedProcedureComments = 0x00401400;

		/// <summary>(0040,2004) VR=DA VM=1 Issue Date of Imaging Service Request</summary>
		DicomConstTags.IssueDateOfImagingServiceRequest = 0x00402004;

		/// <summary>(0040,2005) VR=TM VM=1 Issue Time of Imaging Service Request</summary>
		DicomConstTags.IssueTimeOfImagingServiceRequest = 0x00402005;

		/// <summary>(0040,2008) VR=PN VM=1 Order Entered By</summary>
		DicomConstTags.OrderEnteredBy = 0x00402008;

		/// <summary>(0040,2009) VR=SH VM=1 Order Enterer's Location</summary>
		DicomConstTags.OrderEnterersLocation = 0x00402009;

		/// <summary>(0040,2010) VR=SH VM=1 Order Callback Phone Number</summary>
		DicomConstTags.OrderCallbackPhoneNumber = 0x00402010;

		/// <summary>(0040,2016) VR=LO VM=1 Placer Order Number / Imaging Service Request</summary>
		DicomConstTags.PlacerOrderNumberImagingServiceRequest = 0x00402016;

		/// <summary>(0040,2017) VR=LO VM=1 Filler Order Number / Imaging Service Request</summary>
		DicomConstTags.FillerOrderNumberImagingServiceRequest = 0x00402017;

		/// <summary>(0040,2400) VR=LT VM=1 Imaging Service Request Comments</summary>
		DicomConstTags.ImagingServiceRequestComments = 0x00402400;

		/// <summary>(0040,3001) VR=LO VM=1 Confidentiality Constraint on Patient Data Description</summary>
		DicomConstTags.ConfidentialityConstraintOnPatientDataDescription = 0x00403001;

		/// <summary>(0040,4001) VR=CS VM=1 General Purpose Scheduled Procedure Step Status</summary>
		DicomConstTags.GeneralPurposeScheduledProcedureStepStatus = 0x00404001;

		/// <summary>(0040,4002) VR=CS VM=1 General Purpose Performed Procedure Step Status</summary>
		DicomConstTags.GeneralPurposePerformedProcedureStepStatus = 0x00404002;

		/// <summary>(0040,4003) VR=CS VM=1 General Purpose Scheduled Procedure Step Priority</summary>
		DicomConstTags.GeneralPurposeScheduledProcedureStepPriority = 0x00404003;

		/// <summary>(0040,4004) VR=SQ VM=1 Scheduled Processing Applications Code Sequence</summary>
		DicomConstTags.ScheduledProcessingApplicationsCodeSequence = 0x00404004;

		/// <summary>(0040,4005) VR=DT VM=1 Scheduled Procedure Step Start Date and Time</summary>
		DicomConstTags.ScheduledProcedureStepStartDateAndTime = 0x00404005;

		/// <summary>(0040,4006) VR=CS VM=1 Multiple Copies Flag</summary>
		DicomConstTags.MultipleCopiesFlag = 0x00404006;

		/// <summary>(0040,4007) VR=SQ VM=1 Performed Processing Applications Code Sequence</summary>
		DicomConstTags.PerformedProcessingApplicationsCodeSequence = 0x00404007;

		/// <summary>(0040,4009) VR=SQ VM=1 Human Performer Code Sequence</summary>
		DicomConstTags.HumanPerformerCodeSequence = 0x00404009;

		/// <summary>(0040,4010) VR=DT VM=1 Scheduled Procedure Step Modification Date and Time</summary>
		DicomConstTags.ScheduledProcedureStepModificationDateAndTime = 0x00404010;

		/// <summary>(0040,4011) VR=DT VM=1 Expected Completion Date and Time</summary>
		DicomConstTags.ExpectedCompletionDateAndTime = 0x00404011;

		/// <summary>(0040,4015) VR=SQ VM=1 Resulting General Purpose Performed Procedure Steps Sequence</summary>
		DicomConstTags.ResultingGeneralPurposePerformedProcedureStepsSequence = 0x00404015;

		/// <summary>(0040,4016) VR=SQ VM=1 Referenced General Purpose Scheduled Procedure Step Sequence</summary>
		DicomConstTags.ReferencedGeneralPurposeScheduledProcedureStepSequence = 0x00404016;

		/// <summary>(0040,4018) VR=SQ VM=1 Scheduled Workitem Code Sequence</summary>
		DicomConstTags.ScheduledWorkitemCodeSequence = 0x00404018;

		/// <summary>(0040,4019) VR=SQ VM=1 Performed Workitem Code Sequence</summary>
		DicomConstTags.PerformedWorkitemCodeSequence = 0x00404019;

		/// <summary>(0040,4020) VR=CS VM=1 Input Availability Flag</summary>
		DicomConstTags.InputAvailabilityFlag = 0x00404020;

		/// <summary>(0040,4021) VR=SQ VM=1 Input Information Sequence</summary>
		DicomConstTags.InputInformationSequence = 0x00404021;

		/// <summary>(0040,4022) VR=SQ VM=1 Relevant Information Sequence</summary>
		DicomConstTags.RelevantInformationSequence = 0x00404022;

		/// <summary>(0040,4023) VR=UI VM=1 Referenced General Purpose Scheduled Procedure Step Transaction UID</summary>
		DicomConstTags.ReferencedGeneralPurposeScheduledProcedureStepTransactionUID = 0x00404023;

		/// <summary>(0040,4025) VR=SQ VM=1 Scheduled Station Name Code Sequence</summary>
		DicomConstTags.ScheduledStationNameCodeSequence = 0x00404025;

		/// <summary>(0040,4026) VR=SQ VM=1 Scheduled Station Class Code Sequence</summary>
		DicomConstTags.ScheduledStationClassCodeSequence = 0x00404026;

		/// <summary>(0040,4027) VR=SQ VM=1 Scheduled Station Geographic Location Code Sequence</summary>
		DicomConstTags.ScheduledStationGeographicLocationCodeSequence = 0x00404027;

		/// <summary>(0040,4028) VR=SQ VM=1 Performed Station Name Code Sequence</summary>
		DicomConstTags.PerformedStationNameCodeSequence = 0x00404028;

		/// <summary>(0040,4029) VR=SQ VM=1 Performed Station Class Code Sequence</summary>
		DicomConstTags.PerformedStationClassCodeSequence = 0x00404029;

		/// <summary>(0040,4030) VR=SQ VM=1 Performed Station Geographic Location Code Sequence</summary>
		DicomConstTags.PerformedStationGeographicLocationCodeSequence = 0x00404030;

		/// <summary>(0040,4031) VR=SQ VM=1 Requested Subsequent Workitem Code Sequence</summary>
		DicomConstTags.RequestedSubsequentWorkitemCodeSequence = 0x00404031;

		/// <summary>(0040,4032) VR=SQ VM=1 Non-DICOM Output Code Sequence</summary>
		DicomConstTags.NonDICOMOutputCodeSequence = 0x00404032;

		/// <summary>(0040,4033) VR=SQ VM=1 Output Information Sequence</summary>
		DicomConstTags.OutputInformationSequence = 0x00404033;

		/// <summary>(0040,4034) VR=SQ VM=1 Scheduled Human Performers Sequence</summary>
		DicomConstTags.ScheduledHumanPerformersSequence = 0x00404034;

		/// <summary>(0040,4035) VR=SQ VM=1 Actual Human Performers Sequence</summary>
		DicomConstTags.ActualHumanPerformersSequence = 0x00404035;

		/// <summary>(0040,4036) VR=LO VM=1 Human Performer's Organization</summary>
		DicomConstTags.HumanPerformersOrganization = 0x00404036;

		/// <summary>(0040,4037) VR=PN VM=1 Human Performer's Name</summary>
		DicomConstTags.HumanPerformersName = 0x00404037;

		/// <summary>(0040,8302) VR=DS VM=1 Entrance Dose in mGy</summary>
		DicomConstTags.EntranceDoseInMGy = 0x00408302;

		/// <summary>(0040,9094) VR=SQ VM=1 Referenced Image Real World Value Mapping Sequence</summary>
		DicomConstTags.ReferencedImageRealWorldValueMappingSequence = 0x00409094;

		/// <summary>(0040,9096) VR=SQ VM=1 Real World Value Mapping Sequence</summary>
		DicomConstTags.RealWorldValueMappingSequence = 0x00409096;

		/// <summary>(0040,9098) VR=SQ VM=1 Pixel Value Mapping Code Sequence</summary>
		DicomConstTags.PixelValueMappingCodeSequence = 0x00409098;

		/// <summary>(0040,9210) VR=SH VM=1 LUT Label</summary>
		DicomConstTags.LUTLabel = 0x00409210;

		/// <summary>(0040,9211) VR=US/SS VM=1 Real World Value Last Value Mapped</summary>
		DicomConstTags.RealWorldValueLastValueMapped = 0x00409211;

		/// <summary>(0040,9212) VR=FD VM=1-n Real World Value LUT Data</summary>
		DicomConstTags.RealWorldValueLUTData = 0x00409212;

		/// <summary>(0040,9216) VR=US/SS VM=1 Real World Value First Value Mapped</summary>
		DicomConstTags.RealWorldValueFirstValueMapped = 0x00409216;

		/// <summary>(0040,9224) VR=FD VM=1 Real World Value Intercept</summary>
		DicomConstTags.RealWorldValueIntercept = 0x00409224;

		/// <summary>(0040,9225) VR=FD VM=1 Real World Value Slope</summary>
		DicomConstTags.RealWorldValueSlope = 0x00409225;

		/// <summary>(0040,a010) VR=CS VM=1 Relationship Type</summary>
		DicomConstTags.RelationshipType = 0x0040a010;

		/// <summary>(0040,a027) VR=LO VM=1 Verifying Organization</summary>
		DicomConstTags.VerifyingOrganization = 0x0040a027;

		/// <summary>(0040,a030) VR=DT VM=1 Verification Date Time</summary>
		DicomConstTags.VerificationDateTime = 0x0040a030;

		/// <summary>(0040,a032) VR=DT VM=1 Observation Date Time</summary>
		DicomConstTags.ObservationDateTime = 0x0040a032;

		/// <summary>(0040,a040) VR=CS VM=1 Value Type</summary>
		DicomConstTags.ValueType = 0x0040a040;

		/// <summary>(0040,a043) VR=SQ VM=1 Concept Name Code Sequence</summary>
		DicomConstTags.ConceptNameCodeSequence = 0x0040a043;

		/// <summary>(0040,a050) VR=CS VM=1 Continuity Of Content</summary>
		DicomConstTags.ContinuityOfContent = 0x0040a050;

		/// <summary>(0040,a073) VR=SQ VM=1 Verifying Observer Sequence</summary>
		DicomConstTags.VerifyingObserverSequence = 0x0040a073;

		/// <summary>(0040,a075) VR=PN VM=1 Verifying Observer Name</summary>
		DicomConstTags.VerifyingObserverName = 0x0040a075;

		/// <summary>(0040,a078) VR=SQ VM=1 Author Observer Sequence</summary>
		DicomConstTags.AuthorObserverSequence = 0x0040a078;

		/// <summary>(0040,a07a) VR=SQ VM=1 Participant Sequence</summary>
		DicomConstTags.ParticipantSequence = 0x0040a07a;

		/// <summary>(0040,a07c) VR=SQ VM=1 Custodial Organization Sequence</summary>
		DicomConstTags.CustodialOrganizationSequence = 0x0040a07c;

		/// <summary>(0040,a080) VR=CS VM=1 Participation Type</summary>
		DicomConstTags.ParticipationType = 0x0040a080;

		/// <summary>(0040,a082) VR=DT VM=1 Participation DateTime</summary>
		DicomConstTags.ParticipationDateTime = 0x0040a082;

		/// <summary>(0040,a084) VR=CS VM=1 Observer Type</summary>
		DicomConstTags.ObserverType = 0x0040a084;

		/// <summary>(0040,a088) VR=SQ VM=1 Verifying Observer Identification Code Sequence</summary>
		DicomConstTags.VerifyingObserverIdentificationCodeSequence = 0x0040a088;

		/// <summary>(0040,a0b0) VR=US VM=2-2n Referenced Waveform Channels</summary>
		DicomConstTags.ReferencedWaveformChannels = 0x0040a0b0;

		/// <summary>(0040,a120) VR=DT VM=1 DateTime</summary>
		DicomConstTags.DateTime = 0x0040a120;

		/// <summary>(0040,a121) VR=DA VM=1 Date</summary>
		DicomConstTags.Date = 0x0040a121;

		/// <summary>(0040,a122) VR=TM VM=1 Time</summary>
		DicomConstTags.Time = 0x0040a122;

		/// <summary>(0040,a123) VR=PN VM=1 Person Name</summary>
		DicomConstTags.PersonName = 0x0040a123;

		/// <summary>(0040,a124) VR=UI VM=1 UID</summary>
		DicomConstTags.UID = 0x0040a124;

		/// <summary>(0040,a130) VR=CS VM=1 Temporal Range Type</summary>
		DicomConstTags.TemporalRangeType = 0x0040a130;

		/// <summary>(0040,a132) VR=UL VM=1-n Referenced Sample Positions</summary>
		DicomConstTags.ReferencedSamplePositions = 0x0040a132;

		/// <summary>(0040,a136) VR=US VM=1-n Referenced Frame Numbers</summary>
		DicomConstTags.ReferencedFrameNumbers = 0x0040a136;

		/// <summary>(0040,a138) VR=DS VM=1-n Referenced Time Offsets</summary>
		DicomConstTags.ReferencedTimeOffsets = 0x0040a138;

		/// <summary>(0040,a13a) VR=DT VM=1-n Referenced DateTime</summary>
		DicomConstTags.ReferencedDateTime = 0x0040a13a;

		/// <summary>(0040,a160) VR=UT VM=1 Text Value</summary>
		DicomConstTags.TextValue = 0x0040a160;

		/// <summary>(0040,a168) VR=SQ VM=1 Concept Code Sequence</summary>
		DicomConstTags.ConceptCodeSequence = 0x0040a168;

		/// <summary>(0040,a170) VR=SQ VM=1 Purpose of Reference Code Sequence</summary>
		DicomConstTags.PurposeOfReferenceCodeSequence = 0x0040a170;

		/// <summary>(0040,a180) VR=US VM=1 Annotation Group Number</summary>
		DicomConstTags.AnnotationGroupNumber = 0x0040a180;

		/// <summary>(0040,a195) VR=SQ VM=1 Modifier Code Sequence</summary>
		DicomConstTags.ModifierCodeSequence = 0x0040a195;

		/// <summary>(0040,a300) VR=SQ VM=1 Measured Value Sequence</summary>
		DicomConstTags.MeasuredValueSequence = 0x0040a300;

		/// <summary>(0040,a301) VR=SQ VM=1 Numeric Value Qualifier Code Sequence</summary>
		DicomConstTags.NumericValueQualifierCodeSequence = 0x0040a301;

		/// <summary>(0040,a30a) VR=DS VM=1-n Numeric Value</summary>
		DicomConstTags.NumericValue = 0x0040a30a;

		/// <summary>(0040,a360) VR=SQ VM=1 Predecessor Documents Sequence</summary>
		DicomConstTags.PredecessorDocumentsSequence = 0x0040a360;

		/// <summary>(0040,a370) VR=SQ VM=1 Referenced Request Sequence</summary>
		DicomConstTags.ReferencedRequestSequence = 0x0040a370;

		/// <summary>(0040,a372) VR=SQ VM=1 Performed Procedure Code Sequence</summary>
		DicomConstTags.PerformedProcedureCodeSequence = 0x0040a372;

		/// <summary>(0040,a375) VR=SQ VM=1 Current Requested Procedure Evidence Sequence</summary>
		DicomConstTags.CurrentRequestedProcedureEvidenceSequence = 0x0040a375;

		/// <summary>(0040,a385) VR=SQ VM=1 Pertinent Other Evidence Sequence</summary>
		DicomConstTags.PertinentOtherEvidenceSequence = 0x0040a385;

		/// <summary>(0040,a390) VR=SQ VM=1 HL7 Structured Document Reference Sequence</summary>
		DicomConstTags.HL7StructuredDocumentReferenceSequence = 0x0040a390;

		/// <summary>(0040,a491) VR=CS VM=1 Completion Flag</summary>
		DicomConstTags.CompletionFlag = 0x0040a491;

		/// <summary>(0040,a492) VR=LO VM=1 Completion Flag Description</summary>
		DicomConstTags.CompletionFlagDescription = 0x0040a492;

		/// <summary>(0040,a493) VR=CS VM=1 Verification Flag</summary>
		DicomConstTags.VerificationFlag = 0x0040a493;

		/// <summary>(0040,a494) VR=CS VM=1 Archive Requested</summary>
		DicomConstTags.ArchiveRequested = 0x0040a494;

		/// <summary>(0040,a504) VR=SQ VM=1 Content Template Sequence</summary>
		DicomConstTags.ContentTemplateSequence = 0x0040a504;

		/// <summary>(0040,a525) VR=SQ VM=1 Identical Documents Sequence</summary>
		DicomConstTags.IdenticalDocumentsSequence = 0x0040a525;

		/// <summary>(0040,a730) VR=SQ VM=1 Content Sequence</summary>
		DicomConstTags.ContentSequence = 0x0040a730;

		/// <summary>(0040,b020) VR=SQ VM=1 Annotation Sequence</summary>
		DicomConstTags.AnnotationSequence = 0x0040b020;

		/// <summary>(0040,db00) VR=CS VM=1 Template Identifier</summary>
		DicomConstTags.TemplateIdentifier = 0x0040db00;

		/// <summary>(0040,db73) VR=UL VM=1-n Referenced Content Item Identifier</summary>
		DicomConstTags.ReferencedContentItemIdentifier = 0x0040db73;

		/// <summary>(0040,e001) VR=ST VM=1 HL7 Instance Identifier</summary>
		DicomConstTags.HL7InstanceIdentifier = 0x0040e001;

		/// <summary>(0040,e004) VR=DT VM=1 HL7 Document Effective Time</summary>
		DicomConstTags.HL7DocumentEffectiveTime = 0x0040e004;

		/// <summary>(0040,e006) VR=SQ VM=1 HL7 Document Type Code Sequence</summary>
		DicomConstTags.HL7DocumentTypeCodeSequence = 0x0040e006;

		/// <summary>(0040,e010) VR=UT VM=1 Retrieve URI</summary>
		DicomConstTags.RetrieveURI = 0x0040e010;

		/// <summary>(0042,0010) VR=ST VM=1 Document Title</summary>
		DicomConstTags.DocumentTitle = 0x00420010;

		/// <summary>(0042,0011) VR=OB VM=1 Encapsulated Document</summary>
		DicomConstTags.EncapsulatedDocument = 0x00420011;

		/// <summary>(0042,0012) VR=LO VM=1 MIME Type of Encapsulated Document</summary>
		DicomConstTags.MIMETypeOfEncapsulatedDocument = 0x00420012;

		/// <summary>(0042,0013) VR=SQ VM=1 Source Instance Sequence</summary>
		DicomConstTags.SourceInstanceSequence = 0x00420013;

		/// <summary>(0042,0014) VR=LO VM=1-n List of MIME Types</summary>
		DicomConstTags.ListOfMIMETypes = 0x00420014;

		/// <summary>(0044,0001) VR=ST VM=1 Product Package Identifier</summary>
		DicomConstTags.ProductPackageIdentifier = 0x00440001;

		/// <summary>(0044,0002) VR=CS VM=1 Substance Administration Approval</summary>
		DicomConstTags.SubstanceAdministrationApproval = 0x00440002;

		/// <summary>(0044,0003) VR=LT VM=1 Approval Status Further Description</summary>
		DicomConstTags.ApprovalStatusFurtherDescription = 0x00440003;

		/// <summary>(0044,0004) VR=DT VM=1 Approval Status DateTime</summary>
		DicomConstTags.ApprovalStatusDateTime = 0x00440004;

		/// <summary>(0044,0007) VR=SQ VM=1 Product Type Code Sequence</summary>
		DicomConstTags.ProductTypeCodeSequence = 0x00440007;

		/// <summary>(0044,0008) VR=LO VM=1-n Product Name</summary>
		DicomConstTags.ProductName = 0x00440008;

		/// <summary>(0044,0009) VR=LT VM=1 Product Description</summary>
		DicomConstTags.ProductDescription = 0x00440009;

		/// <summary>(0044,000a) VR=LO VM=1 Product Lot Identifier</summary>
		DicomConstTags.ProductLotIdentifier = 0x0044000a;

		/// <summary>(0044,000b) VR=DT VM=1 Product Expiration DateTime</summary>
		DicomConstTags.ProductExpirationDateTime = 0x0044000b;

		/// <summary>(0044,0010) VR=DT VM=1 Substance Administration DateTime</summary>
		DicomConstTags.SubstanceAdministrationDateTime = 0x00440010;

		/// <summary>(0044,0011) VR=LO VM=1 Substance Administration Notes</summary>
		DicomConstTags.SubstanceAdministrationNotes = 0x00440011;

		/// <summary>(0044,0012) VR=LO VM=1 Substance Administration Device ID</summary>
		DicomConstTags.SubstanceAdministrationDeviceID = 0x00440012;

		/// <summary>(0044,0013) VR=SQ VM=1 Product Parameter Sequence</summary>
		DicomConstTags.ProductParameterSequence = 0x00440013;

		/// <summary>(0044,0019) VR=SQ VM=1 Substance Administration Parameter Sequence</summary>
		DicomConstTags.SubstanceAdministrationParameterSequence = 0x00440019;

		/// <summary>(0050,0004) VR=CS VM=1 Calibration Image</summary>
		DicomConstTags.CalibrationImage = 0x00500004;

		/// <summary>(0050,0010) VR=SQ VM=1 Device Sequence</summary>
		DicomConstTags.DeviceSequence = 0x00500010;

		/// <summary>(0050,0014) VR=DS VM=1 Device Length</summary>
		DicomConstTags.DeviceLength = 0x00500014;

		/// <summary>(0050,0016) VR=DS VM=1 Device Diameter</summary>
		DicomConstTags.DeviceDiameter = 0x00500016;

		/// <summary>(0050,0017) VR=CS VM=1 Device Diameter Units</summary>
		DicomConstTags.DeviceDiameterUnits = 0x00500017;

		/// <summary>(0050,0018) VR=DS VM=1 Device Volume</summary>
		DicomConstTags.DeviceVolume = 0x00500018;

		/// <summary>(0050,0019) VR=DS VM=1 Intermarker Distance</summary>
		DicomConstTags.IntermarkerDistance = 0x00500019;

		/// <summary>(0050,0020) VR=LO VM=1 Device Description</summary>
		DicomConstTags.DeviceDescription = 0x00500020;

		/// <summary>(0054,0010) VR=US VM=1-n Energy Window Vector</summary>
		DicomConstTags.EnergyWindowVector = 0x00540010;

		/// <summary>(0054,0011) VR=US VM=1 Number of Energy Windows</summary>
		DicomConstTags.NumberOfEnergyWindows = 0x00540011;

		/// <summary>(0054,0012) VR=SQ VM=1 Energy Window Information Sequence</summary>
		DicomConstTags.EnergyWindowInformationSequence = 0x00540012;

		/// <summary>(0054,0013) VR=SQ VM=1 Energy Window Range Sequence</summary>
		DicomConstTags.EnergyWindowRangeSequence = 0x00540013;

		/// <summary>(0054,0014) VR=DS VM=1 Energy Window Lower Limit</summary>
		DicomConstTags.EnergyWindowLowerLimit = 0x00540014;

		/// <summary>(0054,0015) VR=DS VM=1 Energy Window Upper Limit</summary>
		DicomConstTags.EnergyWindowUpperLimit = 0x00540015;

		/// <summary>(0054,0016) VR=SQ VM=1 Radiopharmaceutical Information Sequence</summary>
		DicomConstTags.RadiopharmaceuticalInformationSequence = 0x00540016;

		/// <summary>(0054,0017) VR=IS VM=1 Residual Syringe Counts</summary>
		DicomConstTags.ResidualSyringeCounts = 0x00540017;

		/// <summary>(0054,0018) VR=SH VM=1 Energy Window Name</summary>
		DicomConstTags.EnergyWindowName = 0x00540018;

		/// <summary>(0054,0020) VR=US VM=1-n Detector Vector</summary>
		DicomConstTags.DetectorVector = 0x00540020;

		/// <summary>(0054,0021) VR=US VM=1 Number of Detectors</summary>
		DicomConstTags.NumberOfDetectors = 0x00540021;

		/// <summary>(0054,0022) VR=SQ VM=1 Detector Information Sequence</summary>
		DicomConstTags.DetectorInformationSequence = 0x00540022;

		/// <summary>(0054,0030) VR=US VM=1-n Phase Vector</summary>
		DicomConstTags.PhaseVector = 0x00540030;

		/// <summary>(0054,0031) VR=US VM=1 Number of Phases</summary>
		DicomConstTags.NumberOfPhases = 0x00540031;

		/// <summary>(0054,0032) VR=SQ VM=1 Phase Information Sequence</summary>
		DicomConstTags.PhaseInformationSequence = 0x00540032;

		/// <summary>(0054,0033) VR=US VM=1 Number of Frames in Phase</summary>
		DicomConstTags.NumberOfFramesInPhase = 0x00540033;

		/// <summary>(0054,0036) VR=IS VM=1 Phase Delay</summary>
		DicomConstTags.PhaseDelay = 0x00540036;

		/// <summary>(0054,0038) VR=IS VM=1 Pause Between Frames</summary>
		DicomConstTags.PauseBetweenFrames = 0x00540038;

		/// <summary>(0054,0039) VR=CS VM=1 Phase Description</summary>
		DicomConstTags.PhaseDescription = 0x00540039;

		/// <summary>(0054,0050) VR=US VM=1-n Rotation Vector</summary>
		DicomConstTags.RotationVector = 0x00540050;

		/// <summary>(0054,0051) VR=US VM=1 Number of Rotations</summary>
		DicomConstTags.NumberOfRotations = 0x00540051;

		/// <summary>(0054,0052) VR=SQ VM=1 Rotation Information Sequence</summary>
		DicomConstTags.RotationInformationSequence = 0x00540052;

		/// <summary>(0054,0053) VR=US VM=1 Number of Frames in Rotation</summary>
		DicomConstTags.NumberOfFramesInRotation = 0x00540053;

		/// <summary>(0054,0060) VR=US VM=1-n R-R Interval Vector</summary>
		DicomConstTags.RRIntervalVector = 0x00540060;

		/// <summary>(0054,0061) VR=US VM=1 Number of R-R Intervals</summary>
		DicomConstTags.NumberOfRRIntervals = 0x00540061;

		/// <summary>(0054,0062) VR=SQ VM=1 Gated Information Sequence</summary>
		DicomConstTags.GatedInformationSequence = 0x00540062;

		/// <summary>(0054,0063) VR=SQ VM=1 Data Information Sequence</summary>
		DicomConstTags.DataInformationSequence = 0x00540063;

		/// <summary>(0054,0070) VR=US VM=1-n Time Slot Vector</summary>
		DicomConstTags.TimeSlotVector = 0x00540070;

		/// <summary>(0054,0071) VR=US VM=1 Number of Time Slots</summary>
		DicomConstTags.NumberOfTimeSlots = 0x00540071;

		/// <summary>(0054,0072) VR=SQ VM=1 Time Slot Information Sequence</summary>
		DicomConstTags.TimeSlotInformationSequence = 0x00540072;

		/// <summary>(0054,0073) VR=DS VM=1 Time Slot Time</summary>
		DicomConstTags.TimeSlotTime = 0x00540073;

		/// <summary>(0054,0080) VR=US VM=1-n Slice Vector</summary>
		DicomConstTags.SliceVector = 0x00540080;

		/// <summary>(0054,0081) VR=US VM=1 Number of Slices</summary>
		DicomConstTags.NumberOfSlices = 0x00540081;

		/// <summary>(0054,0090) VR=US VM=1-n Angular View Vector</summary>
		DicomConstTags.AngularViewVector = 0x00540090;

		/// <summary>(0054,0100) VR=US VM=1-n Time Slice Vector</summary>
		DicomConstTags.TimeSliceVector = 0x00540100;

		/// <summary>(0054,0101) VR=US VM=1 Number of Time Slices</summary>
		DicomConstTags.NumberOfTimeSlices = 0x00540101;

		/// <summary>(0054,0200) VR=DS VM=1 Start Angle</summary>
		DicomConstTags.StartAngle = 0x00540200;

		/// <summary>(0054,0202) VR=CS VM=1 Type of Detector Motion</summary>
		DicomConstTags.TypeOfDetectorMotion = 0x00540202;

		/// <summary>(0054,0210) VR=IS VM=1-n Trigger Vector</summary>
		DicomConstTags.TriggerVector = 0x00540210;

		/// <summary>(0054,0211) VR=US VM=1 Number of Triggers in Phase</summary>
		DicomConstTags.NumberOfTriggersInPhase = 0x00540211;

		/// <summary>(0054,0220) VR=SQ VM=1 View Code Sequence</summary>
		DicomConstTags.ViewCodeSequence = 0x00540220;

		/// <summary>(0054,0222) VR=SQ VM=1 View Modifier Code Sequence</summary>
		DicomConstTags.ViewModifierCodeSequence = 0x00540222;

		/// <summary>(0054,0300) VR=SQ VM=1 Radionuclide Code Sequence</summary>
		DicomConstTags.RadionuclideCodeSequence = 0x00540300;

		/// <summary>(0054,0302) VR=SQ VM=1 Administration Route Code Sequence</summary>
		DicomConstTags.AdministrationRouteCodeSequence = 0x00540302;

		/// <summary>(0054,0304) VR=SQ VM=1 Radiopharmaceutical Code Sequence</summary>
		DicomConstTags.RadiopharmaceuticalCodeSequence = 0x00540304;

		/// <summary>(0054,0306) VR=SQ VM=1 Calibration Data Sequence</summary>
		DicomConstTags.CalibrationDataSequence = 0x00540306;

		/// <summary>(0054,0308) VR=US VM=1 Energy Window Number</summary>
		DicomConstTags.EnergyWindowNumber = 0x00540308;

		/// <summary>(0054,0400) VR=SH VM=1 Image ID</summary>
		DicomConstTags.ImageID = 0x00540400;

		/// <summary>(0054,0410) VR=SQ VM=1 Patient Orientation Code Sequence</summary>
		DicomConstTags.PatientOrientationCodeSequence = 0x00540410;

		/// <summary>(0054,0412) VR=SQ VM=1 Patient Orientation Modifier Code Sequence</summary>
		DicomConstTags.PatientOrientationModifierCodeSequence = 0x00540412;

		/// <summary>(0054,0414) VR=SQ VM=1 Patient Gantry Relationship Code Sequence</summary>
		DicomConstTags.PatientGantryRelationshipCodeSequence = 0x00540414;

		/// <summary>(0054,0500) VR=CS VM=1 Slice Progression Direction</summary>
		DicomConstTags.SliceProgressionDirection = 0x00540500;

		/// <summary>(0054,1000) VR=CS VM=2 Series Type</summary>
		DicomConstTags.SeriesType = 0x00541000;

		/// <summary>(0054,1001) VR=CS VM=1 Units</summary>
		DicomConstTags.Units = 0x00541001;

		/// <summary>(0054,1002) VR=CS VM=1 Counts Source</summary>
		DicomConstTags.CountsSource = 0x00541002;

		/// <summary>(0054,1004) VR=CS VM=1 Reprojection Method</summary>
		DicomConstTags.ReprojectionMethod = 0x00541004;

		/// <summary>(0054,1100) VR=CS VM=1 Randoms Correction Method</summary>
		DicomConstTags.RandomsCorrectionMethod = 0x00541100;

		/// <summary>(0054,1101) VR=LO VM=1 Attenuation Correction Method</summary>
		DicomConstTags.AttenuationCorrectionMethod = 0x00541101;

		/// <summary>(0054,1102) VR=CS VM=1 Decay Correction</summary>
		DicomConstTags.DecayCorrection = 0x00541102;

		/// <summary>(0054,1103) VR=LO VM=1 Reconstruction Method</summary>
		DicomConstTags.ReconstructionMethod = 0x00541103;

		/// <summary>(0054,1104) VR=LO VM=1 Detector Lines of Response Used</summary>
		DicomConstTags.DetectorLinesOfResponseUsed = 0x00541104;

		/// <summary>(0054,1105) VR=LO VM=1 Scatter Correction Method</summary>
		DicomConstTags.ScatterCorrectionMethod = 0x00541105;

		/// <summary>(0054,1200) VR=DS VM=1 Axial Acceptance</summary>
		DicomConstTags.AxialAcceptance = 0x00541200;

		/// <summary>(0054,1201) VR=IS VM=2 Axial Mash</summary>
		DicomConstTags.AxialMash = 0x00541201;

		/// <summary>(0054,1202) VR=IS VM=1 Transverse Mash</summary>
		DicomConstTags.TransverseMash = 0x00541202;

		/// <summary>(0054,1203) VR=DS VM=2 Detector Element Size</summary>
		DicomConstTags.DetectorElementSize = 0x00541203;

		/// <summary>(0054,1210) VR=DS VM=1 Coincidence Window Width</summary>
		DicomConstTags.CoincidenceWindowWidth = 0x00541210;

		/// <summary>(0054,1220) VR=CS VM=1-n Secondary Counts Type</summary>
		DicomConstTags.SecondaryCountsType = 0x00541220;

		/// <summary>(0054,1300) VR=DS VM=1 Frame Reference Time</summary>
		DicomConstTags.FrameReferenceTime = 0x00541300;

		/// <summary>(0054,1310) VR=IS VM=1 Primary (Prompts) Counts Accumulated</summary>
		DicomConstTags.PrimaryPromptsCountsAccumulated = 0x00541310;

		/// <summary>(0054,1311) VR=IS VM=1-n Secondary Counts Accumulated</summary>
		DicomConstTags.SecondaryCountsAccumulated = 0x00541311;

		/// <summary>(0054,1320) VR=DS VM=1 Slice Sensitivity Factor</summary>
		DicomConstTags.SliceSensitivityFactor = 0x00541320;

		/// <summary>(0054,1321) VR=DS VM=1 Decay Factor</summary>
		DicomConstTags.DecayFactor = 0x00541321;

		/// <summary>(0054,1322) VR=DS VM=1 Dose Calibration Factor</summary>
		DicomConstTags.DoseCalibrationFactor = 0x00541322;

		/// <summary>(0054,1323) VR=DS VM=1 Scatter Fraction Factor</summary>
		DicomConstTags.ScatterFractionFactor = 0x00541323;

		/// <summary>(0054,1324) VR=DS VM=1 Dead Time Factor</summary>
		DicomConstTags.DeadTimeFactor = 0x00541324;

		/// <summary>(0054,1330) VR=US VM=1 Image Index</summary>
		DicomConstTags.ImageIndex = 0x00541330;

		/// <summary>(0060,3000) VR=SQ VM=1 Histogram Sequence</summary>
		DicomConstTags.HistogramSequence = 0x00603000;

		/// <summary>(0060,3002) VR=US VM=1 Histogram Number of Bins</summary>
		DicomConstTags.HistogramNumberOfBins = 0x00603002;

		/// <summary>(0060,3004) VR=US/SS VM=1 Histogram First Bin Value</summary>
		DicomConstTags.HistogramFirstBinValue = 0x00603004;

		/// <summary>(0060,3006) VR=US/SS VM=1 Histogram Last Bin Value</summary>
		DicomConstTags.HistogramLastBinValue = 0x00603006;

		/// <summary>(0060,3008) VR=US VM=1 Histogram Bin Width</summary>
		DicomConstTags.HistogramBinWidth = 0x00603008;

		/// <summary>(0060,3010) VR=LO VM=1 Histogram Explanation</summary>
		DicomConstTags.HistogramExplanation = 0x00603010;

		/// <summary>(0060,3020) VR=UL VM=1-n Histogram Data</summary>
		DicomConstTags.HistogramData = 0x00603020;

		/// <summary>(0062,0001) VR=CS VM=1 Segmentation Type</summary>
		DicomConstTags.SegmentationType = 0x00620001;

		/// <summary>(0062,0002) VR=SQ VM=1 Segment Sequence</summary>
		DicomConstTags.SegmentSequence = 0x00620002;

		/// <summary>(0062,0003) VR=SQ VM=1 Segmented Property Category Code Sequence</summary>
		DicomConstTags.SegmentedPropertyCategoryCodeSequence = 0x00620003;

		/// <summary>(0062,0004) VR=US VM=1 Segment Number</summary>
		DicomConstTags.SegmentNumber = 0x00620004;

		/// <summary>(0062,0005) VR=LO VM=1 Segment Label</summary>
		DicomConstTags.SegmentLabel = 0x00620005;

		/// <summary>(0062,0006) VR=ST VM=1 Segment Description</summary>
		DicomConstTags.SegmentDescription = 0x00620006;

		/// <summary>(0062,0008) VR=CS VM=1 Segment Algorithm Type</summary>
		DicomConstTags.SegmentAlgorithmType = 0x00620008;

		/// <summary>(0062,0009) VR=LO VM=1 Segment Algorithm Name</summary>
		DicomConstTags.SegmentAlgorithmName = 0x00620009;

		/// <summary>(0062,000a) VR=SQ VM=1 Segment Identification Sequence</summary>
		DicomConstTags.SegmentIdentificationSequence = 0x0062000a;

		/// <summary>(0062,000b) VR=US VM=1-n Referenced Segment Number</summary>
		DicomConstTags.ReferencedSegmentNumber = 0x0062000b;

		/// <summary>(0062,000c) VR=US VM=1 Recommended Display Grayscale Value</summary>
		DicomConstTags.RecommendedDisplayGrayscaleValue = 0x0062000c;

		/// <summary>(0062,000d) VR=US VM=3 Recommended Display CIELab Value</summary>
		DicomConstTags.RecommendedDisplayCIELabValue = 0x0062000d;

		/// <summary>(0062,000e) VR=US VM=1 Maximum Fractional Value</summary>
		DicomConstTags.MaximumFractionalValue = 0x0062000e;

		/// <summary>(0062,000f) VR=SQ VM=1 Segmented Property Type Code Sequence</summary>
		DicomConstTags.SegmentedPropertyTypeCodeSequence = 0x0062000f;

		/// <summary>(0062,0010) VR=CS VM=1 Segmentation Fractional Type</summary>
		DicomConstTags.SegmentationFractionalType = 0x00620010;

		/// <summary>(0064,0002) VR=SQ VM=1 Deformable Registration Sequence</summary>
		DicomConstTags.DeformableRegistrationSequence = 0x00640002;

		/// <summary>(0064,0003) VR=UI VM=1 Source Frame of Reference UID</summary>
		DicomConstTags.SourceFrameOfReferenceUID = 0x00640003;

		/// <summary>(0064,0005) VR=SQ VM=1 Deformable Registration Grid Sequence</summary>
		DicomConstTags.DeformableRegistrationGridSequence = 0x00640005;

		/// <summary>(0064,0007) VR=UL VM=3 Grid Dimensions</summary>
		DicomConstTags.GridDimensions = 0x00640007;

		/// <summary>(0064,0008) VR=FD VM=3 Grid Resolution</summary>
		DicomConstTags.GridResolution = 0x00640008;

		/// <summary>(0064,0009) VR=OF VM=1 Vector Grid Data</summary>
		DicomConstTags.VectorGridData = 0x00640009;

		/// <summary>(0064,000f) VR=SQ VM=1 Pre Deformation Matrix Registration Sequence</summary>
		DicomConstTags.PreDeformationMatrixRegistrationSequence = 0x0064000f;

		/// <summary>(0064,0010) VR=SQ VM=1 Post Deformation Matrix Registration Sequence</summary>
		DicomConstTags.PostDeformationMatrixRegistrationSequence = 0x00640010;

		/// <summary>(0070,0001) VR=SQ VM=1 Graphic Annotation Sequence</summary>
		DicomConstTags.GraphicAnnotationSequence = 0x00700001;

		/// <summary>(0070,0002) VR=CS VM=1 Graphic Layer</summary>
		DicomConstTags.GraphicLayer = 0x00700002;

		/// <summary>(0070,0003) VR=CS VM=1 Bounding Box Annotation Units</summary>
		DicomConstTags.BoundingBoxAnnotationUnits = 0x00700003;

		/// <summary>(0070,0004) VR=CS VM=1 Anchor Point Annotation Units</summary>
		DicomConstTags.AnchorPointAnnotationUnits = 0x00700004;

		/// <summary>(0070,0005) VR=CS VM=1 Graphic Annotation Units</summary>
		DicomConstTags.GraphicAnnotationUnits = 0x00700005;

		/// <summary>(0070,0006) VR=ST VM=1 Unformatted Text Value</summary>
		DicomConstTags.UnformattedTextValue = 0x00700006;

		/// <summary>(0070,0008) VR=SQ VM=1 Text Object Sequence</summary>
		DicomConstTags.TextObjectSequence = 0x00700008;

		/// <summary>(0070,0009) VR=SQ VM=1 Graphic Object Sequence</summary>
		DicomConstTags.GraphicObjectSequence = 0x00700009;

		/// <summary>(0070,0010) VR=FL VM=2 Bounding Box Top Left Hand Corner</summary>
		DicomConstTags.BoundingBoxTopLeftHandCorner = 0x00700010;

		/// <summary>(0070,0011) VR=FL VM=2 Bounding Box Bottom Right Hand Corner</summary>
		DicomConstTags.BoundingBoxBottomRightHandCorner = 0x00700011;

		/// <summary>(0070,0012) VR=CS VM=1 Bounding Box Text Horizontal Justification</summary>
		DicomConstTags.BoundingBoxTextHorizontalJustification = 0x00700012;

		/// <summary>(0070,0014) VR=FL VM=2 Anchor Point</summary>
		DicomConstTags.AnchorPoint = 0x00700014;

		/// <summary>(0070,0015) VR=CS VM=1 Anchor Point Visibility</summary>
		DicomConstTags.AnchorPointVisibility = 0x00700015;

		/// <summary>(0070,0020) VR=US VM=1 Graphic Dimensions</summary>
		DicomConstTags.GraphicDimensions = 0x00700020;

		/// <summary>(0070,0021) VR=US VM=1 Number of Graphic Points</summary>
		DicomConstTags.NumberOfGraphicPoints = 0x00700021;

		/// <summary>(0070,0022) VR=FL VM=2-n Graphic Data</summary>
		DicomConstTags.GraphicData = 0x00700022;

		/// <summary>(0070,0023) VR=CS VM=1 Graphic Type</summary>
		DicomConstTags.GraphicType = 0x00700023;

		/// <summary>(0070,0024) VR=CS VM=1 Graphic Filled</summary>
		DicomConstTags.GraphicFilled = 0x00700024;

		/// <summary>(0070,0041) VR=CS VM=1 Image Horizontal Flip</summary>
		DicomConstTags.ImageHorizontalFlip = 0x00700041;

		/// <summary>(0070,0042) VR=US VM=1 Image Rotation</summary>
		DicomConstTags.ImageRotation = 0x00700042;

		/// <summary>(0070,0052) VR=SL VM=2 Displayed Area Top Left Hand Corner</summary>
		DicomConstTags.DisplayedAreaTopLeftHandCorner = 0x00700052;

		/// <summary>(0070,0053) VR=SL VM=2 Displayed Area Bottom Right Hand Corner</summary>
		DicomConstTags.DisplayedAreaBottomRightHandCorner = 0x00700053;

		/// <summary>(0070,005a) VR=SQ VM=1 Displayed Area Selection Sequence</summary>
		DicomConstTags.DisplayedAreaSelectionSequence = 0x0070005a;

		/// <summary>(0070,0060) VR=SQ VM=1 Graphic Layer Sequence</summary>
		DicomConstTags.GraphicLayerSequence = 0x00700060;

		/// <summary>(0070,0062) VR=IS VM=1 Graphic Layer Order</summary>
		DicomConstTags.GraphicLayerOrder = 0x00700062;

		/// <summary>(0070,0066) VR=US VM=1 Graphic Layer Recommended Display Grayscale Value</summary>
		DicomConstTags.GraphicLayerRecommendedDisplayGrayscaleValue = 0x00700066;

		/// <summary>(0070,0068) VR=LO VM=1 Graphic Layer Description</summary>
		DicomConstTags.GraphicLayerDescription = 0x00700068;

		/// <summary>(0070,0080) VR=CS VM=1 Content Label</summary>
		DicomConstTags.ContentLabel = 0x00700080;

		/// <summary>(0070,0081) VR=LO VM=1 Content Description</summary>
		DicomConstTags.ContentDescription = 0x00700081;

		/// <summary>(0070,0082) VR=DA VM=1 Presentation Creation Date</summary>
		DicomConstTags.PresentationCreationDate = 0x00700082;

		/// <summary>(0070,0083) VR=TM VM=1 Presentation Creation Time</summary>
		DicomConstTags.PresentationCreationTime = 0x00700083;

		/// <summary>(0070,0084) VR=PN VM=1 Content Creator's Name</summary>
		DicomConstTags.ContentCreatorsName = 0x00700084;

		/// <summary>(0070,0086) VR=SQ VM=1 Content Creator's Identification Code Sequence</summary>
		DicomConstTags.ContentCreatorsIdentificationCodeSequence = 0x00700086;

		/// <summary>(0070,0100) VR=CS VM=1 Presentation Size Mode</summary>
		DicomConstTags.PresentationSizeMode = 0x00700100;

		/// <summary>(0070,0101) VR=DS VM=2 Presentation Pixel Spacing</summary>
		DicomConstTags.PresentationPixelSpacing = 0x00700101;

		/// <summary>(0070,0102) VR=IS VM=2 Presentation Pixel Aspect Ratio</summary>
		DicomConstTags.PresentationPixelAspectRatio = 0x00700102;

		/// <summary>(0070,0103) VR=FL VM=1 Presentation Pixel Magnification Ratio</summary>
		DicomConstTags.PresentationPixelMagnificationRatio = 0x00700103;

		/// <summary>(0070,0306) VR=CS VM=1 Shape Type</summary>
		DicomConstTags.ShapeType = 0x00700306;

		/// <summary>(0070,0308) VR=SQ VM=1 Registration Sequence</summary>
		DicomConstTags.RegistrationSequence = 0x00700308;

		/// <summary>(0070,0309) VR=SQ VM=1 Matrix Registration Sequence</summary>
		DicomConstTags.MatrixRegistrationSequence = 0x00700309;

		/// <summary>(0070,030a) VR=SQ VM=1 Matrix Sequence</summary>
		DicomConstTags.MatrixSequence = 0x0070030a;

		/// <summary>(0070,030c) VR=CS VM=1 Frame of Reference Transformation Matrix Type</summary>
		DicomConstTags.FrameOfReferenceTransformationMatrixType = 0x0070030c;

		/// <summary>(0070,030d) VR=SQ VM=1 Registration Type Code Sequence</summary>
		DicomConstTags.RegistrationTypeCodeSequence = 0x0070030d;

		/// <summary>(0070,030f) VR=ST VM=1 Fiducial Description</summary>
		DicomConstTags.FiducialDescription = 0x0070030f;

		/// <summary>(0070,0310) VR=SH VM=1 Fiducial Identifier</summary>
		DicomConstTags.FiducialIdentifier = 0x00700310;

		/// <summary>(0070,0311) VR=SQ VM=1 Fiducial Identifier Code Sequence</summary>
		DicomConstTags.FiducialIdentifierCodeSequence = 0x00700311;

		/// <summary>(0070,0312) VR=FD VM=1 Contour Uncertainty Radius</summary>
		DicomConstTags.ContourUncertaintyRadius = 0x00700312;

		/// <summary>(0070,0314) VR=SQ VM=1 Used Fiducials Sequence</summary>
		DicomConstTags.UsedFiducialsSequence = 0x00700314;

		/// <summary>(0070,0318) VR=SQ VM=1 Graphic Coordinates Data Sequence</summary>
		DicomConstTags.GraphicCoordinatesDataSequence = 0x00700318;

		/// <summary>(0070,031a) VR=UI VM=1 Fiducial UID</summary>
		DicomConstTags.FiducialUID = 0x0070031a;

		/// <summary>(0070,031c) VR=SQ VM=1 Fiducial Set Sequence</summary>
		DicomConstTags.FiducialSetSequence = 0x0070031c;

		/// <summary>(0070,031e) VR=SQ VM=1 Fiducial Sequence</summary>
		DicomConstTags.FiducialSequence = 0x0070031e;

		/// <summary>(0070,0401) VR=US VM=3 Graphic Layer Recommended Display CIELab Value</summary>
		DicomConstTags.GraphicLayerRecommendedDisplayCIELabValue = 0x00700401;

		/// <summary>(0070,0402) VR=SQ VM=1 Blending Sequence</summary>
		DicomConstTags.BlendingSequence = 0x00700402;

		/// <summary>(0070,0403) VR=FL VM=1 Relative Opacity</summary>
		DicomConstTags.RelativeOpacity = 0x00700403;

		/// <summary>(0070,0404) VR=SQ VM=1 Referenced Spatial Registration Sequence</summary>
		DicomConstTags.ReferencedSpatialRegistrationSequence = 0x00700404;

		/// <summary>(0070,0405) VR=CS VM=1 Blending Position</summary>
		DicomConstTags.BlendingPosition = 0x00700405;

		/// <summary>(0072,0002) VR=SH VM=1 Hanging Protocol Name</summary>
		DicomConstTags.HangingProtocolName = 0x00720002;

		/// <summary>(0072,0004) VR=LO VM=1 Hanging Protocol Description</summary>
		DicomConstTags.HangingProtocolDescription = 0x00720004;

		/// <summary>(0072,0006) VR=CS VM=1 Hanging Protocol Level</summary>
		DicomConstTags.HangingProtocolLevel = 0x00720006;

		/// <summary>(0072,0008) VR=LO VM=1 Hanging Protocol Creator</summary>
		DicomConstTags.HangingProtocolCreator = 0x00720008;

		/// <summary>(0072,000a) VR=DT VM=1 Hanging Protocol Creation DateTime</summary>
		DicomConstTags.HangingProtocolCreationDateTime = 0x0072000a;

		/// <summary>(0072,000c) VR=SQ VM=1 Hanging Protocol Definition Sequence</summary>
		DicomConstTags.HangingProtocolDefinitionSequence = 0x0072000c;

		/// <summary>(0072,000e) VR=SQ VM=1 Hanging Protocol User Identification Code Sequence</summary>
		DicomConstTags.HangingProtocolUserIdentificationCodeSequence = 0x0072000e;

		/// <summary>(0072,0010) VR=LO VM=1 Hanging Protocol User Group Name</summary>
		DicomConstTags.HangingProtocolUserGroupName = 0x00720010;

		/// <summary>(0072,0012) VR=SQ VM=1 Source Hanging Protocol Sequence</summary>
		DicomConstTags.SourceHangingProtocolSequence = 0x00720012;

		/// <summary>(0072,0014) VR=US VM=1 Number of Priors Referenced</summary>
		DicomConstTags.NumberOfPriorsReferenced = 0x00720014;

		/// <summary>(0072,0020) VR=SQ VM=1 Image Sets Sequence</summary>
		DicomConstTags.ImageSetsSequence = 0x00720020;

		/// <summary>(0072,0022) VR=SQ VM=1 Image Set Selector Sequence</summary>
		DicomConstTags.ImageSetSelectorSequence = 0x00720022;

		/// <summary>(0072,0024) VR=CS VM=1 Image Set Selector Usage Flag</summary>
		DicomConstTags.ImageSetSelectorUsageFlag = 0x00720024;

		/// <summary>(0072,0026) VR=AT VM=1 Selector Attribute</summary>
		DicomConstTags.SelectorAttribute = 0x00720026;

		/// <summary>(0072,0028) VR=US VM=1 Selector Value Number</summary>
		DicomConstTags.SelectorValueNumber = 0x00720028;

		/// <summary>(0072,0030) VR=SQ VM=1 Time Based Image Sets Sequence</summary>
		DicomConstTags.TimeBasedImageSetsSequence = 0x00720030;

		/// <summary>(0072,0032) VR=US VM=1 Image Set Number</summary>
		DicomConstTags.ImageSetNumber = 0x00720032;

		/// <summary>(0072,0034) VR=CS VM=1 Image Set Selector Category</summary>
		DicomConstTags.ImageSetSelectorCategory = 0x00720034;

		/// <summary>(0072,0038) VR=US VM=2 Relative Time</summary>
		DicomConstTags.RelativeTime = 0x00720038;

		/// <summary>(0072,003a) VR=CS VM=1 Relative Time Units</summary>
		DicomConstTags.RelativeTimeUnits = 0x0072003a;

		/// <summary>(0072,003c) VR=SS VM=2 Abstract Prior Value</summary>
		DicomConstTags.AbstractPriorValue = 0x0072003c;

		/// <summary>(0072,003e) VR=SQ VM=1 Abstract Prior Code Sequence</summary>
		DicomConstTags.AbstractPriorCodeSequence = 0x0072003e;

		/// <summary>(0072,0040) VR=LO VM=1 Image Set Label</summary>
		DicomConstTags.ImageSetLabel = 0x00720040;

		/// <summary>(0072,0050) VR=CS VM=1 Selector Attribute VR</summary>
		DicomConstTags.SelectorAttributeVR = 0x00720050;

		/// <summary>(0072,0052) VR=AT VM=1 Selector Sequence Pointer</summary>
		DicomConstTags.SelectorSequencePointer = 0x00720052;

		/// <summary>(0072,0054) VR=LO VM=1 Selector Sequence Pointer Private Creator</summary>
		DicomConstTags.SelectorSequencePointerPrivateCreator = 0x00720054;

		/// <summary>(0072,0056) VR=LO VM=1 Selector Attribute Private Creator</summary>
		DicomConstTags.SelectorAttributePrivateCreator = 0x00720056;

		/// <summary>(0072,0060) VR=AT VM=1-n Selector AT Value</summary>
		DicomConstTags.SelectorATValue = 0x00720060;

		/// <summary>(0072,0062) VR=CS VM=1-n Selector CS Value</summary>
		DicomConstTags.SelectorCSValue = 0x00720062;

		/// <summary>(0072,0064) VR=IS VM=1-n Selector IS Value</summary>
		DicomConstTags.SelectorISValue = 0x00720064;

		/// <summary>(0072,0066) VR=LO VM=1-n Selector LO Value</summary>
		DicomConstTags.SelectorLOValue = 0x00720066;

		/// <summary>(0072,0068) VR=LT VM=1 Selector LT Value</summary>
		DicomConstTags.SelectorLTValue = 0x00720068;

		/// <summary>(0072,006a) VR=PN VM=1-n Selector PN Value</summary>
		DicomConstTags.SelectorPNValue = 0x0072006a;

		/// <summary>(0072,006c) VR=SH VM=1-n Selector SH Value</summary>
		DicomConstTags.SelectorSHValue = 0x0072006c;

		/// <summary>(0072,006e) VR=ST VM=1 Selector ST Value</summary>
		DicomConstTags.SelectorSTValue = 0x0072006e;

		/// <summary>(0072,0070) VR=UT VM=1 Selector UT Value</summary>
		DicomConstTags.SelectorUTValue = 0x00720070;

		/// <summary>(0072,0072) VR=DS VM=1-n Selector DS Value</summary>
		DicomConstTags.SelectorDSValue = 0x00720072;

		/// <summary>(0072,0074) VR=FD VM=1-n Selector FD Value</summary>
		DicomConstTags.SelectorFDValue = 0x00720074;

		/// <summary>(0072,0076) VR=FL VM=1-n Selector FL Value</summary>
		DicomConstTags.SelectorFLValue = 0x00720076;

		/// <summary>(0072,0078) VR=UL VM=1-n Selector UL Value</summary>
		DicomConstTags.SelectorULValue = 0x00720078;

		/// <summary>(0072,007a) VR=US VM=1-n Selector US Value</summary>
		DicomConstTags.SelectorUSValue = 0x0072007a;

		/// <summary>(0072,007c) VR=SL VM=1-n Selector SL Value</summary>
		DicomConstTags.SelectorSLValue = 0x0072007c;

		/// <summary>(0072,007e) VR=SS VM=1-n Selector SS Value</summary>
		DicomConstTags.SelectorSSValue = 0x0072007e;

		/// <summary>(0072,0080) VR=SQ VM=1 Selector Code Sequence Value</summary>
		DicomConstTags.SelectorCodeSequenceValue = 0x00720080;

		/// <summary>(0072,0100) VR=US VM=1 Number of Screens</summary>
		DicomConstTags.NumberOfScreens = 0x00720100;

		/// <summary>(0072,0102) VR=SQ VM=1 Nominal Screen Definition Sequence</summary>
		DicomConstTags.NominalScreenDefinitionSequence = 0x00720102;

		/// <summary>(0072,0104) VR=US VM=1 Number of Vertical Pixels</summary>
		DicomConstTags.NumberOfVerticalPixels = 0x00720104;

		/// <summary>(0072,0106) VR=US VM=1 Number of Horizontal Pixels</summary>
		DicomConstTags.NumberOfHorizontalPixels = 0x00720106;

		/// <summary>(0072,0108) VR=FD VM=4 Display Environment Spatial Position</summary>
		DicomConstTags.DisplayEnvironmentSpatialPosition = 0x00720108;

		/// <summary>(0072,010a) VR=US VM=1 Screen Minimum Grayscale Bit Depth</summary>
		DicomConstTags.ScreenMinimumGrayscaleBitDepth = 0x0072010a;

		/// <summary>(0072,010c) VR=US VM=1 Screen Minimum Color Bit Depth</summary>
		DicomConstTags.ScreenMinimumColorBitDepth = 0x0072010c;

		/// <summary>(0072,010e) VR=US VM=1 Application Maximum Repaint Time</summary>
		DicomConstTags.ApplicationMaximumRepaintTime = 0x0072010e;

		/// <summary>(0072,0200) VR=SQ VM=1 Display Sets Sequence</summary>
		DicomConstTags.DisplaySetsSequence = 0x00720200;

		/// <summary>(0072,0202) VR=US VM=1 Display Set Number</summary>
		DicomConstTags.DisplaySetNumber = 0x00720202;

		/// <summary>(0072,0203) VR=LO VM=1 Display Set Label</summary>
		DicomConstTags.DisplaySetLabel = 0x00720203;

		/// <summary>(0072,0204) VR=US VM=1 Display Set Presentation Group</summary>
		DicomConstTags.DisplaySetPresentationGroup = 0x00720204;

		/// <summary>(0072,0206) VR=LO VM=1 Display Set Presentation Group Description</summary>
		DicomConstTags.DisplaySetPresentationGroupDescription = 0x00720206;

		/// <summary>(0072,0208) VR=CS VM=1 Partial Data Display Handling</summary>
		DicomConstTags.PartialDataDisplayHandling = 0x00720208;

		/// <summary>(0072,0210) VR=SQ VM=1 Synchronized Scrolling Sequence</summary>
		DicomConstTags.SynchronizedScrollingSequence = 0x00720210;

		/// <summary>(0072,0212) VR=US VM=2-n Display Set Scrolling Group</summary>
		DicomConstTags.DisplaySetScrollingGroup = 0x00720212;

		/// <summary>(0072,0214) VR=SQ VM=1 Navigation Indicator Sequence</summary>
		DicomConstTags.NavigationIndicatorSequence = 0x00720214;

		/// <summary>(0072,0216) VR=US VM=1 Navigation Display Set</summary>
		DicomConstTags.NavigationDisplaySet = 0x00720216;

		/// <summary>(0072,0218) VR=US VM=1-n Reference Display Sets</summary>
		DicomConstTags.ReferenceDisplaySets = 0x00720218;

		/// <summary>(0072,0300) VR=SQ VM=1 Image Boxes Sequence</summary>
		DicomConstTags.ImageBoxesSequence = 0x00720300;

		/// <summary>(0072,0302) VR=US VM=1 Image Box Number</summary>
		DicomConstTags.ImageBoxNumber = 0x00720302;

		/// <summary>(0072,0304) VR=CS VM=1 Image Box Layout Type</summary>
		DicomConstTags.ImageBoxLayoutType = 0x00720304;

		/// <summary>(0072,0306) VR=US VM=1 Image Box Tile Horizontal Dimension</summary>
		DicomConstTags.ImageBoxTileHorizontalDimension = 0x00720306;

		/// <summary>(0072,0308) VR=US VM=1 Image Box Tile Vertical Dimension</summary>
		DicomConstTags.ImageBoxTileVerticalDimension = 0x00720308;

		/// <summary>(0072,0310) VR=CS VM=1 Image Box Scroll Direction</summary>
		DicomConstTags.ImageBoxScrollDirection = 0x00720310;

		/// <summary>(0072,0312) VR=CS VM=1 Image Box Small Scroll Type</summary>
		DicomConstTags.ImageBoxSmallScrollType = 0x00720312;

		/// <summary>(0072,0314) VR=US VM=1 Image Box Small Scroll Amount</summary>
		DicomConstTags.ImageBoxSmallScrollAmount = 0x00720314;

		/// <summary>(0072,0316) VR=CS VM=1 Image Box Large Scroll Type</summary>
		DicomConstTags.ImageBoxLargeScrollType = 0x00720316;

		/// <summary>(0072,0318) VR=US VM=1 Image Box Large Scroll Amount</summary>
		DicomConstTags.ImageBoxLargeScrollAmount = 0x00720318;

		/// <summary>(0072,0320) VR=US VM=1 Image Box Overlap Priority</summary>
		DicomConstTags.ImageBoxOverlapPriority = 0x00720320;

		/// <summary>(0072,0330) VR=FD VM=1 Cine Relative to Real-Time</summary>
		DicomConstTags.CineRelativeToRealTime = 0x00720330;

		/// <summary>(0072,0400) VR=SQ VM=1 Filter Operations Sequence</summary>
		DicomConstTags.FilterOperationsSequence = 0x00720400;

		/// <summary>(0072,0402) VR=CS VM=1 Filter-by Category</summary>
		DicomConstTags.FilterbyCategory = 0x00720402;

		/// <summary>(0072,0404) VR=CS VM=1 Filter-by Attribute Presence</summary>
		DicomConstTags.FilterbyAttributePresence = 0x00720404;

		/// <summary>(0072,0406) VR=CS VM=1 Filter-by Operator</summary>
		DicomConstTags.FilterbyOperator = 0x00720406;

		/// <summary>(0072,0500) VR=CS VM=1 Blending Operation Type</summary>
		DicomConstTags.BlendingOperationType = 0x00720500;

		/// <summary>(0072,0510) VR=CS VM=1 Reformatting Operation Type</summary>
		DicomConstTags.ReformattingOperationType = 0x00720510;

		/// <summary>(0072,0512) VR=FD VM=1 Reformatting Thickness</summary>
		DicomConstTags.ReformattingThickness = 0x00720512;

		/// <summary>(0072,0514) VR=FD VM=1 Reformatting Interval</summary>
		DicomConstTags.ReformattingInterval = 0x00720514;

		/// <summary>(0072,0516) VR=CS VM=1 Reformatting Operation Initial View Direction</summary>
		DicomConstTags.ReformattingOperationInitialViewDirection = 0x00720516;

		/// <summary>(0072,0520) VR=CS VM=1-n 3D Rendering Type</summary>
		DicomConstTags.RenderingType3D = 0x00720520;

		/// <summary>(0072,0600) VR=SQ VM=1 Sorting Operations Sequence</summary>
		DicomConstTags.SortingOperationsSequence = 0x00720600;

		/// <summary>(0072,0602) VR=CS VM=1 Sort-by Category</summary>
		DicomConstTags.SortbyCategory = 0x00720602;

		/// <summary>(0072,0604) VR=CS VM=1 Sorting Direction</summary>
		DicomConstTags.SortingDirection = 0x00720604;

		/// <summary>(0072,0700) VR=CS VM=2 Display Set Patient Orientation</summary>
		DicomConstTags.DisplaySetPatientOrientation = 0x00720700;

		/// <summary>(0072,0702) VR=CS VM=1 VOI Type</summary>
		DicomConstTags.VOIType = 0x00720702;

		/// <summary>(0072,0704) VR=CS VM=1 Pseudo-color Type</summary>
		DicomConstTags.PseudocolorType = 0x00720704;

		/// <summary>(0072,0706) VR=CS VM=1 Show Grayscale Inverted</summary>
		DicomConstTags.ShowGrayscaleInverted = 0x00720706;

		/// <summary>(0072,0710) VR=CS VM=1 Show Image True Size Flag</summary>
		DicomConstTags.ShowImageTrueSizeFlag = 0x00720710;

		/// <summary>(0072,0712) VR=CS VM=1 Show Graphic Annotation Flag</summary>
		DicomConstTags.ShowGraphicAnnotationFlag = 0x00720712;

		/// <summary>(0072,0714) VR=CS VM=1 Show Patient Demographics Flag</summary>
		DicomConstTags.ShowPatientDemographicsFlag = 0x00720714;

		/// <summary>(0072,0716) VR=CS VM=1 Show Acquisition Techniques Flag</summary>
		DicomConstTags.ShowAcquisitionTechniquesFlag = 0x00720716;

		/// <summary>(0072,0717) VR=CS VM=1 Display Set Horizontal Justification</summary>
		DicomConstTags.DisplaySetHorizontalJustification = 0x00720717;

		/// <summary>(0072,0718) VR=CS VM=1 Display Set Vertical Justification</summary>
		DicomConstTags.DisplaySetVerticalJustification = 0x00720718;

		/// <summary>(0074,1000) VR=CS VM=1 Unified Procedure Step State</summary>
		DicomConstTags.UnifiedProcedureStepState = 0x00741000;

		/// <summary>(0074,1002) VR=SQ VM=1 UPS Progress Information Sequence</summary>
		DicomConstTags.UPSProgressInformationSequence = 0x00741002;

		/// <summary>(0074,1004) VR=DS VM=1 Unified Procedure Step Progress</summary>
		DicomConstTags.UnifiedProcedureStepProgress = 0x00741004;

		/// <summary>(0074,1006) VR=ST VM=1 Unified Procedure Step Progress Description</summary>
		DicomConstTags.UnifiedProcedureStepProgressDescription = 0x00741006;

		/// <summary>(0074,1008) VR=SQ VM=1 Unified Procedure Step Communications URI Sequence</summary>
		DicomConstTags.UnifiedProcedureStepCommunicationsURISequence = 0x00741008;

		/// <summary>(0074,100a) VR=ST VM=1 Contact URI</summary>
		DicomConstTags.ContactURI = 0x0074100a;

		/// <summary>(0074,100c) VR=LO VM=1 Contact Display Name</summary>
		DicomConstTags.ContactDisplayName = 0x0074100c;

		/// <summary>(0074,100e) VR=SQ VM=1 Unified Procedure Step Discontinuation Reason Code Sequence</summary>
		DicomConstTags.UnifiedProcedureStepDiscontinuationReasonCodeSequence = 0x0074100e;

		/// <summary>(0074,1020) VR=SQ VM=1 Beam Task Sequence</summary>
		DicomConstTags.BeamTaskSequence = 0x00741020;

		/// <summary>(0074,1022) VR=CS VM=1 Beam Task Type</summary>
		DicomConstTags.BeamTaskType = 0x00741022;

		/// <summary>(0074,1024) VR=IS VM=1 Beam Order Index</summary>
		DicomConstTags.BeamOrderIndex = 0x00741024;

		/// <summary>(0074,1030) VR=SQ VM=1 Delivery Verification Image Sequence</summary>
		DicomConstTags.DeliveryVerificationImageSequence = 0x00741030;

		/// <summary>(0074,1032) VR=CS VM=1 Verification Image Timing</summary>
		DicomConstTags.VerificationImageTiming = 0x00741032;

		/// <summary>(0074,1034) VR=CS VM=1 Double Exposure Flag</summary>
		DicomConstTags.DoubleExposureFlag = 0x00741034;

		/// <summary>(0074,1036) VR=CS VM=1 Double Exposure Ordering</summary>
		DicomConstTags.DoubleExposureOrdering = 0x00741036;

		/// <summary>(0074,1038) VR=DS VM=1 Double Exposure Meterset</summary>
		DicomConstTags.DoubleExposureMeterset = 0x00741038;

		/// <summary>(0074,103a) VR=DS VM=4 Double Exposure Field Delta</summary>
		DicomConstTags.DoubleExposureFieldDelta = 0x0074103a;

		/// <summary>(0074,1040) VR=SQ VM=1 Related Reference RT Image Sequence</summary>
		DicomConstTags.RelatedReferenceRTImageSequence = 0x00741040;

		/// <summary>(0074,1042) VR=SQ VM=1 General Machine Verification Sequence</summary>
		DicomConstTags.GeneralMachineVerificationSequence = 0x00741042;

		/// <summary>(0074,1044) VR=SQ VM=1 Conventional Machine Verification Sequence</summary>
		DicomConstTags.ConventionalMachineVerificationSequence = 0x00741044;

		/// <summary>(0074,1046) VR=SQ VM=1 Ion Machine Verification Sequence</summary>
		DicomConstTags.IonMachineVerificationSequence = 0x00741046;

		/// <summary>(0074,1048) VR=SQ VM=1 Failed Attributes Sequence</summary>
		DicomConstTags.FailedAttributesSequence = 0x00741048;

		/// <summary>(0074,104a) VR=SQ VM=1 Overridden Attributes Sequence</summary>
		DicomConstTags.OverriddenAttributesSequence = 0x0074104a;

		/// <summary>(0074,104c) VR=SQ VM=1 Conventional Control Point Verification Sequence</summary>
		DicomConstTags.ConventionalControlPointVerificationSequence = 0x0074104c;

		/// <summary>(0074,104e) VR=SQ VM=1 Ion Control Point Verification Sequence</summary>
		DicomConstTags.IonControlPointVerificationSequence = 0x0074104e;

		/// <summary>(0074,1050) VR=SQ VM=1 Attribute Occurrence Sequence</summary>
		DicomConstTags.AttributeOccurrenceSequence = 0x00741050;

		/// <summary>(0074,1052) VR=AT VM=1 Attribute Occurrence Pointer</summary>
		DicomConstTags.AttributeOccurrencePointer = 0x00741052;

		/// <summary>(0074,1054) VR=UL VM=1 Attribute Item Selector</summary>
		DicomConstTags.AttributeItemSelector = 0x00741054;

		/// <summary>(0074,1056) VR=LO VM=1 Attribute Occurrence Private Creator</summary>
		DicomConstTags.AttributeOccurrencePrivateCreator = 0x00741056;

		/// <summary>(0074,1200) VR=CS VM=1 Scheduled Procedure Step Priority</summary>
		DicomConstTags.ScheduledProcedureStepPriority = 0x00741200;

		/// <summary>(0074,1202) VR=LO VM=1 Worklist Label</summary>
		DicomConstTags.WorklistLabel = 0x00741202;

		/// <summary>(0074,1204) VR=LO VM=1 Procedure Step Label</summary>
		DicomConstTags.ProcedureStepLabel = 0x00741204;

		/// <summary>(0074,1210) VR=SQ VM=1 Scheduled Processing Parameters Sequence</summary>
		DicomConstTags.ScheduledProcessingParametersSequence = 0x00741210;

		/// <summary>(0074,1212) VR=SQ VM=1 Performed Processing Parameters Sequence</summary>
		DicomConstTags.PerformedProcessingParametersSequence = 0x00741212;

		/// <summary>(0074,1216) VR=SQ VM=1 UPS Performed Procedure Sequence</summary>
		DicomConstTags.UPSPerformedProcedureSequence = 0x00741216;

		/// <summary>(0074,1220) VR=SQ VM=1 Related Procedure Step Sequence</summary>
		DicomConstTags.RelatedProcedureStepSequence = 0x00741220;

		/// <summary>(0074,1222) VR=LO VM=1 Procedure Step Relationship Type</summary>
		DicomConstTags.ProcedureStepRelationshipType = 0x00741222;

		/// <summary>(0074,1230) VR=LO VM=1 Deletion Lock</summary>
		DicomConstTags.DeletionLock = 0x00741230;

		/// <summary>(0074,1234) VR=AE VM=1 Receiving AE</summary>
		DicomConstTags.ReceivingAE = 0x00741234;

		/// <summary>(0074,1236) VR=AE VM=1 Requesting AE</summary>
		DicomConstTags.RequestingAE = 0x00741236;

		/// <summary>(0074,1238) VR=LT VM=1 Reason for Cancellation</summary>
		DicomConstTags.ReasonForCancellation = 0x00741238;

		/// <summary>(0074,1242) VR=CS VM=1 SCP Status</summary>
		DicomConstTags.SCPStatus = 0x00741242;

		/// <summary>(0074,1244) VR=CS VM=1 Subscription List Status</summary>
		DicomConstTags.SubscriptionListStatus = 0x00741244;

		/// <summary>(0074,1246) VR=CS VM=1 UPS List Status</summary>
		DicomConstTags.UPSListStatus = 0x00741246;

		/// <summary>(0088,0130) VR=SH VM=1 Storage Media File-set ID</summary>
		DicomConstTags.StorageMediaFilesetID = 0x00880130;

		/// <summary>(0088,0140) VR=UI VM=1 Storage Media File-set UID</summary>
		DicomConstTags.StorageMediaFilesetUID = 0x00880140;

		/// <summary>(0088,0200) VR=SQ VM=1 Icon Image Sequence</summary>
		DicomConstTags.IconImageSequence = 0x00880200;

		/// <summary>(0100,0410) VR=CS VM=1 SOP Instance Status</summary>
		DicomConstTags.SOPInstanceStatus = 0x01000410;

		/// <summary>(0100,0420) VR=DT VM=1 SOP Authorization Date and Time</summary>
		DicomConstTags.SOPAuthorizationDateAndTime = 0x01000420;

		/// <summary>(0100,0424) VR=LT VM=1 SOP Authorization Comment</summary>
		DicomConstTags.SOPAuthorizationComment = 0x01000424;

		/// <summary>(0100,0426) VR=LO VM=1 Authorization Equipment Certification Number</summary>
		DicomConstTags.AuthorizationEquipmentCertificationNumber = 0x01000426;

		/// <summary>(0400,0005) VR=US VM=1 MAC ID Number</summary>
		DicomConstTags.MACIDNumber = 0x04000005;

		/// <summary>(0400,0010) VR=UI VM=1 MAC Calculation Transfer Syntax UID</summary>
		DicomConstTags.MACCalculationTransferSyntaxUID = 0x04000010;

		/// <summary>(0400,0015) VR=CS VM=1 MAC Algorithm</summary>
		DicomConstTags.MACAlgorithm = 0x04000015;

		/// <summary>(0400,0020) VR=AT VM=1-n Data Elements Signed</summary>
		DicomConstTags.DataElementsSigned = 0x04000020;

		/// <summary>(0400,0100) VR=UI VM=1 Digital Signature UID</summary>
		DicomConstTags.DigitalSignatureUID = 0x04000100;

		/// <summary>(0400,0105) VR=DT VM=1 Digital Signature DateTime</summary>
		DicomConstTags.DigitalSignatureDateTime = 0x04000105;

		/// <summary>(0400,0110) VR=CS VM=1 Certificate Type</summary>
		DicomConstTags.CertificateType = 0x04000110;

		/// <summary>(0400,0115) VR=OB VM=1 Certificate of Signer</summary>
		DicomConstTags.CertificateOfSigner = 0x04000115;

		/// <summary>(0400,0120) VR=OB VM=1 Signature</summary>
		DicomConstTags.Signature = 0x04000120;

		/// <summary>(0400,0305) VR=CS VM=1 Certified Timestamp Type</summary>
		DicomConstTags.CertifiedTimestampType = 0x04000305;

		/// <summary>(0400,0310) VR=OB VM=1 Certified Timestamp</summary>
		DicomConstTags.CertifiedTimestamp = 0x04000310;

		/// <summary>(0400,0401) VR=SQ VM=1 Digital Signature Purpose Code Sequence</summary>
		DicomConstTags.DigitalSignaturePurposeCodeSequence = 0x04000401;

		/// <summary>(0400,0402) VR=SQ VM=1 Referenced Digital Signature Sequence</summary>
		DicomConstTags.ReferencedDigitalSignatureSequence = 0x04000402;

		/// <summary>(0400,0403) VR=SQ VM=1 Referenced SOP Instance MAC Sequence</summary>
		DicomConstTags.ReferencedSOPInstanceMACSequence = 0x04000403;

		/// <summary>(0400,0404) VR=OB VM=1 MAC</summary>
		DicomConstTags.MAC = 0x04000404;

		/// <summary>(0400,0500) VR=SQ VM=1 Encrypted Attributes Sequence</summary>
		DicomConstTags.EncryptedAttributesSequence = 0x04000500;

		/// <summary>(0400,0510) VR=UI VM=1 Encrypted Content Transfer Syntax UID</summary>
		DicomConstTags.EncryptedContentTransferSyntaxUID = 0x04000510;

		/// <summary>(0400,0520) VR=OB VM=1 Encrypted Content</summary>
		DicomConstTags.EncryptedContent = 0x04000520;

		/// <summary>(0400,0550) VR=SQ VM=1 Modified Attributes Sequence</summary>
		DicomConstTags.ModifiedAttributesSequence = 0x04000550;

		/// <summary>(0400,0561) VR=SQ VM=1 Original Attributes Sequence</summary>
		DicomConstTags.OriginalAttributesSequence = 0x04000561;

		/// <summary>(0400,0562) VR=DT VM=1 Attribute Modification DateTime</summary>
		DicomConstTags.AttributeModificationDateTime = 0x04000562;

		/// <summary>(0400,0563) VR=LO VM=1 Modifying System</summary>
		DicomConstTags.ModifyingSystem = 0x04000563;

		/// <summary>(0400,0564) VR=LO VM=1 Source of Previous Values</summary>
		DicomConstTags.SourceOfPreviousValues = 0x04000564;

		/// <summary>(0400,0565) VR=CS VM=1 Reason for the Attribute Modification</summary>
		DicomConstTags.ReasonForTheAttributeModification = 0x04000565;

		/// <summary>(2000,0010) VR=IS VM=1 Number of Copies</summary>
		DicomConstTags.NumberOfCopies = 0x20000010;

		/// <summary>(2000,001e) VR=SQ VM=1 Printer Configuration Sequence</summary>
		DicomConstTags.PrinterConfigurationSequence = 0x2000001e;

		/// <summary>(2000,0020) VR=CS VM=1 Print Priority</summary>
		DicomConstTags.PrintPriority = 0x20000020;

		/// <summary>(2000,0030) VR=CS VM=1 Medium Type</summary>
		DicomConstTags.MediumType = 0x20000030;

		/// <summary>(2000,0040) VR=CS VM=1 Film Destination</summary>
		DicomConstTags.FilmDestination = 0x20000040;

		/// <summary>(2000,0050) VR=LO VM=1 Film Session Label</summary>
		DicomConstTags.FilmSessionLabel = 0x20000050;

		/// <summary>(2000,0060) VR=IS VM=1 Memory Allocation</summary>
		DicomConstTags.MemoryAllocation = 0x20000060;

		/// <summary>(2000,0061) VR=IS VM=1 Maximum Memory Allocation</summary>
		DicomConstTags.MaximumMemoryAllocation = 0x20000061;

		/// <summary>(2000,00a0) VR=US VM=1 Memory Bit Depth</summary>
		DicomConstTags.MemoryBitDepth = 0x200000a0;

		/// <summary>(2000,00a1) VR=US VM=1 Printing Bit Depth</summary>
		DicomConstTags.PrintingBitDepth = 0x200000a1;

		/// <summary>(2000,00a2) VR=SQ VM=1 Media Installed Sequence</summary>
		DicomConstTags.MediaInstalledSequence = 0x200000a2;

		/// <summary>(2000,00a4) VR=SQ VM=1 Other Media Available Sequence</summary>
		DicomConstTags.OtherMediaAvailableSequence = 0x200000a4;

		/// <summary>(2000,00a8) VR=SQ VM=1 Supported Image Display Formats Sequence</summary>
		DicomConstTags.SupportedImageDisplayFormatsSequence = 0x200000a8;

		/// <summary>(2000,0500) VR=SQ VM=1 Referenced Film Box Sequence</summary>
		DicomConstTags.ReferencedFilmBoxSequence = 0x20000500;

		/// <summary>(2010,0010) VR=ST VM=1 Image Display Format</summary>
		DicomConstTags.ImageDisplayFormat = 0x20100010;

		/// <summary>(2010,0030) VR=CS VM=1 Annotation Display Format ID</summary>
		DicomConstTags.AnnotationDisplayFormatID = 0x20100030;

		/// <summary>(2010,0040) VR=CS VM=1 Film Orientation</summary>
		DicomConstTags.FilmOrientation = 0x20100040;

		/// <summary>(2010,0050) VR=CS VM=1 Film Size ID</summary>
		DicomConstTags.FilmSizeID = 0x20100050;

		/// <summary>(2010,0052) VR=CS VM=1 Printer Resolution ID</summary>
		DicomConstTags.PrinterResolutionID = 0x20100052;

		/// <summary>(2010,0054) VR=CS VM=1 Default Printer Resolution ID</summary>
		DicomConstTags.DefaultPrinterResolutionID = 0x20100054;

		/// <summary>(2010,0060) VR=CS VM=1 Magnification Type</summary>
		DicomConstTags.MagnificationType = 0x20100060;

		/// <summary>(2010,0080) VR=CS VM=1 Smoothing Type</summary>
		DicomConstTags.SmoothingType = 0x20100080;

		/// <summary>(2010,00a6) VR=CS VM=1 Default Magnification Type</summary>
		DicomConstTags.DefaultMagnificationType = 0x201000a6;

		/// <summary>(2010,00a7) VR=CS VM=1-n Other Magnification Types Available</summary>
		DicomConstTags.OtherMagnificationTypesAvailable = 0x201000a7;

		/// <summary>(2010,00a8) VR=CS VM=1 Default Smoothing Type</summary>
		DicomConstTags.DefaultSmoothingType = 0x201000a8;

		/// <summary>(2010,00a9) VR=CS VM=1-n Other Smoothing Types Available</summary>
		DicomConstTags.OtherSmoothingTypesAvailable = 0x201000a9;

		/// <summary>(2010,0100) VR=CS VM=1 Border Density</summary>
		DicomConstTags.BorderDensity = 0x20100100;

		/// <summary>(2010,0110) VR=CS VM=1 Empty Image Density</summary>
		DicomConstTags.EmptyImageDensity = 0x20100110;

		/// <summary>(2010,0120) VR=US VM=1 Min Density</summary>
		DicomConstTags.MinDensity = 0x20100120;

		/// <summary>(2010,0130) VR=US VM=1 Max Density</summary>
		DicomConstTags.MaxDensity = 0x20100130;

		/// <summary>(2010,0140) VR=CS VM=1 Trim</summary>
		DicomConstTags.Trim = 0x20100140;

		/// <summary>(2010,0150) VR=ST VM=1 Configuration Information</summary>
		DicomConstTags.ConfigurationInformation = 0x20100150;

		/// <summary>(2010,0152) VR=LT VM=1 Configuration Information Description</summary>
		DicomConstTags.ConfigurationInformationDescription = 0x20100152;

		/// <summary>(2010,0154) VR=IS VM=1 Maximum Collated Films</summary>
		DicomConstTags.MaximumCollatedFilms = 0x20100154;

		/// <summary>(2010,015e) VR=US VM=1 Illumination</summary>
		DicomConstTags.Illumination = 0x2010015e;

		/// <summary>(2010,0160) VR=US VM=1 Reflected Ambient Light</summary>
		DicomConstTags.ReflectedAmbientLight = 0x20100160;

		/// <summary>(2010,0376) VR=DS VM=2 Printer Pixel Spacing</summary>
		DicomConstTags.PrinterPixelSpacing = 0x20100376;

		/// <summary>(2010,0500) VR=SQ VM=1 Referenced Film Session Sequence</summary>
		DicomConstTags.ReferencedFilmSessionSequence = 0x20100500;

		/// <summary>(2010,0510) VR=SQ VM=1 Referenced Image Box Sequence</summary>
		DicomConstTags.ReferencedImageBoxSequence = 0x20100510;

		/// <summary>(2010,0520) VR=SQ VM=1 Referenced Basic Annotation Box Sequence</summary>
		DicomConstTags.ReferencedBasicAnnotationBoxSequence = 0x20100520;

		/// <summary>(2020,0010) VR=US VM=1 Image Box Position</summary>
		DicomConstTags.ImageBoxPosition = 0x20200010;

		/// <summary>(2020,0020) VR=CS VM=1 Polarity</summary>
		DicomConstTags.Polarity = 0x20200020;

		/// <summary>(2020,0030) VR=DS VM=1 Requested Image Size</summary>
		DicomConstTags.RequestedImageSize = 0x20200030;

		/// <summary>(2020,0040) VR=CS VM=1 Requested Decimate/Crop Behavior</summary>
		DicomConstTags.RequestedDecimateCropBehavior = 0x20200040;

		/// <summary>(2020,0050) VR=CS VM=1 Requested Resolution ID</summary>
		DicomConstTags.RequestedResolutionID = 0x20200050;

		/// <summary>(2020,00a0) VR=CS VM=1 Requested Image Size Flag</summary>
		DicomConstTags.RequestedImageSizeFlag = 0x202000a0;

		/// <summary>(2020,00a2) VR=CS VM=1 Decimate/Crop Result</summary>
		DicomConstTags.DecimateCropResult = 0x202000a2;

		/// <summary>(2020,0110) VR=SQ VM=1 Basic Grayscale Image Sequence</summary>
		DicomConstTags.BasicGrayscaleImageSequence = 0x20200110;

		/// <summary>(2020,0111) VR=SQ VM=1 Basic Color Image Sequence</summary>
		DicomConstTags.BasicColorImageSequence = 0x20200111;

		/// <summary>(2030,0010) VR=US VM=1 Annotation Position</summary>
		DicomConstTags.AnnotationPosition = 0x20300010;

		/// <summary>(2030,0020) VR=LO VM=1 Text String</summary>
		DicomConstTags.TextString = 0x20300020;

		/// <summary>(2050,0010) VR=SQ VM=1 Presentation LUT Sequence</summary>
		DicomConstTags.PresentationLUTSequence = 0x20500010;

		/// <summary>(2050,0020) VR=CS VM=1 Presentation LUT Shape</summary>
		DicomConstTags.PresentationLUTShape = 0x20500020;

		/// <summary>(2050,0500) VR=SQ VM=1 Referenced Presentation LUT Sequence</summary>
		DicomConstTags.ReferencedPresentationLUTSequence = 0x20500500;

		/// <summary>(2100,0020) VR=CS VM=1 Execution Status</summary>
		DicomConstTags.ExecutionStatus = 0x21000020;

		/// <summary>(2100,0030) VR=CS VM=1 Execution Status Info</summary>
		DicomConstTags.ExecutionStatusInfo = 0x21000030;

		/// <summary>(2100,0040) VR=DA VM=1 Creation Date</summary>
		DicomConstTags.CreationDate = 0x21000040;

		/// <summary>(2100,0050) VR=TM VM=1 Creation Time</summary>
		DicomConstTags.CreationTime = 0x21000050;

		/// <summary>(2100,0070) VR=AE VM=1 Originator</summary>
		DicomConstTags.Originator = 0x21000070;

		/// <summary>(2100,0160) VR=SH VM=1 Owner ID</summary>
		DicomConstTags.OwnerID = 0x21000160;

		/// <summary>(2100,0170) VR=IS VM=1 Number of Films</summary>
		DicomConstTags.NumberOfFilms = 0x21000170;

		/// <summary>(2110,0010) VR=CS VM=1 Printer Status</summary>
		DicomConstTags.PrinterStatus = 0x21100010;

		/// <summary>(2110,0020) VR=CS VM=1 Printer Status Info</summary>
		DicomConstTags.PrinterStatusInfo = 0x21100020;

		/// <summary>(2110,0030) VR=LO VM=1 Printer Name</summary>
		DicomConstTags.PrinterName = 0x21100030;

		/// <summary>(2200,0001) VR=CS VM=1 Label Using Information Extracted From Instances</summary>
		DicomConstTags.LabelUsingInformationExtractedFromInstances = 0x22000001;

		/// <summary>(2200,0002) VR=UT VM=1 Label Text</summary>
		DicomConstTags.LabelText = 0x22000002;

		/// <summary>(2200,0003) VR=CS VM=1 Label Style Selection</summary>
		DicomConstTags.LabelStyleSelection = 0x22000003;

		/// <summary>(2200,0004) VR=LT VM=1 Media Disposition</summary>
		DicomConstTags.MediaDisposition = 0x22000004;

		/// <summary>(2200,0005) VR=LT VM=1 Barcode Value</summary>
		DicomConstTags.BarcodeValue = 0x22000005;

		/// <summary>(2200,0006) VR=CS VM=1 Barcode Symbology</summary>
		DicomConstTags.BarcodeSymbology = 0x22000006;

		/// <summary>(2200,0007) VR=CS VM=1 Allow Media Splitting</summary>
		DicomConstTags.AllowMediaSplitting = 0x22000007;

		/// <summary>(2200,0008) VR=CS VM=1 Include Non-DICOM Objects</summary>
		DicomConstTags.IncludeNonDICOMObjects = 0x22000008;

		/// <summary>(2200,0009) VR=CS VM=1 Include Display Application</summary>
		DicomConstTags.IncludeDisplayApplication = 0x22000009;

		/// <summary>(2200,000a) VR=CS VM=1 Preserve Composite Instances After Media Creation</summary>
		DicomConstTags.PreserveCompositeInstancesAfterMediaCreation = 0x2200000a;

		/// <summary>(2200,000b) VR=US VM=1 Total Number of Pieces of Media Created</summary>
		DicomConstTags.TotalNumberOfPiecesOfMediaCreated = 0x2200000b;

		/// <summary>(2200,000c) VR=LO VM=1 Requested Media Application Profile</summary>
		DicomConstTags.RequestedMediaApplicationProfile = 0x2200000c;

		/// <summary>(2200,000d) VR=SQ VM=1 Referenced Storage Media Sequence</summary>
		DicomConstTags.ReferencedStorageMediaSequence = 0x2200000d;

		/// <summary>(2200,000e) VR=AT VM=1-n Failure Attributes</summary>
		DicomConstTags.FailureAttributes = 0x2200000e;

		/// <summary>(2200,000f) VR=CS VM=1 Allow Lossy Compression</summary>
		DicomConstTags.AllowLossyCompression = 0x2200000f;

		/// <summary>(2200,0020) VR=CS VM=1 Request Priority</summary>
		DicomConstTags.RequestPriority = 0x22000020;

		/// <summary>(3002,0002) VR=SH VM=1 RT Image Label</summary>
		DicomConstTags.RTImageLabel = 0x30020002;

		/// <summary>(3002,0003) VR=LO VM=1 RT Image Name</summary>
		DicomConstTags.RTImageName = 0x30020003;

		/// <summary>(3002,0004) VR=ST VM=1 RT Image Description</summary>
		DicomConstTags.RTImageDescription = 0x30020004;

		/// <summary>(3002,000a) VR=CS VM=1 Reported Values Origin</summary>
		DicomConstTags.ReportedValuesOrigin = 0x3002000a;

		/// <summary>(3002,000c) VR=CS VM=1 RT Image Plane</summary>
		DicomConstTags.RTImagePlane = 0x3002000c;

		/// <summary>(3002,000d) VR=DS VM=3 X-Ray Image Receptor Translation</summary>
		DicomConstTags.XRayImageReceptorTranslation = 0x3002000d;

		/// <summary>(3002,000e) VR=DS VM=1 X-Ray Image Receptor Angle</summary>
		DicomConstTags.XRayImageReceptorAngle = 0x3002000e;

		/// <summary>(3002,0010) VR=DS VM=6 RT Image Orientation</summary>
		DicomConstTags.RTImageOrientation = 0x30020010;

		/// <summary>(3002,0011) VR=DS VM=2 Image Plane Pixel Spacing</summary>
		DicomConstTags.ImagePlanePixelSpacing = 0x30020011;

		/// <summary>(3002,0012) VR=DS VM=2 RT Image Position</summary>
		DicomConstTags.RTImagePosition = 0x30020012;

		/// <summary>(3002,0020) VR=SH VM=1 Radiation Machine Name</summary>
		DicomConstTags.RadiationMachineName = 0x30020020;

		/// <summary>(3002,0022) VR=DS VM=1 Radiation Machine SAD</summary>
		DicomConstTags.RadiationMachineSAD = 0x30020022;

		/// <summary>(3002,0024) VR=DS VM=1 Radiation Machine SSD</summary>
		DicomConstTags.RadiationMachineSSD = 0x30020024;

		/// <summary>(3002,0026) VR=DS VM=1 RT Image SID</summary>
		DicomConstTags.RTImageSID = 0x30020026;

		/// <summary>(3002,0028) VR=DS VM=1 Source to Reference Object Distance</summary>
		DicomConstTags.SourceToReferenceObjectDistance = 0x30020028;

		/// <summary>(3002,0029) VR=IS VM=1 Fraction Number</summary>
		DicomConstTags.FractionNumber = 0x30020029;

		/// <summary>(3002,0030) VR=SQ VM=1 Exposure Sequence</summary>
		DicomConstTags.ExposureSequence = 0x30020030;

		/// <summary>(3002,0032) VR=DS VM=1 Meterset Exposure</summary>
		DicomConstTags.MetersetExposure = 0x30020032;

		/// <summary>(3002,0034) VR=DS VM=4 Diaphragm Position</summary>
		DicomConstTags.DiaphragmPosition = 0x30020034;

		/// <summary>(3002,0040) VR=SQ VM=1 Fluence Map Sequence</summary>
		DicomConstTags.FluenceMapSequence = 0x30020040;

		/// <summary>(3002,0041) VR=CS VM=1 Fluence Data Source</summary>
		DicomConstTags.FluenceDataSource = 0x30020041;

		/// <summary>(3002,0042) VR=DS VM=1 Fluence Data Scale</summary>
		DicomConstTags.FluenceDataScale = 0x30020042;

		/// <summary>(3004,0001) VR=CS VM=1 DVH Type</summary>
		DicomConstTags.DVHType = 0x30040001;

		/// <summary>(3004,0002) VR=CS VM=1 Dose Units</summary>
		DicomConstTags.DoseUnits = 0x30040002;

		/// <summary>(3004,0004) VR=CS VM=1 Dose Type</summary>
		DicomConstTags.DoseType = 0x30040004;

		/// <summary>(3004,0006) VR=LO VM=1 Dose Comment</summary>
		DicomConstTags.DoseComment = 0x30040006;

		/// <summary>(3004,0008) VR=DS VM=3 Normalization Point</summary>
		DicomConstTags.NormalizationPoint = 0x30040008;

		/// <summary>(3004,000a) VR=CS VM=1 Dose Summation Type</summary>
		DicomConstTags.DoseSummationType = 0x3004000a;

		/// <summary>(3004,000c) VR=DS VM=2-n Grid Frame Offset Vector</summary>
		DicomConstTags.GridFrameOffsetVector = 0x3004000c;

		/// <summary>(3004,000e) VR=DS VM=1 Dose Grid Scaling</summary>
		DicomConstTags.DoseGridScaling = 0x3004000e;

		/// <summary>(3004,0010) VR=SQ VM=1 RT Dose ROI Sequence</summary>
		DicomConstTags.RTDoseROISequence = 0x30040010;

		/// <summary>(3004,0012) VR=DS VM=1 Dose Value</summary>
		DicomConstTags.DoseValue = 0x30040012;

		/// <summary>(3004,0014) VR=CS VM=1-3 Tissue Heterogeneity Correction</summary>
		DicomConstTags.TissueHeterogeneityCorrection = 0x30040014;

		/// <summary>(3004,0040) VR=DS VM=3 DVH Normalization Point</summary>
		DicomConstTags.DVHNormalizationPoint = 0x30040040;

		/// <summary>(3004,0042) VR=DS VM=1 DVH Normalization Dose Value</summary>
		DicomConstTags.DVHNormalizationDoseValue = 0x30040042;

		/// <summary>(3004,0050) VR=SQ VM=1 DVH Sequence</summary>
		DicomConstTags.DVHSequence = 0x30040050;

		/// <summary>(3004,0052) VR=DS VM=1 DVH Dose Scaling</summary>
		DicomConstTags.DVHDoseScaling = 0x30040052;

		/// <summary>(3004,0054) VR=CS VM=1 DVH Volume Units</summary>
		DicomConstTags.DVHVolumeUnits = 0x30040054;

		/// <summary>(3004,0056) VR=IS VM=1 DVH Number of Bins</summary>
		DicomConstTags.DVHNumberOfBins = 0x30040056;

		/// <summary>(3004,0058) VR=DS VM=2-2n DVH Data</summary>
		DicomConstTags.DVHData = 0x30040058;

		/// <summary>(3004,0060) VR=SQ VM=1 DVH Referenced ROI Sequence</summary>
		DicomConstTags.DVHReferencedROISequence = 0x30040060;

		/// <summary>(3004,0062) VR=CS VM=1 DVH ROI Contribution Type</summary>
		DicomConstTags.DVHROIContributionType = 0x30040062;

		/// <summary>(3004,0070) VR=DS VM=1 DVH Minimum Dose</summary>
		DicomConstTags.DVHMinimumDose = 0x30040070;

		/// <summary>(3004,0072) VR=DS VM=1 DVH Maximum Dose</summary>
		DicomConstTags.DVHMaximumDose = 0x30040072;

		/// <summary>(3004,0074) VR=DS VM=1 DVH Mean Dose</summary>
		DicomConstTags.DVHMeanDose = 0x30040074;

		/// <summary>(3006,0002) VR=SH VM=1 Structure Set Label</summary>
		DicomConstTags.StructureSetLabel = 0x30060002;

		/// <summary>(3006,0004) VR=LO VM=1 Structure Set Name</summary>
		DicomConstTags.StructureSetName = 0x30060004;

		/// <summary>(3006,0006) VR=ST VM=1 Structure Set Description</summary>
		DicomConstTags.StructureSetDescription = 0x30060006;

		/// <summary>(3006,0008) VR=DA VM=1 Structure Set Date</summary>
		DicomConstTags.StructureSetDate = 0x30060008;

		/// <summary>(3006,0009) VR=TM VM=1 Structure Set Time</summary>
		DicomConstTags.StructureSetTime = 0x30060009;

		/// <summary>(3006,0010) VR=SQ VM=1 Referenced Frame of Reference Sequence</summary>
		DicomConstTags.ReferencedFrameOfReferenceSequence = 0x30060010;

		/// <summary>(3006,0012) VR=SQ VM=1 RT Referenced Study Sequence</summary>
		DicomConstTags.RTReferencedStudySequence = 0x30060012;

		/// <summary>(3006,0014) VR=SQ VM=1 RT Referenced Series Sequence</summary>
		DicomConstTags.RTReferencedSeriesSequence = 0x30060014;

		/// <summary>(3006,0016) VR=SQ VM=1 Contour Image Sequence</summary>
		DicomConstTags.ContourImageSequence = 0x30060016;

		/// <summary>(3006,0020) VR=SQ VM=1 Structure Set ROI Sequence</summary>
		DicomConstTags.StructureSetROISequence = 0x30060020;

		/// <summary>(3006,0022) VR=IS VM=1 ROI Number</summary>
		DicomConstTags.ROINumber = 0x30060022;

		/// <summary>(3006,0024) VR=UI VM=1 Referenced Frame of Reference UID</summary>
		DicomConstTags.ReferencedFrameOfReferenceUID = 0x30060024;

		/// <summary>(3006,0026) VR=LO VM=1 ROI Name</summary>
		DicomConstTags.ROIName = 0x30060026;

		/// <summary>(3006,0028) VR=ST VM=1 ROI Description</summary>
		DicomConstTags.ROIDescription = 0x30060028;

		/// <summary>(3006,002a) VR=IS VM=3 ROI Display Color</summary>
		DicomConstTags.ROIDisplayColor = 0x3006002a;

		/// <summary>(3006,002c) VR=DS VM=1 ROI Volume</summary>
		DicomConstTags.ROIVolume = 0x3006002c;

		/// <summary>(3006,0030) VR=SQ VM=1 RT Related ROI Sequence</summary>
		DicomConstTags.RTRelatedROISequence = 0x30060030;

		/// <summary>(3006,0033) VR=CS VM=1 RT ROI Relationship</summary>
		DicomConstTags.RTROIRelationship = 0x30060033;

		/// <summary>(3006,0036) VR=CS VM=1 ROI Generation Algorithm</summary>
		DicomConstTags.ROIGenerationAlgorithm = 0x30060036;

		/// <summary>(3006,0038) VR=LO VM=1 ROI Generation Description</summary>
		DicomConstTags.ROIGenerationDescription = 0x30060038;

		/// <summary>(3006,0039) VR=SQ VM=1 ROI Contour Sequence</summary>
		DicomConstTags.ROIContourSequence = 0x30060039;

		/// <summary>(3006,0040) VR=SQ VM=1 Contour Sequence</summary>
		DicomConstTags.ContourSequence = 0x30060040;

		/// <summary>(3006,0042) VR=CS VM=1 Contour Geometric Type</summary>
		DicomConstTags.ContourGeometricType = 0x30060042;

		/// <summary>(3006,0044) VR=DS VM=1 Contour Slab Thickness</summary>
		DicomConstTags.ContourSlabThickness = 0x30060044;

		/// <summary>(3006,0045) VR=DS VM=3 Contour Offset Vector</summary>
		DicomConstTags.ContourOffsetVector = 0x30060045;

		/// <summary>(3006,0046) VR=IS VM=1 Number of Contour Points</summary>
		DicomConstTags.NumberOfContourPoints = 0x30060046;

		/// <summary>(3006,0048) VR=IS VM=1 Contour Number</summary>
		DicomConstTags.ContourNumber = 0x30060048;

		/// <summary>(3006,0049) VR=IS VM=1-n Attached Contours</summary>
		DicomConstTags.AttachedContours = 0x30060049;

		/// <summary>(3006,0050) VR=DS VM=3-3n Contour Data</summary>
		DicomConstTags.ContourData = 0x30060050;

		/// <summary>(3006,0080) VR=SQ VM=1 RT ROI Observations Sequence</summary>
		DicomConstTags.RTROIObservationsSequence = 0x30060080;

		/// <summary>(3006,0082) VR=IS VM=1 Observation Number</summary>
		DicomConstTags.ObservationNumber = 0x30060082;

		/// <summary>(3006,0084) VR=IS VM=1 Referenced ROI Number</summary>
		DicomConstTags.ReferencedROINumber = 0x30060084;

		/// <summary>(3006,0085) VR=SH VM=1 ROI Observation Label</summary>
		DicomConstTags.ROIObservationLabel = 0x30060085;

		/// <summary>(3006,0086) VR=SQ VM=1 RT ROI Identification Code Sequence</summary>
		DicomConstTags.RTROIIdentificationCodeSequence = 0x30060086;

		/// <summary>(3006,0088) VR=ST VM=1 ROI Observation Description</summary>
		DicomConstTags.ROIObservationDescription = 0x30060088;

		/// <summary>(3006,00a0) VR=SQ VM=1 Related RT ROI Observations Sequence</summary>
		DicomConstTags.RelatedRTROIObservationsSequence = 0x300600a0;

		/// <summary>(3006,00a4) VR=CS VM=1 RT ROI Interpreted Type</summary>
		DicomConstTags.RTROIInterpretedType = 0x300600a4;

		/// <summary>(3006,00a6) VR=PN VM=1 ROI Interpreter</summary>
		DicomConstTags.ROIInterpreter = 0x300600a6;

		/// <summary>(3006,00b0) VR=SQ VM=1 ROI Physical Properties Sequence</summary>
		DicomConstTags.ROIPhysicalPropertiesSequence = 0x300600b0;

		/// <summary>(3006,00b2) VR=CS VM=1 ROI Physical Property</summary>
		DicomConstTags.ROIPhysicalProperty = 0x300600b2;

		/// <summary>(3006,00b4) VR=DS VM=1 ROI Physical Property Value</summary>
		DicomConstTags.ROIPhysicalPropertyValue = 0x300600b4;

		/// <summary>(3006,00b6) VR=SQ VM=1 ROI Elemental Composition Sequence</summary>
		DicomConstTags.ROIElementalCompositionSequence = 0x300600b6;

		/// <summary>(3006,00b7) VR=US VM=1 ROI Elemental Composition Atomic Number</summary>
		DicomConstTags.ROIElementalCompositionAtomicNumber = 0x300600b7;

		/// <summary>(3006,00b8) VR=FL VM=1 ROI Elemental Composition Atomic Mass Fraction</summary>
		DicomConstTags.ROIElementalCompositionAtomicMassFraction = 0x300600b8;

		/// <summary>(3006,00c0) VR=SQ VM=1 Frame of Reference Relationship Sequence</summary>
		DicomConstTags.FrameOfReferenceRelationshipSequence = 0x300600c0;

		/// <summary>(3006,00c2) VR=UI VM=1 Related Frame of Reference UID</summary>
		DicomConstTags.RelatedFrameOfReferenceUID = 0x300600c2;

		/// <summary>(3006,00c4) VR=CS VM=1 Frame of Reference Transformation Type</summary>
		DicomConstTags.FrameOfReferenceTransformationType = 0x300600c4;

		/// <summary>(3006,00c6) VR=DS VM=16 Frame of Reference Transformation Matrix</summary>
		DicomConstTags.FrameOfReferenceTransformationMatrix = 0x300600c6;

		/// <summary>(3006,00c8) VR=LO VM=1 Frame of Reference Transformation Comment</summary>
		DicomConstTags.FrameOfReferenceTransformationComment = 0x300600c8;

		/// <summary>(3008,0010) VR=SQ VM=1 Measured Dose Reference Sequence</summary>
		DicomConstTags.MeasuredDoseReferenceSequence = 0x30080010;

		/// <summary>(3008,0012) VR=ST VM=1 Measured Dose Description</summary>
		DicomConstTags.MeasuredDoseDescription = 0x30080012;

		/// <summary>(3008,0014) VR=CS VM=1 Measured Dose Type</summary>
		DicomConstTags.MeasuredDoseType = 0x30080014;

		/// <summary>(3008,0016) VR=DS VM=1 Measured Dose Value</summary>
		DicomConstTags.MeasuredDoseValue = 0x30080016;

		/// <summary>(3008,0020) VR=SQ VM=1 Treatment Session Beam Sequence</summary>
		DicomConstTags.TreatmentSessionBeamSequence = 0x30080020;

		/// <summary>(3008,0021) VR=SQ VM=1 Treatment Session Ion Beam Sequence</summary>
		DicomConstTags.TreatmentSessionIonBeamSequence = 0x30080021;

		/// <summary>(3008,0022) VR=IS VM=1 Current Fraction Number</summary>
		DicomConstTags.CurrentFractionNumber = 0x30080022;

		/// <summary>(3008,0024) VR=DA VM=1 Treatment Control Point Date</summary>
		DicomConstTags.TreatmentControlPointDate = 0x30080024;

		/// <summary>(3008,0025) VR=TM VM=1 Treatment Control Point Time</summary>
		DicomConstTags.TreatmentControlPointTime = 0x30080025;

		/// <summary>(3008,002a) VR=CS VM=1 Treatment Termination Status</summary>
		DicomConstTags.TreatmentTerminationStatus = 0x3008002a;

		/// <summary>(3008,002b) VR=SH VM=1 Treatment Termination Code</summary>
		DicomConstTags.TreatmentTerminationCode = 0x3008002b;

		/// <summary>(3008,002c) VR=CS VM=1 Treatment Verification Status</summary>
		DicomConstTags.TreatmentVerificationStatus = 0x3008002c;

		/// <summary>(3008,0030) VR=SQ VM=1 Referenced Treatment Record Sequence</summary>
		DicomConstTags.ReferencedTreatmentRecordSequence = 0x30080030;

		/// <summary>(3008,0032) VR=DS VM=1 Specified Primary Meterset</summary>
		DicomConstTags.SpecifiedPrimaryMeterset = 0x30080032;

		/// <summary>(3008,0033) VR=DS VM=1 Specified Secondary Meterset</summary>
		DicomConstTags.SpecifiedSecondaryMeterset = 0x30080033;

		/// <summary>(3008,0036) VR=DS VM=1 Delivered Primary Meterset</summary>
		DicomConstTags.DeliveredPrimaryMeterset = 0x30080036;

		/// <summary>(3008,0037) VR=DS VM=1 Delivered Secondary Meterset</summary>
		DicomConstTags.DeliveredSecondaryMeterset = 0x30080037;

		/// <summary>(3008,003a) VR=DS VM=1 Specified Treatment Time</summary>
		DicomConstTags.SpecifiedTreatmentTime = 0x3008003a;

		/// <summary>(3008,003b) VR=DS VM=1 Delivered Treatment Time</summary>
		DicomConstTags.DeliveredTreatmentTime = 0x3008003b;

		/// <summary>(3008,0040) VR=SQ VM=1 Control Point Delivery Sequence</summary>
		DicomConstTags.ControlPointDeliverySequence = 0x30080040;

		/// <summary>(3008,0041) VR=SQ VM=1 Ion Control Point Delivery Sequence</summary>
		DicomConstTags.IonControlPointDeliverySequence = 0x30080041;

		/// <summary>(3008,0042) VR=DS VM=1 Specified Meterset</summary>
		DicomConstTags.SpecifiedMeterset = 0x30080042;

		/// <summary>(3008,0044) VR=DS VM=1 Delivered Meterset</summary>
		DicomConstTags.DeliveredMeterset = 0x30080044;

		/// <summary>(3008,0045) VR=FL VM=1 Meterset Rate Set</summary>
		DicomConstTags.MetersetRateSet = 0x30080045;

		/// <summary>(3008,0046) VR=FL VM=1 Meterset Rate Delivered</summary>
		DicomConstTags.MetersetRateDelivered = 0x30080046;

		/// <summary>(3008,0047) VR=FL VM=1-n Scan Spot Metersets Delivered</summary>
		DicomConstTags.ScanSpotMetersetsDelivered = 0x30080047;

		/// <summary>(3008,0048) VR=DS VM=1 Dose Rate Delivered</summary>
		DicomConstTags.DoseRateDelivered = 0x30080048;

		/// <summary>(3008,0050) VR=SQ VM=1 Treatment Summary Calculated Dose Reference Sequence</summary>
		DicomConstTags.TreatmentSummaryCalculatedDoseReferenceSequence = 0x30080050;

		/// <summary>(3008,0052) VR=DS VM=1 Cumulative Dose to Dose Reference</summary>
		DicomConstTags.CumulativeDoseToDoseReference = 0x30080052;

		/// <summary>(3008,0054) VR=DA VM=1 First Treatment Date</summary>
		DicomConstTags.FirstTreatmentDate = 0x30080054;

		/// <summary>(3008,0056) VR=DA VM=1 Most Recent Treatment Date</summary>
		DicomConstTags.MostRecentTreatmentDate = 0x30080056;

		/// <summary>(3008,005a) VR=IS VM=1 Number of Fractions Delivered</summary>
		DicomConstTags.NumberOfFractionsDelivered = 0x3008005a;

		/// <summary>(3008,0060) VR=SQ VM=1 Override Sequence</summary>
		DicomConstTags.OverrideSequence = 0x30080060;

		/// <summary>(3008,0061) VR=AT VM=1 Parameter Sequence Pointer</summary>
		DicomConstTags.ParameterSequencePointer = 0x30080061;

		/// <summary>(3008,0062) VR=AT VM=1 Override Parameter Pointer</summary>
		DicomConstTags.OverrideParameterPointer = 0x30080062;

		/// <summary>(3008,0063) VR=IS VM=1 Parameter Item Index</summary>
		DicomConstTags.ParameterItemIndex = 0x30080063;

		/// <summary>(3008,0064) VR=IS VM=1 Measured Dose Reference Number</summary>
		DicomConstTags.MeasuredDoseReferenceNumber = 0x30080064;

		/// <summary>(3008,0065) VR=AT VM=1 Parameter Pointer</summary>
		DicomConstTags.ParameterPointer = 0x30080065;

		/// <summary>(3008,0066) VR=ST VM=1 Override Reason</summary>
		DicomConstTags.OverrideReason = 0x30080066;

		/// <summary>(3008,0068) VR=SQ VM=1 Corrected Parameter Sequence</summary>
		DicomConstTags.CorrectedParameterSequence = 0x30080068;

		/// <summary>(3008,006a) VR=FL VM=1 Correction Value</summary>
		DicomConstTags.CorrectionValue = 0x3008006a;

		/// <summary>(3008,0070) VR=SQ VM=1 Calculated Dose Reference Sequence</summary>
		DicomConstTags.CalculatedDoseReferenceSequence = 0x30080070;

		/// <summary>(3008,0072) VR=IS VM=1 Calculated Dose Reference Number</summary>
		DicomConstTags.CalculatedDoseReferenceNumber = 0x30080072;

		/// <summary>(3008,0074) VR=ST VM=1 Calculated Dose Reference Description</summary>
		DicomConstTags.CalculatedDoseReferenceDescription = 0x30080074;

		/// <summary>(3008,0076) VR=DS VM=1 Calculated Dose Reference Dose Value</summary>
		DicomConstTags.CalculatedDoseReferenceDoseValue = 0x30080076;

		/// <summary>(3008,0078) VR=DS VM=1 Start Meterset</summary>
		DicomConstTags.StartMeterset = 0x30080078;

		/// <summary>(3008,007a) VR=DS VM=1 End Meterset</summary>
		DicomConstTags.EndMeterset = 0x3008007a;

		/// <summary>(3008,0080) VR=SQ VM=1 Referenced Measured Dose Reference Sequence</summary>
		DicomConstTags.ReferencedMeasuredDoseReferenceSequence = 0x30080080;

		/// <summary>(3008,0082) VR=IS VM=1 Referenced Measured Dose Reference Number</summary>
		DicomConstTags.ReferencedMeasuredDoseReferenceNumber = 0x30080082;

		/// <summary>(3008,0090) VR=SQ VM=1 Referenced Calculated Dose Reference Sequence</summary>
		DicomConstTags.ReferencedCalculatedDoseReferenceSequence = 0x30080090;

		/// <summary>(3008,0092) VR=IS VM=1 Referenced Calculated Dose Reference Number</summary>
		DicomConstTags.ReferencedCalculatedDoseReferenceNumber = 0x30080092;

		/// <summary>(3008,00a0) VR=SQ VM=1 Beam Limiting Device Leaf Pairs Sequence</summary>
		DicomConstTags.BeamLimitingDeviceLeafPairsSequence = 0x300800a0;

		/// <summary>(3008,00b0) VR=SQ VM=1 Recorded Wedge Sequence</summary>
		DicomConstTags.RecordedWedgeSequence = 0x300800b0;

		/// <summary>(3008,00c0) VR=SQ VM=1 Recorded Compensator Sequence</summary>
		DicomConstTags.RecordedCompensatorSequence = 0x300800c0;

		/// <summary>(3008,00d0) VR=SQ VM=1 Recorded Block Sequence</summary>
		DicomConstTags.RecordedBlockSequence = 0x300800d0;

		/// <summary>(3008,00e0) VR=SQ VM=1 Treatment Summary Measured Dose Reference Sequence</summary>
		DicomConstTags.TreatmentSummaryMeasuredDoseReferenceSequence = 0x300800e0;

		/// <summary>(3008,00f0) VR=SQ VM=1 Recorded Snout Sequence</summary>
		DicomConstTags.RecordedSnoutSequence = 0x300800f0;

		/// <summary>(3008,00f2) VR=SQ VM=1 Recorded Range Shifter Sequence</summary>
		DicomConstTags.RecordedRangeShifterSequence = 0x300800f2;

		/// <summary>(3008,00f4) VR=SQ VM=1 Recorded Lateral Spreading Device Sequence</summary>
		DicomConstTags.RecordedLateralSpreadingDeviceSequence = 0x300800f4;

		/// <summary>(3008,00f6) VR=SQ VM=1 Recorded Range Modulator Sequence</summary>
		DicomConstTags.RecordedRangeModulatorSequence = 0x300800f6;

		/// <summary>(3008,0100) VR=SQ VM=1 Recorded Source Sequence</summary>
		DicomConstTags.RecordedSourceSequence = 0x30080100;

		/// <summary>(3008,0105) VR=LO VM=1 Source Serial Number</summary>
		DicomConstTags.SourceSerialNumber = 0x30080105;

		/// <summary>(3008,0110) VR=SQ VM=1 Treatment Session Application Setup Sequence</summary>
		DicomConstTags.TreatmentSessionApplicationSetupSequence = 0x30080110;

		/// <summary>(3008,0116) VR=CS VM=1 Application Setup Check</summary>
		DicomConstTags.ApplicationSetupCheck = 0x30080116;

		/// <summary>(3008,0120) VR=SQ VM=1 Recorded Brachy Accessory Device Sequence</summary>
		DicomConstTags.RecordedBrachyAccessoryDeviceSequence = 0x30080120;

		/// <summary>(3008,0122) VR=IS VM=1 Referenced Brachy Accessory Device Number</summary>
		DicomConstTags.ReferencedBrachyAccessoryDeviceNumber = 0x30080122;

		/// <summary>(3008,0130) VR=SQ VM=1 Recorded Channel Sequence</summary>
		DicomConstTags.RecordedChannelSequence = 0x30080130;

		/// <summary>(3008,0132) VR=DS VM=1 Specified Channel Total Time</summary>
		DicomConstTags.SpecifiedChannelTotalTime = 0x30080132;

		/// <summary>(3008,0134) VR=DS VM=1 Delivered Channel Total Time</summary>
		DicomConstTags.DeliveredChannelTotalTime = 0x30080134;

		/// <summary>(3008,0136) VR=IS VM=1 Specified Number of Pulses</summary>
		DicomConstTags.SpecifiedNumberOfPulses = 0x30080136;

		/// <summary>(3008,0138) VR=IS VM=1 Delivered Number of Pulses</summary>
		DicomConstTags.DeliveredNumberOfPulses = 0x30080138;

		/// <summary>(3008,013a) VR=DS VM=1 Specified Pulse Repetition Interval</summary>
		DicomConstTags.SpecifiedPulseRepetitionInterval = 0x3008013a;

		/// <summary>(3008,013c) VR=DS VM=1 Delivered Pulse Repetition Interval</summary>
		DicomConstTags.DeliveredPulseRepetitionInterval = 0x3008013c;

		/// <summary>(3008,0140) VR=SQ VM=1 Recorded Source Applicator Sequence</summary>
		DicomConstTags.RecordedSourceApplicatorSequence = 0x30080140;

		/// <summary>(3008,0142) VR=IS VM=1 Referenced Source Applicator Number</summary>
		DicomConstTags.ReferencedSourceApplicatorNumber = 0x30080142;

		/// <summary>(3008,0150) VR=SQ VM=1 Recorded Channel Shield Sequence</summary>
		DicomConstTags.RecordedChannelShieldSequence = 0x30080150;

		/// <summary>(3008,0152) VR=IS VM=1 Referenced Channel Shield Number</summary>
		DicomConstTags.ReferencedChannelShieldNumber = 0x30080152;

		/// <summary>(3008,0160) VR=SQ VM=1 Brachy Control Point Delivered Sequence</summary>
		DicomConstTags.BrachyControlPointDeliveredSequence = 0x30080160;

		/// <summary>(3008,0162) VR=DA VM=1 Safe Position Exit Date</summary>
		DicomConstTags.SafePositionExitDate = 0x30080162;

		/// <summary>(3008,0164) VR=TM VM=1 Safe Position Exit Time</summary>
		DicomConstTags.SafePositionExitTime = 0x30080164;

		/// <summary>(3008,0166) VR=DA VM=1 Safe Position Return Date</summary>
		DicomConstTags.SafePositionReturnDate = 0x30080166;

		/// <summary>(3008,0168) VR=TM VM=1 Safe Position Return Time</summary>
		DicomConstTags.SafePositionReturnTime = 0x30080168;

		/// <summary>(3008,0200) VR=CS VM=1 Current Treatment Status</summary>
		DicomConstTags.CurrentTreatmentStatus = 0x30080200;

		/// <summary>(3008,0202) VR=ST VM=1 Treatment Status Comment</summary>
		DicomConstTags.TreatmentStatusComment = 0x30080202;

		/// <summary>(3008,0220) VR=SQ VM=1 Fraction Group Summary Sequence</summary>
		DicomConstTags.FractionGroupSummarySequence = 0x30080220;

		/// <summary>(3008,0223) VR=IS VM=1 Referenced Fraction Number</summary>
		DicomConstTags.ReferencedFractionNumber = 0x30080223;

		/// <summary>(3008,0224) VR=CS VM=1 Fraction Group Type</summary>
		DicomConstTags.FractionGroupType = 0x30080224;

		/// <summary>(3008,0230) VR=CS VM=1 Beam Stopper Position</summary>
		DicomConstTags.BeamStopperPosition = 0x30080230;

		/// <summary>(3008,0240) VR=SQ VM=1 Fraction Status Summary Sequence</summary>
		DicomConstTags.FractionStatusSummarySequence = 0x30080240;

		/// <summary>(3008,0250) VR=DA VM=1 Treatment Date</summary>
		DicomConstTags.TreatmentDate = 0x30080250;

		/// <summary>(3008,0251) VR=TM VM=1 Treatment Time</summary>
		DicomConstTags.TreatmentTime = 0x30080251;

		/// <summary>(300a,0002) VR=SH VM=1 RT Plan Label</summary>
		DicomConstTags.RTPlanLabel = 0x300a0002;

		/// <summary>(300a,0003) VR=LO VM=1 RT Plan Name</summary>
		DicomConstTags.RTPlanName = 0x300a0003;

		/// <summary>(300a,0004) VR=ST VM=1 RT Plan Description</summary>
		DicomConstTags.RTPlanDescription = 0x300a0004;

		/// <summary>(300a,0006) VR=DA VM=1 RT Plan Date</summary>
		DicomConstTags.RTPlanDate = 0x300a0006;

		/// <summary>(300a,0007) VR=TM VM=1 RT Plan Time</summary>
		DicomConstTags.RTPlanTime = 0x300a0007;

		/// <summary>(300a,0009) VR=LO VM=1-n Treatment Protocols</summary>
		DicomConstTags.TreatmentProtocols = 0x300a0009;

		/// <summary>(300a,000a) VR=CS VM=1 Plan Intent</summary>
		DicomConstTags.PlanIntent = 0x300a000a;

		/// <summary>(300a,000b) VR=LO VM=1-n Treatment Sites</summary>
		DicomConstTags.TreatmentSites = 0x300a000b;

		/// <summary>(300a,000c) VR=CS VM=1 RT Plan Geometry</summary>
		DicomConstTags.RTPlanGeometry = 0x300a000c;

		/// <summary>(300a,000e) VR=ST VM=1 Prescription Description</summary>
		DicomConstTags.PrescriptionDescription = 0x300a000e;

		/// <summary>(300a,0010) VR=SQ VM=1 Dose Reference Sequence</summary>
		DicomConstTags.DoseReferenceSequence = 0x300a0010;

		/// <summary>(300a,0012) VR=IS VM=1 Dose Reference Number</summary>
		DicomConstTags.DoseReferenceNumber = 0x300a0012;

		/// <summary>(300a,0013) VR=UI VM=1 Dose Reference UID</summary>
		DicomConstTags.DoseReferenceUID = 0x300a0013;

		/// <summary>(300a,0014) VR=CS VM=1 Dose Reference Structure Type</summary>
		DicomConstTags.DoseReferenceStructureType = 0x300a0014;

		/// <summary>(300a,0015) VR=CS VM=1 Nominal Beam Energy Unit</summary>
		DicomConstTags.NominalBeamEnergyUnit = 0x300a0015;

		/// <summary>(300a,0016) VR=LO VM=1 Dose Reference Description</summary>
		DicomConstTags.DoseReferenceDescription = 0x300a0016;

		/// <summary>(300a,0018) VR=DS VM=3 Dose Reference Point Coordinates</summary>
		DicomConstTags.DoseReferencePointCoordinates = 0x300a0018;

		/// <summary>(300a,001a) VR=DS VM=1 Nominal Prior Dose</summary>
		DicomConstTags.NominalPriorDose = 0x300a001a;

		/// <summary>(300a,0020) VR=CS VM=1 Dose Reference Type</summary>
		DicomConstTags.DoseReferenceType = 0x300a0020;

		/// <summary>(300a,0021) VR=DS VM=1 Constraint Weight</summary>
		DicomConstTags.ConstraintWeight = 0x300a0021;

		/// <summary>(300a,0022) VR=DS VM=1 Delivery Warning Dose</summary>
		DicomConstTags.DeliveryWarningDose = 0x300a0022;

		/// <summary>(300a,0023) VR=DS VM=1 Delivery Maximum Dose</summary>
		DicomConstTags.DeliveryMaximumDose = 0x300a0023;

		/// <summary>(300a,0025) VR=DS VM=1 Target Minimum Dose</summary>
		DicomConstTags.TargetMinimumDose = 0x300a0025;

		/// <summary>(300a,0026) VR=DS VM=1 Target Prescription Dose</summary>
		DicomConstTags.TargetPrescriptionDose = 0x300a0026;

		/// <summary>(300a,0027) VR=DS VM=1 Target Maximum Dose</summary>
		DicomConstTags.TargetMaximumDose = 0x300a0027;

		/// <summary>(300a,0028) VR=DS VM=1 Target Underdose Volume Fraction</summary>
		DicomConstTags.TargetUnderdoseVolumeFraction = 0x300a0028;

		/// <summary>(300a,002a) VR=DS VM=1 Organ at Risk Full-volume Dose</summary>
		DicomConstTags.OrganAtRiskFullvolumeDose = 0x300a002a;

		/// <summary>(300a,002b) VR=DS VM=1 Organ at Risk Limit Dose</summary>
		DicomConstTags.OrganAtRiskLimitDose = 0x300a002b;

		/// <summary>(300a,002c) VR=DS VM=1 Organ at Risk Maximum Dose</summary>
		DicomConstTags.OrganAtRiskMaximumDose = 0x300a002c;

		/// <summary>(300a,002d) VR=DS VM=1 Organ at Risk Overdose Volume Fraction</summary>
		DicomConstTags.OrganAtRiskOverdoseVolumeFraction = 0x300a002d;

		/// <summary>(300a,0040) VR=SQ VM=1 Tolerance Table Sequence</summary>
		DicomConstTags.ToleranceTableSequence = 0x300a0040;

		/// <summary>(300a,0042) VR=IS VM=1 Tolerance Table Number</summary>
		DicomConstTags.ToleranceTableNumber = 0x300a0042;

		/// <summary>(300a,0043) VR=SH VM=1 Tolerance Table Label</summary>
		DicomConstTags.ToleranceTableLabel = 0x300a0043;

		/// <summary>(300a,0044) VR=DS VM=1 Gantry Angle Tolerance</summary>
		DicomConstTags.GantryAngleTolerance = 0x300a0044;

		/// <summary>(300a,0046) VR=DS VM=1 Beam Limiting Device Angle Tolerance</summary>
		DicomConstTags.BeamLimitingDeviceAngleTolerance = 0x300a0046;

		/// <summary>(300a,0048) VR=SQ VM=1 Beam Limiting Device Tolerance Sequence</summary>
		DicomConstTags.BeamLimitingDeviceToleranceSequence = 0x300a0048;

		/// <summary>(300a,004a) VR=DS VM=1 Beam Limiting Device Position Tolerance</summary>
		DicomConstTags.BeamLimitingDevicePositionTolerance = 0x300a004a;

		/// <summary>(300a,004b) VR=FL VM=1 Snout Position Tolerance</summary>
		DicomConstTags.SnoutPositionTolerance = 0x300a004b;

		/// <summary>(300a,004c) VR=DS VM=1 Patient Support Angle Tolerance</summary>
		DicomConstTags.PatientSupportAngleTolerance = 0x300a004c;

		/// <summary>(300a,004e) VR=DS VM=1 Table Top Eccentric Angle Tolerance</summary>
		DicomConstTags.TableTopEccentricAngleTolerance = 0x300a004e;

		/// <summary>(300a,004f) VR=FL VM=1 Table Top Pitch Angle Tolerance</summary>
		DicomConstTags.TableTopPitchAngleTolerance = 0x300a004f;

		/// <summary>(300a,0050) VR=FL VM=1 Table Top Roll Angle Tolerance</summary>
		DicomConstTags.TableTopRollAngleTolerance = 0x300a0050;

		/// <summary>(300a,0051) VR=DS VM=1 Table Top Vertical Position Tolerance</summary>
		DicomConstTags.TableTopVerticalPositionTolerance = 0x300a0051;

		/// <summary>(300a,0052) VR=DS VM=1 Table Top Longitudinal Position Tolerance</summary>
		DicomConstTags.TableTopLongitudinalPositionTolerance = 0x300a0052;

		/// <summary>(300a,0053) VR=DS VM=1 Table Top Lateral Position Tolerance</summary>
		DicomConstTags.TableTopLateralPositionTolerance = 0x300a0053;

		/// <summary>(300a,0055) VR=CS VM=1 RT Plan Relationship</summary>
		DicomConstTags.RTPlanRelationship = 0x300a0055;

		/// <summary>(300a,0070) VR=SQ VM=1 Fraction Group Sequence</summary>
		DicomConstTags.FractionGroupSequence = 0x300a0070;

		/// <summary>(300a,0071) VR=IS VM=1 Fraction Group Number</summary>
		DicomConstTags.FractionGroupNumber = 0x300a0071;

		/// <summary>(300a,0072) VR=LO VM=1 Fraction Group Description</summary>
		DicomConstTags.FractionGroupDescription = 0x300a0072;

		/// <summary>(300a,0078) VR=IS VM=1 Number of Fractions Planned</summary>
		DicomConstTags.NumberOfFractionsPlanned = 0x300a0078;

		/// <summary>(300a,0079) VR=IS VM=1 Number of Fraction Pattern Digits Per Day</summary>
		DicomConstTags.NumberOfFractionPatternDigitsPerDay = 0x300a0079;

		/// <summary>(300a,007a) VR=IS VM=1 Repeat Fraction Cycle Length</summary>
		DicomConstTags.RepeatFractionCycleLength = 0x300a007a;

		/// <summary>(300a,007b) VR=LT VM=1 Fraction Pattern</summary>
		DicomConstTags.FractionPattern = 0x300a007b;

		/// <summary>(300a,0080) VR=IS VM=1 Number of Beams</summary>
		DicomConstTags.NumberOfBeams = 0x300a0080;

		/// <summary>(300a,0082) VR=DS VM=3 Beam Dose Specification Point</summary>
		DicomConstTags.BeamDoseSpecificationPoint = 0x300a0082;

		/// <summary>(300a,0084) VR=DS VM=1 Beam Dose</summary>
		DicomConstTags.BeamDose = 0x300a0084;

		/// <summary>(300a,0086) VR=DS VM=1 Beam Meterset</summary>
		DicomConstTags.BeamMeterset = 0x300a0086;

		/// <summary>(300a,0088) VR=FL VM=1 Beam Dose Point Depth</summary>
		DicomConstTags.BeamDosePointDepth = 0x300a0088;

		/// <summary>(300a,0089) VR=FL VM=1 Beam Dose Point Equivalent Depth</summary>
		DicomConstTags.BeamDosePointEquivalentDepth = 0x300a0089;

		/// <summary>(300a,008a) VR=FL VM=1 Beam Dose Point SSD</summary>
		DicomConstTags.BeamDosePointSSD = 0x300a008a;

		/// <summary>(300a,00a0) VR=IS VM=1 Number of Brachy Application Setups</summary>
		DicomConstTags.NumberOfBrachyApplicationSetups = 0x300a00a0;

		/// <summary>(300a,00a2) VR=DS VM=3 Brachy Application Setup Dose Specification Point</summary>
		DicomConstTags.BrachyApplicationSetupDoseSpecificationPoint = 0x300a00a2;

		/// <summary>(300a,00a4) VR=DS VM=1 Brachy Application Setup Dose</summary>
		DicomConstTags.BrachyApplicationSetupDose = 0x300a00a4;

		/// <summary>(300a,00b0) VR=SQ VM=1 Beam Sequence</summary>
		DicomConstTags.BeamSequence = 0x300a00b0;

		/// <summary>(300a,00b2) VR=SH VM=1 Treatment Machine Name</summary>
		DicomConstTags.TreatmentMachineName = 0x300a00b2;

		/// <summary>(300a,00b3) VR=CS VM=1 Primary Dosimeter Unit</summary>
		DicomConstTags.PrimaryDosimeterUnit = 0x300a00b3;

		/// <summary>(300a,00b4) VR=DS VM=1 Source-Axis Distance</summary>
		DicomConstTags.SourceAxisDistance = 0x300a00b4;

		/// <summary>(300a,00b6) VR=SQ VM=1 Beam Limiting Device Sequence</summary>
		DicomConstTags.BeamLimitingDeviceSequence = 0x300a00b6;

		/// <summary>(300a,00b8) VR=CS VM=1 RT Beam Limiting Device Type</summary>
		DicomConstTags.RTBeamLimitingDeviceType = 0x300a00b8;

		/// <summary>(300a,00ba) VR=DS VM=1 Source to Beam Limiting Device Distance</summary>
		DicomConstTags.SourceToBeamLimitingDeviceDistance = 0x300a00ba;

		/// <summary>(300a,00bb) VR=FL VM=1 Isocenter to Beam Limiting Device Distance</summary>
		DicomConstTags.IsocenterToBeamLimitingDeviceDistance = 0x300a00bb;

		/// <summary>(300a,00bc) VR=IS VM=1 Number of Leaf/Jaw Pairs</summary>
		DicomConstTags.NumberOfLeafJawPairs = 0x300a00bc;

		/// <summary>(300a,00be) VR=DS VM=3-n Leaf Position Boundaries</summary>
		DicomConstTags.LeafPositionBoundaries = 0x300a00be;

		/// <summary>(300a,00c0) VR=IS VM=1 Beam Number</summary>
		DicomConstTags.BeamNumber = 0x300a00c0;

		/// <summary>(300a,00c2) VR=LO VM=1 Beam Name</summary>
		DicomConstTags.BeamName = 0x300a00c2;

		/// <summary>(300a,00c3) VR=ST VM=1 Beam Description</summary>
		DicomConstTags.BeamDescription = 0x300a00c3;

		/// <summary>(300a,00c4) VR=CS VM=1 Beam Type</summary>
		DicomConstTags.BeamType = 0x300a00c4;

		/// <summary>(300a,00c6) VR=CS VM=1 Radiation Type</summary>
		DicomConstTags.RadiationType = 0x300a00c6;

		/// <summary>(300a,00c7) VR=CS VM=1 High-Dose Technique Type</summary>
		DicomConstTags.HighDoseTechniqueType = 0x300a00c7;

		/// <summary>(300a,00c8) VR=IS VM=1 Reference Image Number</summary>
		DicomConstTags.ReferenceImageNumber = 0x300a00c8;

		/// <summary>(300a,00ca) VR=SQ VM=1 Planned Verification Image Sequence</summary>
		DicomConstTags.PlannedVerificationImageSequence = 0x300a00ca;

		/// <summary>(300a,00cc) VR=LO VM=1-n Imaging Device-Specific Acquisition Parameters</summary>
		DicomConstTags.ImagingDeviceSpecificAcquisitionParameters = 0x300a00cc;

		/// <summary>(300a,00ce) VR=CS VM=1 Treatment Delivery Type</summary>
		DicomConstTags.TreatmentDeliveryType = 0x300a00ce;

		/// <summary>(300a,00d0) VR=IS VM=1 Number of Wedges</summary>
		DicomConstTags.NumberOfWedges = 0x300a00d0;

		/// <summary>(300a,00d1) VR=SQ VM=1 Wedge Sequence</summary>
		DicomConstTags.WedgeSequence = 0x300a00d1;

		/// <summary>(300a,00d2) VR=IS VM=1 Wedge Number</summary>
		DicomConstTags.WedgeNumber = 0x300a00d2;

		/// <summary>(300a,00d3) VR=CS VM=1 Wedge Type</summary>
		DicomConstTags.WedgeType = 0x300a00d3;

		/// <summary>(300a,00d4) VR=SH VM=1 Wedge ID</summary>
		DicomConstTags.WedgeID = 0x300a00d4;

		/// <summary>(300a,00d5) VR=IS VM=1 Wedge Angle</summary>
		DicomConstTags.WedgeAngle = 0x300a00d5;

		/// <summary>(300a,00d6) VR=DS VM=1 Wedge Factor</summary>
		DicomConstTags.WedgeFactor = 0x300a00d6;

		/// <summary>(300a,00d7) VR=FL VM=1 Total Wedge Tray Water-Equivalent Thickness</summary>
		DicomConstTags.TotalWedgeTrayWaterEquivalentThickness = 0x300a00d7;

		/// <summary>(300a,00d8) VR=DS VM=1 Wedge Orientation</summary>
		DicomConstTags.WedgeOrientation = 0x300a00d8;

		/// <summary>(300a,00d9) VR=FL VM=1 Isocenter to Wedge Tray Distance</summary>
		DicomConstTags.IsocenterToWedgeTrayDistance = 0x300a00d9;

		/// <summary>(300a,00da) VR=DS VM=1 Source to Wedge Tray Distance</summary>
		DicomConstTags.SourceToWedgeTrayDistance = 0x300a00da;

		/// <summary>(300a,00db) VR=FL VM=1 Wedge Thin Edge Position</summary>
		DicomConstTags.WedgeThinEdgePosition = 0x300a00db;

		/// <summary>(300a,00dc) VR=SH VM=1 Bolus ID</summary>
		DicomConstTags.BolusID = 0x300a00dc;

		/// <summary>(300a,00dd) VR=ST VM=1 Bolus Description</summary>
		DicomConstTags.BolusDescription = 0x300a00dd;

		/// <summary>(300a,00e0) VR=IS VM=1 Number of Compensators</summary>
		DicomConstTags.NumberOfCompensators = 0x300a00e0;

		/// <summary>(300a,00e1) VR=SH VM=1 Material ID</summary>
		DicomConstTags.MaterialID = 0x300a00e1;

		/// <summary>(300a,00e2) VR=DS VM=1 Total Compensator Tray Factor</summary>
		DicomConstTags.TotalCompensatorTrayFactor = 0x300a00e2;

		/// <summary>(300a,00e3) VR=SQ VM=1 Compensator Sequence</summary>
		DicomConstTags.CompensatorSequence = 0x300a00e3;

		/// <summary>(300a,00e4) VR=IS VM=1 Compensator Number</summary>
		DicomConstTags.CompensatorNumber = 0x300a00e4;

		/// <summary>(300a,00e5) VR=SH VM=1 Compensator ID</summary>
		DicomConstTags.CompensatorID = 0x300a00e5;

		/// <summary>(300a,00e6) VR=DS VM=1 Source to Compensator Tray Distance</summary>
		DicomConstTags.SourceToCompensatorTrayDistance = 0x300a00e6;

		/// <summary>(300a,00e7) VR=IS VM=1 Compensator Rows</summary>
		DicomConstTags.CompensatorRows = 0x300a00e7;

		/// <summary>(300a,00e8) VR=IS VM=1 Compensator Columns</summary>
		DicomConstTags.CompensatorColumns = 0x300a00e8;

		/// <summary>(300a,00e9) VR=DS VM=2 Compensator Pixel Spacing</summary>
		DicomConstTags.CompensatorPixelSpacing = 0x300a00e9;

		/// <summary>(300a,00ea) VR=DS VM=2 Compensator Position</summary>
		DicomConstTags.CompensatorPosition = 0x300a00ea;

		/// <summary>(300a,00eb) VR=DS VM=1-n Compensator Transmission Data</summary>
		DicomConstTags.CompensatorTransmissionData = 0x300a00eb;

		/// <summary>(300a,00ec) VR=DS VM=1-n Compensator Thickness Data</summary>
		DicomConstTags.CompensatorThicknessData = 0x300a00ec;

		/// <summary>(300a,00ed) VR=IS VM=1 Number of Boli</summary>
		DicomConstTags.NumberOfBoli = 0x300a00ed;

		/// <summary>(300a,00ee) VR=CS VM=1 Compensator Type</summary>
		DicomConstTags.CompensatorType = 0x300a00ee;

		/// <summary>(300a,00f0) VR=IS VM=1 Number of Blocks</summary>
		DicomConstTags.NumberOfBlocks = 0x300a00f0;

		/// <summary>(300a,00f2) VR=DS VM=1 Total Block Tray Factor</summary>
		DicomConstTags.TotalBlockTrayFactor = 0x300a00f2;

		/// <summary>(300a,00f3) VR=FL VM=1 Total Block Tray Water-Equivalent Thickness</summary>
		DicomConstTags.TotalBlockTrayWaterEquivalentThickness = 0x300a00f3;

		/// <summary>(300a,00f4) VR=SQ VM=1 Block Sequence</summary>
		DicomConstTags.BlockSequence = 0x300a00f4;

		/// <summary>(300a,00f5) VR=SH VM=1 Block Tray ID</summary>
		DicomConstTags.BlockTrayID = 0x300a00f5;

		/// <summary>(300a,00f6) VR=DS VM=1 Source to Block Tray Distance</summary>
		DicomConstTags.SourceToBlockTrayDistance = 0x300a00f6;

		/// <summary>(300a,00f7) VR=FL VM=1 Isocenter to Block Tray Distance</summary>
		DicomConstTags.IsocenterToBlockTrayDistance = 0x300a00f7;

		/// <summary>(300a,00f8) VR=CS VM=1 Block Type</summary>
		DicomConstTags.BlockType = 0x300a00f8;

		/// <summary>(300a,00f9) VR=LO VM=1 Accessory Code</summary>
		DicomConstTags.AccessoryCode = 0x300a00f9;

		/// <summary>(300a,00fa) VR=CS VM=1 Block Divergence</summary>
		DicomConstTags.BlockDivergence = 0x300a00fa;

		/// <summary>(300a,00fb) VR=CS VM=1 Block Mounting Position</summary>
		DicomConstTags.BlockMountingPosition = 0x300a00fb;

		/// <summary>(300a,00fc) VR=IS VM=1 Block Number</summary>
		DicomConstTags.BlockNumber = 0x300a00fc;

		/// <summary>(300a,00fe) VR=LO VM=1 Block Name</summary>
		DicomConstTags.BlockName = 0x300a00fe;

		/// <summary>(300a,0100) VR=DS VM=1 Block Thickness</summary>
		DicomConstTags.BlockThickness = 0x300a0100;

		/// <summary>(300a,0102) VR=DS VM=1 Block Transmission</summary>
		DicomConstTags.BlockTransmission = 0x300a0102;

		/// <summary>(300a,0104) VR=IS VM=1 Block Number of Points</summary>
		DicomConstTags.BlockNumberOfPoints = 0x300a0104;

		/// <summary>(300a,0106) VR=DS VM=2-2n Block Data</summary>
		DicomConstTags.BlockData = 0x300a0106;

		/// <summary>(300a,0107) VR=SQ VM=1 Applicator Sequence</summary>
		DicomConstTags.ApplicatorSequence = 0x300a0107;

		/// <summary>(300a,0108) VR=SH VM=1 Applicator ID</summary>
		DicomConstTags.ApplicatorID = 0x300a0108;

		/// <summary>(300a,0109) VR=CS VM=1 Applicator Type</summary>
		DicomConstTags.ApplicatorType = 0x300a0109;

		/// <summary>(300a,010a) VR=LO VM=1 Applicator Description</summary>
		DicomConstTags.ApplicatorDescription = 0x300a010a;

		/// <summary>(300a,010c) VR=DS VM=1 Cumulative Dose Reference Coefficient</summary>
		DicomConstTags.CumulativeDoseReferenceCoefficient = 0x300a010c;

		/// <summary>(300a,010e) VR=DS VM=1 Final Cumulative Meterset Weight</summary>
		DicomConstTags.FinalCumulativeMetersetWeight = 0x300a010e;

		/// <summary>(300a,0110) VR=IS VM=1 Number of Control Points</summary>
		DicomConstTags.NumberOfControlPoints = 0x300a0110;

		/// <summary>(300a,0111) VR=SQ VM=1 Control Point Sequence</summary>
		DicomConstTags.ControlPointSequence = 0x300a0111;

		/// <summary>(300a,0112) VR=IS VM=1 Control Point Index</summary>
		DicomConstTags.ControlPointIndex = 0x300a0112;

		/// <summary>(300a,0114) VR=DS VM=1 Nominal Beam Energy</summary>
		DicomConstTags.NominalBeamEnergy = 0x300a0114;

		/// <summary>(300a,0115) VR=DS VM=1 Dose Rate Set</summary>
		DicomConstTags.DoseRateSet = 0x300a0115;

		/// <summary>(300a,0116) VR=SQ VM=1 Wedge Position Sequence</summary>
		DicomConstTags.WedgePositionSequence = 0x300a0116;

		/// <summary>(300a,0118) VR=CS VM=1 Wedge Position</summary>
		DicomConstTags.WedgePosition = 0x300a0118;

		/// <summary>(300a,011a) VR=SQ VM=1 Beam Limiting Device Position Sequence</summary>
		DicomConstTags.BeamLimitingDevicePositionSequence = 0x300a011a;

		/// <summary>(300a,011c) VR=DS VM=2-2n Leaf/Jaw Positions</summary>
		DicomConstTags.LeafJawPositions = 0x300a011c;

		/// <summary>(300a,011e) VR=DS VM=1 Gantry Angle</summary>
		DicomConstTags.GantryAngle = 0x300a011e;

		/// <summary>(300a,011f) VR=CS VM=1 Gantry Rotation Direction</summary>
		DicomConstTags.GantryRotationDirection = 0x300a011f;

		/// <summary>(300a,0120) VR=DS VM=1 Beam Limiting Device Angle</summary>
		DicomConstTags.BeamLimitingDeviceAngle = 0x300a0120;

		/// <summary>(300a,0121) VR=CS VM=1 Beam Limiting Device Rotation Direction</summary>
		DicomConstTags.BeamLimitingDeviceRotationDirection = 0x300a0121;

		/// <summary>(300a,0122) VR=DS VM=1 Patient Support Angle</summary>
		DicomConstTags.PatientSupportAngle = 0x300a0122;

		/// <summary>(300a,0123) VR=CS VM=1 Patient Support Rotation Direction</summary>
		DicomConstTags.PatientSupportRotationDirection = 0x300a0123;

		/// <summary>(300a,0124) VR=DS VM=1 Table Top Eccentric Axis Distance</summary>
		DicomConstTags.TableTopEccentricAxisDistance = 0x300a0124;

		/// <summary>(300a,0125) VR=DS VM=1 Table Top Eccentric Angle</summary>
		DicomConstTags.TableTopEccentricAngle = 0x300a0125;

		/// <summary>(300a,0126) VR=CS VM=1 Table Top Eccentric Rotation Direction</summary>
		DicomConstTags.TableTopEccentricRotationDirection = 0x300a0126;

		/// <summary>(300a,0128) VR=DS VM=1 Table Top Vertical Position</summary>
		DicomConstTags.TableTopVerticalPosition = 0x300a0128;

		/// <summary>(300a,0129) VR=DS VM=1 Table Top Longitudinal Position</summary>
		DicomConstTags.TableTopLongitudinalPosition = 0x300a0129;

		/// <summary>(300a,012a) VR=DS VM=1 Table Top Lateral Position</summary>
		DicomConstTags.TableTopLateralPosition = 0x300a012a;

		/// <summary>(300a,012c) VR=DS VM=3 Isocenter Position</summary>
		DicomConstTags.IsocenterPosition = 0x300a012c;

		/// <summary>(300a,012e) VR=DS VM=3 Surface Entry Point</summary>
		DicomConstTags.SurfaceEntryPoint = 0x300a012e;

		/// <summary>(300a,0130) VR=DS VM=1 Source to Surface Distance</summary>
		DicomConstTags.SourceToSurfaceDistance = 0x300a0130;

		/// <summary>(300a,0134) VR=DS VM=1 Cumulative Meterset Weight</summary>
		DicomConstTags.CumulativeMetersetWeight = 0x300a0134;

		/// <summary>(300a,0140) VR=FL VM=1 Table Top Pitch Angle</summary>
		DicomConstTags.TableTopPitchAngle = 0x300a0140;

		/// <summary>(300a,0142) VR=CS VM=1 Table Top Pitch Rotation Direction</summary>
		DicomConstTags.TableTopPitchRotationDirection = 0x300a0142;

		/// <summary>(300a,0144) VR=FL VM=1 Table Top Roll Angle</summary>
		DicomConstTags.TableTopRollAngle = 0x300a0144;

		/// <summary>(300a,0146) VR=CS VM=1 Table Top Roll Rotation Direction</summary>
		DicomConstTags.TableTopRollRotationDirection = 0x300a0146;

		/// <summary>(300a,0148) VR=FL VM=1 Head Fixation Angle</summary>
		DicomConstTags.HeadFixationAngle = 0x300a0148;

		/// <summary>(300a,014a) VR=FL VM=1 Gantry Pitch Angle</summary>
		DicomConstTags.GantryPitchAngle = 0x300a014a;

		/// <summary>(300a,014c) VR=CS VM=1 Gantry Pitch Rotation Direction</summary>
		DicomConstTags.GantryPitchRotationDirection = 0x300a014c;

		/// <summary>(300a,014e) VR=FL VM=1 Gantry Pitch Angle Tolerance</summary>
		DicomConstTags.GantryPitchAngleTolerance = 0x300a014e;

		/// <summary>(300a,0180) VR=SQ VM=1 Patient Setup Sequence</summary>
		DicomConstTags.PatientSetupSequence = 0x300a0180;

		/// <summary>(300a,0182) VR=IS VM=1 Patient Setup Number</summary>
		DicomConstTags.PatientSetupNumber = 0x300a0182;

		/// <summary>(300a,0183) VR=LO VM=1 Patient Setup Label</summary>
		DicomConstTags.PatientSetupLabel = 0x300a0183;

		/// <summary>(300a,0184) VR=LO VM=1 Patient Additional Position</summary>
		DicomConstTags.PatientAdditionalPosition = 0x300a0184;

		/// <summary>(300a,0190) VR=SQ VM=1 Fixation Device Sequence</summary>
		DicomConstTags.FixationDeviceSequence = 0x300a0190;

		/// <summary>(300a,0192) VR=CS VM=1 Fixation Device Type</summary>
		DicomConstTags.FixationDeviceType = 0x300a0192;

		/// <summary>(300a,0194) VR=SH VM=1 Fixation Device Label</summary>
		DicomConstTags.FixationDeviceLabel = 0x300a0194;

		/// <summary>(300a,0196) VR=ST VM=1 Fixation Device Description</summary>
		DicomConstTags.FixationDeviceDescription = 0x300a0196;

		/// <summary>(300a,0198) VR=SH VM=1 Fixation Device Position</summary>
		DicomConstTags.FixationDevicePosition = 0x300a0198;

		/// <summary>(300a,0199) VR=FL VM=1 Fixation Device Pitch Angle</summary>
		DicomConstTags.FixationDevicePitchAngle = 0x300a0199;

		/// <summary>(300a,019a) VR=FL VM=1 Fixation Device Roll Angle</summary>
		DicomConstTags.FixationDeviceRollAngle = 0x300a019a;

		/// <summary>(300a,01a0) VR=SQ VM=1 Shielding Device Sequence</summary>
		DicomConstTags.ShieldingDeviceSequence = 0x300a01a0;

		/// <summary>(300a,01a2) VR=CS VM=1 Shielding Device Type</summary>
		DicomConstTags.ShieldingDeviceType = 0x300a01a2;

		/// <summary>(300a,01a4) VR=SH VM=1 Shielding Device Label</summary>
		DicomConstTags.ShieldingDeviceLabel = 0x300a01a4;

		/// <summary>(300a,01a6) VR=ST VM=1 Shielding Device Description</summary>
		DicomConstTags.ShieldingDeviceDescription = 0x300a01a6;

		/// <summary>(300a,01a8) VR=SH VM=1 Shielding Device Position</summary>
		DicomConstTags.ShieldingDevicePosition = 0x300a01a8;

		/// <summary>(300a,01b0) VR=CS VM=1 Setup Technique</summary>
		DicomConstTags.SetupTechnique = 0x300a01b0;

		/// <summary>(300a,01b2) VR=ST VM=1 Setup Technique Description</summary>
		DicomConstTags.SetupTechniqueDescription = 0x300a01b2;

		/// <summary>(300a,01b4) VR=SQ VM=1 Setup Device Sequence</summary>
		DicomConstTags.SetupDeviceSequence = 0x300a01b4;

		/// <summary>(300a,01b6) VR=CS VM=1 Setup Device Type</summary>
		DicomConstTags.SetupDeviceType = 0x300a01b6;

		/// <summary>(300a,01b8) VR=SH VM=1 Setup Device Label</summary>
		DicomConstTags.SetupDeviceLabel = 0x300a01b8;

		/// <summary>(300a,01ba) VR=ST VM=1 Setup Device Description</summary>
		DicomConstTags.SetupDeviceDescription = 0x300a01ba;

		/// <summary>(300a,01bc) VR=DS VM=1 Setup Device Parameter</summary>
		DicomConstTags.SetupDeviceParameter = 0x300a01bc;

		/// <summary>(300a,01d0) VR=ST VM=1 Setup Reference Description</summary>
		DicomConstTags.SetupReferenceDescription = 0x300a01d0;

		/// <summary>(300a,01d2) VR=DS VM=1 Table Top Vertical Setup Displacement</summary>
		DicomConstTags.TableTopVerticalSetupDisplacement = 0x300a01d2;

		/// <summary>(300a,01d4) VR=DS VM=1 Table Top Longitudinal Setup Displacement</summary>
		DicomConstTags.TableTopLongitudinalSetupDisplacement = 0x300a01d4;

		/// <summary>(300a,01d6) VR=DS VM=1 Table Top Lateral Setup Displacement</summary>
		DicomConstTags.TableTopLateralSetupDisplacement = 0x300a01d6;

		/// <summary>(300a,0200) VR=CS VM=1 Brachy Treatment Technique</summary>
		DicomConstTags.BrachyTreatmentTechnique = 0x300a0200;

		/// <summary>(300a,0202) VR=CS VM=1 Brachy Treatment Type</summary>
		DicomConstTags.BrachyTreatmentType = 0x300a0202;

		/// <summary>(300a,0206) VR=SQ VM=1 Treatment Machine Sequence</summary>
		DicomConstTags.TreatmentMachineSequence = 0x300a0206;

		/// <summary>(300a,0210) VR=SQ VM=1 Source Sequence</summary>
		DicomConstTags.SourceSequence = 0x300a0210;

		/// <summary>(300a,0212) VR=IS VM=1 Source Number</summary>
		DicomConstTags.SourceNumber = 0x300a0212;

		/// <summary>(300a,0214) VR=CS VM=1 Source Type</summary>
		DicomConstTags.SourceType = 0x300a0214;

		/// <summary>(300a,0216) VR=LO VM=1 Source Manufacturer</summary>
		DicomConstTags.SourceManufacturer = 0x300a0216;

		/// <summary>(300a,0218) VR=DS VM=1 Active Source Diameter</summary>
		DicomConstTags.ActiveSourceDiameter = 0x300a0218;

		/// <summary>(300a,021a) VR=DS VM=1 Active Source Length</summary>
		DicomConstTags.ActiveSourceLength = 0x300a021a;

		/// <summary>(300a,0222) VR=DS VM=1 Source Encapsulation Nominal Thickness</summary>
		DicomConstTags.SourceEncapsulationNominalThickness = 0x300a0222;

		/// <summary>(300a,0224) VR=DS VM=1 Source Encapsulation Nominal Transmission</summary>
		DicomConstTags.SourceEncapsulationNominalTransmission = 0x300a0224;

		/// <summary>(300a,0226) VR=LO VM=1 Source Isotope Name</summary>
		DicomConstTags.SourceIsotopeName = 0x300a0226;

		/// <summary>(300a,0228) VR=DS VM=1 Source Isotope Half Life</summary>
		DicomConstTags.SourceIsotopeHalfLife = 0x300a0228;

		/// <summary>(300a,0229) VR=CS VM=1 Source Strength Units</summary>
		DicomConstTags.SourceStrengthUnits = 0x300a0229;

		/// <summary>(300a,022a) VR=DS VM=1 Reference Air Kerma Rate</summary>
		DicomConstTags.ReferenceAirKermaRate = 0x300a022a;

		/// <summary>(300a,022b) VR=DS VM=1 Source Strength</summary>
		DicomConstTags.SourceStrength = 0x300a022b;

		/// <summary>(300a,022c) VR=DA VM=1 Source Strength Reference Date</summary>
		DicomConstTags.SourceStrengthReferenceDate = 0x300a022c;

		/// <summary>(300a,022e) VR=TM VM=1 Source Strength Reference Time</summary>
		DicomConstTags.SourceStrengthReferenceTime = 0x300a022e;

		/// <summary>(300a,0230) VR=SQ VM=1 Application Setup Sequence</summary>
		DicomConstTags.ApplicationSetupSequence = 0x300a0230;

		/// <summary>(300a,0232) VR=CS VM=1 Application Setup Type</summary>
		DicomConstTags.ApplicationSetupType = 0x300a0232;

		/// <summary>(300a,0234) VR=IS VM=1 Application Setup Number</summary>
		DicomConstTags.ApplicationSetupNumber = 0x300a0234;

		/// <summary>(300a,0236) VR=LO VM=1 Application Setup Name</summary>
		DicomConstTags.ApplicationSetupName = 0x300a0236;

		/// <summary>(300a,0238) VR=LO VM=1 Application Setup Manufacturer</summary>
		DicomConstTags.ApplicationSetupManufacturer = 0x300a0238;

		/// <summary>(300a,0240) VR=IS VM=1 Template Number</summary>
		DicomConstTags.TemplateNumber = 0x300a0240;

		/// <summary>(300a,0242) VR=SH VM=1 Template Type</summary>
		DicomConstTags.TemplateType = 0x300a0242;

		/// <summary>(300a,0244) VR=LO VM=1 Template Name</summary>
		DicomConstTags.TemplateName = 0x300a0244;

		/// <summary>(300a,0250) VR=DS VM=1 Total Reference Air Kerma</summary>
		DicomConstTags.TotalReferenceAirKerma = 0x300a0250;

		/// <summary>(300a,0260) VR=SQ VM=1 Brachy Accessory Device Sequence</summary>
		DicomConstTags.BrachyAccessoryDeviceSequence = 0x300a0260;

		/// <summary>(300a,0262) VR=IS VM=1 Brachy Accessory Device Number</summary>
		DicomConstTags.BrachyAccessoryDeviceNumber = 0x300a0262;

		/// <summary>(300a,0263) VR=SH VM=1 Brachy Accessory Device ID</summary>
		DicomConstTags.BrachyAccessoryDeviceID = 0x300a0263;

		/// <summary>(300a,0264) VR=CS VM=1 Brachy Accessory Device Type</summary>
		DicomConstTags.BrachyAccessoryDeviceType = 0x300a0264;

		/// <summary>(300a,0266) VR=LO VM=1 Brachy Accessory Device Name</summary>
		DicomConstTags.BrachyAccessoryDeviceName = 0x300a0266;

		/// <summary>(300a,026a) VR=DS VM=1 Brachy Accessory Device Nominal Thickness</summary>
		DicomConstTags.BrachyAccessoryDeviceNominalThickness = 0x300a026a;

		/// <summary>(300a,026c) VR=DS VM=1 Brachy Accessory Device Nominal Transmission</summary>
		DicomConstTags.BrachyAccessoryDeviceNominalTransmission = 0x300a026c;

		/// <summary>(300a,0280) VR=SQ VM=1 Channel Sequence</summary>
		DicomConstTags.ChannelSequence = 0x300a0280;

		/// <summary>(300a,0282) VR=IS VM=1 Channel Number</summary>
		DicomConstTags.ChannelNumber = 0x300a0282;

		/// <summary>(300a,0284) VR=DS VM=1 Channel Length</summary>
		DicomConstTags.ChannelLength = 0x300a0284;

		/// <summary>(300a,0286) VR=DS VM=1 Channel Total Time</summary>
		DicomConstTags.ChannelTotalTime = 0x300a0286;

		/// <summary>(300a,0288) VR=CS VM=1 Source Movement Type</summary>
		DicomConstTags.SourceMovementType = 0x300a0288;

		/// <summary>(300a,028a) VR=IS VM=1 Number of Pulses</summary>
		DicomConstTags.NumberOfPulses = 0x300a028a;

		/// <summary>(300a,028c) VR=DS VM=1 Pulse Repetition Interval</summary>
		DicomConstTags.PulseRepetitionInterval = 0x300a028c;

		/// <summary>(300a,0290) VR=IS VM=1 Source Applicator Number</summary>
		DicomConstTags.SourceApplicatorNumber = 0x300a0290;

		/// <summary>(300a,0291) VR=SH VM=1 Source Applicator ID</summary>
		DicomConstTags.SourceApplicatorID = 0x300a0291;

		/// <summary>(300a,0292) VR=CS VM=1 Source Applicator Type</summary>
		DicomConstTags.SourceApplicatorType = 0x300a0292;

		/// <summary>(300a,0294) VR=LO VM=1 Source Applicator Name</summary>
		DicomConstTags.SourceApplicatorName = 0x300a0294;

		/// <summary>(300a,0296) VR=DS VM=1 Source Applicator Length</summary>
		DicomConstTags.SourceApplicatorLength = 0x300a0296;

		/// <summary>(300a,0298) VR=LO VM=1 Source Applicator Manufacturer</summary>
		DicomConstTags.SourceApplicatorManufacturer = 0x300a0298;

		/// <summary>(300a,029c) VR=DS VM=1 Source Applicator Wall Nominal Thickness</summary>
		DicomConstTags.SourceApplicatorWallNominalThickness = 0x300a029c;

		/// <summary>(300a,029e) VR=DS VM=1 Source Applicator Wall Nominal Transmission</summary>
		DicomConstTags.SourceApplicatorWallNominalTransmission = 0x300a029e;

		/// <summary>(300a,02a0) VR=DS VM=1 Source Applicator Step Size</summary>
		DicomConstTags.SourceApplicatorStepSize = 0x300a02a0;

		/// <summary>(300a,02a2) VR=IS VM=1 Transfer Tube Number</summary>
		DicomConstTags.TransferTubeNumber = 0x300a02a2;

		/// <summary>(300a,02a4) VR=DS VM=1 Transfer Tube Length</summary>
		DicomConstTags.TransferTubeLength = 0x300a02a4;

		/// <summary>(300a,02b0) VR=SQ VM=1 Channel Shield Sequence</summary>
		DicomConstTags.ChannelShieldSequence = 0x300a02b0;

		/// <summary>(300a,02b2) VR=IS VM=1 Channel Shield Number</summary>
		DicomConstTags.ChannelShieldNumber = 0x300a02b2;

		/// <summary>(300a,02b3) VR=SH VM=1 Channel Shield ID</summary>
		DicomConstTags.ChannelShieldID = 0x300a02b3;

		/// <summary>(300a,02b4) VR=LO VM=1 Channel Shield Name</summary>
		DicomConstTags.ChannelShieldName = 0x300a02b4;

		/// <summary>(300a,02b8) VR=DS VM=1 Channel Shield Nominal Thickness</summary>
		DicomConstTags.ChannelShieldNominalThickness = 0x300a02b8;

		/// <summary>(300a,02ba) VR=DS VM=1 Channel Shield Nominal Transmission</summary>
		DicomConstTags.ChannelShieldNominalTransmission = 0x300a02ba;

		/// <summary>(300a,02c8) VR=DS VM=1 Final Cumulative Time Weight</summary>
		DicomConstTags.FinalCumulativeTimeWeight = 0x300a02c8;

		/// <summary>(300a,02d0) VR=SQ VM=1 Brachy Control Point Sequence</summary>
		DicomConstTags.BrachyControlPointSequence = 0x300a02d0;

		/// <summary>(300a,02d2) VR=DS VM=1 Control Point Relative Position</summary>
		DicomConstTags.ControlPointRelativePosition = 0x300a02d2;

		/// <summary>(300a,02d4) VR=DS VM=3 Control Point 3D Position</summary>
		DicomConstTags.ControlPoint3DPosition = 0x300a02d4;

		/// <summary>(300a,02d6) VR=DS VM=1 Cumulative Time Weight</summary>
		DicomConstTags.CumulativeTimeWeight = 0x300a02d6;

		/// <summary>(300a,02e0) VR=CS VM=1 Compensator Divergence</summary>
		DicomConstTags.CompensatorDivergence = 0x300a02e0;

		/// <summary>(300a,02e1) VR=CS VM=1 Compensator Mounting Position</summary>
		DicomConstTags.CompensatorMountingPosition = 0x300a02e1;

		/// <summary>(300a,02e2) VR=DS VM=1-n Source to Compensator Distance</summary>
		DicomConstTags.SourceToCompensatorDistance = 0x300a02e2;

		/// <summary>(300a,02e3) VR=FL VM=1 Total Compensator Tray Water-Equivalent Thickness</summary>
		DicomConstTags.TotalCompensatorTrayWaterEquivalentThickness = 0x300a02e3;

		/// <summary>(300a,02e4) VR=FL VM=1 Isocenter to Compensator Tray Distance</summary>
		DicomConstTags.IsocenterToCompensatorTrayDistance = 0x300a02e4;

		/// <summary>(300a,02e5) VR=FL VM=1 Compensator Column Offset</summary>
		DicomConstTags.CompensatorColumnOffset = 0x300a02e5;

		/// <summary>(300a,02e6) VR=FL VM=1-n Isocenter to Compensator Distances</summary>
		DicomConstTags.IsocenterToCompensatorDistances = 0x300a02e6;

		/// <summary>(300a,02e7) VR=FL VM=1 Compensator Relative Stopping Power Ratio</summary>
		DicomConstTags.CompensatorRelativeStoppingPowerRatio = 0x300a02e7;

		/// <summary>(300a,02e8) VR=FL VM=1 Compensator Milling Tool Diameter</summary>
		DicomConstTags.CompensatorMillingToolDiameter = 0x300a02e8;

		/// <summary>(300a,02ea) VR=SQ VM=1 Ion Range Compensator Sequence</summary>
		DicomConstTags.IonRangeCompensatorSequence = 0x300a02ea;

		/// <summary>(300a,02eb) VR=LT VM=1 Compensator Description</summary>
		DicomConstTags.CompensatorDescription = 0x300a02eb;

		/// <summary>(300a,0302) VR=IS VM=1 Radiation Mass Number</summary>
		DicomConstTags.RadiationMassNumber = 0x300a0302;

		/// <summary>(300a,0304) VR=IS VM=1 Radiation Atomic Number</summary>
		DicomConstTags.RadiationAtomicNumber = 0x300a0304;

		/// <summary>(300a,0306) VR=SS VM=1 Radiation Charge State</summary>
		DicomConstTags.RadiationChargeState = 0x300a0306;

		/// <summary>(300a,0308) VR=CS VM=1 Scan Mode</summary>
		DicomConstTags.ScanMode = 0x300a0308;

		/// <summary>(300a,030a) VR=FL VM=2 Virtual Source-Axis Distances</summary>
		DicomConstTags.VirtualSourceAxisDistances = 0x300a030a;

		/// <summary>(300a,030c) VR=SQ VM=1 Snout Sequence</summary>
		DicomConstTags.SnoutSequence = 0x300a030c;

		/// <summary>(300a,030d) VR=FL VM=1 Snout Position</summary>
		DicomConstTags.SnoutPosition = 0x300a030d;

		/// <summary>(300a,030f) VR=SH VM=1 Snout ID</summary>
		DicomConstTags.SnoutID = 0x300a030f;

		/// <summary>(300a,0312) VR=IS VM=1 Number of Range Shifters</summary>
		DicomConstTags.NumberOfRangeShifters = 0x300a0312;

		/// <summary>(300a,0314) VR=SQ VM=1 Range Shifter Sequence</summary>
		DicomConstTags.RangeShifterSequence = 0x300a0314;

		/// <summary>(300a,0316) VR=IS VM=1 Range Shifter Number</summary>
		DicomConstTags.RangeShifterNumber = 0x300a0316;

		/// <summary>(300a,0318) VR=SH VM=1 Range Shifter ID</summary>
		DicomConstTags.RangeShifterID = 0x300a0318;

		/// <summary>(300a,0320) VR=CS VM=1 Range Shifter Type</summary>
		DicomConstTags.RangeShifterType = 0x300a0320;

		/// <summary>(300a,0322) VR=LO VM=1 Range Shifter Description</summary>
		DicomConstTags.RangeShifterDescription = 0x300a0322;

		/// <summary>(300a,0330) VR=IS VM=1 Number of Lateral Spreading Devices</summary>
		DicomConstTags.NumberOfLateralSpreadingDevices = 0x300a0330;

		/// <summary>(300a,0332) VR=SQ VM=1 Lateral Spreading Device Sequence</summary>
		DicomConstTags.LateralSpreadingDeviceSequence = 0x300a0332;

		/// <summary>(300a,0334) VR=IS VM=1 Lateral Spreading Device Number</summary>
		DicomConstTags.LateralSpreadingDeviceNumber = 0x300a0334;

		/// <summary>(300a,0336) VR=SH VM=1 Lateral Spreading Device ID</summary>
		DicomConstTags.LateralSpreadingDeviceID = 0x300a0336;

		/// <summary>(300a,0338) VR=CS VM=1 Lateral Spreading Device Type</summary>
		DicomConstTags.LateralSpreadingDeviceType = 0x300a0338;

		/// <summary>(300a,033a) VR=LO VM=1 Lateral Spreading Device Description</summary>
		DicomConstTags.LateralSpreadingDeviceDescription = 0x300a033a;

		/// <summary>(300a,033c) VR=FL VM=1 Lateral Spreading Device Water Equivalent Thickness</summary>
		DicomConstTags.LateralSpreadingDeviceWaterEquivalentThickness = 0x300a033c;

		/// <summary>(300a,0340) VR=IS VM=1 Number of Range Modulators</summary>
		DicomConstTags.NumberOfRangeModulators = 0x300a0340;

		/// <summary>(300a,0342) VR=SQ VM=1 Range Modulator Sequence</summary>
		DicomConstTags.RangeModulatorSequence = 0x300a0342;

		/// <summary>(300a,0344) VR=IS VM=1 Range Modulator Number</summary>
		DicomConstTags.RangeModulatorNumber = 0x300a0344;

		/// <summary>(300a,0346) VR=SH VM=1 Range Modulator ID</summary>
		DicomConstTags.RangeModulatorID = 0x300a0346;

		/// <summary>(300a,0348) VR=CS VM=1 Range Modulator Type</summary>
		DicomConstTags.RangeModulatorType = 0x300a0348;

		/// <summary>(300a,034a) VR=LO VM=1 Range Modulator Description</summary>
		DicomConstTags.RangeModulatorDescription = 0x300a034a;

		/// <summary>(300a,034c) VR=SH VM=1 Beam Current Modulation ID</summary>
		DicomConstTags.BeamCurrentModulationID = 0x300a034c;

		/// <summary>(300a,0350) VR=CS VM=1 Patient Support Type</summary>
		DicomConstTags.PatientSupportType = 0x300a0350;

		/// <summary>(300a,0352) VR=SH VM=1 Patient Support ID</summary>
		DicomConstTags.PatientSupportID = 0x300a0352;

		/// <summary>(300a,0354) VR=LO VM=1 Patient Support Accessory Code</summary>
		DicomConstTags.PatientSupportAccessoryCode = 0x300a0354;

		/// <summary>(300a,0356) VR=FL VM=1 Fixation Light Azimuthal Angle</summary>
		DicomConstTags.FixationLightAzimuthalAngle = 0x300a0356;

		/// <summary>(300a,0358) VR=FL VM=1 Fixation Light Polar Angle</summary>
		DicomConstTags.FixationLightPolarAngle = 0x300a0358;

		/// <summary>(300a,035a) VR=FL VM=1 Meterset Rate</summary>
		DicomConstTags.MetersetRate = 0x300a035a;

		/// <summary>(300a,0360) VR=SQ VM=1 Range Shifter Settings Sequence</summary>
		DicomConstTags.RangeShifterSettingsSequence = 0x300a0360;

		/// <summary>(300a,0362) VR=LO VM=1 Range Shifter Setting</summary>
		DicomConstTags.RangeShifterSetting = 0x300a0362;

		/// <summary>(300a,0364) VR=FL VM=1 Isocenter to Range Shifter Distance</summary>
		DicomConstTags.IsocenterToRangeShifterDistance = 0x300a0364;

		/// <summary>(300a,0366) VR=FL VM=1 Range Shifter Water Equivalent Thickness</summary>
		DicomConstTags.RangeShifterWaterEquivalentThickness = 0x300a0366;

		/// <summary>(300a,0370) VR=SQ VM=1 Lateral Spreading Device Settings Sequence</summary>
		DicomConstTags.LateralSpreadingDeviceSettingsSequence = 0x300a0370;

		/// <summary>(300a,0372) VR=LO VM=1 Lateral Spreading Device Setting</summary>
		DicomConstTags.LateralSpreadingDeviceSetting = 0x300a0372;

		/// <summary>(300a,0374) VR=FL VM=1 Isocenter to Lateral Spreading Device Distance</summary>
		DicomConstTags.IsocenterToLateralSpreadingDeviceDistance = 0x300a0374;

		/// <summary>(300a,0380) VR=SQ VM=1 Range Modulator Settings Sequence</summary>
		DicomConstTags.RangeModulatorSettingsSequence = 0x300a0380;

		/// <summary>(300a,0382) VR=FL VM=1 Range Modulator Gating Start Value</summary>
		DicomConstTags.RangeModulatorGatingStartValue = 0x300a0382;

		/// <summary>(300a,0384) VR=FL VM=1 Range Modulator Gating Stop Value</summary>
		DicomConstTags.RangeModulatorGatingStopValue = 0x300a0384;

		/// <summary>(300a,0386) VR=FL VM=1 Range Modulator Gating Start Water Equivalent Thickness</summary>
		DicomConstTags.RangeModulatorGatingStartWaterEquivalentThickness = 0x300a0386;

		/// <summary>(300a,0388) VR=FL VM=1 Range Modulator Gating Stop Water Equivalent Thickness</summary>
		DicomConstTags.RangeModulatorGatingStopWaterEquivalentThickness = 0x300a0388;

		/// <summary>(300a,038a) VR=FL VM=1 Isocenter to Range Modulator Distance</summary>
		DicomConstTags.IsocenterToRangeModulatorDistance = 0x300a038a;

		/// <summary>(300a,0390) VR=SH VM=1 Scan Spot Tune ID</summary>
		DicomConstTags.ScanSpotTuneID = 0x300a0390;

		/// <summary>(300a,0392) VR=IS VM=1 Number of Scan Spot Positions</summary>
		DicomConstTags.NumberOfScanSpotPositions = 0x300a0392;

		/// <summary>(300a,0394) VR=FL VM=1-n Scan Spot Position Map</summary>
		DicomConstTags.ScanSpotPositionMap = 0x300a0394;

		/// <summary>(300a,0396) VR=FL VM=1-n Scan Spot Meterset Weights</summary>
		DicomConstTags.ScanSpotMetersetWeights = 0x300a0396;

		/// <summary>(300a,0398) VR=FL VM=2 Scanning Spot Size</summary>
		DicomConstTags.ScanningSpotSize = 0x300a0398;

		/// <summary>(300a,039a) VR=IS VM=1 Number of Paintings</summary>
		DicomConstTags.NumberOfPaintings = 0x300a039a;

		/// <summary>(300a,03a0) VR=SQ VM=1 Ion Tolerance Table Sequence</summary>
		DicomConstTags.IonToleranceTableSequence = 0x300a03a0;

		/// <summary>(300a,03a2) VR=SQ VM=1 Ion Beam Sequence</summary>
		DicomConstTags.IonBeamSequence = 0x300a03a2;

		/// <summary>(300a,03a4) VR=SQ VM=1 Ion Beam Limiting Device Sequence</summary>
		DicomConstTags.IonBeamLimitingDeviceSequence = 0x300a03a4;

		/// <summary>(300a,03a6) VR=SQ VM=1 Ion Block Sequence</summary>
		DicomConstTags.IonBlockSequence = 0x300a03a6;

		/// <summary>(300a,03a8) VR=SQ VM=1 Ion Control Point Sequence</summary>
		DicomConstTags.IonControlPointSequence = 0x300a03a8;

		/// <summary>(300a,03aa) VR=SQ VM=1 Ion Wedge Sequence</summary>
		DicomConstTags.IonWedgeSequence = 0x300a03aa;

		/// <summary>(300a,03ac) VR=SQ VM=1 Ion Wedge Position Sequence</summary>
		DicomConstTags.IonWedgePositionSequence = 0x300a03ac;

		/// <summary>(300a,0401) VR=SQ VM=1 Referenced Setup Image Sequence</summary>
		DicomConstTags.ReferencedSetupImageSequence = 0x300a0401;

		/// <summary>(300a,0402) VR=ST VM=1 Setup Image Comment</summary>
		DicomConstTags.SetupImageComment = 0x300a0402;

		/// <summary>(300a,0410) VR=SQ VM=1 Motion Synchronization Sequence</summary>
		DicomConstTags.MotionSynchronizationSequence = 0x300a0410;

		/// <summary>(300a,0412) VR=FL VM=3 Control Point Orientation</summary>
		DicomConstTags.ControlPointOrientation = 0x300a0412;

		/// <summary>(300a,0420) VR=SQ VM=1 General Accessory Sequence</summary>
		DicomConstTags.GeneralAccessorySequence = 0x300a0420;

		/// <summary>(300a,0421) VR=CS VM=1 General Accessory ID</summary>
		DicomConstTags.GeneralAccessoryID = 0x300a0421;

		/// <summary>(300a,0422) VR=ST VM=1 General Accessory Description</summary>
		DicomConstTags.GeneralAccessoryDescription = 0x300a0422;

		/// <summary>(300a,0423) VR=SH VM=1 General Accessory Type</summary>
		DicomConstTags.GeneralAccessoryType = 0x300a0423;

		/// <summary>(300a,0424) VR=IS VM=1 General Accessory Number</summary>
		DicomConstTags.GeneralAccessoryNumber = 0x300a0424;

		/// <summary>(300c,0002) VR=SQ VM=1 Referenced RT Plan Sequence</summary>
		DicomConstTags.ReferencedRTPlanSequence = 0x300c0002;

		/// <summary>(300c,0004) VR=SQ VM=1 Referenced Beam Sequence</summary>
		DicomConstTags.ReferencedBeamSequence = 0x300c0004;

		/// <summary>(300c,0006) VR=IS VM=1 Referenced Beam Number</summary>
		DicomConstTags.ReferencedBeamNumber = 0x300c0006;

		/// <summary>(300c,0007) VR=IS VM=1 Referenced Reference Image Number</summary>
		DicomConstTags.ReferencedReferenceImageNumber = 0x300c0007;

		/// <summary>(300c,0008) VR=DS VM=1 Start Cumulative Meterset Weight</summary>
		DicomConstTags.StartCumulativeMetersetWeight = 0x300c0008;

		/// <summary>(300c,0009) VR=DS VM=1 End Cumulative Meterset Weight</summary>
		DicomConstTags.EndCumulativeMetersetWeight = 0x300c0009;

		/// <summary>(300c,000a) VR=SQ VM=1 Referenced Brachy Application Setup Sequence</summary>
		DicomConstTags.ReferencedBrachyApplicationSetupSequence = 0x300c000a;

		/// <summary>(300c,000c) VR=IS VM=1 Referenced Brachy Application Setup Number</summary>
		DicomConstTags.ReferencedBrachyApplicationSetupNumber = 0x300c000c;

		/// <summary>(300c,000e) VR=IS VM=1 Referenced Source Number</summary>
		DicomConstTags.ReferencedSourceNumber = 0x300c000e;

		/// <summary>(300c,0020) VR=SQ VM=1 Referenced Fraction Group Sequence</summary>
		DicomConstTags.ReferencedFractionGroupSequence = 0x300c0020;

		/// <summary>(300c,0022) VR=IS VM=1 Referenced Fraction Group Number</summary>
		DicomConstTags.ReferencedFractionGroupNumber = 0x300c0022;

		/// <summary>(300c,0040) VR=SQ VM=1 Referenced Verification Image Sequence</summary>
		DicomConstTags.ReferencedVerificationImageSequence = 0x300c0040;

		/// <summary>(300c,0042) VR=SQ VM=1 Referenced Reference Image Sequence</summary>
		DicomConstTags.ReferencedReferenceImageSequence = 0x300c0042;

		/// <summary>(300c,0050) VR=SQ VM=1 Referenced Dose Reference Sequence</summary>
		DicomConstTags.ReferencedDoseReferenceSequence = 0x300c0050;

		/// <summary>(300c,0051) VR=IS VM=1 Referenced Dose Reference Number</summary>
		DicomConstTags.ReferencedDoseReferenceNumber = 0x300c0051;

		/// <summary>(300c,0055) VR=SQ VM=1 Brachy Referenced Dose Reference Sequence</summary>
		DicomConstTags.BrachyReferencedDoseReferenceSequence = 0x300c0055;

		/// <summary>(300c,0060) VR=SQ VM=1 Referenced Structure Set Sequence</summary>
		DicomConstTags.ReferencedStructureSetSequence = 0x300c0060;

		/// <summary>(300c,006a) VR=IS VM=1 Referenced Patient Setup Number</summary>
		DicomConstTags.ReferencedPatientSetupNumber = 0x300c006a;

		/// <summary>(300c,0080) VR=SQ VM=1 Referenced Dose Sequence</summary>
		DicomConstTags.ReferencedDoseSequence = 0x300c0080;

		/// <summary>(300c,00a0) VR=IS VM=1 Referenced Tolerance Table Number</summary>
		DicomConstTags.ReferencedToleranceTableNumber = 0x300c00a0;

		/// <summary>(300c,00b0) VR=SQ VM=1 Referenced Bolus Sequence</summary>
		DicomConstTags.ReferencedBolusSequence = 0x300c00b0;

		/// <summary>(300c,00c0) VR=IS VM=1 Referenced Wedge Number</summary>
		DicomConstTags.ReferencedWedgeNumber = 0x300c00c0;

		/// <summary>(300c,00d0) VR=IS VM=1 Referenced Compensator Number</summary>
		DicomConstTags.ReferencedCompensatorNumber = 0x300c00d0;

		/// <summary>(300c,00e0) VR=IS VM=1 Referenced Block Number</summary>
		DicomConstTags.ReferencedBlockNumber = 0x300c00e0;

		/// <summary>(300c,00f0) VR=IS VM=1 Referenced Control Point Index</summary>
		DicomConstTags.ReferencedControlPointIndex = 0x300c00f0;

		/// <summary>(300c,00f2) VR=SQ VM=1 Referenced Control Point Sequence</summary>
		DicomConstTags.ReferencedControlPointSequence = 0x300c00f2;

		/// <summary>(300c,00f4) VR=IS VM=1 Referenced Start Control Point Index</summary>
		DicomConstTags.ReferencedStartControlPointIndex = 0x300c00f4;

		/// <summary>(300c,00f6) VR=IS VM=1 Referenced Stop Control Point Index</summary>
		DicomConstTags.ReferencedStopControlPointIndex = 0x300c00f6;

		/// <summary>(300c,0100) VR=IS VM=1 Referenced Range Shifter Number</summary>
		DicomConstTags.ReferencedRangeShifterNumber = 0x300c0100;

		/// <summary>(300c,0102) VR=IS VM=1 Referenced Lateral Spreading Device Number</summary>
		DicomConstTags.ReferencedLateralSpreadingDeviceNumber = 0x300c0102;

		/// <summary>(300c,0104) VR=IS VM=1 Referenced Range Modulator Number</summary>
		DicomConstTags.ReferencedRangeModulatorNumber = 0x300c0104;

		/// <summary>(300e,0002) VR=CS VM=1 Approval Status</summary>
		DicomConstTags.ApprovalStatus = 0x300e0002;

		/// <summary>(300e,0004) VR=DA VM=1 Review Date</summary>
		DicomConstTags.ReviewDate = 0x300e0004;

		/// <summary>(300e,0005) VR=TM VM=1 Review Time</summary>
		DicomConstTags.ReviewTime = 0x300e0005;

		/// <summary>(300e,0008) VR=PN VM=1 Reviewer Name</summary>
		DicomConstTags.ReviewerName = 0x300e0008;

		/// <summary>(4ffe,0001) VR=SQ VM=1 MAC Parameters Sequence</summary>
		DicomConstTags.MACParametersSequence = 0x4ffe0001;

		/// <summary>(5200,9229) VR=SQ VM=1 Shared Functional Groups Sequence</summary>
		DicomConstTags.SharedFunctionalGroupsSequence = 0x52009229;

		/// <summary>(5200,9230) VR=SQ VM=1 Per-frame Functional Groups Sequence</summary>
		DicomConstTags.PerframeFunctionalGroupsSequence = 0x52009230;

		/// <summary>(5400,0100) VR=SQ VM=1 Waveform Sequence</summary>
		DicomConstTags.WaveformSequence = 0x54000100;

		/// <summary>(5400,0110) VR=OB/OW VM=1 Channel Minimum Value</summary>
		DicomConstTags.ChannelMinimumValue = 0x54000110;

		/// <summary>(5400,0112) VR=OB/OW VM=1 Channel Maximum Value</summary>
		DicomConstTags.ChannelMaximumValue = 0x54000112;

		/// <summary>(5400,1004) VR=US VM=1 Waveform Bits Allocated</summary>
		DicomConstTags.WaveformBitsAllocated = 0x54001004;

		/// <summary>(5400,1006) VR=CS VM=1 Waveform Sample Interpretation</summary>
		DicomConstTags.WaveformSampleInterpretation = 0x54001006;

		/// <summary>(5400,100a) VR=OB/OW VM=1 Waveform Padding Value</summary>
		DicomConstTags.WaveformPaddingValue = 0x5400100a;

		/// <summary>(5400,1010) VR=OB/OW VM=1 Waveform Data</summary>
		DicomConstTags.WaveformData = 0x54001010;

		/// <summary>(5600,0010) VR=OF VM=1 First Order Phase Correction Angle</summary>
		DicomConstTags.FirstOrderPhaseCorrectionAngle = 0x56000010;

		/// <summary>(5600,0020) VR=OF VM=1 Spectroscopy Data</summary>
		DicomConstTags.SpectroscopyData = 0x56000020;

		/// <summary>(7fe0,0010) VR=OB/OW VM=1 Pixel Data</summary>
		DicomConstTags.PixelData = 0x7fe00010;

		/// <summary>(fffa,fffa) VR=SQ VM=1 Digital Signatures Sequence</summary>
		DicomConstTags.DigitalSignaturesSequence = 0xfffafffa;

		/// <summary>(fffc,fffc) VR=OB VM=1 Data Set Trailing Padding</summary>
		DicomConstTags.DataSetTrailingPadding = 0xfffcfffc;

		/// <summary>(fffe,e000) VR=NONE VM=1 Item</summary>
		DicomConstTags.Item = 0xfffee000;

		/// <summary>(fffe,e00d) VR=NONE VM=1 Item Delimitation Item</summary>
		DicomConstTags.ItemDelimitationItem = 0xfffee00d;

		/// <summary>(fffe,e0dd) VR=NONE VM=1 Sequence Delimitation Item</summary>
		DicomConstTags.SequenceDelimitationItem = 0xfffee0dd;
			
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	