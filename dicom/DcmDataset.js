
//ivanov enum
var DcmElementType =
{
    "DcmUnsignedShort":1,
    "DcmMultiStringElement": 2,    
    "DcmOtherByte":3,
    "DcmStringElement":4,
    "CFindRequest":5,
    "CFindResponse": 6,
    "DcmUniqueIdentifier": 7,
    "DcmCodeString": 8,
    "DcmDateElementBase": 9,
    "DcmDate": 10,
    "DcmApplicationEntity": 11,
    "DcmAgeString": 12,
    "DcmAttributeTag": 13,
    "DcmDecimalString": 14,
    "DcmDateTime": 15,
    "DcmFloatingPointDouble": 16,
    "DcmFloatingPointSingle": 17,
    "DcmIntegerString": 18,
    "DcmLongString": 19,
    "DcmLongText": 20,
    "DcmOtherWord": 21,
    "DcmOtherFloat": 22,
    "DcmShortString": 23,
    "DcmSignedLong": 24,
    "DcmSignedShort": 25,
    "DcmShortText": 26,
    "DcmTime": 27,
    "DcmUnsignedLong": 28,    
    "DcmUnknown": 29,
    "DcmUnlimitedText": 30
};

//ivannov enum
var DcmValueType = 
{
    "DicomUID":1,
    "String": 2,
    "UShort": 3

};

function DcmDataset()
{
    var el = this;
	//#region Private Members
	/*SortedList<DicomTag, DcmItem>*/ el._items = [];
	//moja varijanta koristi /*SortedList<DicomTag.Card, DcmItem>*/

	var /* long*/ _streamPosition = 0;
	var /*uint*/ _streamLength = 0xffffffff;

	var /*private DicomTransferSyntax*/ _transferSyntax;
	var /*private object*/ _userState;
	//#endregion

	//#region Public Constructors
	//public DcmDataset() : el(DicomTransferSyntax.ExplicitVRLittleEndian) {
	    _transferSyntax = DicomTransferSyntax.ExplicitVRLittleEndian
	//}

	el.DcmDataset = function(/*DicomTransferSyntax*/ transferSyntax) {
		_transferSyntax = transferSyntax;
		//_items = new SortedList<DicomTag, DcmItem>(new DicomTagComparer());
	}

	//public DcmDataset(long streamPosition, uint lengthInStream) : el(streamPosition, lengthInStream, DicomTransferSyntax.ExplicitVRLittleEndian) {
	//}

	//public DcmDataset(long streamPosition, uint lengthInStream, DicomTransferSyntax transferSyntax) {
	//	_streamPosition = streamPosition;
	//	_streamLength = lengthInStream;
	//	_transferSyntax = transferSyntax;
	//	_items = new SortedList<DicomTag, DcmItem>(new DicomTagComparer());
	//}
	//#endregion

	//#region Public Properties
	//// <summary>
	//// Position of el dataset in the source stream.
	//// </summary>
	el.StreamPosition = function()
	{
		return _streamPosition;
	}

	//// <summary>
	//// Length of el dataset in the source stream.
	//// </summary>
	el.StreamLength = function()
	{
		return _streamLength;
	}

	//// <summary>
	//// Transfer syntax used to encode the elements in el dataset.
	//// </summary>
	el.InternalTransferSyntax = function()
	{
		return _transferSyntax;
	}

	//// <summary>
	//// List of the items contained in el dataset.
	//// </summary>
	el.Elements = function()
	{
	    return el._items; //.Values;
	}

	//// <summary>
	//// User state object
	//// </summary>
	el.UserState = function(value)
	{
		if(value != undefined){
			_userState = value;
		}
		else{
			return _userState;
		}		
	}

	el.CalculateGroupWriteLength = function(/*ushort*/ group, /*DicomTransferSyntax*/ syntax, /*DicomWriteOptions*/ options) 
	{	    
	    var /*uint*/ length = 0;

	    for (/*DcmItem*/ itm in el._items)
	    {
	        var num = parseInt(itm);
	        if(isNaN(itm)){continue;}

	        var item = el._items[itm];
	        var tag;// = item.inherit.inherit.inherit.Tag();
	        if (item.inherit.Type == DcmElementType.DcmDateElementBase)
	            tag = item.inherit.inherit.inherit.inherit.Tag();
	        else
	            tag = item.inherit.inherit.inherit.Tag();

	        if (tag.Group() < group || tag.Element() == 0x0000)
	        {
                continue;
	        }

            if (tag.Group() > group){
                return length;
            }

            if (item.inherit.Type == DcmElementType.DcmDateElementBase)
                length += item.inherit.inherit.inherit.CalculateWriteLength(syntax, options);
            else
                length += item.inherit.inherit.CalculateWriteLength(syntax, options);
        }
        
	    return length;
    }
	
	
	el.CalculateWriteLength = function(/* DicomTransferSyntax */ syntax, /* DicomWriteOptions */ options) 
	{
			var length = 0;
			var group = 0xffff;

			for (/*DcmItem*/ itm in el._items) {
			    var num = parseInt(itm);
			    if (isNaN(itm)) { continue; }

			    var item = el._items[itm];
			    var tag;
			    if (item.inherit.Type == DcmElementType.DcmDateElementBase)
			        tag = item.inherit.inherit.inherit.inherit.Tag();
			    else
			        tag = item.inherit.inherit.inherit.Tag();
			    

				if (tag.Element() == 0x0000)
					continue;
				if (tag.Group() != group) {
					group = tag.Group();
					if (Flags.IsSet(options, DicomWriteOptions.CalculateGroupLengths)) {
					    if (syntax.IsExplicitVR) {
					        length += 4 + 2 + 2 + 4;
					    }
					    else {
					        length += 4 + 4 + 4;
					    }
					}
				}
				length += item.inherit.inherit.CalculateWriteLength(syntax, options);
			}
			return length;
		}
	
	
	el.ContainsKey = function(tag)
	{
		var val = el._items[tag.Card()];
		
		if(val != undefined){return true;}
		else{return false;}
		
	}
	
	el.Contains = function(/* DicomTag */ tag) 
	{
		return el.ContainsKey(tag);
	}
	
	el.GetElement = function(/* DicomTag */ tag) 
	{
		var /* DcmItem */ item = null;
		
		var ele = el._items[tag.Card()];

		if (ele != null) {
		    return ele;
		}

		return null;		
	}
	
	el.GetUI = function(/*DicomTag*/ tag) {
	    var /*DcmElement*/ elem = el.GetElement(tag);

		if (elem.Type = DcmElementType.DcmUniqueIdentifier)
		    return elem; // as DcmUniqueIdentifier;

	    if (elem != null)
	        alert("Tried to access element with incorrect VR");

	    return null;
    }
	
	el.GetString = function(/* DicomTag */ tag, /* string */ deflt) 
	{
		return el.GetString1(tag, 0, deflt);
	}

	el.GetString1 = function(/* DicomTag */ tag, /* int */ index, /* string */ deflt) 
	{
	    var /* DcmElement */ elem = el.GetElement(tag);

	    if (elem == null) {
	        return null;
	    }

		if (elem.Name == "DcmStringElement")
		    return elem.GetValue(index);

		if (elem.inherit.Name == "DcmStringElement")
		    return elem.inherit.GetValue(index);
			
		if (elem.Name == "DcmMultiStringElement")
		    return elem.GetValue(index);

		if (elem.inherit.Name == "DcmMultiStringElement")
		    return elem.inherit.GetValue(index);
			
		if (elem != null)
			alert("Tried to access element with incorrect VR");
			
		return deflt;
	}

	el.SetString = function(/*DicomTag*/ tag, /*string*/ value) 
    {
		var /*DcmElement*/ elem = el.GetElement(tag);
		if (elem.inherit.Name == "DcmStringElement") 
		{
	        elem.inherit.SetValue(value);
	        return;
		}

		if (elem.inherit.Name == "DcmMultiStringElement")
		{
	        elem.inherit.SetValue(value);
	        return;
		}

		if (elem != null)
		{
	        console.log("Tried to access element with incorrect VR");
	    }
	    
	    console.log("Element does not exist in Dataset");
    }

	//el.GetElement = function/*(DicomTag*/ tag) {
	//		DcmItem item = null;
	//    if (!_items.TryGetValue(tag, out item))
    //        return null;
    //    if (item is DcmElement)
    //        return item as DcmElement;
    //    return null;
    //}

	el.GetUID = function(/*DicomTag*/ tag) 
	{
		var /*DcmUniqueIdentifier*/ ui = el.GetUI(tag);
	    if (ui != null && ui.inherit.inherit.Length() > 0)
	        return ui.GetUID();
	    return null;
    }

	el.GetUS = function(/*DicomTag*/ tag) 
	{
		var /*DcmElement*/ elem = el.GetElement(tag);

		if (elem == null) {
		    return null;
		}

		if (elem.Type = DcmElementType.DcmUnsignedShort) {
		    return elem /*as DcmUnsignedShort*/;
		}

	    if (elem != null){
	        alert("Tried to access element with incorrect VR");
	    }
	    return null;
	}

	el.GetOB = function(/*DicomTag*/ tag) {
	    var /*DcmElement*/ elem = el.GetElement(tag);

	    if (elem == null) {
	        return null;
	    }

	    if (elem.Type = DcmElementType.DcmOtherByte) {
	        return elem /*as DcmOtherByte*/;
	    }

	    if (elem != null) {
	        alert("Tried to access element with incorrect VR");
	    }
	    return null;
    }

	el.GetUInt16 = function(/*DicomTag*/ tag, /*ushort*/ deflt) 
	{
	    var /*DcmUnsignedShort*/ us = el.GetUS(tag);
	    if (us != null && us.inherit.inherit.Length() > 0){
	        return us.inherit.GetValue();
	    }
	    return deflt;
    }
	
	el.AddItem = function(/* DcmItem */ item) 
	{
	    //_items.splice(item.inherit.inherit.inherit.Tag().Card(),1);
	    //el._items.splice(item.inherit.inherit.inherit.Tag().Card(), 1);
        
	    try
	    {
	        el._items[item.inherit.inherit.inherit.inherit.Tag().Card()] = item;
	    }
	    catch (err)
	    {
	        try
	        {
	            el._items[item.inherit.inherit.inherit.Tag().Card()] = item;
	        }
	        catch (er)
	        {
	            el._items[item.inherit.Tag().Card()] = item;
	        }
	    }

	    //if (item.inherit.Tag != undefined)
	    //{

	    //}
	    //else if (item.inherit.inherit.inherit.Tag == undefined)
	    //{
	    //    el._items[item.inherit.inherit.inherit.inherit.Tag().Card()] = item;
	    //}
	    //else
	    //{
	    //    el._items[item.inherit.inherit.inherit.Tag().Card()] = item;
	    //}

		//item.Endian(InternalTransferSyntax.Endian);
	}
 
	el.AddElement = function(/*DicomTag*/ tag, /*DicomVR*/ vr) 
    {
		var /*DcmElement*/ elem = DcmElement.Create3(tag, vr, null, null, null);

		//if (vr.IsEncodedString()) {
		    //elem.ByteBuffer.Encoding = SpecificCharacterSetEncoding;
		//}

	    el.AddItem(elem);
	    return true;
    }
	
	el.AddElementWithValueUShort = function (/*DicomTag*/ tag, /*ushort*/ value)
	{
	    var /*DicomVR*/ vr = tag.Entry().DefaultVR();

		//if (vr != DicomVR.US)
	    //    console.log("Tried to create element with incorrect VR");

	    if (el.AddElement(tag, vr))
	    {
	        var ele = el.GetUS(tag).inherit;
	        ele.SetValue(value);
	        return true;
	    }
	    return false;
	}

    //string type

	el.AddElementWithValueString = function(/*DicomTag*/ tag, /*DicomUID*/ value) 
    {
		return el.AddElementWithValueString2(tag, value.UID);
	}

	el.AddElementWithValue = function(/*DicomTag*/ tag, type, value)
	{
	    switch (type) {

	        case DcmValueType.DicomUID:
	            el.AddElementWithValueString2(tag, value.UID);
	            break;
            case DcmValueType.String:

	            el.AddElementWithValueString2(tag, value);

	            break;

	        case DcmValueType.UShort:

	            el.AddElementWithValueUShort(tag, value);

	            break;
	    }

		//return el.AddElementWithValue(tag, value.UID);
    }

	el.AddElementWithValueString2 = function(/*DicomTag*/ tag, /*string*/ value) 
	{
		var /*DicomVR*/ vr = tag.Entry().DefaultVR();
	    if (!vr.IsString())
	        console.log("Tried to create element with incorrect VR");
	    if (el.AddElement(tag, vr)) {
	        if (value != null && value != "")
	            el.SetString(tag, value);
	        return true;
	    }
	return false;
	}

	function SaveDicomFieldValue(tag, value, createEmpty) 
	{
	    if (value != null /*&& value != DBNull.Value*/) 
	    {	        
	        if (value.Type != undefined)
	        {
	            el.AddItem(/*(DcmItem)*/value);
	        } 
	        else 
	        {
	            if (!el.Contains(tag)) 
	            {
	                el.AddElement(tag, tag.Entry().DefaultVR());
	            }

	            var /*DcmElement*/ elem = el.GetElement(tag);

	            if (Array.isArray(value)) 
	            {
	                console.log("Array");
	                //if (vtype.GetElementType() != elem.GetValueType())
	                //    console.log("Invalid binding type for Element VR!");

	                //if (elem.GetValueType() == typeof(DateTime))
	                //{
	                //    console.log("SetDateTime");
	                //    //(elem as DcmDateElementBase).SetDateTimes((DateTime[])value);
	                //}
	                //else{
	                //    elem.SetValueObjectArray((object[])value);
	                //}
	            } 
	            else 
	            {
	                //if (elem.VR() == DicomVR.UI.VR() && value.Type == DicomUIDType) 
	                //{
	                //    var /*DicomUID*/ ui = /*(DicomUID)*/value;
	                //    elem.SetValueString(ui.UID);
	                //} 
	                //else if (elem.VR() == DicomVR.UI.VR() && elem.Type == DicomUidType.TransferSyntax)
	                //{
	                //    //DicomTransferSyntax ts = (DicomTransferSyntax)value;
	                //    //elem.SetValueString(ts.UID.UID);
	                //}
	                if (elem.Type == DcmElementType.DcmDateElementBase)
	                {
	                    var /*DcmDateRange*/ dr = /*(DcmDateRange)*/value;
	                    elem.SetDateTimeRange(dr);
	                } 
	                //else if (vtype != elem.GetValueType()) 
	                //{
	                //    if (typeof(value) == "string") 
	                //    {
	                //        elem.SetValueString(/*(string)*/value);
	                //    } 
	                //    else
	                //    {
	                //        console.log("Invalid binding type for Element VR!");
	                //    }
	                //} 
	                else 
	                {
	                    //treba biti SetValueObject
	                    elem.inherit.SetValue(value);
	                }
	            }
	        }
	    } 
	    else 
	    {
	        if (el.Contains(tag)) 
	        {
	            el.GetElement(tag).ByteBuffer().Clear();
	        } 
	        else if (createEmpty) 
	        {
	            el.AddElement(tag, tag.Entry().DefaultVR());
	        }
        }
    }

	el.SaveDicomFields = function(obj)
    {
	    //FieldInfo[] fields = obj.GetType().GetFields();

		//foreach (FieldInfo field in fields) 
        //{
		//    if (field.IsDefined(typeof(DicomFieldAttribute), true)) 
	    //    {
		//	    DicomFieldAttribute dfa = (DicomFieldAttribute)field.GetCustomAttributes(typeof(DicomFieldAttribute), true)[0];
	    //        object value = field.GetValue(obj);
	    //        SaveDicomFieldValue(dfa.Tag, value, dfa.CreateEmptyElement);
        //    }
        //}

        //PropertyInfo[] properties = obj.GetType().GetProperties();
        //foreach (PropertyInfo property in properties) 
        //{
		//    if (property.IsDefined(typeof(DicomFieldAttribute), true)) 
        //    {
			    //var /*DicomFieldAttribute*/ dfa = (DicomFieldAttribute)property.GetCustomAttributes(typeof(DicomFieldAttribute), true)[0];
                //var value = property.GetValue(obj, null);
	    SaveDicomFieldValue(DicomTags.PatientID, null, true);
	    SaveDicomFieldValue(DicomTags.PatientsName, null, true);
	    //SaveDicomFieldValue(DicomTags.PatientsSex, null, true);
	    SaveDicomFieldValue(DicomTags.StudyDate, null, true);
	    SaveDicomFieldValue(DicomTags.StudyTime, null, true);
	    SaveDicomFieldValue(DicomTags.StudyID, null, true);
	    SaveDicomFieldValue(DicomTags.AccessionNumber, null, true);
	    SaveDicomFieldValue(DicomTags.StudyInstanceUID, null, true);
	    //SaveDicomFieldValue(DicomTags.Modality, null, true);
	    SaveDicomFieldValue(DicomTags.ModalitiesInStudy, null, true);
	    SaveDicomFieldValue(DicomTags.StudyDescription, null, true);
	    SaveDicomFieldValue(DicomTags.InstitutionName, null, true);

        //    }
        //}
    }	

}


























