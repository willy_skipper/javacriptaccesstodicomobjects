function Wildcard()
{

}
    /// <summary>
    /// Array of valid wildcards
/// </summary>
Wildcard.Wildcards = ["*", "?" ];

    /// <summary>
    /// Returns true if the string matches the pattern which may contain * and ? wildcards.
    /// Matching is done without regard to case.
    /// </summary>
    /// <param name="pattern"></param>
    /// <param name="s"></param>
    /// <returns></returns>
Wildcard.Match = function(/*string*/ pattern, /*string*/ s) 
{
    return Match2(pattern, s, false);
}

/// <summary>
/// Returns true if the string matches the pattern which may contain * and ? wildcards.
/// </summary>
/// <param name="pattern"></param>
/// <param name="s"></param>
/// <param name="caseSensitive"></param>
/// <returns></returns>
Wildcard.Match2 = function(/*string*/ pattern, /*string*/ s,/* bool*/ caseSensitive) 
{
    // if not concerned about case, convert both string and pattern
    // to lower case for comparison
    if (!caseSensitive) 
    {
	    pattern = pattern.ToLower();    
	    s = s.ToLower();
    }

    // if pattern doesn't actually contain any wildcards, use simple equality
    if (pattern.IndexOf(Wildcards) == -1)
        return (s == pattern);

    // otherwise do pattern matching
    var i = 0;
    var j = 0;
    while (i < s.length && j < pattern.length && pattern[j] != '*') 
    {
        if ((pattern[j] != s[i]) && (pattern[j] != '?')) 
        {
            return false;
        }

        i++;
        j++;
    }

    // if we have reached the end of the pattern without finding a * wildcard,
    // the match must fail if the string is longer or shorter than the pattern
    if (j == pattern.Length)
        return s.Length == pattern.Length;

    var cp = 0;
    var mp = 0;
    while (i < s.Length) 
    {
        if (j < pattern.length && pattern[j] == '*') {
            if ((j++) >= pattern.length) {
                return true;
            }
            mp = j;
            cp = i + 1;
        } else if (j < pattern.length && (pattern[j] == s[i] || pattern[j] == '?')) {
            j++;
            i++;
        } else {
            j = mp;
            i = cp++;
        }
    }

    while (j < pattern.length && pattern[j] == '*') {
        j++;
    }

    return j >= pattern.length;
}