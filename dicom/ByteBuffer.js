function ByteBuffer()
{
		//#region Private Members
		var /*MemoryStream*/ _ms;
		var /*byte[]*/ _data;
		var /*BinaryReader*/ _br;
		var /*BinaryWriter*/ _bw;
		var /*Endian*/ _endian;
		var /*Encoding*/ _encoding;
		var /*FileSegment*/ _segment;
    //#endregion
		this.Lento;

		//#region Public Constructors
		//public ByteBuffer() : this(Endian.LocalMachine) {
		//}

		this.ByteBuffer1 = function (/*Endian*/ endian) {
		    _endian = endian;
		    _encoding = DcmEncoding.Default;
		};

		this.ByteBuffer2 = function (/*byte[]*/ data) {
		    this.Lento = _data.byteLength;
		};

		this.ByteBuffer3 = function (/*byte[]*/ data, /*Endian*/ endian) {
		    _data = data;
		    _endian = endian;
		    _encoding = DcmEncoding.Default;
		    this.Lento = _data.byteLength;
		};

		//public ByteBuffer(FileSegment segment) : this(segment, Endian.LocalMachine) {
		//}

		//public ByteBuffer(FileSegment segment, Endian endian) {
		//	_segment = segment;
		//	_endian = endian;
		//	_encoding = DcmEncoding.Default;
		//}
		//#endregion

		//#region Public Properties
		//this.MemoryStream Stream {
		//	get {
		//		if (_ms == null) {
		//			if (_data != null) {
		//				_ms = new MemoryStream(_data);
		//				_data = null;
		//			} else if (_segment != null) {
		//				_ms = new MemoryStream(_segment.GetData());
		//			} else {
		//				_ms = new MemoryStream();
		//			}
		//		}
		//		return _ms;
		//	}
		//}

		//public BinaryReader Reader {
		//	get {
		//		if (_br == null) {
		//			_br = EndianBinaryReader.Create(Stream, Encoding, Endian);
		//		}
		//		return _br;
		//	}
		//}

		//public BinaryWriter Writer {
		//	get {
		//		if (_bw == null) {
		//			_bw = EndianBinaryWriter.Create(Stream, Encoding, Endian);
		//			_segment = null;
		//		}
		//		return _bw;
		//	}
		//}

		//public Endian Endian {
		//	get { return _endian; }
		//	set {
		//		_endian = value;
		//		_br = null;
		//		_bw = null;
		//	}
		//}

		//public Encoding Encoding {
		//	get { return _encoding; }
		//	set { _encoding = value; }
		//}

		this.Length = function() 
		{
			//get {
				//if (_ms != null)
				//	return _ms.Length;
				if (_data != null)
					return _data.byteLength;
				//if (_segment != null)
				//	return (int)_segment.Length;
				return 0;
			//}
		}

		

		//public bool CanUnload {
		//	get { return _segment != null; }
		//}

		//public bool IsDeferred {
		//	get { return CanUnload && (_ms != null || _data != null); }
		//}
		////#endregion

		//#region Public Functions
		//public void Preload() {
		//	if (_segment != null && _data == null && _ms == null) {
		//		ToBytes();
		//	}
		//}

		//public void Unload() {
		//	if (_segment != null) {
		//		_ms = null;
		//		_br = null;
		//		_bw = null;
		//		_data = null;
		//	}
		//}

		//public ByteBuffer Clone() {
		//	ByteBuffer clone = null;
		//	if (_data != null)
		//		clone = new ByteBuffer((byte[])_data.Clone(), Endian);
		//	else if (_segment != null)
		//		clone = new ByteBuffer(_segment, Endian);
		//	else
		//		clone = new ByteBuffer((byte[])ToBytes().Clone(), Endian);
		//	clone.Encoding = Encoding;
		//	return clone;
		//}

		//public void Clear() {
		//	_ms = null;
		//	_br = null;
		//	_bw = null;
		//	_data = null;
		//	_segment = null;
		//}

		//public void Chop(int count) {
		//	_segment = null;
		//	int len = (int)Stream.Length;
		//	if (len <= count) {
		//		Stream.SetLength(0);
		//		return;
		//	}
		//	byte[] bytes = GetChunk(count, len - count);
		//	Stream.SetLength(0);
		//	Stream.Position = 0;
		//	Stream.Write(bytes, 0, bytes.Length);
		//}

		//public void Append(byte[] buffer, int offset, int count) {
		//	long pos = Stream.Position;
		//	Stream.Seek(0, SeekOrigin.End);
		//	Stream.Write(buffer, offset, count);
		//	Stream.Position = pos;
		//	_segment = null;
		//}

		this.CopyFrom = function(/*Stream*/ s, /*int*/ count) 
		{
			_ms = null;
			_segment = null;
			_br = null;
			_bw = null;

			var /*int*/ read = 0;
			//_data = new byte[count];
			_data = new Uint8Array(count);

			for (var i = 0; i < count; i++) {
			    _data[i] = s.getUint8();
			}

			//while (read < count) {
			//	var /*int*/ rd = s.Read(_data, read, count - read);
			//	if (rd == 0)
			//		return read;
			//	read += rd;
			//}

			this.Lento = _data.byteLength;

			//console.log(_data.byteLength);

			return read;
		}

		this.BlockCopy = function(startPos1, startPos2, count)
		{
		    var _val = new Uint8Array(count);

		    for (var i = startPos1; i < count; i++)
		    {
		        _val[startPos2] = _data[i];
		        startPos2++;
		    }

		    return _val;
		}

		this.BlockCopy2 = function (vals, startPos1, buf, startPos2, count) {
		    
		    var tmp = new Uint16Array(vals);
		    var _val = new Uint8Array(tmp.buffer);

		    for (var i = startPos1; i < count; i++) {
		        buf[startPos2] = _val[i];
		        startPos2++;
		    }

		    return vals;
		}

		this.CopyTo = function(/*Stream*/ s) {
			if (_ms != null) {
				_ms.WriteTo(s);
				return;
			}
			if (_data != null) {
			    s.Write("buffer",_data);//, 0, _data.byteLength);
				return;
			}
			if (_segment != null) {
				_segment.WriteTo(s);
				return;
			}
		}

		//public void CopyTo(int srcOffset, Stream s, int dstOffset, int count) {
		//	byte[] bytes = new byte[count];
		//	Stream.Position = srcOffset;
		//	Stream.Read(bytes, 0, count);
		//	s.Write(bytes, 0, count);
		//}

		//public void CopyTo(int srcOffset, byte[] buffer, int dstOffset, int count) {
		//	if (_ms != null) {
		//		Stream.Position = srcOffset;
		//		Stream.Read(buffer, dstOffset, count);
		//		return;
		//	}
		//	if (_data != null) {
		//		Buffer.BlockCopy(_data, srcOffset, buffer, dstOffset, count);
		//		return;
		//	}
		//	if (_segment != null) {
		//		using (FileStream fs = _segment.OpenStream()) {
		//			fs.Seek(srcOffset, SeekOrigin.Current);
		//			fs.Read(buffer, dstOffset, count);
		//		}
		//		return;
		//	}
		//}

		//public byte[] GetChunk(int srcOffset, int count) {
		//	byte[] chunk = new byte[count];
		//	CopyTo(srcOffset, chunk, 0, count);
		//	return chunk;
		//}

		this.FromBytes = function (/*byte[]*/ bytes)
		{
			_data = bytes;
			_ms = null;
			_segment = null;
		}

		//public byte[] ToBytes() {
		//	if (_data != null)
		//		return _data;
		//	if (_ms != null) {
		//		_data = _ms.ToArray();
		//		_ms = null;
		//		_br = null;
		//		_bw = null;
		//		return _data;
		//	}
		//	if (_segment != null) {
		//		_data = _segment.GetData();
		//		return _data;
		//	}
		//	return new byte[0];
		//}

		//public ushort[] ToUInt16s() {
		//	byte[] bytes = ToBytes();
		//	int count = bytes.Length / 2;
		//	ushort[] words = new ushort[count];
		//	for (int i = 0, p = 0; i < count; i++, p += 2) {
		//		words[i] = unchecked((ushort)((bytes[p] << 8) + bytes[p + 1]));
		//	}
		//	return words;
		//}

		//public short[] ToInt16s() {
		//	byte[] bytes = ToBytes();
		//	int count = bytes.Length / 2;
		//	short[] words = new short[count];
		//	for (int i = 0, p = 0; i < count; i++, p += 2) {
		//		words[i] = unchecked((short)((bytes[p] << 8) + bytes[p + 1]));
		//	}
		//	return words;
		//}

		//public uint[] ToUInt32s() {
		//	byte[] bytes = ToBytes();
		//	int count = bytes.Length / 4;
		//	uint[] dwords = new uint[count];
		//	for (int i = 0, p = 0; i < count; i++, p += 4) {
		//		dwords[i] = BitConverter.ToUInt32(bytes, p);
		//	}
		//	return dwords;
		//}

		this.GetString = function()
		{
		    if (_data != undefined)
		        return ab2str(_data.buffer);
		    else {
		        console.log("empty buffer");
		        return "";
		    }
        }

		//public void SetString(string val) {
		//	_data = _encoding.GetBytes(val);
		//	_ms = null;
		//	_segment = null;
        //}


		this.SetString = function (/*string*/ val, /*byte*/ pad)
		{
			//var /*int*/ count = _encoding.GetByteCount(val);
			//if ((count & 1) == 1)
			//	count++;

			var /*byte[]*/ ab = str2ab(val);
			//if (_encoding.GetBytes(val, 0, val.Length, bytes, 0) < count)
		    //	bytes[count - 1] = pad;
			
			var bytes = new Uint8Array(ab);

			_data = bytes;
			_ms = null;
			_segment = null;
		}

		//public void Swap(int bytesToSwap) {
		//	Endian.SwapBytes(bytesToSwap, ToBytes());
		//}

		//public void Swap2() {
		//	Endian.SwapBytes2(ToBytes());
		//}
		//public void Swap4() {
		//	Endian.SwapBytes4(ToBytes());
		//}
		//public void Swap8() {
		//	Endian.SwapBytes8(ToBytes());
		//}
		//#endregion
	}