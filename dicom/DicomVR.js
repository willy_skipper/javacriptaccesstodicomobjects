var DicomVrRestriction = {
	"NotApplicable":0,
	"Fixed":1,
	"Maximum":2,
	"Any":3
};


	function DicomVR(/* string */ value, /* string */ desc, /* bool */ isString, /* bool */ isEncodedString, /* bool */ is16BitLength, 
			/* byte */ padding, /* int */ maxLength, /* int */ unitSize, /* DicomVrRestriction */ restriction)
	{
		// #region var Members
		var /* string */ _strval;
		var /* int */ _hash;
		var /* string */ _desc;
		var /* bool */ _is16BitLength;
		var /* bool */ _isString;
		var /* bool */ _isEncodedString;
		var /* byte */ _padding;
		var /* int */ _maxLength;
		var /* int */ _unitSize;
		var /* DicomVrRestriction */ _restriction;

		// function DicomVR() 
		// {
		// }

		// function DicomVR(/* string */ value, /* string */ desc, /* bool */ isString, /* bool */ isEncodedString, /* bool */ is16BitLength, 
			// /* byte */ padding, /* int */ maxLength, /* int */ unitSize, /* DicomVrRestriction */ restriction) 
		// {
			_strval = value;
			_hash = ((value[0] << 8) | value[1]);
			_desc = desc;
			_isString = isString;
			_isEncodedString = isEncodedString;
			_is16BitLength = is16BitLength;
			_padding = padding;
			//_padding = PadZero;
			_maxLength = maxLength;
			_unitSize = unitSize;
			_restriction = restriction;
		// }
		// #endregion

		// #region Public Properties
		this.VR = function()
		{
			return _strval;
		}

		this.Description = function() {
			return _desc;
		}

		this.IsString = function() {
			return _isString;
		}

		// <summary>
		// Specific Character Set applies to this VR
		// </summary>
		this.IsEncodedString = function() {
			return _isEncodedString;
		}

		this.Is16BitLengthField = function() {
			return _is16BitLength;
		}

		this.Padding = function() {
			return _padding; 
		}

		this.MaxmimumLength  = function(){
			return _maxLength; 
		}

		this.UnitSize  = function(){
			return _unitSize; 
		}

		this.Restriction = function()
		{
			return _restriction;
		}
		// #endregion

 		this.ToString = function() 
		{
			return _strval + "-" +  _desc;
		}		
		
	}

		var /* const byte */ PadSpace = 0x20;
		var /* const byte */ PadZero = 0x00;

		// <summary>No VR</summary>
		DicomVR.NONE = new DicomVR("NONE", "No VR", false, false, false, PadZero, 0, 0, DicomVrRestriction.NotApplicable);

		// <summary>Application Entity</summary>
		DicomVR.AE = new DicomVR("AE", "Application Entity", true, false, true, PadSpace, 16, 1, DicomVrRestriction.Maximum);

		// <summary>Age String</summary>
		DicomVR.AS = new DicomVR("AS", "Age String", true, false, true, PadSpace, 4, 1, DicomVrRestriction.Fixed);

		// <summary>Attribute Tag</summary>
		DicomVR.AT = new DicomVR("AT", "Attribute Tag", false, false, true, PadZero, 4, 4, DicomVrRestriction.Fixed);

		// <summary>Code String</summary>
		DicomVR.CS = new DicomVR("CS", "Code String", true, false, true, PadSpace, 16, 1, DicomVrRestriction.Maximum);

		// <summary>Date</summary>
		DicomVR.DA = new DicomVR("DA", "Date", true, false, true, PadSpace, 8, 1, DicomVrRestriction.Fixed);

		// <summary>Decimal String</summary>
		DicomVR.DS = new DicomVR("DS", "Decimal String", true, false, true, PadSpace, 16, 1, DicomVrRestriction.Maximum);

		// <summary>Date Time</summary>
		DicomVR.DT = new DicomVR("DT", "Date Time", true, false, true, PadSpace, 26, 1, DicomVrRestriction.Maximum);

		// <summary>Floating Point Double</summary>
		DicomVR.FD = new DicomVR("FD", "Floating Point Double", false, false, true, PadZero, 8, 8, DicomVrRestriction.Fixed);

		// <summary>Floating Point Single</summary>
		DicomVR.FL = new DicomVR("FL", "Floating Point Single", false, false, true, PadZero, 4, 4, DicomVrRestriction.Fixed);

		// <summary>Integer String</summary>
		DicomVR.IS = new DicomVR("IS", "Integer String", true, false, true, PadSpace, 12, 1, DicomVrRestriction.Maximum);

		// <summary>Long String</summary>
		DicomVR.LO = new DicomVR("LO", "Long String", true, true, true, PadSpace, 64, 1, DicomVrRestriction.Maximum);

		// <summary>Long Text</summary>
		DicomVR.LT = new DicomVR("LT", "Long Text", true, true, true, PadSpace, 10240, 1, DicomVrRestriction.Maximum);

		// <summary>Other Byte</summary>
		DicomVR.OB = new DicomVR("OB", "Other Byte", false, false, false, PadZero, 0, 1, DicomVrRestriction.Any);

		// <summary>Other Float</summary>
		DicomVR.OF = new DicomVR("OF", "Other Float", false, false, false, PadZero, 0, 4, DicomVrRestriction.Any);

		// <summary>Other Word</summary>
		DicomVR.OW = new DicomVR("OW", "Other Word", false, false, false, PadZero, 0, 2, DicomVrRestriction.Any);

		// <summary>Person Name</summary>
		DicomVR.PN = new DicomVR("PN", "Person Name", true, true, true, PadSpace, 64, 1, DicomVrRestriction.Maximum);

		// <summary>Short String</summary>
		DicomVR.SH = new DicomVR("SH", "Short String", true, true, true, PadSpace, 16, 1, DicomVrRestriction.Maximum);

		// <summary>Signed Long</summary>
		DicomVR.SL = new DicomVR("SL", "Signed Long", false, false, true, PadZero, 4, 4, DicomVrRestriction.Fixed);

		// <summary>Sequence of Items</summary>
		DicomVR.SQ = new DicomVR("SQ", "Sequence of Items", false, false, false, PadZero, 0, 0, DicomVrRestriction.NotApplicable);

		// <summary>Signed Short</summary>
		DicomVR.SS = new DicomVR("SS", "Signed Short", false, false, true, PadZero, 2, 2, DicomVrRestriction.Fixed);

		// <summary>Short Text</summary>
		DicomVR.ST = new DicomVR("ST", "Short Text", true, true, true, PadSpace, 1024, 1, DicomVrRestriction.Maximum);

		// <summary>Time</summary>
		DicomVR.TM = new DicomVR("TM", "Time", true, false, true, PadSpace, 16, 1, DicomVrRestriction.Maximum);

		// <summary>Unique Identifier</summary>
		DicomVR.UI = new DicomVR("UI", "Unique Identifier", true, false, true, PadZero, 64, 1, DicomVrRestriction.Maximum);

		// <summary>Unsigned Long</summary>
		DicomVR.UL = new DicomVR("UL", "Unsigned Long", false, false, true, PadZero, 4, 4, DicomVrRestriction.Fixed);

		// <summary>Unknown</summary>
		DicomVR.UN = new DicomVR("UN", "Unknown", false, false, false, PadZero, 0, 1, DicomVrRestriction.Any);

		// <summary>Unsigned Short</summary>
		DicomVR.US = new DicomVR("US", "Unsigned Short", false, false, true, PadZero, 2, 2, DicomVrRestriction.Fixed);

		/// <summary>Unlimited Text</summary>
		DicomVR.UT = new DicomVR("UT", "Unlimited Text", true, true, false, PadSpace, 0, 1, DicomVrRestriction.Any);

		// #region Static Methods
		// public static List<DicomVR> Entries = new List<DicomVR>();
		DicomVR.Entries = [];

		
			// #region Load VRs
			DicomVR.Entries.push(DicomVR.AE);
			DicomVR.Entries.push(DicomVR.AS);
			DicomVR.Entries.push(DicomVR.AT);
			DicomVR.Entries.push(DicomVR.CS);
			DicomVR.Entries.push(DicomVR.DA);
			DicomVR.Entries.push(DicomVR.DS);
			DicomVR.Entries.push(DicomVR.DT);
			DicomVR.Entries.push(DicomVR.FD);
			DicomVR.Entries.push(DicomVR.FL);
			DicomVR.Entries.push(DicomVR.IS);
			DicomVR.Entries.push(DicomVR.LO);
			DicomVR.Entries.push(DicomVR.LT);
			DicomVR.Entries.push(DicomVR.OB);
			DicomVR.Entries.push(DicomVR.OF);
			DicomVR.Entries.push(DicomVR.OW);
			DicomVR.Entries.push(DicomVR.PN);
			DicomVR.Entries.push(DicomVR.SH);
			DicomVR.Entries.push(DicomVR.SL);
			DicomVR.Entries.push(DicomVR.SQ);
			DicomVR.Entries.push(DicomVR.SS);
			DicomVR.Entries.push(DicomVR.ST);
			DicomVR.Entries.push(DicomVR.TM);
			DicomVR.Entries.push(DicomVR.UI);
			DicomVR.Entries.push(DicomVR.UL);
			DicomVR.Entries.push(DicomVR.UN);
			DicomVR.Entries.push(DicomVR.US);
			DicomVR.Entries.push(DicomVR.UT);
			// #endregion
	

		DicomVR.Lookup = function(/* ushort */ vr) 
		{
			if (vr == 0x0000)
			{
				return DicomVR.NONE;
			}
			
			var chr = [2];		
			chr[0] = String.fromCharCode(vr >> 8);
			chr[1] = String.fromCharCode(vr);
			
			//return Lookup(new char[] { (char)(vr >> 8), (char)(vr) });
			return DicomVR.Lookup1(chr);
		}

		DicomVR.Lookup1 = function(/* char[] */ vr) {
			return DicomVR.Lookup2(vr);
		}

		DicomVR.Lookup2 = function(/* string */ vr) 
		{
			switch (vr) 
			{
			case "NONE": return DicomVR.NONE;			
			case "AE": return DicomVR.AE;
			case "AS": return DicomVR.AS;
			case "AT": return DicomVR.AT;
			case "CS": return DicomVR.CS;
			case "DA": return DicomVR.DA;
			case "DS": return DicomVR.DS;
			case "DT": return DicomVR.DT;
			case "FD": return DicomVR.FD;
			case "FL": return DicomVR.FL;
			case "IS": return DicomVR.IS;
			case "LO": return DicomVR.LO;
			case "LT": return DicomVR.LT;
			case "OB": return DicomVR.OB;
			case "OF": return DicomVR.OF;
			case "OW": return DicomVR.OW;
			case "PN": return DicomVR.PN;
			case "SH": return DicomVR.SH;
			case "SL": return DicomVR.SL;
			case "SQ": return DicomVR.SQ;
			case "SS": return DicomVR.SS;
			case "ST": return DicomVR.ST;
			case "TM": return DicomVR.TM;
			case "UI": return DicomVR.UI;
			case "UL": return DicomVR.UL;
			case "UN": return DicomVR.UN;
			case "US": return DicomVR.US;
			case "UT": return DicomVR.UT;
			default:
				return DicomVR.UN;
			}
		}
		// #endregion
	