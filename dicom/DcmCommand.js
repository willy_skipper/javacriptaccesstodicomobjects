﻿
var DcmPriority =
{
	"Low":0x0002,
	"Medium":0x0000,
	"High":0x0001
};

var DcmCommandField =
{
	"CStoreRequest":0x0001,
	"CStoreResponse":0x8001,
	"CGetRequest":0x0010,
	"CGetResponse":0x8010,
	"CFindRequest":0x0020,
	"CFindResponse":0x8020,
	"CMoveRequest":0x0021,
	"CMoveResponse":0x8021,
	"CEchoRequest":0x0030,
	"CEchoResponse":0x8030,
	"NEventReportRequest":0x0100,
	"NEventReportResponse":0x8100,
	"NGetRequest":0x0110,
	"NGetResponse":0x8110,
	"NSetRequest":0x0120,
	"NSetResponse":0x8120,
	"NActionRequest":0x0130,
	"NActionResponse":0x8130,
	"NCreateRequest":0x0140,
	"NCreateResponse":0x8140,
	"NDeleteRequest":0x0150,
	"NDeleteResponse":0x8150,
	"CCancelRequest":0x0FFF
};

function DcmCommand()
{

    this.inherit = new DcmDataset();
    this.inherit.DcmDataset(DicomTransferSyntax.ImplicitVRLittleEndian);
	
    this.AffectedSOPClassUID = function(value){
        if(value == undefined) {
            var sop = this.inherit.GetUID(DicomTags.AffectedSOPClassUID);
            console.log("Affected SOP: " + sop);
            return sop;
        }
        else {
            this.inherit.AddElementWithValueString(DicomTags.AffectedSOPClassUID, value);
        }
    }

    this.RequestedSOPClassUID = function (value) {
        if(value == undefined) {
            return this.inherit.GetUID(DicomTags.RequestedSOPClassUID);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.RequestedSOPClassUID, value);
        }
    }

    this.CommandField  = function(value){
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.CommandField, 0);
        }
        else 
        {
            this.inherit.AddElementWithValueUShort(DicomTags.CommandField,value);
        }
    }

    this.MessageID = function(value){
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.MessageID, 0);
        }
        else {
            this.inherit.AddElementWithValueUShort(DicomTags.MessageID, value);
        }
    }

    this.MessageIDBeingRespondedTo  = function(value){
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.MessageIDBeingRespondedTo, 0);
        }
        else {
            this.inherit.AddElementWithValueUShort(DicomTags.MessageIDBeingRespondedTo, value);
        }
    }

    this.MoveDestinationAE  = function(value){
        if(value == undefined) {
            return this.inherit.GetString(DicomTags.MoveDestination, null);
        }
        else {
            this.inherit.AddElementWithValueString2(DicomTags.MoveDestination, value);
        }
    }

    this.Priority = function (value) {
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.Priority, 0);
        }
        else {
            this.inherit.AddElementWithValueUShort(DicomTags.Priority, value);
        }
    }

    this.HasDataset  = function(value){
        if(value == undefined) {
            return this.DataSetType() != 0x0101;
        }
        else {
            var val = value ? 0x0202 : 0x0101;
            this.DataSetType(val);
        }
    }

    this.DataSetType = function (value) {
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.DataSetType, 0x0101);
        }
        else {
            this.inherit.AddElementWithValueUShort(DicomTags.DataSetType, value);
        }
    }

    this.Status = function (value)
    {
        if(value == undefined) {
            var /* DcmStatus */ status = DcmStatus.Lookup(this.inherit.GetUInt16(DicomTags.Status, 0x0211));
            var comment = this.ErrorComment();
            if (comment == null || comment == "")
                //status = new DcmStatus(status, comment);
                return status;
        }
        else {
            this.inherit.AddElementWithValueUShort(DicomTags.Status, value.Code);
        }
    }

    this.OffendingElement = function (value) {
        if(value == undefined) {
            return this.inherit.GetDcmTag(DicomTags.OffendingElement);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.OffendingElement, value);
        }
    }

    this.ErrorComment = function (value) {
        if(value == undefined) {
            return this.inherit.GetString(DicomTags.ErrorComment, null);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.ErrorComment, value);
        }
    }

    this.ErrorID = function (value) {
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.ErrorID, 0);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.ErrorID, value);
        }
    }

    this.AffectedSOPInstanceUID = function (value) {
        if(value == undefined) {
            return this.inherit.GetUID(DicomTags.AffectedSOPInstanceUID);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.AffectedSOPInstanceUID, value);
        }
    }

    this.RequestedSOPInstanceUID = function (value) {
        if(value == undefined) {
            return this.inherit.GetUID(DicomTags.RequestedSOPInstanceUID);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.RequestedSOPInstanceUID, value);
        }
    }

    this.EventTypeID = function (value) {
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.EventTypeID, 0);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.EventTypeID, value);
        }
    }

    this.AttributeIdentifierList = function (value) {
        if(value == undefined) {
            return this.inherit.GetAT(DicomTags.AttributeIdentifierList);
        }
        else {
            AddItem(value);
        }
    }

    this.ActionTypeID = function (value) {
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.ActionTypeID, 0);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.ActionTypeID, value);
        }
    }

    this.NumberOfRemainingSuboperations = function (value) {
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.NumberOfRemainingSuboperations, 0);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.NumberOfRemainingSuboperations, value);
        }
    }

    this.NumberOfCompletedSuboperations = function (value) {
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.NumberOfCompletedSuboperations, 0);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.NumberOfCompletedSuboperations, value);
        }
    }

    this.NumberOfFailedSuboperations = function (value) {
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.NumberOfFailedSuboperations, 0);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.NumberOfFailedSuboperations, value);
        }
    }

    this.NumberOfWarningSuboperations = function (value) {
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.NumberOfWarningSuboperations, 0);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.NumberOfWarningSuboperations, value);
        }
    }

    this.MoveOriginatorAE = function (value) {
        if(value == undefined) {
            return this.inherit.GetString(DicomTags.MoveOriginatorApplicationEntityTitle, null);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.MoveOriginatorApplicationEntityTitle, value);
        }
    }

    this.MoveOriginatorMessageID = function(value) {
        if(value == undefined) {
            return this.inherit.GetUInt16(DicomTags.MoveOriginatorMessageID, 0);
        }
        else {
            this.inherit.AddElementWithValue(DicomTags.MoveOriginatorMessageID, value);
        }
    }
    //#endregion

    this.Contains = function(/*DicomTag*/ tag) {

        var tg = this.inherit._items[tag.Card()];

        if(tg != null){
            return true;
        }
        else return false;

        //return _items.ContainsKey(tag);
    }

    //#region this.Methods
    // this.GetErrorString = function() 
    // {
    // /*StringBuilder*/ var sb = new StringBuilder();

    // if (Contains(DicomTags.ErrorComment))
    // {
    // sb.Append(ErrorComment);
    // }
    // else 
    // {
    // sb.Append("Unspecified");
    // }

    // if (Contains(DicomTags.OffendingElement)) 
    // {
    // DcmAttributeTag at = GetAT(DicomTags.OffendingElement);
    // sb.Append(" [");
    // DicomTag[] tags = at.GetValues();
    // for (int i = 0; i < tags.Length; i++) 
    // {
    // if (i > 0){
    // sb.Append("; ");
    // }
    // sb.Append(tags[i].ToString());
    // sb.Append(" ");
    // sb.Append(tags[i].Entry.Name);
    // }
    // sb.Append("]");
    // }

    // return sb.ToString();
    // }
    //#endregion
}

