function ChunkStream()
{
	//#region Private Members
	var /* long */ _position;
	var /* long */ _length;
	var /* int */ _current;
	var /* int */ _offset;
	var /* List<byte[]> */ _chunks;
	//#endregion

	//#region Public Constructors
	// public ChunkStream() : base() {
		_current = 0;
		_offset = 0;
		_position = 0;
		_length = 0;
		_chunks = []; /* new List<byte[]>(); */
	// }
	//#endregion

	//#region Public Members
	this.AddChunk = function(/* byte[] */ chunk) 
	{
		_chunks.push(chunk);		
		_length += chunk.byteLength; // length;
	}

	this.Stream = function () {
	    //todo
	    return _chunks[0];
	}

	this.Clear = function() {
		_current = 0;
		_offset = 0;
		_position = 0;
		_length = 0;
		_chunks.Clear();
	}
	//#endregion

	//#region Stream Members
	this.CanRead = function(){
		return true;
	}

	this.CanSeek = function(){
		return true;
	}

	this.CanWrite = function(){
		return false;
	}

	this.Flush = function(){
	}

	this.Length = function(){
		return _length;
	}	
	
}