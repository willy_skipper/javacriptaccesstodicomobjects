function DcmDateRange() {
	//#region Private Members
	var _dtBegin;
	var _dtEnd;
	//#endregion

	//#region Public Constructors
	/// <summary>
	/// Initializes a new instance of the <see cref="DcmDateRange"/> class.
	/// </summary>
	//public DcmDateRange() {
	_dtBegin = DateTime.MinValue;
	_dtEnd = DateTime.MinValue;
	//}

	/// <summary>
	/// Initializes a new instance of the <see cref="DcmDateRange"/> class.
	/// </summary>
	/// <param name="dt">Start and end date</param>
	this.DcmDateRange = function(/*DateTime*/ dt) {
		_dtBegin = _dtEnd = dt;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="DcmDateRange"/> class.
	/// </summary>
	/// <param name="begin">Start date</param>
	/// <param name="end">End date</param>
	this.DcmDateRange1 = function(/*DateTime*/ begin, /*DateTime*/ end) {
		_dtBegin = begin;
		_dtEnd = end;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="DcmDateRange"/> class.
	/// </summary>
	/// <param name="range">Date range</param>
	/*this.DcmDateRange(DateTime[] range) {
		if (range.Length == 1) {
			DateTime = range[0];
			return;
		}
		if (range.Length == 2) {
			Begin = range[0];
			End = range[1];
			return;
		}
	}*/
	//endregion

	//# region Public Properties
	/// <summary>
	/// Gets or sets the date time.
	/// </summary>
	/// <value>The date time.</value>
	/*public DateTime DateTime {
		get {
			_dtEnd = _dtBegin;
			return _dtBegin;
		}
		set {
			_dtBegin = _dtEnd = value;
		}
	}*/

	/// <summary>
	/// Gets or sets the start date.
	/// </summary>
	/// <value>Start date</value>
	this.Begin = function(value) {
		if(value) {			
			_dtBegin = value;
		}
		else{
			return _dtBegin;
		}
	}

	/// <summary>
	/// Gets or sets the end date.
	/// </summary>
	/// <value>End date</value>
	this.End = function(value){
		if(value) {			
			_dtEnd = value;
		}
		else{
			return _dtEnd;
		}
	}
	

	//# region Public Members
	/// <summary>
	/// Gets date range as formatted string.
	/// </summary>
	/// <param name="format">DateTime format</param>
	/// <returns></returns>
	this.ToString = function(/*string*/ format) {
		return "19001010";
	}
}