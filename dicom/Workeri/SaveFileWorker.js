﻿
self.requestFileSystemSync = self.webkitRequestFileSystemSync ||
                             self.requestFileSystemSync;

var fs = requestFileSystemSync(TEMPORARY, 1024 * 1024 /*1MB*/);

onmessage = function (e)
{    
    //var data = e.data;
    var date = new Date();
    var ticks = date.getTime();
    var blob1 = new Blob([new Uint8Array(e.data)]);
    var fileEntry = fs.root.getFile(ticks + ".dcm", { create: true});
    fileEntry.createWriter().write(blob1);
};