function initFS() {
	window.requestFileSystem(window.TEMPORARY, 50 * 1024 * 1024, function(filesystem) {
		fs = filesystem;

		//******* test *****************************************
		//var pp = new DicomFileFormat();
		//cr ne radi
		//pp.Load("test.dcm", null, DicomReadOptions.AllowSeekingForContext);
		//ct radi
		//pp.Load("test.dcm", null, DicomReadOptions.AllowSeekingForContext);
		//******* test *****************************************		

		ReadFilesOnFS();


	}, errorHandler);
}

function errorHandler(e) {
	var msg = '';
	switch (e.code) {
		case FileError.QUOTA_EXCEEDED_ERR:
			msg = 'QUOTA_EXCEEDED_ERR';
			break;
		case FileError.NOT_FOUND_ERR:
			msg = 'NOT_FOUND_ERR';
			break;
		case FileError.SECURITY_ERR:
			msg = 'SECURITY_ERR';
			break;
		case FileError.INVALID_MODIFICATION_ERR:
			msg = 'INVALID_MODIFICATION_ERR';
			break;
		case FileError.INVALID_STATE_ERR:
			msg = 'INVALID_STATE_ERR';
			break;
		default:
			msg = 'Unknown Error';
			break;
	};
	console.log('Error: ' + msg);
}

function toArray(list) {
	return Array.prototype.slice.call(list || [], 0);
}

function Logger(message, type) {
	var color = "";

	switch (type) {
		case LOGGER.SUCCESS:
			color = 'text-success';
			break;
		case LOGGER.ERROR:
			color = 'text-danger';
			break;
		default:
			color = 'text-warning';
			break;
	};

	var div = document.createElement("div");
	div.innerHTML = "<small class='" + color + "'>" + message + "</small>";
	logger.insertBefore(div, logger.firstChild);
}

function DeleteFSEntries() {
	var dirReader = fs.root.createReader();
	var entries = [];

	// Call the reader.readEntries() until no more results are returned.
	var readEntries1 = function() {
		dirReader.readEntries(function(results) {
			results.forEach(function(entry, i) {
				entry.remove(function() {
					console.log('File removed.');
				}, errorHandler);
			});

			ReadFilesOnFS();

		}, errorHandler);
	};

	readEntries1();
}

function ReadFilesOnFS() {

	$("#filelist").empty();

	var dirReader = fs.root.createReader();
	var entries = [];

	// Call the reader.readEntries() until no more results are returned.
	var readEntries = function() {
		dirReader.readEntries(function(results) {
			if (!results.length) {
				listResults(entries.sort());
			} else {
				entries = entries.concat(toArray(results));
				readEntries();
			}
		}, errorHandler);
	};

	readEntries(); // Start readin
}


function listResults(entries) {
	// Document fragments can improve performance since they're only appended
	// to the DOM once. Only one browser reflow occurs.
	var fragment = document.createDocumentFragment();

	entries.forEach(function(entry, i) {

		$("#filelist").append("<li class='list-group-item'><a href='" + entry.toURL() + "'>" + entry.name + "</a><span id='badge-" + i + "' class='badge'></span></li>");

		entry.getMetadata(function(metadata) {
			//console.log(metadata); // or do something more useful with it...
			$("#badge-" + i).append(Math.floor(metadata.size / 1024) + " KB");
		});


	});

	document.querySelector('#filelist').appendChild(fragment);
}