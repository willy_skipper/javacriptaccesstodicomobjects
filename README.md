# Javascript DICOM network library#

**Proof-of-concept** implementation of the DICOM network protocol in the JavaScript.

### How do I get set up? ###

* Download the repo
* Put it on the web server
* Go to the index.html
* Make sure to set DICOM 2 WebSocket proxy up and running https://bitbucket.org/willy_skipper/dicom2websocket
* Connect to the WebSocket proxy
* Use DICOM ping, query or move
* You can also receive DICOM objects